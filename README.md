# The Narrator [Web Server]

## Discord Bot Setup

### **Invite the bot to your server** 
* [`Located Here`](https://discordapp.com/oauth2/authorize?client_id=384543567034449920&scope=bot)

### **Setup Permissions**
* Create a Bot role that can do the following.  _Rember to add the role to the Narrator!_
#### BotRole Permissions
* **Manage Roles**
  * Can create the _Living_ role which allows people to talk only if they're alive, removing it if they die
  
#### HomeChannel Permissions (_the home channel of the bot_)
* **Manage Messages**
  * Delete other's messages, for less spam.  Like vote, prefer, or basic chat enforcement _to be added_
* **Create Instant Invite**
  * For ease of use going back to the main chat channel, and for sending out invites to people subscribed.
* **Read Message History**
  * Partially to keep a changing timer on how long nighttime is
* **Embed Links**
  * Allows it to pretty print game info, vote count, deaths, you name it
* **Send Messages**
  * Silly, but because the bot stops @Everyone from talking in the channel, it'll also stop itself.  Grant it the power to always talk in this channel

> I'd also recommend blocking the bot from talking in the other channels.  Otherwise people can start games anywhere.

## Dev Setup
1. clone the repo
`git clone git@gitlab.com:the-narrator/NarratorNode.git`

### Create Config
1. install jq (this can be done on mac using `brew install jq`)
2. open up ~/.bash_profile and add the following lines to it.  End of the file is fine
* `export NARRATOR_CONFIG_LOCATION=<REPO_LOCATION>/config.json` repo location is where you downloaded the narrator repo
* `export NARRATOR_ENVIRONMENT=local`
* `export RANDOM_ROLE_ASSIGNMENT=false`
* `export PORT_NUMBER=4501`
* `export TEST_MODE=true`
3. run `source ~/.bash_profile` when you're done

### Java Setup
1. install java sdk.  You'll need this to compile the files
* I use 1.8

2. Temporarily comment or delete out the flyway block in `build.gradle`
3. Open Eclipse
4. Go to File -> Import -> Gradle> Existing Gradle Project
5. Click next/finish.  For project directory, set it as the narrator repo directory.  All the defaults should be fine
6. Open LogicTests.java (don't run it yet)
7. Hover over one of the errors in this file.  It should give you an option to fix it by adding junit3 to the build project.  Do that
8. Run LogicTests with Junit by right clicking on LogicTests.java in the Package explorer.  It'll fail.  That's okay
9. Open run configurations by clicking the little black arrow by the green triangle.
10. Go to Junit -> Logic Tests.  Click the environment tab.  Input a new variable.  The name being `NARRATOR_CONFIG_LOCATION` and the value being the location of config.json
11. Logic tests should now run! 

### DB Setup
1. Install mariadb 
* https://mariadb.com/kb/en/library/installing-mariadb-on-macos-using-homebrew/
* go ahead and follow all the steps until you log in
2. run `sudo mysql_secure_installation`
3. Create a database called `narrator` by opening up a mariadb terminal, and running `CREATE DATABASE narrator;`
4. Install flyway `brew install flyway`
5. Temporarily set the following environment variables
* db_username -> your db username
* db_password -> your db password
* db_hostname -> localhost
* db_name -> narrator
6. Run database migrations`flyway migrate -user=$DB_USERNAME -password=$DB_PASSWORD -url=jdbc:mariadb://$DB_HOSTNAME/$DB_NAME -locations=filesystem:src`
7. Open up config.json, and edit the following keys (they should be at the bottom of the file)_
* db_username -> your db username
* db_password -> your db password
* db_hostname -> localhost
* db_name -> narrator
8. Run NodeTests with Junit by right clicking on NodeTests.java in the Package explorer.  It'll fail.  That's okay
9. Open run configurations by clicking the little black arrow by the green triangle.
10. Go to Junit -> Node Tests.  Click the environment tab.  Input a new variable.  The name being `NARRATOR_CONFIG_LOCATION` and the value being the location of config.json
11. Node Tests should now work!

### Node Setup

1. Download nodejs
* https://nodejs.org/en/download/
2. run `npm install`
3. run `sudo npm install -g nodemon` to install nodemon
4. run `scp ${SERVER_USERNAME}@narrator.systeminplace.net:/home/voss/prod/firebase.json .` to get firebase file
5. run `npm install -g sass` to install sass

### How to extract backup sql data
1. ssh into prod
2. verify mariadb docker instance is running `docker run --name some-mariadb -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mariadb:10.1.34`
3. get container id `docker ps -a`
4. start container `docker start <id>`
5. bash into mariadb docker `docker exec -it <id>> bash`
6. dump database `mysqldump -h <db hostname> -u <db_user> narrator -p > backup.sql`
7. bring backup into server files `docker cp <id>:/backup.sql backup.sql`
8. copy backup to local `scp <server>/home/voss/backup.sql .`
```
