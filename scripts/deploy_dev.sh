DB_USERNAME=$1
DB_PASSWORD=$2
NARRATOR_ENVIRONMENT=$3
FIREBASE_TOKEN=$4
DISCORD_MASTER_KEY=$5
SC2MAFIA_API_KEY=$6;
SC2MAFIA_PASSWORD=$7;

. scripts/create_config.sh

curl https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/5.2.1/flyway-commandline-5.2.1-linux-x64.tar.gz -o flyaway.tar.gz
tar -xvzf flyaway.tar.gz > /dev/null 2>&1
rm flyaway.tar.gz
./flyway-5.2.1/flyway migrate -user=$DB_USERNAME -password=$DB_PASSWORD -url=jdbc:mariadb://$DB_HOSTNAME/$DB_NAME -locations=filesystem:src
rm -rf flyway*

sh scripts/vis_restart.sh
echo 'Compiling Java files.'
sh scripts/compile.sh

sudo iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 4501
