gradle flywayClean
mv src/main/resources/db/migration/V2__setup_refactor.sql .
gradle flywayMigrate
cat ../docker_narrator/backup.sql2 | mysql narrator --user root
mv V2__setup_refactor.sql src/main/resources/db/migration/
gradle flywayMigrate

afplay public/audio/ping.mp3
