const db = require('../web/db_communicator');

function getURL(token){
  return 'narrator.systeminplace.net/' + token;
}

var args = process.argv;
if(args.length < 3){
  console.log('not enough args');
  process.exit();
}

db.connect()
.then(_ =>{
  var name = args[2];
  return db.query('select token, game_id from player_saved_games where deathday is null and name = \'' + name + '\';');
}).then((results) => {
  for(var i in results){
    console.log(results[i].game_id, getURL(results[i].token));
  }
}).catch(err => {
    console.log(err);
}).then(_ => {
  process.exit();
});
