# compile scss to css files
[ -d static/css ] || mkdir static/css

sass static/scss/index.scss static/css/index.css -C
sass static/scss/index-tablet.scss static/css/index-tablet.css -C
sass static/scss/index-desktop.scss static/css/index-desktop.css -C

sass static/scss/condorcet.scss static/css/condorcet.css -C
sass static/scss/deatheditor.scss static/css/deatheditor.css -C

# minify html files
html-minifier public/index.html --html5 --collapse-whitespace --remove-comments --removeRedundantAttributes -o public/index2.html
mv public/index2.html public/index.html

# minify css files
csso static/css/index.css public/css/index.css
csso static/css/index-tablet.css public/css/index-tablet.css
csso static/css/index-desktop.css public/css/index-desktop.css

csso static/css/condorcet.css public/css/condorcet.css
csso static/css/deatheditor.css public/css/deatheditor.css

[ -d static/css ] || mkdir static/css
# minify js files
npm run buildJS
