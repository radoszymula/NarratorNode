const path = require('path');


module.exports = {
    entry: {
        condorcet: './static/js/condorcet.js',
        index: './static/js/index.js',
    },
    output: {
        path: path.resolve(__dirname, 'public/js'),
    },
    optimization: {
        minimize: false,
    },
};
