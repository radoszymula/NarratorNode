const helpers = require('./browserHelpers');


function ArrayList(){
    this.backer = [];
}

ArrayList.prototype.add = function(x, isUnique){
    if(!isUnique || !this.contains(x))
        this.backer.push(x);
};

ArrayList.prototype.get = function(num){
    return this.backer[num];
};

ArrayList.prototype.contains = function(x){
    for(let i = 0; i < this.backer.length; i++)
        if(x === this.backer[i])
            return true;

    return false;
};

ArrayList.prototype.remove = function(x, squelch){
    const index = this.backer.indexOf(x);
    if(index > -1)
        this.backer.splice(index, 1);
    else if(!squelch)
        helpers.log('element not found', x);
};

ArrayList.prototype.removeLast = function(){
    if(!this.backer.length)
        return null;
    const index = this.backer.length - 1;
    return this.backer.splice(index, 1)[0];
};

ArrayList.prototype.isEmpty = function(){
    return this.backer.length === 0;
};

ArrayList.prototype.clear = function(){
    this.backer = [];
};

ArrayList.prototype.size = function(){
    return this.backer.length;
};

module.exports = {
    ArrayList,
};
