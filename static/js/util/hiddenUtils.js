/* eslint-env browser */
const colors = require('../util/colors');


function getColor(hidden){
    const setup = window.gameState.setup;
    const factionIDs = hidden.factionRoleIDs
        .map(factionRoleID => setup.factionRoleMap[factionRoleID].factionID);
    const roleSet = new Set(factionIDs);
    if(roleSet.size === 1)
        return setup.factionMap[factionIDs[0]].color;
    return colors.white;
}

module.exports = {
    getColor,
};
