/* eslint-env browser, jquery */


function equalStringLists(l1, l2){
    if(l1.length !== l2.length)
        return false;

    for(let i = 0; i < l1.length; i++)
        if(l1[i] !== l2[i])
            return false;


    return true;
}

function flattenList(inputList){
    return inputList.reduce((acc, val) => acc.concat(val), []);
}

function getBaseURL(){
    if(window.location.href.indexOf('localhost') !== -1)
        return 'http://localhost:4501/';
    if(window.location.href.indexOf('4000') !== -1)
        return 'http://narrator.systeminplace.net:4000/';
    return 'http://narrator.systeminplace.net/';
}

function getURLParameters(){
    const parameters = {};
    let tmp = [];
    window.location.search
        .substr(1)
        .split('&')
        .forEach(item => {
            tmp = item.split('=');
            const key = tmp[0];
            let value = tmp[1];
            if(value === 'false')
                value = false;
            parameters[key] = value;
        });
    return parameters;
}

function isAlpha(str){
    return str.match(/^[a-zA-Z]+$/i) !== null;
}

function isAlphaNumeric(str){
    return str.match(/^[a-zA-Z0-9]+$/i) !== null;
}

function isLoggedIn(){
    return window.user && !window.user.isAnonymous;
}

function isMobile(){
    return $('body').width() < 601;
}

function isOnLobby(){
    return $('#lobby_page').is(':visible');
}

function log(err, message){
    if(!err){
        err = message;
        message = null;
    }

    console.log(err); // eslint-disable-line no-console
    if(message)
        console.log(message); // eslint-disable-line no-console
}

function setPseudoURL(pseudoURL){
    window.history.pushState('', '', pseudoURL);
}

function storageEnabled(){
    return typeof(Storage) !== 'undefined';
}

module.exports = {
    equalStringLists,
    flattenList,
    getBaseURL,
    getURLParameters,
    isAlpha,
    isAlphaNumeric,
    isLoggedIn,
    isMobile,
    isOnLobby,
    log,
    setPseudoURL,
    storageEnabled,
};
