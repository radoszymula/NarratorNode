/* eslint-env jquery, browser */
/* global firebase */
const helpers = require('./browserHelpers');

function get(endpoint, args, headers){
    return serverRequest(endpoint, args, headers, 'GET');
}

function post(endpoint, args, headers){
    return serverRequest(endpoint, args, headers, 'POST');
}

function put(endpoint, args, headers){
    return serverRequest(endpoint, args, headers, 'PUT');
}

function del(endpoint, args, headers){
    return serverRequest(endpoint, args, headers, 'DELETE');
}

function serverRequest(endpoint, args, headers, method){
    const url = helpers.getBaseURL() + endpoint;
    const async = true;

    let request;

    if(args && typeof(args) === 'object')
        args = JSON.stringify(args);

    return getBrowserAuthRequest()
        .then(authRequest => {
            request = new XMLHttpRequest();
            request.open(method, url, async);
            if(window.user){
                request.setRequestHeader('auth', authRequest.token);
                request.setRequestHeader('authtype', authRequest.wrapperType);
            }
            if(helpers.getURLParameters().auth_token)
                request.setRequestHeader('auth_token', helpers.getURLParameters().auth_token);

            return new Promise(((resolve, reject) => {
                request.onload = function(){
                    if(request.status !== 200 && request.status !== 204){
                        if(request.status === 500)
                            helpers.log(request.responseText);
                        let data;
                        try{
                            data = JSON.parse(request.responseText);
                        }catch(err){
                            data = request.responseText;
                        }
                        return reject(data);
                    }
                    if(request.responseText){
                        const response = JSON.parse(request.responseText);
                        resolve(response);
                    }else{
                        resolve();
                    }
                };
                if(args)
                    request.send(args);
                else
                    request.send();
            }));
        });
}

async function getBrowserAuthRequest(){
    const firebaseToken = await getToken();
    return {
        wrapperType: 'firebase',
        token: firebaseToken,
    };
}

function getToken(){
    if(typeof(firebase) === 'undefined')
        return '';
    return new Promise(resolve => {
        const unsubscribe = firebase.auth().onAuthStateChanged(user => {
            unsubscribe();
            if(user)
                user.getIdToken().then(idToken => {
                    resolve(idToken);
                }, () => {
                    resolve(null);
                });
            else
                resolve(null);
        });
    });
}

module.exports = {
    get,
    post,
    put,
    delete: del,

    getBrowserAuthRequest,
    getToken,
};
