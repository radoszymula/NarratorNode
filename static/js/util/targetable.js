/* eslint-env browser, jquery */
const selectOptions = require('./selectOptions');


function Targetable(player, text, isSplit, hoverable){
    const playerName = player.name;
    const { isComputer } = player;
    const li = $('<li>');
    const gameState = window.gameState;
    li.addClass('action_targetable_element');
    li.addClass(`playerLI${playerName.replace(/\s/g, '')}`);
    li.attr('name', playerName);

    this.isComputer = isComputer;
    this.elm = li;
    this.player = player;

    if(window.gameState.started){
        const divWrapper = $('<div>');
        divWrapper.addClass('playerList_wrapper');

        const rightDiv = $('<div>');
        rightDiv.addClass('right_targetable targetable_half');
        rightDiv.attr('name', playerName);
        const leftDiv = $('<div>');
        leftDiv.addClass('left_targetable targetable_half');
        leftDiv.attr('name', playerName);

        const rightDivClick = $('<div>').addClass('right_targetable_click targetable_half');
        const leftDivClick = $('<div>').addClass('left_targetable_click targetable_half');
        rightDivClick.attr('name', playerName);
        leftDivClick.attr('name', playerName);

        const nameWrapperDiv = $('<div>');
        nameWrapperDiv.addClass('playerList_name');

        const p = $('<p>');
        p.addClass('playername');
        p.text(text);

        nameWrapperDiv.append(p);

        li.append(divWrapper);
        divWrapper.append(leftDiv);
        divWrapper.append(rightDiv);
        divWrapper.append(leftDivClick);
        divWrapper.append(rightDivClick);
        divWrapper.append(nameWrapperDiv);
        if(hoverable)
            addHoverProperties(leftDiv, rightDiv, leftDivClick, rightDivClick, isSplit);
    }else{
        li.click(() => {
            const stats = {
                wins: 0,
                losses: 0,
            };
            const showKick = gameState.profile.name !== playerName && gameState.isHost();
            const showPing = !isComputer && window.user.displayName !== playerName;
            const showRepick = (Object.values(gameState.playerMap).length - 1)
                && (gameState.isHost() || gameState.host.name === playerName);
            showUserDetailPopup(playerName, stats, showKick, showPing, showRepick);
        });

        li.append($('<span>').text(text));
    }
    this.refreshActivityMarking();

    this.leftClass = null;
    this.rightClass = null;
    this.log = false;

    this.addClass(true, selectOptions.TARGETABLE_LEFT_DIV); // is left
    this.addClass(false, selectOptions.TARGETABLE_RIGHT_DIV); // is right
}

module.exports = {
    Targetable,
};

Targetable.prototype.addClass = function(isLeft, className){
    let div;
    if(isLeft){
        div = this.elm.children().find('.left_targetable');
        this.leftClass = className;
    }else{
        div = this.elm.children().find('.right_targetable');
        this.rightClass = className;
    }
    div.addClass(className);
};

Targetable.prototype.removeClass = function(isLeft){
    let className;
    let div;
    if(isLeft){
        div = this.elm.children().find('.left_targetable');
        className = this.leftClass;
        this.leftClass = null;
    }else{
        div = this.elm.children().find('.right_targetable');
        className = this.rightClass;
        this.rightClass = null;
    }
    div.removeClass(className);
};

Targetable.prototype.makeVisible = function(selectableContainer){
    this.elm.appendTo(selectableContainer);
};

Targetable.prototype.refreshActivityMarking = function(){
    const isActive = window.gameState.activeUserIDs.has(this.player.userID);
    const li = this.elm;
    if(window.gameState.started)
        if(isActive)
        // li.css('font-weight', 'bold');
            li.css('font-style', null);
        else
        // li.css('font-weight', null);
            li.css('font-style', 'italic');

    const playerName = this.player.name;
    this.elm.find('i').remove();

    const setupMarking = $('<i>').addClass('fa').addClass('playerListSetupMarking');
    if(window.gameState.host.id === this.player.userID){
        setupMarking.addClass('fa-star');
        if(isActive || window.gameState.profile.name === playerName)
            setupMarking.css('color', 'yellow');
    }else if(window.gameState.profile.name === playerName){
        setupMarking.addClass('fa-user');
        setupMarking.css('color', 'green');
    }else if(this.isComputer){
        setupMarking.addClass('fa-laptop');
    }else{
        setupMarking.addClass('fa-circle');
        if(isActive)
            setupMarking.css('color', 'green');
    }

    li.prepend(setupMarking);
};

module.exports = {
    Targetable,
};

function addHoverProperties(leftDiv, rightDiv, leftDivClick, rightDivClick, isSplit){
    leftDivClick.mouseenter(() => {
        leftDiv.addClass('targetable_hovering');
        if(!isSplit)
            rightDiv.addClass('targetable_hovering');
    });
    rightDivClick.mouseenter(() => {
        rightDiv.addClass('targetable_hovering');
        if(!isSplit)
            leftDiv.addClass('targetable_hovering');
    });
    leftDivClick.mouseleave(() => {
        leftDiv.removeClass('targetable_hovering');
        if(!isSplit)
            rightDiv.removeClass('targetable_hovering');
    });
    rightDivClick.mouseleave(() => {
        rightDiv.removeClass('targetable_hovering');
        if(!isSplit)
            leftDiv.removeClass('targetable_hovering');
    });

    leftDivClick.mousedown(() => {
        leftDiv.addClass('targetable_active');
        if(!isSplit)
            rightDiv.addClass('targetable_active');
    });
    rightDivClick.mousedown(() => {
        rightDiv.addClass('targetable_active');
        if(!isSplit)
            leftDiv.addClass('targetable_active');
    });

    leftDivClick.mouseup(() => {
        leftDiv.removeClass('targetable_active');
        if(!isSplit)
            rightDiv.removeClass('targetable_active');
    });
    rightDivClick.mouseup(() => {
        rightDiv.removeClass('targetable_active');
        if(!isSplit)
            leftDiv.removeClass('targetable_active');
    });
}

function showUserDetailPopup(playerName, stats, showKick, showPing, showRepick){
    $('.userPopupHeader').text(playerName);

    require('../index').setPlot(stats.wins, stats.losses, 'userPopupContainer', true);

    let botPlacement = 0;

    botPlacement = showUSerDetailPopupButton('userPopupKick', showKick, botPlacement);
    botPlacement = showUSerDetailPopupButton('userPopupPing', showPing, botPlacement);
    showUSerDetailPopupButton('userPopupRepick', showRepick, botPlacement);

    if(showRepick){
        let repickText;
        if(window.gameState.isHost())
            if(playerName === window.gameState.profile.name)
                repickText = 'Ditch Host';
            else
                repickText = 'Set Host';
        else
            repickText = 'Vote to Repick';
        $('#userPopupRepick span').text(repickText);
    }

    $('#userPopupBackdrop').show();
    $('#userPopup').show(800);
}

function showUSerDetailPopupButton(labelID, shouldShow, bot){
    const e = $(`#${labelID}`);
    if(shouldShow){
        e.show();
        e.css('bottom', bot);
        bot += e.height();
    }else{
        e.hide();
    }

    return bot;
}
