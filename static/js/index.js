/* eslint-env jquery, browser */
/* global SimpleBar, Plotly */

const helpers = require('./util/browserHelpers');
const requests = require('./util/browserRequests');

const actionService = require('./services/browserActionService');
const authService = require('./services/browserAuthService');
const chatService = require('./services/browserChatService');
const factionModifierService = require('./services/browserFactionModifierService');
const gameService = require('./services/browserGameService');
const moderatorService = require('./services/browserModeratorService');
const phaseService = require('./services/browserPhaseService');
const playerService = require('./services/browserPlayerService');
const setupService = require('./services/browserSetupService');
const setupHiddenService = require('./services/browserSetupHiddenService');
const setupModifierService = require('./services/browserSetupModifierService');
const soundService = require('./services/browserSoundService');
const storageService = require('./services/browserStorageService');

const chatView = require('./views/chatView');
const factionDetailsView = require('./views/factionDetailsView');
const factionsView = require('./views/factionsView');
const generalSettingsView = require('./views/generalSettingsView');
const playerOverview = require('./views/playerOverview');
const playerView = require('./views/playerView');
const rolesOverviewView = require('./views/versatileOverview');
const roleInfoView = require('./views/roleInfoView');
const rolesView = require('./views/rolesView');
const voteOverview = require('./views/voteView');

const { ArrayList } = require('./util/arrayList');
const { GameState } = require('./models/gameState');
const hiddenUtils = require('./util/hiddenUtils');
const roleImages = require('./util/roleImages');
const { Targetable } = require('./util/targetable');
const selectOptions = require('./util/selectOptions');

const NORM_TEXT_COLOR = '#F2F2F2';

const PORT_NUMBER = window.location.href.indexOf('4000') !== -1 ? ':4000' : ':4501';

const LOGIN = 0;
const SIGN_UP = 1;
const JOIN_GAME = 2;
const UNIQUE = true;

const RANDOMS_SELECTED_FACTION_ID = -1;

let socketConnectingPromise = null;
let user = null;

let right = null;
let left = null;
let savedOpts = [null, null, null];
let selected = null;
let selected2 = null; // for jailors left and right selected lists
let deadSelected = null;
let currentAction = null;

const seenFeedbacks = [];

const J = {
    HTML_IDS: {
        TOP_SELECT: 'teamSelect',
        MID_SELECT: 'midSelect',
        BOT_SELECT: 'tailor_spinner',
    },
};

const SETTINGS_KEYS = ['narration_enabled', 'chat_ping', 'music_enabled', 'day_music_enabled',
    'night_music_enabled',
];

function resetGameState(){
    window.gameState = new GameState();


    $('#team_catalogue_pane').empty();
    $('#chat_tabs').empty();
    $('#chat_ticker_new_message').text('');
    $('#chat_ticker_expander i').removeClass('unread_ticker');
    $('#last_will_area').val('');
    $('#last_will_area').hide();
    $('#last_will_button').text('Last Will');
    $('.last_will_item').css('display', ''); // when you die, i hide it in the graveyard, and changing the class later doesn't unhide it.

    window.gameState.warning = Promise.resolve();
    window.gameState.speech = Promise.resolve();


    if(!helpers.storageEnabled())
        SETTINGS_KEYS.forEach(value => {
            window.gameState[value] = false;
        });
}

resetGameState();

function isOnSetup(){
    return $('#setup_page').is(':visible');
}

function isOnMain(){
    return $('#main').is(':visible');
}

function isTablet(){
    return $('#tablet').css('color') === 'rgb(255, 165, 0)';
}

(function($){
    $.fn.hasScrollBar = function(){
        return this.get(0).scrollWidth > this.width();
    };
}(jQuery));

function setRead(e){
    pushServerUnread(e.attr('name'));
    if(!getUnread())
        $('#chat_ticker_expander i').removeClass('unread_ticker');
    e.removeClass('unread_ticker');
}

function pushServerUnread(newLogName){
    webSend({
        message: 'setReadChat',
        setReadChat: newLogName,
    });
    window.gameState.unreadChats[newLogName] = 0;
}

function removeNonAlphaNumericCharacters(str){
    return str.replace(/[^0-9a-z]/gi, '');
}

function getPuppets(){
    if(!window.gameState.role || !window.gameState.role.puppets)
        return [];
    return window.gameState.role.puppets.map(puppetName => puppetName.replace(/ vote/ig, ''));
}

function hasPuppets(){
    return getPuppets().length;
}

function onCurrentDayChat(){
    return window.gameState.chatKeys[$('.activeTab').attr('name')] === 'everyone';
}

function getPuppetList(){
    const puppets = getPuppets();
    if(window.gameState.chatKeys[`Day ${window.gameState.dayNumber}`] === 'everyone')
        puppets.push(window.gameState.role.displayName);

    return puppets;
}

function getVisiblePuppetList(){
    const vPuppets = [];
    $('#puppetList li').each(function(){
        vPuppets.push($(this).text());
    });

    return vPuppets;
}

function puppetChangeClick(e){
    e = $(this);
    e.parent().hide();
    if(e.hasClass('selectedPuppet'))
        return;
    $('.selectedPuppet').removeClass('selectedPuppet');

    e.addClass('selectedPuppet');
    setPuppetTalk(e.text());
}

function reloadPuppetChoices(){
    const chatName = $('.activeTab').attr('name');

    const vPuppets = getVisiblePuppetList();

    $('#puppetList').empty();
    const puppets = getPuppetList();

    if(!chatName || helpers.equalStringLists(puppets, vPuppets))
        return;

    puppets.forEach((puppetName, i) => {
        const fa = $('<i>').addClass('fa fa-commenting puppetIco');

        const li = $('<li>');
        li.text(puppetName);
        li.append(fa);
        li.click(puppetChangeClick);

        if(i === puppets.length - 1)
            li.addClass('selectedPuppet');

        $('#puppetList').append(li);
    });
}


function tabClick(){
    const e = $(this);
    setRead(e);

    if(e.hasClass('activeTab') && !helpers.isMobile())
        return;
    $('.activeTab').removeClass('activeTab');

    e.addClass('activeTab');

    chatView.setScrolledToBottom();
    $('#messages').empty();

    chatService.setActiveChat(e.attr('name'));

    const key = window.gameState.chatKeys[e.attr('name')];

    if(key && !window.gameState.integrations.includes('sc2mafia'))
        showChatInput();
    else
        chatView.hideChatInput();

    const cName = e.attr('name') || '';

    if(hasPuppets() && onCurrentDayChat() && cName.toLowerCase().indexOf('day ') !== -1)
        $('body').addClass('puppetsShowing');
    else
        $('body').removeClass('puppetsShowing');


    $('#puppetList').hide();

    if(key)
        setPuppetTalk(window.gameState.profile.name);
    else if(hasPuppets())
        setPuppetTalk(getPuppets()[0]);


    reloadPuppetChoices();

    if(helpers.isMobile())
        switchToChatView(key || (hasPuppets() && onCurrentDayChat()));
}

function setPuppetTalk(name){
    $('#puppetText').text(name);
}

function showChatInput(){
    $('body').addClass('chat_active');
}

function switchToChatView(inputShowing){
    hideLeftPane();
    $('#chat_tabs_wrapper').hide();
    $('.chat_pane').show();
    if(!inputShowing)
        $('#chat_ticker_pane').show();
    else
        $('#chat_ticker_pane').hide();
    $('#chat_ticker_expander').show();
}

function addTab(chatObject){
    const chatHeader = chatObject.chatName;
    const div = $('<div>').addClass('chatTab');
    div.attr('name', chatHeader);
    div.attr('id', chatHeader.replace(/\s/g, ''));
    div.append($('<div>').addClass('chatPointer'));

    // mobile portion
    const aMobile = $('<a>').addClass('mobileChatLabel');
    const iMobile = $('<i>').addClass(chatObject.chatType);
    aMobile.append(iMobile);
    aMobile.append(' ');
    aMobile.append(chatHeader);
    div.append(aMobile);


    // tablet portion
    const aTablet = $('<a>');


    const iTablet = $('<i>').addClass(chatObject.chatType);
    iTablet.addClass('chatTabIcon');
    if(chatHeader.startsWith('Day'))
        iTablet.text(chatHeader.replace('Day', ''));
    else if(chatHeader.startsWith('Night'))
        iTablet.text(chatHeader.replace('Night', ''));
    div.append(iTablet);


    if(chatObject.chatDay && isTablet())
        aTablet.append(chatObject.chatDay);
    else
        aTablet.append(chatHeader);

    iTablet.append(aTablet);


    div.click(tabClick);


    window.gameState.chats[chatHeader] = {
        messages: [],
        voteCount: -1,
    };
    let oldMessage;
    const chats = window.gameState.chats;
    for(let i = 0; chats.null && i < chats.null.messages.length; i++){
        oldMessage = window.gameState.chats.null.messages[i];
        window.gameState.chats[chatHeader].messages.push(oldMessage);
    }

    const sl = new SimpleBar($('#chat_tabs')[0]);
    if(helpers.isMobile())
        $(sl.getContentElement()).prepend(div);
    else
        $(sl.getContentElement()).append(div);


    sl.recalculate();
}

function addToChat(message){
    if(typeof(message) !== 'object')
        message = {
            text: message,
        };

    if(message.text && !message.text.length)
        return;
    if(soundService.defaultSound('chat_ping'))
        $('#newChatMessageAudio')[0].play();
    if(message.chat && message.chat[0] && message.chat[0] === 'Pregame')
        if(!window.gameState.isOver() && window.gameState.started)
            return;
        else{
            chatService.chatQueue.push(message);
            return ['Pregame', message];
        }

    const currentChatID = $('.activeTab').attr('id') || null;
    if(!message.chat){ // belongs to a particular chat
        helpers.log(message, 'no chat');
        throw message;
    }

    let returning;
    for(let i = 0; i < message.chat.length; i++){
        const chatHeader = message.chat[i];
        if(!window.gameState.chats[chatHeader])
            return helpers.log(`chat error${chatHeader}`, message);

        window.gameState.chats[chatHeader].messages.push(message);
        const activeHeaderElement = $('.activeTab');
        if(activeHeaderElement.length > 0){
            const activeHeader = activeHeaderElement.first().attr('name');
            if(activeHeader === chatHeader)
                chatService.chatQueue.push(message);
        }
        const messageType = message.messageType;
        if(messageType === 'VoteAnnouncement' || messageType === 'DeathAnnouncement'){
            window.gameState.chats[chatHeader].voteCount++;
            message.voteIndex = window.gameState.chats[chatHeader].voteCount;
        }
        if(!currentChatID || chatHeader.replace(' ', '') === currentChatID)
            returning = [chatHeader, message];
    }
    return returning;

    // }else{
    //     if(message.messageType === 'Feedback'){
    //         window.gameState.feedback.push(message);
    //     }
    //     var ret;
    //     for(chatHeader in window.gameState.chats){
    //         window.gameState.chats[chatHeader].messages.push(message);
    //         if(!currentChatID || chatHeader.replace(" ", "") === currentChatID)
    //             ret = chatHeader
    //     }
    //     chatQueue.push(message);
    //     return [ret, message];
    // }
}

function webSend(o){
    if(window.socket){
        window.socket.send(JSON.stringify(o));
        return;
    }
    connect().then(() => {
        window.socket.send(JSON.stringify(o));
    });
}

$('form').submit(() => false);

function ping(toPing){
    const o = {
        message: 'ping',
        ping: toPing,
    };

    webSend(o);
}

function onRuleClickChange(){
    const e = $(this);
    const value = e.is(':checked');
    const ruleName = e.attr('name');
    return setupModifierService.update(ruleName, value);
}

function onRuleValueChange(){
    const e = $(this);
    let value = e.val();
    if(value === '-')
        return;
    value = parseInt(value, 10);
    const ruleName = e.attr('name');
    return setupModifierService.update(ruleName, value);
}

function setSuperCustom(value){
    if(helpers.storageEnabled())
        return localStorage.setItem('isSuperCustom', value);
    window.isSuperCustom = value;
}

function isSuperCustom(){
    if(helpers.storageEnabled())
        return localStorage.getItem('isSuperCustom') === 'true';
    return window.isSuperCustom;
}

function isHostsSetup(){
    return window.gameState.isHost() && setupService.isHostsSetup();
}

function setActiveFaction(factionID){
    window.gameState.activeFactionID = factionID;
    window.gameState.activePlayerName = null;
    if(window.gameState.isStarted){
        window.gameState.activeFactionRoleID = null;
        window.gameState.activeHiddenID = null;
    }
}

function setActiveFactionRole(factionRoleID){
    if(factionRoleID && !window.gameState.isStarted)
        setActiveFaction(window.gameState.setup.factionRoleMap[factionRoleID].factionID);
    window.gameState.activeFactionRoleID = factionRoleID;
    window.gameState.activeHiddenID = null;
    window.gameState.activePlayerName = null;
}

function setActiveHidden(hiddenID){
    if(window.gameState.isStarted)
        window.gameState.activeFactionID = null;
    else
        window.gameState.activeFactionID = RANDOMS_SELECTED_FACTION_ID;
    window.gameState.activeHiddenID = hiddenID;
    window.gameState.activeFactionRoleID = null;
    window.gameState.activePlayerName = null;
}

function setActivePlayer(playerName){
    window.gameState.activeFactionID = null;
    window.gameState.activeHiddenID = null;
    window.gameState.activeFactionRoleID = null;
    window.gameState.activePlayerName = playerName;
}

function createRuleElement(rule, element, onClickAction){
    if(typeof(rule) === 'string')
        return element.html(`${rule}`);

    let type;
    let changeHandler;
    if(typeof(rule.value) === 'boolean'){
        type = 'checkbox';
        changeHandler = onRuleClickChange;
    }else{
        type = 'number';
        changeHandler = onRuleValueChange;
    }
    changeHandler = onClickAction || changeHandler;

    element.html(`${rule.label} <input type=${type}>`);
    element.show();

    const input = element.find('input');
    input.attr('name', rule.name);
    input.unbind();
    if(type === 'checkbox'){
        input.prop('checked', rule.value);
        if(window.gameState.isHost())
            input.click(changeHandler);
    }else{
        input.val(rule.value);
        if(window.gameState.isHost())
            input.bind('keyup input', changeHandler);
    }
    input.prop('disabled', !window.gameState.isHost());
}

function removeBots(numberOfBots){
    const o = {
        number_of_bots: numberOfBots,
        message: 'removeBotUsers',
    };
    webSend(o);
}

function setImage(ele, name){
    if(name)
        name = name.toLowerCase();
    const imageElement = new Image();

    imageElement.onload = function(){
        ele.find('img').remove();
        ele.prepend(imageElement);
    };
    imageElement.onerror = function(){
        // image did not load

        const err = new Image();
        err.src = '../rolepics/citizen.png';

        ele.find('img').remove();
        ele.prepend(err);
    };
    imageElement.src = `../rolepics/${name}.png`;
    ele.addClass('roleCardImage');
}

function convertColor(color){
    color = color.replace(' ', '');
    color = color.replace(' ', '');
    color = color.replace('rgb(', '');
    color = color.replace(')', '');
    color = color.split(',');

    return `#${hex(color[0])}${hex(color[1])}${hex(color[2])}`;
}

function refreshRolesUL(){
    const setup = window.gameState.setup;
    const activeFactionID = window.gameState.activeFactionID;
    let catalogueRoles;

    if(!activeFactionID)
        catalogueRoles = [];
    else if(activeFactionID === RANDOMS_SELECTED_FACTION_ID)
        catalogueRoles = Object.values(setup.hiddenMap)
            .filter(hidden => Object.values(hidden.factionRoleMap)
                .filter(factionRole => factionRole.name === hidden.name).length !== 1);
    else
        catalogueRoles = setup.factionMap[activeFactionID].factionRoles;

    catalogueRoles.sort((a, b) => a.name.localeCompare(b.name));

    function getName(role){
        return role.name;
    }

    function getColor(role){
        if(activeFactionID !== RANDOMS_SELECTED_FACTION_ID)
            return setup.factionMap[activeFactionID].color;
        return hiddenUtils.getColor(role);
    }

    function onRoleSingleULClick(){
        const e = $(this);
        const roleNameAttr = parseInt(e.attr('name'), 10);
        if(e.hasClass('rSetupFocused')){
            $('.rSetupFocused').removeClass('rSetupFocused');
            $('.rSetupBG').addClass('rSetupFocused').removeClass('rSetupBG');
            setActiveFactionRole(null);

            if(helpers.isMobile())
                $('.team_settings_pane').show();

            $('.role_settings_pane').hide();
            factionDetailsView.refresh();
        }else{
            $('#role_catalogue .rSetupFocused').removeClass('rSetupFocused');
            $('#team_catalogue .rSetupFocused').removeClass('rSetupFocused').addClass('rSetupBG');
            e.addClass('rSetupFocused');

            if(helpers.isMobile())
                $('.team_settings_pane').hide();

            $('.role_settings_pane').show();
            if(window.gameState.activeFactionID === RANDOMS_SELECTED_FACTION_ID)
                setActiveHidden(roleNameAttr);
            else
                setActiveFactionRole(roleNameAttr);
        }
        rolesOverviewView.refresh();
    }

    function onRoleDoubleULClick(e){
        const roleID = $(e.target).attr('name');
        setupHiddenService.addSetupHidden(parseInt(roleID, 10));
        event.stopPropagation(); // eslint-disable-line no-restricted-globals
    }

    function spanProperties(role, li, span){
        li.attr('name', role.id);
        span.attr('name', role.id);
    }

    function appendPlus(li, o){
        li.addClass(o.name.replace(' ', ''));
        if(window.gameState.activeFactionRoleID === o.id
            || window.gameState.activeHiddenID === o.id)
            li.addClass('rSetupFocused');

        if(window.gameState.isHost() && isHostsSetup()){
            const photo = $('<i>');
            photo.addClass('setup_plus_minus fa fa-lg fa-plus-square');

            const nameAttr = o.id;
            photo.attr('name', nameAttr);
            li.append(photo);

            photo.unbind();
            photo.click(onRoleDoubleULClick);
        }

        if(o.simpleName === 'Sheriff'){
            const i = $('<i>').addClass('fa setup_plus_minus');

            if(window.gameState.isHost() && isSuperCustom())
                i.addClass('fa-pencil');
            else
                i.addClass('fa-search');

            li.append(i);

            i.click(function(e){
                e = $(this).parent();
                let color = e.attr('name');
                const ind = color.indexOf('#') + 1;
                color = `color${color.substring(ind)}`;
                sheriffLoader(color);
            });
        }
    }
    populateUL('roles_pane', catalogueRoles, getName, getColor,
        [onRoleSingleULClick, null], spanProperties, appendPlus);
}

function populateUL(ulToPopulate, objectsToPopulate, getName,
    getColor, funcOnClicks, spanProperties, liProperties){
    if(typeof(ulToPopulate) === 'string')
        ulToPopulate = $(`#${ulToPopulate}`);
    ulToPopulate.unbind();
    ulToPopulate.empty();

    let li;
    let span;
    let objectPopulate;
    for(let i = 0; i < objectsToPopulate.length; i++){
        objectPopulate = objectsToPopulate[i];
        li = $('<li>');
        span = $('<span>');
        span.text(getName(objectPopulate));
        if(getColor)
            span.css('color', getColor(objectPopulate));

        if(funcOnClicks[0])
            li.click(funcOnClicks[0]);
        if(funcOnClicks[1])
            li.dblclick(funcOnClicks[1]);
        li.append(span);

        if(spanProperties)
            spanProperties(objectPopulate, li, span);


        if(liProperties)
            liProperties(li, objectPopulate);


        ulToPopulate.append(li);
    }
    new SimpleBar(ulToPopulate[0]); // eslint-disable-line no-new
}

function hardCodeSort(array, predeterminedArray, reader){
    array.sort((a, b) => {
        if(reader){
            a = reader(a);
            b = reader(b);
        }
        const index1 = predeterminedArray.indexOf(a);
        const index2 = predeterminedArray.indexOf(b);

        if(index1 === -1 && index2 === -1)
            return a.localeCompare(b);
        if(index1 === -1)
            return 1;
        if(index2 === -1)
            return -1;
        return index1 - index2;
    });
}

const ORDERING_PAR = ['Town', 'Mafia', 'Yakuza', 'Cult', 'Benigns', 'Outcasts', 'Serial Killer',
    'Arsonist', 'Mass Murderer', 'Poisoner', 'ElectroManiac'];

function refreshEnemiesUL(){
    const gameState = window.gameState;
    let factions;

    // no active faction, or neutrals/randoms
    if(gameState.setup.factionMap[gameState.activeFactionID])
        factions = gameState.setup.factionMap[gameState.activeFactionID].enemies;
    else
        factions = [];

    hardCodeSort(factions, ORDERING_PAR, x => x.name);

    function getName(faction){
        return faction.name;
    }

    function getColor(faction){
        return faction.color;
    }

    function spanProperties(faction, li, span){
        // name is in the allies, add the strike through class
        // if(gameState.activeFactionID.allies.indexOf(faction) !== -1){
        //     li.addClass('ally-strike-through');
        //     span.addClass('ally-strike-through');
        // }
        li.attr('name', faction.color);
        span.attr('name', faction.color);
    }

    populateUL('team_allies_pane', factions, getName, getColor, [null, null],
        spanProperties);
}

function hex(num){
    num = parseInt(num, 10);
    num = num.toString(16).toUpperCase();
    if(num.length === 1)
        num = `0${num}`;
    return num;
}


function showRoleInfo(){
    const hiddenID = parseInt($(this).attr('name'), 10);
    const hidden = window.gameState.setup.hiddenMap[hiddenID];
    if(hidden.factionRoleIDs.length !== 1)
        setActiveHidden(hidden.id);
    else
        setActiveFactionRole(hidden.factionRoleIDs[0]);
    if(isOnSetup()){
        rolesView.refresh();
        factionsView.refresh();
        factionDetailsView.refresh();

        const jQueryLocator = `#role_catalogue .roleID${hiddenID}`;
        const roleFocusedE = $(jQueryLocator);
        if(roleFocusedE.position())
            $('#roles_pane').animate({
                scrollTop: roleFocusedE.position().top,
            }, 'slow');
    }
    rolesOverviewView.refresh();
}


function setGraveYard(){
    const graveyard = window.gameState.graveyard;

    function graveYardClick(){
        const name = $(this).attr('name');
        setActivePlayer(name);
        rolesOverviewView.refresh();
    }

    for(let i = 0; i < graveyard.length; i++)
        window.gameState.deadPlayers.push(graveyard[i].name);


    function getName(dead){
        return `${dead.name} (${dead.roleName})`;
    }

    function getColor(dead){
        return window.gameState.setup.factionMap[dead.factionID].color;
    }

    function spanProperties(dead, li){
        const index = dead.name;
        li.addClass('roleHover');
        li.attr('name', index);
    }

    const graveYard = $('#graveyard_ul');
    populateUL(graveYard, graveyard, getName, getColor, [graveYardClick, null], spanProperties,
        null);

    const rolesListLength = window.gameState.setup.setupHiddens.length;
    const deadLength = graveyard.length;
    $('.peopleRemaining').text(rolesListLength - deadLength);
}

function setCommandsLabel(label){
    if(label === 'Gun')
        label = 'Give Gun';
    else if(label === 'Armor')
        label = 'Give Armor';
    else if(label === 'SpyAbility')
        label = 'Spy on';
    else if(label === 'Build')
        label = 'Build Room';
    else if(label === 'Plan')
        label = 'Death Reveal';
    else if(label === 'Murder')
        label = 'Order Hit';
    else if(label === 'Jail' && !window.gameState.isDay())
        label = 'Execute';
    else if(label === 'Votesteal')
        label = 'Redirect Vote';
    else if(label === 'Compare')
        label = 'Compare Alignment';

    $('#playerList_header span').html(label);
}

function getSpinnerOptions(spinnerID){
    const isTop = spinnerID === J.HTML_IDS.TOP_SELECT;
    const isMid = spinnerID === J.HTML_IDS.MID_SELECT;
    const isBot = spinnerID === J.HTML_IDS.BOT_SELECT;

    function onCraftSelect(e, optIndex, oldVal, newVal){
        const vestChange = (optIndex === 1 && getAction() === 'craft')
            || (optIndex === 2 && isSelectedDigCommand('craft'));

        if(selected.size() || left || right)
            if(oldVal !== 'no' && newVal !== 'no'){
                sendAction();
            }else if(oldVal === 'no' && newVal !== 'no' && selected.size()){
                cancelAction();

                if(vestChange)
                    left = selected.get(0);
                else // if(optIndex === 0 && getAction() === 'craft'), or
                    right = selected.get(0);
            }else if(oldVal !== 'no' && newVal === 'no'){
                if(left && right){
                    let name;
                    if(vestChange)
                        name = left;
                    else
                        name = right;

                    left = right = null;
                    selected.add(name);
                    sendAction();
                }else if(right && !vestChange){
                    selected.add(right);
                    right = null;
                    sendAction();
                }else if(left && vestChange){
                    selected.add(left);
                    left = null;
                    sendAction();
                }else if(left && !vestChange){
                    setButtonSelected(left, J.TARGET_TYPE.UNCLICKED);
                    left = null;
                }else{
                    setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
                    right = null;
                }
            }
    }

    function onTailorSelect(e, optIndex, oldVal, newVal){
        const teamChange = (optIndex === 0 && getAction() === 'suit')
            || (optIndex === 1 && isSelectedDigCommand('suit'));
        let idToChange;
        if(optIndex)
            idToChange = J.HTML_IDS.BOT_SELECT;
        else
            idToChange = J.HTML_IDS.MID_SELECT;

        if(teamChange){
            savedOpts[optIndex] = newVal;
            const spinner = $(`.${idToChange}`);
            const options = getSpinnerOptions(idToChange).vals;
            appendPlayerSelectOptions(spinner, options);
        }
        if(selected.size() !== 0)
            sendAction();
    }

    function onDigCommandChange(e, optIndex, oldVal, newVal){
        savedOpts[optIndex] = newVal;
        $(`.${J.HTML_IDS.MID_SELECT}`).remove();
        $(`.${J.HTML_IDS.BOT_SELECT}`).remove();

        newVal = newVal.toLowerCase();
        oldVal = oldVal.toLowerCase();
        if(newVal === 'burn' || newVal === 'alert' || newVal === 'commute')
            sendAction();
        else if(oldVal === 'burn' || oldVal === 'alert' || oldVal === 'commute')
            cancelAction();


        let spinObject = getSpinnerOptions(J.HTML_IDS.MID_SELECT);
        let options = spinObject.vals;
        if(options && options.length){
            const spinner = getBaseSpinner(J.HTML_IDS.MID_SELECT);
            appendPlayerSelectOptions(spinner, options);
            savedOpts[1] = options[0].val;
            addOptionChangeListener(spinner, spinObject.spinFunc);
            $(`.${J.HTML_IDS.TOP_SELECT}`).after(spinner);
        }

        spinObject = getSpinnerOptions(J.HTML_IDS.BOT_SELECT);
        options = spinObject.vals;
        if(options && options.length){
            const spinner = getBaseSpinner(J.HTML_IDS.BOT_SELECT);
            appendPlayerSelectOptions(spinner, options);
            addOptionChangeListener(spinner, spinObject.spinFunc);
            $(`.${J.HTML_IDS.MID_SELECT}`).after(spinner);
        }


        if(selected.size() !== 0)
            sendAction();
    }

    const action = getAction();
    let spinFunc;

    if(action === 'craft' || (isSelectedDigCommand('craft') && !isTop))
        spinFunc = onCraftSelect;
    else if(action === 'suit' || (isSelectedDigCommand('suit') && !isTop))
        spinFunc = onTailorSelect;
    else if(visibleAbilityIs('dig') && isTop)
        spinFunc = onDigCommandChange;
    else
        spinFunc = sendAction;


    let options = [];
    if(isTop){
        const command = capitalizeFirstLetter(getAction());
        if(!window.gameState.playerLists[command]){
            options = [];
        }else if(command !== 'Dig' || deadSelected.size()){
            options = window.gameState.playerLists[command].options;
            if(options)
                options = Object.values(options);
            else
                options = [];
        }else{
            options = [];
        }
    }else if(isMid && savedOpts[0]){
        const command = capitalizeFirstLetter(getAction());
        if(!window.gameState.playerLists[command]){
            options = [];
        }else if(command !== 'Dig' || deadSelected.size()){
            options = window.gameState.playerLists[command].options;
            if(!options || $.isEmptyObject(options))
                options = [];

            if($.isEmptyObject(options))
                options = [];
            else
                options = Object.values(options[savedOpts[0]].map);
        }else{
            options = [];
        }
    }else if(isBot && savedOpts[0] && savedOpts[1]){
        const command = capitalizeFirstLetter(getAction());
        if(!window.gameState.playerLists[command]){
            options = [];
        }else if(command !== 'Dig' || deadSelected.size()){
            options = window.gameState.playerLists[command].options;
            if(!options || $.isEmptyObject(options))
                options = [];
            else if(options[savedOpts[0]])
                options = options[savedOpts[0]].map;
            else
                options = Object.values(options)[0].map;


            // should be mid options now

            if(!$.isEmptyObject(options) && options[savedOpts[1]])
                options = Object.values(options[savedOpts[1]].map);
            else
                options = [];
        }else{
            options = [];
        }
    }

    if(!options)
        options = [];

    options.sort((x, y) => {
        if(x.val === 'real' || y.val === 'no')
            return -1;
        if(x.val === 'no' || y.val === 'real')
            return 1;
        return 0;
    });

    return {
        vals: options,
        spinFunc,
        spinnerID,
    };
}

function optionExists(val, optSpinner){
    let valName;
    if(typeof(val) === 'string')
        valName = val;
    else
        valName = val.val;
    const qString = `.${optSpinner} option[value='${valName}']`;
    const element = $(qString);
    if(typeof(val) === 'string')
        return element.length === 1;
    return element.length && element.text() === (val.name || val.val);
}

function capitalizeFirstLetter(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function isSelectedDigCommand(command){
    if(typeof(command) === 'object')
        return command.filter(isSelectedDigCommand).length;
    if(!command || typeof(command) !== 'string')
        return false;
    command = command.toLowerCase();
    return command === ($(`.${J.HTML_IDS.TOP_SELECT}`).val() || savedOpts[0] || '').toLowerCase();
}

function getBaseSpinner(spinnerIdentifier){
    const spinner = $('<select>').addClass('playerList_wrapper');
    spinner.addClass('action_targetable_element');
    spinner.addClass(spinnerIdentifier);
    return spinner;
}

function refreshSpinners(){
    const selectables = $('#player_list');
    const gameState = window.gameState;
    if(gameState.isDay() || gameState.isOver() || !gameState.started || gameState.isObserver)
        return selectables.find('select').remove();

    const spinnerIDs = [J.HTML_IDS.TOP_SELECT, J.HTML_IDS.MID_SELECT, J.HTML_IDS.BOT_SELECT];

    for(let index = 0; index < spinnerIDs.length; index++){
        const spinObject = getSpinnerOptions(spinnerIDs[index]);
        if(!spinObject.vals.length){
            $(`.${spinObject.spinnerID}`).remove();
            continue;
        }

        let optsExistsAlready = true;
        spinObject.vals.forEach(value => {
            optsExistsAlready = optsExistsAlready && optionExists(value, spinObject.spinnerID);
        });

        if(optsExistsAlready)
            continue;


        $(`.${spinObject.spinnerID}`).remove();
        const spinner = getBaseSpinner(spinObject.spinnerID);

        const options = spinObject.vals;
        if(!savedOpts[index])
            savedOpts[index] = options[0].val || options[0];


        appendPlayerSelectOptions(spinner, options);
        addOptionChangeListener(spinner, spinObject.spinFunc);

        const selects = $('#player_list').find('select');
        if(selects.length)
            selects.last().after(spinner);
        else
            $('#player_list').prepend(spinner);


        if(!savedOpts[index])
            savedOpts[index] = spinner.val();
        else
            delayedSpinnerSet(spinObject.spinnerID, savedOpts[index]);
    }
}

function appendPlayerSelectOptions(spinner, options){
    spinner.empty();
    let option;
    for(let i = 0; i < options.length; i++){
        option = $('<option>').text(options[i].name || options[i].val);
        option.css('color', options[i].color || 'black');
        option.attr('name', options[i].name || options[i].val);
        option.val(options[i].val || options[i]);
        spinner.append(option);
    }
}

function addOptionChangeListener(spinner, funcToCall){
    spinner.on('change', function(e){
        e = $(this);
        const newVal = e.val();
        let optIndex;
        if(e.hasClass(J.HTML_IDS.TOP_SELECT))
            optIndex = 0;
        else if(e.hasClass(J.HTML_IDS.MID_SELECT))
            optIndex = 1;
        else
            optIndex = 2;

        const oldVal = savedOpts[optIndex];
        if(oldVal === newVal)
            return;
        funcToCall(e, optIndex, oldVal, newVal);

        savedOpts[optIndex] = newVal;
    });
}

let actionIndex = 0;
function breadPruning(bread){
    const playerLists = window.gameState.playerLists;

    let breadCount = window.gameState.role.breadCount;
    const color = window.gameState.role.roleColor;
    const rules = window.gameState.factions[color].rules;
    const knowsTeams = rules.indexOf('Knows the identity of teammates') !== -1;
    const isBaker = hasAbility('Baker');
    const teammateNames = (window.gameState.role.roleTeam || []).map(val => val.teamAllyName);

    let givingBreadToNonteammate = false;
    let submittedBreadActionPassed = false;

    window.gameState.actions.actionList.forEach((action, i) => {
        if(action.command.toLowerCase() === 'bread')
            if(teammateNames.indexOf(action.playerNames[0]) === -1){
                givingBreadToNonteammate = true;
            }else if(i === actionIndex){
                submittedBreadActionPassed = true;
            }else{
                breadCount--;
            }
    });

    const canGiveToTeammate = !isBaker || !knowsTeams || givingBreadToNonteammate
        || submittedBreadActionPassed || breadCount > 0;

    const visible = [];
    playerLists[bread].players.forEach(pObject => {
        if(canGiveToTeammate || !teammateNames.includes(pObject.playerName))
            visible.push(pObject);
    });

    return visible;
}

function refreshBreadButton(actionName){
    const actions = window.gameState.actions;
    let sends = 0;
    let i = 0;
    for(; actions && i < actions.actionList.length; i++){
        const action = actions.actionList[i];

        if(action.command === 'send')
            sends++;
    }
    // i = # of submitted actions
    i -= sends;

    const breadButton = $('#add_action');

    let showBreadButton = true;
    if(window.gameState.isOver())
        showBreadButton = false;
    else if(!window.gameState.playerLists[actionName])
        showBreadButton = false;
    else if(i === 0 || !window.gameState.playerLists[actionName].canAddAction)
        showBreadButton = false;
    else if(actionIndex === actions.actionList.length)
        showBreadButton = false;
    if(showBreadButton)
        breadButton.show();
    else
        breadButton.hide();
}

function isSplitClick(name, command){
    if(command === 'votesteal')
        return true;
    if(command === 'control')
        return true;
    if(command === 'bounty' && window.gameState.role.jokerKills)
        return true;
    if(command === 'craft')
        return true;
    if(command === 'charge' && window.gameState.role.electroExtraCharge && actionIndex === 0)
        return true;
    if(command === 'dig'){
        if(!isAlive(name) || !savedOpts[0])
            return false;
        const opt = savedOpts[0].toLowerCase();
        if(opt === 'control' || opt === 'craft' || opt === 'charge')
            return true;
    }
    if(command === 'vote'){
        if(window.gameState.isHost())
            return true;
        if(name === 'Skip Day')
            return false;
        return window.gameState.getRuleValue('HOST_VOTING');
    }

    return false;
}

function refreshPlayers(){
    if(!window.gameState.isStarted)
        return;
    const gameState = window.gameState;
    const playerLists = gameState.playerLists;
    const playerListType = playerLists.type[gameState.commandsIndex % playerLists.type.length]
        || '';

    refreshBreadButton(playerListType);

    window.gameState.visiblePlayers = [];
    if(window.gameState.isOver()){
        window.gameState.visiblePlayers = Object.values(window.gameState.playerMap);
    }else if(playerListType.toLowerCase() === 'bread'){
        window.gameState.visiblePlayers = breadPruning(playerListType);
    }else if(playerListType.toLowerCase() === 'spyability'){
        const opts = Object.values(window.gameState.playerLists[playerListType].options);
        for(let i = 0; opts && i < opts.length; i++)
            window.gameState.visiblePlayers.push({
                playerName: opts[i].name,
            });
    }else if(playerListType && !window.gameState.endedNight()){
        const players = playerLists[playerListType].players;
        for(let i = 0; players && i < players.length; i++)
            gameState.visiblePlayers.push(players[i]);
    }else if(window.gameState.endedNight()){
        const someoneEndedNight = Object.values(window.gameState.playerMap)
            .some(player => player.endedNight && player.name !== window.gameState.profile.name);

        let players = Object.values(window.gameState.playerMap)
            .filter(player => player.name !== window.gameState.profile.name);
        if(someoneEndedNight)
            players = players.filter(player => !player.endedNight);
        else
            players = players.filter(player => !player.flip);
        window.gameState.visiblePlayers = players;
    }else if(window.gameState.profile.isJailed){
        window.gameState.visiblePlayers = [];
    }else{
        window.gameState.visiblePlayers = Object.values(window.gameState.playerMap)
            .filter(player => !player.flip);
    }

    const abilityName = playerListType.toLowerCase();
    const isDay = window.gameState.isDay();
    const isVoteAction = abilityName.endsWith('vote') || abilityName === 'controlled voting';
    const isNotObserving = !gameState.isObserver;
    if(isDay && gameState.getRuleValue('skip_Vote') && isNotObserving && isVoteAction){
        let skipVote;

        if(window.gameState.isSkipping){
            skipVote = [0];
        }else if(playerListType.toLowerCase().endsWith(' vote')){
            const puppet = playerListType.toLowerCase().replace(' vote', '');
            if(isVoting(puppet, 'Skip Day'))
                skipVote = [0];
            else
                skipVote = [];
        }else{
            skipVote = [];
        }
        window.gameState.visiblePlayers.unshift({
            playerName: 'Skip Day',
            playerActive: false,
            playerVote: window.gameState.skipVote,
            playerSelectedColumn: skipVote,
        });
    }

    // getAction will now work AFTER the commands label is called.
    if(window.gameState.isOver())
        setCommandsLabel('Game Over');
    else if(window.gameState.profile.isJailed)
        setCommandsLabel('You are jailed!');
    else if(!abilityName)
        setCommandsLabel('The Living');
    else if(abilityName === 'controlled voting')
        if(window.gameState.isHost()){
            setCommandsLabel('Vote');
        }else
            setCommandsLabel(`${window.gameState.host.name} controls voting`);
    else if(!window.gameState.endedNight())
        setCommandsLabel(playerListType);
    else if(window.gameState.endedNight())
        setCommandsLabel('Waiting on');
    else
        setCommandsLabel('Waiting for day start.');

    const command = getAction();

    currentAction = null;
    if(gameState.actions){
        const actionList = window.gameState.actions.actionList;
        const size = actionList.length;
        const lastAction = actionList[size - 1];
        if(command === 'send' && lastAction && lastAction.command.toLowerCase() === 'send')
            currentAction = actionList[size - 1];
        else if(actionIndex < actionList.length)
            currentAction = actionList[actionIndex];
    }

    const togglable = gameState.visiblePlayers.length === 0
        && command !== 'currently sleeping'
        && command !== 'you are jailed!'
        && !gameState.endedNight();
    if(togglable){
        let name1;
        let name2;
        let activated;

        if(command === 'recruitment'){
            name1 = 'Confirm';
            name2 = 'Decline';
            activated = window.gameState.role.acceptingRecruitment;
        }else{
            activated = currentAction;
            name1 = getOnText(command);
            name2 = getOffText(command);
        }

        if(activated){
            gameState.visiblePlayers.push({
                playerName: name1,
                playerActive: true,
            });
            gameState.visiblePlayers.push({
                playerName: name2,
                playerActive: true,
            });
        }else{
            gameState.visiblePlayers.push({
                playerName: name1,
                playerActive: true,
            });
            gameState.visiblePlayers.push({
                playerName: name2,
                playerActive: true,
            });
        }
    }


    const selectables = $('#player_list');

    if(!selected && gameState.started){
        selected = new ArrayList();
        selected2 = new ArrayList();
        deadSelected = new ArrayList();
        savedOpts = [null, null, null];
    }
    if(currentAction){
        if(currentAction.option)
            savedOpts[0] = currentAction.option;
        if(currentAction.option2)
            savedOpts[1] = currentAction.option2;
        if(currentAction.option3)
            savedOpts[2] = currentAction.option3;
    }

    selectables.find('li').remove();

    refreshSpinners();

    for(let i = 0; i < gameState.visiblePlayers.length; i++){
        const player = gameState.visiblePlayers[i];
        const playerName = player.playerName || player.name;
        let text = playerName;

        const dontShowVoteTexts = new Set(['recruitment', 'burn', 'reveal', 'marshalllaw']);
        if(gameState.isDay() && !dontShowVoteTexts.has(command)
            && !window.gameState.getRuleValue('HOST_VOTING'))
            text += (` (${player.playerVote})`);

        const isComputer = player.isComputer;
        const isSplit = isSplitClick(playerName, command);
        const hoverable = !window.gameState.isOver() && abilityName;
        const targetable = new Targetable({
            name: playerName,
            isComputer,
            userID: player.userID,
        }, text, isSplit, hoverable);
        playerView.saveTargetable(targetable);
        if(!gameState.endedNight() && !gameState.isOver())
            if(gameState.isAlive === true
                || (hasAbility('Ghost') && command !== 'vote')
                || (gameState.host.name === gameState.role.displayName
                    && window.gameState.getRuleValue('HOST_VOTING')
                    && gameState.isDay())){ // === true is because it could be equal to 'Ghost'
                if(command.indexOf('controls voting') === -1 && (command !== 'vote'
                    || playerName !== gameState.profile.name
                    || window.gameState.getRuleValue('HOST_VOTING')
                    || window.gameState.getRuleValue('SELF_VOTE'))){
                    const classesToFind = '.right_targetable_click, .left_targetable_click';
                    const element = targetable.elm.find(classesToFind);
                    element.click(targetableClick);
                }
            }


        // for the not allowing a live click
        if(command !== 'dig' && command !== 'murder')
            targetable.makeVisible(selectables);
        else if(currentAction)
            targetable.makeVisible(selectables);
        else if(command === 'dig' && (deadSelected.size() || !isAlive(playerName)))
            targetable.makeVisible(selectables);
        else if(command === 'murder')
            if(left)
                targetable.makeVisible(selectables);
            else{
                let showMurders = false;
                if(gameState.role && gameState.role.roleTeam)
                    gameState.role.roleTeam.forEach(val => {
                        if(playerName === val.teamAllyName)
                            showMurders = true;
                    });

                if(playerName === gameState.role.displayName)
                    showMurders = true;
                if(showMurders)
                    targetable.makeVisible(selectables);
            }
    }

    const actionCopy = copyAction(currentAction);

    if(actionCopy && getAction() === actionCopy.command.toLowerCase()){
        if(command === 'jail' && gameState.role.jailCleanings)
            colorJailExecution(actionCopy);
        else if(command === 'dig')
            colorDigList(actionCopy);
        else if(command === 'craft')
            colorCraftList(actionCopy);
        else if(command === 'charge' || command === 'control' || command === 'murder'
            || command === 'votesteal')
            colorChargeList(currentAction);
        else if(isOnOffCommand(command))
            colorOnOffList(actionCopy);
        else if(command === 'bounty')
            colorBountyList(actionCopy);
        else
            colorSingle(actionCopy);
    }else if(command.toLowerCase().endsWith('vote')){
        colorVotes();
    }else if(getAction() === 'recruitment'){
        selected.clear();
        if(gameState.role.acceptingRecruitment){
            selected.add('confirm');
            setButtonSelected('Confirm', J.TARGET_TYPE.FULLY_CLICKED);
        }else{
            selected.add('decline');
            setButtonSelected('Decline', J.TARGET_TYPE.FULLY_CLICKED);
        }
    }else if(command === 'dig'){
        colorDigList();
    }else if(left === right && left){
        setButtonSelected(left, J.TARGET_TYPE.WITCH_SELECTED);
    }else if(left || right){
        if(left)
            setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
        if(right)
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
    }else if(deadSelected && deadSelected.size()){
        setButtonSelected(deadSelected.get(0), J.TARGET_TYPE.FULLY_CLICKED);
    }else if(selected && selected.size()){
        setButtonSelected(selected.get(0), J.TARGET_TYPE.FULLY_CLICKED);
    }

    if(isOnOffCommand(command))
        colorOnOffList(actionCopy);

    adjustActionPane();

    // new SimpleBar(selectables[0]);
}

function copyAction(cAction){
    if(!cAction)
        return cAction;
    const ret = {};
    ret.command = cAction.command;
    ret.option = cAction.option;
    ret.option2 = cAction.option2;
    ret.option3 = cAction.option3;
    ret.text = cAction.text;
    ret.playerNames = [];
    for(let i = 0; i < cAction.playerNames.length; i++)
        ret.playerNames[i] = cAction.playerNames[i];

    return ret;
}

const onOffCommands = new Set(['burn', 'vest', 'alert', 'reveal', 'commute', 'marshalllaw']);
function isOnOffCommand(command){
    return onOffCommands.has(command);
}

function getOnText(command){
    if(command === 'vest')
        return 'Use Vest';
    if(command === 'burn')
        return 'Burn';
    if(command === 'alert')
        return 'Guard house';
    if(command === 'reveal')
        return `Reveal as ${window.gameState.role.roleName}`;
    if(command === 'marshalllaw')
        return `Reveal as ${window.gameState.role.roleName}`;
    if(command === 'commute')
        return 'Leave Town';
}

function getOffText(command){
    if(command === 'vest')
        return 'Save Vest';
    if(command === 'burn')
        return "Don't Burn";
    if(command === 'alert')
        return "Don't kill visitors";
    if(command === 'reveal')
        return 'Stay hidden';
    if(command === 'marshalllaw')
        return 'Stay hidden';
    if(command === 'commute')
        return 'Stay in Town';
}

function colorVotes(){
    if(!window.gameState.voteInfo.currentVotes)
        return;
    let name;
    if(getAction() === 'vote')
        name = window.gameState.profile.name;
    else
        name = $('#playerList_header span').text().replace(' Vote', '');
    const myVote = window.gameState.voteInfo.currentVotes[name];

    if(myVote)
        setButtonSelected(myVote, J.TARGET_TYPE.FULLY_CLICKED);
}

function colorJailExecution(cAction){
    const rights = parseInt(cAction.option || 0, 10);
    selected.clear();
    selected2.clear();
    let name;
    for(let i = 0; i < cAction.playerNames.length; i++){
        name = cAction.playerNames[i];
        if(i < rights){
            selected2.add(name);
            setButtonSelected(name, J.TARGET_TYPE.RIGHT_SELECTED);
        }else{
            selected.add(name);
            setButtonSelected(name, J.TARGET_TYPE.LEFT_SELECTED);
        }
    }
}

function colorBountyList(cAction){
    if(!window.gameState.role.jokerKills)
        return colorSingle(cAction);
    selected.clear();
    let killStart = 0;

    if(!cAction.option){
        left = cAction.playerNames[0];
        setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
        killStart++;
    }

    cAction.playerNames.forEach((val, i) => {
        if(i < killStart)
            return;
        selected.add(val);
        setButtonSelected(val, J.TARGET_TYPE.RIGHT_SELECTED);
    });
}

function colorCraftList(cAction){
    if(cAction){
        const targets = cAction.playerNames;
        if(targets.length === 1){
            colorSingle(cAction);
        }else{
            left = targets[0];
            right = targets[1];
            setButtonSelected(targets[0], J.TARGET_TYPE.LEFT_SELECTED);
            setButtonSelected(targets[1], J.TARGET_TYPE.RIGHT_SELECTED);
        }

        if(cAction.option3){
            delayedSpinnerSet(J.HTML_IDS.TOP_SELECT, cAction.option);
            delayedSpinnerSet(J.HTML_IDS.MID_SELECT, cAction.option2);
            delayedSpinnerSet(J.HTML_IDS.BOT_SELECT, cAction.option3);
        }else{
            delayedSpinnerSet(J.HTML_IDS.TOP_SELECT, cAction.option);
            delayedSpinnerSet(J.HTML_IDS.MID_SELECT, cAction.option2);
        }
    }else if(left){
        setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
    }else if(right){
        setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
    }
}

function colorDigList(cAction){
    if(cAction){
        const targets = cAction.playerNames;
        deadSelected.add(targets[0], UNIQUE);
        setButtonSelected(targets[0], J.TARGET_TYPE.FULLY_CLICKED);

        savedOpts[0] = cAction.option;
        savedOpts[1] = cAction.option2;
        savedOpts[2] = cAction.option3;
        refreshSpinners();

        delayedSpinnerSet(J.HTML_IDS.TOP_SELECT, cAction.option);
        delayedSpinnerSet(J.HTML_IDS.MID_SELECT, cAction.option2);
        delayedSpinnerSet(J.HTML_IDS.BOT_SELECT, cAction.option3);

        targets.splice(0, 1);
        if(isSelectedDigCommand('craft') || savedOpts[0] === 'Craft'){
            colorCraftList(cAction);
            return;
        }if(isSelectedDigCommand('control') || savedOpts[0] === 'control'
            || isSelectedDigCommand('murder') || savedOpts[0] === 'murder')
            colorChargeList(cAction);
        else
            colorSingle(cAction);


        targets.splice(0, 0, deadSelected.get(0));
    }

    setButtonSelected(deadSelected.get(0), J.TARGET_TYPE.FULLY_CLICKED);

    if(isSelectedDigCommand('craft'))
        colorCraftList();
    else
        colorSingle();

    // should just follow some other color protocol

    // if(left)
    //  setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
    // if(right)
    //  setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
    // if(deadSelected && deadSelected.size()){
    //  setButtonSelected(deadSelected.get(0), J.TARGET_TYPE.FULLY_CLICKED);
    // }
    // $.each(selected.backer, function(index, value){
    //  setButtonSelected(value, J.TARGET_TYPE.FULLY_CLICKED);
    // });
}

function delayedSpinnerSet(spinnerID, val){
    if(val)
        setTimeout(() => {
            if($(`.${spinnerID}`).val() !== val)
                $(`.${spinnerID}`).val(val);
        }, 100);
}

// don't edit currentAction, this is NOT a copy! (if its coming from electro)
// could also come from witch
function colorChargeList(action){
    if(action){
        const targets = action.playerNames;
        const role = window.gameState.role;
        const actions = window.gameState.actions;
        if(targets.length === 1){
            if(role.electroExtraCharge && actions.actionList[0] !== action){
                selected.add(targets[0]);
                setButtonSelected(targets[0], J.TARGET_TYPE.FULLY_CLICKED);
            }else if(!right){
                left = targets[0];
                setButtonSelected(targets[0], J.TARGET_TYPE.LEFT_SELECTED);
            }else{
                right = targets[0];
                setButtonSelected(targets[0], J.TARGET_TYPE.RIGHT_SELECTED);
            }
        }else if(targets[0] === targets[1]){
            left = right = targets[0];
            setButtonSelected(targets[0], J.TARGET_TYPE.WITCH_SELECTED);
        }else{
            left = targets[0];
            right = targets[1];
            setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
        }
    }else if(getAction() === 'control'){
        if(left)
            setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
        else if(right)
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
    }
}

function colorOnOffList(cAction){
    const action = getAction();
    let text;

    if(cAction)
        text = getOnText(action);
    else
        text = getOffText(action);

    selected.clear();
    selected.add(text);
    setButtonSelected(text, J.TARGET_TYPE.FULLY_CLICKED);
}

function colorSingle(cAction){
    let targets;
    let opt1;
    let opt2;
    if(cAction){
        targets = cAction.playerNames;
        opt1 = cAction.option;
        opt2 = cAction.option2;
    }else if(selected){
        targets = selected.backer;
        opt1 = savedOpts[0];
        opt2 = savedOpts[1];
    }else{
        selected = new ArrayList();
    }
    targets.forEach(target => {
        selected.add(target, UNIQUE);
        setButtonSelected(target, J.TARGET_TYPE.FULLY_CLICKED);
    });

    if(opt1){
        savedOpts[0] = opt1;
        delayedSpinnerSet(J.HTML_IDS.TOP_SELECT, opt1);
    }
    if(opt2){
        savedOpts[1] = opt2;
        delayedSpinnerSet(J.HTML_IDS.MID_SELECT, opt2);
    }
}

function targetableClick(){
    const e = $(this);
    const name = e.attr('name');
    resolveClick(name, e.hasClass('right_targetable_click'));
}


function singleTargetAction(){
    const command = getAction();
    if(command === 'control')
        return false;
    if(command === 'votesteal')
        return false;
    if(command === 'murder')
        return false;
    if(command === 'bounty')
        return false;
    if(command === 'craft')
        return false;
    if(command === 'dig')
        return false;
    if(command !== 'charge')
        return true;

    // handling electrocutioner initial charge


    // at this point the command is 'charge'
    const actions = window.gameState.actions;
    if(!actions)
        return true;
    let action;
    for(let i = 0; i < actions.length; i++){
        action = actions[i];
        if(actions.playerNames > 1 && command === action.command)
            return i === actionIndex;
    }


    return !window.gameState.role.electroExtraCharge;
}

function resolveClick(name, rclick){
    if(window.gameState.endedNight() || !window.gameState.started)
        return;
    const command = getAction();

    if(command === 'send')
        return handleSendClick(name);
    if(command === 'vote' && window.gameState.getRuleValue('HOST_VOTING'))
        return handleHostVoteClick(name, rclick);
    if(command === 'jail' && !window.gameState.isDay())
        if(window.gameState.role.jailCleanings){
            handleCleanedExecutionClick(name, rclick);
        }else
            handleSingleClick(name, 1, window.gameState.visiblePlayers.length);
    else if(singleTargetAction())
        if(command !== 'swap' && command !== 'build' && command !== 'switch'){
            handleSingleClick(name);
        }else{ // if swap
            if(selected.contains(name)){
                if(selected.size() === 2)
                    cancelAction();

                selected.remove(name);
                setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
                return;
            }if(selected.size() === 2){
                setButtonSelected(selected.get(1), J.TARGET_TYPE.UNCLICKED);
                selected.backer[1] = name;
                setButtonSelected(name, J.TARGET_TYPE.FULLY_CLICKED);
            }else{
                selected.add(name);
                setButtonSelected(name, J.TARGET_TYPE.FULLY_CLICKED);
            }
            if(selected.size() === 2)
                sendAction();
        }
    else if(command === 'charge')
        handleDoubleElectroCharge(name, rclick);
    else if(command === 'dig')
        handleDigClick(name, rclick);
    else if(command === 'craft')
        handleCraftClick(name, rclick);
    // these are now split acitons.
    else if(command === 'control')
        handleControlClick(name, rclick);
    else if(command === 'murder')
        handleMurderClick(name, rclick);
    else if(command === 'bounty')
        handleBountyClick(name, rclick);
    else
        handleSplitClick(name, rclick);
}

function handleBountyClick(name, rclick){
    if(!window.gameState.role.jokerKills)
        return handleSingleClick(name);

    if(!rclick){ // bounty submission
        if(left === name){
            left = null;
            if(selected.contains(name))
                setButtonSelected(name, J.TARGET_TYPE.RIGHT_SELECTED);
            else
                setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
        }else if(left){
            if(selected.contains(left))
                setButtonSelected(left, J.TARGET_TYPE.RIGHT_SELECTED);
            else
                setButtonSelected(left, J.TARGET_TYPE.UNCLICKED);
            left = name;
            if(selected.contains(name))
                selected.remove(name);
            setButtonSelected(name, J.TARGET_TYPE.LEFT_SELECTED);
        }else{
            left = name;
            if(selected.contains(name))
                selected.remove(name);
            setButtonSelected(name, J.TARGET_TYPE.LEFT_SELECTED);
        }
    }else{ // kill submission
        const jKills = window.gameState.role.jokerKills;
        if(selected.size() === jKills && !selected.contains(name)){
            const toRemove = selected.removeLast();
            setButtonSelected(toRemove, J.TARGET_TYPE.UNCLICKED);

            if(left === name)
                left = null;
            setButtonSelected(name, J.TARGET_TYPE.RIGHT_SELECTED);
            selected.add(name);
        }else if(selected.contains(name)){
            if(left === name)
                setButtonSelected(name, J.TARGET_TYPE.LEFT_SELECTED);
            else
                setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
            selected.remove(name);
        }else{
            if(left === name)
                left = null;
            setButtonSelected(name, J.TARGET_TYPE.RIGHT_SELECTED);
            selected.add(name);
        }
    }
    if(left || selected.size())
        sendAction();
    else
        cancelAction();
}

function handleHostVoteClick(name, rclick){
    if(name === 'Skip Day'){
        if(!left && selected.isEmpty()){
            left = 'Skip Day';
            window.gameState.playerLists['Controlled Voting'].players.forEach(val => {
                selected.add(val.playerName);
            });
            submitHostVote();
        }else{
            if(left){
                setButtonSelected(left, J.TARGET_TYPE.UNCLICKED);
                left = null;
            }
            selected.backer.forEach(val => {
                setButtonSelected(val, J.TARGET_TYPE.UNCLICKED);
            });
            selected.clear();
        }
        return;
    }
    if(rclick && (!left || left === name))
        rclick = false;
    if(!rclick){ // leftClick
        selected.backer.forEach(sName => {
            setButtonSelected(sName, J.TARGET_TYPE.UNCLICKED);
        });
        selected.clear();

        if(left){
            if(left === name){ // unclick
                left = null;
                setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
            }else{ // change click
                setButtonSelected(left, J.TARGET_TYPE.UNCLICKED);
                left = name;
                setButtonSelected(name, J.TARGET_TYPE.LEFT_SELECTED);
            }
        }else{
            left = name;
            setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
        }
    }else{ // right click
        if(!left)
            return;
        if(left === name)
            return;
        if(selected.contains(name)){
            selected.remove(name);
            setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
        }else{
            selected.add(name);
            setButtonSelected(name, J.TARGET_TYPE.RIGHT_SELECTED);
        }
    }

    refreshActionButtonText(left !== null && selected.size());
    adjustActionPane();
}

function handleCleanedExecutionClick(name, rclick){
    let list;
    let color;
    let otherList;
    if(rclick){
        list = selected2;
        otherList = selected;
        color = J.TARGET_TYPE.RIGHT_SELECTED;
        if(!selected2.contains(name) && selected2.size() >= window.gameState.role.jailCleanings){
            const last = selected2.removeLast();
            setButtonSelected(last, J.TARGET_TYPE.UNCLICKED);
        }
    }else{
        list = selected;
        otherList = selected2;
        color = J.TARGET_TYPE.LEFT_SELECTED;
    }
    if(list.contains(name)){
        list.remove(name);
        setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
    }else{
        list.add(name);
        if(otherList.contains(name))
            otherList.remove(name);
        setButtonSelected(name, color);
    }

    if(list.size() || otherList.size())
        sendAction();
    else
        cancelAction();
}

function handleControlClick(name, rclick){
    if(rclick || name !== window.gameState.profile.name)
        handleSplitClick(name, rclick);
}

function handleMurderClick(name, rclick){
    if(rclick)
        handleSplitClick(name, rclick);


    let doClick = false;
    if(window.gameState.role && window.gameState.role.roleTeam)
        window.gameState.role.roleTeam(val => {
            if(name === val.teamAllyName || name === window.gameState.role.displayName)
                doClick = true;
        });

    if(doClick){
        handleSplitClick(name, rclick);
        refreshPlayers();
    }
}

function handleSendClick(name){
    const maxSize = 1; // window.gameState.role.maxSends || 1;  NEED TO BE ADDRESSED
    if(selected.contains(name))
        selected.remove(name);
    else if(selected.size() === maxSize)
        selected.backer[maxSize - 1] = name;
    else
        selected.add(name);


    if(selected.size())
        sendAction();
    else
        cancelAction();
}


function handleCraftClick(name, rclick){
    let gOpt;
    let vOpt;
    if(getAction() === 'dig'){
        gOpt = $(`.${J.HTML_IDS.MID_SELECT}`).val();
        vOpt = $(`.${J.HTML_IDS.BOT_SELECT}`).val();
    }else{
        gOpt = $(`.${J.HTML_IDS.TOP_SELECT}`).val();
        vOpt = $(`.${J.HTML_IDS.MID_SELECT}`).val();
    }

    if(gOpt === 'no' || vOpt === 'no'){
        if(gOpt !== vOpt)
            handleSingleClick(name);
    }else if((name === left && rclick) || (name === right && !rclick)){
        const hadAction = left && right;
        setButtonSelected(left, J.TARGET_TYPE.UNCLICKED);
        setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
        right = left = null;
        if(rclick)
            right = name;
        else
            left = name;

        if(hadAction){
            cancelAction(); // safe while canceling actions dont trigger player refresh
        }else{
            setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
        }
    }else{
        handleSplitClick(name, rclick);
    }
}

function handleDriverClick(name){
    handleSingleClick(name, 2);
}

function handleSingleClick(name, needed, limit){
    if(!needed)
        needed = 1;
    if(!limit)
        limit = needed;
    if(window.gameState.actions.actionList.length)
        currentAction = window.gameState.actions.actionList[actionIndex];

    let submitting;
    const command = getAction();
    if(isOnOffCommand(command)){
        if(name === selected.get(0))
            return;
        selected.clear();
        selected.add(name);
        if(name === getOnText(command))
            sendAction();
        else
            cancelAction();

        return;
    }
    if(!command.endsWith('vote'))
        if(selected.contains(name))
            submitting = selected.size() > needed;
        else
            submitting = selected.size() + 1 >= needed;

    // submitting = !selected.contains(name) || selected.size > needed;//!(currentAction && currentAction.playerNames.indexOf(name) !== -1 && currentAction.command.toLowerCase() === command);
    else
        submitting = !selected.contains(name);

    if(submitting){
        if(selected.size() === limit)
            selected.remove(selected.get(limit - 1));
        selected.add(name);
        sendAction();
    }else if(selected.contains(name)){
        selected.remove(name);
        if(selected.size() + 1 >= needed)
            cancelAction();
        else
            setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
    }else{
        selected.add(name);
        setButtonSelected(name, J.TARGET_TYPE.FULLY_CLICKED);
    }
}

function handleSplitClick(name, rclick){
    if(rclick){
        if(right && left){
            if(right === name){
                right = null;
                cancelAction();
            }else{
                right = name;
                sendAction();
            }
        }else if(left){
            right = name;
            sendAction();
        }else if(right === name){
            setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
            right = null;
        }else if(right){
            setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
            right = name;
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
        }else{
            right = name;
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
        }
    }else
    if(left && right){
        if(left === name){
            left = null;
            cancelAction();
        }else{
            left = name;
            sendAction();
        }
    }else if(right){
        left = name;
        sendAction();
    }else if(left === name){
        setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
        left = null;
    }else if(left){
        setButtonSelected(left, J.TARGET_TYPE.UNCLICKED);
        left = name;
        setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
    }else{
        left = name;
        setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
    }
}

function handleDoubleElectroCharge(name, rclick){
    if(window.gameState.actions)
        if(actionIndex < window.gameState.actions.actionList.length)
            currentAction = window.gameState.actions.actionList[actionIndex];

    let actionHandled = false;
    window.gameState.actions.actionList.forEach((action, index) => {
        if(action.command === 'charge' && action.playerNames.length === 2 && index !== 0)
            actionHandled = true;
    });
    if(actionHandled)
        return handleSingleClick(name);

    if(rclick){
        if(right && left){
            if(right === name){
                right = null;
                sendAction();
            }else{
                right = name;
                sendAction();
            }
        }else if(left){
            right = name;
            sendAction();
        }else if(right !== name){
            right = name;
            sendAction();
        }else{
            right = null;
            cancelAction();
        }
    }else
    if(left && right){
        if(left === name){
            left = null;
            sendAction();
        }else{
            left = name;
            sendAction();
        }
    }else if(right){
        left = name;
        sendAction();
    }else if(left !== name){
        left = name;
        sendAction();
    }else{
        left = null;
        cancelAction();
    }
}

function getDeadPerson(deadName){
    let deadPerson;
    for(let i = 0; i < window.gameState.graveyard.length; i++){
        deadPerson = window.gameState.graveyard[i];
        if(deadPerson.name === deadName)
            return deadPerson;
    }
    return null;
}

const deadNoTargetActions = new Set(['burn', 'alert', 'commute']);
function handleDigClick(name, rclick){
    function clearLiveSelection(){
        if(right)
            setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
        if(left)
            setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
        for(let i = 0; i < selected.size(); i++)
            setButtonSelected(selected.get(i), J.TARGET_TYPE.UNCLICKED);

        selected.clear();
        left = right = null;
    }

    let deadPerson = getDeadPerson(name);


    if(deadPerson){ // was clicked
        if(deadSelected.size() === 0){
            deadSelected.add(name);

            refreshPlayers();
            let selectedAction = savedOpts[0];
            if(selectedAction)
                selectedAction = selectedAction.toLowerCase();

            if(deadNoTargetActions.has(selectedAction))
                sendAction();
        }else{
            if(deadSelected.contains(name))
                deadSelected.clear();
            else
                deadSelected.backer[0] = name;

            clearLiveSelection();
            if(window.gameState.actions.actionList.length - 1 === actionIndex)
                cancelAction();
            else
                refreshPlayers();
        }
    }else{ // live person was clicked
        if(deadSelected.size() === 0) // this should never happen now
            return;
        deadPerson = getDeadPerson(deadSelected.get(0));

        if(isSelectedDigCommand('craft')){
            handleCraftClick(name, rclick);
        }else if(isSelectedDigCommand('control')){
            handleSplitClick(name, rclick);
        }else if(isSelectedDigCommand('swap') || isSelectedDigCommand('switch')){
            handleDriverClick(name);
        }else if(isSelectedDigCommand('burn', 'alert', 'commute')){
            if(!selected.contains(name)){
                selected.add(name);
                sendAction();
            }else{
                selected.remove(name);
                cancelAction();
            }
        }else if(selected.size() === 0){ // all other roles
            selected.add(name);
            setButtonSelected(name, J.TARGET_TYPE.FULLY_CLICKED);
            sendAction();
        }else if(selected.contains(name)){
            selected.clear();
            setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
            cancelAction();
        }else{
            setButtonSelected(selected.get(0), J.TARGET_TYPE.UNCLICKED);
            setButtonSelected(name, J.TARGET_TYPE.FULLY_CLICKED);
            selected.backer[0] = name;
            sendAction();
        }
        // targeting logic except for witch
    } // end live clicked person
}

function handlePuppetVote(submitAction, name){
    let puppet = getAction();
    puppet = puppet.toLowerCase().replace(' vote', '');
    let message;

    if(!submitAction)
        message = `vunvote ${puppet}`;
    else if(name === 'Skip Day')
        message = `vskipday ${puppet}`;
    else
        message = `vvote ${puppet} ${name}`;


    webSend({
        message,
    });
}

function handlePuppetPunch(name){
    let puppet = getAction();
    puppet = puppet.toLowerCase().replace(' punch', '');
    const message = `vpunch ${puppet} ${name}`;
    webSend({
        message,
    });
}

function cancelAction(){
    submitActionToServer(false);
}

function sendAction(){
    submitActionToServer(true);
}

function submitHostVote(){
    const message = {
        command: 'vote',
        'skip day': left === 'Skip Day' || selected.get(0) === 'Skip Day',
        unvote: false,
        targets: selected.backer,
    };
    if(!message['skip day'])
        message.targets.unshift(left);
    webSend(message);
}

const noTargetCommands = new Set(['burn', 'alert', 'vest', 'marshalllaw', 'reveal', 'commute']);
const doubleTargetCommands = new Set(['swap', 'switch', 'build']);
const optionCommands = new Set(['frame', 'drug', 'gun', 'armor']);
function submitActionToServer(submitAction){
    let message;
    let command = getAction();
    if(command === 'vote'){
        if(!submitAction)
            message = 'unvote';
        else if(selected.get(0) === 'Skip Day')
            message = 'skip day';
        else
            message = `vote ${selected.get(0)}`;
    }else if(command.toLowerCase().endsWith(' vote')){
        return handlePuppetVote(submitAction, selected.get(0));
    }else if(command.toLowerCase().endsWith(' punch')){
        return handlePuppetPunch(selected.get(0));
    }else if(command === 'recruitment'){
        message = selected.get(0).toLowerCase();
    }else if(command === 'assassinate'){
        message = `${command} ${selected.get(0)}`;
    }else if(command === 'shoot' && window.gameState.isDay()){
        message = `${command} ${selected.get(0)}`;
    }else if(noTargetCommands.has(command)){
        submitAction = selected.get(0) === window.gameState.visiblePlayers[0].playerName;
        if(submitAction)
            message = command;
    }

    if(typeof(message) === 'string')
        return actionService.submit(message).then(refreshPlayers);

    const newAction = {
        command,
    };

    if(!submitAction){
        newAction.message = 'cancelAction';
        if(command === 'send')
            command = window.gameState.actions.actionList.length - 1;
        else
            newAction.oldAction = actionIndex;
        if(!doubleTargetCommands.has(command) && singleTargetAction()){
            if(command !== 'send')
                actionIndex = Math.max(0, actionIndex - 1);
            selected.clear();
        }
        return actionService.cancel(actionIndex, command);
    }

    const topSpinner = `.${J.HTML_IDS.TOP_SELECT}`;
    const midSpinner = `.${J.HTML_IDS.MID_SELECT}`;
    const botSpinner = `.${J.HTML_IDS.BOT_SELECT}`;

    if(optionCommands.has(command)
        || (command === 'convert' && window.gameState.getRuleValue('CULT_PROMOTION'))){
        newAction.option = $(topSpinner).val();
        newAction.targets = [selected.get(0)];
    }else if(command === 'bounty'){
        if(window.gameState.role.jokerKills)
            if(!left){
                newAction.targets = selected.backer;
                newAction.option = 'kill';
            }else{
                newAction.targets = [left];
                selected.backer.forEach(newAction.targets.push);
            }
        else
            newAction.targets = selected.backer;
    }else if(command === 'spyability'){
        newAction.option = selected.get(0);
        newAction.targets = [];
    }else if(command === 'suit'){
        newAction.targets = [selected.get(0)];
        newAction.option = $(topSpinner).val();
        newAction.option2 = $(midSpinner).val();
    }else if(command === 'craft'){
        newAction.option = $(topSpinner).val().replace(' gun', '');
        newAction.option2 = $(midSpinner).val();
        if(left || right)
            newAction.targets = [left, right];
        else
            newAction.targets = selected.backer;
    }else if(command === 'dig'){
        if(deadSelected.size() < 1)
            return;
        newAction.targets = [deadSelected.get(0)];

        newAction.option = $(topSpinner).val() || null;
        newAction.option2 = $(midSpinner).val() || null;
        newAction.option3 = $(botSpinner).val() || null;

        if(isSelectedDigCommand('control') || (isSelectedDigCommand('craft') && (left || right))){
            if(!left || !right)
                return;
            newAction.targets.push(left);
            newAction.targets.push(right);
        }else if(newAction.option === 'Burn'
            || newAction.option === 'Commute'
            || newAction.option === 'Alert'){
            // no work needed
        }else{
            if(selected.size() < 1)
                return;
            for(let i = 0; i < selected.size(); i++)
                newAction.targets.push(selected.get(i));
        }
    }else if(command === 'jail' && window.gameState.role.jailCleanings && submitAction){
        newAction.targets = selected2.backer.concat(selected.backer);
        if(selected2.size())
            newAction.option = selected2.size().toString();
    }else if(left || right){
        newAction.targets = [];
        if(left)
            newAction.targets.push(left);
        if(right)
            newAction.targets.push(right);
    }else if(selected.size){
        newAction.targets = selected.backer;
    }

    const submittedAction = window.gameState.actions.actionList[actionIndex];
    const tryingToReplaceSend = submittedAction && submittedAction.command.toLowerCase() === 'send';

    const actionList = window.gameState.actions.actionList;
    if(actionIndex < actionList.length && command !== 'send' && !tryingToReplaceSend){
        if(command !== 'charge')
            left = right = selected = deadSelected = selected2 = null; // if i'm going to replace the action, i don't need to save my old stuff
        newAction.oldAction = actionIndex;
    }

    actionService.submit(newAction);
}

J.TARGET_TYPE = {
    UNCLICKED: 0,
    FULLY_CLICKED: 1,
    RIGHT_SELECTED: 2,
    LEFT_SELECTED: 3,
    WITCH_SELECTED: 4,
};

function setButtonSelected(name, mode){
    if(!name)
        return;
    const targetable = playerView.getTargetable(name);

    targetable.removeClass(true);
    targetable.removeClass(false);

    if(mode === J.TARGET_TYPE.FULLY_CLICKED){
        targetable.addClass(false, selectOptions.RIGHT_REGULAR_SELECT);
        targetable.addClass(true, selectOptions.LEFT_REGULAR_SELECT);
    }else if(mode === J.TARGET_TYPE.WITCH_SELECTED){
        targetable.addClass(true, selectOptions.LEFT_REGULAR_SELECT);
        targetable.addClass(false, selectOptions.WITCH_TARGET);
    }else if(mode === J.TARGET_TYPE.RIGHT_SELECTED){
        targetable.addClass(true, selectOptions.TARGETABLE_LEFT_DIV);
        targetable.addClass(false, selectOptions.RIGHT_REGULAR_SELECT);
    }else if(mode === J.TARGET_TYPE.LEFT_SELECTED){
        targetable.addClass(true, selectOptions.LEFT_REGULAR_SELECT);
        targetable.addClass(false, selectOptions.TARGETABLE_RIGHT_DIV);
    }else if(mode === J.TARGET_TYPE.UNCLICKED){
        targetable.addClass(true, selectOptions.TARGETABLE_LEFT_DIV); // is left
        targetable.addClass(false, selectOptions.TARGETABLE_RIGHT_DIV); // is right
    }
}

function setMultiCommandButtons(){
    const len = window.gameState.playerLists.type.length;
    if(len < 2){
        $('#arrow1').hide();
        $('#arrow2').hide();
    }else{
        const lt = $('#arrow1');
        const gt = $('#arrow2');
        lt.show();
        gt.show();
        lt.unbind();
        gt.unbind();
        lt.click(() => {
            window.gameState.commandsIndex = (window.gameState.commandsIndex + len - 1) % len;
            selected = null;
            selected2 = null;
            deadSelected = null;
            savedOpts = [null, null, null];
            refreshPlayers();
        });
        gt.click(() => {
            selected = null;
            selected2 = null;
            deadSelected = null;
            savedOpts = [null, null, null];
            window.gameState.commandsIndex = (window.gameState.commandsIndex + 1) % len;

            refreshPlayers();
        });
    }
}

function getAction(){
    let command = $('#playerList_header span').text().toLowerCase();
    if(command === 'give gun')
        command = 'gun';
    else if(command === 'give armor')
        command = 'armor';
    else if(command === 'spy on')
        command = 'spyability';
    else if(command === 'build room')
        command = 'build';
    else if(command === 'control voting')
        command = 'vote';
    else if(command === 'death reveal')
        command = 'plan';
    else if(command === 'order hit')
        command = 'murder';
    else if(command === 'execute')
        command = 'jail';
    else if(command === 'redirect vote')
        command = 'votesteal';
    else if(command === 'compare alignment')
        command = 'compare';
    return command;
}


function isVoting(p, q){
    p = p.toLowerCase();
    q = q.toLowerCase();
    let isVotingReturn = false;
    const gameState = window.gameState;
    const dayNumber = gameState.dayNumber;
    for(let i = 0; i < gameState.voteInfo.voteCounts[dayNumber - 1].movements.length; i++){
        const vote = gameState.voteInfo.voteCounts[dayNumber - 1].movements[i];
        if(vote.voter && vote.voter.toLowerCase() === p){
            if(vote.prev && vote.prev.toLowerCase() === q)
                isVotingReturn = false;
            if(vote.voted && vote.voted.toLowerCase() === q)
                isVotingReturn = true;
        }
    }
    return isVotingReturn;
}

function hasAbility(abilityName){
    abilityName = abilityName.replace(' ', '');
    const abilities = window.gameState.role.abilities;
    return abilities && abilities.indexOf(abilityName) !== -1;
}

function isAlive(playerName){
    if(!playerName)
        return false;
    return window.gameState.deadPlayers.indexOf(playerName) === -1;
}

function visibleAbilityIs(ability){
    if(!ability || typeof(ability) !== 'string')
        return false;
    const visibleAbility = getAction();
    return ability.toLowerCase() === visibleAbility;
}

function adjustActionPane(){
    const actionPane = $('#action_nav_pane');
    actionPane.removeClass('pl_no_actions');
    actionPane.removeClass('pl_show_actions');
    actionPane.removeClass('pl_only_button');
    actionPane.removeClass('pl_show_actions_no_button');
    $('#end_night_button').css('display', '');


    try{
        if(window.gameState.isOver())
            actionPane.addClass('pl_only_button');
        else if(window.gameState.getRuleValue('HOST_VOTING') && left && getAction() === 'vote'
            && window.gameState.isDay())
            if(selected.size())
                actionPane.addClass('pl_only_button');
            else
                actionPane.addClass('pl_no_actions');
        else if(!window.gameState.isAlive || window.gameState.isObserver)
            actionPane.addClass('pl_no_actions');
        else if(window.gameState.isDay())
            if(window.gameState.profile.isPuppeted || window.gameState.profile.isBlackmailed){
                actionPane.addClass('pl_no_actions');
            }else if(visibleAbilityIs('Jail') || visibleAbilityIs('Build')){
                actionPane.addClass('pl_show_actions_no_button');
            }else if(!visibleAbilityIs('Assassinate') && window.gameState.hasDayAction){
                actionPane.addClass('pl_only_button');
            }else{
                actionPane.addClass('pl_no_actions');
            }
        else if(window.gameState.endedNight())
            actionPane.addClass('pl_show_actions');
        else if(window.gameState.role && window.gameState.role.breadCount)
            actionPane.addClass('pl_show_actions');
        else if(window.gameState.playerLists.type.length > 1)
            actionPane.addClass('pl_show_actions');
        else if(visibleAbilityIs('Craft')
            || visibleAbilityIs('Dig')
            || visibleAbilityIs('Murder')
            || visibleAbilityIs('Control')
            || (visibleAbilityIs('Bounty') && window.gameState.role.jokerKills))
            actionPane.addClass('pl_show_actions');
        else
            actionPane.addClass('pl_only_button');
    }catch(err){
        helpers.log(err);
        actionPane.addClass('pl_show_actions');
    }
}

function setDayLabel(label){
    $('#day').html(label);
}

function setDayIcon(){
    const iconElement = $('#nav_tab_1 .navbarIcon');
    if(window.gameState.isDay()){
        iconElement.removeClass('fa fa-moon');
        iconElement.addClass('fa fa-sun');
    }else{
        iconElement.removeClass('fa fa-sun');
        iconElement.addClass('fa fa-moon');
    }
}

function setTimer(){
    let timeLeft;
    if(window.gameState.timer !== null){
        if(window.gameState.getRuleValue('HOST_VOTING') && window.gameState.isDay())
            timeLeft = new Date().getTime() - window.gameState.timer;
        else
            timeLeft = window.gameState.timer - new Date().getTime();
        if(timeLeft < 0)
            timeLeft = null;
    }else{
        timeLeft = null;
    }

    if(timeLeft !== null){
        let seconds = Math.floor(timeLeft / 1000);
        let minutes = Math.floor(seconds / 60);
        const hour = Math.floor(minutes / 60);

        let text = '';
        if(hour > 0){
            text += `${hour}:`;
            minutes %= 60;
            if(minutes < 10)
                minutes = `0${minutes}`;
        }
        text += `${minutes}:`;
        seconds = (seconds % 60).toString();
        if(seconds < 10)
            seconds = `0${seconds}`;

        text += seconds;


        $('.timerText').text(text);
    }else{
        $('.timerText').text('0:00');
    }
    if(getUnread() !== null){
        if($('#chat_ticker_expander i').hasClass('unread_ticker'))
            $('#chat_ticker_expander i').removeClass('unread_ticker');
        else
            $('#chat_ticker_expander i').addClass('unread_ticker');

        tabBlinker();

        Object.keys(window.gameState.unreadChats).forEach(name => {
            if(name === 'Pregame' && window.gameState.started)
                return;
            let eleID = name;
            eleID = eleID.replace(/ /g, '');
            eleID = eleID.replace('{', '\\{');
            eleID = eleID.replace('}', '\\}');
            eleID = eleID.replace('[', '\\[');
            eleID = eleID.replace(']', '\\]');
            eleID = eleID.replace('^', '\\^');
            eleID = eleID.replace("'", "\\'");
            eleID = eleID.replace('/', '\\/');
            eleID = eleID.replace('$', '\\$');
            const ele = $(`#${eleID}`);
            if(!window.gameState.unreadChats[name])
                ele.removeClass('unread_ticker');
            else
                ele.addClass('unread_ticker');
        });
    }else{
        resetTitle();
    }
    setTimeout(setTimer, 500);
}

function getHiddenProp(){
    const prefixes = ['webkit', 'moz', 'ms', 'o'];

    // if 'hidden' is natively supported just return it
    if('hidden' in document)
        return 'hidden';

    // otherwise loop over all the known prefixes until we find one
    for(let i = 0; i < prefixes.length; i++)
        if((`${prefixes[i]}Hidden`) in document)
            return `${prefixes[i]}Hidden`;


    // otherwise it's not supported
    return null;
}

function isHidden(){
    const prop = getHiddenProp();
    if(!prop)
        return false;

    return document[prop];
}

function getUnread(){
    let ret = null;
    Object.keys(window.gameState.unreadChats).forEach(val => {
        if(window.gameState.unreadChats[val] !== 0)
            if(!ret)
                ret = val;
            else if(window.gameState.unreadChats[val] > window.gameState.unreadChats[ret])
                ret = val;
    });

    return ret;
}

let blinked = 0;

function tabBlinker(){
    if(blinked === 0){
        blinked = 1;
    }else if(blinked === 1){
        blinked = 2;
        const unread = getUnread();
        setTitle(`${unread} (${window.gameState.unreadChats[unread]})`);
    }else if(blinked === 2){
        blinked = 3;
    }else{
        blinked = 0;
        resetTitle();
    }
}

function setTrim(color){
    if(!window.gameState.getRuleValue('CHAT_ROLES'))
        color = $('.nav_button_text').css('color');
    const trimObjects = $('.trim');

    trimObjects.css('color', color);
    // trimObjects.css('border-color', color);
    trimObjects.css('-moz-text-decoration-color', color);
    trimObjects.css('text-decoration-color', color);

    $('#chat_header').css('background', color);
    if(color === '#FFFF00')
        $('#chat_header h2').css('color', 'black');


    // const navBarEls = $('.nav_button .fa, .nav_button span');
    // if(window.gameState.started)
    //     navBarEls.css('color', color);
    // else
    //     navBarEls.css('color', NORM_TEXT_COLOR);
}
setTrim(NORM_TEXT_COLOR);

function refreshActionButtonText(){
    if(window.gameState.isOver())
        setButtonText('Exit Game');
    else if(window.gameState.isDay() && window.gameState.role)
        if(left && window.gameState.getRuleValue('HOST_VOTING'))
            if(left === 'Skip Day')
                setButtonText('Skip Day - No Lynch');
            else
                setButtonText(`Lynch ${left}`);
        else
            setButtonText('Assassinate');
    else if(window.gameState.endedNight())
        setButtonText('I\'m not Ready!');
    else
        setButtonText('Skip to Daytime');
}

function setButtonText(text){
    const button = $('#end_night_button');
    button.text(text);
}

function addAvailableRole(roleName){
    const o = {};
    o.message = 'addTeamRole';
    o.color = window.gameState.activeFactionID.color;
    o.simpleName = [roleName];
    o.roleName = roleName;
    webSend(o);
}

function blacklistRole(roleName){
    const o = {};
    o.message = 'removeTeamRole';
    o.color = window.gameState.activeFactionID.color;
    o.roleName = roleName;
    webSend(o);
}

function removeEnemy(e){
    const o = {};
    o.message = 'removeTeamEnemy';
    o.color = window.gameState.activeFactionID.color;
    o.enemy = e;
    webSend(o);
}

function removeAlly(e){
    const o = {};
    o.message = 'removeTeamAlly';
    o.color = window.gameState.activeFactionID.color;
    o.ally = e;
    webSend(o);
}

function getEditingFaction(){
    const color = `#${$('#teamEditorPane .title').attr('name').replace('color', '')}`;
    return window.gameState.factions[color];
}

function sheriffLoader(strippedColor){
    const color = `#${strippedColor.replace('color', '')}`;
    $('#teamEditorPane .title').attr('name', strippedColor);

    const tDetects = window.gameState.factions[color].sheriffDetectables;

    const teams = getAllTeams(nameSort);

    const detectsUL = $('#sheriffDetectables');
    const hiddenUL = $('#sheriffHiddens');

    const detects = [];
    const hiddens = [];
    for(let i = 0; i < teams.length; i++)
        if(tDetects.indexOf(teams[i].color) === -1)
            hiddens.push(teams[i]);
        else
            detects.push(teams[i]);


    function getTeamName(o){
        return o.name;
    }

    function getColor(o){
        return o.color;
    }

    function makeHidden(e){
        e = $(this);
        const detectable = convertColor(e.css('color'));

        const o = {};
        o.message = 'removeSheriffDetectable';
        o.color = getEditingFaction().color;
        o.detectableColor = detectable;
        webSend(o);
    }

    function makeDetectable(e){
        e = $(this);
        const detectable = convertColor(e.css('color'));

        const o = {};
        o.message = 'addSheriffDetectable';
        o.color = getEditingFaction().color;
        o.detectableColor = detectable;
        webSend(o);
    }

    function blackSpanProperty(o, li){
        li.css('color', o.color);
    }

    populateUL(detectsUL, detects, getTeamName, getColor, [makeHidden, null],
        blackSpanProperty, null);
    populateUL(hiddenUL, hiddens, getTeamName, getColor, [makeDetectable, null],
        blackSpanProperty, null);
}

function teamLoader(factionID){
    let faction = null;
    function getTeamName(o){
        return o.name;
    }

    function getColor(o){
        return o.color;
    }

    function getName(o){
        return o.roleName;
    }

    function getBlackistName(o){
        return o.name;
    }

    function makeAvailable(e){
        if(faction.isEditable && window.gameState.isHost() && isSuperCustom()){
            e = $(this);
            const toMakeAvail = e.attr('name');
            addAvailableRole(toMakeAvail);
        }
    }

    function availLiProperty(li, o){
        li.attr('name', o.simpleName);
    }

    function blackLiProperty(li, o){
        li.attr('name', o.simpleName);
        li.addClass('role-strike-through');
    }

    function blackSpanProperty(o, li, span){
        span.attr('name', o.simpleName);
        span.addClass('role-strike-through');
    }

    function makeUnavailable(){
        if(faction.isEditable && window.gameState.isHost() && isSuperCustom())
            blacklistRole($(this).attr('name'));
    }

    function makeEnemy(e){
        if(faction.isEditable && window.gameState.isHost() && isSuperCustom()){
            e = $(this);
            let enemyColor = e.attr('name');
            enemyColor = enemyColor.replace('color', '#');
            removeAlly(enemyColor);
        }
    }

    function allySpan(o, li, span){
        span.css('color', o.color);
    }

    function allyLi(li, o){
        const allyColor = o.color.replace('#', 'color');
        li.attr('name', allyColor);
    }

    function makeAlly(){
        if(faction.isEditable && window.gameState.isHost() && isSuperCustom()){
            const e = $(this);
            const allyColor = e.attr('name')
                .replace('color', '#');
            removeEnemy(allyColor);
        }
    }

    if(factionID){
        faction = window.gameState.setup.factionMap[factionID];

        setActiveFaction(factionID);

        $('#teamEditorPane .title').attr('name', factionID);

        $('#newTeamName').val(faction.name);
        $('#newTeamColor').val(faction.color);
        $('#teamDescription').text(faction.description);

        const canNotEdit = !(window.gameState.isHost() && faction.isEditable && isSuperCustom());
        $('.teamEditorWidget').prop('disabled', canNotEdit);

        $('#newTeamName').unbind();
        $('#newTeamName').focusout(function(e){
            e = $(this);
            const text = e.val();

            const o = {};
            o.message = 'teamChangeName';
            o.color = faction.color;
            o.teamName = text;
            webSend(o);
        });

        $('#teamDescription').unbind();
        $('#teamDescription').focusout(function(e){
            e = $(this);
            const text = e.val();

            const o = {};
            o.message = 'teamChangeDescription';
            o.color = faction.color;
            o.teamChangeDescription = text;
            webSend(o);
        });

        for(let i = 0; i < faction.rules.length; i++){
            let rule = faction.rules[i];
            const val = window.gameState.rules[rule].val;
            rule = rule.replace('#', '').substring(6);

            if(rule === 'killcd' || rule === 'punchSucc'){
                $(`.${rule}val`).text(val);
                if(val === -1 && rule === 'killcd'){
                    $(`.${rule}`).hide();

                    rule = 'hasKill';
                    $(`.${rule}.left_toggle_border`).addClass('selected_toggle');
                    $(`.${rule}.right_toggle_border`).removeClass('selected_toggle');
                }else if(rule === 'killcd'){
                    $(`.${rule}`).show();

                    rule = 'hasKill';
                    $(`.${rule}.left_toggle_border`).removeClass('selected_toggle');
                    $(`.${rule}.right_toggle_border`).addClass('selected_toggle');
                }
            }else if(!val){
                $(`.${rule}.left_toggle_border`).addClass('selected_toggle');
                $(`.${rule}.right_toggle_border`).removeClass('selected_toggle');
            }else{
                $(`.${rule}.left_toggle_border`).removeClass('selected_toggle');
                $(`.${rule}.right_toggle_border`).addClass('selected_toggle');
            }
        }

        $('.teamEditor2Element .settings_toggle').unbind();

        if(window.gameState.isHost() && isSuperCustom())
            $('.teamEditor2Element .settings_toggle').click(function(e){
                e = $(this);
                if(e.hasClass('selected_toggle'))
                    return;


                const classes = e.attr('class').split(' ');
                let class_;
                let val;
                for(let i = 0; i < classes.length; i++){
                    class_ = classes[i];
                    val = e.hasClass('left_toggle_border');
                    if(class_ === 'right_toggle_border'){
                        continue;
                    }else if(class_ === 'left_toggle_border'){
                        continue;
                    }else if(class_ === 'settings_toggle'){
                        continue;
                    }else if(class_ === 'selected_toggle'){
                        continue;
                    }else if(class_ === 'hasKill'){
                        class_ = 'killcd';
                        if(val)
                            val = -1;
                        else
                            val = 0;
                    }
                    factionModifierService.update(val, class_, factionID);
                }
            });

        $('.teamEditor2Element .team_plus_minus').unbind();

        if(window.gameState.isHost() && isSuperCustom())
            $('.teamEditor2Element .team_plus_minus').click(function(){
                const e = $(this);
                const clickedColor = `#${$('#teamEditorPane .title')
                    .attr('name').replace('color', '')}`;
                const classes = e.attr('class').split(' ');
                let class_;
                let val;
                let plus;
                for(let i = 0; i < classes.length; i++){
                    class_ = classes[i];
                    if(class_ === 'fa'){
                        continue;
                    }else if(class_ === 'team_plus_minus'){
                        continue;
                    }else if(class_ === 'fa-minus-square'){
                        plus = false;
                        continue;
                    }else if(class_ === 'fa-plus-square'){
                        plus = true;
                        continue;
                    }
                }

                if(class_){
                    val = window.gameState.rules[clickedColor + class_].val;
                    if(plus)
                        val++;
                    else
                        val--;
                    factionModifierService.update(val, class_, factionID);
                }
            });


        populateUL($('#teamEditorAvail'), [], getName, getColor,
            [makeUnavailable, null], null, availLiProperty);
        populateUL($('#teamEditorBlackL'), [], getBlackistName,
            null, [makeAvailable, null], blackSpanProperty, blackLiProperty);
        populateUL($('#teamEditorAllies'), faction.allies, getTeamName, getColor,
            [makeEnemy, null], allySpan, allyLi);
        populateUL($('#teamEditorEnemies'), faction.enemies, getTeamName, getColor,
            [makeAlly, null], allySpan, allyLi);
    }
    const teams = getAllTeams(prioritySort);


    let prevPriority = null;

    function tieSpan(o, li, span){
        const s = $('<span>');
        s.addClass('tieNumberLabel');
        s.text(`${o.priority})`);
        if((!prevPriority && prevPriority !== 0) || prevPriority !== o.priority){
            li.prepend(s);
            prevPriority = o.priority;
        }

        span.addClass('tieTextLabel');

        if(window.gameState.isHost() && isSuperCustom() && setupService.isHostsSetup()){
            const minus = $('<i>').addClass('fa fa-arrow-down tiebreakerPlusMinus');
            minus.click(function(e){
                e = $(this).parent();
                let color = e.find('.tieTextLabel').css('color');
                color = convertColor(color);

                const prevValue = window.gameState.rules[`${color}priority`].val;

                factionModifierService.update(prevValue - 1, 'priority', factionID);
            });
            li.append(minus);

            const plus = $('<i>').addClass('fa fa-arrow-up tiebreakerPlusMinus');
            plus.click(function(e){
                e = $(this).parent();
                let color = e.find('.tieTextLabel').css('color');
                color = convertColor(color);

                const prevValue = window.gameState.rules[`${color}priority`].val;
                factionModifierService.update(prevValue + 1, 'priority', factionID);
            });
            li.append(plus);
        }
    }

    populateUL($('#tieBreakerUL'), teams, getTeamName, getColor, [null, null], tieSpan, null);


    if(!$('body').hasClass('teamEditor')){
        showTeamEditor();
        if(window.gameState.isHost() && setupService.isHostsSetup() && isSuperCustom())
            $('#teamDelete').show();
        else
            $('#teamDelete').hide();
    }
}

function prioritySort(x, y){
    if(y.priority === x.priority)
        return nameSort(x, y);
    return y.priority - x.priority;
}

function nameSort(x, y){
    return x.name.localeCompare(y.name);
}

function getAllTeams(sortFunction){
    const teams = Object.values(window.gameState.setup.factionMap)
        .map(faction => ({
            name: faction.name,
            color: faction.color,
            priority: faction.modifiers.WIN_PRIORITY,
        }));

    if(sortFunction)
        teams.sort(sortFunction);

    return teams;
}

function showTeamEditor(){
    $('body').addClass('teamEditor teamEditor1'); // neds to change back to 1
}

function setLastWillContainer(){
    const lwContainer = $('#your_exp_pane');
    let chosenLWClass;
    const generalLW = window.gameState.getRuleValue('LAST_WILL');
    if(window.gameState.isAlive)
        if(generalLW && hasTeamLastWill()){
            chosenLWClass = 'lw_double';
            if(hasTeamLastWill()){
                const teamName = window.gameState.factions[window.gameState.role.roleColor].name;
                lwContainer.find('.settings_off').text(teamName);
            }
        }else if(generalLW || hasTeamLastWill()){
            chosenLWClass = 'lw_single';
        }else{
            chosenLWClass = 'lw_none';
        }
    else
        chosenLWClass = 'lw_none';


    lwContainer.removeClass('lw_single lw_double lw_none');
    lwContainer.addClass(chosenLWClass);
}

function hasTeamLastWill(){
    if(!window.gameState.role)
        return false;
    const color = window.gameState.role.roleColor;
    const faction = window.gameState.factions[color];
    if(!faction)
        return false;
    const rules = faction.rules;
    for(let i = 0; i < rules.length; i++)
        if(rules[i].indexOf('upon death') !== -1)
            return true;

    return false;
}

function setIsAlive(livingStatus){
    window.gameState.isAlive = livingStatus;

    if(!livingStatus)
        $('.last_will_item').hide();

    if(!window.gameState.isAlive || window.gameState.isOver())
        $('#leaveGameButton').show();
    else
        $('#leaveGameButton').hide();
}

function setRoleInfo(){
    if(!window.gameState.started)
        return;

    const roleInfo = window.gameState.role;

    setIsAlive(roleInfo.isAlive);

    const roleImageName = 'citizen'; // roleInfo.roleBaseName.toLowerCase();
    setImage($('#your_exp_pane'), roleImageName);

    setTrim(roleInfo.roleColor);
}

function prepObserverMode(){
    if(!window.gameState.isObserver)
        return;

    $('#time h2').css('color', 'black');
    $('#day h2').css('color', 'black');
    $('#action_nav_pane').hide();
    $('#observer_votelist_pane').show();

    const innerLI = $('#observer_votelist li ol li');
    innerLI.css('font-size', '1.1vw');
    innerLI.css('text-align', 'left');

    $('#observer_votelist li ol').css('padding-left', '0%');

    $('#observer_votelist li ol li').css('text-align', 'center');

    $('#observer_votelist_ul').css('text-align', 'center');
    // $("#observer_votelist_ul li div span").css("text-decoration", "underline");

    $('#observer_votelist_pane').css('width', '25%');

    $('#main_table').css('left', '35%');
    $('#main_table').css('width', '35%');

    $('#your_info_wrapper').css('width', '31%');

    $('#roleList_pane').css('height', '82%');
    $('#roleList_content').css('top', '8%');

    $('#graveyard_pane').css('height', '82%');
    $('#graveyard_content').css('top', '8%');


    $('#game_play_card').hide();
}

function refreshActionUL(){
    const actions = window.gameState.actions;

    function actionClick(e){
        e = $(this);
        const index = parseInt(e.attr('name'), 10);
        const ability = actions.actionList[index].command.toLowerCase();

        window.gameState.playerLists.type.forEach(value => {
            if(value.toLowerCase() === ability)
                window.gameState.commandsIndex = index;
        });
        if(ability !== 'send')
            actionIndex = index;


        selected = null;
        selected2 = null;
        deadSelected = null;
        left = null;
        right = null;
        savedOpts = [null, null, null];

        refreshActionUL();
        refreshPlayers();
    }

    const submittedActions = actions.actionList.map((action, i) => ({
        text: action.text,
        italicized: (actionIndex === i && action.command.toLowerCase() !== 'send'),
        nameAttr: i.toString(),
    }));

    actionIndex = Math.min(actionIndex, submittedActions.length);


    $('#add_action')
        .off('click')
        .click(() => {
            left = null;
            right = null;
            selected = null;
            selected2 = null;
            deadSelected = null;
            savedOpts = [null, null, null];
            actionIndex++;
            refreshPlayers();
            $('#add_action').hide();
        });
    adjustActionPane();


    const actionContainer = $('#actions_content ol');
    actionContainer.empty();

    for(let i = 0; i < actions.actionList.length; i++){
        const action = submittedActions[i];
        const li = $('<li>');
        let text = action.text.replace('You will ', '');
        text = text.charAt(0).toUpperCase() + text.substr(1);
        li.html(text);
        if(action.italicized)
            li.css('font-style', 'italic');
        li.attr('name', action.nameAttr);
        li.click(actionClick);
        actionContainer.append(li);
    }
}

function refreshHost(){
    if(window.gameState.isHost())
        $('body').addClass('isHost');
    else
        $('body').removeClass('isHost');


    if(isFirstPhase())
        $('body').addClass('isFirstPhase');
    else
        $('body').removeClass('isFirstPhase');

    const navBar = $('.nav_bar');
    if(!isOnSetup()){
        navBar.removeClass('fourButtonNav');
        return;
    }

    if(window.gameState.isHost())
        navBar.removeClass('fourButtonNav');
    else
        navBar.addClass('fourButtonNav');

    generalSettingsView.refresh(); // this used to be just in the host.

    $('.general_rules').prop('disabled', !window.gameState.isHost());
}

function setPrivateInstance(){
    const pane = $('#setup_gen_settings_pane');
    if(window.gameState.isPrivateInstance){
        if(!pane.hasClass('isPrivateInstance'))
            pane.addClass('isPrivateInstance');
    }else if(pane.hasClass('isPrivateInstance')){
        pane.removeClass('isPrivateInstance');
    }
}

function setGameID(){
    helpers.setPseudoURL(window.gameState.joinID);
    if(!window.gameState.started){
        $('.gameIDArea').text(window.gameState.joinID);
        $('.gameIDURLArea').text(`narrator.systeminplace.net/${window.gameState.joinID}`);
    }
}

function clickLastElementInFactionUL(){
    $('#team_catalogue_pane span').last().click();
}

function getEmpIndex(){
    if(!helpers.storageEnabled())
        return window.gameState.empCounter;
    return parseInt(localStorage.getItem('emp_last_role'), 10);
}

function setEmpIndex(index){
    if(!helpers.storageEnabled())
        window.gameState.empCounter = index;
    else
        localStorage.setItem('emp_last_role', index);
}

function queryEmpData(){
    if(window.gameState.emporiumData)
        return Promise.resolve(window.gameState.emporiumData);

    return requests.get('emporium')
        .then(data => {
            window.gameState.emporiumData = {
                roles: data,
            };
            return window.gameState.emporiumData;
        });
}

function setEmpCenter(bName){
    const object = window.gameState.emporiumData.roles[bName];
    $('#emporiumPic3 span').text(object.roleName);

    setImage($('#emporiumPic3'), bName);

    const appearances = parseInt(object.appearances || 1, 10);

    let winRate = (parseInt(object.wins || 0, 10) / appearances);
    winRate = parseFloat(winRate * 100).toFixed(2);

    $('#global_rate').text(winRate);

    if(helpers.isLoggedIn() && window.gameState.statData.roleStats[bName]){
        const stats = window.gameState.statData.roleStats[bName];
        let rate = stats.wins / stats.appearances;
        rate = parseFloat(rate * 100).toFixed(2);
        $('#personal_rate').text(`${rate}%`);
        $('#personal_win_rate_label').show();
    }else{
        $('#personal_win_rate_label').hide();
    }

    $('#emp_tidbits').empty();

    $('<li>').text(object.description).appendTo($('#emp_tidbits'));

    $('#emp_options').empty();
    if(object.rules.length){
        $('.emp_option_element').show();
        $.each(object.rules, (key, value) => {
            $('<li>').text(value).appendTo($('#emp_options'));
        });
    }else{
        $('.emp_option_element').hide();
    }


    $('#emp_stats').empty();

    if(object.appearances){
        let lynchRate = (parseInt(object.lynches || 0, 10) / appearances) * 100;
        lynchRate = parseFloat(lynchRate).toFixed(0);
        $('<li>').text(`Lynched ${lynchRate}% of the time.`).appendTo($('#emp_stats'));

        let avgDeath = parseFloat(object.deathDay) / 10;
        if(avgDeath % 1 >= 0.5)
            avgDeath = `Usually lives until until Night ${parseInt(avgDeath, 10)} or game over`;
        else
            avgDeath = `Usually lives until until Day ${parseInt(avgDeath, 10)} or game over`;

        $('<li>').text(avgDeath).appendTo($('#emp_stats'));
        $('#emp_stats_title').show();
    }else{
        $('#emp_stats_title').hide();
    }
}

function setSideView(bName, ind){
    const object = window.gameState.emporiumData.roles[bName];
    const ele = $(`#emporiumPic${ind}`);
    if(!object){
        ele.hide();
    }else{
        ele.show();
        ele.find('span').text(object.roleName);
        setImage(ele, bName);
    }
}

// eslint-disable-next-line no-unused-vars
function loadEmporiumData(){
    let p;
    if(helpers.isLoggedIn())
        p = getPersonalStats();
    else
        p = Promise.resolve();


    p.then(queryEmpData)
        .then(data => {
            data.list = Object.keys(data.roles);
            data.list.sort((x, y) => data.roles[x].roleName.localeCompare(data.roles[y].roleName));

            let index;
            if(helpers.storageEnabled()
                && (localStorage.getItem('emp_last_role')
                    || localStorage.getItem('emp_last_role') === 0))
                index = parseInt(localStorage.getItem('emp_last_role'), 10);

            if(!index && index !== 0)
                index = Math.floor(data.list.length / 2);

            if(index >= data.list.length)
                index = Math.floor(data.list.length / 2);
            setEmpIndex(index);
            refreshEmporium(data);
        }).catch(helpers.log);
}

function refreshEmporium(data){
    const index = getEmpIndex();
    setEmpCenter(data.list[index]);
    setSideView(data.list[index - 1], 2);
    setSideView(data.list[index - 2], 1);
    setSideView(data.list[index + 1], 4);
    setSideView(data.list[index + 2], 5);

    if(data.list[index + 3])
        $('#right_emporium6').show();
    else
        $('#right_emporium6').hide();


    if(data.list[index - 3])
        $('#left_emporium0').show();
    else
        $('#left_emporium0').hide();
}

function queryLeaderboard(cName){
    return requests.get('leaderboard', {
        columnName: cName,
    });
}

function loadLeaderboard(columnName){
    const labels = [{
        columnName: 'points',
        title: 'Most Points',
        subTitle: 'Most Points',
    },
    {
        columnName: 'winRate',
        title: 'Best Win Rate',
        subTitle: 'Most Wins',
    },
    {
        columnName: 'gamesPlayed',
        title: 'Most Games Played',
        subTitle: 'Most Games',
    },
    ];

    let label;
    let leftLabel;
    let rightLabel;
    const length = labels.length;
    for(let i = 0; i < length; i++){
        label = labels[i];
        if(label.columnName === columnName){
            rightLabel = labels[(i + 1) % length];
            leftLabel = labels[(i - 1 + length) % length];
            break;
        }
    }

    $('#leaderboard_pane .emp_title').text(label.title);

    $('#leaderboard_pane .label_left').text(leftLabel.subTitle);
    $('.leaderboardLeftChanger').attr('name', leftLabel.columnName);

    $('#leaderboard_pane .label_right').text(rightLabel.subTitle);
    $('.leaderboardRightChanger').attr('name', rightLabel.columnName);

    queryLeaderboard(columnName)
        .then(leaderboard => {
            const first = leaderboard[0];
            $('.leaderboard_firstName').text(first.name || user.displayName);
            $('.leaderboard_firstLabel').text(first.value);
            $('.leaderboard_length').text(''); // Todo

            let prevRank = 1;
            const leaderUL = $('#leaderboard_list');
            leaderUL.empty();
            for(let i = 1; i < leaderboard.length; i++){
                const person = leaderboard[i];
                const li = $('<li>').addClass('leaderboard_listing');
                const numberSpan = $('<span>').addClass('leaderboard_number');
                const valueSpan = $('<span>').addClass('leaderboard_content_label');

                numberSpan.text(`${person.rank}.`);
                valueSpan.text(person.value);

                li.append(numberSpan);
                li.append(person.name || user.displayName);
                li.append(valueSpan);

                if(!person.name){
                    li.addClass('leaderboard_me');
                    li.prepend($('<i>').addClass('fa fa-arrow-right leaderboard_mearrow'));
                }

                if(prevRank + 1 !== person.rank && prevRank){
                    const spacer = $('<li>')
                        .addClass('leaderboard_list_spacer')
                        .append($('<i>')
                            .addClass('fa fa-ellipsis-h')
                            .attr('id', 'leaderboard_list_spacer'));
                    spacer.appendTo(leaderUL);
                    prevRank = null;
                }else if(prevRank){
                    prevRank = person.rank;
                }

                li.appendTo(leaderUL);
            }
        }).catch(helpers.log);
}

// https://plot.ly/javascript/reference/
function setPlot(wins, losses, plotLoc){
    if(!wins && !losses)
        losses = 1;

    let labels = ['Wins', 'Losses'];
    if(!losses){
        const x = losses;
        losses = wins;
        wins = x;
        labels = ['Losses', 'Wins']; // have to do this switch because of a bug in plotly not handling 0 values very well
    }

    const data = [{
        values: [wins, losses],
        labels,
        type: 'pie',
    }];

    let fontSize;
    if(helpers.isMobile())
        fontSize = 20;
    else
        fontSize = 20;


    const layout = {

        font: {
            size: fontSize,
            color: NORM_TEXT_COLOR,
        },
        paper_bgcolor: 'rgba(0,0,0,0)',
        plot_bgcolor: 'rgba(0,0,0,0)',
    };

    Plotly.newPlot(plotLoc, data, layout, {
        displayModeBar: false,
        responseive: true,
    });
}

function getPersonalStats(){
    if(window.gameState.statData)
        return Promise.resolve(window.gameState.statData);


    return requests.get('personal_stats')
        .then(data => {
            const roleNames = Object.keys(data.roleStats);
            roleNames.forEach(roleName => {
                const role = data.roleStats[roleName];

                if(!role.wins)
                    role.wins = 0;
                else
                    role.wins = parseInt(role.wins, 10);

                role.avgDeathDay = parseFloat(role['AVG(deathDay)']);
                role.appearances = parseInt(role['COUNT(baseName)'], 10);
                role.survivor = parseInt(role.survivor || 0, 10);
                role.lynch = parseInt(role.lynch || 0, 10);

                role.games.forEach(sGame => {
                    sGame.date = new Date(sGame.date);
                });

                delete role['AVG(deathDay)'];
                delete role['COUNT(baseName)'];
            });
            window.gameState.statData = data;

            return data;
        }).catch(helpers.log);
}


function loadProfileView(username, selectedRole){
    $('#displayNameSpan').text(username);

    getPersonalStats()
        .then(data => {
            const roleNames = Object.keys(data.roleStats);

            let wins = 0;
            let total = 0;
            let survivor = 0;
            let lynched = 0;

            let bestWin;
            let bestRole;

            const pastGames = [];
            const points = data.points;

            function collectStats(role){
                wins += role.wins;
                total += role.appearances;
                survivor += role.survivor;
                lynched += role.lynch;

                const winRatio = role.wins / role.appearances;

                const option = $('<option>');
                option.text(role.roleName);
                option.attr('value', role.baseName);
                option.addClass('profile_non_general');
                if(!selectedRole)
                    $('#profile_role_selector').append(option);

                if(!bestWin || winRatio > bestWin
                    || (winRatio === bestWin
                        && role.appearances > data.roleStats[bestRole].appearances)){
                    bestWin = winRatio;
                    bestRole = role.baseName;
                }

                role.games.forEach(pastGames.push);
            }

            let role;
            if(!roleNames.length){
                $('.bestRole, .profile_achievements').hide();

                return;
            }if(!selectedRole){
                $('.bestRole, .profile_achievements').show();

                $('.profile_non_general').remove();

                roleNames.forEach(roleIndicator => {
                    const roleStats = data.roleStats[roleIndicator];
                    collectStats(roleStats);
                });

                bestRole = data.roleStats[bestRole].roleName;
                $('.profile_list.bestRole').text(`${bestRole} : ${bestWin * 100}%`);

                if(data.achievements.length){
                    $('.profile_achievements').show();
                    data.achievements.forEach(achievement => {
                        addTextToUL(achievement, '#profile_achievements_list');
                    });
                }else{
                    $('.profile_achievements').hide();
                }
            }else{ // role selected
                $('.bestRole, .profile_achievements').hide();
                role = data.roleStats[selectedRole];
                collectStats(role);
            }

            $('.profile_points').text(points);

            if(wins && total === wins)
                wins--;

            setPlot(wins, total - wins, 'profile_pie_container');

            $('#profile_statistics_list').empty();
            const ulID = '#profile_statistics_list';
            if(total){
                const winPercentage = parseFloat((100 * survivor) / total).toFixed(0);
                addTextToUL(`Survived until the end: ${winPercentage}%`, ulID);
            }else{
                total = 1;
            }

            addTextToUL(`Lynched : ${parseFloat((100 * lynched) / total).toFixed(0)}%`, ulID);

            pastGames.sort((x, y) => y.date.getTime() - x.date.getTime());

            $('#profile_recent_games_list').empty();
            if(pastGames.length){
                pastGames.forEach(pGame => {
                    const li = $('<li>');
                    const a = $('<a>');
                    a.append(pGame.date.toLocaleDateString());
                    if(!selectedRole)
                        a.append(` - ${pGame.role}`);

                    a.attr('href', `${helpers.getBaseURL()}storyRecap/${pGame.game_id}`);
                    a.appendTo(li);
                    $('#profile_recent_games_list').append(li);
                });
                $('.profile_recent_games').show();
            }else{
                $('.profile_recent_games').hide();
            }
        });
}

function addTextToUL(text, ul){
    if(typeof(ul) === 'string')
        ul = $(ul);
    ul.append($('<li>').text(text));
}

function changeDisplayName(newName){
    const jo = {
        message: 'nameChange',
        playerName: newName,
        action: true,
    };
    $('#displayNameSpan').text(newName);
    webSend(jo);
}

function clearGameHostPopup(){
    const ret = $('#gameHostBox').is(':visible');
    $('.gameHostContainers').hide();
    $('#logo').show();
    return ret;
}

const PRESET = 0;
const BASIC_CUSTOM = 1;
const ADV_CUSTOM = 2;

function goToGlobalLobbyPage(){
    $('#main').hide();
    $('#setup_page').hide();
    $('#lobby_page').show();

    $('#nav_tab_1 span').removeClass('timerText');
    $('#nav_tab_1 i').text('');
    $('#nav_tab_1 span').text('Home');
    $('#nav_tab_2 span').text('Discord');
    $('#nav_tab_3 span').text('Forums');
    $('#nav_tab_4 span').text('Profile');

    $('.nav_button').addClass('unselected_nav').removeClass('selected_nav');
    $('#nav_tab_1').addClass('selected_nav').removeClass('unselected_nav');

    clearNavTabImages();

    $('#nav_tab_1 i').addClass('fa fa-home');
    $('#nav_tab_2 i').addClass('fab fa-discord');
    $('#nav_tab_3 i').addClass('far fa fa-comments');
    $('#nav_tab_4 i').addClass('fa fa-sign-in-alt');

    showGameHostPopup(PRESET);
    if(helpers.isLoggedIn()){
        $('.nav_bar').removeClass('fourButtonNav');
        $('#nav_tab_4 .fa').addClass('fa-user');
        $('#nav_tab_5 span').text('Log Out');
        // $("#togglePublicButton").addClass('selected_toggle');
        // $("#togglePrivateButton").removeClass('selected_toggle');

        $('.displayNameInput').val(window.user.displayName);

        $('#gameHost_custom').addClass('selected_toggle');
        $('#gameHost_preset').removeClass('selected_toggle');
    }else{
        $('.nav_bar').addClass('fourButtonNav');
        $('#nav_tab_4 .fa').addClass('fa-sign-in-alt');
        $('#nav_tab_4 span').text('Sign In');

        // $("#togglePrivateButton").addClass('selected_toggle');
        // $("#togglePublicButton").removeClass('selected_toggle');

        $('#gameHost_preset').addClass('selected_toggle');
        $('#gameHost_custom').removeClass('selected_toggle');
    }

    // temporary
    $('#togglePrivateButton').addClass('selected_toggle');
    $('#togglePublicButton').removeClass('selected_toggle');


    $('.chat_ticker').hide();

    $('#gameCodeBox').hide();

    $('.displayNameInput').unbind();


    $('.displayNameInput').bind('change keyup', function(e){
        e = $(this);
        let text = e.val();
        if(text)
            text = text.replace(/\s/g, '');

        e = $('#displayNameSpan');
        if(!text)
            e.addClass('error_text_input');
        else
            e.removeClass('error_text_input');
    });

    $('.displayNameInput').focusout(function(e){
        e = $(this);
        let text = e.val();
        if(text)
            text = removeNonAlphaNumericCharacters(text);
        if(!text && user.email)
            text = user.email.substring(0, user.email.indexOf('@'));

        changeDisplayName(text);
        saveInputtedGameName(text);
        $('body').removeClass('profile_editName');

        $('.displayNameInput').val(text);
        $('#displayNameSpan').removeClass('error_text_input');
    });

    $('#nav_tab_1, .miniIcon, #logo').unbind().click(function(e){
        if($('.miniIcon:hover').length)
            setMiniIconBlue();
        if(clearGameHostPopup())
            return;
        e = $(this);
        setNavSelected(e);
        $('.changeable_display').hide();
        $('#home_pane').show();
        $('#gameCodeBox').hide();
    });

    $('#nav_tab_2').unbind().click(() => {
        window.location.href = 'https://discord.gg/MDpPMaW';
        // setWarning('This page is temporarily disabled');
        // const e = $(this);
        // setNavSelected($(this));
        // $('.changeable_display').hide();
        // $('#emporium_pane').show();
        // loadEmporiumData();
    });

    $('#nav_tab_3').unbind().click(() => {
        const link = 'http://www.sc2mafia.com/forum/forumdisplay.php/442-Narrator-Discussion';
        window.location.href = link;
        // setWarning('This page is temporarily disabled');
        // setNavSelected($(this));
        // $('.changeable_display').hide();
        // $('#leaderboard_pane').show();
        // loadLeaderboard('points');
    });


    $('#nav_tab_4').unbind();
    $('#nav_tab_4').click(function(){
        const e = $(this);
        if(e.hasClass('selected_nav'))
            return;

        setNavSelected(e);
        $('.changeable_display').hide();

        if(helpers.isLoggedIn()){
            $('.changeable_display').hide();
            loadProfileView(window.user.displayName, null);
            $('#profile_pane').show();
        }else{
            showHomePopup(LOGIN);
        }
    });

    $('#nav_tab_5').unbind();
    $('#nav_tab_5').click(authService.logout);


    clearNavBarClasses();
    if(isTablet())
        $('body').addClass('tablet_home');
    else if(helpers.isMobile())
        $('body').addClass('mobile');

    bodySet();

    $('.fa-home').click();
    $('.changeable_display').hide();
    $('#home_pane').show();

    const imageHeight = $('#logo img').css('height').replace('px', '');
    const divHeight = $('#logo').css('height').replace('px', '');
    $('#logo img').css('top', (divHeight - imageHeight) / 2);


    new SimpleBar($('#lobby_messages')[0]); // eslint-disable-line no-new
}

function goToSetupPage(){
    $('#main').hide();
    $('#setup_page').show();
    $('#lobby_page').hide();

    $('#gameCodeBox .login_input_style').val('');

    $('#nav_tab_1 span').removeClass('timerText');
    $('#nav_tab_1 i').text('');
    $('#nav_tab_1 span').text('Leave');
    $('#nav_tab_2 span').text('Settings');
    $('#nav_tab_3 span').text('Roles (')
        .append($('<span>').addClass('rolesListCounter'))
        .append(')');
    $('#nav_tab_4 span').text('Lobby (')
        .append($('<span>').addClass('lobbyCountLabel'))
        .append(')');
    $('#nav_tab_5 span').text('Start');

    clearNavTabImages();

    $('#nav_tab_1 i').addClass('fa fa-arrow-left');
    $('#nav_tab_2 i').addClass('fa fa-cogs');
    $('#nav_tab_3 i').addClass('fa fa-book');
    $('#nav_tab_4 i').addClass('fa fa-users');
    $('#nav_tab_5 i').addClass('fab fa-youtube');

    if(!helpers.isMobile()){
        $('.changeable_display').show();
        $('.chat_ticker').hide();
        $(window).unbind();
        $(window).scroll(() => {
            if($(window).scrollTop() + $(window).height() === $(document).height())
                pushServerUnread('Pregame');
        });
    }

    clearNavBarClasses();
    if(isTablet())
        $('body').addClass('tablet_setup');
    else if(helpers.isMobile())
        $('body').addClass('mobile');
    bodySet();

    $('.nav_button, .startButton').unbind();

    $('.startButton').click(() => {
        closeTeamEditor();
        gameService.start();
    });

    $('.leaveButton').unbind();
    $('.leaveButton').click(() => {
        closeTeamEditor();
        if(window.gameState.started)
            return;
        chatView.hideChatInput();
        $('#pregame_messages').empty();
        leaveGame();
        resetGameState();
    });

    $('#nav_tab_2').click(function(e){
        e = $(this);
        closeTeamEditor();
        if(e.hasClass('selected_nav'))
            return;
        setNavSelected(e);
        $('.changeable_display').hide();

        $('#setup_gen_settings_pane').show();
        $('.chat_ticker').show();
        chatView.hideChatInput();
        $('#setup_page').addClass('ticker_showing');
    });

    $('#nav_tab_3').click(function(e){
        e = $(this);
        closeTeamEditor();
        if(e.hasClass('selected_nav'))
            return;
        setNavSelected(e);
        $('.changeable_display').hide();

        $('.roles_setup_element').show();
        $('.chat_ticker').show();
        chatView.hideChatInput();
        $('#setup_page').removeClass('ticker_showing');
    });

    $('#nav_tab_4').click(function(e){
        e = $(this);
        closeTeamEditor();
        if(e.hasClass('selected_nav'))
            return;
        setNavSelected(e);
        $('.changeable_display').hide();

        $('#setup_lobby_pane').show();
        $('.chat_ticker').show();
        chatView.hideChatInput();
        $('#setup_page').addClass('ticker_showing');
    });

    if(helpers.isMobile())
        $('.fa-cogs').click();
    soundService.play();

    if(helpers.isMobile() || isTablet()){
        $('.editRolesButton span').text('Roles ');
        $('.editAlliesButton span').text('Allies ');
        // $(".editTeamButton").append($("<i>").addClass("fa fa-cog fa-lg"));
    }

    if(helpers.isMobile())
        $('.chat_pane').hide();

    $('.gameHostContainers').hide();

    $('#chat_ticker_pane, #chat_ticker_expander').unbind();
    if(helpers.isMobile())
        $('#chat_ticker_pane, #chat_ticker_expander').click(() => {
            setNavSelected();

            $('.changeable_display').hide();

            $('.chat_ticker').hide();
            $('.chat_pane').show();
            showChatInput();
        });


    new SimpleBar($('#pregame_messages')[0]); // eslint-disable-line no-new

    if(helpers.storageEnabled()){
        localStorage.removeItem('seenFeedbacks');
        seenFeedbacks.length = 0; // clears array
    }
    connect();
}

function goToGamePlayPage(){
    $('#main').show();
    $('#setup_page').hide();
    $('#lobby_page').hide();

    $('.nav_bar').show();
    $('.fourButtonNav').removeClass('fourButtonNav');
    if(helpers.isMobile())
        $('.chat_ticker').show();
    else
        $('.chat_ticker').hide();

    clearNavBarClasses();
    if(isTablet())
        $('body').addClass('tablet_game');
    else if(helpers.isMobile())
        $('body').addClass('mobile');
    bodySet();

    if(helpers.isMobile())
        $('#nav_tab_1 span').addClass('timerText');
    else
        $('#nav_tab_1 span').text('Info');

    $('#nav_tab_2 span').text('Role');
    $('#nav_tab_3 span').text('Recap');
    $('#nav_tab_4 span').text('Actions');
    if(!window.gameState.isOver()){
        $('#nav_tab_5 span').text('Settings');
    }else{
        $('#nav_tab_5 span').text('Exit');
        $('body').addClass('gameOver');
    }

    clearNavTabImages();
    setDayIcon();
    $('#nav_tab_2 i').addClass('fa fa-book');
    $('#nav_tab_3 i').addClass('fa fa-edit');
    $('#nav_tab_4 i').addClass('fa fa-bolt');
    $('#nav_tab_5 i').addClass('fa fa-cog');

    $('.leaveButton').unbind();
    $('.leaveButton').click(() => {
        leaveGame();
        resetGameState();
    });

    changeableDisplayClick('game_info');
    changeableDisplayClick('your_info');
    changeableDisplayClick('vote_recap');
    changeableDisplayClick('action_nav');
    $('.settings_button').unbind();
    if(!window.gameState.isOver())
        changeableDisplayClick('settings');
    else
        $('.settings_button').click(leaveGame);

    SETTINGS_KEYS.forEach(value => {
        if(helpers.storageEnabled()){
            $(`.${value}`).removeClass('selected_toggle');
            if(!soundService.defaultSound(value))
                $(`.${value}.left_toggle_border`).addClass('selected_toggle');
            else
                $(`.${value}.right_toggle_border`).addClass('selected_toggle');
        }
        $(`.${value}`).click(function(e){
            e = $(this);
            if(e.hasClass('selected_toggle'))
                return;
            $(`.${value}`).removeClass('selected_toggle');
            e.addClass('selected_toggle');

            setSettingValue(value, e.hasClass('right_toggle_border'));
            if(value.indexOf('_music_enabled') !== -1)
                if(e.hasClass('right_toggle_border'))
                    soundService.play();
                else if((value === 'day_music_enabled' && window.gameState.isDay())
                    || (value === 'night_music_enabled' && !window.gameState.isDay())){
                    soundService.paus();
                }
        });
    });

    $('.puppetClick').off('click')
        .on('click', () => {
            $('#puppetList').show();
        });


    $('#chat_ticker_pane, #chat_ticker_expander').unbind();
    $('#chat_ticker_pane').click(function(){
        const chatTab = $(`#${$(this).attr('name')}`);
        chatTab.click();
    });

    $('#chat_ticker_expander').click(() => {
        if(!isOnMain())
            return;
        hideLeftPane();
        const prevSelected = $('.selected_nav');
        prevSelected.removeClass('selected_nav');
        prevSelected.addClass('unselected_nav');
        $('.chat_pane').hide();
        $('#puppetList').hide();
        chatView.hideChatInput();
        $('#chat_tabs_wrapper').show();
        $('.chat_ticker').hide();
    });

    function visChange(){
        if(window.gameState.scrollToBottom)
            pushServerUnread($('.activeTab').attr('name'));
    }

    const visProp = getHiddenProp();
    if(visProp){
        const evtname = `${visProp.replace(/[H|h]idden/, '')}visibilitychange`;
        document.addEventListener(evtname, visChange);
    }


    $('#nav_tab_4').click();
    soundService.play();
    $('.gameHostContainers').hide();

    new SimpleBar($('#messages')[0]); // eslint-disable-line no-new
    connect();
}

function bodySet(){
    const body = $('body');
    body.removeClass('isFirstPhase');
}

function setNavSelected(e){
    $('.selected_nav').removeClass('selected_nav').addClass('unselected_nav');
    if(e)
        e.addClass('selected_nav').removeClass('unselected_nav');
}

function clearNavTabImages(){
    $('.nav_bar i').removeClass('fa-sun fa-moon fa-book fa-edit fa-bolt fa-cog fa fab fas');
    $('.nav_bar i').removeClass('fa-home fa-trophy fa-user fa-sign-in-alt fa fab fas'); // fa-book omitted
    $('.nav_bar i').removeClass('fa-arrow-left fa-cogs fa-users fa-youtube fa fab fas'); // fa-book omitted
    $('.nav_bar i').removeClass('far fa-discord fa-comments');
}

function clearNavBarClasses(){
    $('body').removeClass('tablet_home tablet_setup tablet_game mobile gameOver');
}

function hasType(key, type){
    if(type)
        return (type.indexOf(key) >= 0);

    return false;
}

function isFirstPhase(){
    if(!window.gameState.started)
        return false;
    if(window.gameState.dayNumber === 0)
        return true;
    if(!window.gameState.isDay())
        return false;
    return (window.gameState.dayNumber === 1 && window.gameState.getRuleValue('DAY_START'));
}

let receivedFirstObject = false;
async function handleObject(object){
    const funcs = [];
    delete object.name;
    if(object.event)
        return require('./services/browserEventService').handle(object);
    if(object.guiUpdate){
        const gameState = window.gameState;
        if(!gameState.setup)
            await gameService.getGameStateAndChat();
        if(gameState.profile)
            roleInfoView.refresh();

        if(object.gameStart !== undefined){
            if(object.gameStart === true && gameState.started === false){
                $('#team_catalogue_pane').empty();
                setActiveFactionRole(null);
                gameState.isStarted = true;
            }
            gameState.started = object.gameStart;

            if(gameState.started && !isOnMain()){
                funcs.push(goToGamePlayPage);
            }else if(!gameState.started && !isOnSetup()){
                goToSetupPage();
                goToSetupPage();
            }
        }

        if(object.host){
            gameState.host = object.host;
            funcs.push(refreshHost);
        }

        if(object.isObserver !== undefined){
            gameState.isObserver = object.isObserver;
            if(gameState.isObserver)
                prepObserverMode();
        }
        if(object.joinID && gameState.joinID !== object.joinID){
            gameState.joinID = object.joinID;
            setGameID();
        }
        if(object.activeTeams !== undefined)
            gameState.activeTeams = object.activeTeams;


        if(object.isPrivateInstance !== undefined){
            gameState.isPrivateInstance = object.isPrivateInstance;
            funcs.push(setPrivateInstance);
        }

        if(hasType('rules', object.type)){
            gameState.hiddenRandoms = object.hiddenRandoms;

            // checks if new team is created
            let newTeamCreated = false;
            const funcsToLoad = [];
            if(gameState.factions && !gameState.started){
                const oldFactionLength = gameState.factions.factionNames.length;
                const newFactionLength = object.factions.factionNames.length; // is colors
                if(oldFactionLength + 1 === newFactionLength){
                    newTeamCreated = true;
                    let newColor = object.factions.factionNames[newFactionLength - 1]; // is colors
                    newColor = `color${newColor.replace('#', '')}`;
                    if($('body').hasClass('teamEditor')){
                        funcsToLoad.push(teamLoader.bind(null, newColor));
                        funcsToLoad.push(() => {
                            $('body').removeClass('teamEditor1').addClass('teamEditor2');
                        });
                    }
                }else if($('body').hasClass('teamEditor')){
                    const teamEditorName = $('#teamEditorPane .title').attr('name');
                    funcsToLoad.push(teamLoader.bind(null, teamEditorName));
                }
            }
            gameState.rules = object.rules;
            gameState.factions = object.factions;
            if(gameState.activeFactionID){
                const activeFactionID = gameState.activeFactionID;
                if(activeFactionID !== RANDOMS_SELECTED_FACTION_ID
                    && !gameState.setup.factionMap[activeFactionID])
                    setActiveFaction(null);
            }

            if(!gameState.started){
                funcs.push(factionsView.refresh);
                funcs.push(refreshRolesUL);
                funcs.push(refreshEnemiesUL);
                funcs.push(factionDetailsView.refresh);
                funcs.push(rolesOverviewView.refresh);
                funcs.push(generalSettingsView.refresh);

                if(newTeamCreated && !gameState.activeFactionID)
                    funcs.push(clickLastElementInFactionUL);


                if($('body').hasClass('sheriffView')){
                    let color = getEditingFaction().color;
                    color = `color${color.replace('#', '')}`;
                    funcsToLoad.push(sheriffLoader.bind(null, color));
                }
                for(let k = 0; k < funcsToLoad.length; k++)
                    funcs.push(funcsToLoad[k]);
            }else{
                funcs.push(setLastWillContainer);
            }
        }

        if(hasType('roleInfo', object.type)){
            gameState.role = object.roleInfo;
            funcs.push(setRoleInfo);
            if(hasType('rules', object.type) && !receivedFirstObject){
                receivedFirstObject = true;
                setActiveFactionRole(gameState.profile.roleID);
                rolesOverviewView.refresh();
            }
        }

        if(hasType('graveYard', object.type)){
            gameState.graveyard = object.graveYard;
            funcs.push(setGraveYard);
        }

        if(object.isDay !== undefined){
            if(gameState.isDay() !== object.isDay){
                selected = null;
                selected2 = null;
                left = null;
                right = null;
                deadSelected = null;
                savedOpts = [null, null, null];
                setDayIcon();
            }
            refreshActionButtonText();
            if(!gameState.isDay()){
                $('#actions_pane').show();
                $('#forceEndNightButton').text('Force End Night');
            }else{
                $('#forceEndNightButton').text('Force End Day');
            }
        }

        if(object.showButton !== undefined)
            refreshActionButtonText(object.showButton);

        if(object.skipVote !== undefined){
            gameState.skipVote = object.skipVote;
            gameState.isSkipping = object.isSkipping;
        }
        if(hasType(J.activeTeams, object.type))
            gameState.activeTeams = object[J.activeTeams];

        if(hasType('playerLists', object.type)){
            if(hasType('actions', object.type))
                gameState.actions = object.actions;

            gameState.playerLists = object.playerLists;
            funcs.push(setMultiCommandButtons);
            funcs.push(refreshPlayers);
        }

        if(object.hasDayAction)
            gameState.hasDayAction = object.hasDayAction;

        if(hasType('dayLabel', object.type)){
            setDayLabel(object.dayLabel);
            gameState.dayNumber = object.dayNumber;
            $('#nav_tab_1 i').text(` ${gameState.dayNumber}`);
        }
        if(object.timer !== undefined)
            if(window.gameState.getRuleValue('HOST_VOTING') && gameState.isDay()
                && !gameState.isOver())
                gameState.timer = object.timer;
            else
                gameState.timer = object.timer;

        if(hasType('actions', object.type)){
            gameState.actions = object.actions;
            refreshActionUL();
        }

        if(hasType('voteInfo', object.type)){
            gameState.voteInfo = object.voteInfo;
            const dayNumber = gameState.dayNumber;
            const voteInfo = gameState.voteInfo;
            const voteView = gameState.voteView;
            if(voteView.day === -1){
                voteView.day = dayNumber;
                voteView.counter = voteInfo.voteCounts[dayNumber - 1].movements.length;
            }else if(voteView.day + 1 === gameState.dayNumber
                && voteInfo.voteCounts[voteView.day - 1].movements.length === voteView.counter){
                voteView.day = dayNumber;
                voteView.counter = 0;

                // trying to figure out if current votes
            }else{
                const isCurrentDay = gameState.voteView.day = dayNumber;
                if(isCurrentDay){
                    const currentDay = dayNumber;
                    const counter = voteInfo.voteCounts[currentDay - 1].movements.length;
                    if(voteView.counter + 1 === counter)
                        voteView.counter++;
                        // window.gameState.voteView.counter = Math.min(window.gameState.voteView.counter, )
                }
            }
            funcs.push(voteOverview.refresh);
        }

        if(object.playerName !== undefined){
            gameState.playerName = object.playerName;
            if(!$('#gen_rules_edit_name_pane input').is(':focus'))
                $('#gen_rules_edit_name').val(gameState.playerName);
            $('#gen_rules_edit_name').removeClass('error_text_input');
        }


        if(object.lastWill){
            gameState.lastWill = object.lastWill;
            if(!hasTeamLastWill() || $('#your_exp_pane .settings_on').hasClass('selected_toggle')){
                const lwArea = $('#last_will_area');
                if(!lwArea.val().length)
                    lwArea.val(object.lastWill);
            }
        }

        if(object.teamLastWill){
            gameState.teamLastWill = object.teamLastWill;
            if(!gameState.getRuleValue('LAST_WILL')
                || $('#your_exp_pane .settings_off').hasClass('selected_toggle')){
                const lwArea = $('#last_will_area');
                if(!lwArea.val().length)
                    lwArea.val(object.teamLastWill);
            }
        }
        if(object.setupName){
            gameState.setupName = object.setupName;
            funcs.push(generalSettingsView.refreshSetupSpinner);
        }

        if(object.ping)
            $('#ping')[0].play();

        for(let i = 0; i < funcs.length; i++)
            funcs[i]();
        return;
    }
    if(object.speechContent)
        return soundService.speak(object.speechContent);
    if(object.nodeError){
        if(object.nodeError === 'nameChange')
            $('#gen_rules_edit_name')
                .addClass('error_text_input');
        return;
    }
    if(object.warning)
        return setWarning(object.warning);
    if(object.unreadUpdate){
        const lookingAtChat = (isOnMain() && $('.activeTab')
            .attr('name') === object.unreadUpdate)
            || (isOnSetup() && $(window)
                .scrollTop() + $(window)
                .height() >= $(document)
                .height());
        if(window.gameState.scrollToBottom && lookingAtChat && $('.chat_pane')
            .is(':visible')
            && !isHidden() && object.count)
            pushServerUnread(object.unreadUpdate);
        else
            window.gameState.unreadChats[object.unreadUpdate] = object.count;
        return;
    }

    // resetchat
    if(object.chatReset){
        $('#chat_tabs').empty();
        window.gameState.chats = {};
        if(window.gameState.started)
            window.gameState.chatKeys = {};
        let chatKeys = object.chatReset;
        chatKeys = addOtherDayChats(chatKeys);

        let chatName;
        let chatObject;
        for(let c = 0; c < chatKeys.length; c++){
            chatObject = chatKeys[c];
            chatName = chatObject.chatName;
            if(chatObject.chatKey && window.gameState.started)
                window.gameState.chatKeys[chatName] = chatObject.chatKey;
            addTab(chatObject);
        }


        let element;
        if(window.gameState.started)
            element = $('#messages');
        else
            element = $('#pregame_messages');

        element.empty();
        Object.keys(window.gameState.chats).forEach(chatKey => {
            window.gameState.chats[chatKey] = {
                voteCount: -1,
                messages: [],
            };
        });

        chatService.chatQueue.length = 0;
    }

    // var feedbackSize = window.gameState.feedback.length;

    let ret; // returns where the id of the chat tab that this was added to
    if(typeof(object.message) === 'string'){
        let chat;
        if(!window.gameState.isAlive && !hasAbility('Ghost'))
            chat = 'Dead Chat';
        else
            chat = 'Clues';
        ret = addToChat({
            text: object.message,
            chat: [chat],
            day: window.gameState.dayNumber,
            isDay: window.gameState.isDay(),
        });
    }else{
        let tempMessage;
        for(let i = 0; i < object.message.length; i++){
            tempMessage = addToChat(object.message[i]);
            if(object.message[i].mID)
                pushFeedback(object.message[i]);
            if(tempMessage)
                ret = tempMessage;
        }
        if(window.gameState.started && $('.activeTab').length === 0 && !helpers.isMobile())
            if(window.gameState.isAlive === 'Ghost')
                $('#chat_tabs div').last().click();
            else
                $('#chat_tabs div:nth-last-child(2)').last().click();
        // else
    }
    if(ret){
        if(!ret[0])
            return helpers.log('bad message', object);
        const chatID = ret[0].replace(' ', '');
        const message = ret[1];
        $('#chat_ticker_pane').attr('name', chatID);
        $('#chat_ticker_new_message').empty();
        chatView.fillEleWithMessage($('#chat_ticker_new_message'), message);
    }
}

function addOtherDayChats(chatKeys){
    chatKeys.reverse();
    for(let i = window.gameState.dayNumber - 1; i > 0; i--)
        chatKeys.push({
            chatName: `Day ${i}`,
            isDay: true,
            chatType: 'fas fa-sun',
        });


    if(!helpers.isMobile())
        chatKeys.reverse(); // unreverse it
    return chatKeys;
}

function setWarningCancel(){
    if(window.gameState.warningResolve)
        window.gameState.warningResolve();
}

function setWarning(text, length){
    if(!length)
        length = 4000;
    window.gameState.warning = window.gameState.warning.then(() => {
        $('#warningPopup').stop();
        $('#warningPopup').css('opacity', 1);
        $('#warningPopup').show();
        $('#warningPopup span').text(text);

        return new Promise((resolve => {
            setTimeout(resolve, length);
            if(!window.gameState.warningResolve)
                window.gameState.warningResolve = resolve;
            $('#warningPopup').fadeOut(length);
        })).then(() => {
            window.gameState.warningResolve = null;
            $('#warningPopup').hide();
            $('#warningPopup span').text('');
        });
    });
}

function resetGame(){
    webSend({
        message: 'resetGame',
    });
}

function nextPhase(){
    editTimer(0);
}

function editTimer(seconds){
    phaseService.setTimer(seconds);
}

function hostSubmit(input){
    playerService.joinByGameID(input.val())
        .catch(setHomePopupErrorCode);
}

async function leaveGame(){
    soundService.pause();

    try{
        await playerService.leave();
    }catch(response){
        return setWarning(response.errors[0], 6000);
    }
    resetGameState();
    setTrim(NORM_TEXT_COLOR);
    window.gameState.phase = null;
    refreshActionButtonText();

    goToGlobalLobbyPage();
    helpers.setPseudoURL('/');
}

// eslint-disable-next-line no-unused-vars
function peekRolesList(){
    const o = {};
    o.message = 'getRolesList';
    o.server = false;
    webSend(o);
}

function setSubmitFunction(){
    $('form').submit(function(e){
        e = $(this);
        if(e.attr('id') === 'gameCodeInput')
            return hostSubmit(e); // i don't think this is being used anymore
    });
}


function connect(){
    if(window.socket)
        return Promise.resolve();

    if(!socketConnectingPromise)
        socketConnectingPromise = new Promise(resolve => {
            let host = window.location.origin.replace(/^http/, 'ws');
            if(!host.endsWith(PORT_NUMBER))
                host += PORT_NUMBER;
            const ws = new WebSocket(host);

            setSubmitFunction(); // only used here.  probably should go in the index startup

            ws.onmessage = msg => {
                if(!socketConnectingPromise){
                    msg = JSON.parse(msg.data);
                    handleObject(msg);
                }
            };

            ws.onopen = () => {
                requests.getBrowserAuthRequest()
                    .then(authRequest => {
                        window.socket = ws;
                        webSend(authRequest);
                        socketConnectingPromise = null;
                        resolve();
                    });
            };
            ws.onclose = () => {
                window.socket = null;
                attemptReconnect();
            };
        });

    return socketConnectingPromise;
}

function attemptReconnect(){
    setTimeout(() => {
        if(!helpers.isOnLobby())
            connect();
    }, 3000);
}

function onLogoutClick(){
    return authService.logout()
        .then(() => {
            window.socket.close();
            window.socket = null;
            goToGlobalLobbyPage();
        });
}

function onLoginClick(){
    const email = $('.emailInput').val();
    const password = $('.passwordInput').val();
    if(!email)
        return;
    authService.loginWithEmail(email, password)
        .then(() => {
            $('.login_input_style').val('');
            return onUserInitialization();
        }).catch(setHomePopupErrorCode);
}

function signup(){
    const username = $('.usernameInput').val();
    const email = $('.emailInput').val();
    const password = $('.passwordInput').val();

    if(!username)
        return;

    authService.createUserWithEmailAndPassword(email, password, username)
        .then(() => {
            $('#login_page').hide();
            $('.login_input_style').val('');
        }).catch(error => {
            setHomePopupErrorCode(error);
        });
}

function isHex(h){
    let a = parseInt(h, 16);
    a = a.toString(16).toLowerCase();
    while (a.length < 6)
        a = `0${a}`;
    return (a === h.toLowerCase());
}

function hideLeftPane(){
    $('.changeable_display').hide();
    const prevSelected = $('.selected_nav');
    prevSelected.removeClass('selected_nav');
    prevSelected.addClass('unselected_nav');
    if(helpers.isMobile() || isTablet()){
        $('#action_nav_pane').hide();
        $('#settings_pane').hide();
    }
}

function isFullScreen(){
    const doc = window.document;
    return doc.fullscreenElement
        || doc.mozFullScreenElement
        || doc.webkitFullscreenElement
        || doc.msFullscreenElement;
}

function toggleFullScreen(){ // variable was 'on'

    // const doc = window.document;
    // const docEl = doc.documentElement;
    //
    // const requestFullScreen = docEl.requestFullscreen
    //     || docEl.mozRequestFullScreen
    //     || docEl.webkitRequestFullScreen
    //     || docEl.msRequestFullscreen;
    // const cancelFullScreen = doc.exitFullscreen
    //     || doc.mozCancelFullScreen
    //     || doc.webkitExitFullscreen
    //     || doc.msExitFullscreen;
    //
    // if(on && requestFullScreen)
    //     requestFullScreen.call(docEl);
    // else if(cancelFullScreen)
    //     cancelFullScreen.call(doc);
    //
    //
    // if(isFullScreen())
    //     $('body').css('font-size', '40%');
    // else
    //     $('body').css('font-size', '100%');
}

function selectedToggle(elementID){
    return $(`#${elementID}`).hasClass('selected_toggle');
}

function hostGame(){
    // const publicSelected = !selectedToggle('togglePrivateButton');
    const hostName = loadPreviousName() || 'guest';
    const isCustom = selectedToggle('gameHost_custom');
    const isSuperCustomGame = selectedToggle('gameHost_advCustom');
    setSuperCustom(isCustom && isSuperCustomGame);
    const setupName = !isCustom && $('#gameHostBox select').val();
    return gameService.createGame(helpers.isMobile(), hostName, setupName);
}

function joinGame(){
    if($('#gameHostBox').is(':visible'))
        return;
    const publicSelected = !($('#togglePrivateButton').hasClass('selected_toggle'));
    const gameCodeInput = $('.gameCodeInput');

    if(publicSelected)
        return playerService.joinPublic();

    if(!$('#gameCodeBox').is(':visible')){
        showHomePopup(JOIN_GAME);
        loadPreviousName();
        return;
    }

    if(gameCodeInput.val().length !== 4){
        setHomePopupErrorCode('Game code needs to be 4 characters!');
        return;
    }

    const name = $('.your_name_input').val();
    if(!name)
        return setHomePopupErrorCode('Please submit a name.');

    saveInputtedGameName();

    if(!removeNonAlphaNumericCharacters(name))
        return setHomePopupErrorCode('You cannot use this name.');

    setHomePopupErrorCode(); // hides it

    playerService.joinByGameID(gameCodeInput.val())
        .catch(response => setHomePopupErrorCode(response.errors[0]));
}

function loadPreviousName(){
    if(!helpers.storageEnabled())
        return Promise.resolve();

    const prevName = localStorage.getItem('lastgameName');
    if(!prevName){
        if(user)
            return user.displayName;
        return;
    }

    user = {
        displayName: prevName,
    };
    $('#homeInput3').val(prevName);

    return prevName;
}

function saveInputtedGameName(){
    if(!helpers.storageEnabled())
        return;

    let name;
    if(helpers.isOnLobby())
        name = $('#homeInput3').val();
    else if(isOnSetup())
        name = $('#gen_rules_edit_name').val();
    else
        return;

    localStorage.setItem('lastgameName', name);
}


function showHomePopup(mode){
    clearGameHostPopup();
    $('#home_pane').show();
    $('#gameCodeBox').show();
    $('#gameCodeError').slideUp();

    if(mode === SIGN_UP){
        $('.usernameInput').show();
        $('#homePopUpSubmit').text('Sign Up');
    }else{
        $('.usernameInput').hide();

        if(mode === LOGIN)
            $('#homePopUpSubmit').text('Login');
        else // join
            $('#homePopUpSubmit').text('Join');
    }

    if(mode === JOIN_GAME){
        $('.gameCodeInput').attr('placeHolder', 'Enter game code');
        if(helpers.isLoggedIn() && loadPreviousName()){
            $('.your_name_input').hide();
        }else{
            $('.your_name_input').show();
            $('.your_name_input').attr('placeHolder', 'Enter your name');
        }
        $('.passwordInput').attr('type', 'text');
    }else{
        $('.usernameInput').attr('placeHolder', 'Enter display name');
        $('.emailInput').attr('placeHolder', 'Enter email');
        $('.passwordInput').attr('placeHolder', 'Enter password');
        $('.passwordInput').attr('type', 'password');
    }

    if(mode === LOGIN)
        $('#closeHomePopUp').text('Sign Up');
    else
        $('#closeHomePopUp').text('Back');
}

function setHomePopupErrorCode(errorText){
    const gce = $('#gameCodeError');
    if(!errorText){
        gce.slideUp();
    }else{
        gce.text(errorText);
        gce.slideDown();
    }
}


function setGameHostDescription(attrs){
    $('#gameHost_info').empty();
    for(let i = 0; i < attrs.length; i++)
        $('<li>').text(` - ${attrs[i]}`).appendTo($('#gameHost_info'));

    new SimpleBar($('#gameHost_info')[0]); // eslint-disable-line no-new
}

function showGameHostPopup(customLevel){
    let texts;
    if(customLevel === PRESET){
        $('.chooseAdvanced').hide();
        $('#game_host_preset_list').show();
        setupService.getSetups()
            .then(setups => {
                const firstKey = $('#game_host_preset_list').val();
                if($.isEmptyObject(setups))
                    return;
                texts = setups[firstKey].description;
                setGameHostDescription(texts);
            });
        return;
    }
    $('.chooseAdvanced').show();
    $('#game_host_preset_list').hide();
    if(customLevel === BASIC_CUSTOM)
        texts = ['For a quick custom game setup'];
    else
        texts = [
            'For a lengthier custom game setup',
            'Not recommended for players new to Narrator Mafia, nor for quick game setup',
        ];
    setGameHostDescription(texts);
}

function setTitle(x){
    document.title = x;
}

function getNormalTitle(){
    const url = window.location.href;
    if(url.indexOf('localhost') !== -1)
        return 'Dev Narrator';
    return 'The Narrator';
}

function resetTitle(){
    setTitle(getNormalTitle());
}

function setMiniIconBlue(){
    $('.miniIcon').addClass('miniIconHover');
    $('#miniIconVisual').attr('src', 'img/iconLogoLightBlue.png');
}

$(document).ready(() => {
    window.socket = null;
    $(document).on('mouseenter mouseleave', '.miniIcon', e => {
        e.stopPropagation();
        if(e.type === 'mouseenter'){
            setMiniIconBlue();
        }else{
            $('.miniIcon').removeClass('miniIconHover');
            $('#miniIconVisual').attr('src', 'img/iconLogoWhite.png');
        }
    });

    $('.miniIcon').mouseover(() => {
        $('.miniIcon').addClass('miniIconHover');
    });

    setTitle(getNormalTitle());
    setTimeout(() => {
        window.scrollTo(0, 1);
    }, 100);

    $('#chat_tabs').empty();
    $('#login_button').click(onLoginClick);
    $('#signup_button').click(signup);

    setupService.getSetups();

    $('#gameHost_close, #gameHost_background').click(() => {
        $('.gameHostContainers').slideUp();
        $('#logo').show();
    });

    $('#warningPopup i').click(setWarningCancel);

    $('#joinButton').click(joinGame);
    $('#hostPublicButton').click(() => {
        $('.gameHostContainers').slideDown();
        $('#logo').hide();
    });

    $('.presetCustom').click(function(){
        const clicked = $(this);
        if(clicked.hasClass('selected_toggle'))
            return;

        $('.presetCustom').removeClass('selected_toggle');
        clicked.addClass('selected_toggle');

        let mode = PRESET;
        if(clicked.attr('id') === 'gameHost_custom')
            mode = BASIC_CUSTOM;

        showGameHostPopup(mode);
    });

    $('.chooseAdvanced').click(function(){
        const clicked = $(this);
        if(clicked.hasClass('selected_toggle'))
            return;

        $('.chooseAdvanced').removeClass('selected_toggle');
        clicked.addClass('selected_toggle');

        let mode = BASIC_CUSTOM;
        if(clicked.attr('id') === 'gameHost_advCustom')
            mode = ADV_CUSTOM;

        showGameHostPopup(mode);
    });

    $('#gameHost_submit').click(() => {
        authService.ensureLoggedIn()
            .then(connect).then(hostGame);
    });

    $('.gameEntryToggle').click(function(){
        const clicked = $(this);
        if(clicked.hasClass('selected_toggle'))
            return;
        if(clicked.attr('id') === 'togglePublicButton'){
            const warningText = "You won't find any public games right now. "
                + 'Check the forums for private games!';
            return setWarning(warningText, 6000);
        }
        if(!helpers.isLoggedIn() && clicked.attr('id') === 'togglePublicButton'){
            setWarning('Cannot host or join public games as a guest.  Sign up to continue.');
            return;
        }
        $('.gameEntryToggle').removeClass('selected_toggle');
        clicked.addClass('selected_toggle');
    });


    $('#closeHomePopUp').click(function(){
        if($(this).text() === 'Sign Up'){
            showHomePopup(SIGN_UP);
            return;
        }if($('#homePopUpSubmit').text() === 'Sign Up'){
            showHomePopup(LOGIN);
            return;
        }
        $('#gameCodeBox').hide();
        $('.gameCodeInputFields').val('');

        $('#nav_tab_1').removeClass('unselected_nav').addClass('selected_nav');
        $('#nav_tab_5').addClass('unselected_nav').removeClass('selected_nav');
    });
    $('#homePopUpSubmit').click(function(e){
        e = $(this);
        if(e.text() === 'Join')
            joinGame();
        else if(e.text() === 'Login')
            onLoginClick();
        else // sign up
            signup();
    });
    $('#homeInput3').unbind();
    $('#homeInput3').keyup(event => {
        if(parseInt(event.keyCode, 10) === 13)
            $('#homePopUpSubmit').click();
        return false;
    });
    $('#homeInput2').unbind();
    $('#homeInput2').keyup(event => {
        if(parseInt(event.keyCode, 10) === 13)
            $('#homePopUpSubmit').click();
        return false;
    });

    $('.emp_sort_button').click(function(e){
        e = $(this);
        if(e.hasClass('selected_toggle'))
            return;
        $('.emp_sort_button').removeClass('selected_toggle');
        e.addClass('selected_toggle');

        queryEmpData()
            .then(empData => {
                const empSelected = empData.list[getEmpIndex()];
                empData.list.sort((x, y) => {
                    x = empData.roles[x];
                    y = empData.roles[y];
                    const xwinRate = (x.wins / (x.appearances || 1));
                    const ywinRate = (y.wins / (y.appearances || 1));

                    if(xwinRate === ywinRate || e.attr('id') !== 'emp_global_sort')
                        return x.roleName.localeCompare(y.roleName);
                    return ywinRate - xwinRate;
                });

                for(let i = 0; i < empData.list.length; i++)
                    if(empData.list[i] === empSelected){
                        setEmpIndex(i);
                        break;
                    }

                refreshEmporium(empData);
            });
    });

    $('#emporium_pane .emporiumChanger').click(function(e){
        e = $(this);
        const picID = e.attr('id');
        const lastChar = picID[picID.length - 1];

        const curIndex = getEmpIndex();
        setEmpIndex(curIndex - 3 + parseInt(lastChar, 10));
        queryEmpData().then(refreshEmporium);
    });

    $('.leaderboardChanger').click(function(e){
        e = $(this);
        loadLeaderboard(e.attr('name'));
    });

    function profileEditName(){
        if($('body').hasClass('profile_editName'))
            return;
        $('body').addClass('profile_editName');
        $('#profile_displayNameEdit').focus();
    }

    $('#profile_editName').click(profileEditName);
    $('#displayNameSpan').dblclick(profileEditName);

    $('#profile_role_selector').change(() => {
        let bName = $('#profile_role_selector option:selected').attr('value');
        if(bName === 'general')
            bName = null;


        loadProfileView(user.displayName, bName);
    });
    setPlot(1, 1, 'profile_pie_container');

    if(helpers.isMobile() && $(document).height() / $(document).width() < 1.5)
        $('body').addClass('keyboardUp');


    $(window).resize(() => {
        if($('#profile_pane').is(':visible')){
            let bName = $('#profile_role_selector option:selected').attr('value');
            if(bName === 'general')
                bName = null;


            loadProfileView(user.displayName, bName);
        }

        if(helpers.isMobile() && $(document).height() / $(document).width() < 1.5)
            $('body').addClass('keyboardUp');
        else
            $('body').removeClass('keyboardUp');
    });

    $('#profile_pane .logout_button').click(onLogoutClick);

    $('.closeTeamEditor').click(closeTeamEditor);

    $('#newTeamButton').click(() => {
        if(!window.gameState.isHost())
            return;
        $('body').addClass('teamEditor teamEditor1');
        $('#newTeamColor').val('');
        $('#newTeamName').val('');
        $('#teamDescription').val('');
        $('.teamEditorInput, #teamDescription').unbind();
        $('.teamEditorWidget').prop('disabled', false);
        $('#teamDelete').hide();
    });

    $('#tieBreakerButton').click(() => {
        $('body').addClass('teamEditor tieBreaker');
        teamLoader(null);
        $('#teamDelete').hide();
    });

    $('#teamSubmit').click(() => {
        if(window.gameState.isHost() && !$('#teamEditorPane .title').attr('name')){
            const newColor = $('#newTeamColor');
            const newTeam = $('#newTeamName');

            let color = newColor.val();
            const team = newTeam.val();
            if(color.length === 0 || team.length === 0)
                return;
            if(color[0] !== '#')
                color = `#${color}`;

            if(color.length !== 7 && color.length !== 4){
                setWarning('RGB codes are typically 6 characters long.');
                return;
            }
            if(color.length === 4)
                color = color[0] + color[1] + color[1] + color[2] + color[2] + color[3] + color[3];

            if(!isHex(color.substring(1))){
                setWarning("This isn't in RGB format.");
                return;
            }
            const description = $('#teamDescription').val();

            const o = {};
            o.message = 'addTeam';
            o.color = color.toUpperCase();
            o.teamName = team;
            o.teamDescription = description;
            webSend(o);
        }else if($('body').hasClass('teamEditor1')){
            $('body').removeClass('teamEditor1').addClass('teamEditor2');
            $('#teamDelete').hide();
        }else if($('body').hasClass('teamEditor2')){
            $('body').removeClass('teamEditor2').addClass('teamEditor3');
        }else if($('body').hasClass('teamEditor3')){
            $('body').removeClass('teamEditor3').addClass('teamEditor4');
        }else if($('body').hasClass('teamEditor4')){
            $('body').removeClass('teamEditor4').addClass('tieBreaker');
        }
    });

    $('#teamBack').click(() => {
        if($('body').hasClass('teamEditor2')){
            $('body').removeClass('teamEditor2').addClass('teamEditor1');
            if(window.gameState.isHost() && getEditingFaction().isEditable && isSuperCustom())
                $('#teamDelete').show();
            else
                $('#teamDelete').hide();
        }else if($('body').hasClass('teamEditor3')){
            $('body').removeClass('teamEditor3').addClass('teamEditor2');
        }else if($('body').hasClass('teamEditor4')){
            $('body').removeClass('teamEditor4').addClass('teamEditor3');
        }
    });

    $('.teamBasic').click(() => {
        $('body')
            .removeClass('teamEditor2 teamEditor3 teamEditor4')
            .addClass('teamEditor1');
        const editingFaction = getEditingFaction();
        const isEditable = editingFaction.isEditable;
        if(window.gameState.isHost() && editingFaction && isEditable && isSuperCustom())
            $('#teamDelete').show();
        else
            $('#teamDelete').hide();
    });


    $('.teamRules').click(() => {
        if(!getEditingFaction())
            return;
        $('body').removeClass('teamEditor1 teamEditor3 teamEditor4').addClass('teamEditor2');
        $('#teamDelete').hide();
    });


    $('.teamRoles').click(() => {
        if(!getEditingFaction())
            return;
        $('body').removeClass('teamEditor1 teamEditor2 teamEditor4').addClass('teamEditor3');
        $('#teamDelete').hide();
    });


    $('.teamAllies').click(() => {
        if(!getEditingFaction())
            return;
        $('body').removeClass('teamEditor1 teamEditor2 teamEditor3').addClass('teamEditor4');
        $('#teamDelete').hide();
    });


    // #game_info_pane


    $('.chat_input').keyup(function(e){
        if(parseInt(e.keyCode, 10) !== 13)
            return;
        e = $(this);
        let key;
        if(helpers.isOnLobby()){
            key = true;
        }else if(isOnMain()){
            const activeChat = $('.activeTab').attr('name');
            key = window.gameState.chatKeys[activeChat];
        }else{
            key = 'everyone';
            window.gameState.unreadChats = {};
        }

        const puppetText = $('#puppetText').text();
        const role = window.gameState.role;
        if(!key && hasPuppets())
            key = $('#puppetText').text();
        else if(role && puppetText !== role.displayName && puppetText)
            key = puppetText;


        if(e.val().length === 0 || window.gameState.isObserver || !key){
            e.val('');
            return false;
        }

        const message = `say ${key} ${e.val()}`;

        const o = {};
        o.action = false;
        o.message = message;
        webSend(o);

        e.val('');
        return false;
    });

    $('.lw_toggle').click(function(e){
        e = $(this);
        if(e.hasClass('selected_toggle') || $('#last_will_area').is(':visible'))
            return;
        $('.selected_toggle').removeClass('selected_toggle');
        e.addClass('selected_toggle');

        if(e.text() === 'Public')
            $('#last_will_area').val(window.gameState.lastWill || '');
        else
            $('#last_will_area').val(window.gameState.teamLastWill || '');
    });

    $('#last_will_button').click(function(e){
        e = $(this);
        const tArea = $('#last_will_area');
        if(tArea.is(':visible')){
            tArea.hide();
            e.text('Last Will');

            const lastWillText = tArea.val();

            let m;
            if($('#your_exp_pane').hasClass('lw_double'))
                if($('#your_exp_pane .selected_toggle').text() === 'Public'){
                    m = 'lastWill';
                }else{
                    m = 'teamLastWill';
                }
            else if(hasTeamLastWill())
                m = 'teamLastWill';
            else
                m = 'lastWill';


            window.gameState[m] = lastWillText;

            const o = {
                willText: lastWillText,
                message: m,
            };
            webSend(o);
        }else{
            tArea.show();
            e.text('Save');
        }
    });

    $('#end_night_button').on('click', () => {
        const button = $('#end_night_button');
        if(window.gameState.isOver())
            return leaveGame();
        if(window.gameState.isDay()){
            if(left && selected.size() && window.gameState.getRuleValue('HOST_VOTING'))
                return submitHostVote();

            if(window.gameState.role.roleName === 'Mayor')
                webSend({
                    message: 'Reveal',
                });
            else
                webSend({
                    message: 'Burn',
                });

            button.hide();
        }else{
            webSend({
                message: 'end night',
            });
            const playerName = window.gameState.profile.name;
            const { playerMap } = window.gameState;
            const player = playerMap[playerName];
            player.endedNight = !player.endedNight;
            if(window.gameState.endedNight())
                setButtonText("I'm not ready");
            else
                setButtonText('Skip to Day');
            refreshPlayers();
        }
    });

    $('#restartGameButton').click(resetGame);
    $('#forceEndNightButton').click(() => {
        if(!window.gameState.isDay() || !window.gameState.getRuleValue('HOST_VOTING'))
            nextPhase();
        else
            actionService.submit({
                command: 'vote',
                'skip day': true,
                unvote: false,
                targets: Object.values(window.gameState.playerMap)
                    .filter(player => !player.flip)
                    .map(player => player.name),
            });


        $('#nav_tab_1').click();
    });

    onUserInitialization();

    chatView.refresh();

    $('.tablet_inviteLink, .inviteLink').click(() => {
        const el = document.createElement('textarea');
        const str = helpers.getBaseURL() + window.gameState.gameID;
        el.value = str;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        document.execCommand('copy');

        setWarning('Game link copied to clipboard!');
    });

    $('#setup_gen_settings_pane .fa-plus-square').click(function(){
        const e = $(this).parent();
        const ruleID = e.attr('id').toUpperCase();
        setupModifierService.increaseGeneralSetting(ruleID);
    });
    $('#setup_gen_settings_pane .fa-minus-square').click(function(){
        const e = $(this).parent();
        const ruleID = e.attr('id').toUpperCase();
        setupModifierService.decreaseGeneralSetting(ruleID);
    });

    new SimpleBar($('#chat_tabs')[0]); // eslint-disable-line no-new

    $('#userPopupBackdrop').click(playerOverview.hide);

    $('#userPopupRepick').click(() => {
        moderatorService.repick($('.userPopupHeader').text());
    });

    $('#userPopupPing').click(function(){
        ping($('.userPopupHeader').text());
        $('#userPopupRepick').css('bottom', $(this).css('bottom'));
        $(this).hide();
    });

    $('#userPopupKick').click(() => {
        playerService.kick($('.userPopupHeader').text());
    });

    setTimer();

    $('body').click(() => {
        if(!isFullScreen()){
            if(helpers.isMobile())
                toggleFullScreen(true);
        }else if(!helpers.isMobile()){
            toggleFullScreen(false);
        }
        if(!soundService.isPlaying())
            soundService.speak(' ');

        soundService.play();
    });
});

function onUserInitialization(){
    return gameService.recoverWithAuthTokenInURL()
        .then(() => {
            if(!window.gameState.isInGame())
                return gameService.recoverWithPreviousLogIn();
        }).then(() => {
            if(!window.gameState.isInGame())
                return playerService.joinByGameID()
                    .catch(responseErr => {
                        helpers.setPseudoURL('/');
                        if(responseErr && responseErr.errors)
                            setWarning(responseErr.errors[0]);
                    });
        }).then(() => {
            if(!window.gameState.isInGame())
                return authService.ensureLoggedIn()
                    .then(goToGlobalLobbyPage());
        });
}

function setSettingValue(key, val){
    if(helpers.storageEnabled())
        localStorage.setItem(key, val);
    else
        window.gameState[key] = val;
}

function changeableDisplayClick(elementID){
    $(`.${elementID}_button`).unbind();
    $(`.${elementID}_button`).click(() => {
        hideLeftPane();
        $(`.${elementID}_button`).removeClass('unselected_nav');
        $(`.${elementID}_button`).addClass('selected_nav');
        $(`#${elementID}_pane`).show();

        chatView.hideChatElements();

        // remanent of tab5click
        $('#gameCodeBox').hide();
        $('.gameCodeInputFields').val('');
    });
}

function closeTeamEditor(){
    const classesToRemove = 'teamEditor teamEditor1 teamEditor2 teamEditor3 '
        + 'teamEditor4 tieBreaker sheriffView';
    $('body').removeClass(classesToRemove);
    $('#teamEditorPane .title').attr('name', '');
}

function getFeedbackMinimizationToArea(mType){
    if(helpers.isMobile()){
        if(mType === 'DeathAnnouncement')
            return $('#nav_tab_1');
        if(mType === 'Feedback' || mType === 'SnitchAnnouncement' || mType === 'Happening')
            return $('#chat_ticker_expander_icon');
    }else{
        if(mType === 'Feedback' || mType === 'SnitchAnnouncement' || mType === 'Happening'){
            const ret = $('#Clues');
            if(ret.length)
                return ret;
            return $('#DeadChat');
        }if(mType === 'DeathAnnouncement')
            return $('#nav_tab_1');
    }

    return $('#nav_tab_2');
}

let feedbackPromise = Promise.resolve();

function minimizeFeedback(top, mID, mType){
    const options = {
        to: {
            width: 0,
            height: 0,
        },
        queue: false,
    };

    const toArea = getFeedbackMinimizationToArea(mType);
    const toX = toArea.offset().left + (toArea.width() / 2);
    const toY = toArea.offset().top + (toArea.height() / 2);

    const fromX = top.offset().left;
    const fromY = top.offset().top;

    feedbackPromise = feedbackPromise.then(() => new Promise((resolve => {
        top.effect('size', options, 700, resolve);
        top.animate({
            left: `+=${toX - fromX}`,
            top: `+=${toY - fromY}`,
            queue: false,
        }, 700);
    }))).then(() => {
        top.remove();

        if(helpers.storageEnabled()){
            let seen = localStorage.getItem('seenFeedbacks');
            if(!seen)
                seen = [];
            else
                seen = JSON.parse(seen);
            seen.push(mID);
            localStorage.setItem('seenFeedbacks', JSON.stringify(seen));
        }

        if(!$('.newFeedbackPopup').length)
            $('#feedbackBackdrop').hide();
    });
}

function pushRoleCardPopup(){
    if(!window.gameState.started)
        return;
    let bullets = ['Game has started.', 'Press \'Next\' when you\'re ready to see your role.'];
    getFeedbackPopup('Game has started!', 'citizen', bullets, 0, null);

    let rolePic;

    try{
        rolePic = window.gameState.profile.roleCard.abilities[0].command;
    }catch(err){
        rolePic = 'citizen';
    }

    bullets = roleInfoView.getRoleCardDetails();
    const { profile } = window.gameState;
    const teamName = window.gameState.setup.factionMap[profile.roleCard.factionID].name;
    const roleName = profile.roleCard.roleName;
    const title = `${teamName} ${roleName}`;
    getFeedbackPopup(title, rolePic, bullets, -1, null); // change -1 to roleCardUpdateCount
}

function pushFeedback(message){
    if(message.messageType === 'Happening')
        return;
    let title = '';
    if(message.isDay)
        title += 'Day';
    else
        title += 'Night';
    title += ' ';
    if(!message.isDay && message.messageType === 'Feedback')
        title += (message.day - 1);
    else
        title += message.day;

    const bullets = [message.text];
    if(message.extras)
        for(let i = 0; i < message.extras.length; i++)
            bullets.push(message.extras[i]);


    getFeedbackPopup(title, message.pic || 'citizen', bullets, message.mID, message.messageType);
}

function getFeedbackPopup(title, picture, bullets, mID, mType){
    if(storageService.getFeedbackWasSeen(mID))
        return;

    if(!roleImages.contains(picture))
        picture = 'citizen';

    if(seenFeedbacks.indexOf(mID) !== -1)
        return;
    seenFeedbacks.push(mID);
    $('#feedbackBackdrop').show();
    $('.feedbackNext').text('Next');
    const card = $('<div>').addClass('newFeedbackPopup');
    const header = $('<h1>').addClass('newFeedbackHeader').text(title);
    const pic = $('<img>')
        .addClass('feedbackPic')
        .attr('src', `rolepics/${picture}.png`)
        .attr('alt', 'Feedback pic');
    const bulletHolder = $('<ul>').addClass('newFeedbackContents');
    const doneButton = $('<button>').addClass('feedbackNext').text('Done');

    doneButton.click(() => {
        minimizeFeedback(card, mID, mType);
    });

    if(!bullets)
        bullets = ['really really really long test bullet point', 'shouldn\'t be on production'];


    let bullet;
    for(let i = 0; i < bullets.length; i++){
        bullet = bullets[i];
        if(!bullet)
            continue;
        if(typeof(bullet) === 'object'){
            let ret = '';
            for(let j = 0; j < bullet.length; j++){
                if(!j)
                    ret += ' ';
                const part = bullet[j];
                if(typeof(part) === 'string'){
                    ret += part;
                }else{
                    const ht = $('<span>');
                    ht.css('color', part.color);
                    ht.text(part.text);
                    ret += ht[0].outerHTML;
                }
            }
            bullet = ret;
        }
        bulletHolder.append($('<li>').html(bullet));
    }

    card.append(header).append(pic).append(bulletHolder).append(doneButton);

    card.insertAfter($('#feedbackBackdrop'));

    return card;
}

module.exports = {
    connect,
    handleObject,
    webSend,
    loadPreviousName,
    setGameHostDescription,

    RANDOMS_SELECTED_FACTION_ID,

    closeTeamEditor,
    createRuleElement,
    isHostsSetup,
    isSuperCustom,
    goToGamePlayPage,
    goToGlobalLobbyPage,
    goToSetupPage,
    hideLeftPane,
    leaveGame,
    populateUL,
    prepObserverMode,
    pushRoleCardPopup,
    refreshActionButtonText,
    refreshEnemiesUL,
    refreshHost,
    refreshRolesUL,
    refreshPlayers,
    saveInputtedGameName,
    setActiveFaction,
    setActiveFactionRole,
    setActiveHidden,
    setImage,
    setPlot,
    setSettingValue,
    setSuperCustom,
    SETTINGS_KEYS,
    setWarning, // also known as setToast maybe?
    showRoleInfo,
    teamLoader,
};

window.addBots = playerService.addBots;
window.handleObject = handleObject;
window.removeBots = removeBots;
window.editTimer = editTimer;
window.soundService = soundService;
window.storageService = storageService;
window.resetSetup = setupService.resetSetup;
