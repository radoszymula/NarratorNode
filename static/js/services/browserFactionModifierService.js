/* eslint-env browser */
const requests = require('../util/browserRequests');
const factionsView = require('../views/factionsView');


async function update(value, modifierName, factionID){
    const args = {
        value,
        name: modifierName,
    };
    const serverResponse = await requests.put(`factions/${factionID}/modifiers`, args);
    const faction = window.gameState.setup.factionMap[factionID];
    faction.modifiers.forEach(modifier => {
        if(modifier.name === modifierName)
            modifier.value = serverResponse.response.value;
    });
    factionsView.refresh();
}

module.exports = {
    update,
};
