const requests = require('../util/browserRequests');


function addObserverWithJoinID(joinID){
    return requests.post('observers', { joinID });
}

module.exports = {
    addObserverWithJoinID,
};
