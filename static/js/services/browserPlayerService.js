/* eslint-env jquery, browser */
const helpers = require('../util/browserHelpers');
const requests = require('../util/browserRequests');

const playerOverview = require('../views/playerOverview');
const playerView = require('../views/playerView');


async function addBots(botCount){
    try{
        const serverResponse = await requests.post('players/bots', { botCount });
        onPlayerAdd({
            players: serverResponse.response.players,
            setup: serverResponse.response.setup,
        });
    }catch(err){
        require('../index').setWarning(err.errors[0]);
    }
}

async function getActiveUserIDs(gameID){
    try{
        const serverResponse = await requests.get(
            `channels/browser/activeUserIDs?game_id=${gameID}`,
        );
        return new Set(serverResponse.response);
    }catch(err){
        return new Set();
    }
}

async function joinByGameID(joinID){
    joinID = joinID || window.location.pathname.substring(1);
    if(!looksLikeGameID(joinID))
        return Promise.reject();

    const authService = require('./browserAuthService');
    const gameService = require('./browserGameService');
    const index = require('../index');

    const playerName = index.loadPreviousName() || 'guest';

    await authService.ensureLoggedIn();
    const serverResponse = await requests.post('players', { joinID, playerName });
    return gameService.recoverWithPreviousLogIn(serverResponse.response);
}

async function kick(playerName){
    const player = window.gameState.playerMap[playerName];
    if(!player || !player.userID)
        return;
    const serverResponse = await requests.delete(`players/kick?playerID=${player.userID}`);
    onPlayerExit({
        host: serverResponse.response.host,
        playerName: player.name,
        setup: serverResponse.response.setup,
        userID: player.userID,
    });
    playerOverview.hide();
}

function leave(){
    return requests.delete('players');
}

function joinPublic(){
    // TODO
}

function onKick(){
    const index = require('../index');
    index.goToGlobalLobbyPage();
    index.setWarning('You\'ve been kicked from the lobby!');
    if(window.socket){
        window.socket.close();
        window.socket = null;
    }
    helpers.setPseudoURL('');
}

function onPhaseEndBid(phaseEndBidObj){
    phaseEndBidObj.players.forEach(player => {
        window.gameState.playerMap[player.name].endedNight = player.endedNight;
    });
    if(window.gameState.endedNight())
        require('../index').refreshPlayers();
}

function onPlayerAdd(playerAddObj){
    savePlayers(playerAddObj.players);
    require('./browserSetupService').postSetupChangeRefresh(playerAddObj.setup);
    // not adding userID to gameState.users yet because game doesn't use users
    playerView.refresh();
}

function onPlayerExit(playerExitObj){
    delete window.gameState.playerMap[playerExitObj.playerName];
    const oldHost = window.gameState.host;
    const newHost = playerExitObj.host;
    if(oldHost.id !== newHost.id)
        require('./browserModeratorService').onHostChange({
            host: newHost,
        });
    else
        playerView.refresh();
    require('./browserSetupService').postSetupChangeRefresh(playerExitObj.setup);
    playerOverview.hideIfIs(playerExitObj.playerName);
}

function onPlayerStatusChange(object){
    const { activeUserIDs } = window.gameState;
    if(object.isActive)
        activeUserIDs.add(object.userID);
    else
        activeUserIDs.delete(object.userID);
    const changedPlayer = Object.values(window.gameState.playerMap)
        .find(player => player.userID === object.userID);
    if(!changedPlayer)
        return;
    const targetable = playerView.getTargetable(changedPlayer.name);
    if(targetable)
        targetable.refreshActivityMarking();
}

function savePlayers(players){
    window.gameState.playerMap = {};
    players.forEach(player => {
        window.gameState.playerMap[player.name] = player;
    });
}

module.exports = {
    addBots,
    getActiveUserIDs,
    joinByGameID,
    joinPublic,
    kick,
    leave,
    onKick,
    onPhaseEndBid,
    onPlayerAdd,
    onPlayerExit,
    onPlayerStatusChange,
    savePlayers,
};

function looksLikeGameID(url){
    if(!url)
        return false;
    return url.length === 4 && helpers.isAlpha(url);
}
