/* eslint-env browser */
const requests = require('../util/browserRequests');

const factionDetailsView = require('../views/factionDetailsView');
const factionsEditorView = require('../views/factionsEditorView');
const factionsView = require('../views/factionsView');
const rolesView = require('../views/rolesView');
const versatileOverview = require('../views/versatileOverview');
const setupHiddensView = require('../views/setupHiddensView');


async function deleteFaction(){
    const factionID = window.gameState.activeFactionID;
    await requests.delete(`factions/${factionID}`);
    window.gameState.activeFactionID = null;
    window.gameState.activeFactionRoleID = null;
    const { setup } = window.gameState;
    const deletedFactionRoleIDs = [];
    Object.values(window.gameState.setup.factionRoleMap).forEach(factionRole => {
        if(factionRole.factionID !== factionID)
            return;
        deletedFactionRoleIDs.push(factionRole.id);
        delete setup.factionRoleMap[factionRole.id];
    });
    Object.values(window.gameState.setup.hiddenMap).forEach(hidden => {
        hidden.factionRoleIDs = hidden.factionRoleIDs
            .filter(factionRoleID => !deletedFactionRoleIDs.includes(factionRoleID));
        Object.values(hidden.factionRoleMap).forEach(factionRole => {
            if(deletedFactionRoleIDs.includes(factionRole.id))
                delete hidden.factionRoleMap[factionRole.id];
        });
    });
    delete window.gameState.setup.factionMap[factionID];
    factionsEditorView.hide();
    factionsView.refresh();
    setupHiddensView.refresh();
}

function openFirst(){
    const factionIDs = Object.keys(window.gameState.setup.factionMap);
    if(!factionIDs.length)
        return;
    const index = require('../index');
    const factionID = parseInt(factionIDs[0], 10);
    index.setActiveFaction(factionID);
    factionDetailsView.refresh();

    const factionRoleIDs = Object.keys(window.gameState.setup.factionRoleMap);
    if(!factionRoleIDs.length)
        return factionsView.refresh();

    const factionRoleID = parseInt(factionRoleIDs[0], 10);
    index.setActiveFactionRole(factionRoleID);
    factionsView.refresh();
    versatileOverview.refresh();
    rolesView.refresh();
}

module.exports = {
    delete: deleteFaction,
    openFirst,
};
