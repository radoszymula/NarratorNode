/* eslint-env browser */
const requests = require('../util/browserRequests');


const chatQueue = [];

async function getChats(){
    const serverResponse = await requests.get('chats');
    return serverResponse.response;
}

function setActiveChat(specifiedChat){
    chatQueue.length = 0;
    window.gameState.chats[specifiedChat].messages
        .forEach(message => chatQueue.push(message));
}

module.exports = {
    chatQueue,
    getChats,
    setActiveChat,
};
