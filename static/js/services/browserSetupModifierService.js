/* eslint-env browser */
const requests = require('../util/browserRequests');

const generalSettingsView = require('../views/generalSettingsView');


const MINITE_MODIFIERS = ['DAY_LENGTH', 'NIGHT_LENGTH'];

async function decreaseGeneralSetting(name){
    let { value } = window.gameState.setup.setupModifiers[name];
    if(MINITE_MODIFIERS.includes(name))
        value -= 60;
    else
        value -= 1;
    await update(name, value);
    generalSettingsView.refresh();
}

async function increaseGeneralSetting(name){
    let { value } = window.gameState.setup.setupModifiers[name];
    if(MINITE_MODIFIERS.includes(name))
        value += 60;
    else
        value += 1;
    await update(name, value);
    generalSettingsView.refresh();
}

async function update(name, value){
    const args = { name, value };

    const browserResponse = await requests.put('setupModifiers', args);
    name = browserResponse.response.name;
    window.gameState.setup.setupModifiers[name].value = browserResponse.response.value;
}

module.exports = {
    decreaseGeneralSetting,
    increaseGeneralSetting,
    update,
};
