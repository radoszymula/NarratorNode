/* eslint-env browser */
const requests = require('../util/browserRequests');


async function submit(action){
    if(typeof(action) === 'string')
        action = { message: action };
    const index = require('../index');
    let serverResponse;
    try{
        serverResponse = await requests.put('actions', action);
    }catch(err){
        return index.setWarning(err.errors[0]);
    }
    const { response } = serverResponse;
    window.gameState.actions = response.actions;
    window.gameState.isSkipping = response.isSkipping;
    if(response.voteInfo)
        window.gameState.voteInfo = response.voteInfo;
    index.refreshPlayers();
}

async function cancel(actionIndex, command){
    const index = require('../index');
    try{
        await requests.delete(`actions?actionIndex=${actionIndex}&command=${command}`);
    }catch(err){
        return index.setWarning(err.errors[0]);
    }
    window.gameState.actions.actionList.splice(actionIndex, 1);
    require('../index').refreshPlayers();
}

module.exports = {
    cancel,
    submit,
};
