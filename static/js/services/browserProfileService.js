/* eslint-env browser */
const requests = require('../util/browserRequests');


async function get(){
    const serverResponse = await requests.get('profiles');
    return serverResponse.response;
}

module.exports = {
    get,
};
