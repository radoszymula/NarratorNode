/* eslint-env jquery, browser */
const backupStorage = {};

function getFeedbackWasSeen(feedbackID){
    const feedbacks = getStorage('seenFeedbacks') || [];
    return feedbacks.includes(feedbackID);
}

function pushSeenFeedback(feedbackID){
    const feedbacks = getStorage('seenFeedbacks') || [];
    feedbacks.push(feedbackID);
    saveStorage('seenFeedbacks', feedbacks);
}

module.exports = {
    getFeedbackWasSeen,
    pushSeenFeedback,
};

function getStorage(storageKey){
    let storageValue;
    if(storageEnabled()){
        storageValue = localStorage.getItem(storageKey);
        if(storageValue)
            return JSON.parse(storageValue);
    }else{
        return backupStorage[storageKey];
    }
}

function saveStorage(storageKey, storageValue){
    if(storageEnabled())
        localStorage.setItem(storageKey, JSON.stringify(storageValue));
    else
        backupStorage[storageKey] = storageValue;
}

function storageEnabled(){
    return typeof(Storage) !== 'undefined';
}
