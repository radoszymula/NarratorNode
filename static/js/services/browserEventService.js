/* eslint-env browser, jquery */
const index = require('../index');

const moderatorService = require('./browserModeratorService');
const playerService = require('./browserPlayerService');
const setupHiddenService = require('./browserSetupHiddenService');
const setupService = require('./browserSetupService');

const roleInfoView = require('../views/roleInfoView');


function handle(object){
    switch (object.event){
    case 'hostChange':
        return moderatorService.onHostChange(object);

    case 'kicked':
        return playerService.onKick(object);

    case 'playerAdded':
        return playerService.onPlayerAdd(object);
    case 'playerExit':
        return playerService.onPlayerExit(object);
    case 'playerStatusChange':
        return playerService.onPlayerStatusChange(object);

    case 'setupChange':
        return setupService.postSetupChangeRefresh(object.setup);
    case 'setupHiddenAdd':
        return setupHiddenService.onSetupHiddenAdd({
            hiddenID: object.hiddenID,
            id: object.setupHiddenID,
        });
    case 'setupHiddenRemove':
        return setupHiddenService.onSetupHiddenRemove(object);

    case 'dayStart':
    case 'nightStart':
    case 'votePhaseReset':
    case 'votePhaseStart':
        return onPhaseStart(object);

    case 'gameEnd':
        return onGameEnd(object);

    case 'phaseEndBid':
        return playerService.onPhaseEndBid(object);

    default:
        break;
    }
}

module.exports = {
    handle,
};


function onGameStart(){
    window.gameState.isStarted = true;
    index.goToGamePlayPage();
}

function onGameEnd(object){
    window.gameState.phase = object.phase;
    window.gameState.timer = null;
    $('#actions_pane').show();
    $('body').addClass('gameOver');
    $('#nav_tab_5 span').text('Exit');
    $('.settings_button').unbind().click(index.leaveGame);

    // window.left = null;
    // window.right = null;
    // window.selected = null;
    index.refreshPlayers();

    $('#end_night_button').text('Leave Game');
}

function onPhaseStart(object){
    if(!window.gameState.isStarted)
        onGameStart(object);
    // setup has to be saved first because profile may reference new enemy faction ids
    // these faction ids might exist in this new setup
    if(object.setup)
        setupService.postSetupChangeRefresh(object.setup);
    if(object.phase){
        window.gameState.phase = object.phase;
        index.refreshActionButtonText();
    }
    Object.values(window.gameState.playerMap).forEach(player => {
        player.endedNight = false;
    });
    if(object.profile){
        window.gameState.profile = object.profile;
        roleInfoView.refresh();
        index.refreshPlayers();
    }
    index.pushRoleCardPopup(); // this should be on the onRoleCardUpdate event
}
