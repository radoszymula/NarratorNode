/* eslint-env browser */
const requests = require('../util/browserRequests');


async function sync(roleID){
    const serverResponse = await requests.get(`roles/${roleID}`);
    const role = serverResponse.response;
    window.gameState.setup.roleMap[role.id] = role;
}

module.exports = {
    sync,
};
