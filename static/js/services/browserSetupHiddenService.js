/* eslint-env browser */
const requests = require('../util/browserRequests');

const setupHiddensView = require('../views/setupHiddensView');


async function addSetupHidden(factionRoleID){
    if(window.gameState.activeFactionID === require('../index').RANDOMS_SELECTED_FACTION_ID)
        return addNonsingleSetupHidden(factionRoleID);
    const setup = window.gameState.setup;
    const factionRole = setup.factionRoleMap[factionRoleID];
    let singleHidden = Object.values(setup.hiddenMap)
        .filter(hidden => hidden.factionRoleIDs.length === 1
            && hidden.name === factionRole.name
            && hidden.factionRoleIDs[0] === factionRoleID)[0];
    if(!singleHidden){
        const hiddenService = require('./browserHiddenService');
        singleHidden = await hiddenService.createHidden(factionRole);
    }
    return addNonsingleSetupHidden(singleHidden.id);
}

function onSetupHiddenAdd({ hiddenID, id }){
    if(window.gameState.setup.setupHiddens.some(setupHidden => setupHidden.id === id))
        return;
    window.gameState.setup.setupHiddens.push({ hiddenID, id });
    setupHiddensView.refresh();
}

function onSetupHiddenRemove({ setupHiddenID }){
    window.gameState.setup.setupHiddens = window.gameState.setup.setupHiddens
        .filter(setupHidden => setupHiddenID !== setupHidden.id);
    setupHiddensView.refresh();
}

async function remove(setupHiddenID){
    await requests.delete(`setupHiddens/${setupHiddenID}`);
    onSetupHiddenRemove({ setupHiddenID });
}

module.exports = {
    addSetupHidden,
    onSetupHiddenAdd,
    onSetupHiddenRemove,
    remove,
};

async function addNonsingleSetupHidden(hiddenID){
    const serverResponse = await requests.post('setupHiddens', { hiddenID });
    onSetupHiddenAdd({ hiddenID, id: serverResponse.response.id });
}
