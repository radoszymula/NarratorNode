/* eslint-env browser */
const requests = require('../util/browserRequests');


async function sync(factionRoleID){
    const serverResponse = await requests.get(`factionRoles/${factionRoleID}`);
    const factionRole = serverResponse.response;
    transformFactionRole(factionRole);
    const faction = window.gameState.setup.factionMap[factionRole.factionID];
    faction.factionRoles = faction.factionRoles
        .map(oldFactionRole => (oldFactionRole.id === factionRoleID
            ? factionRole : oldFactionRole));
    window.gameState.setup.factionRoleMap[factionRole.id] = factionRole;
}

function syncOtherRoles(filteredFactionRoleID, modifierName){
    Object.values(window.gameState.setup.factionRoleMap)
        .filter(factionRole => factionRole.id !== filteredFactionRoleID)
        .filter(factionRole => factionRole.abilities
            .map(ability => ability.setupModifierNames.includes(modifierName)))
        .forEach(factionRole => sync(factionRole.id));
}

function transformFactionRole(factionRole){
    factionRole.abilityMap = {};
    factionRole.abilities.forEach(ability => {
        factionRole.abilityMap[ability.id] = ability;
    });
}

async function updateAbilityModifier(factionRoleID, abilityID, modifierName, value){
    const args = {
        name: modifierName,
        value,
    };
    const serverResponse = await requests.put(
        `factionRoles/${factionRoleID}/abilities/${abilityID}/modifiers`, args,
    );
    value = serverResponse.response.value;
    const factionRole = window.gameState.setup.factionRoleMap[factionRoleID];
    factionRole.abilityMap[abilityID].modifiers
        .find(modifier => modifier.name === modifierName)
        .value = value;
}

async function updateRoleModifier(factionRoleID, modifierName, value){
    const args = {
        name: modifierName,
        value,
    };
    const serverResponse = await requests.put(`factionRoles/${factionRoleID}/modifiers`,
        args);
    value = serverResponse.response.value;
    const factionRole = window.gameState.setup.factionRoleMap[factionRoleID];
    factionRole.modifiers
        .find(modifier => modifier.name === modifierName)
        .value = value;
}

module.exports = {
    sync,
    syncOtherRoles,
    transformFactionRole,
    updateAbilityModifier,
    updateRoleModifier,
};
