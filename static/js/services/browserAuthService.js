/* eslint-env browser */
/* global firebase */
const helpers = require('../util/browserHelpers');
const requests = require('../util/browserRequests');


const config = {
    apiKey: 'AIzaSyBqatywpJsXTXBmYP9p8eR-p3B0OWsmDTk',
    authDomain: 'narrator-119be.firebaseapp.com',
    databaseURL: 'https://narrator-119be.firebaseio.com',
    storageBucket: 'narrator-119be.appspot.com',
};
firebase.initializeApp(config);
const UNKNOWN_AUTH_TOKEN = 'UNKNOWN_AUTH_TOKEN';

function checkPreviousLogin(){
    return new Promise(resolve => {
        const stopListening = firebase.auth().onAuthStateChanged(user => {
            stopListening(); // this stops the auth state from firing a million times
            if(user)
                window.user = user;
            resolve();
        });
    });
}

function createUserAnonymously(){
    return new Promise((resolve, reject) => {
        const unsubscribe = firebase.auth().onAuthStateChanged(user => {
            if(!user)
                return;
            unsubscribe();
            window.user = user;
            resolve();
        });

        firebase.auth().signInAnonymously().catch(error => {
            helpers.log(error, 'Failed to sign in anonymously.');
            unsubscribe();
            reject(error);
        });
    });
}

function createUserWithEmailAndPassword(email, password, displayName){
    if(/\s/g.test(displayName)){
        const errorText = 'Profile name cannot contain spaces';
        return Promise.reject(errorText);
    }

    return firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(user => {
            window.user = user;
            if(displayName)
                return user.updateProfile({
                    displayName,
                }).catch(setDefaultDisplayName);
            return setDefaultDisplayName();
        }).catch(err => {
            if(!err.code){
                helpers.log(err, 'Unknown error on signup.');
                return Promise.reject(err.toString());
            }
            const code = err.code;
            let parsedError;

            if(code === 'auth/invalid-email'){
                parsedError = 'Badly formatted username';
            }else if(code === 'auth/weak-password'){
                parsedError = 'Password is too weak.';
            }else if(code === 'auth/email-already-in-use'){
                parsedError = 'That username is taken!';
            }else{
                parsedError = code;
                helpers.log(err, 'Couldn\t parse signup error.');
            }

            return Promise.reject(parsedError);
        });
}

function ensureLoggedIn(){
    if(window.user)
        return Promise.resolve();
    return createUserAnonymously();
}

function getUserIDWithAuthToken(authToken){
    const userService = require('./browserUserService');
    return requests.get(`users?auth_token=${authToken}`)
        .then(responseObj => {
            const userID = responseObj.response.userID;
            userService.setUserID(userID);
            return userID;
        }).catch(() => {
            throw UNKNOWN_AUTH_TOKEN;
        });
}

function isLoggedIn(){
    return !!window.user;
}

function loginWithEmail(username, password){
    return firebase.auth().signInWithEmailAndPassword(username, password)
        .then(user => {
            window.user = user;
        }).catch(err => {
            if(!err.code){
                helpers.log(err, 'Unknown error on signup.');
                return Promise.reject(err);
            }

            const code = err.code;
            let parsedError;
            if(code === 'auth/wrong-password'){
                parsedError = 'Incorrect password.';
            }else if(code === 'auth/user-not-found' || code === 'auth/invalid-email'){
                parsedError = 'Username not found.';
            }else{
                parsedError = code;
                helpers.log(err, 'Couldn\t parse signup error.');
            }

            return Promise.reject(parsedError);
        });
}

function logout(){
    return firebase.auth().signOut()
        .then(() => {
            window.user = null;
        }).catch(err => {
            helpers.log(err, 'Failed to log out.');
        });
}
window.logout = logout;

function setDefaultDisplayName(){
    const user = window.user;
    if(!user.displayName){
        const index = user.email ? user.email.indexOf('@') : -1;
        let name;
        if(index !== -1)
            name = user.email.substring(0, index);
        else
            name = 'guest';
        return user.updateProfile({ displayName: name });
    } return Promise.resolve();
}

module.exports = {
    checkPreviousLogin,
    createUserWithEmailAndPassword,
    createUserAnonymously,
    ensureLoggedIn,
    getUserIDWithAuthToken,
    isLoggedIn,
    loginWithEmail,
    logout,
    setDefaultDisplayName,

    UNKNOWN_AUTH_TOKEN,
};
