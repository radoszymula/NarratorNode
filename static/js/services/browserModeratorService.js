/* eslint-env browser */
const requests = require('../util/browserRequests');

const playerOverview = require('../views/playerOverview');
const playerView = require('../views/playerView');


function onHostChange(request){
    const index = require('../index');
    window.gameState.host = request.host;
    playerView.refresh();
    index.refreshHost();
}

async function repick(repickTarget){
    const args = {
        repickTarget,
        gameID: window.gameState.gameID,
    };

    const oldHostID = window.gameState.host.id;
    let newHost;
    try{
        const serverResponse = await requests.post('moderators/repick', args);
        newHost = serverResponse.response;
    }catch(err){
        return require('../index').setWarning(err.errors[0]);
    }
    playerOverview.hideRepick();
    if(oldHostID === newHost.id)
        return;
    onHostChange({
        host: newHost,
    });
}

module.exports = {
    onHostChange,
    repick,
};
