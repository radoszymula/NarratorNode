/* eslint-env browser */
const requests = require('../util/browserRequests');


async function createHidden(factionRole){
    const response = await requests.post('hiddens', {
        name: factionRole.name,
        factionRoleIDs: [factionRole.id],
    });
    const setup = window.gameState.setup;
    const hidden = response.response;
    setup.hiddenMap[hidden.id] = hidden;
    return response.response;
}

module.exports = {
    createHidden,
};
