/* eslint-env browser, jquery */

const helpers = require('../util/browserHelpers');
const requests = require('../util/browserRequests');

const playerView = require('../views/playerView');
const roleInfoView = require('../views/roleInfoView');
const setupHiddensView = require('../views/setupHiddensView');

const NO_PREVIOUS_LOGIN = 'NO_PREVIOUS_LOGIN';


async function createGame(isMobile, hostName, setupName){
    const args = {
        name: hostName,
        rules: {
            HOST_VOTING: isMobile,
            CHAT_ROLES: isMobile,
        },
    };
    if(setupName){
        args.setupName = setupName;
    }else{
        const setupService = require('./browserSetupService');
        args.setupID = await setupService.getDefaultSetupID();
    }
    const serverResponse = await requests.post('games', args);
    return getGameStateAndChat(serverResponse.response);
}

async function getGameStateAndChat(game){
    const index = require('../index');
    const chatService = require('./browserChatService');
    const playerService = require('./browserPlayerService');
    const profileService = require('./browserProfileService');
    const setupService = require('./browserSetupService');

    const { gameState } = window;
    let profile;
    try{
        profile = await profileService.get();
        if(!isInGame(profile))
            return;
        gameState.profile = profile;
    }catch(err){
        throw NO_PREVIOUS_LOGIN;
    }
    if(!game)
        game = await getGame(profile.gameID);
    setupService.transformSetup(game.setup);
    gameState.activeUserIDs = await playerService.getActiveUserIDs(game.id);
    gameState.gameID = game.id;
    gameState.host = game.host;
    gameState.integrations = game.integrations;
    gameState.isStarted = game.isStarted;
    playerService.savePlayers(game.players);
    gameState.phase = game.phase;
    gameState.setup = game.setup;
    if(gameState.isStarted){
        refreshGameViews();
    }else{
        index.goToSetupPage();
        refreshLobby();
    }
    setupHiddensView.refresh();

    const userState = await requests.get('games/userState');
    await index.handleObject(userState.response);

    require('./browserFactionService').openFirst();

    const chats = await chatService.getChats();
    await index.handleObject(chats);
}

async function getGame(gameID){
    const serverResponse = await requests.get(`games/${gameID}`);
    return serverResponse.response;
}

function getByJoinID(joinID){
    return requests.get(`games?join_id=${joinID}`);
}

function recoverWithAuthTokenInURL(){
    const authToken = helpers.getURLParameters().auth_token;
    if(!looksLikeAuthToken(authToken))
        return Promise.resolve();

    const authService = require('./browserAuthService');
    const userService = require('./browserUserService');
    return authService.getUserIDWithAuthToken(authToken)
        .then(userID => {
            userService.setUserID(userID);
            if(!userService.isSignedInAnonymously())
                return authService.logout()
                    .then(authService.createUserAnonymously);
        }).then(() => {
            const authIntegrationService = require('./browserAuthIntegrationService');
            return authIntegrationService.addIntegration();
        }).then(() => {
            helpers.setPseudoURL('/');
            return getGameStateAndChat();
        })
        .catch(err => {
            if(err === authService.UNKNOWN_AUTH_TOKEN)
                return require('../index').setWarning('Woops! This game link is no longer active.');
            throw err;
        });
}

async function recoverWithPreviousLogIn(game){
    const authService = require('../services/browserAuthService');
    return authService.checkPreviousLogin()
        .then(() => {
            if(!authService.isLoggedIn())
                throw NO_PREVIOUS_LOGIN;

            const userService = require('./browserUserService');
            return userService.getUserID();
        }).then(() => getGameStateAndChat(game))
        .catch(err => {
            if(err !== NO_PREVIOUS_LOGIN)
                throw err;
        });
}

async function start(){
    try{
        await requests.post('games/1234/start'); // not saving the game id yet
    }catch(err){
        const index = require('../index');
        index.setWarning(err.errors[0]);
    }
}

module.exports = {
    createGame,
    getByJoinID,
    getGameStateAndChat,
    recoverWithAuthTokenInURL,
    recoverWithPreviousLogIn,
    start,
};

function looksLikeAuthToken(authToken){
    if(!authToken)
        return false;
    return authToken.length === 128 && helpers.isAlphaNumeric(authToken);
}

function isInGame(profile){
    return profile.gameID;
}

function refreshLobby(){
    playerView.refresh();
}

function refreshGameViews(){
    roleInfoView.refresh();
    require('../index').refreshActionButtonText();
}
