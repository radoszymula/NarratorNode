/* eslint-env browser */

const requests = require('../util/browserRequests');


function setUserID(newUserID){
    window.gameState.userID = newUserID;
}

function getUserID(){
    if(window.gameState.userID)
        return Promise.resolve(window.gameState.userID);

    return requests.get('users/id')
        .then(responseObj => {
            setUserID(responseObj.response.id);
            return window.gameState.userID;
        });
}

function isSignedInAnonymously(){
    if(!window.user)
        return false;
    return window.user.isAnonymous;
}

module.exports = {
    getUserID,
    isSignedInAnonymously,
    setUserID,
};
