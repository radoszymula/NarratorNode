/* eslint-env jquery, browser */
const requests = require('../util/browserRequests');
const helpers = require('../util/browserHelpers');

const factionsView = require('../views/factionsView');
const generalSettingsView = require('../views/generalSettingsView');
const rolesView = require('../views/rolesView');
const setupHiddensView = require('../views/setupHiddensView');
const versatileOverview = require('../views/versatileOverview');


let setups;

async function getDefaultSetupID(){
    const loadedSetups = Object.values(setups);
    for(let i = 0; i < loadedSetups.length; i++){
        const setup = loadedSetups[i];
        if(!setup.id)
            continue;
        return setup.id;
    }
    const newSetup = await createSetup();
    return newSetup.id;
}

function getSetups(){
    if(setups)
        return Promise.resolve(setups);
    return loadSetups();
}

function isHostsSetup(){
    return window.gameState.setup.ownerID === window.gameState.userID
        && !window.gameState.setup.isPreset;
}

function postSetupChangeRefresh(setup){
    const index = require('../index');
    transformSetup(setup);
    window.gameState.setup = setup;
    window.gameState.setupName = setup.name;

    if(!setup.factionMap[window.gameState.activeFactionID]){
        const factions = Object.values(setup.factionMap)
            .sort((faction1, faction2) => faction1.name.localeCompare(faction2.name));
        const faction = factions[0];
        if(faction){
            const factionRole = setup.factionRoleMap[faction.factionRoles[0].id];
            index.setActiveFaction(faction.id);
            index.setActiveFactionRole(factionRole.id);
        }else{
            index.setActiveFaction(null);
            index.setActiveFactionRole(null);
        }
    }

    generalSettingsView.refresh();
    factionsView.refresh();
    rolesView.refresh();
    setupHiddensView.refresh();
    versatileOverview.refresh();
}

async function resetSetup(){
    if(isHostsSetup())
        await usePresetSetup('classic');
    const defaultSetupID = await getDefaultSetupID();
    await deleteSetup(defaultSetupID);
    return useCustomSetup();
}

function transformSetup(setup){
    const factionRoleService = require('./browserFactionRoleService');
    const roleMap = {};
    setup.roles.forEach(role => {
        roleMap[role.id] = role;
    });

    const factionMap = {};
    const factionRoleMap = {};
    setup.factions.forEach(faction => {
        factionMap[faction.id] = faction;
        faction.factionRoles.forEach(factionRole => {
            factionRoleMap[factionRole.id] = factionRole;
            factionRoleService.transformFactionRole(factionRole);
        });
    });

    const hiddenMap = {};
    setup.hiddens.forEach(hidden => {
        hiddenMap[hidden.id] = hidden;
        hidden.factionRoleMap = {};
        hidden.factionRoleIDs.forEach(factionRoleID => {
            hidden.factionRoleMap[factionRoleID] = factionRoleMap[factionRoleID];
        });
    });

    setup.factionMap = factionMap;
    setup.factionRoleMap = factionRoleMap;
    setup.hiddenMap = hiddenMap;
    setup.roles = roleMap;

    delete setup.factions;
    delete setup.hiddens;
}

async function useCustomSetup(){
    const setupID = await getDefaultSetupID();
    const serverResponse = await requests.put('setups', { setupID });
    postSetupChangeRefresh(serverResponse.response.setup);
    require('./browserFactionService').openFirst();
}

async function usePresetSetup(setupKey){
    const serverResponse = await requests.put('setups', { setupKey });
    postSetupChangeRefresh(serverResponse.response.setup);
    require('./browserFactionService').openFirst();
}

module.exports = {
    getDefaultSetupID,
    getSetups,
    isHostsSetup,
    postSetupChangeRefresh,
    resetSetup,
    transformSetup,
    useCustomSetup,
    usePresetSetup,
};

async function createSetup(){
    const serverResponse = await requests.post('setups');
    setups[''] = serverResponse.response;
    return setups[''];
}

function deleteSetup(setupID){
    return requests.delete(`setups/${setupID}`);
}

function loadSetups(){
    return requests.get('setups')
        .then(data => {
            data = data.response;
            setups = {};

            let opt;
            let key;
            for(let i = 0; i < data.length; i++){
                key = data[i].key;
                setups[key] = data[i];
                if(data[i].id)
                    continue;
                opt = $('<option>');
                opt.text(data[i].name);
                opt.attr('value', key);

                opt.appendTo($('.setup_select_list'));
            }

            $('#game_host_preset_list').change(function(){
                const attrs = setups[$(this).val()].description;

                require('../index').setGameHostDescription(attrs);
            });
            return setups;
        }).catch(helpers.log);
}
