/* eslint-env browser, jquery */
const channelService = require('./services/condorcetServices/browserChannelService');

const currentStandingView = require('./views/condorcetViews/currentStandingView');
const draggableSuspiciousView = require('./views/condorcetViews/draggableSuspiciousView');
const playerVotePopupView = require('./views/condorcetViews/playerVotePopupView');


window.narratorStore = {};

$(document).on('ready', async() => {
    await channelService.getVotes();

    currentStandingView.refresh();
    draggableSuspiciousView.refresh();
    playerVotePopupView.setListeners();

    channelService.savePlayers(draggableSuspiciousView.getPlayers());

    setSpoilerTextButtonListener();
});

function setSpoilerTextButtonListener(){
    $('#genButton').on('click', () => {
        const textArea = $('textarea');
        let text = '[SPOILER=cvote]';
        draggableSuspiciousView.getPlayers().forEach(p => {
            text += `${p},`;
        });
        text += '[/SPOILER]';
        textArea.text(text);
        $('.copiedText').show();

        /* Select the text field */
        textArea[0].select();

        /* Copy the text inside the text field */
        document.execCommand('copy');
    });
}
