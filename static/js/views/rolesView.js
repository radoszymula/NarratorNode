/* eslint-env browser */
const helpers = require('../util/browserHelpers');


function refresh(){
    if(window.gameState.activeFactionRoleID && helpers.isMobile())
        require('../views/factionDetailsView').clear();
    const index = require('../index');
    index.refreshRolesUL();
}

module.exports = {
    refresh,
};
