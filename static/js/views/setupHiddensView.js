/* eslint-env browser, jquery */
const hiddenUtils = require('../util/hiddenUtils');

exports.refresh = () => {
    const { setup } = window.gameState;
    const index = require('../index');
    let rolesListUL;
    if(window.gameState.isStarted){
        rolesListUL = $('#roleList_content');
    }else{
        rolesListUL = $('#setup_roles_list');
        $('.rolesListCounter').text(setup.setupHiddens.length);
    }

    function getName(setupHidden){
        if(!setupHidden.playerName){
            const hidden = setup.hiddenMap[setupHidden.hiddenID];
            return hidden.name;
        }
        return `${setupHidden.playerName} - ${setupHidden.roleType}`;
    }

    function getColor(setupHidden){
        const hidden = setup.hiddenMap[setupHidden.hiddenID];
        return hiddenUtils.getColor(hidden);
    }

    function spanProperties(setupHidden, li, span){
        li.attr('name', setupHidden.hiddenID);
        span.attr('name', setupHidden.hiddenID);
        span.addClass('roleHover');
    }

    function submitRoleRemove(e){
        e.stopPropagation();

        const setupHiddenID = $(e.target).attr('name');
        require('../services/browserSetupHiddenService').remove(parseInt(setupHiddenID, 10));
    }

    function optionClick(e){
        const key = this.value.toString();
        const roleID = e.target.name;

        const o = {
            roleID,
            message: 'setRandom',
            key,
        };
        index.webSend(o);
    }

    function makeOption(li, o){
        const select = $('<select>');
        select.attr('name', o.roleID);
        select.on('change', optionClick);
        const optgroup = $('<optgroup>');

        let pRandom;
        let option;
        optgroup.attr('label', o.roleType);
        optgroup.attr('name', o.roleID);
        for(let i = 0; i < o.possibleRandoms.length; i++){
            pRandom = o.possibleRandoms[i];
            option = $('<option>');
            option.val(pRandom.key);
            option.text(pRandom.roleType);
            option.css('color', pRandom.color);
            optgroup.append(option);
        }
        if(o.spawn){
            // setSpawn
        }

        li.css('margin-bottom', 40);

        select.append(optgroup);
        li.append(select);
    }

    const setupService = require('../services/browserSetupService');
    function appendMinus(li, o){
        if(window.gameState.started || !window.gameState.isHost() || !setupService.isHostsSetup())
            return;
        if(window.gameState.hiddenRandoms && o.possibleRandoms)
            makeOption(li, o);

        const photo = $('<i>');
        // photo.addClass("setup_plus_minus");
        photo.addClass('setup_plus_minus fa fa-lg fa-minus-square');

        photo.attr('name', o.id);
        li.append(photo);

        photo.unbind();
        photo.click(submitRoleRemove);
    }

    index.populateUL(rolesListUL, setup.setupHiddens, getName, getColor,
        [index.showRoleInfo, null], spanProperties, appendMinus);
};
