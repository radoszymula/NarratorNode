/* eslint-env jquery */


function hide(){
    $('#userPopup').hide();
    $('#userPopupBackdrop').hide(400);
}

function hideIfIs(playerName){
    const currentPlayerName = $('.userPopupHeader').text();
    if(currentPlayerName !== playerName)
        return;
    hide();
}

function hideRepick(){
    $('#userPopupRepick').hide();
}

module.exports = {
    hide,
    hideIfIs,
    hideRepick,
};
