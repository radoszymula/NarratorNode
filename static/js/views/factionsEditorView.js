/* eslint-env jquery */

function hide(){
    require('../index').closeTeamEditor();
}

module.exports = {
    hide,
};
