/* eslint-env browser, jquery */
const channelService = require('../../services/condorcetServices/browserChannelService');

const colorHelpers = require('../../util/colorHelpers');

function getPlayers(){
    return $('#personalRanking li').map((index, val) => $(val).text()).toArray();
}

function refresh(){
    const playerCount = window.narratorStore.options.length;
    const sortableContainer = $('#personalRanking');

    window.narratorStore.options.forEach((playerName, index) => {
        const li = $('<li>').text(playerName);
        const color = colorHelpers.getGradientColor(index, playerCount);
        li.css('color', color);
        sortableContainer.append(li);
    });

    sortableContainer.sortable();
    sortableContainer.disableSelection();
    sortableContainer.listview('refresh');

    sortableContainer.on('sortstop', () => {
        $('#personalRanking').listview('refresh');
        channelService.savePlayers(getPlayers());
        $('.copiedText').hide();
        refreshColors();
    });
}

module.exports = {
    getPlayers,
    refresh,
};

function refreshColors(){
    const playerCount = window.narratorStore.options.length;
    $('#personalRanking li').each(function(index){
        const color = colorHelpers.getGradientColor(index, playerCount);
        $(this).css('color', color);
    });
}
