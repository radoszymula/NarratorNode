/* eslint-env browser, jquery */

function refresh(){
    const { currentPlayer } = window.narratorStore;
    const voteInput = window.narratorStore.voterMetadata[currentPlayer];

    const singleRankingContainer = $('#votelisit');
    singleRankingContainer.empty();

    $('#popupheadertext').text(`${currentPlayer}'s Votes `);

    if(voteInput)
        voteInput.ranking.forEach(vote => $('#votelist').append($('<li>').text(vote)));
}

function setListeners(){
    setOnHeaderClick();
    setOnClickaway();
}

function show(){
    $('.votepopup').show();
}

module.exports = {
    refresh,
    setListeners,
    show,
};

function setOnClickaway(){
    $('#votepopupexit').on('click', () => $('.votepopup').hide());
}

function setOnHeaderClick(){
    $('#popupheadertext').on('click', () => {
        const { currentPlayer } = window.narratorStore;
        const voteMetadata = window.narratorStore.voterMetadata[currentPlayer];
        window.location = voteMetadata.url;
    });
}
