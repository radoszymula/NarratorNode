/* eslint-env browser, jquery */
const colorHelpers = require('../../util/colorHelpers');


function refresh(){
    const { standing } = window.narratorStore;
    const playerCount = window.narratorStore.options.length;
    const standingContainer = $('#currentRanking');
    standingContainer.empty();

    let seen = 0;
    standing.forEach(layer => {
        layer.forEach(p => {
            const li = $('<li>').text(`${seen + 1}. ${p} `);
            const color = colorHelpers.getGradientColor(seen, playerCount);
            li.css('color', color);

            const link = $('<i>').addClass('fa fa-link');
            li.append(link);
            li.css('cursor', 'pointer');
            li.on('click', () => showVotes(p));

            standingContainer.append(li);
        });
        seen += layer.length;
    });
    standingContainer.listview('refresh');
}

module.exports = {
    refresh,
};

function showVotes(player){
    window.narratorStore.currentPlayer = player;

    const playerVotePopupView = require('./playerVotePopupView');
    playerVotePopupView.refresh();
    playerVotePopupView.show();
}
