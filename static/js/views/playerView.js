/* eslint-env browser, jquery */
const { Targetable } = require('../util/targetable');


const targetables = {};

function getTargetable(name){
    return targetables[name];
}

function refresh(){
    if(!window.gameState.isStarted)
        refreshSetup();
}

function saveTargetable(targetable){
    targetables[targetable.player.name] = targetable;
}

module.exports = {
    getTargetable,
    refresh,
    saveTargetable,
};

function refreshSetup(){
    const playerUL = $('#setup_players_list');
    playerUL.find('li').remove();

    const players = Object.values(window.gameState.playerMap);
    players.forEach(player => {
        const { name } = player;
        const targetable = new Targetable(player, name, false);
        targetable.makeVisible(playerUL);
        saveTargetable(targetable);
    });
    $('.lobbyCountLabel').text(Object.values(window.gameState.playerMap).length);
}
