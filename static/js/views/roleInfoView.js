/* eslint-env browser, jquery */


function refresh(){
    if(window.gameState.isStarted)
        refreshGame();
    else
        refreshLobby();
}

function getRoleCardDetails(){
    const index = require('../index');
    const factionDetailsView = require('./factionDetailsView');
    const tabView = require('./tabView');
    const { profile } = window.gameState;
    const { setup } = window.gameState;

    const bulletPoints = [...profile.roleCard.details];
    if(bulletPoints.length)
        bulletPoints.push(null);

    if(profile.allies.length){
        const { allies } = profile;
        let bulletPoint = [];
        if(allies.length > 1)
            bulletPoint.push('Your allies are: ');
        else
            bulletPoint.push('Your ally is: ');

        profile.allies.forEach((ally, counter) => {
            if(counter)
                bulletPoint.push(', ');
            bulletPoint.push({
                text: `${ally.name} (${ally.roleName})`,
                color: setup.factionMap[ally.factionID].color,
                // onClick: () => {
                //     if(!setup.roleMap[ally.roleID])
                //         return;
                //     index.setActiveFactionRole(ally.roleID);
                //     tabView.setGameInfoSelected();
                //     refresh();
                // },
            });
        });
        bulletPoints.push(bulletPoint);

        profile.allies.forEach(ally => {
            if(ally.night_kill_immune){
                bulletPoint = [];
                bulletPoint.push({
                    text: ally.teamAllyName,
                    bolded: true,
                });
                bulletPoint.push(' is unkillable at night.');
                bulletPoints.push(bulletPoint);
            }
            if(ally.undetectable){
                bulletPoint = [];
                bulletPoint.push({
                    text: ally.teamAllyName,
                    bolded: true,
                });
                bulletPoint.push(' is not detectable by investigatives.');
                bulletPoints.push(bulletPoint);
            }
            if(ally.unblockable){
                bulletPoint = [];
                bulletPoint.push({
                    text: ally.teamAllyName,
                    bolded: true,
                });
                bulletPoint.push(' cannot be stopped by roleblockers.');
                bulletPoints.push(bulletPoint);
            }
            if(ally.auto_vest){
                bulletPoint = [];
                bulletPoint.push({
                    text: ally.teamAllyName,
                    bolded: true,
                });
                bulletPoint.push(` started off with ${ally.auto_vest} auto vest(s).`);
                bulletPoints.push(bulletPoint);
            }
        });
    }

    if(profile.roleCard.enemyFactionIDs.length){
        const bulletPoint = ['You must eliminate: '];
        profile.roleCard.enemyFactionIDs.forEach((enemyID, counter) => {
            if(counter)
                bulletPoint.push(', ');
            const enemyFaction = setup.factionMap[enemyID];
            bulletPoint.push({
                text: enemyFaction.name,
                color: enemyFaction.color,
                onClick: () => {
                    index.setActiveFaction(enemyID);
                    tabView.setGameInfoSelected();
                    factionDetailsView.refresh();
                },
            });
        });
        bulletPoint.push('.');
        bulletPoints.push(bulletPoint);
    }else if(profile.roleCard.winConditionText){
        bulletPoints.push(profile.roleCard.winConditionText);
    }

    return bulletPoints;
}

module.exports = {
    getRoleCardDetails,
    refresh,
};

function refreshLobby(){}

function refreshGame(){
    const { profile } = window.gameState;
    $('#yourName').text(profile.name);
    addRoleTeamClickable();

    $('#your_exp_extras').empty();
    getRoleCardDetails().forEach(addTextToRoleExtras);
}

function addRoleTeamClickable(){
    const index = require('../index');
    const factionDetailsView = require('./factionDetailsView');
    const tabView = require('./tabView');
    const { profile } = window.gameState;
    const faction = window.gameState.setup.factionMap[profile.roleCard.factionID];
    const factionSpan = $('<span>');
    factionSpan.text(`${faction.name} `);
    factionSpan.css('color', faction.color);
    factionSpan.addClass('roleHover');
    factionSpan.on('click', () => {
        index.setActiveFaction(faction.id);
        tabView.setGameInfoSelected();
        factionDetailsView.refresh();
    });

    $('#yourRole').empty();
    $('#yourRole').append(factionSpan);

    const roleSpan = $('<span>');
    roleSpan.text(profile.roleCard.roleName);
    roleSpan.css('color', faction.color);

    $('#yourRole').append(roleSpan);
}

function addTextToRoleExtras(parts){
    if(!parts){
        $('#your_exp_extras li').last().addClass('your_exp_spacer');
        return;
    }


    const li = $('<li>');
    li.addClass('rule_game_info_extras');

    if(parts.isFeedback){
        li.append($(parts.text));
        return $('#your_exp_extras').append(li);
    }

    for(let i = 0; i < parts.length; i++){
        const part = parts[i];
        if(typeof(part) === 'string'){
            li.append(part);
            continue;
        }

        const span = $('<span>');
        span.text(part.text);
        if(part.color)
            span.css('color', part.color);
        if(part.onClick){
            span.click(part.onClick);
            span.addClass('roleHover');
            span.attr('name', part.nameAttr);
        }
        if(part.bolded)
            span.css('font-weight', 'bold');

        li.append(span);
    }
    $('#your_exp_extras').append(li);
}
