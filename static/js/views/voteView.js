/* eslint-env browser, jquery */
const { ArrayList } = require('../util/arrayList');


function refresh(){
    const day = window.gameState.voteView.day;
    let nextDayButtonText;
    let nextDayOnClick;
    if(day === window.gameState.dayNumber){
        nextDayButtonText = 'Reset';
        nextDayOnClick = resetVoteCount;
    }else{
        nextDayButtonText = `Day ${window.gameState.voteView.day + 1}`;
        nextDayOnClick = incrementDay;
    }

    $('#prev_day_vc').unbind();
    $('#prev_day_vc').click(onPrevDayClick);

    $('#next_day_vc').unbind();
    $('#next_day_vc').show();
    $('#next_day_vc').click(nextDayOnClick);

    let prevDayText = null;
    if(day > 1)
        prevDayText = `Day ${window.gameState.dayNumber - 1}`;

    const headerText = `VC: ${day}.${window.gameState.voteView.counter}`;
    const currentDay = window.gameState.voteInfo.voteCounts[day - 1]; // 0 based indexing, gotta do the minus 1

    const votedFor = getScore(currentDay, window.gameState.voteView.counter);


    $('.vc_header_label_text').text(headerText);

    if(prevDayText){
        $('#prev_day_vc').show();
        $('#prev_day_vc').text(prevDayText);
    }else{
        $('#prev_day_vc').hide();
    }

    $('#next_day_vc').text(nextDayButtonText);

    const outerUL = $('.votelist_ul');
    outerUL.empty();

    for(let i = 0; i < votedFor.length; i++){
        const li = $('<li>');
        const div = $('<div>');
        const ol = $('<ol>');
        const span1 = $('<span>');
        const span2 = $('<span>');

        li.css('list-style', 'none');
        ol.addClass('vc_people2Person');

        // handles the header for the vote count of the current person being voted
        span1.append(votedFor[i].name);
        if(votedFor[i].name !== 'Not Voting')
            span1.append(':');
        span1.addClass('observer_player');

        // this means this user doesn't have enough votes on her yet.
        if(votedFor[i].toLynch >= 0){
            span2.append(`   L - ${votedFor[i].toLynch}`);
            span2.addClass('observer_hammer');
        }

        span1.append(span2);
        div.append(span1);
        // end header handling

        // getting the list of users
        const innerList = votedFor[i].voters;

        let seenVoters = [];
        for(let j = 0; j < innerList.length; j++){
            const voter = innerList[j];
            if(seenVoters.indexOf(voter) !== -1) // handles mayor double voting
                continue;
            seenVoters.push(voter);
            const li2 = $('<li>');
            li2.append(voter);
            if(j + 1 === innerList.length && innerList.length % 2)
                li2.addClass('vc_lastVoter');
            else if(j % 2)
                li2.addClass('vc_rightVoter');
            else
                li2.addClass('vc_leftVoter');

            li2.addClass('vc_voteeElement');

            ol.append(li2);
        }
        seenVoters = [];

        div.append(ol);
        li.append(div);
        outerUL.append(li);
    }
    require('../index').prepObserverMode();
}

module.exports = {
    refresh,
};

function getScore(voteLog, counter){
    const finalScore = {
        'Not Voting': new ArrayList(),
    };
    for(let i = 0; i < voteLog.alivePlayers.length; i++)
        finalScore['Not Voting'].add(voteLog.alivePlayers[i]);

    const movements = voteLog.movements;
    for(let i = 0; i < counter && i < movements.length && movements.length > 0; i++){
        const movement = movements[i];

        if(movement.length !== undefined){
            removeFromScore(finalScore, movement, voteLog);
            continue;
        }

        const voter = movement.voter;
        const voted = movement.voted || 'Not Voting';
        const prev = movement.prev || 'Not Voting';

        while (finalScore[prev].contains(voter))
            finalScore[prev].remove(voter);
        if(!finalScore[voted])
            finalScore[voted] = new ArrayList();
        if(movement.power && movement.voted)
            for(let p = 0; p < movement.power; p++)
                finalScore[voted].add(voter);

        else
            finalScore[voted].add(voter);
    }

    let minLynch = voteLog.alivePlayers.length / 2;
    minLynch = Math.floor(minLynch) + 1;

    const keys = Object.keys(finalScore);
    const ret = [];
    let lynchList;
    for(let i = 0; i < keys.length; i++){
        lynchList = {};
        lynchList.voters = finalScore[keys[i]].backer;
        if(lynchList.voters.length === 0)
            continue;
        lynchList.name = keys[i];
        if(keys[i] !== 'Not Voting')
            lynchList.toLynch = minLynch - lynchList.voters.length;
        ret.push(lynchList);
    }

    sortVotes(ret);

    return ret;
}

function onPrevDayClick(){
    window.gameState.voteView.day--;
    const voteView = window.gameState.voteView;
    const dayNumber = window.gameState.dayNumber - 1;
    voteView.counter = window.gameState.voteInfo.voteCounts[dayNumber].movements.length;
    refresh();
}

function incrementDay(){
    window.gameState.voteView.day++;
    const voteView = window.gameState.voteView;
    const dayNumber = window.gameState.dayNumber - 1;
    voteView.counter = window.gameState.voteInfo.voteCounts[dayNumber].movements.length;
    refresh();
}

function resetVoteCount(){
    const voteView = window.gameState.voteView;
    const dayNumber = window.gameState.dayNumber - 1;
    voteView.counter = window.gameState.voteInfo.voteCounts[dayNumber].movements.length;
    refresh();
}

function removeFromScore(finalScore, dead, voteLog){
    const keys = Object.keys(finalScore);
    for(let i = 0; i < dead.length; i++){
        for(let k = 0; k < keys.length; k++)
            finalScore[keys[k]].remove(dead[i], true);

        const index = voteLog.alivePlayers.indexOf(dead[i]);
        if(index > -1)
            voteLog.alivePlayers.splice(index, 1);
    }
}

function sortVotes(ret){
    ret.sort((x, y) => {
        if(x.toLynch && y.toLynch)
            return x.toLynch - y.toLynch;

        if(x.toLynch)
            return -1;

        if(y.toLynch)
            return 1;
        return 0;
    });
}
