/* eslint-env browser, jquery */
const colors = require('../util/colors');


function refresh(){
    const index = require('../index');
    const factionDetailsView = require('./factionDetailsView');
    const factionService = require('../services/browserFactionService');
    const rolesOverviewView = require('./versatileOverview');

    function factionClick(){
        const e = $(this);
        let factionID;
        if(e.hasClass('rSetupFocused')){
            factionID = null;
            $('.rSetupFocused').removeClass('rSetupFocused');
            collapseRoleSettingsCard();
        }else{
            const clickedFaction = window.gameState.setup.factionMap[e.attr('name')];
            if(clickedFaction)
                factionID = clickedFaction.id;
            else
                factionID = index.RANDOMS_SELECTED_FACTION_ID;
            $('.rSetupFocused').removeClass('rSetupFocused');
            e.addClass('rSetupFocused');
            expandRoleSettingsCard();

            $('.role_settings_pane').hide();
            $('.team_settings_pane').show();
        }
        $('.rSetupBG').removeClass('rSetupBG');

        $('.addTeamTrio').hide();

        index.setActiveFaction(factionID);
        index.setActiveFactionRole(null);

        refresh();
        index.refreshRolesUL();
        index.refreshEnemiesUL();
        factionDetailsView.refresh();
        rolesOverviewView.refresh();
    }


    const pane = $('#team_catalogue_pane');
    pane.empty();

    function getName(id){
        if(id === index.RANDOMS_SELECTED_FACTION_ID)
            return 'Randoms';
        return window.gameState.setup.factionMap[id].name;
    }

    function getColor(id){
        if(id === index.RANDOMS_SELECTED_FACTION_ID)
            return colors.white;
        return window.gameState.setup.factionMap[id].color;
    }

    function spanProperties(o, li, span){
        span.addClass('table_span_items');
    }

    function liProps(li, factionID){
        const color = getColor(factionID);
        const name = getName(factionID);
        li.attr('name', factionID);
        li.addClass(name); // probably unused
        li.addClass(`${color.substring(1)}`); // probably unused
        li.addClass(`factionID${factionID}`);

        if(window.gameState.activeFactionID && window.gameState.activeFactionID === factionID)
            if(window.gameState.activeFactionRoleID || window.gameState.activeHiddenID)
                li.addClass('rSetupBG');
            else
                li.addClass('rSetupFocused');

        const photo = $('<i>');
        // photo.addClass("setup_plus_minus");
        photo.addClass('setup_plus_minus fa fa-lg');
        if(window.gameState.isHost() && name !== 'Randoms' && index.isSuperCustom())
            photo.addClass('fa-pencil');
        else
            photo.addClass('fa-search');

        li.append(photo);
        if(name !== 'Randoms' && name !== 'Neutrals'){
            photo.attr('id', factionID.id);
            photo.click(function(e){
                e.stopPropagation();
                index.teamLoader($(this).attr('id'));
            });
        }else{
            photo.css('color', colors.background);
        }
    }

    const onClicks = [factionClick, null];
    const setup = window.gameState.setup;
    const factionIDs = Object.keys(setup.factionMap).map(factionID => parseInt(factionID, 10));
    const nonSingleHiddens = Object.values(setup.hiddenMap)
        .filter(hidden => {
            if(hidden.factionRoleIDs.length !== 1)
                return true;
            const factionRole = setup.factionRoleMap[hidden.factionRoleIDs[0]];
            return hidden.name !== factionRole.name;
        });
    if(nonSingleHiddens.length)
        factionIDs.push(index.RANDOMS_SELECTED_FACTION_ID);
    index.populateUL(pane, factionIDs, getName, getColor, onClicks, spanProperties, liProps);
    expandRoleSettingsCard();

    $('#teamDelete').unbind();
    $('#teamDelete').click(factionService.delete);

    $('.editRolesButton').unbind();
    $('.editRolesButton').click(() => {
        $('.deleteTeamButton').text('Save Changes');
        index.refreshEnemiesUL();
        index.refreshRolesUL();
    });

    $('.editAlliesButton').unbind();
    $('.editAlliesButton').click(() => {
        $('.deleteTeamButton').text('Save Changes');
        index.refreshEnemiesUL();
        index.refreshRolesUL();
    });
}

module.exports = {
    refresh,
};

function collapseRoleSettingsCard(){
    $('body').removeClass('setup_expanded_card');
    $('body').addClass('setup_contracted_card');
}

function expandRoleSettingsCard(){
    $('body').addClass('setup_expanded_card');
    $('body').removeClass('setup_contracted_card');
}
