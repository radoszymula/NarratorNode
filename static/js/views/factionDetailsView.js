/* eslint-env browser, jquery */
const helpers = require('../util/browserHelpers');


function clear(){
    $('.teamDescriptionLabel').text('');
    $('#teamDescriptionText').text('');

    $('#team_catalogue .roleCardSettingsAdapter').empty();
    hideEditFactionTrio();
    $('.team_settings_pane').hide();
}

function refresh(){
    const faction = window.gameState.setup.factionMap[window.gameState.activeFactionID];
    if(!faction || (window.gameState.activeFactionRoleID && helpers.isMobile()))
        return clear();

    const index = require('../index');
    const factionModifierService = require('../services/browserFactionModifierService');
    const versatileOverview = require('./versatileOverview');

    $('.team_settings_pane').show();


    const { name } = faction;
    const { color } = faction;

    const header = $('.teamDescriptionLabel');
    if(!index.isHostsSetup() || !index.isSuperCustom())
        hideEditFactionTrio();

    header.text(name);
    header.css('color', color);
    $('#game_play_card .roleCardSubHeader').hide();

    index.setImage($('.team_settings_pane'), 'citizen');

    const ruleList = $('.team_settings_pane .teamBulletPointHolder');
    ruleList.empty();
    const ruleSource = faction.description
        ? faction.description.split('\n')
        : [];
    if(index.isHostsSetup() && !window.gameState.isStarted)
        faction.modifiers.forEach(modifier => ruleSource.push(modifier));
    else
        faction.details.forEach(detail => ruleSource.push(detail));


    if(window.gameState.isStarted){
        if(faction.factionRoles.length)
            ruleSource.push(getPossibleRolesBulletPoint());
        if(faction.enemyIDs.length)
            ruleSource.push(getEnemyBulletPoint());
        $('#game_play_card').show();
    }

    ruleSource.forEach(rule => {
        if(rule.name === 'WIN_PRIORITY')
            return;
        const element = $('<li>');

        let changeFunc;
        if(typeof(rule.value) === 'boolean')
            changeFunc = function(){
                const factionID = window.gameState.activeFactionID;
                const modifierName = rule.name;
                factionModifierService.update(this.checked, modifierName, factionID);
            };
        else
            changeFunc = function(){
                const factionID = window.gameState.activeFactionID;
                const modifierName = rule.name;
                factionModifierService.update(parseInt(this.value, 10), modifierName, factionID);
            };

        if(window.gameState.isStarted)
            versatileOverview.addTextToRuleExtras(rule, ruleList);
        else
            index.createRuleElement(rule, element, changeFunc);

        ruleList.append(element);
        element.addClass(rule.id);
    });
}

module.exports = {
    clear,
    refresh,
};

function hideEditFactionTrio(){
    if(helpers.isMobile())
        $('.mainEditTeamButton').hide();
    else
        $('.editTeamButton').hide();
}

function getEnemyBulletPoint(){
    const index = require('../index');
    const bulletPoint = ['Must eliminate: '];
    const { factionMap } = window.gameState.setup;
    factionMap[window.gameState.activeFactionID].enemyIDs.forEach((enemyFactionID, counter) => {
        if(counter)
            bulletPoint.push(', ');
        const enemyFaction = factionMap[enemyFactionID];
        bulletPoint.push({
            text: enemyFaction.name,
            color: enemyFaction.color,
            onClick: () => {
                index.setActiveFaction(enemyFactionID);
                refresh();
            },
        });
    });
    bulletPoint.push('.');
    return bulletPoint;
}

function getPossibleRolesBulletPoint(){
    const index = require('../index');
    const versatileOverview = require('./versatileOverview');
    const bulletPoint = ['Possible roles: '];
    const { factionMap } = window.gameState.setup;
    const faction = factionMap[window.gameState.activeFactionID];
    faction.factionRoles.forEach((factionRole, counter) => {
        if(counter)
            bulletPoint.push(', ');
        bulletPoint.push({
            text: factionRole.name,
            color: faction.color,
            onClick: () => {
                index.setActiveFactionRole(factionRole.id);
                if(!window.gameState.isStarted){
                    index.setActiveFaction(factionRole.id);
                    refresh();
                }else{
                    versatileOverview.refresh();
                }
            },
        });
    });
    bulletPoint.push('.');
    return bulletPoint;
}
