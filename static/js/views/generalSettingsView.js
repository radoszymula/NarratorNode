/* eslint-env browser, jquery */
const helpers = require('../util/browserHelpers');


function refresh(){
    const setupService = require('../services/browserSetupService');
    if(setupService.isHostsSetup())
        $('.setup_input').hide();
    else
        $('.setup_input').show();

    refreshPane();
}

function refreshSetupSpinner(){
    $('#setup_gen_settings_pane select').val(window.gameState.setupName);
}

module.exports = {
    refresh,
    refreshSetupSpinner,
};

function refreshPane(){
    const index = require('../index');
    const setupModifierService = require('../services/browserSetupModifierService');
    const setupService = require('../services/browserSetupService');
    const soundService = require('../services/browserSoundService');

    const { setupModifiers } = window.gameState.setup;
    $('.general_rules').unbind();
    $('.general_rules').prop('disabled', !window.gameState.isHost());
    $('.nightLengthRuleVal').val(setupModifiers.NIGHT_LENGTH.value / 60);
    $('.nightLengthRuleVal').text(setupModifiers.NIGHT_LENGTH.value / 60);
    $('.dayLengthRuleVal').val(setupModifiers.DAY_LENGTH.value / 60);
    $('.dayLengthRuleVal').text(setupModifiers.DAY_LENGTH.value / 60);
    $('.chargeVariabilityRuleVal').val(setupModifiers.CHARGE_VARIABILITY.value);
    $('.chargeVariabilityRuleVal').text(setupModifiers.CHARGE_VARIABILITY.value);
    $('#host_mode').prop('checked', window.gameState.hiddenRandoms);

    if(window.gameState.isHost())
        $('body').removeClass('disabled_settings');
    else
        $('body').addClass('disabled_settings');


    $('.settings_toggle').removeClass('selected_toggle');
    setCustomToggleValue('CHAT_ROLES');
    setCustomToggleValue('PUNCH_ALLOWED');
    setCustomToggleValue('HOST_VOTING');
    setCustomToggleValue('DAY_START');
    setCustomToggleValue('LAST_WILL');
    setCustomToggleValue('DIFFERENTIATED_FACTION_KILLS');
    setCustomToggleValue('SELF_VOTE');
    setCustomToggleValue('OMNISCIENT_DEAD');

    if(!setupService.isHostsSetup()){
        $('.autosetup .settings_on').addClass('selected_toggle');
        $('.autosetup .settings_off').removeClass('selected_toggle');
        $('.customizability').hide();
        $('.setup_input').show();
        if(window.gameState.rules.PUNCH_ALLOWED.val)
            $('.punch_allowed').show();
        else
            $('.punch_allowed').hide();
    }else{
        $('.customizability').show();
        $('.setup_input').hide();
        $('.autosetup .settings_on').removeClass('selected_toggle');
        $('.autosetup .settings_off').addClass('selected_toggle');
        if(!index.isSuperCustom()){
            $('.customizability .settings_on').addClass('selected_toggle');
            $('.customizability .settings_off').removeClass('selected_toggle');
            $('.punch_allowed').hide();
        }else{ // full throttle
            $('.customizability .settings_on').removeClass('selected_toggle');
            $('.customizability .settings_off').addClass('selected_toggle');
            $('.punch_allowed').show();
        }
    }

    if(soundService.defaultSound('music_enabled')){
        $('.sound_toggle .settings_on').addClass('selected_toggle');
        $('.sound_toggle .settings_off').removeClass('selected_toggle');
        $('.tablet_sound_toggle span').text('Sound Off');
    }else{
        $('.sound_toggle .settings_on').removeClass('selected_toggle');
        $('.sound_toggle .settings_off').addClass('selected_toggle');
        $('.tablet_sound_toggle span').text('Sound On');
    }

    const body = $('body');
    if(index.isSuperCustom()){
        if(!body.hasClass('setup_full_custom')){
            body.removeClass('setup_partial_custom');
            body.addClass('setup_full_custom');
        }
    }else if(!body.hasClass('setup_partial_custom')){
        body.removeClass('setup_full_custom');
        body.addClass('setup_partial_custom');
    }

    if(window.gameState.rules.CHAT_ROLES.val || window.gameState.integrations.includes('discord'))
        $('#host_voting').hide();
    else
        $('#host_voting').show();


    if(window.gameState.rules.PUNCH_ALLOWED.val)
        $('.punchSucc').show();
    else
        $('.punchSucc').hide();

    if(window.gameState.integrations.includes('discord'))
        $('#chat_roles').hide();


    if(window.gameState.rules.HOST_VOTING.val && !helpers.isMobile())
        $('#Day_Length').hide();
    else
        $('#Day_Length').show();


    $('#gen_rules_edit_name').bind('change keyup', function(){
        const newName = $(this).val();
        const o = {};
        o.message = 'nameChange';
        o.nameChange = newName;
        index.saveInputtedGameName();
        index.webSend(o);
    });
    $('#gen_rules_edit_name').focusout(() => {
        $('#gen_rules_edit_name').val(window.gameState.playerName);
        setTimeout(() => {
            $('#gen_rules_edit_name').removeClass('error_text_input');
        }, 10);
    });

    $('.tablet_sound_toggle').unbind();
    $('.tablet_sound_toggle').click(function(){
        const s = $(this).find('span');
        const clickedOn = s.text().indexOf('On') !== -1;
        index.SETTINGS_KEYS.forEach(key => {
            if(key === 'chat_ping' && helpers.isMobile())
                return;
            index.setSettingValue(key, clickedOn);
        });

        if(clickedOn)
            $('.tablet_sound_toggle span').text('Sound Off');
        else
            $('.tablet_sound_toggle span').text('Sound On');
    });


    $('#setup_gen_settings_pane .settings_toggle').unbind();
    $('#setup_gen_settings_pane .settings_toggle').click(async function(){
        const clicked = $(this);

        const isSoundToggle = clicked.parent().hasClass('sound_toggle');

        if(clicked.hasClass('selected_toggle')
            || clicked.parent().hasClass('autosetup')
            || clicked.parent().hasClass('customizability')
            || (!window.gameState.isHost() && !isSoundToggle))
            return;
        const clickedOn = clicked.hasClass('settings_on');

        clicked.siblings().removeClass('selected_toggle');
        clicked.addClass('selected_toggle');
        if(isSoundToggle){
            index.SETTINGS_KEYS.forEach(key => {
                if(key === 'chat_ping' && helpers.isMobile())
                    return;
                index.setSettingValue(key, clickedOn);
            });
            if(clickedOn)
                soundService.play();
            else
                soundService.pause();

            return;
        }

        const ruleName = clicked.parent().attr('id');
        if(ruleName === 'CHAT_ROLES' && clickedOn)
            await setupModifierService.update('HOST_VOTING', false);
        await setupModifierService.update(ruleName.toUpperCase(), clickedOn);
    });

    $('.autosetup .settings_toggle').unbind();
    $('.autosetup .settings_toggle').click(function(){
        const clicked = $(this);
        if(clicked.hasClass('selected_toggle') || !window.gameState.isHost())
            return;
        clicked.siblings().removeClass('selected_toggle');
        clicked.addClass('selected_toggle');

        if(clicked.hasClass('settings_on'))
            setupService.usePresetSetup('classic');
        else
            setupService.useCustomSetup();
    });

    $('.customizability .settings_toggle').unbind();
    $('.customizability .settings_toggle').click(function(){
        const clicked = $(this);
        if(clicked.hasClass('selected_toggle') || !window.gameState.isHost())
            return;
        clicked.siblings().removeClass('selected_toggle');
        clicked.addClass('selected_toggle');

        index.setSuperCustom(!clicked.hasClass('settings_on'));
        refreshPane();
        require('./factionsView').refresh();
    });


    $('#setup_gen_settings_pane select').unbind();
    $('#setup_gen_settings_pane select').change(function(e){
        e = $(this);
        if(window.gameState.isHost())
            setupService.usePresetSetup(e.val());
        else
            refreshSetupSpinner();
    });

    $('#host_mode').change(() => {
        index.webSend({
            message: 'hiddenRandoms',
        });
    });
}

function setCustomToggleValue(ruleName){
    let selectorName = `.${ruleName.toLowerCase()}`;
    if(window.gameState.rules[ruleName].val)
        selectorName += ' .settings_on';
    else
        selectorName += ' .settings_off';

    $(selectorName).addClass('selected_toggle');
}
