/* eslint-env browser, jquery */
const hiddenUtils = require('../util/hiddenUtils');
const roleImages = require('../util/roleImages');


function refresh(){
    if(window.gameState.activeFactionRoleID)
        return setRoleInfo();

    if(window.gameState.activeHiddenID)
        return setHiddenInfo();

    if(window.gameState.activePlayerName)
        return setDeadPlayerInRoleSettings();
    clear();
}

function addTextToRuleExtras(parts, container){
    const li = $('<li>');
    li.addClass('rule_game_info_extras');

    if(typeof(parts) === 'string')
        li.append(parts);
    else
        for(let i = 0; i < parts.length; i++){
            const part = parts[i];
            if(typeof(part) === 'string'){
                li.append(part);
                continue;
            }

            const span = $('<span>');
            span.text(part.text);
            span.css('color', part.color);
            span.addClass('roleHover');
            span.attr('name', part.nameAttr);
            span.click(part.onClick);

            if(part.className)
                span.addClass(part.className);

            li.append(span);
        }

    container.append(li);
}

module.exports = {
    addTextToRuleExtras,
    refresh,
};

function setHiddenInfo(){
    const { setup } = window.gameState;
    const hidden = setup.hiddenMap[window.gameState.activeHiddenID];

    const header = $('.notTeamCard .roleCardHeader');
    const subHeader = $('.notTeamCard .roleCardSubHeader');

    header.text(hidden.name);
    header.show();
    header.css('color', hiddenUtils.getColor(hidden));
    subHeader.hide();

    const spawnableRoles = hidden.factionRoleIDs
        .map(factionRoleID => setup.factionRoleMap[factionRoleID]);
    const spawningRolesBullet = getSpawningRolesBullet('Spawns: ', spawnableRoles);

    pushBulletPoints([spawningRolesBullet]);

    if(!window.gameState.started)
        $('.role_settings_pane').show();
    else
        $('#game_play_card').show();
}

function setRoleInfo(){
    const index = require('../index');
    const factionDetailsView = require('./factionDetailsView');
    const setup = window.gameState.setup;
    const factionRole = setup.factionRoleMap[window.gameState.activeFactionRoleID];
    if(!window.gameState.started)
        $('.role_settings_pane').show();

    const textColor = setup.factionMap[factionRole.factionID].color;

    const subHeaderText = setup.factionMap[factionRole.factionID].name;
    const subHeaderNameAttr = factionRole.factionID;

    const abilityNames = new Set(factionRole.abilities.map(ability => ability.name.toLowerCase()));
    const imageNames = roleImages.intersection(abilityNames);
    const imageName = imageNames.size ? [...imageNames][0] : 'citizen';

    const bulletPoints = [...factionRole.details];

    const otherVersions = getOtherVersionsBullet(setup, factionRole);
    if(otherVersions)
        bulletPoints.push(otherVersions);
    bulletPoints.push(getSpawnedFromBullet(setup, factionRole));

    // $("#role_exp_extras").css('font-size', '100%');

    const header = $('.notTeamCard .roleCardHeader');
    const subHeader = $('.notTeamCard .roleCardSubHeader');
    subHeader.show();
    subHeader.text(subHeaderText);
    subHeader.css('color', textColor);
    subHeader.attr('name', subHeaderNameAttr);
    subHeader.addClass('roleHover');
    subHeader.unbind();
    if(window.gameState.isStarted)
        subHeader.click(() => {
            index.setActiveFaction(factionRole.factionID);
            factionDetailsView.refresh();
        });
    header.text(factionRole.name);
    header.css('color', textColor);

    index.setImage($('.notTeamCard'), imageName);

    pushBulletPoints(bulletPoints);

    // called only when the game is in setup page, and the 'role card' has rules.
    if(!window.gameState.started && index.isHostsSetup())
        refreshRoleModifierInputs(factionRole, setup);

    if(window.gameState.isStarted)
        $('#game_play_card').show();
}

function pushBulletPoints(bulletPoints){
    const container = window.gameState.started ? $('#role_exp_extras')
        : $('#setup_role_modifiers');
    container.empty();
    bulletPoints.forEach(point => {
        addTextToRuleExtras(point, container);
    });

    if(bulletPoints.length)
        container.show();
    else
        container.hide();
}

function setDeadPlayerInRoleSettings(){
    const index = require('../index');
    const factionDetailsView = require('./factionDetailsView');
    const dead = window.gameState.graveyard
        .find(grave => grave.name === window.gameState.activePlayerName);
    const deadFaction = window.gameState.setup.factionMap[dead.factionID];
    const color = deadFaction.color;
    const cleaned = !dead.factionID;

    const header = $('.roleCardHeader').empty();
    header.append(`${dead.name} (`);
    const span = $('<span>');
    span.text(dead.roleName);
    // if(!cleaned)
    //     span.addClass('roleHover');

    header.append(span);
    header.append(')');

    header.css('color', color);

    const subHeader = $('.roleCardSubHeader');
    subHeader.unbind();
    subHeader.show();
    if(!cleaned){
        subHeader.text(deadFaction.name);
        subHeader.addClass('roleHover');
        subHeader.click(() => {
            index.setActiveFaction(deadFaction.id);
            factionDetailsView.refresh();
        });
    }else{
        subHeader.text('Unknown Team');
        subHeader.removeClass('roleHover');
    }
    subHeader.css('color', color);


    index.setImage($('#game_play_card'), 'citizen');

    let descText;
    if(dead.phase)
        descText = 'Day';
    else
        descText = 'Night';
    descText = `Died on ${descText} ${dead.day}`;

    $('.roleCardExplanationText').text(descText);

    $('#role_exp_extras').empty();
    $('#role_exp_extras').show();

    const dTypes = dead.deathTypes;
    let li;
    for(let i = 0; i < dTypes.length; i++){
        li = $('<li>');
        li.text(dTypes[i]);
        $('#role_exp_extras').append(li);
        li.addClass('rule_game_info_extras');
    }
    if(dead.lastWill){
        li = $('<li>');
        li.text('Last Will');
        li.css('text-align', 'center');
        li.css('font-weight', 'bold');
        $('#role_exp_extras').append(li);

        li = $('<li>');
        li.text(dead.lastWill);
        $('#role_exp_extras').append(li);
        li.addClass('rule_game_info_extras');
    }
}

function clear(){
    $('#roleDescriptionLabel').text('');
    $('#roleDescriptionText').text('');
    $('#role_catalogue .roleCardSettingsAdapter').empty();

    $('#role_settings_pic').hide();
}

function getSpawningRolesBullet(pretext, roles){
    const index = require('../index');
    const factionDetailsView = require('./factionDetailsView');
    const factionsView = require('./factionsView');
    const rolesView = require('./rolesView');

    const textArray = [pretext];
    roles.forEach((role, counter) => {
        if(counter)
            textArray.push(', ');
        textArray.push({
            text: role.name,
            className: `roleID${role.id}`,
            color: window.gameState.setup.factionMap[role.factionID].color,
            onClick: () => {
                if(window.gameState.isStarted)
                    index.setActiveFaction(null);
                else
                    index.setActiveFaction(role.factionID);
                index.setActiveFactionRole(role.id);
                refresh();
                rolesView.refresh();
                if(!window.gameState.isStarted){
                    factionDetailsView.refresh();
                    factionsView.refresh();
                }
            },
        });
    });
    return textArray;
}

function getSpawnedFromBullet(setup, factionRole){
    const index = require('../index');
    const factionDetailsView = require('./factionDetailsView');
    const factionsView = require('./factionsView');
    const rolesView = require('./rolesView');
    let hiddens = setup.setupHiddens
        .map(setupHidden => setup.hiddenMap[setupHidden.hiddenID])
        .filter(hidden => hidden.factionRoleIDs.includes(factionRole.id));
    if(!hiddens.length)
        return 'This role cannot currently be spawned.';
    hiddens = hiddens.filter(hidden => hidden.factionRoleIDs.length > 1);
    if(!hiddens.length)
        return 'This role is guaranteed to be in the game.';

    const hiddenSet = new Set();
    const textArray = ['Spawnable by: '];
    hiddens.forEach((hidden, counter) => {
        if(hiddenSet.has(hidden.id))
            return;
        hiddenSet.add(hidden.id);
        if(counter)
            textArray.push(', ');
        textArray.push({
            text: hidden.name,
            className: `hiddenID${hidden.id}`,
            color: hiddenUtils.getColor(hidden),
            onClick: () => {
                index.setActiveHidden(hidden.id);
                if(!window.gameState.isStarted)
                    index.setActiveFaction(index.RANDOMS_SELECTED_FACTION_ID);
                refresh();
                rolesView.refresh();
                if(!window.gameState.isStarted)
                    factionDetailsView.refresh();
                factionsView.refresh();
            },
        });
    });
    textArray.push('.');
    return textArray;
}

function getOtherVersionsBullet(setup, factionRole){
    const otherVersions = Object.values(setup.factionMap)
        .map(faction => faction.factionRoles)
        .flat()
        .filter(otherFactionRole => otherFactionRole.id !== factionRole.id
            && otherFactionRole.roleID === factionRole.roleID);
    if(!otherVersions.length)
        return;

    return getSpawningRolesBullet('Also available in: ', otherVersions);
}

function refreshRoleModifierInputs(factionRole, setup){
    const factionRoleService = require('../services/browserFactionRoleService');
    const setupModifierService = require('../services/browserSetupModifierService');
    const index = require('../index');
    const ruleList = $('#setup_role_modifiers');
    const modifiers = factionRole.abilities.map(ability => {
        const setupModifiers = ability.setupModifierNames
            .map(setupModifierName => {
                const setupModifier = setup.setupModifiers[setupModifierName];
                return {
                    value: setupModifier.value,
                    label: setupModifier.label,
                    onClick: async function(){
                        let submitValue;
                        if(typeof(setupModifier.value) !== 'number'){
                            submitValue = $(this).is(':checked');
                        }else{
                            submitValue = $(this).val();
                            if(!submitValue)
                                return;
                            submitValue = parseInt(submitValue, 10);
                        }
                        await setupModifierService.update(setupModifierName, submitValue);
                        await factionRoleService.sync(factionRole.id);
                        refresh();
                        factionRoleService.syncOtherRoles(factionRole.id, setupModifierName);
                    },
                };
            });
        const abilityModifiers = ability.modifiers.map(modifier => ({
            label: modifier.label,
            value: modifier.value,
            onClick: async function(){
                let submitValue;
                if(typeof(modifier.value) !== 'number'){
                    submitValue = $(this).is(':checked');
                }else{
                    submitValue = $(this).val();
                    if(!submitValue)
                        return;
                    submitValue = parseInt(submitValue, 10);
                }
                await factionRoleService.updateAbilityModifier(factionRole.id, ability.id,
                    modifier.name, submitValue);
                await factionRoleService.sync(factionRole.id);
                refresh();
            },
        }));
        return setupModifiers.concat(abilityModifiers);
    });
    const roleModifiers = factionRole.modifiers.map(modifier => ({
        label: modifier.label,
        value: modifier.value,
        onClick: async function(){
            let submitValue;
            if(typeof(modifier.value) !== 'number'){
                submitValue = $(this).is(':checked');
            }else{
                submitValue = $(this).val();
                if(!submitValue)
                    return;
                submitValue = parseInt(submitValue, 10);
            }
            await factionRoleService.updateRoleModifier(factionRole.id, modifier.name, submitValue);
            await factionRoleService.sync(factionRole.id);
            refresh();
        },
    }));

    modifiers.concat(roleModifiers)
        .flat().forEach(rule => {
            const element = $('<div>');
            index.createRuleElement(rule, element, rule.onClick);
            ruleList.append(element);
        });
}
