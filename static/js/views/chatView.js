/* eslint-env browser, jquery */
/* global SimpleBar */
const helpers = require('../util/browserHelpers');


function fillEleWithMessage(li, message, content){
    const color = '#C6D8FB';
    if(message.messageType === 'ChatMessage' || helpers.isOnLobby()){
        const fpic = $('<i>').addClass('chatPic');
        fpic.css('color', color);

        const speaker = $('<span>').addClass('speakerLabel');
        speaker.html(message.sender);
        speaker.find('font').css('color', color);

        li.attr('name', speaker.text());
        fpic.addClass(message.icon);

        const mText = $('<span>').addClass('speakerText');
        mText.html(message.text);

        if(!content || content.children().last().attr('name') !== speaker.text()){
            li.append(fpic);
            li.append(speaker);
        }
        li.append(mText);
    }else{
        li.append($('<span>').html(message.text));
    }
}

function hideChatElements(){
    if(helpers.isMobile()){
        $('#chat_tabs_wrapper').hide();
        $('.chat_pane').hide();
        $('#puppetList').hide();
        hideChatInput();
        if(helpers.isLoggedIn() || !helpers.isOnLobby()){
            $('.chat_ticker').show();
            $('#chat_ticker_pane').show();
        }
    }
}

function hideChatInput(){
    $('body').removeClass('chat_active puppetsShowing');
}

function refresh(){
    const { chatQueue } = require('../services/browserChatService');
    if(!chatQueue.length)
        return setTimeout(refresh, 30);

    const chatUL = getChatContainer();

    const sl = new SimpleBar(chatUL[0]);
    const content = $(sl.getContentElement());
    const scrollElement = $(sl.getScrollElement());

    function chatScroll(){
        const liHeight = chatUL.find('li').last().height();
        const scrollTop = $(this).scrollTop();
        if(scrollTop + $(this).innerHeight() + liHeight >= this.scrollHeight)
            window.gameState.scrollToBottom = true;
        else
            window.gameState.scrollToBottom = false;
    }

    scrollElement.unbind();

    let wasDay = null;
    let lastDay;
    for(let i = 0; i < chatQueue.length; i++){
        let message = chatQueue[i];
        const messageType = message.messageType;

        if(lastDay === undefined || lastDay !== message.day || wasDay !== message.isDay){
            lastDay = message.day;
            wasDay = message.isDay;
            let headerText;
            if(wasDay)
                headerText = 'Day';
            else
                headerText = 'Night';
            headerText += ' ';
            if(messageType === 'DeathAnnouncement' && message.nightDeath)
                headerText += (lastDay - 1);
            else
                headerText += lastDay;
            if(!hasHeaderText(content, headerText) && headerText !== 'Day 0'){
                message = {
                    text: `<div class='headerLabel'><u>${headerText}</u></div>`,
                };
                i--;
            }
        }

        const li = $('<li>');

        fillEleWithMessage(li, message, content);

        if(message.voteIndex || message.voteIndex === 0)
            if(messageType === 'VoteAnnouncement' || messageType === 'DeathAnnouncement'){
                li.addClass('roleHover');
                li.attr('name', message.voteIndex);
                li.click(voteItemClick);
            }


        content.append(li);
    }
    sl.recalculate();
    chatQueue.length = 0;

    if(window.gameState.scrollToBottom){
        const element = scrollElement[0];
        if(element)
            element.scrollTop = element.scrollHeight;
    }

    setTimeout(() => {
        scrollElement.on('scroll', chatScroll);
        setTimeout(refresh, 30);
    }, 10);
}

function setScrolledToBottom(){
    window.gameState.scrollToBottom = true;
}

module.exports = {
    fillEleWithMessage,
    hideChatElements,
    hideChatInput,
    refresh,
    setScrolledToBottom,
};

function getChatContainer(){
    if(helpers.isOnLobby())
        return $('#lobby_messages');
    if(window.gameState.started)
        return $('#messages');
    return $('#pregame_messages');
}

function hasHeaderText(ul, text){
    let hasText = false;
    ul.children('li').each(function(){
        if($(this).text() === text)
            hasText = true;
    });
    return hasText;
}

function voteItemClick(){
    const index = require('../index');
    const voteIndex = $(this).attr('name');
    let dayLabel = $('.activeTab').text();
    dayLabel = parseInt(dayLabel.replace('Day ', ''), 10);
    window.gameState.voteView.day = dayLabel;
    window.gameState.voteView.counter = parseInt(voteIndex, 10) + 1;
    hideChatElements();
    require('./voteView').refresh();
    index.hideLeftPane();
    $('#vote_recap_pane').show();
}
