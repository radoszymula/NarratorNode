/* eslint-env jquery */


function GameState(userID){
    this.scrollToBottom = true;

    this.userID = userID;

    this.started = false;
    this.isPrivateInstance = false;
    this.host = {};
    this.commandsIndex = 0;
    this.timer = null;
    this.isAlive = true;
    this.isObserver = false;
    this.hiddenRandoms = false;

    this.activeFactionID = null;
    this.activeRole = null;

    this.activeTeams = [];

    this.deadPlayers = [];

    this.chats = {};
    this.chatKeys = {};
    this.unreadChats = {};
    // this.feedback = [];

    this.voteView = {
        day: -1,
        counter: 1,
    };
    this.voteInfo = {
        voteCounts: [],
    };

    this.activeUserIDs = new Set();
    this.setup = null;
}

GameState.prototype.endedNight = function(){
    // ideally checking if the name is in the playerMap would NEVER happen.  TODO test refactor
    return this.profile && this.playerMap && this.playerMap[this.profile.name]
        && this.playerMap[this.profile.name].endedNight;
};

GameState.prototype.isDay = function(){
    return this.isStarted && this.phase && this.phase.name !== 'NIGHT_ACTION_SUBMISSION'
        && !this.isOver();
};

GameState.prototype.isHost = function(){
    return this.host.id === this.userID;
};

GameState.prototype.isInGame = function(){
    return !$.isEmptyObject(this.host);
};

GameState.prototype.isOver = function(){
    return this.isStarted && this.phase && this.phase.name === 'FINISHED';
};

GameState.prototype.getDisplayName = function(){
    return this.role ? this.role.displayName : null;
};

GameState.prototype.getRuleValue = function(ruleName){
    return this.setup && this.setup.setupModifiers
        && this.setup.setupModifiers[ruleName.toUpperCase()].value;
};

module.exports = {
    GameState,
};
