const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');


module.exports = {
    entry: {
        condorcet: './static/js/condorcet.js',
        index: './static/js/index.js',
    },
    output: {
        path: path.resolve(__dirname, 'public/js'),
    },
    optimization: {
        minimizer: [new TerserPlugin()],
    },
};
