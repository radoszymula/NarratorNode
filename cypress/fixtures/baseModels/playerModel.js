import {
    PLAYER_NAME, PLAYER_NAME2, USER_ID, USER_ID2,
} from '../fakeConstants';


function get(attributes = {}){
    return {
        endedNight: attributes.endedNight || false,
        flip: attributes.flip,
        isComputer: false,
        name: attributes.name || PLAYER_NAME,
        userID: attributes.userID || USER_ID,
    };
}

function get2(attributes = {}){
    return get({
        name: attributes.name || PLAYER_NAME2,
        flip: attributes.flip,
        userID: attributes.userID || USER_ID2,
    });
}

function getBot(attributes = {}){
    const player = get(attributes);
    delete player.userID;
    player.isComputer = true;
    return player;
}


module.exports = {
    get,
    get2,
    getBot,
};
