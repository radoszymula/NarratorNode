import phaseModel from './phaseModel';
import playerModel from './playerModel';
import setupModel from './setupModel';
import userModel from './userModel';

import { GAME_ID } from '../fakeConstants';


function get(attributes = {}){
    return {
        host: attributes.host || userModel.get(),
        id: GAME_ID,
        integrations: attributes.integrations || [],
        isStarted: attributes.isStarted,
        phase: attributes.phase || [phaseModel.getNight()],
        players: attributes.players || [playerModel.get()],
        setup: attributes.setup || setupModel.get(),
    };
}

module.exports = {
    get,
};
