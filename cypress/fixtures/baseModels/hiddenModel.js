import { HIDDEN_ID, HIDDEN_NAME, FACTION_ROLE_ID } from '../fakeConstants';


function get(attributes = {}){
    return {
        id: attributes.id || HIDDEN_ID,
        name: attributes.name || HIDDEN_NAME,
        factionRoleIDs: attributes.factionRoleIDs || [FACTION_ROLE_ID],
    };
}

module.exports = {
    get,
};
