function getDay(){
    return {
        name: 'VOTE_PHASE',
    };
}

function getFinished(){
    return {
        name: 'FINISHED',
    };
}

function getNight(){
    return {
        name: 'NIGHT_ACTION_SUBMISSION',
    };
}

module.exports = {
    getDay,
    getFinished,
    getNight,
};
