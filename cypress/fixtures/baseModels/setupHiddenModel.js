import { HIDDEN_ID, SETUP_HIDDEN_ID } from '../fakeConstants';

function get(attributes = {}){
    return {
        hiddenID: HIDDEN_ID,
        id: attributes.id || SETUP_HIDDEN_ID,
    };
}

module.exports = {
    get,
};
