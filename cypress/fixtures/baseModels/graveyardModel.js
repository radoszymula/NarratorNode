import { FACTION_ID, PLAYER_NAME, ROLE_NAME } from '../fakeConstants';

function get(){
    return {
        day: 1,
        deathTypes: ['Died via day execution'],
        factionID: FACTION_ID,
        name: PLAYER_NAME,
        phase: true,
        roleName: ROLE_NAME,
        // flip: {
        //     factionID: FACTION_ID,
        //     name: ROLE_NAME,
        // },
    };
}

module.exports = {
    get,
};
