import { CHAT_NAME, MESSAGE_TEXT } from '../fakeConstants';


function get(){
    return {
        chat: [CHAT_NAME],
        text: MESSAGE_TEXT,
    };
}

module.exports = {
    get,
};
