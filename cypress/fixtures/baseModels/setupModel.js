import {
    FACTION_ID, FACTION_ID2, FACTION_ROLE_ID, FACTION_ROLE_ID2, USER_ID,
} from '../fakeConstants';

import factionRoleModel from './factionRoleModel';
import factionModel from './factionModel';
import roleModel from './roleModel';
import hiddenModel from './hiddenModel';
import setupHiddenModel from './setupHiddenModel';
import setupModifierModel from './setupModifierModel';

function get(attributes = {}){
    return {
        factions: attributes.factions || [],
        hiddens: attributes.hiddens || [],
        isPreset: attributes.isPreset || false,
        ownerID: USER_ID,
        roles: attributes.roles || [],
        setupHiddens: attributes.setupHiddens || [],
        setupModifiers: getSetupModifiers(attributes.setupModifiers),
    };
}

function getDefaultStarted(attributes = {}){
    return {
        factions: [
            factionModel.get({
                enemyIDs: [FACTION_ID2],
                factionRoles: [factionRoleModel.get()],
            }),
            factionModel.get2({
                enemyIDs: [FACTION_ID],
                factionRoles: [factionRoleModel.get2()],
            }),
        ],
        hiddens: [hiddenModel.get({ factionRoleIDs: [FACTION_ROLE_ID, FACTION_ROLE_ID2] })],
        roles: [roleModel.get(), roleModel.get2()],
        setupHiddens: [setupHiddenModel.get()],
        setupModifiers: getSetupModifiers(attributes.setupModifiers),
    };
}

module.exports = {
    get,
    getDefaultStarted,
};

function getSetupModifiers(input = {}){
    return Object.assign({
        CHARGE_VARIABILITY: setupModifierModel.get({ name: 'CHARGE_VARIABILITY', value: 60 }),
        CULT_PROMOTION: setupModifierModel.get({ name: 'CULT_PROMOTION', value: false }),
        DAY_LENGTH: setupModifierModel.get({ name: 'DAY_LENGTH', value: 60 }),
        HOST_VOTING: setupModifierModel.get({ name: 'HOST_VOTING', value: false }),
        LAST_WILL: setupModifierModel.get({ name: 'LAST_WILL', value: false }),
        NIGHT_LENGTH: setupModifierModel.get({ name: 'NIGHT_LENGTH', value: 60 }),
        SELF_VOTE: setupModifierModel.get({ name: 'SELF_VOTE', value: 60 }),
        SKIP_VOTE: setupModifierModel.get({ name: 'SKIP_VOTE', value: true }),
    }, input);
}
