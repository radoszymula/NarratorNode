import { USER_ID } from '../fakeConstants';

function get(attributes = {}){
    return {
        id: attributes.id || USER_ID,
    };
}

module.exports = {
    get,
};
