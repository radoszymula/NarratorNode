module.exports = {
    ABILITY_ID: 9999,
    ABILITY_NAME: 'fakecheck',

    CHAT_NAME: 'Day 1',

    COLOR: '#6495ED',
    COLOR2: '#FF0000',

    FACTION_ID: 2222,
    FACTION_ID2: 6666,
    FACTION_NAME: 'FakeMafia', // these need to be alphabetical for tests
    FACTION_NAME2: 'FakeTown',

    FACTION_ROLE_ID: 8888,
    FACTION_ROLE_ID2: 88188,

    JOIN_ID: 'ABCD',

    GAME_ID: 112211,

    HIDDEN_NAME: 'FakeTown Hidden',
    HIDDEN_ID: 3333,
    HIDDEN_ID2: 4444,

    MESSAGE_TEXT: 'I am sending a fake message',

    PLAYER_NAME: 'playerAlpha',
    PLAYER_NAME2: 'playerBeta',
    PLAYER_NAME3: 'playerCharlie',

    ROLE_ABILITY_MODIFIER_NAME: 'ROLE_ABILITY_MODIFIER',
    ROLE_ID: 1111,
    ROLE_ID2: 5555,
    ROLE_MODIFIER_NAME: 'ROLE_MODIFIER_NAME',
    ROLE_NAME: 'FakeCitizen',
    ROLE_NAME2: 'FakeGoon',

    SETUP_HIDDEN_ID: 777,

    SETUP_MODIFIER_NAME: 'GLOBAL_ABILITY_ATTRIBUTE_MODIFIER',
    SETUP_NAME: 'setupName',
    SETUP_ID: 444111444,

    USER_ID: 1,
    USER_ID2: 2,
};
