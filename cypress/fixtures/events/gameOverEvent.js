import phaseModel from '../baseModels/phaseModel';


function get(){
    return {
        event: 'gameEnd',
        phase: phaseModel.getFinished(),
    };
}

module.exports = {
    get,
};
