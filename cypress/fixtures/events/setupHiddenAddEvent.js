import { HIDDEN_ID, SETUP_HIDDEN_ID } from '../fakeConstants';


function get(attributes = {}){
    return {
        event: 'setupHiddenAdd',
        hiddenID: HIDDEN_ID,
        setupHiddenID: attributes.setupHiddenID || SETUP_HIDDEN_ID,
    };
}

module.exports = {
    get,
};
