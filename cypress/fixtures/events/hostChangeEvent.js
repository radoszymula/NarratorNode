import userModel from '../baseModels/userModel';


function get(attributes = {}){
    return {
        event: 'hostChange',
        host: attributes.host || userModel.get({ id: attributes.hostID }),
    };
}

module.exports = {
    get,
};
