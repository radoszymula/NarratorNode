import setupModel from '../baseModels/setupModel';
import userModel from '../baseModels/userModel';

import { PLAYER_NAME } from '../fakeConstants';


function get(attributes = {}){
    return {
        event: 'playerExit',
        host: attributes.host || userModel.get({ id: attributes.id }),
        playerName: attributes.playerName || PLAYER_NAME,
        setup: attributes.setup || setupModel.get(),
    };
}

module.exports = {
    get,
};
