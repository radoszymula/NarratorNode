const setupModel = require('../baseModels/setupModel');


function get(setup){
    if(setup)
        setup = JSON.parse(JSON.stringify(setup));
    else
        setup = setupModel.get();
    return {
        event: 'setupChange',
        setup,
    };
}

module.exports = {
    get,
};
