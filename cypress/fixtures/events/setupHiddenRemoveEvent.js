import { HIDDEN_ID, SETUP_HIDDEN_ID } from '../fakeConstants';


function get(){
    return {
        event: 'setupHiddenRemove',
        hiddenID: HIDDEN_ID,
        setupHiddenID: SETUP_HIDDEN_ID,
    };
}

module.exports = {
    get,
};
