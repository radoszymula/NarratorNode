const profileModel = require('../baseModels/profileModel');
const setupModel = require('../baseModels/setupModel');


function get(attributes = {}){
    return {
        event: 'dayStart',
        profile: attributes.profile || profileModel.getInGame(),
        setup: setupModel.getDefaultStarted(),
    };
}

module.exports = {
    get,
};
