import {
    HIDDEN_ID,
} from '../fakeConstants';


function post(){
    return {
        response: {
            hiddenID: HIDDEN_ID,
        },
    };
}

module.exports = {
    post,
};
