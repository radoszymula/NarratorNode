import userModel from '../baseModels/userModel';


function repickHost(userID){
    return {
        response: userModel.get({ id: userID }),
    };
}

module.exports = {
    repickHost,
};
