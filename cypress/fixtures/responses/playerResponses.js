import setupModel from '../baseModels/setupModel';
import userModel from '../baseModels/userModel';


function addBots(attributes = {}){
    return {
        response: {
            players: attributes.players,
            setup: setupModel.get(),
        },
    };
}

function kick(){
    return {
        response: {
            host: userModel.get(),
            setup: setupModel.get(),
        },
    };
}

module.exports = {
    addBots,
    kick,
};
