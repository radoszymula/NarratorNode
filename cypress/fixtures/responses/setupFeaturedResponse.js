function get(){
    return {
        response: [{
            key: 'setupName',
            name: 'Fake Setup Name',
            description: [],
        }],
        errors: [],
    };
}

module.exports = {
    get,
};
