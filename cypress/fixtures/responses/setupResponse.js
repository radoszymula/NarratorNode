import setupModel from '../baseModels/setupModel';

import { SETUP_ID } from '../fakeConstants';


function change(attributes = {}){
    return {
        response: {
            setup: attributes.setup || setupModel.get(),
        },
    };
}

function create(){
    return {
        response: {
            id: SETUP_ID,
        },
    };
}

module.exports = {
    change,
    create,
};
