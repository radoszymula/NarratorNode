import {
    FACTION_ROLE_ID,
    HIDDEN_ID,
    HIDDEN_NAME,
} from '../fakeConstants';


function post(){
    return {
        response: {
            id: HIDDEN_ID,
            name: HIDDEN_NAME,
            factionRoleIDs: [FACTION_ROLE_ID],
        },
    };
}

module.exports = {
    post,
};
