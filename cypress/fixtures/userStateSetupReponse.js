import setupModel from './baseModels/setupModel';
import userModel from './baseModels/userModel';

import { PLAYER_NAME } from './fakeConstants';


function get(attributes = {}){
    return {
        factions: {
            factionNames: [],
        },
        gameStart: attributes.isStarted || false,
        guiUpdate: true,
        host: attributes.host || userModel.get(),
        isPrivateInstance: true,
        rules: {
            CHARGE_VARIABILITY: {
                val: 0,
            },
            CHAT_ROLES: {
                val: true,
            },
            DAY_LENGTH: {
                val: 60,
            },
            DAY_START: {
                val: true,
            },
            DIFFERENTIATED_FACTION_KILLS: {
                val: false,
            },
            HOST_VOTING: {
                val: false,
            },
            LAST_WILL: {
                val: false,
            },
            NIGHT_LENGTH: {
                val: 60,
            },
            OMNISCIENT_DEAD: {
                val: 7,
            },
            PUNCH_ALLOWED: {
                val: false,
            },
            SELF_VOTE: {
                val: false,
            },
        },
        setup: setupModel.get(),
        type: ['rules'],
    };
}

function getStartedGame(attributes = {}){
    const response = get({
        isStarted: true,
    });
    if(!attributes.profile)
        response.profile = {
            name: PLAYER_NAME,
        };
    return response;
}

module.exports = {
    get,
    getStartedGame,
};
