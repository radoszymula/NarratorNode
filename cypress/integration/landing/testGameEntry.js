import landingPane from '../../pages/landing/landingPane';
import routesPage from '../../pages/routesPage';
import { JOIN_ID } from '../../../web/test/fakeConstants';


describe('Game entry test', () => {
    context('Joining game', () => {
        it('Will go to setup upon successful join', () => {
            cy.goToHome();

            landingPane.joinGame(JOIN_ID);

            routesPage.assertOnSetup();
        });
    });
});
