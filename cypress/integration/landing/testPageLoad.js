import setupFeaturedResponse from '../../fixtures/responses/setupFeaturedResponse';

import toasts from '../../pages/toasts';


describe('Page load test', () => {
    context('Page load test', () => {
        it('will show the host button', () => {
            let fixtureCount = 0;
            cy.server();
            cy.route({
                method: 'GET',
                url: 'setups',
                response: setupFeaturedResponse.get(),
                onResponse: () => {
                    fixtureCount++;
                },
            });

            cy.visit('http://localhost:4501');

            cy.get('#hostPublicButton').should('be.visible');
            cy.wait(100).then(() => {
                expect(fixtureCount).to.be.eq(1);
            });
        });
    });

    context('Auth errors', () => {
        it('Will show an error when the auth token doesn\'t exist', () => {
            const authToken = new Array(128 + 1).join('A');
            cy.server();
            cy.route({
                method: 'GET',
                url: `users?auth_token=${authToken}`,
                status: 422,
                response: { errors: ['Auth token not found.'] },
                failOnStatusCode: false,
            });
            cy.route({
                method: 'GET',
                url: 'setups',
                response: setupFeaturedResponse.get(),
            });

            cy.visit(`http://localhost:4501/?auth_token=${authToken}`);

            toasts.assertText('Woops! This game link is no longer active.');
        });
    });
});
