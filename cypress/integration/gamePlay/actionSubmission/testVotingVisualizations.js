import actionPane from '../../../pages/gamePlay/actionPane';
import tabs from '../../../pages/gamePlay/tabs';

import { PLAYER_NAME } from '../../../fixtures/fakeConstants';

import phaseModel from '../../../fixtures/baseModels/phaseModel';


function getAbilities(){
    return {
        Vote: {
            canAddAction: true,
            options: {},
            players: [{
                playerName: PLAYER_NAME,
            }],
        },
        type: ['Vote'],
    };
}

describe('Test Voting Visualizations', () => {
    context('Clicking vote actions', () => {
        it('Will submit a skip vote to the server', () => {
            const abilities = getAbilities();
            const phase = phaseModel.getDay();
            cy.goToGame({ abilities, phase });
            tabs.clickActions();

            actionPane.clickLeftSubmit('Skip Day', {
                onRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.be.eql({ message: 'skip day' });
                },
                response: {
                    isSkipping: true,
                    voteInfo: {
                        currentVotes: {
                            [PLAYER_NAME]: 'Skip Day',
                        },
                    },
                },
            });

            actionPane.assertClicked('Skip Day');
        });
    });
});
