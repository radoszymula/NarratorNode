import phaseModel from '../../fixtures/baseModels/phaseModel';
import playerModel from '../../fixtures/baseModels/playerModel';
import setupModel from '../../fixtures/baseModels/setupModel';
import setupModifierModel from '../../fixtures/baseModels/setupModifierModel';

import settingsPane from '../../pages/gamePlay/settingsPane';
import tabs from '../../pages/gamePlay/tabs';

import { PLAYER_NAME, PLAYER_NAME2 } from '../../fixtures/fakeConstants';


describe('Test Settings', () => {
    context('When viewing the settings', () => {
        it('Will have defaults colored', () => {
            cy.goToGame();

            tabs.clickSettings();

            settingsPane.assertNightMusicOn();
        });
    });

    context('On force end via skip', () => {
        it('Will send a mass skip day request', () => {
            const phase = phaseModel.getDay();
            const players = [playerModel.get(), playerModel.get2()];
            const setup = setupModel.getDefaultStarted({
                setupModifiers: {
                    HOST_VOTING: setupModifierModel.get({
                        name: 'HOST_VOTING',
                        value: true,
                    }),
                },
            });
            cy.goToGame({ phase, players, setup });
            tabs.clickSettings();

            settingsPane.clickForceEndPhase(requestResponse => {
                const body = requestResponse.request.body;
                expect(body.targets).to.have.members([PLAYER_NAME, PLAYER_NAME2]);
            });
        });
    });
});
