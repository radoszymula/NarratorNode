import gameInfo from '../../pages/gamePlay/gameInfo';
import graveyardPane from '../../pages/gamePlay/graveyardPane';
import tabs from '../../pages/gamePlay/tabs';

import graveyardModel from '../../fixtures/baseModels/graveyardModel';
import {
    FACTION_NAME, HIDDEN_NAME, PLAYER_NAME, ROLE_NAME,
} from '../../fixtures/fakeConstants';


describe('Graveyard visualizations', () => {
    context('Viewing graveyard', () => {
        it('Will show the graveyard contents', () => {
            const graveyard = [
                graveyardModel.get(),
            ];
            cy.goToGame({ graveyard });

            tabs.clickGameOverview();

            graveyardPane.assertSetupHiddenCount(1);
        });
    });

    context('Clicking on graveyard', () => {
        it('Will show grave details', () => {
            const graveyard = [
                graveyardModel.get(),
            ];
            cy.goToGame({ graveyard });
            tabs.clickGameOverview();

            graveyardPane.clickFirst();

            gameInfo.assertGraveClicked(PLAYER_NAME);
        });

        it('Will show faction from the grave overview', () => {
            const graveyard = [
                graveyardModel.get(),
            ];
            cy.goToGame({ graveyard });
            tabs.clickGameOverview();
            graveyardPane.clickFirst();

            gameInfo.clickTeamSubheader();

            gameInfo.assertFactionClicked(FACTION_NAME);
        });

        it('Will show nested hidden info from the grave overview', () => {
            const graveyard = [
                graveyardModel.get(),
            ];
            cy.goToGame({ graveyard });
            tabs.clickGameOverview();

            graveyardPane.clickFirst();
            gameInfo.clickTeamSubheader();
            gameInfo.clickRole(ROLE_NAME);
            gameInfo.clickHidden(HIDDEN_NAME);
            gameInfo.clickRole(ROLE_NAME);
            gameInfo.clickHidden(HIDDEN_NAME);

            gameInfo.assertHiddenClicked(HIDDEN_NAME);
        });
    });
});
