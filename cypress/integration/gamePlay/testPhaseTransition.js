import deprecatedAbilitiesModel from '../../fixtures/baseModels/deprecatedAbilitiesModel';
import phaseModel from '../../fixtures/baseModels/phaseModel';
import playerModel from '../../fixtures/baseModels/playerModel';

import dayStartEvent from '../../fixtures/events/dayStartEvent';
import gameOverEvent from '../../fixtures/events/gameOverEvent';
import nightStartEvent from '../../fixtures/events/nightStartEvent';

import actionPane from '../../pages/gamePlay/actionPane';
import tabs from '../../pages/gamePlay/tabs';

import { PLAYER_NAME, PLAYER_NAME2 } from '../../fixtures/fakeConstants';


describe('Test phase transition visualisations', () => {
    context('On day to night', () => {
        it('Will show the end night button', () => {
            const phase = phaseModel.getDay();
            const nightStartEventObj = nightStartEvent.get();
            cy.goToGame({ phase });
            tabs.clickActions();

            cy.window().then(async window => {
                await window.handleObject(nightStartEventObj);
                cy.wait(2000);

                actionPane.assertActionButtonVisible();
                actionPane.assertActionButtonText('Skip to Daytime');
            });
        });
    });

    context('On night to day', () => {
        it('Will reset views', () => {
            const phase = phaseModel.getDay();
            const players = [playerModel.get({
                endedNight: true,
            })];
            const dayStartEventObj = dayStartEvent.get();
            cy.goToGame({ phase, players });
            tabs.clickActions();

            cy.window().then(async window => {
                await window.handleObject(dayStartEventObj);
                cy.wait(2000);

                // it would be 'Waiting on', if ended night wasn't reset on phase reset
                actionPane.assertHeader('kill');
            });
        });
    });

    context('On game over', () => {
        it('Will show the game over screen', () => {
            const players = [playerModel.get(), playerModel.get2()];
            const gameOverEventObj = gameOverEvent.get();
            cy.goToGame({
                abilities: deprecatedAbilitiesModel.getEmpty(),
                players,
            });
            tabs.clickActions();

            cy.window().then(async window => {
                await window.handleObject(gameOverEventObj);
                cy.wait(2000);
                actionPane.hoverLeft(PLAYER_NAME2);

                // it would be 'Waiting on', if ended night wasn't reset on phase reset
                actionPane.assertHeader('Game Over');
                actionPane.assertVisible([PLAYER_NAME, PLAYER_NAME2]);
                actionPane.assertNotVisible(['undefined']);
                actionPane.assertRightNotHovered(PLAYER_NAME2);
                tabs.assertLeaveButtonShowing();
                actionPane.assertActionButtonText('Leave Game');
            });
        });
    });
});
