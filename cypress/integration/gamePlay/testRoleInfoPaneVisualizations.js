import {
    FACTION_NAME,
    FACTION_NAME2,
    PLAYER_NAME, PLAYER_NAME2, ROLE_NAME,
    ROLE_NAME2,
} from '../../fixtures/fakeConstants';

import profileAllyModel from '../../fixtures/baseModels/profileAllyModel';
import profileModel from '../../fixtures/baseModels/profileModel';

import guiUpdateEvent from '../../fixtures/events/guiUpdateEvent';

import gameInfo from '../../pages/gamePlay/gameInfo';
import roleInfo from '../../pages/gamePlay/roleInfo';
import tabs from '../../pages/gamePlay/tabs';


describe('Role Info Visualizations', () => {
    context('Viewing info page', () => {
        it('Will show the appropriate info', () => {
            const profile = profileModel.getInGame({
                allies: [profileAllyModel.get({
                    name: PLAYER_NAME2,
                })],
            });
            cy.goToGame({ profile });

            tabs.clickRoleOverview();

            roleInfo.assertNameVisible(PLAYER_NAME);
            roleInfo.assertRoleVisible(ROLE_NAME);
            roleInfo.assertDetails(profileModel.getInGame().roleCard.details);
            roleInfo.assertAllyVisible(PLAYER_NAME2);
        });
    });

    context('Clicking to info pane', () => {
        it('Will change panes to info on faction click', () => {
            cy.goToGame();
            tabs.clickRoleOverview();

            roleInfo.clickEnemyFaction(FACTION_NAME2);

            tabs.assertGameOverviewSelected();
            gameInfo.assertFactionClicked(FACTION_NAME2);
            gameInfo.assertImageVisible();
            gameInfo.assertEnemiesShown(FACTION_NAME);
            gameInfo.assertFactionRoleShown(ROLE_NAME2);
        });

        it('Will show faction info on faction click', () => {
            cy.mobile();
            cy.goToGame();
            tabs.clickRoleOverview();

            roleInfo.clickFaction();

            gameInfo.assertFactionClicked(FACTION_NAME);
        });
    });

    context('On phase change', () => {
        it('Will not mess up the info', () => {
            const guiUpdateEventObj = guiUpdateEvent.get();
            cy.goToGame();
            tabs.clickRoleOverview();

            cy.window().then(async window => {
                window.handleObject(guiUpdateEventObj);

                roleInfo.assertOneClickableFaction();
            });
        });
    });
});
