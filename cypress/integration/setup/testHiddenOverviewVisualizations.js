import {
    COLOR, FACTION_ROLE_ID,
    HIDDEN_ID,
    HIDDEN_NAME,
    ROLE_NAME,
} from '../../fixtures/fakeConstants';

import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import hiddenModel from '../../fixtures/baseModels/hiddenModel';
import setupHiddenModel from '../../fixtures/baseModels/setupHiddenModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import factionUL from '../../pages/setup/factionUL';
import roleOverview from '../../pages/setup/roleOverview';
import roleUL from '../../pages/setup/roleUL';


function getSetupData(){
    return {
        setup: setupModel.get({
            factions: [factionModel.get({
                factionRoles: [factionRoleModel.get()],
            })],
            hiddens: [hiddenModel.get({
                name: HIDDEN_NAME,
                factionRoleIDs: [FACTION_ROLE_ID],
            })],
            setupHiddens: [setupHiddenModel.get({
                hiddenID: HIDDEN_ID,
            })],
        }),
    };
}

describe('Role Details Visualizations', () => {
    context('Viewing hidden details', () => {
        it('Will show the roles associated with a hidden', () => {
            const setupData = getSetupData();
            cy.goToSetup(setupData);
            factionUL.clickHiddens();

            roleUL.clickFirst();

            roleOverview.assertVisible();
            roleOverview.assertHeader(HIDDEN_NAME);
            roleOverview.assertHeaderColor(COLOR);
            roleOverview.assertVisibleModifiers([ROLE_NAME]);
        });
    });
});
