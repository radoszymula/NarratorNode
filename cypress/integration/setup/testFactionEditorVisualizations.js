import {
    COLOR2,
    FACTION_ID,
    FACTION_NAME, FACTION_ROLE_ID, FACTION_ROLE_ID2,
    HIDDEN_ID,
    HIDDEN_NAME,
} from '../../fixtures/fakeConstants';

import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import factionEditor from '../../pages/setup/factionEditor';
import factionUL from '../../pages/setup/factionUL';
import setupHiddenUL from '../../pages/setup/setupHiddenUL';


function getSetupData(){
    return {
        setup: setupModel.get({
            factions: [factionModel.get({
                modifiers: [{
                    name: 'WIN_PRIORITY',
                    value: 0,
                }],
                factionRoles: [factionRoleModel.get()],
            })],
        }),
    };
}

describe('Faction Editor Visualizations', () => {
    context('Opening editor', () => {
        it('Will open the team editor', () => {
            cy.goToSetup(getSetupData());

            cy.get('#team_catalogue_pane li:first i').click();

            cy.get('#teamEditorPane').should('be.visible');
        });
    });

    context('Deleting factions', () => {
        it('Will close the editor after a successful delete', () => {
            cy.goToSetup(getSetupData());
            cy.get('.customizability .settings_off').click();
            cy.get('#team_catalogue_pane li:first i').click();
            cy.route({
                method: 'DELETE',
                url: `factions/${FACTION_ID}`,
                response: '',
            }).as('factionDeleteResponse');

            cy.get('#teamDelete').click();
            cy.wait(['@factionDeleteResponse']);

            factionEditor.assertHidden();
            factionUL.assertNoActiveFactions();
            factionUL.assertTeamDoesNotExist(FACTION_NAME);
        });

        it('Will change the color of multi-faction setup hiddens', () => {
            const data = getSetupData();
            data.setup.factions.push(factionModel.get2({
                factionRoles: [factionRoleModel.get2()],
                modifiers: {
                    WIN_PRIORITY: 0,
                },
            }));
            data.setup.setupHiddens = [{
                hiddenID: HIDDEN_ID,
            }];
            data.setup.hiddens = [{
                id: HIDDEN_ID,
                name: HIDDEN_NAME,
                factionRoleIDs: [FACTION_ROLE_ID, FACTION_ROLE_ID2],
            }];
            cy.goToSetup(data);
            cy.get('.customizability .settings_off').click();
            cy.get('#team_catalogue_pane li:first i').click();
            cy.route({
                method: 'DELETE',
                url: `factions/${FACTION_ID}`,
                response: '',
            }).as('factionDeleteResponse');

            cy.get('#teamDelete').click();
            cy.wait(['@factionDeleteResponse']);

            setupHiddenUL.assertColor(COLOR2);
        });
    });
});
