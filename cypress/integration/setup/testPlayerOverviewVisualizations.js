import playerModel from '../../fixtures/baseModels/playerModel';

import playerExitEvent from '../../fixtures/events/playerExitEvent';

import generalSettings from '../../pages/setup/generalSettings';
import playerOverview from '../../pages/setup/playerOverview';
import playerUL from '../../pages/setup/playerUL';


describe('Player Overview Visualizations', () => {
    context('Repicking players', () => {
        it('Should submit a host reassignment', () => {
            const player2 = playerModel.get2();
            cy.goToSetup({
                players: [playerModel.get(), player2],
            });
            playerUL.click(player2.name);

            playerOverview.clickSetAsHost(player2.name, player2.userID);

            playerOverview.assertNoRepickOption();
            playerUL.assertHost(player2.name);
            generalSettings.assertNoHostControls();
        });
    });

    context('Kicking players', () => {
        it('Should remove the player from the screen', () => {
            const player2 = playerModel.get2();
            cy.goToSetup({
                players: [playerModel.get(), player2],
            });
            playerUL.click(player2.name);

            playerOverview.clickKick(player2.userID);

            playerUL.assertNameNotInUL(player2.name);
            playerOverview.assertInvisible();
        });
    });

    context('On exiting player event', () => {
        it('Should hide the player overview', () => {
            const player2 = playerModel.get2();
            cy.goToSetup({
                players: [playerModel.get(), player2],
            });
            playerUL.click(player2.name);
            const playerExitEventObj = playerExitEvent.get({
                playerName: player2.name,
            });

            cy.window().then(async window => {
                await window.handleObject(playerExitEventObj);
                cy.wait(2000);

                playerOverview.assertInvisible();
                playerUL.assertHeaderCount(1);
                playerUL.assertNameNotInUL(player2.name);
            });
        });
    });
});
