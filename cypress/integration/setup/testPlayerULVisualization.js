import playerModel from '../../fixtures/baseModels/playerModel';
import userModel from '../../fixtures/baseModels/userModel';

import hostChangeEvent from '../../fixtures/events/hostChangeEvent';
import playerAddedEvent from '../../fixtures/events/playerAddedEvent';
import playerExitEvent from '../../fixtures/events/playerExitEvent';
import playerStatusChangeEvent from '../../fixtures/events/playerStatusChangeEvent';

import generalSettings from '../../pages/setup/generalSettings';
import playerOverview from '../../pages/setup/playerOverview';
import playerUL from '../../pages/setup/playerUL';

import {
    PLAYER_NAME, PLAYER_NAME2, USER_ID, USER_ID2,
} from '../../fixtures/fakeConstants';


describe('Player UL Visualizations', () => {
    context('On load', () => {
        it('Should show the users as active', () => {
            cy.goToSetup({
                activeUserIDs: [USER_ID, USER_ID2],
                players: [
                    playerModel.get(),
                    playerModel.get2(),
                ],
            });

            playerUL.assertActiveHost(PLAYER_NAME);
            playerUL.assertActive(PLAYER_NAME2);
        });

        it('Should show the player count on mobile', () => {
            cy.mobile();
            cy.goToSetup({
                players: [
                    playerModel.get(),
                ],
            });

            playerUL.assertHeaderCount(1);
        });
    });

    context('Clicking player ULs', () => {
        it('Should show the player overview popup for yourself', () => {
            cy.goToSetup();

            playerUL.click(PLAYER_NAME);

            playerOverview.assertVisible();
        });

        it('Should show the player overview popup for others', () => {
            const player2 = playerModel.get2();
            cy.goToSetup({
                players: [playerModel.get(), player2],
            });

            playerUL.click(player2.name);

            playerOverview.assertVisible();
        });
    });

    context('On new player event', () => {
        it('Should show that a new player was added', () => {
            const playerAddedEventObj = playerAddedEvent.get({
                playerName: PLAYER_NAME2,
                players: [
                    playerModel.get(),
                    playerModel.get2(),
                ],
            });
            cy.goToSetup();

            cy.window().then(async window => {
                await window.handleObject(playerAddedEventObj);
                cy.wait(2000);

                playerUL.assertHeaderCount(2);
                playerUL.assertNameInUL(PLAYER_NAME2);
            });
        });
    });

    context('On exited host event', () => {
        it('Should show that the host left', () => {
            const playerExitEventObject = playerExitEvent.get({
                host: userModel.get(),
                playerName: PLAYER_NAME2,
            });
            cy.goToSetup({
                host: userModel.get({ id: USER_ID2 }),
                players: [playerModel.get(), playerModel.get2()],
            });

            cy.window().then(async window => {
                await window.handleObject(playerExitEventObject);
                cy.wait(2000);

                playerUL.assertHeaderCount(1);
                playerUL.assertNameNotInUL(PLAYER_NAME2);
                playerUL.assertHost(PLAYER_NAME);
                generalSettings.assertHostControls();
            });
        });
    });

    context('On host change event', () => {
        it('Should update the player list', () => {
            const hostChangeEventObj = hostChangeEvent.get({
                hostID: USER_ID,
            });
            cy.goToSetup({
                host: userModel.get({ id: USER_ID2 }),
                players: [playerModel.get(), playerModel.get2()],
            });

            cy.window().then(async window => {
                await window.handleObject(hostChangeEventObj);
                cy.wait(2000);

                playerUL.assertHost(PLAYER_NAME);
                generalSettings.assertHostControls();
            });
        });
    });

    context('On player status change event', () => {
        it('Should set the player to inactive', () => {
            const player2 = playerModel.get2();
            cy.goToSetup({
                activeUserIDs: [player2.userID],
                players: [playerModel.get(), player2],
            });
            const playerStatusChangeEventObj = playerStatusChangeEvent.get({
                isActive: false,
                userID: USER_ID2,
            });

            cy.window().then(async window => {
                await window.handleObject(playerStatusChangeEventObj);
                cy.wait(2000);

                playerUL.assertInactive(PLAYER_NAME2);
            });
        });

        it('Should set the player to active', () => {
            const player2 = playerModel.get2();
            cy.goToSetup({
                activeUserIDs: [],
                players: [playerModel.get(), player2],
            });
            const playerStatusChangeEventObj = playerStatusChangeEvent.get({
                isActive: true,
                userID: USER_ID2,
            });

            cy.window().then(async window => {
                await window.handleObject(playerStatusChangeEventObj);
                cy.wait(2000);

                playerUL.assertActive(PLAYER_NAME2);
            });
        });
    });
});
