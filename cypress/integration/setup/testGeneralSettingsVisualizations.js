import {
    FACTION_ID, FACTION_ROLE_ID,
    HIDDEN_ID,
    HIDDEN_NAME,
    ROLE_NAME,
    SETUP_NAME,
} from '../../fixtures/fakeConstants';

import integrationTypes from '../../../web/models/enums/integrationTypes';

const factionModel = require('../../fixtures/baseModels/factionModel');
const factionRoleModel = require('../../fixtures/baseModels/factionRoleModel');
const setupModel = require('../../fixtures/baseModels/setupModel');

const factionOverview = require('../../pages/setup/factionOverview');
const factionUL = require('../../pages/setup/factionUL');
const generalSettings = require('../../pages/setup/generalSettings');
const roleOverview = require('../../pages/setup/roleOverview');
const roleUL = require('../../pages/setup/roleUL');
const setupHiddenUL = require('../../pages/setup/setupHiddenUL');


describe('General Settings Visualization', () => {
    context('Viewing general settins', () => {
        it('Will not show certain settings if discord integration exists', () => {
            cy.goToSetup({
                integrations: [integrationTypes.DISCORD],
            });

            generalSettings.assertNoChatRolesSetting();
            generalSettings.assertNoHostVotingSetting();
        });
    });

    context('Manipulating general settings', () => {
        it('Will update the day length by seconds', () => {
            cy.goToSetup({
                setup: setupModel.get({
                    setupModifiers: {
                        DAY_LENGTH: {
                            value: 60,
                        },
                    },
                }),
            });

            generalSettings.increaseDayLength(120, requestResponse => {
                const { body } = requestResponse.request;
                expect(body.value).to.be.equal(120);
            });

            generalSettings.assertDayLength(2);
        });
    });

    context('Auto Setup Manipulation', () => {
        it('Goes to custom setup from preset', () => {
            cy.goToSetup({
                setupName: SETUP_NAME,
                setup: setupModel.get({
                    isPreset: true,
                }),
            });
            const newSetup = setupModel.get({
                factions: [factionModel.get({
                    description: '',
                    factionRoles: [factionRoleModel.get()],
                })],
                hiddens: [{
                    name: HIDDEN_NAME,
                    id: HIDDEN_ID,
                    factionRoleIDs: [FACTION_ROLE_ID],
                }],
                setupHiddens: [{ hiddenID: HIDDEN_ID }],
            });

            generalSettings.turnOffAutoSetup(newSetup);

            generalSettings.assertSetupDropdownHidden();
            setupHiddenUL.assertSize(1);
            factionUL.assertBackgroundFaction(FACTION_ID);
            roleUL.assertFocusedText(ROLE_NAME);
            roleOverview.assertHeader(ROLE_NAME);
            roleOverview.assertVisible();
            factionOverview.assertVisible();
        });
    });
});
