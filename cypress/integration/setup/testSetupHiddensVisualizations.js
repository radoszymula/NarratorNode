import {
    COLOR,
    FACTION_ID,
    FACTION_ID2, FACTION_ROLE_ID, FACTION_ROLE_ID2,
    HIDDEN_ID,
    HIDDEN_NAME,
    ROLE_NAME,
    ROLE_NAME2, SETUP_HIDDEN_ID,
} from '../../fixtures/fakeConstants';

import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import hiddenModel from '../../fixtures/baseModels/hiddenModel';
import setupHiddenModel from '../../fixtures/baseModels/setupHiddenModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import playerAddedEvent from '../../fixtures/events/playerAddedEvent';
import setupHiddenAddEvent from '../../fixtures/events/setupHiddenAddEvent';
import setupHiddenRemoveEvent from '../../fixtures/events/setupHiddenRemoveEvent';

import factionOverview from '../../pages/setup/factionOverview';
import factionUL from '../../pages/setup/factionUL';
import roleUL from '../../pages/setup/roleUL';
import setupHiddenUL from '../../pages/setup/setupHiddenUL';

function getSetupData(){
    return {
        setup: setupModel.get({
            factions: [
                factionModel.get({
                    factionRoles: [factionRoleModel.get()],
                }),
            ],
        }),
    };
}

describe('Setup Hidden Visualizations', () => {
    context('Setup Hidden clicking', () => {
        it('Will show role information on click', () => {
            const setupData = getSetupData();
            setupData.setup.factions.push(factionModel.get2({
                factionRoles: [factionRoleModel.get2()],
            }));
            setupData.setup.hiddens = [hiddenModel.get({
                name: ROLE_NAME2,
                factionRoleIDs: [FACTION_ROLE_ID2],
            })];
            setupData.setup.setupHiddens.push(setupHiddenModel.get());
            cy.goToSetup(setupData);
            roleUL.clickFirst();

            setupHiddenUL.clickFirst();

            roleUL.assertFocusedText(ROLE_NAME2);
            factionUL.assertBackgroundFaction(FACTION_ID2);
        });

        it('Will show hidden information on click', () => {
            const setupData = getSetupData();
            setupData.setup.factions[0].factionRoles.push(factionRoleModel.get2({
                factionID: FACTION_ID,
            }));
            setupData.setup.hiddens = [hiddenModel.get({
                factionRoleIDs: [FACTION_ROLE_ID, FACTION_ROLE_ID2],
            })];
            setupData.setup.setupHiddens.push(setupHiddenModel.get());
            cy.goToSetup(setupData);

            setupHiddenUL.clickFirst();

            roleUL.assertFocusedText(HIDDEN_NAME);
            factionUL.assertHiddenBackgroundSelected();
            factionOverview.assertInvisible();
        });
    });

    context('RoleList Adding', () => {
        it('Will add a base role with no created hidden to roles list', () => {
            cy.goToSetup(getSetupData());

            roleUL.addUncachedRole();

            setupHiddenUL.assertSize(1);
            setupHiddenUL.assertColor(COLOR);
            setupHiddenUL.assertText(HIDDEN_NAME);
        });

        it('Will add cached setupHidden to roles list', () => {
            const cachedSetupHiddens = getSetupData();
            cachedSetupHiddens.setup.hiddens = [{
                id: HIDDEN_ID,
                name: ROLE_NAME,
                factionRoleIDs: [FACTION_ROLE_ID],
            }];
            cy.goToSetup(cachedSetupHiddens);

            roleUL.addCachedRole();

            setupHiddenUL.assertSize(1);
        });

        it('Will add a nonsingle setupHidden to roles list', () => {
            const cachedSetupHiddens = getSetupData();
            cachedSetupHiddens.setup.hiddens = [hiddenModel.get({
                factionRoleIDs: [FACTION_ROLE_ID, FACTION_ROLE_ID2],
            })];
            cachedSetupHiddens.setup.factions[0].factionRoles.push(factionRoleModel.get2());
            cy.goToSetup(cachedSetupHiddens);

            roleUL.addHidden();

            setupHiddenUL.assertSize(1);
        });
    });

    context('RoleList Removing', () => {
        it('Will delete a setup hidden', () => {
            const setupHiddenData = getSetupData();
            setupHiddenData.setup.hiddens.push({
                id: HIDDEN_ID,
                name: HIDDEN_NAME,
                factionRoleIDs: [FACTION_ROLE_ID],
            });
            setupHiddenData.setup.setupHiddens.push(setupHiddenModel.get());
            cy.goToSetup(setupHiddenData);

            setupHiddenUL.removeSetupHidden();

            setupHiddenUL.assertSize(0);
        });
    });

    context('On a setup hidden add event', () => {
        it('Will show on the UL', () => {
            const setupHiddenAddEventObj = setupHiddenAddEvent.get();
            cy.goToSetup({
                setup: setupModel.get({
                    factions: [factionModel.get({
                        factionRoles: [factionRoleModel.get()],
                    })],
                    hiddens: [hiddenModel.get()],
                    setupHiddens: [],
                }),
            });

            cy.window().then(async window => {
                await window.handleObject(setupHiddenAddEventObj);
                cy.wait(2000);

                setupHiddenUL.assertSize(1);
            });
        });

        it('Will not show more than one setup hidden with the same id', () => {
            const setupHiddenAddEventObj = setupHiddenAddEvent
                .get({ setupHiddenID: SETUP_HIDDEN_ID });
            cy.goToSetup({
                setup: setupModel.get({
                    factions: [factionModel.get({
                        factionRoles: [factionRoleModel.get()],
                    })],
                    hiddens: [hiddenModel.get()],
                    setupHiddens: [
                        setupHiddenModel.get(setupHiddenModel.get()),
                    ],
                }),
            });

            cy.window().then(async window => {
                await window.handleObject(setupHiddenAddEventObj);
                cy.wait(2000);

                setupHiddenUL.assertSize(1);
            });
        });
    });

    context('On a setup hidden remove event', () => {
        it('Will show on the UL', () => {
            const setupHiddenAddEventObj = setupHiddenRemoveEvent.get();
            cy.goToSetup({
                setup: setupModel.get({
                    factions: [factionModel.get({
                        factionRoles: [factionRoleModel.get()],
                    })],
                    hiddens: [hiddenModel.get()],
                    setupHiddens: [setupHiddenModel.get()],
                }),
            });

            cy.window().then(async window => {
                await window.handleObject(setupHiddenAddEventObj);
                cy.wait(2000);

                setupHiddenUL.assertSize(0);
            });
        });
    });

    context('On a new player event', () => {
        it('Should refresh the setup hiddens page', () => {
            const newSetup = setupModel.get({
                factions: [factionModel.get({
                    factionRoles: [factionRoleModel.get()],
                })],
                hiddens: [hiddenModel.get()],
                setupHiddens: [
                    setupHiddenModel.get(),
                ],
            });
            const playerAddedEventObj = playerAddedEvent.get({
                setup: newSetup,
            });
            cy.goToSetup({
                setup: setupModel.get({
                    setupHiddens: [],
                }),
            });

            cy.window().then(async window => {
                await window.handleObject(playerAddedEventObj);
                cy.wait(2000);

                setupHiddenUL.assertSize(1);
            });
        });
    });
});
