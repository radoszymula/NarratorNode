import soundSystem from '../../pages/setup/soundSystem';


describe('Page Load Test', () => {
    context('On load', () => {
        it('Should play music', () => {
            cy.goToSetup();

            cy.window().then(window => {
                window.soundService.setCypressTesting();
            });

            soundSystem.assertNightMusicOn();
        });
    });
});
