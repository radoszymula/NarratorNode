import { FACTION_NAME, USER_ID2 } from '../../fixtures/fakeConstants';

import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import factionOverview from '../../pages/setup/factionOverview';
import factionUL from '../../pages/setup/factionUL';
import tabs from '../../pages/setup/tabs';


const WIN_PRIORITY_LABEL = 'not going to be shown';

function getSetupData(){
    return {
        setup: setupModel.get({
            factions: [factionModel.get({
                details: ['Faction attribute description.'],
                modifiers: [{
                    value: false,
                    name: 'modifierName',
                    label: 'Faction editing label',
                }, {
                    value: 3,
                    name: 'WIN_PRIORITY',
                    label: WIN_PRIORITY_LABEL,
                }],
            })],
        }),
    };
}

describe('Faction Details Visualizations', () => {
    context('Viewing faction settings', () => {
        it('Will show the factions bullet points for non hosts', () => {
            const setupData = getSetupData();

            cy.goToSetup(setupData, USER_ID2);

            factionOverview.assertVisible();
            factionOverview.assertVisibleModifiers(setupData.setup.factions[0].details);
        });
    });

    context('Interacting with the faction list', () => {
        it('Will show the faction overview', () => {
            cy.mobile();
            const setupData = getSetupData();
            setupData.setup.factions[0].factionRoles.push(factionRoleModel.get());
            cy.goToSetup(setupData);
            tabs.goToRoles();

            factionUL.clickFirst();

            factionOverview.assertHeader(FACTION_NAME);
        });
    });

    context('Interacting with super custom toggle UL', () => {
        it('Will appropriately show the faction font awesome icon on change', () => {
            cy.goToSetup(getSetupData());

            // one for the faction, one for the randoms
            cy.get('.customizability .settings_off').click();
            cy.get('.customizability .settings_on').click();
            cy.get('.customizability .settings_off').click();

            factionUL.shouldLookEditable();
        });
    });

    context('Editing faction settings', () => {
        it('Will edit faction int up', () => {
            const setupData = getSetupData();
            const label = setupData.setup.factions[0].modifiers[0].label;
            setupData.setup.factions[0].modifiers[0].value = 5;
            cy.goToSetup(setupData);

            factionOverview.setInputValue(6);

            factionUL.assertActiveFaction();
            factionOverview.assertVisibleModifiers([label]);
            factionOverview.assertModifierNotVisible(WIN_PRIORITY_LABEL);
            factionOverview.assertInputValue(6);
        });

        it('Will edit faction checkbox on', () => {
            const setupData = getSetupData();
            const label = setupData.setup.factions[0].modifiers[0].label;
            setupData.setup.factions[0].modifiers[0].value = false;
            cy.goToSetup(setupData);

            factionOverview.setChecked(true);

            factionUL.assertActiveFaction();
            factionOverview.assertVisibleModifiers([label]);
            factionOverview.assertIsChecked();
        });

        it('Will edit faction checkbox off', () => {
            const setupData = getSetupData();
            const label = setupData.setup.factions[0].modifiers[0].label;
            setupData.setup.factions[0].modifiers[0].value = true;
            cy.goToSetup(setupData);

            factionOverview.setChecked(false);

            factionUL.assertActiveFaction();
            factionOverview.assertVisibleModifiers([label]);
            factionOverview.assertIsUnchecked();
        });
    });
});
