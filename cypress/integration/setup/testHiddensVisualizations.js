import {
    COLOR, FACTION_ROLE_ID, HIDDEN_ID2,
    HIDDEN_NAME,
    ROLE_NAME,
} from '../../fixtures/fakeConstants';

import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import hiddenModel from '../../fixtures/baseModels/hiddenModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import userStateSetupResponse from '../../fixtures/userStateSetupReponse';


describe('Hidden Visualizations', () => {
    const newFactions = {
        factions: {
            factionNames: [COLOR],
        },
        setup: setupModel.get({
            factions: [factionModel.get({
                factionRoles: [factionRoleModel.get()],
            })],
            hiddens: [hiddenModel.get({
                name: HIDDEN_NAME,
                factionRoleIDs: [FACTION_ROLE_ID],
            }), hiddenModel.get({
                id: HIDDEN_ID2,
                name: ROLE_NAME,
                factionRoleIDs: [FACTION_ROLE_ID],
            })],
        }),
    };

    context('Hidden Catalogue Manipulation', () => {
        it('Shows factions in pane', () => {
            // NO GIVEN

            cy.goToSetup(newFactions);

            // one for the faction, one for the randoms
            cy.get('#team_catalogue_pane li').should('have.length', 2);
        });

        it('Should show the setup\'s hiddens when "Randoms" are clicked', () => {
            cy.goToSetup(newFactions);

            openRandomsCatalogue();

            cy.get('#roles_pane li').should('have.length', 1);
            cy.get('#roles_pane span')
                .should('have.css', 'color')
                .and('be.colored', COLOR);
        });

        it('Should not show "Hidden Singles"', () => {
            cy.goToSetup(newFactions);

            openRandomsCatalogue();

            cy.get('#roles_pane li').should('have.length', 1);
        });
    });

    context('On upstream setup change', () => {
        it('Stays on the "Randoms" tab', () => {
            cy.goToSetup(newFactions);
            const guiUpdate = {
                factions: {
                    factionNames: [COLOR],
                },
                guiUpdate: true,
                type: ['rules'],
                rules: userStateSetupResponse.get().rules,
            };
            openRandomsCatalogue();

            cy.window().then(async window => {
                await window.handleObject(guiUpdate);

                cy.wait(2000);
                cy.get('#roles_pane li').should('have.length', 1);
            });
        });
    });
});

function openRandomsCatalogue(){
    cy.get('#team_catalogue_pane li:last-child').click();
}
