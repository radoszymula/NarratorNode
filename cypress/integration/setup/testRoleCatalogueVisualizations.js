import {
    COLOR,
} from '../../fixtures/fakeConstants';

import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import setupModel from '../../fixtures/baseModels/setupModel';


function setupData(){
    return {
        setup: setupModel.get({
            factions: [factionModel.get({
                factionRoles: [factionRoleModel.get()],
            })],
        }),
    };
}

describe('Role Catalogue Visualizations', () => {
    context('Interacting with Faction UL', () => {
        it('Will show the catalogue with the proper colors', () => {
            cy.goToSetup(setupData());

            // one for the faction, one for the randoms
            const roleCatalogueLI = cy.get('#roles_pane span');

            roleCatalogueLI
                .should('have.css', 'color')
                .and('be.colored', COLOR);
        });
    });
});
