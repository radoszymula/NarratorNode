import {
    ABILITY_ID,
    ABILITY_NAME,
    COLOR,
    COLOR2,
    FACTION_ID2,
    FACTION_NAME2, FACTION_ROLE_ID, FACTION_ROLE_ID2, ROLE_ABILITY_MODIFIER_NAME,
    ROLE_MODIFIER_NAME,
    ROLE_NAME,
    SETUP_MODIFIER_NAME,
    USER_ID2,
} from '../../fixtures/fakeConstants';

import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import setupChangeEvent from '../../fixtures/events/setupChangeEvent';

import factionOverview from '../../pages/setup/factionOverview';
import factionUL from '../../pages/setup/factionUL';
import roleOverview from '../../pages/setup/roleOverview';
import roleUL from '../../pages/setup/roleUL';
import tabs from '../../pages/setup/tabs';


function getSetupData(){
    return {
        setup: setupModel.get({
            factions: [
                factionModel.get({
                    factionRoles: [factionRoleModel.get({
                        abilities: [{
                            id: ABILITY_ID,
                            modifiers: [{
                                name: ROLE_ABILITY_MODIFIER_NAME,
                                label: 'Role ability modifier editing label',
                                value: true,
                            }],
                            name: ABILITY_NAME,
                            setupModifierNames: [SETUP_MODIFIER_NAME],
                        }],
                        details: ['Role attribute description.'],
                        modifiers: [{
                            name: ROLE_MODIFIER_NAME,
                            label: 'Role modifier editing lable',
                            value: true,
                        }],
                    })],
                }),
            ],
            setupModifiers: {
                [SETUP_MODIFIER_NAME]: {
                    label: 'Setup modifier editing label',
                    value: true,
                },
            },
        }),
    };
}

describe('Faction Role Overview Visualizations', () => {
    context('Viewing role settings', () => {
        it('Will appropriately show the role bullet points for non hosts', () => {
            const setupData = getSetupData();
            const { details } = setupData.setup.factions[0].factionRoles[0];

            cy.goToSetup(setupData, USER_ID2);

            roleOverview.assertVisible();
            roleOverview.assertHeader(ROLE_NAME);
            roleOverview.assertHeaderColor(COLOR);
            roleOverview.assertVisibleModifiers(details);
        });

        it('Will show roles in other colors', () => {
            const setupData = getSetupData();
            const faction = setupData.setup.factions[0];
            const factionRole = faction.factionRoles[0];
            const newFactionRole = Object.assign({}, factionRole, {
                factionID: FACTION_ID2,
                id: FACTION_ROLE_ID2,
            });
            const newFaction = Object.assign({}, faction, {
                color: COLOR2,
                id: FACTION_ID2,
                factionRoles: [newFactionRole],
            });
            setupData.setup.factions.push(newFaction);
            cy.goToSetup(setupData, USER_ID2);

            roleUL.clickFirst();

            roleOverview.assertOtherColor(FACTION_ROLE_ID2);
        });

        it('Will change roles when "other" roles are clicked', () => {
            const setupData = getSetupData();
            const faction = setupData.setup.factions[0];
            const factionRole = faction.factionRoles[0];
            const newFactionRole = Object.assign({}, factionRole,
                { id: FACTION_ROLE_ID2, factionID: FACTION_ID2 });
            const newFaction = Object.assign({}, faction, {
                color: COLOR2,
                id: FACTION_ID2,
                name: FACTION_NAME2,
                factionRoles: [newFactionRole],
            });
            setupData.setup.factions.push(newFaction);
            cy.goToSetup(setupData, USER_ID2);

            roleOverview.clickOther(FACTION_ROLE_ID2);
            roleOverview.clickOther(FACTION_ROLE_ID); // make sure its not being duplicated
            roleOverview.clickOther(FACTION_ROLE_ID2);

            roleOverview.assertOtherColor(FACTION_ROLE_ID);
            roleOverview.assertHeaderColor(COLOR2);
            factionUL.assertBackgroundFaction(FACTION_ID2);
            roleUL.assertColor(COLOR2);
            roleUL.assertFocusedText(ROLE_NAME);
        });

        it('Will not show both faction and role overviews on load', () => {
            cy.mobile();
            const setupData = getSetupData();
            cy.goToSetup(setupData);

            tabs.goToRoles();

            factionOverview.assertInvisible();
        });

        it('Will not change faction roles on setup update', () => {
            const setupData = getSetupData();
            setupData.setup.factions.push(factionModel.get2({
                factionRoles: [factionRoleModel.get2()],
            }));
            const setupChangeEventObject = setupChangeEvent.get(setupData.setup);
            cy.goToSetup(setupData);
            factionUL.click(FACTION_NAME2);

            cy.window().then(async window => {
                await window.handleObject(setupChangeEventObject);
                cy.wait(2000);

                roleUL.assertColor(COLOR2);
            });
        });

        it('Will hide the roles on mobile', () => {
            cy.mobile();
            const setupData = getSetupData();
            cy.goToSetup(setupData);
            tabs.goToRoles();

            factionUL.clickFirst();

            roleOverview.assertInvisible();
        });
    });

    context('Updating global setup modifier role settings', () => {
        it('Will update setup modifiers to on (bool)', () => {
            const setupData = getSetupData();
            const factionRole = setupData.setup.factions[0].factionRoles[0];
            factionRole.abilities[0].modifiers = [];
            factionRole.modifiers = [];
            const roleResponse = JSON.parse(JSON.stringify(factionRole));
            setupData.setup.setupModifiers[SETUP_MODIFIER_NAME].value = false;
            cy.goToSetup(setupData);
            const { label } = setupData.setup.setupModifiers[SETUP_MODIFIER_NAME];

            roleOverview.setSetupModifierChecked(true, factionRole.id, roleResponse);

            roleOverview.assertIsChecked();
            roleOverview.assertVisibleModifiers([label]);
        });

        it('Will update setup modifiers to off (bool)', () => {
            const setupData = getSetupData();
            const factionRole = setupData.setup.factions[0].factionRoles[0];
            factionRole.abilities[0].modifiers = [];
            factionRole.modifiers = [];
            const roleResponse = JSON.parse(JSON.stringify(factionRole));
            cy.goToSetup(setupData);
            const { label } = setupData.setup.setupModifiers[SETUP_MODIFIER_NAME];

            roleOverview.setSetupModifierChecked(false, factionRole.id, roleResponse);

            roleOverview.assertIsUnchecked();
            roleOverview.assertVisibleModifiers([label]);
        });

        it('Will update setup modifiers (int)', () => {
            const setupData = getSetupData();
            const factionRole = setupData.setup.factions[0].factionRoles[0];
            factionRole.abilities[0].modifiers = [];
            factionRole.modifiers = [];
            const roleResponse = JSON.parse(JSON.stringify(factionRole));
            const modifier = setupData.setup.setupModifiers[SETUP_MODIFIER_NAME];
            modifier.value = 4;
            cy.goToSetup(setupData);

            roleOverview.setSetupModifierInputValue(5, 6, roleResponse);

            roleOverview.assertInputValue(6);
            roleOverview.assertVisibleModifiers([modifier.label]);
        });

        it('Will update attribute texts on edit', () => {
            const setupData = getSetupData();
            const faction = setupData.setup.factions[0];
            const factionRole = faction.factionRoles[0];
            factionRole.abilities[0].modifiers = [];
            factionRole.modifiers = [];
            const newFactionRole = factionRoleModel.get2();
            const newFaction = factionModel.get2({ factionRoles: [newFactionRole] });
            setupData.setup.factions.push(newFaction);
            const label = setupData.setup.factions[0].factionRoles[0].details;
            const customResponse = Object.assign({}, setupData.setup.factions[0].factionRoles[0],
                { details: [label + label] });
            const extraCustomResponse = factionRoleModel.get2();
            cy.goToSetup(setupData);
            cy.route({
                method: 'GET',
                url: `factionRoles/${FACTION_ROLE_ID2}`,
                response: { response: extraCustomResponse },
            }).as('factionRoleResponse2');

            roleOverview.setSetupModifierChecked(false, FACTION_ROLE_ID, customResponse);
            cy.wait(['@factionRoleResponse2']);

            roleOverview.assertVisibleModifiers([label + label]);
        });
    });

    context('Updating specific role ability modifiers', () => {
        it('Will update role ability modifiers to on (bool)', () => {
            const setupData = getSetupData();
            const factionRole = setupData.setup.factions[0].factionRoles[0];
            factionRole.abilities[0].setupModifierNames = [];
            factionRole.abilities[0].modifiers[0].value = false;
            factionRole.modifiers = [];
            const { label } = factionRole.abilities[0].modifiers[0];
            const customRoleResponse = JSON.parse(JSON.stringify(factionRole));
            customRoleResponse.abilities[0].modifiers[0].value = true;
            cy.goToSetup(setupData);

            roleOverview.setFactionRoleAbilityModifierChecked(true, customRoleResponse);

            roleOverview.assertIsChecked();
            roleOverview.assertVisibleModifiers([label]);
        });

        it('Will update role ability modifiers to off (bool)', () => {
            const setupData = getSetupData();
            const factionRole = setupData.setup.factions[0].factionRoles[0];
            factionRole.abilities[0].setupModifierNames = [];
            factionRole.abilities[0].modifiers[0].value = true;
            factionRole.modifiers = [];
            const { label } = factionRole.abilities[0].modifiers[0];
            const customRoleResponse = JSON.parse(JSON.stringify(factionRole));
            customRoleResponse.abilities[0].modifiers[0].value = false;
            cy.goToSetup(setupData);

            roleOverview.setFactionRoleAbilityModifierChecked(false, customRoleResponse);

            roleOverview.assertIsUnchecked();
            roleOverview.assertVisibleModifiers([label]);
        });

        it('Will update role ability modifiers (int)', () => {
            const setupData = getSetupData();
            const factionRole = setupData.setup.factions[0].factionRoles[0];
            factionRole.abilities[0].setupModifierNames = [];
            factionRole.abilities[0].modifiers[0].value = 4;
            factionRole.modifiers = [];
            const { label } = factionRole.abilities[0].modifiers[0];
            const customRoleResponse = JSON.parse(JSON.stringify(factionRole));
            customRoleResponse.abilities[0].modifiers[0].value = 6;
            cy.goToSetup(setupData);

            roleOverview.setFactionRoleAbilityModifierInputValue(5, 6, customRoleResponse);

            roleOverview.assertInputValue(6);
            roleOverview.assertVisibleModifiers([label]);
        });
    });

    context('Updating specific role modifiers', () => {
        it('Will update role modifiers to on (bool)', () => {
            const setupData = getSetupData();
            const factionRole = setupData.setup.factions[0].factionRoles[0];
            factionRole.abilities[0].setupModifierNames = [];
            factionRole.abilities[0].modifiers = [];
            factionRole.modifiers[0].value = false;
            const { label } = factionRole.modifiers[0];
            const customRoleResponse = JSON.parse(JSON.stringify(factionRole));
            customRoleResponse.modifiers[0].value = true;
            cy.goToSetup(setupData);

            roleOverview.setFactionRoleModifierChecked(true, customRoleResponse);

            roleOverview.assertIsChecked();
            roleOverview.assertVisibleModifiers([label]);
        });

        it('Will update role ability to off (bool)', () => {
            const setupData = getSetupData();
            const factionRole = setupData.setup.factions[0].factionRoles[0];
            factionRole.abilities[0].setupModifierNames = [];
            factionRole.abilities[0].modifiers = [];
            factionRole.modifiers[0].value = true;
            const { label } = factionRole.modifiers[0];
            const customRoleResponse = JSON.parse(JSON.stringify(factionRole));
            customRoleResponse.modifiers[0].value = false;
            cy.goToSetup(setupData);

            roleOverview.setFactionRoleModifierChecked(false, customRoleResponse);

            roleOverview.assertIsUnchecked();
            roleOverview.assertVisibleModifiers([label]);
        });

        it('Will update role modifiers (int)', () => {
            const setupData = getSetupData();
            const factionRole = setupData.setup.factions[0].factionRoles[0];
            factionRole.abilities[0].setupModifierNames = [];
            factionRole.abilities[0].modifiers = [];
            factionRole.modifiers[0].value = 4;
            const { label } = factionRole.modifiers[0];
            const customRoleResponse = JSON.parse(JSON.stringify(factionRole));
            customRoleResponse.modifiers[0].value = 6;
            cy.goToSetup(setupData);

            roleOverview.setFactionRoleModifierInputValue(5, 6, customRoleResponse);

            roleOverview.assertInputValue(6);
            roleOverview.assertVisibleModifiers([label]);
        });
    });
});
