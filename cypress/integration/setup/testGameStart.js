import startPane from '../../pages/setup/startPane';
import toasts from '../../pages/toasts';


describe('Game Start', () => {
    context('Game start errors', () => {
        it('Will show a popup when game fails to start', () => {
            const errorText = 'Failed to start';
            cy.goToSetup();

            startPane.failStart(errorText);

            toasts.assertText(errorText);
        });
    });
});
