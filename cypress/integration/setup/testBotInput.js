import { PLAYER_NAME2, PLAYER_NAME3 } from '../../fixtures/fakeConstants';

import playerModel from '../../fixtures/baseModels/playerModel';

import playerResponses from '../../fixtures/responses/playerResponses';

import playerUL from '../../pages/setup/playerUL';


describe('Bot Input', () => {
    context('On bot add', () => {
        it('Should show that bots were added', () => {
            const addBotResponse = playerResponses.addBots({
                players: [
                    playerModel.get(),
                    playerModel.getBot({ name: PLAYER_NAME2 }),
                    playerModel.getBot({ name: PLAYER_NAME3 }),
                ],
            });
            cy.goToSetup();
            cy.route({
                method: 'POST',
                url: 'players/bots',
                response: addBotResponse,
            }).as('botRequest');

            cy.window().then(async window => {
                await window.addBots(2);
                cy.wait(['@botRequest']);

                playerUL.assertHeaderCount(3);
                playerUL.assertIsComputer(PLAYER_NAME2);
                playerUL.assertIsComputer(PLAYER_NAME3);
                playerUL.assertNameInUL(PLAYER_NAME2);
                playerUL.assertNameInUL(PLAYER_NAME3);
            });
        });
    });
});
