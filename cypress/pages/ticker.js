function clickChatBubble(){
    cy.get('#chat_ticker_expander_icon').click();
}

module.exports = {
    clickChatBubble,
};
