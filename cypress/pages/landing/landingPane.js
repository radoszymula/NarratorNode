import gameModel from '../../fixtures/baseModels/gameModel';
import profileModel from '../../fixtures/baseModels/profileModel';

import { PLAYER_NAME2 } from '../../fixtures/fakeConstants';
import userStateSetupResponse from '../../fixtures/userStateSetupReponse';
import chatResponse from '../../fixtures/chatResponse';


function joinGame(joinID){
    const responseObj = userStateSetupResponse.get({
        isStarted: false,
    });

    cy.route({
        method: 'GET',
        url: 'chats',
        response: { response: chatResponse.getEmpty() },
    }).as('chatResponse');
    cy.route({
        method: 'POST',
        url: 'players',
        response: { response: gameModel.get() },
    }).as('playerJoinResponse');
    cy.route({
        method: 'GET',
        url: 'profiles',
        response: { response: profileModel.getInSetup() },
    }).as('profileResponse');
    cy.route({
        method: 'GET',
        url: 'games/userState',
        response: { response: responseObj },
    });

    cy.get('#joinButton').click();
    cy.get('.gameCodeInput').type(joinID);
    cy.get('.your_name_input').type(PLAYER_NAME2);
    cy.get('#homePopUpSubmit').click();
    cy.wait(['@playerJoinResponse', '@profileResponse', '@chatResponse']);
}

module.exports = {
    joinGame,
};
