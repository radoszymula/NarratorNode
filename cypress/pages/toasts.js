function assertText(text){
    cy.get('#warningPopup span').contains(text);
}

function assertHidden(){
    cy.get('#warningPopup', { timeout: 1000 }).should('not.be.visible');
}

module.exports = {
    assertHidden,
    assertText,
};
