import { GAME_ID } from '../fixtures/fakeConstants';


function assertOnHome(){
    cy.get('#lobby_page').should('be.visible');
    cy.get('#setup_page').should('not.be.visible');
    cy.get('#main').should('not.be.visible');
    cy.url().should('not.include', GAME_ID);
}

function assertOnSetup(){
    cy.get('#lobby_page').should('not.be.visible');
    cy.get('#setup_page').should('be.visible');
    cy.get('#main').should('not.be.visible');
}

module.exports = {
    assertOnHome,
    assertOnSetup,
};
