function clickFirst(){
    cy.get('#graveyard_ul li:first').click();
}

function assertSetupHiddenCount(count){
    cy.get('#graveyard_ul').find('li').should('have.length', count);
}

module.exports = {
    clickFirst,

    assertSetupHiddenCount,
};
