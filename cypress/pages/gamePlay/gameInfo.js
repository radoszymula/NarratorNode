function clickHidden(hiddenName){
    cy.get('#game_play_card').contains(hiddenName).click();
}

function clickRole(roleName){
    cy.get('#game_play_card').contains(roleName).click();
}

function clickTeamSubheader(){
    cy.get('#game_play_card .roleCardSubHeader').click();
}

function assertEnemiesShown(factionName){
    cy.get('#game_play_card .teamBulletPointHolder').contains('Must eliminate');
    cy.get('#game_play_card .teamBulletPointHolder').contains(factionName);
}

function assertFactionClicked(factionName){
    cy.get('#game_play_card .teamDescriptionLabel').contains(factionName);
    cy.get('#game_play_card .teamDescriptionLabel').contains(factionName);
}

function assertFactionRoleShown(roleName){
    cy.get('#game_play_card .teamBulletPointHolder').contains('Possible roles');
    cy.get('#game_play_card .teamBulletPointHolder').contains(roleName);
}

function assertGraveClicked(playerName){
    cy.get('#game_play_card .teamDescriptionLabel').contains(playerName);
}

function assertHiddenClicked(hiddenName){
    cy.get('#game_play_card .teamDescriptionLabel').contains(hiddenName);
}

function assertImageVisible(){
    cy.get('#game_play_card img').should('be.visible');
}

function assertRoleClicked(roleName){
    cy.get('#game_play_card .teamDescriptionLabel').contains(roleName);
}

function assertSetupHiddenName(hiddenName){
    cy.get('#game_play_card .teamDescriptionLabel').contains(hiddenName);
    cy.get('#game_play_card .teamDescriptionLabel').should('be.visible');
}

function assertSpawnedByCount(count){
    cy.get('#role_exp_extras').find('.roleHover').should('have.length', count);
}

function assertSubheaderHidden(){
    cy.get('#game_play_card .roleCardSubHeader').should('not.be.visible');
}

module.exports = {
    clickHidden,
    clickRole,
    clickTeamSubheader,

    assertEnemiesShown,
    assertFactionClicked,
    assertGraveClicked,
    assertHiddenClicked,
    assertImageVisible,
    assertFactionRoleShown,
    assertRoleClicked,
    assertSetupHiddenName,
    assertSpawnedByCount,
    assertSubheaderHidden,
};
