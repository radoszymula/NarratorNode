function clickFirst(){
    cy.get('#roleList_content li:first').click();
}

function assertSetupHiddenCount(count){
    cy.get('#roleList_content').find('li').should('have.length', count);
}

module.exports = {
    clickFirst,

    assertSetupHiddenCount,
};
