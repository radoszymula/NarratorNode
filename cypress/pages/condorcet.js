function showVoteList(name){
    cy.get('#currentRanking').within(() => {
        cy.contains(name).click();
    });
}

function assertInCurrentRanking(name){
    cy.get('#currentRanking').should('contain', name);
}

function assertInPersonalList(name){
    cy.get('#personalRanking').within(() => {
        cy.contains(name);
    });
}

function assertNameNotInSubmittedVotes(name){
    cy.get('#votelist').should('not.contain', name);
}

module.exports = {
    showVoteList,

    assertInCurrentRanking,
    assertInPersonalList,
    assertNameNotInSubmittedVotes,
};
