import factionModifierResponse from '../../fixtures/responses/factionModifierResponse';


function setChecked(value){
    cy.route({
        method: 'PUT',
        url: 'factions/*/modifiers',
        response: factionModifierResponse.get(),
    }).as('factionModifierResponse');
    if(value)
        cy.get('#setup_page .team_settings_pane input').check();
    else
        cy.get('#setup_page .team_settings_pane input').uncheck();
    cy.wait(['@factionModifierResponse']);
}

function setInputValue(value){
    cy.route({
        method: 'PUT',
        url: 'factions/*/modifiers',
        response: factionModifierResponse.get(),
    }).as('factionModifierResponse');
    cy.get('#setup_page .team_settings_pane input').clear();
    cy.get('#setup_page .team_settings_pane input').type(value);
    cy.wait(['@factionModifierResponse']);
}

function assertHeader(factionName){
    cy.get('.teamPickerEditor .roleCardHeader').should('contain', factionName);
}

function assertInputValue(value){
    cy.get('#setup_page .team_settings_pane input').should('have.value', value.toString());
}

function assertInvisible(){
    cy.get('#setup_page .team_settings_pane').should('not.be.visible');
}

function assertIsChecked(){
    cy.get('#setup_page .team_settings_pane input').should('be.checked');
}

function assertIsUnchecked(){
    cy.get('#setup_page .team_settings_pane input').should('not.be.checked');
}

function assertModifierNotVisible(text){
    cy.get('#setup_page .team_settings_pane').should('not.contain', text);
}

function assertVisible(){
    cy.get('#setup_page .team_settings_pane').should('be.visible');
}

function assertVisibleModifiers(modifiers){
    modifiers.forEach(modifier => {
        cy.get('#setup_page .team_settings_pane').should('contain', modifier);
    });
}

module.exports = {
    setChecked,
    setInputValue,

    assertHeader,
    assertInputValue,
    assertInvisible,
    assertIsChecked,
    assertIsUnchecked,
    assertModifierNotVisible,
    assertVisible,
    assertVisibleModifiers,
};
