function click(factionName){
    cy.get('#team_catalogue_pane').within(() => {
        cy.contains(factionName).click();
    });
}

function clickFirst(){
    cy.get('#team_catalogue_pane li:first').click();
}

function clickHiddens(){
    cy.get('#team_catalogue_pane li:last').click();
}

function assertActiveFaction(factionID){
    if(!factionID)
        cy.get('#team_catalogue_pane .rSetupFocused').should('exist');
    else
        cy.get(`#team_catalogue_pane .factionID${factionID}`)
            .should('have.class', 'rSetupFocused');
}

function assertBackgroundFaction(factionID){
    if(!factionID)
        cy.get('#team_catalogue_pane .rSetupBG').should('exist');
    else
        cy.get(`#team_catalogue_pane .factionID${factionID}`)
            .should('have.class', 'rSetupBG');
}

function assertHiddenBackgroundSelected(){
    cy.get('#team_catalogue_pane .rSetupBG').should('contain', 'Randoms');
}

function assertNoActiveFactions(){
    cy.get('#team_catalogue_pane .rSetupFocused').should('not.exist');
    cy.get('#team_catalogue_pane .rSetupBG').should('not.exist');
}

function assertTeamDoesNotExist(name){
    cy.get('#team_catalogue_pane').should('not.contain', name);
}

function shouldLookEditable(){
    cy.get('#team_catalogue_pane li:first i').should('have.class', 'fa-pencil');
}

module.exports = {
    click,
    clickFirst,
    clickHiddens,

    assertActiveFaction,
    assertBackgroundFaction,
    assertHiddenBackgroundSelected,
    assertNoActiveFactions,
    assertTeamDoesNotExist,
    shouldLookEditable,
};
