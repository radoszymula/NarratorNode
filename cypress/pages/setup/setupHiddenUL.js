import { SETUP_HIDDEN_ID } from '../../fixtures/fakeConstants';

function clickFirst(){
    cy.get('#setup_roles_list span').click();
}

function removeSetupHidden(){
    cy.route({
        method: 'DELETE',
        url: `setupHiddens/${SETUP_HIDDEN_ID}`,
        response: '',
    }).as('setupHiddenDeleteResponse');

    cy.get('#setup_roles_list .setup_plus_minus:first').click();
    cy.wait(['@setupHiddenDeleteResponse']);
}

function assertColor(color){
    cy.get('#setup_roles_list span')
        .should('have.css', 'color')
        .and('be.colored', color);
}

function assertText(text){
    cy.get('#setup_roles_list span').contains(text);
}

function assertSize(size){
    cy.get('#setup_roles_list').find('li').should('have.length', size);
}

module.exports = {
    clickFirst,
    removeSetupHidden,

    assertColor,
    assertSize,
    assertText,
};
