import setupModifierResponse from '../../fixtures/responses/setupModifierResponse';
import setupResponse from '../../fixtures/responses/setupResponse';


function increaseDayLength(newValue, onRequest){
    cy.route({
        method: 'PUT',
        url: 'setupModifiers',
        response: setupModifierResponse.get({
            name: 'DAY_LENGTH',
            value: newValue,
        }),
        onRequest,
    }).as('setupModifierRequest');
    cy.get('#Day_Length .fa-plus-square').click();
    cy.wait(['@setupModifierRequest']);
}

function turnOffAutoSetup(newSetup){
    cy.route({
        method: 'PUT',
        url: 'setups',
        response: setupResponse.change({ setup: newSetup }),
    }).as('setupChange');
    cy.route({
        method: 'POST',
        url: 'setups',
        response: setupResponse.create(),
    }).as('setupCreate');

    cy.get('#auto_setup_toggle .settings_off').click();
    cy.wait(['@setupCreate', '@setupChange']);
}

function assertDayLength(dayLength){
    cy.get('#Day_Length span').should('have.text', dayLength.toString());
}

function assertHostControls(){
    cy.get('#Day_Length i').should('be.visible');
}

function assertNoChatRolesSetting(){
    cy.get('#setup_gen_settings_pane').within(() => {
        cy.contains('Chat Roles').should('not.be.visible');
    });
}

function assertNoHostControls(){
    cy.get('#Day_Length i').should('not.be.visible');
}

function assertNoHostVotingSetting(){
    cy.get('#setup_gen_settings_pane').within(() => {
        cy.contains('Controlled Voting').should('not.be.visible');
    });
}

function assertSetupDropdownHidden(){
    cy.get('.setup_input').should('be.not.visible');
}

module.exports = {
    increaseDayLength,
    turnOffAutoSetup,

    assertDayLength,
    assertHostControls,
    assertNoChatRolesSetting,
    assertNoHostControls,
    assertNoHostVotingSetting,
    assertSetupDropdownHidden,
};
