import setupHiddenResponse from '../../fixtures/responses/setupHiddenResponse';
import hiddenResponse from '../../fixtures/responses/hiddenResponse';


function addCachedRole(){
    cy.route({
        method: 'POST',
        url: 'setupHiddens',
        response: setupHiddenResponse.post(),
    }).as('setupHiddenResponse');
    cy.get('#roles_pane .setup_plus_minus:first').click();
    cy.wait(['@setupHiddenResponse']);
}

function addHidden(){
    cy.route({
        method: 'POST',
        url: 'setupHiddens',
        response: setupHiddenResponse.post(),
    }).as('setupHiddenResponse');
    cy.contains('Randoms').click();
    cy.get('#roles_pane .setup_plus_minus:first').click();
    cy.wait(['@setupHiddenResponse']);
}

function addUncachedRole(){
    cy.route({
        method: 'POST',
        url: 'setupHiddens',
        response: setupHiddenResponse.post(),
    }).as('setupHiddenResponse');
    cy.route({
        method: 'POST',
        url: 'hiddens',
        response: hiddenResponse.post(),
    }).as('hiddenResponse');
    cy.get('#roles_pane .setup_plus_minus:first').click();
    cy.wait(['@setupHiddenResponse', '@hiddenResponse']);
}

function click(factionName){
    cy.get('#role_catalogue').within(() => {
        cy.contains(factionName).click();
    });
}

function clickFirst(){
    cy.get('#role_catalogue li:first').click();
}

function assertColor(color){
    cy.get('#roles_pane span:first')
        .should('have.css', 'color')
        .and('be.colored', color);
}

function assertFocusedText(text){
    cy.get('#roles_pane .rSetupFocused').should('contain', text);
}

module.exports = {
    addCachedRole,
    addHidden,
    addUncachedRole,
    click,
    clickFirst,

    assertColor,
    assertFocusedText,
};
