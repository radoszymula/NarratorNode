import { GAME_ID } from '../../fixtures/fakeConstants';

import moderatorResponses from '../../fixtures/responses/moderatorResponses';
import playerResponses from '../../fixtures/responses/playerResponses';


function clickSetAsHost(playerName, newHostID){
    cy.route({
        method: 'POST',
        url: 'moderators/repick',
        response: moderatorResponses.repickHost(newHostID),
        onRequest: requestResponse => {
            const { body } = requestResponse.request;
            expect(body.repickTarget).to.be.equal(playerName);
            expect(body.gameID).to.be.equal(GAME_ID);
        },
    }).as('hostRepick');
    cy.contains('Set Host').click();
    cy.wait(['@hostRepick']);
}

function clickKick(userID){
    cy.route({
        method: 'DELETE',
        url: `players/kick?playerID=${userID}`,
        response: playerResponses.kick(),
    }).as('kick');
    cy.contains('Kick').click();
    cy.wait(['@kick']);
}


function assertInvisible(){
    cy.get('#userPopup').should('not.be.visible');
    cy.get('#userPopupBackdrop').should('not.be.visible');
}

function assertNoRepickOption(){
    cy.get('#userPopupRepick').should('not.be.visible');
}

function assertVisible(){
    cy.get('#userPopup').should('be.visible');
}

module.exports = {
    clickSetAsHost,
    clickKick,

    assertInvisible,
    assertNoRepickOption,
    assertVisible,
};
