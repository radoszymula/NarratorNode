function assertHidden(){
    cy.get('#teamEditorPane').should('be.hidden');
}

module.exports = {
    assertHidden,
};
