function click(playerName){
    cy.get('#setup_players_list').contains(playerName).click();
}

function assertActive(playerName){
    cy.get(`#setup_players_list .playerLI${playerName} i`)
        .should('have.css', 'color')
        .and('be.colored', 'green');
}

function assertActiveHost(playerName){
    assertHost(playerName);
    cy.get(`#setup_players_list .playerLI${playerName} i`)
        .should('have.css', 'color')
        .and('be.colored', 'yellow');
}

function assertHost(playerName){
    cy.get(`#setup_players_list .playerLI${playerName} i`).should('have.class', 'fa-star');
}

function assertHeaderCount(count){
    cy.get('.lobbyCountLabel').contains(count.toString());
    cy.get('#setup_players_list').find('li').should('have.length', count);
}

function assertIsComputer(playerName){
    cy.get(`#setup_players_list .playerLI${playerName} i`).should('have.class', 'fa-laptop');
}

function assertInactive(playerName){
    cy.get(`#setup_players_list .playerLI${playerName} i`)
        .should('have.css', 'color')
        .and('be.colored', '#666');
}

function assertNameInUL(playerName){
    cy.get('#setup_players_list').contains(playerName);
}

function assertNameNotInUL(playerName){
    cy.get(`#setup_players_list .playerLI${playerName}`).should('not.exist');
}

module.exports = {
    click,

    assertActive,
    assertActiveHost,
    assertHeaderCount,
    assertHost,
    assertInactive,
    assertIsComputer,
    assertNameInUL,
    assertNameNotInUL,
};
