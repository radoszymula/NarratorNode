import roleModel from '../../fixtures/baseModels/roleModel';

import factionRoleAbilityModifierResponse from '../../fixtures/responses/factionRoleAbilityModifierResponse'; // eslint-disable-line max-len
import setupModifierResponse from '../../fixtures/responses/setupModifierResponse';


function clickOther(roleID){
    cy.get(`#setup_role_modifiers .roleID${roleID}`).click();
}

function setFactionRoleAbilityModifierInputValue(value, responseValue, customRoleResponse){
    cy.route({
        method: 'PUT',
        url: 'factionRoles/*/abilities/*/modifiers',
        response: setupModifierResponse.get({ value: responseValue }),
    }).as('factionRoleAbilityModifierResponse');
    cy.route({
        method: 'GET',
        url: 'factionRoles/*',
        response: { response: customRoleResponse || roleModel.get() },
    }).as('factionRoleResponse');
    cy.get('#setup_role_modifiers input').clear();
    cy.get('#setup_role_modifiers input').type(value);
    cy.wait(['@factionRoleAbilityModifierResponse', '@factionRoleResponse']);
}

function setFactionRoleModifierInputValue(value, responseValue, customRoleResponse){
    cy.route({
        method: 'PUT',
        url: 'factionRoles/*/modifiers',
        response: setupModifierResponse.get({ value: responseValue }),
    }).as('roleModifierResponse');
    cy.route({
        method: 'GET',
        url: 'factionRoles/*',
        response: { response: customRoleResponse || roleModel.get() },
    }).as('factionRoleResponse');
    cy.get('#setup_role_modifiers input').clear();
    cy.get('#setup_role_modifiers input').type(value);
    cy.wait(['@roleModifierResponse', '@factionRoleResponse']);
}

function setFactionRoleAbilityModifierChecked(isChecked, customRoleResponse){
    cy.route({
        method: 'PUT',
        url: 'factionRoles/*/abilities/*/modifiers',
        response: factionRoleAbilityModifierResponse.get(isChecked),
    }).as('factionRoleAbilityModifierResponse');
    cy.route({
        method: 'GET',
        url: 'factionRoles/*',
        response: { response: customRoleResponse || roleModel.get() },
    }).as('factionRoleResponse');
    if(isChecked)
        cy.get('#setup_role_modifiers input').check();
    else
        cy.get('#setup_role_modifiers input').uncheck();
    cy.wait(['@factionRoleAbilityModifierResponse', '@factionRoleResponse']);
}

function setFactionRoleModifierChecked(isChecked, customRoleResponse){
    cy.route({
        method: 'PUT',
        url: 'factionRoles/*/modifiers',
        response: factionRoleAbilityModifierResponse.get(isChecked),
    }).as('roleModifierResponse');
    cy.route({
        method: 'GET',
        url: 'factionRoles/*',
        response: { response: customRoleResponse || roleModel.get() },
    }).as('factionRoleResponse');
    if(isChecked)
        cy.get('#setup_role_modifiers input').check();
    else
        cy.get('#setup_role_modifiers input').uncheck();
    cy.wait(['@roleModifierResponse', '@factionRoleResponse']);
}

function setSetupModifierChecked(isChecked, roleID, customRoleResponse){
    roleID = roleID || '*';
    cy.route({
        method: 'PUT',
        url: 'setupModifiers',
        response: setupModifierResponse.get({ value: isChecked }),
    }).as('setupModifierResponse');
    cy.route({
        method: 'GET',
        url: `factionRoles/${roleID}`,
        response: { response: customRoleResponse || roleModel.get() },
    }).as('factionRoleResponse');
    if(isChecked)
        cy.get('#setup_role_modifiers input').check();
    else
        cy.get('#setup_role_modifiers input').uncheck();
    cy.wait(['@setupModifierResponse', '@factionRoleResponse']);
}

function setSetupModifierInputValue(value, responseValue, customResponse){
    cy.route({
        method: 'PUT',
        url: 'setupModifiers',
        response: setupModifierResponse.get({ value: responseValue }),
    }).as('setupModifierResponse');
    cy.route({
        method: 'GET',
        url: 'factionRoles/*',
        response: { response: customResponse || roleModel.get() },
    }).as('factionRoleResponse');
    cy.get('#setup_role_modifiers input').clear();
    cy.get('#setup_role_modifiers input').type(value);
    cy.wait(['@setupModifierResponse', '@factionRoleResponse']);
}

function assertHeader(headerText){
    cy.get('.role_settings_pane .roleCardHeader').should('contain', headerText);
}

function assertHeaderColor(color){
    cy.get('.role_settings_pane .roleCardHeader')
        .should('have.css', 'color')
        .and('be.colored', color);
}

function assertInputValue(value){
    cy.get('#setup_role_modifiers input').should('have.value', value.toString());
}

function assertInvisible(){
    cy.get('.role_settings_pane').should('not.be.visible');
}

function assertIsChecked(){
    cy.get('#setup_role_modifiers input').should('be.checked');
}

function assertIsUnchecked(){
    cy.get('#setup_role_modifiers input').should('not.be.checked');
}

function assertOtherColor(roleID){
    cy.get(`#setup_role_modifiers .roleID${roleID}`).should('have.length', 1);
}

function assertVisible(){
    cy.get('.role_settings_pane').should('be.visible');
}

function assertVisibleModifiers(modifiers){
    modifiers.forEach(modifier => {
        cy.get('.role_settings_pane').should('contain', modifier);
    });
}

module.exports = {
    clickOther,
    setFactionRoleAbilityModifierChecked,
    setFactionRoleAbilityModifierInputValue,
    setFactionRoleModifierChecked,
    setFactionRoleModifierInputValue,
    setSetupModifierChecked,
    setSetupModifierInputValue,

    assertHeader,
    assertHeaderColor,
    assertInputValue,
    assertInvisible,
    assertIsChecked,
    assertIsUnchecked,
    assertOtherColor,
    assertVisible,
    assertVisibleModifiers,
};
