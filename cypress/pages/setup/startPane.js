function failStart(errorText){
    cy.route({
        method: 'POST',
        url: 'games/*/start',
        status: 422,
        response: {
            errors: [errorText],
        },
    });
    cy.get('#notMobileStartButton').click();
}

module.exports = {
    failStart,
};
