/* global Cypress:readonly */
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import threadVotesModel from '../fixtures/condorcetModels/threadVotesModel';

import gameModel from '../fixtures/baseModels/gameModel';
import phaseModel from '../fixtures/baseModels/phaseModel';
import playerModel from '../fixtures/baseModels/playerModel';
import profileModel from '../fixtures/baseModels/profileModel';
import setupModel from '../fixtures/baseModels/setupModel';
import userModel from '../fixtures/baseModels/userModel';

import authTokenResponse from '../fixtures/responses/authTokenResponse';
import chatResponse from '../fixtures/chatResponse';
import setupFeaturedResponse from '../fixtures/responses/setupFeaturedResponse';
import userStateSetupResponse from '../fixtures/userStateSetupReponse';

import {
    GAME_ID, PLAYER_NAME, USER_ID,
} from '../fixtures/fakeConstants';


Cypress.Commands.add('goToHome', (userID = USER_ID) => {
    cy.server();
    cy.route({
        method: 'GET',
        url: 'profiles',
        response: {},
    }).as('profileInitResponse');
    cy.route({
        method: 'GET',
        url: 'setups',
        response: setupFeaturedResponse.get(),
    }).as('featuredSetupsResponse');
    cy.route({
        method: 'GET',
        url: 'users/id',
        response: { response: { id: userID } },
    }).as('userResponse');
    cy.visit('http://localhost:4501', {
        onLoad: stubFirebase,
    });
    cy.wait(['@featuredSetupsResponse']);
});

Cypress.Commands.add('goToSetup', (userState = {}, userID = USER_ID) => {
    const activeUserIDs = userState.activeUserIDs || [userID];
    const chats = userState.chats || chatResponse.getEmpty();

    cy.server();
    cy.route({
        method: 'GET',
        url: 'profiles',
        response: { response: profileModel.getInSetup() },
    });
    cy.route({
        method: 'GET',
        url: 'setups',
        response: setupFeaturedResponse.get(),
    });
    cy.route({
        method: 'GET',
        url: 'users*',
        response: authTokenResponse.get(userID),
    });
    cy.route({
        method: 'POST',
        url: 'user_integrations*',
        response: authTokenResponse.get(userID),
    });
    cy.route({
        method: 'GET',
        url: `channels/browser/activeUserIDs?game_id=${GAME_ID}`,
        response: { response: activeUserIDs },
    }).as('activeBrowserUserIDs');

    cy.route({
        method: 'GET',
        url: 'chats',
        response: { response: chats },
    }).as('chatResponse');

    const host = userState.host || userModel.get();

    const responseObj = userStateSetupResponse.get({
        isStarted: false,
        host,
    });
    Object.keys(userState || [])
        .forEach(key => {
            responseObj[key] = userState[key];
        });

    const { setup } = responseObj;
    setup.hiddens = setup.hiddens || [];
    setup.ownerID = userID;
    setup.setupHiddens = setup.setupHiddens || [];
    const players = userState.players || [playerModel.get()];

    cy.route({
        method: 'GET',
        url: `games/${GAME_ID}`,
        response: {
            response: gameModel.get({
                host,
                integrations: userState.integrations,
                isStarted: false,
                players,
                setup,
            }),
        },
    }).as('gameResponse');
    cy.route({
        method: 'GET',
        url: 'games/userState',
        response: { response: responseObj },
    }).as('userStateResponse');

    const authToken = new Array(128 + 1).join('A');
    cy.visit(`http://localhost:4501/?auth_token=${authToken}`, {
        onLoad: stubFirebase,
    });
    cy.wait(['@userStateResponse', '@chatResponse', '@activeBrowserUserIDs', '@gameResponse']);
});

Cypress.Commands.add('goToGame', (userState = {}, userID) => {
    userID = userID || USER_ID;
    const activeUserIDs = userState.activeUserIDs || [userID];
    const abilities = userState.abilities || {
        type: ['kill'],
        kill: {
            players: [{ playerName: PLAYER_NAME }],
        },
    };
    const actions = userState.actions || {
        actionList: [],
    };
    const authToken = new Array(128 + 1).join('A');
    const chats = userState.chats || chatResponse.getEmpty();
    const graveyard = userState.graveyard || [];
    const integrations = userState.integrations || [];
    const phase = userState.phase || phaseModel.getDay();
    const players = userState.players || [];
    const profile = userState.profile || profileModel.getInGame();
    const setup = userState.setup || setupModel.getDefaultStarted();
    cy.server();
    cy.route({
        method: 'GET',
        url: `games/${GAME_ID}`,
        response: {
            response: gameModel.get({
                integrations,
                isStarted: true,
                phase,
                players,
                setup,
            }),
        },
    });
    cy.route({
        method: 'GET',
        url: 'profiles',
        response: { response: profile },
    });
    cy.route({
        method: 'GET',
        url: 'setups',
        response: setupFeaturedResponse.get(),
    });
    cy.route({
        method: 'GET',
        url: 'users/id',
        response: { response: { id: userID } },
    }).as('userResponse');
    cy.route({
        method: 'GET',
        url: `users?auth_token=${authToken}`,
        response: authTokenResponse.get(userID),
    }).as('userResponse');
    cy.route({
        method: 'POST',
        url: 'user_integrations*',
        response: authTokenResponse.get(userID),
    });
    cy.route({
        method: 'GET',
        url: `channels/browser/activeUserIDs?game_id=${GAME_ID}`,
        response: { response: activeUserIDs },
    });

    cy.route({
        method: 'GET',
        url: 'chats',
        response: { response: chats },
    }).as('chatResponse');

    const responseObj = userStateSetupResponse.getStartedGame();
    Object.keys(userState || [])
        .forEach(key => {
            responseObj[key] = userState[key];
        });
    responseObj.actions = actions;
    responseObj.playerLists = abilities;
    responseObj.setup = setup;
    responseObj.graveYard = graveyard;
    responseObj.type.push('graveYard');
    responseObj.type.push('playerLists');
    responseObj.type.push('actions');
    cy.route({
        method: 'GET',
        url: 'games/userState',
        response: { response: responseObj },
    }).as('userStateResponse');

    cy.visit(`http://localhost:4501/?auth_token=${authToken}`, {
        onLoad: stubFirebase,
    });
    cy.wait(['@userStateResponse', '@chatResponse', '@userResponse']);
});

Cypress.Commands.add('mobile', () => {
    cy.viewport(400, 800);
});

Cypress.Commands.add('goToCondorcet', goToCondorcet);

function goToCondorcet(params = {}){
    cy.server();
    cy.route({
        method: 'GET',
        url: '/channels/sc2mafia/condorcet?threadID=1',
        response: { response: params.threadVotes || threadVotesModel.get() },
    }).as('getVotesResponse');

    cy.route({
        method: 'POST',
        url: '/condorcet',
        response: { response: params.condorcetResponse || [] },
        onRequest: params.condorcetSubmitRequest,
    }).as('condorcetResponse');

    const urlParams = params.urlParams || {};
    urlParams.thread = 1;
    const url = `http://localhost:4501/condorcet.html${encodeURL(urlParams)}`;
    cy.visit(url);

    cy.wait(['@getVotesResponse', '@condorcetResponse']);
}

function encodeURL(params){
    if(!params)
        return '';
    return Object.keys(params).map((key, index) => {
        const value = params[key];
        if(index)
            return `&${key}=${value}`;
        return `?${key}=${value}`;
    }).join('');
}

function stubFirebase(window){
    window.firebase = {
        auth: () => ({
            onAuthStateChanged: passedInFunction => {
                const user = {
                    getIdToken: async() => '',
                };
                Promise.resolve().then(() => {
                    passedInFunction(user);
                });
                return () => {};
            },
            signInAnonymously: () => Promise.resolve(),
            signOut: () => Promise.resolve(),
        }),
    };
}
