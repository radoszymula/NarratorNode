// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands';
import chaiColors from 'chai-colors';

chai.use(chaiColors);

// Alternatively you can use CommonJS syntax:
// require('./commands')

before('External request mask', () => {
    // Runs before all other tests
    // cy.server();
    // cy.route({
    //     method: 'GET',
    //     url: '*',
    //     response: [],
    // });
    // cy.route({
    //     method: 'GET',
    //     url: 'users/id',
    //     response: {
    //         response: {
    //             id: 1,
    //         },
    //     },
    // });
});
