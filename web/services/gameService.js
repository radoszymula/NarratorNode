const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function addGameObjectProperties(game, requestingUserID){
    game.integrations = require('./gameIntegrationService').getIntegrations(game.joinID);
    game.players = await require('./playerService').filterKeys(game, requestingUserID);
    return game;
}

async function create(userID, hostName, setupName, setupID, rules){
    const args = {
        userID,
    };
    if(setupID)
        args.setupID = setupID;
    else
        args.setupKey = setupName || 'custom';

    args.isPublic = false;
    args.rules = rules || {};
    args.hostName = hostName || 'guest';

    const javaResponse = await javaRequests.sendRequest('POST', 'games', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    const game = javaResponse.response;
    return addGameObjectProperties(game);
}

function deleteGame(joinID){
    const args = {
        joinID,
    };
    return javaRequests.sendRequest('DELETE', 'games', args);
}

async function getAll(){
    return (await javaRequests.sendRequest('GET', 'games')).response;
}

async function getByID(gameID, requestingUserID){
    const game = await javaRequests.sendRequest('GET', 'games', { gameID });
    if(helpers.isEmptyObject(game.response))
        return null;
    return addGameObjectProperties(game.response, requestingUserID);
}

async function getByJoinID(joinID, requestingUserID){
    const game = await javaRequests.sendRequest('GET', 'games', { joinID });
    if(helpers.isEmptyObject(game.response))
        return null;
    return addGameObjectProperties(game.response, requestingUserID);
}

async function getBySetupID(setupID){
    const game = await javaRequests.sendRequest('GET', 'games', { setupID });
    if(helpers.isEmptyObject(game.response))
        return null;
    return addGameObjectProperties(game.response);
}


async function getUserState(userID){
    const args = { userID };
    const { response } = await javaRequests.sendRequest('GET', 'getGameState', args);
    if(!response)
        return {};
    if(response.gameStart && !response.isFinished){
        const phaseService = require('./phaseService');
        response.timer = phaseService.getEndTime(response.joinID);
    }
    return response;
}

async function start(userID){
    const args = { userID, start: true };
    const javaResponse = await javaRequests.sendRequest('POST', 'games', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    return addGameObjectProperties(javaResponse.response);
}

module.exports = {
    addGameObjectProperties,
    create,
    deleteGame,
    getAll,
    getByID,
    getByJoinID,
    getBySetupID,
    getUserState,
    start,
};
