const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function create(userID){
    const args = { userID };
    const javaResponse = await javaRequests.sendRequest('POST', 'moderators', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

async function del(userID, playerName){
    playerName = playerName || 'oldmoddy';
    const args = { userID, playerName };
    const javaResponse = await javaRequests.sendRequest('DELETE', 'moderators', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

async function repick(gameID, userID, repickTarget){
    const app = require('../app');
    const activeUserIDs = await app.browserWrapper.getActiveGameUsersByUserID(gameID);
    repickTarget = repickTarget || '';
    const args = { activeUserIDs: [...activeUserIDs], userID, repickTarget };
    const javaResponse = await javaRequests.sendRequest('POST', 'moderators', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

module.exports = {
    create,
    delete: del,
    repick,
};
