const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function addBots(userID, botCount){
    const serverResponse = await javaRequests.sendRequest('POST', 'players', { userID, botCount });
    if(serverResponse.errors.length)
        throw helpers.httpError(serverResponse.errors, 422);
    return serverResponse.response;
}

async function create(userID, playerName, joinID){
    const args = {
        userID,
        joinID,
        playerName: playerName || 'guest',
    };
    const javaResponse = await javaRequests.sendRequest('POST', 'players', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    return require('./gameService').addGameObjectProperties(javaResponse.response);
}

async function filterKeys(game, requesterUserID){
    if(!game.isStarted || !requesterUserID)
        return game.players;
    const phaseService = require('./phaseService');
    const phaseLength = phaseService.getPhaseLength(game.joinID) * 1000;
    const endTime = phaseService.getEndTime(game.joinID);
    const now = new Date().getTime();
    if(now > endTime - (phaseLength / 3))
        return game.players;
    return game.players.map(player => {
        if(player.userID === requesterUserID)
            return player;
        return { ...player, endedNight: false };
    });
}

async function kick(kickerID, playerID){
    const args = { kickerID, playerID };
    const serverResponse = await javaRequests.sendRequest('DELETE', 'players', args);
    if(serverResponse.errors.length)
        throw helpers.httpError(serverResponse.errors, 422);
    return serverResponse.response;
}

async function leave(userID){
    const args = {
        leaverID: userID,
        kickerID: userID,
    };
    const responseObj = await javaRequests.sendRequest('DELETE', 'players', args);
    if(responseObj.errors.length)
        throw helpers.httpError(responseObj.errors, 422);
}

async function modkill(userID){
    const responseObj = await javaRequests.sendRequest('PUT', 'players', { userID });
    if(responseObj.errors.length)
        throw helpers.httpError(responseObj.errors, 422);
}

module.exports = {
    addBots,
    create,
    filterKeys,
    kick,
    leave,
    modkill,
};
