const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');

const setupRepo = require('../repos/setupRepo');


async function getFeaturedSetups(userID){
    const args = {
        userID,
    };
    return (await javaRequests.sendRequest('GET', 'setups', args)).response;
}

async function getByID(setupID){
    const args = {
        setupID,
    };
    return (await javaRequests.sendRequest('GET', 'setups', args)).response;
}

async function setSetup(args){
    args = {
        joinID: args.joinID,
        setupID: args.setupID,
        setupKey: args.setupKey,
        userID: args.userID,
    };
    const javaResponse = await javaRequests.sendRequest('PUT', 'setups', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    require('./eventService').setupChange(javaResponse.response);
    return javaResponse.response;
}

async function create(userID){
    const args = {
        userID,
        setup: {
            name: 'My custom setup',
        },
    };
    return (await javaRequests.sendRequest('POST', 'setups', args)).response;
}

function deleteSetup(setupID, userID){
    return setupRepo.deleteEditableSetups(setupID, userID)
        .then(() => setupRepo.deactivateNoneditableSetups(setupID, userID));
}

async function onSetupChange(setupID){
    const gameService = require('./gameService');
    const game = await gameService.getBySetupID(setupID);
    if(!game)
        return;

    const eventService = require('./eventService');
    eventService.setupChange(game);
}

module.exports = {
    create,
    getFeaturedSetups,
    getByID,
    setSetup,
    deleteSetup,
    onSetupChange,
};
