const helpers = require('../utils/helpers');

const userRepo = require('../repos/userRepo');

const integrationType = require('../models/enums/integrationTypes');
const userTypes = require('../models/enums/userTypes');

const sc2mafiaClient = require('../channels/sc2mafia/sc2mafiaClient');


const TEN_MINUTES = 1000 * 60 * 10;

let lastCheckTime = new Date();

async function findGameDeltas(){
    const requestTime = lastCheckTime;
    lastCheckTime = new Date();
    const gameDeltas = await sc2mafiaClient.findGameDeltas(requestTime);

    let gamesAndUsers = await Promise.all(gameDeltas.map(getExternalGameUser));
    const gameService = require('./gameService');
    const games = await gameService.getAll();

    const userIDsInGames = games.map(game => {
        const playerIDs = game.players.map(player => player.userID);
        return [game.host.id].concat(playerIDs);
    });
    const usersInGamesSet = new Set(helpers.flattenList(userIDsInGames));
    gamesAndUsers = gamesAndUsers
        .filter(userAndGame => !userAndGame.user || !usersInGamesSet.has(userAndGame.user.id));

    return postNewGames(gamesAndUsers, gameService);
}

setInterval(findGameDeltas, TEN_MINUTES);

module.exports = {
    findGameDeltas,
};

function postNewGames(gameRequests, gameService){
    const userService = require('./userService');
    const gameIntegrationService = require('./gameIntegrationService');
    return Promise.all(gameRequests.map(async request => {
        const user = request.user || await userRepo.create(false, request.externalUserID,
            request.integrationType);
        const game = await gameService.create(user.id, request.externalGame.name, 'classic', null,
            request.externalGame.rules);
        gameIntegrationService.register(game.joinID, integrationType.SC2MAFIA);
        const token = await userService.addTempAuth(user.id);
        const externalInfo = await sc2mafiaClient.postGame(request.externalGame, game, token);
        return {
            externalInfo,
            joinID: game.joinID,
            host: user,
        };
    }));
}

async function getExternalGameUser(externalGame){
    const user = await userRepo.getByExternalID(externalGame.externalUserID, userTypes.SC2MAFIA);
    return {
        externalGame,
        user,
    };
}
