const integrationTypes = require('../models/enums/integrationTypes');


const joinIDToIntegrations = {};

function getIntegrations(joinID){
    const integrations = [...joinIDToIntegrations[joinID] || []];
    integrations.push(integrationTypes.BROWSER);
    return integrations;
}

function register(joinID, integrationType){
    if(!joinIDToIntegrations[joinID])
        joinIDToIntegrations[joinID] = [integrationType];
    else if(!joinIDToIntegrations[joinID].includes(integrationType))
        joinIDToIntegrations[joinID].push(integrationType);
}

function remove(joinID){
    delete joinIDToIntegrations[joinID];
}

module.exports = {
    getIntegrations,
    register,
    remove,
};
