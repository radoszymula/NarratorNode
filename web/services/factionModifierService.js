const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function update(userID, factionID, args){
    args = Object.assign({}, { userID }, { factionID }, args);
    const javaResponse = await javaRequests.sendRequest('PUT', 'factionModifiers', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    const setupService = require('./setupService');
    // noinspection JSIgnoredPromiseFromCall
    setupService.onSetupChange(javaResponse.response.setupID);
    return javaResponse.response.modifier;
}

module.exports = {
    update,
};
