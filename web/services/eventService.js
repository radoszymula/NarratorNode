const helpers = require('../utils/helpers');

const gameIntegrationService = require('./gameIntegrationService');
const phaseService = require('./phaseService');
const timerService = require('./timerService');


function actionSubmit(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onActionSubmit(request);
        });
}

function broadcast(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onBroadcast(request);
        });
}

function closeLobby(request){
    timerService.stopTimeout(request.joinID);
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onLobbyClose(request);
        });
    gameIntegrationService.remove(request.joinID);
}

function dayDeath(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onDayDeath(request);
        });
}

function dayStart(request){
    phaseService.setExpiration(request);
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onDayStart(request);
        });
}

function gameEnd(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onGameEnd(request);
        });
}

function gameStart(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onGameStart(request);
        });
}

function hostChange(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onHostChange(request);
        });
}

function newPlayer(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onNewPlayer(request);
        });
}

function newVote(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onNewVote(request);
        });
}

function nightStart(request){
    phaseService.setExpiration(request);
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onNightStart(request);
        });
}

function phaseEndBid(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onPhaseEndBid(request);
        });
}

function phaseReset(request){
    phaseService.setExpiration(request);
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onPhaseReset(request);
        });
}

function playerRemove(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onPlayerRemove(request);
        });
}

function roleCardUpdate(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onRoleCardUpdate(request);
        });
}

function rolePickingStart(request){
    phaseService.setExpiration(request);
}

function setupChange(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onSetupChange(request);
        });
}

function setupHiddenAdd(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onSetupHiddenAdd(request);
        });
}

function setupHiddenRemove(request){
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onSetupHiddenRemove(request);
        });
}

function trialStart(request){
    phaseService.setExpiration(request);
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onTrialStart(request);
        });
}

function votePhaseReset(request){
    phaseService.setExpiration(request);
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onVotePhaseReset(request);
        });
}

function votePhaseStart(request){
    phaseService.setExpiration(request);
    gameIntegrationService.getIntegrations(request.joinID)
        .map(helpers.getWrapper)
        .forEach(client => {
            client.onVotePhaseStart(request);
        });
}

module.exports = {
    actionSubmit,
    broadcast,
    closeLobby,
    dayDeath,
    dayStart,
    gameEnd,
    gameStart,
    hostChange,
    newPlayer,
    newVote,
    nightStart,
    phaseEndBid,
    phaseReset,
    playerRemove,
    roleCardUpdate,
    rolePickingStart,
    setupChange,
    setupHiddenAdd,
    setupHiddenRemove,
    trialStart,
    votePhaseReset,
    votePhaseStart,
};
