const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function edit(userID, request){
    const args = {
        userID,
        id: request.id || request.name,
        value: request.value,
    };
    const javaResponse = await javaRequests.sendRequest('PUT', 'setupModifiers', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    const setupService = require('./setupService');
    setupService.onSetupChange(javaResponse.response.setupID);
    return javaResponse.response.modifier;
}

module.exports = {
    edit,
};
