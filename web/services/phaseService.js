const javaRequests = require('../utils/javaRequests');
const helpers = require('../utils/helpers');

const gamePhase = require('../models/enums/gamePhase');


const joinIDToEndTime = {};

function getEndTime(joinID){
    return joinIDToEndTime[joinID].endTime;
}

function getPhaseLength(joinID){
    return joinIDToEndTime[joinID].phaseLength;
}

function setExpiration(request){
    const secondsRemaining = request.phase.length;
    if(joinIDToEndTime[request.joinID]){
        clearTimeout(joinIDToEndTime[request.joinID].timeoutFunc);
        clearTimeout(joinIDToEndTime[request.joinID].phaseEndWarning);
    }
    if(secondsRemaining <= 0)
        return endPhaseNow(request.joinID);
    if(!secondsRemaining)
        return new Date().getTime();

    const millisecondsRemaining = secondsRemaining * 1000;
    const timeoutFunc = setTimeout(() => {
        const args = {
            joinID: request.joinID,
            phaseHash: request.phase.hash,
        };
        javaRequests.sendRequest('PUT', 'phases', args);
    }, millisecondsRemaining);
    const phaseEndWarning = request.phase.name === gamePhase.NIGHTTIME && setTimeout(async() => {
        const gameService = require('./gameService');
        const game = await gameService.getByJoinID(request.joinID);
        if(game)
            require('./eventService').phaseEndBid({
                event: 'phaseEndBid',
                gameID: game.id,
                players: game.players,
            });
    }, millisecondsRemaining * 0.6666);
    const newEndTime = new Date().getTime() + millisecondsRemaining;
    joinIDToEndTime[request.joinID] = {
        endTime: newEndTime,
        phaseLength: secondsRemaining,
        phaseEndWarning,
        timeoutFunc,
        hash: request.phase.hash,
    };
    return newEndTime;
}

async function setExpirationViaUser(userID, joinID, seconds){
    await require('./userPermissionService').verifyAdmin(userID);

    const phaseObject = joinIDToEndTime[joinID];
    if(!phaseObject)
        throw helpers.httpError('Phase with that id not found.', 422);
    return setExpiration({
        joinID,
        phase: {
            length: seconds,
            hash: phaseObject.hash,
        },
    });
}

module.exports = {
    getEndTime,
    getPhaseLength,
    setExpiration,
    setExpirationViaUser,
};

function endPhaseNow(joinID){
    javaRequests.sendRequest('PUT', 'phases', { joinID, phaseHash: '' });
    return 0; // this might need to be new Date().getTime()
}
