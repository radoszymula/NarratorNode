const sc2mafiaClient = require('../channels/sc2mafia/sc2mafiaClient');

const userType = require('../models/enums/userTypes');


function clearCache(threadID){
    sc2mafiaClient.clearCache(threadID);
}

async function sc2mafiaNewPost(threadID){
    const playerService = require('./playerService');
    const userService = require('./userService');
    const actions = await sc2mafiaClient.onNewPost(threadID);
    const promises = actions.playerJoins.map(async playerJoinRequest => {
        const user = await userService.getByExternalID(false, playerJoinRequest.externalID,
            userType.SC2MAFIA);
        await playerService.create(user.id, playerJoinRequest.name, playerJoinRequest.joinID);
        const authToken = userService.addTempAuth(user.id);
        return sc2mafiaClient.sendNarratorLink(playerJoinRequest.name, authToken);
    });
    const playerVotePromise = getPlayerVotePromise(actions.playerVotes);
    promises.push(playerVotePromise);
    return Promise.all(promises);
}

function getCondorcetVotes(threadID){
    return sc2mafiaClient.getCondorcetVotes(threadID);
}

function getActiveBrowserUserIDs(gameID){
    const app = require('../app');
    return app.browserWrapper.getActiveGameUsersByUserID(gameID);
}

module.exports = {
    clearCache,
    getActiveBrowserUserIDs,
    getCondorcetVotes,
    sc2mafiaNewPost,
};

async function getPlayerVotePromise(playerVotes){
    const actionService = require('./actionService');
    const userService = require('./userService');
    for(let i = 0; i < playerVotes.length; i++){
        const playerVote = playerVotes[i];
        const user = await userService.getByExternalID(false, playerVote.externalID,
            userType.SC2MAFIA);
        if(!user)
            continue;
        await actionService.submitActionMessage(user.id,
            { message: `vote ${playerVote.voteTarget}` })
            .catch(() => {});
    }
}
