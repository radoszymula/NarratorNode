const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function add(userID, hiddenID){
    const args = {
        userID,
        hiddenID,
    };
    const javaResponse = await javaRequests.sendRequest('POST', 'setupHiddens', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    require('./eventService').setupHiddenAdd(javaResponse.response);
    return javaResponse.response;
}


async function remove(userID, setupHiddenID){
    const args = {
        userID,
        setupHiddenID,
    };
    const javaResponse = await javaRequests.sendRequest('DELETE', 'setupHiddens', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    require('./eventService').setupHiddenRemove({
        gameID: javaResponse.response.gameID,
        setupHiddenID,
    });
}

module.exports = {
    add,
    remove,
};
