const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function cancelAction(userID, actionIndex, command){
    const args = { userID, actionIndex, command };
    const responseObj = await javaRequests.sendRequest('DELETE', 'actions', args);
    if(responseObj.errors.length)
        throw helpers.httpError(responseObj.errors, 422);
}

async function submitActionMessage(userID, message){
    const args = Object.assign({ userID }, message);
    const responseObj = await javaRequests.sendRequest('PUT', 'actions', args);
    if(responseObj.errors.length)
        throw helpers.httpError(responseObj.errors, 422);
    return responseObj.response;
}

module.exports = {
    cancelAction,
    submitActionMessage,
};
