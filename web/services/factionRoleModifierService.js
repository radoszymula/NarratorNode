const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function update(userID, factionRoleID, args){
    args = Object.assign({}, { userID }, { factionRoleID }, args);
    const javaResponse = await javaRequests.sendRequest('PUT', 'factionRoleModifiers', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    const setupService = require('./setupService');
    setupService.onSetupChange(javaResponse.response.setupID);
    return javaResponse.response.modifier;
}

module.exports = {
    update,
};
