const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function createFaction(args){
    const response = await javaRequests.sendRequest('POST', 'factions', args);
    if(response.errors.length)
        throw helpers.httpError(response.errors, 422);
    const setupService = require('./setupService');
    setupService.onSetupChange(response.response.setupID);
    return response.response;
}

async function deleteFaction(args){
    const response = await javaRequests.sendRequest('DELETE', 'factions', args);
    if(response.errors.length)
        throw helpers.httpError(response.errors, 422);
    const setupService = require('./setupService');
    setupService.onSetupChange(response.response.setupID);
    return response.response;
}

module.exports = {
    createFaction,
    deleteFaction,
};
