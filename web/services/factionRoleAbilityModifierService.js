const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function update(userID, factionRoleID, abilityID, args){
    args = Object.assign({}, { userID }, { factionRoleID, abilityID }, args);
    const javaResponse = await javaRequests.sendRequest('PUT', 'factionRoleAbilityModifiers', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    const setupService = require('./setupService');
    setupService.onSetupChange(javaResponse.response.setupID);
    return javaResponse.response.modifier;
}

module.exports = {
    update,
};
