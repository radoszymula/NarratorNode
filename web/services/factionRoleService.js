const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function get(userID, factionRoleID){
    const javaResponse = await javaRequests.sendRequest('GET', 'factionRoles',
        { userID, factionRoleID });
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

module.exports = {
    get,
};
