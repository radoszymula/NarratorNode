const authValidators = require('../validators/authValidators');
const helpers = require('../utils/helpers');

const userRepo = require('../repos/userRepo');
const userIntegrationsRepo = require('../repos/userIntegrationsRepo');


const tempAuthToID = {};
const tempIDToAuth = {};

function addTempAuth(userID){
    userRepo.getByID(userID);
    const authToken = helpers.getRandomString(128);
    if(tempIDToAuth[userID]){
        const oldToken = tempIDToAuth[userID];
        delete tempAuthToID[oldToken];
    }
    tempAuthToID[authToken] = userID;
    tempIDToAuth[userID] = authToken;
    return authToken;
}

async function getByAuthToken(request){
    authValidators.socketAuth(request);
    const { token } = request;
    const wrapperType = request.wrapperType.toLowerCase();

    if(wrapperType === 'tempAuth'.toLowerCase()){
        if(tempAuthToID[token])
            return { id: tempAuthToID[token] };
        throw helpers.httpError('Authentication failed.', 422);
    }

    const wrapper = helpers.getWrapper(wrapperType);
    const externalUserInfo = await wrapper.getUserInfoByAuthToken(token);
    if(!externalUserInfo)
        throw helpers.httpError('Authentication failed.', 422);

    return getByExternalID(externalUserInfo.isGuest, externalUserInfo.externalID, wrapperType);
}

function getIDByAuthToken(authToken){
    return tempAuthToID[authToken];
}

async function getByExternalID(isGuest, externalID, wrapperType){
    const user = await userRepo.getByExternalID(externalID, wrapperType);
    if(user)
        return getNestedInfo(user.id, user);

    return userRepo.create(isGuest, externalID, wrapperType);
}

async function getNestedInfo(userID, cachedUser, cachedIntegrations){
    const user = cachedUser || await userRepo.getByID(userID);
    user.integrations = cachedIntegrations || await userIntegrationsRepo.getByUserID(userID);
    return user;
}

module.exports = {
    addTempAuth,
    getByAuthToken,
    getByExternalID,
    getIDByAuthToken,
    getNestedInfo,
};
