const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function get(userID, roleID){
    const javaResponse = await javaRequests.sendRequest('GET', 'roles', { userID, roleID });
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

module.exports = {
    get,
};
