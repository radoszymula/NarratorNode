const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function get(userID){
    const javaResponse = await javaRequests.sendRequest('GET', 'profiles', { userID });
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    javaResponse.response.userID = userID;
    return javaResponse.response;
}

module.exports = {
    get,
};
