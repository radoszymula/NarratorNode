const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function create(userID, request){
    request.userID = userID;
    const javaResponse = await javaRequests.sendRequest('POST', 'hiddens', request);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}


module.exports = {
    create,
};
