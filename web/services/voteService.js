const helpers = require('../utils/helpers');
const javaRequests = require('../utils/javaRequests');


async function getVotes(joinID){
    const args = {
        joinID,
    };
    const javaResponse = await javaRequests.sendRequest('GET', 'votes', args);
    if(javaResponse.errors.length)
        throw helpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

module.exports = {
    getVotes,
};
