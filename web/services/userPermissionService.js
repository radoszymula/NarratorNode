const helpers = require('../utils/helpers');

const userPermission = require('../models/enums/userPermissions');

const userPermissionRepo = require('../repos/userPermissionRepo');


function setAdmin(userID){
    return userPermissionRepo.create(userID, userPermission.ADMIN);
}

async function verifyAdmin(userID){
    const permissions = await userPermissionRepo.getByUserID(userID);
    if(!permissions.includes(userPermission.ADMIN))
        throw helpers.httpError('User is not an admin', 401);
}

module.exports = {
    setAdmin,
    verifyAdmin,
};
