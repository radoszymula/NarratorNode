const javaRequests = require('../utils/javaRequests');


async function resolveInput(votes){
    const args = {
        mayorResolve: false,
        votes,
    };
    const serverResponse = await javaRequests.sendRequest('GET', 'condorcet', args);
    return serverResponse.response;
}

module.exports = {
    resolveInput,
};
