const javaRequests = require('../utils/javaRequests');


async function getUserChat(userID){
    return (await javaRequests.sendRequest('GET', 'chats', { userID })).response;
}

module.exports = {
    getUserChat,
};
