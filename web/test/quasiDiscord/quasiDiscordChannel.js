const helpers = require('../../utils/helpers');

const Collection = require('./collections').Collections;

const SEND_CHANNEL_ERROR = 'You can\'t talk here.';


function QuasiDiscordChannel(id, guild){
    this.clients = [];
    this.guild = guild;
    this.messageHistory = {};
    this.permissionOverwrites = new Collection();
    this._resetPermissions();

    if(typeof id === 'string'){
        this.id = id;
        this.type = 'text';
    }else{
        this.id = `dm_${id[0].id}_${id[1].id}`;
        this.type = 'dm';

        this._join(id[0]);
        this._join(id[1]);
    }
}

QuasiDiscordChannel.prototype._canSendMessage = function(id){
    const quasiDiscordClient = require('./quasiDiscordClient');
    if(!Object.keys(this._permittedSpeakers).length) // no restrictions put in this channel
        return true;
    if(id === quasiDiscordClient.narratorClientID)
        return true;
    if(id in this._permittedSpeakers)
        return this._permittedSpeakers[id];
    if(this.guild.defaultRole.id in this._permittedSpeakers)
        return this._permittedSpeakers[this.guild.defaultRole.id];
    return false;
};

QuasiDiscordChannel.prototype._editMessage = function(oldMessageText, newMessage, channelReference){
    const { author } = channelReference;
    const messageUpdateListeners = getListeners(this.clients, 'messageUpdate');

    messageUpdateListeners.forEach(clientListenerFunction => {
        const { client } = clientListenerFunction;
        const { listenerFunction } = clientListenerFunction;
        const clientChannel = client.channels.get(channelReference.id);
        listenerFunction(null, {
            _edits: [{ content: oldMessageText }],
            author,
            channel: clientChannel,
            content: newMessage,
            reply: replyMessage => {
                const message = `<@!${author.user.id}>, ${replyMessage}`;
                return clientChannel.send(message);
            },
        });
    });
};

QuasiDiscordChannel.prototype._join = function(client){
    const quasiDiscordChannelReference = require('./quasiDiscordChannelReference');
    const { QuasiDiscordChannelReference } = quasiDiscordChannelReference;

    this.clients.push(client);
    const channelReference = new QuasiDiscordChannelReference(this, client);
    client.channels.put(this.id, channelReference);

    return channelReference;
};

QuasiDiscordChannel.prototype._overwritePermissions = async function(entity, permissionObj){
    if('SEND_MESSAGES' in permissionObj)
        this._permittedSpeakers[entity] = permissionObj.SEND_MESSAGES;
};

QuasiDiscordChannel.prototype._resetPermissions = function(){
    this._permittedSpeakers = {};

    this.permissionOverwrites.deleteAll();
    this.permissionOverwrites.put('', {
        delete: () => {
            this._resetPermissions();
            return Promise.resolve();
        },
        type: 'member',
    });
};

QuasiDiscordChannel.prototype.send = function(messageText, channelReference){
    const { author } = channelReference;
    if(!this._canSendMessage(author.id))
        return Promise.reject(SEND_CHANNEL_ERROR);

    const messageListeners = getListeners(this.clients, 'message');
    const messageID = helpers.getRandomString(5);

    const message = {
        author,
        content: messageText,
        edit: async m => this._editMessage(messageText, m, channelReference),
        channel: channelReference,
        delete: async() => {},
        id: messageID,
    };
    this.messageHistory[message.id] = message;

    messageListeners.forEach(clientListenerFunction => {
        const { client } = clientListenerFunction;
        const { listenerFunction } = clientListenerFunction;
        const clientChannel = client.channels.get(channelReference.id);
        listenerFunction({
            author: message.author,
            channel: clientChannel,
            content: message.content,
            delete: async() => {},
            edit: message.edit,
            id: messageID,
            member: {
                nickname: author.nickname,
            },
            reply: replyMessage => {
                replyMessage = `<@!${author.user.id}>, ${replyMessage}`;
                return clientChannel.send(replyMessage);
            },
        });
    });

    return Promise.resolve(message);
};

module.exports = {
    QuasiDiscordChannel,

    PUBLIC: 'text',
    PRIVATE: 'dm',
    SEND_CHANNEL_ERROR,
};

function getListeners(clients, listenerType){
    const listeners = clients
        .map(client => (client.listeners[listenerType] || [])
            .map(listenerFunction => ({
                client,
                listenerFunction,
            })));
    return [].concat(...listeners);
}
