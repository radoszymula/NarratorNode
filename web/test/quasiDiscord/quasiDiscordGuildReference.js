const { Collections } = require('./collections');


function QuasiDiscordGuildReference(client, guild){
    this.id = guild.id;
    this.channels = new Collections();
    this.client = client;
    this.defaultRole = guild.defaultRole;
    this.guild = guild;
    this.members = guild.members;
}

QuasiDiscordGuildReference.prototype.createChannel = function(channelName, channelType){
    const channel = this.guild._createChannel(channelName, channelType);
    const channelRef = channel._join(this.client);
    this.channels.put(channelRef.id, channelRef);
    return Promise.resolve(channelRef);
};

module.exports = {
    QuasiDiscordGuildReference,
};
