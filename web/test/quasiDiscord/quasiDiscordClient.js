const config = require('../../../config');

const helpers = require('../../utils/helpers');

const quasiDiscordChannel = require('./quasiDiscordChannel');
const quasiDiscordGuild = require('./quasiDiscordGuild');

const { Collections } = require('./collections');

const { QuasiDiscordChannel } = quasiDiscordChannel;
const { QuasiDiscordGuild } = quasiDiscordGuild;


const homeChannelID = config.discord.test_channel_id;
const testChannel2 = config.discord.test_channel_id2;
const nightChatGuildID = config.discord.night_chat_server;

const narratorClientID = 'narratorClientID';

const nightChatGuild = new QuasiDiscordGuild(nightChatGuildID);
const mainGuild = new QuasiDiscordGuild();

const mainChannel = new QuasiDiscordChannel(homeChannelID, mainGuild);
const mainChannel2 = new QuasiDiscordChannel(testChannel2, mainGuild);

mainGuild.channels.put(homeChannelID, mainChannel);
mainGuild.channels.put(testChannel2, mainChannel2);

let counter = -1;
const clients = {};

function QuasiDiscordClient(){
    this.channels = new Collections();
    this.guilds = new Collections();
    this.listeners = {};
    let id;
    if(counter++ === -1){
        id = narratorClientID;
        this.username = 'TheNarrator';
        this.nickname = 'TheNarrator';
        nightChatGuild._join(this);
    }else{
        id = `user_${helpers.getRandomString(5)}`;
        this.username = `ogPlayer${counter}`;
        this.nickname = `Player${counter}`;
    }
    this.user = {
        id,
        username: this.username,
    };
    this.id = id;
    mainGuild._join(this);

    mainChannel._join(this);
    mainChannel2._join(this);

    Object.values(clients).forEach(client => {
        new QuasiDiscordChannel([this, client]); /* eslint-disable-line no-new */
    });

    clients[this.user.id] = this;
}

function resetCounter(){
    counter = 0;
}

QuasiDiscordClient.prototype.createDM = function(){

};

QuasiDiscordClient.prototype.login = function(){
    const readyListeners = this.listeners.ready || [];
    readyListeners.forEach(f => {
        f();
    });
};

QuasiDiscordClient.prototype.fetchUser = function(userID){
    const createDMFunction = () => {
        // find private room that has my user and the user id in question
        // and return the channel i found it in

        const channels = Object.values(this.channels.dict)
            .filter(channelRefs => channelRefs.type === quasiDiscordChannel.PRIVATE)
            .filter(channelRefs => channelRefs.channel.clients
                .map(client => client.id)
                .includes(userID));
        return Promise.resolve(channels[0]);
    };
    return Promise.resolve({
        createDM: createDMFunction,
        username: clients[userID].username,
        presence: {
            status: 'online',
        },
    });
};

QuasiDiscordClient.prototype.on = function(eventType, f){
    if(this.listeners[eventType])
        this.listeners[eventType].push(f);
    else
        this.listeners[eventType] = [f];
};

module.exports = {
    Client: QuasiDiscordClient,
    narratorClientID,
    resetCounter,
};
