const helpers = require('../../utils/helpers');

const { Collections } = require('./collections');


function QuasiDiscordGuild(guildID){
    if(!guildID)
        guildID = `guild_${helpers.getRandomString(5)}`;
    this.channels = new Collections();
    this.defaultRole = { id: `role_${helpers.getRandomString(5)}` };
    this.id = guildID;
    this.members = new Collections();
}

QuasiDiscordGuild.prototype._createChannel = function(){
    const quasiDiscordChannel = require('./quasiDiscordChannel');
    const { QuasiDiscordChannel } = quasiDiscordChannel;

    const channelID = `channel_${helpers.getRandomString(5)}`;
    return new QuasiDiscordChannel(channelID, this);
};

QuasiDiscordGuild.prototype._join = function(client){
    const quasiDiscordGuildReference = require('./quasiDiscordGuildReference');
    const { QuasiDiscordGuildReference } = quasiDiscordGuildReference;

    const guildReference = new QuasiDiscordGuildReference(client, this);
    client.guilds.put(this.id, guildReference);
    this.members.put(client.id, client);
};

module.exports = {
    QuasiDiscordGuild,
};
