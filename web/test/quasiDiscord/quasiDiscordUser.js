const Discord = require('discord.js');

const config = require('../../../config');
const Constants = require('../../channels/discord/Constants');
const userTypes = require('../../models/enums/userTypes');

const gameService = require('../../services/gameService');
const setupService = require('../../services/setupService');
const userService = require('../../services/userService');

const helpers = require('../../utils/helpers');
const javaRequests = require('../../utils/javaRequests');

const quasiDiscordChannel = require('./quasiDiscordChannel');

// https://discord.js.org/#/
// https://github.com/jagrosh/MusicBot/wiki/Adding-Your-Bot-To-Your-Server

/*
Bot.
https://discordapp.com/oauth2/authorize?client_id=394259731172163587&scope=bot
1.
https://discordapp.com/oauth2/authorize?client_id=383529750049718272&scope=bot
2.
https://discordapp.com/oauth2/authorize?client_id=385650126418345987&scope=bot
3.
https://discordapp.com/oauth2/authorize?client_id=387719001704366082&scope=bot
4.
https://discordapp.com/oauth2/authorize?client_id=387848473606619156&scope=bot
5.
https://discordapp.com/oauth2/authorize?client_id=387848894366613504&scope=bot
6.
https://discordapp.com/oauth2/authorize?client_id=387849191029866496&scope=bot
7.
https://discordapp.com/oauth2/authorize?client_id=387849406927601664&scope=bot
8.
https://discordapp.com/oauth2/authorize?client_id=389663582473814016&scope=bot
*/


function QuasiDiscordUser(){
    this.mListeners = [];
    this.discordClient = new Discord.Client();
    this.discordClient.on('message', message => {
        for(let i = 0; i < this.mListeners.length; i++)
            if(this.mListeners[i](message)){
                this.mListeners.splice(i, 1);
                i--;
            }
    });
    this.id = helpers.getRandomString(10);
}

QuasiDiscordUser.prototype.waitForMessage = function(waitFunction){
    return new Promise(resolve => {
        this.mListeners.push(message => {
            if(this.discordClient.user.id === message.author.id)
                return;
            if(waitFunction(message)){
                resolve();
                return true;
            }
        });
    });
};

QuasiDiscordUser.prototype.getID = function(){
    return this.discordClient.user.id;
};

QuasiDiscordUser.prototype.getName = function(){
    return this.discordClient.nickname;
};

QuasiDiscordUser.prototype.getMention = function(){
    return `<@!${this.getID()}>`;
};

QuasiDiscordUser.prototype.say = function(message, toWaitFor, channelID){
    if(!message){
        const err = 'Must submit a command';
        throw err;
    }
    if(!channelID)
        channelID = config.discord.test_channel_id;

    return new Promise(resolve => {
        this.waitForMessage(m => {
            if(!toWaitFor){
                resolve(m);
                return true;
            }
            // console.log(toWaitFor);
            // console.log(m.content);
            if(typeof toWaitFor === typeof m.content){
                if(typeof toWaitFor === 'string' && m.content.includes(toWaitFor)){
                    resolve(m.content);
                    return true;
                }
                if(typeof toWaitFor === 'object')
                    if(helpers.isDictEqual(m.content, toWaitFor)){
                        // console.log(JSON.stringify(m.content, null, 2));
                        // console.log(JSON.stringify(toWaitFor, null, 2));
                        resolve(m.content);
                        return true;
                    }
            }
            if(typeof toWaitFor === 'function')
                if(toWaitFor(m)){
                    resolve(m.content);
                    return true;
                }
        });

        const channel = this.discordClient.channels.get(channelID);
        channel.send(`!${message}`);
    });
};

QuasiDiscordUser.prototype.whisper = async function(message, toWaitFor, channelID){
    if(!message){
        const err = 'Must submit a command';
        throw err;
    }
    if(!channelID)
        channelID = config.discord.test_channel_id;

    return new Promise(async(resolve, reject) => {
        this.waitForMessage(m => {
            if(typeof toWaitFor === 'function')
                if(toWaitFor(m)){
                    resolve(m.content);
                    return true;
                }

            if(typeof toWaitFor !== typeof m.content)
                return false;
            if(typeof toWaitFor === 'string' && m.content.includes(toWaitFor)){
                resolve(m.content);
                return true;
            }
        });

        const app = require('../../app');
        const user = await this.discordClient.fetchUser(app.discordWrapper.client.user.id);
        const dm = await user.createDM();
        try{
            await dm.send(message);
        }catch(err){
            reject(err);
        }
    });
};

QuasiDiscordUser.prototype.chatter = function(chatMessage, channelID){
    if(!channelID)
        channelID = config.discord.test_channel_id;
    return new Promise((resolve, reject) => {
        this.mListeners.push(m => {
            if(typeof m.content === 'string' && m.content.includes(chatMessage)){
                resolve(m);
                return true;
            }
        });

        const channel = this.discordClient.channels.get(channelID);
        channel.send(chatMessage).catch(reject);
    });
};


QuasiDiscordUser.prototype.failChatter = async function(chatMessage, channelID){
    try{
        await this.chatter(chatMessage, channelID);
    }catch(err){
        if(err !== quasiDiscordChannel.SEND_CHANNEL_ERROR)
            throw err;
        return;
    }
    const err = `Successfully sent "${chatMessage}" but shouldn't have.`;
    throw err;
};

QuasiDiscordUser.prototype.subscribe = function(){
    return this.say(Constants.subscribe, 'will let you know if any games');
};

QuasiDiscordUser.prototype.host = async function(channelID){
    await this.say(Constants.host, 'A game is being started by', channelID);
    this.inGame = true;
    return this.getGameObj();
};

QuasiDiscordUser.prototype.join = async function(){
    let conditions = 2;
    await this.say(Constants.join, m => {
        if(m.content === `${this.getName()} has joined the lobby.`)
            conditions--;
        if(m.content.embed && m.content.embed.title === 'Setup Link')
            conditions--;
        return !conditions;
    });
    this.inGame = true;
};

QuasiDiscordUser.prototype.leave = function(channelID, hostLeaving){
    if(!channelID)
        channelID = config.discord.test_channel_id;

    if(hostLeaving)
        return this.say(Constants.leave, 'Game canceled.', channelID);
    return this.say(Constants.leave, `${this.getName()} has left the lobby.`, channelID);
};

QuasiDiscordUser.prototype.setPhaseStart = async function(shouldDayStart){
    const gameState = await this.getGameState();
    if(gameState.rules.DAY_START.val && !shouldDayStart)
        await this.say(Constants.nightstart, 'Game will now start at nighttime.');
    else if(!gameState.rules.DAY_START.val && shouldDayStart)
        await this.say(Constants.daystart, 'Game will now start at daytime.');
};

QuasiDiscordUser.prototype.setSetup = async function(setupID){
    const command = `${Constants.setup} ${setupID}`;
    if(setupID === 'custom')
        return this.say(command, 'Setup has been changed to Custom.');

    const setupData = await this.getSetups();
    for(let i = 0; i < setupData.length; i++)
        if(setupData[i].key === setupID)
            return this.say(command, `Setup has been changed to ${setupData[i].name}.`);

    const error = `setupID not found: ${setupID}`;
    throw error;
};

QuasiDiscordUser.prototype.getUserState = async function(){
    const discordID = this.discordClient.user.id;
    const user = await userService.getByExternalID(false, discordID, userTypes.DISCORD);
    return gameService.getUserState(user.id);
};

QuasiDiscordUser.prototype.getGameObj = async function(){
    const gameState = await this.getUserState();
    return gameService.getByJoinID(gameState.joinID);
};

QuasiDiscordUser.prototype.start = async function(){
    const gameObj = await this.getGameObj();

    const dayStart = gameObj.setup.setupModifiers.DAY_START.value;
    await new Promise(resolve => {
        this.waitForMessage(m => {
            if(m.channel.type !== quasiDiscordChannel.PRIVATE)
                return;
            if(m.content.startsWith('**Day 1**') && dayStart){
                resolve();
                return true;
            }
            if(m.content.startsWith('**Night 0**') && !dayStart){
                resolve();
                return true;
            }
        });
        this.say(Constants.start);
    });
    // this is a hack because I need to figure out a way for discord to send a message
    // AFTER everything's been done on the discordWrapper's side.
    await new Promise(resolve => {
        setTimeout(resolve, 300);
    });
};

QuasiDiscordUser.prototype.killGame = async function(){
    const gameObject = await this.getGameObj();
    if(!gameObject)
        return;

    return new Promise(async resolve => {
        this.waitForMessage(m => {
            if(m.channel.type === quasiDiscordChannel.PRIVATE)
                return;
            if(m.content === 'Game canceled.'){
                resolve();
                return true;
            }
        });
        await gameService.deleteGame(gameObject.joinID || gameObject[0].joinID);
    });
};

QuasiDiscordUser.prototype.forceEndPhase = function(){
    const o = {
        message: 'setTimer',
        time: 0,
    };
    return this.instanceMessage(o);
};

QuasiDiscordUser.prototype.getSetups = function(){
    return setupService.getFeaturedSetups();
};

QuasiDiscordUser.prototype.instanceMessage = async function(request){
    if(typeof request === 'string')
        request = {
            message: request,
        };

    const discordID = this.discordClient.user.id;
    const user = await userService.getByExternalID(false, discordID, userTypes.DISCORD);
    request.userID = user.id;
    javaRequests.sendInstanceRequest(request);
    return new Promise(resolve => {
        setTimeout(resolve, 1000);
    });
};

QuasiDiscordUser.prototype.moderate = function(){
    return this.say(`${Constants.moderate} on`, 'will be moderating this game.');
};

QuasiDiscordUser.prototype.vote = function(tBot){
    return this.say(`vote ${tBot.getName()}`, `${this.getName()} voted for ${tBot.getName()}`);
};

QuasiDiscordUser.prototype.getGameState = async function(){
    const discordID = this.discordClient.user.id;
    const user = await userService.getByExternalID(false, discordID, userTypes.DISCORD);
    return gameService.getUserState(user.id);
};

module.exports = {
    QuasiDiscordUser,
};
