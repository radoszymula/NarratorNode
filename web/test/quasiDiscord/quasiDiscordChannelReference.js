const helpers = require('../../utils/helpers'); /* eslint-disable-line no-unused-vars */


function QuasiDiscordChannelReference(channel, author){
    this.author = author;
    this.id = channel.id;
    this.channel = channel;
    this.permissionOverwrites = channel.permissionOverwrites;
    this.type = channel.type;

    if(channel.type === 'text')
        this.guild = author.guilds.get(channel.guild.id);
}

QuasiDiscordChannelReference.prototype.createInvite = function(){
    return Promise.resolve('');
};

QuasiDiscordChannelReference.prototype.overwritePermissions = function(entity, permissionObj){
    if(!entity || typeof entity !== 'string'){
        const message = ['bad request', entity];
        return Promise.reject(message);
    }

    return this.channel._overwritePermissions(entity, permissionObj);
};

QuasiDiscordChannelReference.prototype.send = async function(message){
    const channelMessage = await this.channel.send(message, this);

    /* eslint-disable-next-line no-unused-vars */
    const logMessage = typeof message === 'string'
        ? message : JSON.stringify(message).substring(0, 30);
    let color; /* eslint-disable-line no-unused-vars */
    let prefix; /* eslint-disable-line no-unused-vars */
    if(this.type === 'text'){
        color = 'green';
        prefix = '~~~';
    }else{
        color = 'blue';
        prefix = '~';
    }
    // helpers.colorLog(color, ` ${prefix} ${this.author.nickname}: ${logMessage}`);

    return channelMessage;
};

QuasiDiscordChannelReference.prototype.fetchMessage = function(messageID){
    return Promise.resolve(this.channel.messageHistory[messageID]);
};

module.exports = {
    QuasiDiscordChannelReference,
};
