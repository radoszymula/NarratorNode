async function filterColor(players, color){
    const ps = [];
    for(let i = 0; i < players.length; i++)
        ps.push(players[i].getGameState());

    const results = await Promise.all(ps);
    const ret = [];
    for(let i = 0; i < results.length; i++)
        try{
            if(results[i].roleInfo.roleColor === color)
                ret.push(players[i]);
        }catch(err){}

    return ret;
}

async function filterRole(players, roleName){
    const ps = [];
    for(let i = 0; i < players.length; i++)
        ps.push(players[i].getGameState());

    const results = await Promise.all(ps);

    const ret = [];
    for(let i = 0; i < results.length; i++)
        try{
            if(results[i].roleInfo.roleName === roleName)
                ret.push(players[i]);
        }catch(err){}


    return ret;
}

async function massEndNight(players){
    const gameObject = await players[0].getGameObj();
    const skipNightPromises = players
        .map(player => player.whisper('!end night', `**Day ${gameObject.dayNumber + 1}**`));

    return Promise.all(skipNightPromises);
}

function massVote(target, players){
    const votePromises = players
        .map(player => player.vote(target));
    return Promise.all(votePromises);
}

module.exports = {
    filterColor,
    filterRole,
    massEndNight,
    massVote,
};
