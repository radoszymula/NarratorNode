const profileService = require('../services/profileService');

const util = require('../utils/helpers');


function getFactionByID(game, factionID){
    return game.setup.factions.find(faction => faction.id === factionID);
}

function getFactionRoleWithName(game, roleName){
    const role = getRoleWithName(game, roleName);
    const factionRolesList = game.setup.factions
        .map(faction => faction.factionRoles
            .filter(factionRole => factionRole.roleID === role.id));
    return util.flattenList(factionRolesList)[0];
}

function getRoleWithName(game, roleName){
    return game.setup.roles.find(role => role.name === roleName);
}

async function getProfileWithRole(game, roleName){
    const profilePromises = game.users.map(user => profileService.get(user.id));
    const profiles = await Promise.all(profilePromises);
    return profiles.find(profile => profile.roleCard.roleName === roleName);
}

module.exports = {
    getFactionByID,
    getFactionRoleWithName,
    getRoleWithName,
    getProfileWithRole,
};
