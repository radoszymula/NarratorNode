const baseValidator = require('../../validators/baseValidator');


const setupHidden = {
    type: 'object',
    properties: {
        id: {
            type: 'number',
            required: true,
        },
    },
};

const setupHiddenAddEvent = {
    type: 'object',
    properties: {
        setupHiddenID: {
            type: 'number',
            required: true,
        },
    },
};

function check(query){
    baseValidator.validate(query, setupHidden);
}

function checkAddEvent(query){
    baseValidator.validate(query, setupHiddenAddEvent);
}

module.exports = {
    check,
    checkAddEvent,
};
