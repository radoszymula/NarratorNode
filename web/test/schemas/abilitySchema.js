const { abilityModifierDefinition } = require('./abilityModifierSchema');


const abilityDefinition = {
    id: {
        type: 'number',
        required: true,
    },
    modifiers: {
        type: 'array',
        items: {
            type: 'object',
            properties: abilityModifierDefinition,
            required: true,
        },
        required: true,
    },
};

module.exports = {
    abilityDefinition,
};
