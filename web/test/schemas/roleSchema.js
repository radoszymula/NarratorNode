const { abilityDefinition } = require('./abilitySchema');


const roleDefinition = {
    abilities: {
        type: 'array',
        items: {
            type: 'object',
            properties: abilityDefinition,
            required: true,
        },
        required: true,
    },
    id: {
        type: 'number',
        required: true,
    },
};

module.exports = {
    roleDefinition,
};
