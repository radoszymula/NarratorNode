const baseValidator = require('../../validators/baseValidator');

const { setupDefinition } = require('./setupSchema');


const gameSchema = {
    type: 'object',
    properties: {
        integrations: {
            type: 'array',
            required: true,
        },
        setup: {
            type: 'object',
            properties: setupDefinition,
            required: true,
        },
    },
};

function check(query){
    baseValidator.validate(query, gameSchema);
}

module.exports = {
    check,
};
