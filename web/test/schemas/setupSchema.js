const { roleDefinition } = require('./roleSchema');


const setupDefinition = {
    roles: {
        type: 'array',
        items: {
            type: 'object',
            properties: roleDefinition,
            required: true,
        },
        required: true,
    },
};


module.exports = {
    setupDefinition,
};
