const abilityModifierDefinition = {
    label: {
        type: 'string',
        minLength: 1,
        required: true,
    },
    name: {
        type: 'string',
        minLength: 1,
        required: true,
    },
};

module.exports = {
    abilityModifierDefinition,
};
