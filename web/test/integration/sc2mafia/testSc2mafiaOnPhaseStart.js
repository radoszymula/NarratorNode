/* eslint-disable max-len */
require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');
const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const sc2mafiaGameRepo = require('../../../channels/sc2mafia/repos/sc2mafiaGameRepo');

const quasiGame = require('../../quasiModels/quasiGame');
const {
    SC2MAFIA_GAME_THREAD_ID,
    SC2MAFIA_SETUP_THREAD_ID,
    SC2MAFIA_SIGNUP_THREAD_ID,
} = require('../../fakeConstants');


describe('Sc2mafia On Phase Start', async() => {
    let newPostStub;
    afterEach(() => {
        newPostStub.restore();
    });

    it('Should post a new message when onNightStart is hit', async() => {
        newPostStub = sinon.stub(vBulletinClient, 'newPost');
        const game = await quasiGame.create();
        await sc2mafiaGameRepo.insert(game.joinID,
            SC2MAFIA_SETUP_THREAD_ID,
            SC2MAFIA_SIGNUP_THREAD_ID);
        await sc2mafiaGameRepo.updateGameThreadID(game.joinID, SC2MAFIA_GAME_THREAD_ID);
        const expectedMessage = getExpectedMessage();

        await sc2mafiaClient.onNightStart(game);
        const message = newPostStub.getCall(0).args[0].message;

        expect(message).to.be.equal(expectedMessage);
    });
});

function getExpectedMessage(){
    return `[CENTER][COLOR=white]

[/COLOR][COLOR=white][TABLE="width: 800, class: grid, align: center"][TR][TD]Role List[/TD][td]Living Players[/TD][TD]Graveyard[/TD][/TR][TR][TD][COLOR=#6495ED]Citizen[/COLOR]
[COLOR=#6495ED]Citizen[/COLOR]
[COLOR=#6495ED]Citizen[/COLOR]
[COLOR=#6495ED]Doctor[/COLOR]
[COLOR=#6495ED]Sheriff[/COLOR]
[COLOR=#FF0000]Goon[/COLOR]
[COLOR=#FF0000]Goon[/COLOR][/TD][TD]host[/TD][TD][/TD][/TR][/TABLE][/COLOR][COLOR=white]
Day 1 ends in 240 seconds.[/COLOR]

[COUNTDOWN]0.002777777777777778:00:00:00[/COUNTDOWN][COLOR=white]

With 1 left alive, it takes 1 to vote someone out of the game.[/COLOR][/CENTER]`;
}
