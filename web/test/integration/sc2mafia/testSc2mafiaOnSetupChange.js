/* eslint-disable max-len */
require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');
const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const sc2mafiaGameRepo = require('../../../channels/sc2mafia/repos/sc2mafiaGameRepo');

const quasiGame = require('../../quasiModels/quasiGame');
const {
    SC2MAFIA_POST_ID,
    SC2MAFIA_SETUP_THREAD_ID,
    SC2MAFIA_SIGNUP_THREAD_ID,
} = require('../../fakeConstants');


describe('Sc2mafia Setup Changed', async() => {
    let getThreadStub;
    let editPostStub;
    afterEach(() => {
        getThreadStub.restore();
        editPostStub.restore();
    });

    it('Should update the setup when onLobbyClosed is hit', async() => {
        getThreadStub = sinon.stub(vBulletinClient, 'getThread');
        getThreadStub.onCall(0).returns({
            FIRSTPOSTID: SC2MAFIA_POST_ID,
        });
        editPostStub = sinon.stub(vBulletinClient, 'editPost');
        const game = await quasiGame.create();
        await sc2mafiaGameRepo.insert(game.joinID,
            SC2MAFIA_SETUP_THREAD_ID,
            SC2MAFIA_SIGNUP_THREAD_ID);
        const expectedMessage = getExpectedMessage();


        await sc2mafiaClient.onSetupChange(game);
        const message = editPostStub.getCall(0).args[0].message;

        expect(getThreadStub.calledWithExactly({
            threadid: SC2MAFIA_SETUP_THREAD_ID,
        })).to.equal(true);
        expect(editPostStub.getCall(0).args[0].postid).to.be.equal(SC2MAFIA_POST_ID);
        expect(message).to.be.equal(expectedMessage);
    });
});

function getExpectedMessage(){
    return `[COLOR=#FFD700][SIZE=5][B][U][CENTER]host's Classic Setup[/CENTER][/U][/B]
[/SIZE][/COLOR][CENTER][COLOR=#6495ED]Citizen[/COLOR]
[COLOR=#6495ED]Citizen[/COLOR]
[COLOR=#6495ED]Citizen[/COLOR]
[COLOR=#6495ED]Doctor[/COLOR]
[COLOR=#6495ED]Sheriff[/COLOR]
[COLOR=#FF0000]Goon[/COLOR]
[COLOR=#FF0000]Goon[/COLOR][/CENTER]
[COLOR=white][COLOR=white][SIZE=4]General Settings[/SIZE][/COLOR]

[LIST][*]Game will start with a Night 0.

[*]Day lengths will be 240 seconds.

[*]Night lengths will be 180 seconds  .
[/LIST][/COLOR]
[SPOILER=Town Faction]
[COLOR=white][COLOR=#6495ED][SIZE=5]Town[/SIZE]
[/COLOR]
[LIST][*]The uninformed majority, these roles must eliminate all Mafia factions, the cult, and all evil neutrals.

[*]Must eliminate [COLOR=#FF0000]Mafia[/COLOR].
[/LIST]
[SIZE=4]Available Roles[/SIZE]

[U][COLOR=#6495ED]Citizen[/COLOR][/U]
[LIST][*]A voice of reason during the day.

[*]The percentage of citizens to non citizens is about 40%.
[/LIST]
[U][COLOR=#6495ED]Doctor[/COLOR][/U]
[LIST][*]Save someone from an attack.

[*]Will know if target was successfully saved

[*]Target knows if they were saved, but not by who
[/LIST]
[U][COLOR=#6495ED]Sheriff[/COLOR][/U]
[LIST][*]Investigate someone to determine if they are suspicious or not.

[*]Can pick out the difference between one team and another

[*]Has no pregame check

[*]Sheriff can detect who Mafia are.
[/LIST][/COLOR][/SPOILER]
[SPOILER=Mafia Faction]
[COLOR=white][COLOR=#FF0000][SIZE=5]Mafia[/SIZE]
[/COLOR]
[LIST][*]The informed minority, these roles must eliminate the Town, other Mafia factions, the cult and evil killing neutrals.

[*]Must eliminate [COLOR=#6495ED]Town[/COLOR].
[/LIST]
[SIZE=4]Available Abilities[/SIZE]

[U][COLOR=#FF0000]FactionKill[/COLOR][/U]
[LIST][*]Ability to kill at night.

[*]You will not know which faction made this kill.
[/LIST]
[SIZE=4]Available Roles[/SIZE]

[U][COLOR=#FF0000]Goon[/COLOR][/U]
[LIST][*]A henchman of the mafia.
[/LIST][/COLOR][/SPOILER][SPOILER=Order of Operations]
[COLOR=white]
[SIZE=4]Order of Operations[/SIZE]

[LIST][*]Commuting

[*]Grave Diggers, in reverse order

[*]Sending other people to do actions for you

[*]Witch manipulation

[*]Commuting again, if missed the first round

[*]Vistations for sending other people to do actions for you

[*]Oracle/Snitch action

[*]Role Blocking

[*]Jailor Executions

[*]Operator/Intended target switching

[*]Coroner

[*]Bus Driving

[*]Coward hiding

[*]Vesting

[*]Undousing

[*]Bodyguard moving

[*]Dousing

[*]All kill abilities at once: Veteran alerting, Serial Killer stabbing, Disguiser killing, Mass Murderer spree killing, Joker killing, Interceptor, Electromaniac charging, Cult clubbing, Burning, Gun Shooting, Mafia kill

[*]Bodyguard attacks

[*]Jester suicides

[*]Doctors

[*]Disguiser takes persona

[*]Commuting if applicable

[*]Cleaning

[*]Silencing, Disfranchising, Blackmailing, Puppeteering, and Ghost controlling

[*]Framing

[*]Enforcing

[*]Mason Recruiting

[*]Cult visiting

[*]Cult recruiting

[*]Drug Dealing

[*]Items passed out: guns, vests, breadd

[*]Amnesiac remembers

[*]Investigations: Sheriff, Investigator, Scout, Arms Detector

[*]Poisoning

[*]Suiting

[*]Jester annoys

[*]Normal visitations

[*]Role watching: spy, lookout, detective, agent
[/LIST][/COLOR]
[/SPOILER][COLOR=#FFD700][B][U]Rules of Conduct[/U][/B]
[/COLOR]1. [URL="http://www.sc2mafia.com/forum/showthread.php/42151-Forum-Mafia-Rules"]Forum Mafia Rules[/URL]
2. Inactive players will be replaced by another player, if necessary, they will be modkilled.
3. To ask the Host a question, you must highlight the question [COLOR="#00FF00"]in green[/COLOR], and mention the host [MENTION=6527]host[/MENTION].  You may also pm me on Discord.
4. Minimum of 5 posts per game day.
5. English shall be the only language used.
6. Do not post links to other websites.
7. Pictures are allowed in moderation.
8. Videos are not allowed.
9. Directly quoting any feedback or PM by me is forbidden.
10. Have Fun.`;
}
