require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const integrationType = require('../../../models/enums/integrationTypes');

const actionService = require('../../../services/actionService');
const gameIntegrationService = require('../../../services/gameIntegrationService');

const quasiGame = require('../../quasiModels/quasiGame');

const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const sc2mafiaGameRepo = require('../../../channels/sc2mafia/repos/sc2mafiaGameRepo');
const {
    SC2MAFIA_GAME_THREAD_ID,
    SC2MAFIA_SETUP_THREAD_ID,
    SC2MAFIA_SIGNUP_THREAD_ID,
} = require('../../fakeConstants');


describe('Sc2mafia On Vote', async() => {
    let newPostStub;
    afterEach(() => {
        newPostStub.restore();
    });

    it('Should post a vote indicator after a vote', async() => {
        newPostStub = sinon.stub(vBulletinClient, 'newPost');
        const game = await quasiGame.create({
            isStarted: true,
            setupKey: 'mumash', // for the day start
            playerCount: 7,
        });
        const voteTarget = game.users.find(user => user.id !== game.host.id).name;
        gameIntegrationService.register(game.joinID, integrationType.SC2MAFIA);
        await sc2mafiaGameRepo.insert(game.joinID,
            SC2MAFIA_SETUP_THREAD_ID,
            SC2MAFIA_SIGNUP_THREAD_ID);
        await sc2mafiaGameRepo.updateGameThreadID(game.joinID, SC2MAFIA_GAME_THREAD_ID);
        const expectedMessage = getExpectedMessage(game.host.name, voteTarget);

        await actionService.submitActionMessage(game.host.id, { message: `vote ${voteTarget}` });
        await new Promise(resolve => {
            setTimeout(resolve, 200);
        });
        const message = newPostStub.getCall(0).args[0].message;

        expect(message).to.be.equal(expectedMessage);
    });
});

function getExpectedMessage(voter, voteTarget){
    return `[B]${voter}[/B] voted for [B]${voteTarget}[/B].   (L - 3)`;
}
