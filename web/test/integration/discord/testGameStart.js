require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const discordJsonClient = require('../../../channels/discord/discordJsonClient');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Game Start', async() => {
    let host;
    let deleteMessageSpy;
    beforeEach(() => {
        deleteMessageSpy = sinon.spy(discordJsonClient, 'deleteMessage');
    });

    afterEach(async() => {
        discordJsonClient.deleteMessage.restore();
        await host.killGame();
    });

    it('Should start the game', async() => {
        host = await quasiUser.createDiscordUser();
        const game = await host.host();
        await quasiGame.addPlayersToGame(game.joinID, 6);

        await host.start();
        const gameState = await host.getGameObj();
        const deletedStartMessage = deleteMessageSpy.args
            .reduce((hasStart, message) => {
                if(hasStart)
                    return true;
                if(!message[0].content.includes)
                    return false;
                return message[0].content.includes(Constants.start);
            }, false);

        expect(gameState.isStarted).to.equal(true);
        expect(gameState.setup.setupModifiers.NIGHT_LENGTH.value).to.equal(60);
        expect(deletedStartMessage).to.equal(false);
    });

    it('Should announce the moderator if there is one', async() => {
        host = await quasiUser.createDiscordUser();
        const game = await host.host();
        await quasiGame.addPlayersToGame(game.joinID, 6);
        await Promise.all([host.setSetup('ffa'), host.moderate()]);

        await host.say('start', m => m.content === `${host.getMention()}'s game has begun!`);
    });

    it('Should announce game errors on start', async() => {
        host = await quasiUser.createDiscordUser();
        await host.host();
        await host.setSetup('ffa');

        await host.say('start', m => m.content.includes('You need at THE VERY least '
            + '3 players to start a mafia game.'));
    });
});
