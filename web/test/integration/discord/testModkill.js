require('../../init');

const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Modkill', async() => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        await host.host();
    });

    afterEach(async() => {
        if(host)
            await host.killGame();
    });

    it('Should be able to modkill players', async() => {
        host.say(`${Constants.moderate} on`, 'will be moderating this game.');
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await host.setSetup('millercop7'); // DAY START
        await host.say(Constants.start, '**Day 1**');

        await host.say(`modkill ${players[0].getName()}`, ' has/have inexplicably died.');

        // todo assertions
    });
});
