require('../../init');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');


describe('Chat access', async() => {
    let host;
    afterEach(async() => {
        if(host)
            await host.killGame();
        host = null;
    });

    it('Should allow everyone to speak if the game hasn\'t started', async() => {
        host = await quasiUser.createDiscordUser();
        const user2 = await quasiUser.createDiscordUser();

        await host.host();

        await host.chatter('I, as the host can speak.');
        await user2.chatter('I, as user2, can speak.');
    });

    it('Should stop people from speaking on night game start', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);

        await host.setPhaseStart(false); // NIGHT START
        await host.start();

        const chatterPromises = players
            .map(player => player.failChatter(`${player.getName()} can't speak.`));
        return Promise.all(chatterPromises);
    });

    it('Should allow people to speak again on day start', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.setPhaseStart(false); // NIGHT START
        await host.start();

        await quasiDiscordUtils.massEndNight(players);

        const chatterPromises = players
            .map(player => player.chatter(`${player.getName()} can speak.`));
        return Promise.all(chatterPromises);
    });

    it('Should allow everyone to speak if the game finished', async() => {
        host = await quasiUser.createDiscordUser();
        const user2 = await quasiUser.createDiscordUser();
        const game = await host.host();
        await host.setPhaseStart(false);// NIGHT START
        await quasiGame.addPlayersToGame(game.joinID, 5);
        await user2.join();
        await host.start();

        await host.killGame();
        const user1 = host;
        host = null;

        await user1.chatter('I, as user1, can speak after the game stopped.');
        await user2.chatter('I, as user2, can also speak after the game stopped.');
    });

    it('Should stop out of game players from speaking', async() => {
        host = await quasiUser.createDiscordUser();
        const user2 = await quasiUser.createDiscordUser();
        const user3 = await quasiUser.createDiscordUser();
        const game = await host.host();
        await Promise.all([user2.join(), quasiGame.addPlayersToGame(game.joinID, 5)]);
        await host.start();

        await user3.failChatter('I shouldn\'t interrupt the games of other players.');
        // assertion is user not being able to talk
    });

    it('Should stop night killed people from talking the next day', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.setPhaseStart(false); // NIGHT START
        await host.start();

        const goon = (await quasiDiscordUtils.filterRole(players, 'Goon'))[0];
        const sheriff = (await quasiDiscordUtils.filterRole(players, 'Sheriff'))[0];
        await goon.whisper(`!kill ${sheriff.getName()}`, `You will kill ${sheriff.getName()}`);
        await quasiDiscordUtils.massEndNight(players);

        await sheriff.failChatter('I shouldn\'t be able to talk as a dead person.');
    });

    it('Should stop voted out people from talking the next day', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.setSetup('millercop7');
        await host.start();

        const mafiaMembers = await quasiDiscordUtils.filterColor(players, '#FF0000');
        const townMembers = await quasiDiscordUtils.filterRole(players, 'Sheriff');
        await quasiDiscordUtils.massVote(mafiaMembers[0], townMembers.slice(0, 3));
        await mafiaMembers[1].say(`vote ${mafiaMembers[0].getName()}`, 'It is now nighttime.');
        await quasiDiscordUtils.massEndNight(players.filter(player => player !== mafiaMembers[0]));

        await mafiaMembers[0].failChatter('I shouldn\'t be able to talk as a dead person.');
    });

    it('Should stop daykilled people from talking that day', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.setSetup('mediumh');
        await host.start();

        await quasiDiscordUtils.massEndNight(players);
        const assassin = (await quasiDiscordUtils.filterRole(players, 'Assassin'))[0];
        const target = (await quasiDiscordUtils.filterRole(players, 'Citizen'))[0];
        await assassin.whisper(`assassinate ${target.getName()}`,
            `${assassin.getName()} assassinated ${target.getName()}`);

        await target.failChatter('I shouldn\'t be able to talk as a dead daykilled person.');
    });
});
