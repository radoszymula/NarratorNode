require('../../init');
const { expect } = require('chai');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');
const quasiUser = require('../../quasiModels/quasiUser');


describe('Message manipulation', async() => {
    let host;

    afterEach(async() => {
        if(host)
            await host.killGame();
        host = null;
    });

    it('Should warn people when they edit a message for the first time', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();

        await quasiDiscordUtils.massEndNight(players);
        const chatterMessage = await host.chatter('I will edit this message.');
        const textToWaitFor = `<@!${host.getID()}>, stop editing your commands.  `
            + '**If you edit or delete your messages again, I will modkill you.**';
        await new Promise(resolve => {
            host.waitForMessage(m => {
                if(typeof m.content !== 'string')
                    return;
                return m.content === textToWaitFor;
            }).then(resolve);
            chatterMessage.edit('This was changed!');
        });

        // assertion is the received warning
    });

    it('Should modkill people who edit their message twice', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();
        await quasiDiscordUtils.massEndNight(players);
        const chatterMessage = await host.chatter('I will edit this message.');
        const textToWaitFor = `<@!${host.getID()}>, stop editing your commands.  `
            + '**If you edit or delete your messages again, I will modkill you.**';
        await new Promise(resolve => {
            host.waitForMessage(m => {
                if(typeof m.content !== 'string')
                    return;
                return m.content === textToWaitFor;
            }).then(resolve);
            chatterMessage.edit('This was changed!');
        });

        await new Promise(resolve => {
            host.waitForMessage(m => {
                if(typeof m.content !== 'string')
                    return;
                if(m.content.includes(host.getName()))
                    return m.content.includes(' has/have inexplicably died.');
            }).then(resolve);
            chatterMessage.edit('This was changed!');
        });
        const gameObject = await host.getGameObj();

        expect(host.getName()).to.equal(gameObject.graveyard[0].name);
    });
});
