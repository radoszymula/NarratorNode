require('../../init');
const { expect } = require('chai');

const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Setup Manipulation', async() => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        await host.host();
    });

    afterEach(async() => {
        await host.killGame();
    });

    it('Should start games on "Classic" mode', async() => {
        // Given happens in before Each

        const gameState = await host.getGameState();

        expect(gameState.setupName).to.equal('classic');
    });

    it('Should edit preset setups', async() => {
        // Given happens in before Each

        await host.setSetup('vanillab');
        const gameState = await host.getGameState();

        expect(gameState.setupName).to.equal('vanillab');
    });

    it('Should edit day length', async() => {
        // Given happens in before Each

        await host.say(`${Constants.daylength} 8`, 'Day length changed to 8 minutes.');
        const gameState = await host.getGameState();

        expect(gameState.rules.DAY_LENGTH.val).to.equal(8 * 60);
    });

    it('Should edit night length', async() => {
        // Given happens in before Each

        await host.say(`${Constants.nightlength} 12`, 'Night length changed to 12 minutes.');
        const gameState = await host.getGameState();

        expect(gameState.rules.NIGHT_LENGTH.val).to.equal(12 * 60);
    });

    it('Should set the game start to day', async() => {
        // Given happens in before Each

        await host.setSetup('custom');
        await host.say(Constants.daystart, 'Game will now start at daytime.');
        const gameState = await host.getGameState();

        expect(gameState.rules.DAY_START.val).to.equal(true);
    });

    it('Should set the game start to day with an alternative command', async() => {
        // Given happens in before Each

        await host.setSetup('custom');
        await host.say('day start', 'Game will now start at daytime.');
        const gameState = await host.getGameState();

        expect(gameState.rules.DAY_START.val).to.equal(true);
    });

    it('Should set the game start to night', async() => {
        // Given happens in before Each

        await host.setSetup('custom');
        await host.say(Constants.nightstart, 'Game will now start at nighttime.');
        const gameState = await host.getGameState();

        expect(gameState.rules.DAY_START.val).to.equal(false);
    });

    it('Should set the game start to night with an alternative command', async() => {
        // Given happens in before Each

        await host.setSetup('custom');
        await host.say('night start', 'Game will now start at nighttime.');
        const gameState = await host.getGameState();

        expect(gameState.rules.DAY_START.val).to.equal(false);
    });

    it('Should notify the user when the skip vote option" has been turned off', async() => {
        // Given happens in before Each

        await host.setSetup('custom');
        await host.say(`${Constants.skip} off`, 'Players must vote someone out to end the day.');
        const gameState = await host.getGameObj();

        expect(gameState.setup.setupModifiers.SKIP_VOTE.value).to.equal(false);
    });

    it('Should notify the user when the "skip vote option" has been turned on', async() => {
        const awaitMessage = 'Players may skip the day, avoiding a public execution.';

        await host.setSetup('custom');
        await host.say(`${Constants.skip} on`, awaitMessage);
        const gameState = await host.getGameObj();

        expect(gameState.setup.setupModifiers.SKIP_VOTE.value).to.equal(true);
    });

    it('Should notify the players when moderating has been turned on', async() => {
        // Given happens in before Each

        await host.say(`${Constants.moderate} on`, 'will be moderating this game.');
        await host.say(`${Constants.moderate} off`, 'will no longer be moderating this game.');

        // assertions are the text that comes out
    });

    it('Should not allow non hosts to use this command', async() => {
        const user2 = await quasiUser.createDiscordUser();
        await user2.join();

        await user2.say(`${Constants.moderate} on`, 'Only hosts can use this command.');

        // assertions are the text that comes out
    });
});
