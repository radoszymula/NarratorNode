require('../../init');
const { expect } = require('chai');

const config = require('../../../../config');

const gameService = require('../../../services/gameService');

const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');

const testChannel2 = config.discord.test_channel_id2;


describe('User adding', async() => {
    it('Should start on a basic host of a game', async() => {
        const host = await quasiUser.createDiscordUser();
        const host2 = await quasiUser.createDiscordUser();
        const instanceCount = await getInstanceCount();
        await host.host();
        let newInstanceCount = await getInstanceCount();
        expect(newInstanceCount).to.equal(instanceCount + 1);

        await host.say(Constants.host, `<@!${host.getID()}>, You are already hosting a game!`);
        await host2.say(Constants.host, `<@!${host2.getID()}>, `
            + `${host.getName()} is already hosting a game.`);

        newInstanceCount = await getInstanceCount();
        expect(newInstanceCount).to.equal(instanceCount + 1);

        await host.leave(null, true); // host leaving
        newInstanceCount = await getInstanceCount();
        expect(newInstanceCount).to.equal(instanceCount);
    });

    it('Should pull the nickname for joining and hosting', async() => {
        const host = quasiUser.createDiscordUser();
        const user = quasiUser.createDiscordUser();

        await host.host();
        await user.join();
        const infoMessage = await user.say(Constants.info);
        const gameObject = await host.getGameObj();
        const hostNickname = host.discordClient.nickname;
        const hostUsername = host.discordClient.username;
        const playerNickname = user.discordClient.nickname;
        const playerUsername = user.discordClient.username;

        expect(infoMessage.content.embed.fields[1].value).to.include(hostNickname);
        expect(infoMessage.content.embed.fields[1].value).to.not.include(hostUsername);
        expect(infoMessage.content.embed.fields[1].value).to.include(playerNickname);
        expect(infoMessage.content.embed.fields[1].value).to.not.include(playerUsername);
        expect(gameObject.players.map(player => player.name)).to.include(hostNickname);
        expect(gameObject.players.map(player => player.name)).to.not.include(hostUsername);
        expect(gameObject.players.map(player => player.name)).to.include(playerNickname);
        expect(gameObject.players.map(player => player.name)).to.not.include(playerUsername);

        await user.leave();
        await host.leave(null, true); // host leaving
    });

    it('Should allow users to rehost', async() => {
        const host = quasiUser.createDiscordUser();

        await host.host();
        await host.leave(null, true);
        await host.host();
        await host.leave(null, true);
    });

    it('Should allow users to join and leave games', async() => {
        const hostUser = await quasiUser.createDiscordUser();
        const user2 = await quasiUser.createDiscordUser();

        await hostUser.host();
        await user2.join();
        await user2.leave();
        await user2.join();
        await user2.leave();
        await hostUser.leave(null, true);
    });

    it('Should not allow users to join two games', async() => {
        const user1 = await quasiUser.createDiscordUser();
        const user2 = await quasiUser.createDiscordUser();

        await Promise.all([user1.host(), user2.host(testChannel2)]);

        await user2.say(Constants.join, `<@!${user2.getID()}>, You're already in a game!`);

        await Promise.all([user1.leave(null, true), user2.leave(testChannel2, true)]);
    });

    it('Should allow new games to form after games complete', async() => {
        const host = await quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(3);
        await Promise.all(players.map(player => player.join()));
        await Promise.all([host.setSetup('ffa'), host.moderate()]);
        await host.setPhaseStart(true); // DAY START

        await host.say(Constants.start, '**Day 1**');

        await host.say('end phase', 'Voting polls are now open');

        let votes = [players[0].vote(players[2]), players[1].vote(players[2])];
        await Promise.all(votes);

        await host.say('end phase', ' now give their defense');
        await host.say('end phase', 'Voting polls are now open');

        votes = [players[0].vote(players[2]), players[1].vote(players[2])];
        await Promise.all(votes);

        await host.say('end phase', 'It is now nighttime.');
        await host.say('end phase', 'Game canceled');

        await host.host();
        await host.leave(null, true); // host leaving
    });
});

async function getInstanceCount(){
    return (await gameService.getAll()).length;
}
