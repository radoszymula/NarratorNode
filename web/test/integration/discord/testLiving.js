require('../../init');
const { expect } = require('chai');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');
const quasiUser = require('../../quasiModels/quasiUser');


describe('Living', () => {
    let host;
    afterEach(async() => {
        if(host)
            await host.killGame();
        host = null;
    });

    it('Should show living', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();

        await quasiDiscordUtils.massEndNight(players);
        const message = await host.say('living', m => m.content);

        expect(message).to.eql(expectedLivingMessage());
    });
});

function expectedLivingMessage(){
    return {
        embed: {
            fields: [
                {
                    inline: false,
                    name: '**The Living**',
                    // eslint-disable-next-line max-len
                    value: '**[1]** Player1\n**[2]** Player2\n**[3]** Player3\n**[4]** Player4\n**[5]** Player5\n**[6]** Player6\n**[7]** Player7\n',
                },
            ],
            footer: {
                text: 'Hosted by The Narrator',
            },
        },
    };
}
