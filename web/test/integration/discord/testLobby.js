require('../../init');
const { expect } = require('chai');

const integrationTypes = require('../../../models/enums/integrationTypes');

const quasiUser = require('../../quasiModels/quasiUser');


describe('Lobby', () => {
    let host;
    afterEach(async() => {
        if(host)
            await host.killGame();
        host = null;
    });


    it('Should register discord game integrations', async() => {
        host = quasiUser.createDiscordUser();

        const gameObj = await host.host();

        expect(gameObj.integrations).to.include(integrationTypes.DISCORD);
    });
});
