require('../../init');

const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Setup Command', async() => {
    it('Will show a list of setups', async() => {
        const user = await quasiUser.createDiscordUser();

        await user.say(Constants.setup, m => m.content.includes('*classic*'));
    });
});
