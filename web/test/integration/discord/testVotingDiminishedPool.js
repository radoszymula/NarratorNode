require('../../init');
const { expect } = require('chai');

const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Voting for Diminshing Pool System', async() => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        await host.host();
    });

    afterEach(async() => {
        if(host)
            await host.killGame();
    });

    it('Should not allow players to vote during discussion', async() => {
        await Promise.all([host.setSetup('ffa'), host.moderate()]);
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await host.setPhaseStart(true); // DAY START
        await host.say(Constants.start, '**Day 1**');

        await players[0].say(`vote ${players[1].getName()}`, 'Unknown command');

        const reply = await players[0].say(Constants.commands, 'Bot commands');
        expect(reply).to.not.include('vote for a person');

        await host.say('end phase', 'Voting polls are now open');

        await players[0].say('end phase', 'Unknown command');
    });

    it('Should follow basic diminished pool voting rules', async() => {
        await Promise.all([host.setSetup('ffa'), host.moderate()]);
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));

        await host.say(Constants.start, '**Day 1**');

        await host.say('end phase', 'Voting polls are now open');

        let votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        await host.say('end phase', ' will give their defense first');
        await host.say('end phase', ' is now on trial for crimes against humanity');
        await host.say('end phase', 'Voting polls are now open');

        votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        let waitText = 'There is no vote consensus.  Votes have been reset.  '
            + 'You have 2 more chances before everyone dies.';
        await host.say('end phase', waitText);

        votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        waitText = 'There is no vote consensus.  Votes have been reset.  '
            + 'This is your last chance before everyone dies.';
        await host.say('end phase', waitText);
    });

    it('Should announce the vote count and the trialed voter order', async() => {
        await Promise.all([host.setSetup('ffa'), host.moderate()]);
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));

        await host.say(Constants.start, '**Day 1**');

        await host.say('end phase', 'Voting polls are now open');

        const votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        const expectedTrialOrder1 = `The voting round has concluded.\n${players[1].getMention()} `
            + `will give their defense first, followed by ${players[4].getMention()}.`;
        const expectedTrialOrder2 = `The voting round has concluded.\n${players[4].getMention()} `
            + `will give their defense first, followed by ${players[1].getMention()}.`;
        let voteCountFound = false;
        await host.say('end phase', m => {
            if(m.content.embed && m.content.embed.description === '**Vote Counts**')
                voteCountFound = true;
            if(!voteCountFound)
                return;
            if(m.content === expectedTrialOrder1 || m.content === expectedTrialOrder2)
                return true;
        });
    });

    it('Should announce votes when a tie had been reached', async() => {
        await Promise.all([host.setSetup('ffa'), host.moderate()]);
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));

        await host.say(Constants.start, '**Day 1**');

        await host.say('end phase', 'Voting polls are now open');

        let votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        await host.say('end phase', ' will give their defense first');
        await host.say('end phase', ' is now on trial for crimes against humanity');
        await host.say('end phase', 'Voting polls are now open');

        votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        await host.say('end phase', m => {
            if(typeof m.content !== 'object')
                return false;
            if(m.content.embed.description !== '**Vote Counts**')
                return false;
            const fields = m.content.embed.fields;
            for(let i = 0; i < fields.length; i++){
                const field = fields[i];
                if(field.name.includes('Not Voting'))
                    continue;
                expect(field.value).to.not.be.equal('*No votes*');
            }
            return true;
        });
    });

    it('Should announce the vote count at the end of the day', async() => {
        await Promise.all([host.setSetup('ffa'), host.moderate()]);
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));

        await host.say(Constants.start, '**Day 1**');

        await host.say('end phase', 'Voting polls are now open');

        let votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
        ];
        await Promise.all(votes);

        await host.say('end phase',
            m => m.content.embed && m.content.embed.description === '**Vote Counts**');

        await host.say('end phase',
            m => m.content.includes && m.content.includes('Voting polls are now open'));

        votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
        ];
        await Promise.all(votes);

        await host.say('end phase', m => {
            if(!m.content.embed)
                return false;
            return m.content.embed.description === '**Vote Counts**';
        });
    });
});
