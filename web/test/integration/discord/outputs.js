function infoPregame(joinID){
    return {
        embed: {
            title: `#${joinID} Status`,
            description: 'Waiting for game to start',
            fields: [{
                name: 'Roles List',
                value: ':blue_car: Citizen\n:blue_car: Citizen\n:blue_car: Citizen\n:blue_car: '
                    + 'Doctor\n:blue_car: Sheriff\n:red_circle: Goon\n:red_circle: Goon',
                inline: true,
            }, {
                name: 'Player Lobby',
                value: '\n*7 more to start*',
                inline: true,
            }, {
                name: 'Setup',
                value: 'Classic',
                inline: true,
            }],
            footer: {
                text: 'Hosted by The Narrator',
            },
        },
    };
}

// not ready yet
function voteOverview(votes, players){
    // `**[${getPlayerIndex(player)}] ${player.getName()}`,
    const fields = players.map(() => '');
    return {
        embed: {
            description: '**Vote Counts**',
            fields,
            footer: {
                text: 'Hosted by The Narrator',
            },
        },
    };
}

module.exports = {
    infoPregame,
    voteOverview,
};
