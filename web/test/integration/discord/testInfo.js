require('../../init');
const { expect } = require('chai');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');
const quasiUser = require('../../quasiModels/quasiUser');


describe('Info', () => {
    let host;
    afterEach(async() => {
        if(host)
            await host.killGame();
        host = null;
    });

    it('Should show info', async() => {
        host = quasiUser.createDiscordUser();
        const gameObj = await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();

        await quasiDiscordUtils.massEndNight(players);
        const message = await host.say('info', m => m.content);

        expect(message).to.eql(expectedInfoMessage(gameObj.joinID));
    });
});

function expectedInfoMessage(joinID){
    return {
        embed: {
            description: 'Game in progress',
            fields: [
                {
                    inline: true,
                    name: 'Roles List',
                    // eslint-disable-next-line max-len
                    value: ':blue_car: Citizen\n:blue_car: Citizen\n:blue_car: Citizen\n:blue_car: Doctor\n:blue_car: Sheriff\n:red_circle: Goon\n:red_circle: Goon',
                },
                {
                    inline: true,
                    name: 'Player List',
                    // eslint-disable-next-line max-len
                    value: '**[1]** Player1\n**[2]** Player2\n**[3]** Player3\n**[4]** Player4\n**[5]** Player5\n**[6]** Player6\n**[7]** Player7\n',
                },
            ],
            footer: {
                text: 'Hosted by The Narrator',
            },
            title: `#${joinID} Status`,
        },
    };
}
