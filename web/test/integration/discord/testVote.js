require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const discordJsonClient = require('../../../channels/discord/discordJsonClient');

const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Voting', async() => {
    let host;
    let spy;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        spy = sinon.spy(discordJsonClient, 'deleteMessage');
        await host.host();
    });

    afterEach(async() => {
        discordJsonClient.deleteMessage.restore();
        if(host)
            await host.killGame();
    });

    it('Should notify users when a double vote has occurred', async() => {
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        await host.setSetup('millercop7'); // DAY START
        await host.say(Constants.start, 'Day 1');

        await host.vote(players[0]);
        await host.say(`vote ${players[0].getName()}`, 'You are already voting for this person.');
    });

    it('Should allow skip votes', async() => {
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        await host.setSetup('millercop7'); // DAY START
        await host.start();

        await host.say('skip', `${host.getName()} voted to skip the day.`);
    });

    it('Should announce votes on vote phase starts', async() => {
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await Promise.all([host.setSetup('ffa'), host.moderate()]);

        await host.say(Constants.start, '**Day 1**');

        await host.say('end phase', m => {
            if(typeof m.content === 'string')
                return false;
            return m.content.embed.description === '**Vote Counts**';
        });
    });

    it('Should tell people how to vote every day', async() => {
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await Promise.all([host.setSetup('ffa'), host.moderate()]);

        await host.say(Constants.start, '**Day 1**');

        await host.say('end phase', m => {
            if(typeof m.content !== 'string')
                return false;
            return m.content.includes(`For example, !vote ${host.getMention()}`);
        });

        let votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
        ];
        await Promise.all(votes);

        const deletedVoteMessages = spy.args
            .reduce((hasStart, message) => {
                if(hasStart)
                    return true;
                if(!message[0].content.includes)
                    return false;
                return message[0].content.includes('vote');
            }, false);
        expect(deletedVoteMessages).to.equal(true);


        await host.say('end phase', 'will now give their defense');
        await host.say('end phase', m => {
            if(typeof m.content !== 'string')
                return false;
            return m.content.includes(`For example, !vote ${host.getMention()}`);
        });

        votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
        ];
        await Promise.all(votes);

        await host.say('end phase', 'It is now nighttime');

        await host.say('end phase', '**Day 2**');
        await host.say('end phase', m => {
            if(typeof m.content !== 'string')
                return false;
            return m.content.includes(`For example, !vote ${host.getMention()}`);
        });
    });
});
