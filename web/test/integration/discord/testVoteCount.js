require('../../init');

const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Vote count outputs', async() => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        await host.host();
    });

    afterEach(async() => {
        if(host)
            await host.killGame();
    });

    it('Should output all votes for a moderator', async() => {
        await Promise.all([host.setSetup('ffa'), host.moderate()]);
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await host.setPhaseStart(true); // DAY START
        await host.say(Constants.start, '**Day 1**');
        await host.say('end phase', 'Voting polls are now open');
        const votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        await host.say('vc', m => {
            if(typeof m.content !== 'object')
                return;
            const columnsWithVotes = m.content.embed.fields
                .filter(field => {
                    for(let i = 0; i < players.length; i++)
                        if(field.value.includes(players[i].getName()))
                            return true;
                    return false;
                });
            return columnsWithVotes.length;
        });
    });

    it('Should output all votes for a moderator when whisper', async() => {
        await Promise.all([host.setSetup('ffa'), host.moderate()]);
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await host.setPhaseStart(true); // DAY START
        await host.say(Constants.start, '**Day 1**');
        await host.say('end phase', 'Voting polls are now open');
        const votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        await host.whisper('!vc', m => {
            if(typeof m.content !== 'object')
                return;
            const columnsWithVotes = m.content.embed.fields
                .filter(field => {
                    for(let i = 0; i < players.length; i++)
                        if(field.value.includes(players[i].getName()))
                            return true;
                    return false;
                });
            return columnsWithVotes.length;
        });
    });
});
