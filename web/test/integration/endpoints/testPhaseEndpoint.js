require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const phaseService = require('../../../services/phaseService');
const setupModifierService = require('../../../services/setupModifierService');
const userPermissionService = require('../../../services/userPermissionService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');

const { JOIN_ID } = require('../../fakeConstants');


describe('Phase Endpoint', () => {
    let clock;

    afterEach(() => {
        clock.restore();
    });

    it('Should alter the remaining time left on the phase', async() => {
        clock = sinon.useFakeTimers();
        const gameObj = await quasiGame.create({ playerCount: 7 });
        await userPermissionService.setAdmin(gameObj.host.id);
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        await setDayStart(gameObj, 100);
        await gameService.start(gameObj.host.id);
        const args = {
            seconds: 50,
            joinID: gameObj.joinID,
        };

        const responseObj = await requests.put('phases', args, headers);
        const remainingTime = phaseService.getEndTime(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(remainingTime).to.be.equal(new Date().getTime() + (args.seconds * 1000));
    });

    it('Should force end the phase', async() => {
        clock = sinon.useFakeTimers();
        let gameObj = await quasiGame.create({ playerCount: 8 });
        await userPermissionService.setAdmin(gameObj.host.id);
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        await setDayStart(gameObj, 100);
        gameObj = await gameService.start(gameObj.host.id);
        const args = {
            seconds: 0,
            joinID: gameObj.joinID,
        };
        const phase = gameObj.phase.name;

        const responseObj = await requests.put('phases', args, headers);
        const newPhase = (await gameService.getByJoinID(gameObj.joinID)).phase.name;

        expect(responseObj.statusCode).to.be.equal(200);
        expect(phase).to.not.be.be.equal(newPhase);
    });

    it('Should lengthen the phase', async() => {
        clock = sinon.useFakeTimers();
        let gameObj = await quasiGame.create({ playerCount: 8 });
        await userPermissionService.setAdmin(gameObj.host.id);
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        await setDayStart(gameObj, 100);
        gameObj = await gameService.start(gameObj.host.id);
        const args = {
            seconds: 200,
            joinID: gameObj.joinID,
        };
        const phase = gameObj.phase;

        const responseObj = await requests.put('phases', args, headers);
        advanceSeconds(101);
        const remainingTime = phaseService.getEndTime(gameObj.joinID);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(remainingTime).to.be.equal(new Date().getTime() + (args.seconds * 1000) - 101000);
        expect(phase.name).to.be.equal(gameObj.phase.name);
    });

    it('Should gracefully fail when seconds isn\'t specified', async() => {
        const gameObj = await quasiGame.create({ playerCount: 8 });
        await userPermissionService.setAdmin(gameObj.host.id);
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const args = {
            seconds: 200,
        };
        await setDayStart(gameObj, 100);
        await gameService.start(gameObj.host.id);

        const responseObj = await requests.put('phases', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Should gracefully fail when joinID isn\'t specified', async() => {
        const gameObj = await quasiGame.create({ playerCount: 8 });
        await userPermissionService.setAdmin(gameObj.host.id);
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const args = {
            joinID: gameObj.joinID,
        };
        await setDayStart(gameObj, 100);
        await gameService.start(gameObj.host.id);

        const responseObj = await requests.put('phases', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Should not allow edits unless is admin', async() => {
        const gameObj = await quasiGame.create({ playerCount: 8 });
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const args = {
            seconds: 200,
            joinID: gameObj.joinID,
        };

        const responseObj = await requests.put('phases', args, headers);

        expect(responseObj.statusCode).to.be.equal(401);
    });

    it('Should not allow edits if not in game', async() => {
        const user = await quasiUser.createUser();
        await userPermissionService.setAdmin(user.id);
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            seconds: 200,
            joinID: JOIN_ID,
        };

        const responseObj = await requests.put('phases', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    function advanceSeconds(minutes){
        clock.tick(minutes * 1000);
    }
});

function setDayStart(gameObj, seconds){
    return setupModifierService.edit(gameObj.host.id, {
        id: 'DAY_LENGTH',
        value: seconds,
    });
}
