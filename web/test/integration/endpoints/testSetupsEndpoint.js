require('../../init');
const { expect } = require('chai');

const roleRepo = require('../../../repos/roleRepo');
const setupRepo = require('../../../repos/setupRepo');

const gameService = require('../../../services/gameService');

const requests = require('../../requests');

const authHelpers = require('../../authHelpers');
const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');


describe('Setups', async() => {
    it('Will get setups with no user id specified', async() => {
        // NO USERS

        const responseObj = await requests.get('setups');

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.length).to.not.be.equal(0);
    });

    it('Will get setups with authenticated request', async() => {
        const user = await quasiUser.createUser();
        const userSetup = await quasiSetup.create(user.id);
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.get('setups', null, headers);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.length).to.not.be.equal(0);
        expect(responseObj.json.response
            .filter(setup => setup.id === userSetup.id).length)
            .to.be.equal(1);
    });

    it('Will correctly give role ability modifier labels', async() => {
        const user = await quasiUser.createUser();
        const setup = await quasiSetup.create(user.id);
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.get(`setups/${setup.id}`, null, headers);
        const roles = responseObj.json.response.roles;

        const architect = roles.find(role => role.name === 'Architect');
        const abilityModifiers = architect.abilities[0].modifiers;
        const backToBackModifier = abilityModifiers
            .find(modifier => modifier.name === 'BACK_TO_BACK');

        expect(backToBackModifier.label).to.be.equal(
            'May target the same people on consecutive days using "architect".',
        );
    });

    it('Will not return inactive setups', async() => {
        const user = await quasiUser.createUser();
        const userSetup = await quasiSetup.create(user.id, {
            isActive: false,
        });
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.get('setups', null, headers);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.length).to.not.be.equal(0);
        expect(responseObj.json.response
            .filter(setup => setup.id === userSetup.id).length)
            .to.be.equal(0);
    });

    it('Will create setup shells', async() => {
        const user = await quasiUser.createUser();
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.post('setups', null, headers);
        const setupID = responseObj.json.response.id;
        const setup = await setupRepo.getByID(setupID);
        const allSetups = await setupRepo.getAll();

        expect(responseObj.statusCode).to.be.equal(200);
        expect(setup.id).to.be.equal(setupID);
        expect(allSetups.length).to.be.equal(1);
    });

    it('Will change to a user setup', async() => {
        const gameObj = await quasiGame.create({ setupKey: 'mumash' });
        const userSetup = await quasiSetup.create(gameObj.host.id);
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const args = {
            setupID: userSetup.id,
        };

        const responseObj = await requests.put('setups', args, headers);
        const game = await gameService.getByJoinID(gameObj.joinID);
        const setups = await setupRepo.getAll();

        expect(responseObj.statusCode).to.be.equal(200);
        expect(game.setup.setupHiddens.length).to.be.equal(0);
        expect(game.setup.factions.length).to.not.be.equal(0);
        expect(game.setup.id).to.be.equal(userSetup.id);
        expect(setups.length).to.be.equal(1);
    });

    it('Will change back and forth from a preset setup to a custom one', async() => {
        const gameObj = await quasiGame.create({ setupKey: 'mumash' });
        const userSetup = await quasiSetup.create(gameObj.host.id);
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const setupID = userSetup.id;
        const setupKey = 'classic';

        let responseObj = await requests.put('setups', { setupID }, headers);
        expect(responseObj.statusCode).to.be.equal(200);
        responseObj = await requests.put('setups', { setupKey }, headers);
        expect(responseObj.statusCode).to.be.equal(200);
        const setup = await setupRepo.getByID(setupID);
        expect(setup).to.not.be.a('null');
        responseObj = await requests.put('setups', { setupID }, headers);
        expect(responseObj.statusCode).to.be.equal(200);
        responseObj = await requests.put('setups', { setupKey }, headers);
        expect(responseObj.statusCode).to.be.equal(200);
    });

    it('Will gracefully fail when the setup id is not a number', async() => {
        const gameObj = await quasiGame.create({ setupKey: 'mumash' });
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const args = { setupID: 'notNumber' };

        const responseObj = await requests.put('setups', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will delete user setups', async() => {
        const user = await quasiUser.createUser();
        const userSetup = await quasiSetup.create(user.id);
        const roleID = userSetup.roles[0].id;
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.delete(`setups/${userSetup.id}`, null, headers);
        const [setup, role] = await Promise.all([
            setupRepo.getByID(userSetup.id),
            roleRepo.getByID(roleID),
        ]);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(setup).to.be.a('null');
        expect(role).to.be.a('null');
    });

    it('Will not delete setups if the non-mod user doesn\'t own the setup', async() => {
        const setupOwner = await quasiUser.createUser();
        const hacker = await quasiUser.createUser();
        const userSetup = await quasiSetup.create(setupOwner.id);
        const headers = authHelpers.fakeTempAuthHeader(hacker.id);

        const responseObj = await requests.delete(`setups/${userSetup.id}`, null, headers);
        const setup = await setupRepo.getByID(userSetup.id);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(setup).to.not.be.a('null');
    });

    it('Will delete setups even if mod doesn\'t own the setup', async() => {
        const setupOwner = await quasiUser.createUser();
        const modUser = await quasiUser.createMod();
        const userSetup = await quasiSetup.create(setupOwner.id);
        const headers = authHelpers.fakeTempAuthHeader(modUser.id);

        const responseObj = await requests.delete(`setups/${userSetup.id}`, null, headers);
        const setup = await setupRepo.getByID(userSetup.id);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(setup).to.be.a('null');
    });

    it('Will not delete setups with replays attached to it', async() => {
        const user = await quasiUser.createUser();
        const userSetup = await quasiSetup.create(user.id);
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.delete(`setups/${userSetup.id}`, null, headers);
        const setup = await setupRepo.getByID(userSetup.id);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(setup).to.be.a('null');
    });

    it('Will not create new setups on startup', async() => {
        await quasiGame.create({
            playerCount: 4,
            setupKey: 'mumash',
            isStarted: true,
        });

        const setups = await setupRepo.getAll();

        expect(setups.length).to.be.equal(1);
    });
});
