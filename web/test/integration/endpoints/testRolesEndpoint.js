require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const setupService = require('../../../services/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');


describe('Roles endpoint', async() => {
    it('Will give specific role information', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const role = gameObject.setup.roles[0];
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.get(`roles/${role.id}`, null, headers);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.name).to.be.equal(role.name);
    });

    it('Will fail gracefully when the roleID doesn\'t exist', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const role = gameObject.setup.roles[0];
        const roleID = role.id * 10;
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.get(`roles/${roleID}`, null, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully when the roleID is not a number', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const roleID = 'ABCD';
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.get(`roles/${roleID}`, null, headers);

        expect(responseObj.statusCode).to.be.equal(404);
    });
});
