require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const moderatorService = require('../../../services/moderatorService');
const setupService = require('../../../services/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');
const quasiWebSocket = require('../../quasiWebSocket');

const { JOIN_ID } = require('../../fakeConstants');


describe('Moderators', async() => {
    let ws;
    afterEach(() => {
        if(ws)
            ws.close();
    });

    it('Removes a player from the game, but maintains the hostID', async() => {
        let gameObj = await quasiGame.create();
        const hostID = gameObj.host.id;
        const headers = authHelpers.fakeTempAuthHeader(hostID);

        const responseObj = await requests.post('moderators', null, headers);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(gameObj.players.length).to.be.equal(0);
        expect(gameObj.host.id).to.be.equal(hostID);
    });

    it('Will not allow a moderator to rejoin an already filled game', async() => {
        let gameObj = await quasiGame.create();
        const hostID = gameObj.host.id;
        await moderatorService.create(hostID);
        await quasiGame.addPlayersToGame(gameObj.joinID, 5);
        await setupService.setSetup({ setupKey: 'fivep', joinID: gameObj.joinID });
        const headers = authHelpers.fakeTempAuthHeader(hostID);

        const responseObj = await requests.delete('moderators', null, headers);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(422);
        expect(responseObj.json.errors.length).to.be.equal(1);
        expect(gameObj.players.length).to.be.equal(5);
    });

    it('Will not allow a moderator to rejoin an already filled game', async() => {
        let gameObj = await quasiGame.create({ setupKey: 'ffa' });
        const hostID = gameObj.host.id;
        await moderatorService.create(hostID);
        await quasiGame.addPlayersToGame(gameObj.joinID, 5);
        const headers = authHelpers.fakeTempAuthHeader(hostID);

        const responseObj = await requests.delete('moderators', null, headers);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(gameObj.setup.setupHiddens.length).to.be.equal(6);
        expect(gameObj.players.length).to.be.equal(6);
    });

    it('Will repick to another random player', async() => {
        const gameObj = await quasiGame.create({ playerCount: 2 });
        const otherUserID = gameObj.users.find(user => user.id !== gameObj.host.id).id;
        const args = {
            repickTarget: '',
            gameID: gameObj.id,
        };
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);

        const responseObj = await requests.post('moderators/repick', args, headers);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.id).to.be.equal(otherUserID);
    });

    it('Will repick to another specified host', async() => {
        const gameObj = await quasiGame.create({ playerCount: 9 });
        const otherUser = gameObj.users.find(user => user.id !== gameObj.host.id);
        const otherUserHeaders = authHelpers.fakeTempAuthHeader(otherUser.id);
        const { messages } = await addWebSocket(otherUserHeaders);
        const args = {
            repickTarget: otherUser.name,
            gameID: gameObj.id,
        };
        const hostHeaders = authHelpers.fakeTempAuthHeader(gameObj.host.id);

        const responseObj = await requests.post('moderators/repick', args, hostHeaders);
        await new Promise(resolve => {
            setTimeout(resolve, 1000);
        });
        const hostChangeMessage = messages.find(m => m.event === 'hostChange');

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.id).to.be.equal(otherUser.id);
        expect(hostChangeMessage.host.id).to.be.equal(otherUser.id);
    });

    it('Will gracefully fail on a string gameID', async() => {
        const user = await quasiUser.createUser();
        const args = {
            repickTarget: user.name,
            gameID: JOIN_ID,
        };
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.post('moderators/repick', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response;
    }
});
