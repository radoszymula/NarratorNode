require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const userType = require('../../../models/enums/userTypes');

const userIntegrationRepo = require('../../../repos/userIntegrationsRepo');

const gameService = require('../../../services/gameService');
const voteService = require('../../../services/voteService');

const requests = require('../../requests');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');

const quasiGame = require('../../quasiModels/quasiGame');

const {
    PLAYER_NAME,
    SC2MAFIA_GAME_THREAD_ID,
    SC2MAFIA_SIGNUP_THREAD_ID,
} = require('../../fakeConstants');


describe('Channel - Sc2mafia Endpoint', () => {
    let onNewPostStub;
    let sendNarratorLinkStub;

    afterEach(() => {
        onNewPostStub.restore();
        if(sendNarratorLinkStub)
            sendNarratorLinkStub.restore();
    });

    it('Will add new players to a game', async() => {
        let game = await quasiGame.create();
        onNewPostStub = sinon.stub(sc2mafiaClient, 'onNewPost');
        sendNarratorLinkStub = sinon.stub(sc2mafiaClient, 'sendNarratorLink');
        onNewPostStub.onCall(0).returns({
            playerJoins: [{
                externalID: PLAYER_NAME,
                joinID: game.joinID,
                name: PLAYER_NAME,
            }],
            playerVotes: [],
        });
        const args = {
            threadID: SC2MAFIA_SIGNUP_THREAD_ID,
            waitForCompletion: true,
        };

        const responseObj = await requests.post('channels/sc2mafia/thread', args);
        game = await gameService.getByJoinID(game.joinID);
        const players = game.players;

        expect(responseObj.statusCode).to.be.equal(204);
        expect(players.length).to.be.equal(2);
        expect(players.filter(player => player.name === PLAYER_NAME).length)
            .to.be.equal(1);
        expect(sendNarratorLinkStub.getCall(0).args[0]).to.be.equal(PLAYER_NAME);
    });

    it('Will detect votes', async() => {
        const game = await quasiGame.create({
            isStarted: true,
            playerCount: 7,
            setupKey: 'mumash', // for the day start
        });
        await userIntegrationRepo.add(game.host.id, PLAYER_NAME, userType.SC2MAFIA);
        const voteTarget = game.users.find(user => user.id !== game.host.id).name;
        onNewPostStub = sinon.stub(sc2mafiaClient, 'onNewPost');
        onNewPostStub.onCall(0).returns({
            playerJoins: [],
            playerVotes: [{
                externalID: PLAYER_NAME,
                joinID: game.joinID,
                voteTarget,
            }],
        });
        const args = {
            threadID: SC2MAFIA_GAME_THREAD_ID,
            waitForCompletion: true,
        };

        const responseObj = await requests.post('channels/sc2mafia/thread', args);
        const votes = await voteService.getVotes(game.joinID);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(votes.currentVotes[game.host.name]).to.be.equal(voteTarget);
    });
});
