require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const eventService = require('../../../services/eventService');
const gameService = require('../../../services/gameService');
const setupHiddenService = require('../../../services/setupHiddenService');
const setupService = require('../../../services/setupService');

const setupHiddenSchema = require('../../schemas/setupHiddenSchema');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');
const quasiWebSocket = require('../../quasiWebSocket');


describe('Setup Hidden Endpoint', async() => {
    let eventSetupChangeSpy;
    let setupGetByIDSpy;
    let ws;
    afterEach(() => {
        setupGetByIDSpy.restore();
        eventSetupChangeSpy.restore();
        if(ws)
            ws.close();
    });

    it('Will add a setup hidden', async() => {
        const user = await quasiUser.createUser();
        const setup = await quasiSetup.create(user.id);
        let gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const hiddenID = gameObject.setup.hiddens[0].id;
        const messages = await addWebSocket(headers);
        eventSetupChangeSpy = sinon.spy(eventService, 'setupChange');
        setupGetByIDSpy = sinon.spy(setupService, 'getByID');

        const responseObj = await requests.post('setupHiddens', { hiddenID }, headers);
        gameObject = await gameService.getByJoinID(gameObject.joinID);
        await setupGetByIDSpy.returnValues[0];
        await eventSetupChangeSpy.returnValues[eventSetupChangeSpy.returnValues.length - 1];
        await new Promise(resolve => {
            setTimeout(resolve, 1000);
        });

        expect(responseObj.statusCode).to.be.equal(200);
        expect(gameObject.setup.setupHiddens.length).to.be.equal(1);
        setupHiddenSchema.check(responseObj.json.response);
        const setupHiddenAddEventObj = messages
            .find(message => message.event === 'setupHiddenAdd' && message.hiddenID === hiddenID);
        setupHiddenSchema.checkAddEvent(setupHiddenAddEventObj);
    });

    it('Will not add a setup hidden if the hidden doesn\'t belong to the user', async() => {
        const user1 = await quasiUser.createUser();
        const user2 = await quasiUser.createUser();
        const setup = await quasiSetup.create(user1.id);
        let gameObject = await quasiGame.create({ setupID: setup.id, hostID: user1.id });
        const headers = authHelpers.fakeTempAuthHeader(user2.id);
        const hiddenID = gameObject.setup.hiddens[0].id;

        const responseObj = await requests.post('setupHiddens', { hiddenID }, headers);
        gameObject = await gameService.getByJoinID(gameObject.joinID);

        expect(responseObj.statusCode).to.be.equal(422);
        expect(gameObject.setup.setupHiddens.length).to.be.equal(0);
    });

    it('Will not add a setup hidden if the request doesn\'t have a hiddenID', async() => {
        const user = await quasiUser.createUser();
        const setup = await quasiSetup.create(user.id);
        let gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.post('setupHiddens', {}, headers);
        gameObject = await gameService.getByJoinID(gameObject.joinID);

        expect(responseObj.statusCode).to.be.equal(422);
        expect(gameObject.setup.setupHiddens.length).to.be.equal(0);
    });

    it('Will delete a setup hidden', async() => {
        const user = await quasiUser.createUser();
        const setup = await quasiSetup.create(user.id);
        let gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const messages = await addWebSocket(headers);
        eventSetupChangeSpy = sinon.spy(eventService, 'setupChange');
        setupGetByIDSpy = sinon.spy(setupService, 'getByID');
        const setupHidden = await setupHiddenService.add(user.id, gameObject.setup.hiddens[0].id);

        const responseObj = await requests.delete(`setupHiddens/${setupHidden.id}`,
            null, headers);
        gameObject = await gameService.getByJoinID(gameObject.joinID);
        await setupGetByIDSpy.returnValues[0];
        await eventSetupChangeSpy.returnValues[eventSetupChangeSpy.returnValues.length - 1];
        await new Promise(resolve => {
            setTimeout(resolve, 1000);
        });

        expect(responseObj.statusCode).to.be.equal(204);
        expect(gameObject.setup.setupHiddens.length).to.be.equal(0);
        const hasSetupHiddenRemoveEvent = messages
            .some(message => message.event === 'setupHiddenRemove'
                && message.setupHiddenID === setupHidden.id);
        expect(hasSetupHiddenRemoveEvent).to.be.equal(true);
    });

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }
});
