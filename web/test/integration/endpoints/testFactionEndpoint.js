require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const setupService = require('../../../services/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');

describe('Faction Endpoint', async() => {
    it('Will delete a faction', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        let gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const factionLength = gameObject.setup.factions.length;
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const factionID = gameObject.setup.factions[0].id;

        const responseObj = await requests.delete(`factions/${factionID}`, null, headers);
        gameObject = await gameService.getByJoinID(gameObject.joinID);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(gameObject.setup.factions.length).to.be.equal(factionLength - 1);
    });
});
