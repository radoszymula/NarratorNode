require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameIntegrationService = require('../../../services/gameIntegrationService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');
const quasiWebSocket = require('../../quasiWebSocket');


describe('Game Start', () => {
    let ws;
    afterEach(() => {
        if(ws)
            ws.close();
    });

    it('Sends a profile to users in game', async() => {
        const gameObj = await quasiGame.create({ playerCount: 7 });
        const ogFactionIDs = gameObj.setup.factions.map(faction => faction.id);
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const messages = await addWebSocket(headers);

        const responseObj = await requests.post(`games/${gameObj.id}/start`, null, headers);
        const game = responseObj.json.response;
        await new Promise(resolve => {
            setTimeout(resolve, 1000);
        });

        expect(responseObj.statusCode).to.be.equal(200);
        expect(gameIntegrationService.getIntegrations(game.joinID).length).to.not.equal(0);
        expect(game.setup.factions.map(faction => faction.id)).to.have.members(ogFactionIDs);
        const hasProfileUpdate = messages.some(message => message.profile
            && message.profile.gameID === gameObj.id
            && message.profile.roleCard);
        expect(hasProfileUpdate).to.be.equal(true);
    });

    it('Will stop game starts gracefully', async() => {
        const user = await quasiUser.createUser();
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const setup = await quasiSetup.create(user.id);
        const gameObj = await quasiGame.create({ hostID: user.id, setupID: setup.id });

        const responseObj = await requests.post(`games/${gameObj.id}/start`, null, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }
});
