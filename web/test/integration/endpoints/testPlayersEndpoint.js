require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const replayRepo = require('../../../repos/replayRepo');

const actionService = require('../../../services/actionService');
const gameService = require('../../../services/gameService');
const moderatorService = require('../../../services/moderatorService');

const gameSchema = require('../../schemas/gameSchema');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');


describe('Players', async() => {
    it('Creates a player in an open game', async() => {
        const gameObj = await quasiGame.create();
        const headers = authHelpers.fakeFirebaseHeader();
        const playerName = 'newPlayerName';

        const responseObj = await requests.post('players', {
            joinID: gameObj.joinID,
            playerName,
        }, headers);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.joinID).to.be.equal(gameObj.joinID);
        expect(responseObj.json.response.players.map(player => player.name)).to.include(playerName);
        gameSchema.check(responseObj.json.response);
    });

    it('Does not keep the game id in the database', async() => {
        const gameObj = await quasiGame.create();
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);

        await requests.delete('players', null, headers);

        const game = await replayRepo.getByID(gameObj.id);
        expect(game).to.be.a('null');
    });

    it('Will keep the game in the database if everyone leaves and game is done', async() => {
        const gameObj = await quasiGame.create({
            playerCount: 4,
            setupKey: 'mumash',
            isStarted: true,
        });
        const townUserIDs = gameObj.players
            .filter(player => player.factionRole.team.name !== 'Mafia')
            .map(player => player.userID);
        const mafiaName = gameObj.players
            .filter(player => !townUserIDs.includes(player.userID))[0].name;
        const voteSubmissions = townUserIDs
            .map(townUserID => {
                const actionMessage = { message: `vote ${mafiaName}` };
                return actionService.submitActionMessage(townUserID, actionMessage);
            });
        await Promise.all(voteSubmissions);

        const leavePromises = gameObj.players
            .map(player => {
                const headers = authHelpers.fakeTempAuthHeader(player.userID);
                return requests.delete('players', null, headers);
            });
        await Promise.all(leavePromises);

        const game = await replayRepo.getByID(gameObj.id);
        expect(game).to.not.be.a('null');
    });

    it('Closes a game if the host leaves, is a moderator, and is the only one in game', async() => {
        let gameObj = await quasiGame.create();
        await moderatorService.create(gameObj.host.id);
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);

        const responseObj = await requests.delete('players', null, headers);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(responseObj.data).to.equal('');
        expect(gameObj).to.be.a('null');
    });

    it('Will reassign the host if a moderator leaves', async() => {
        let gameObj = await quasiGame.create({ playerCount: 2 });
        await moderatorService.create(gameObj.host.id);
        const newHostID = gameObj.users.find(user => user.id !== gameObj.host.id).id;
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);

        const responseObj = await requests.delete('players', null, headers);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(gameObj.host.id).to.be.equal(newHostID);
    });

    it('Doesn\'t allow you to join a game twice', async() => {
        const playerCount = 2;
        const gameObj = await quasiGame.create({ playerCount });
        const duplicateUserID = gameObj.users.map(user => user.id)
            .filter(userID => userID !== gameObj.host.id)[0];
        const headers = authHelpers.fakeTempAuthHeader(duplicateUserID);

        const responseObj = await requests.post('players', {
            joinID: gameObj.joinID,
        }, headers);
        const game = await gameService.getByJoinID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(422);
        expect(responseObj.json.errors.length).to.be.equal(1);
        expect(game.players.length).to.be.equal(playerCount);
    });

    it('Doesn\'t create a player if the joinID is too long', async() => {
        await quasiUser.createUser();
        const headers = authHelpers.fakeFirebaseHeader();
        const playerName = 'newPlayerName';
        const joinID = 'ABCDE';

        const responseObj = await requests.post('players', {
            joinID,
            playerName,
        }, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not change the host if someone joins', async() => {
        let gameObj = await quasiGame.create();
        const hostID = gameObj.host.id;
        await moderatorService.create(hostID);
        const user = await quasiUser.createUser();
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.post('players', {
            joinID: gameObj.joinID,
            playerName: 'someplayer',
        }, headers);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.errors.length).to.be.equal(0);
        expect(gameObj.host.id).to.be.equal(hostID);
    });

    it('Will kick a player', async() => {
        let gameObj = await quasiGame.create({ playerCount: 2 });
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        let kickedPlayer = gameObj.players.find(player => player.id !== gameObj.host.id);

        const responseObj = await requests.delete(
            `players/kick?playerID=${kickedPlayer.userID}`, null, headers,
        );
        gameObj = await gameService.getByID(gameObj.id);
        kickedPlayer = gameObj.players.find(player => player.id === kickedPlayer.userID);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(gameObj.players.length).to.be.equal(1);
        expect(responseObj.json.response.host).to.be.an('object');
        expect(responseObj.json.response.setup).to.be.an('object');
        expect(kickedPlayer).to.be.an('undefined');
    });

    it('Will gracefully fail when there\'s no playerName', async() => {
        const gameObj = await quasiGame.create({ playerCount: 2 });
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);

        const responseObj = await requests.delete('players/kick', null, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when there are arguments specified', async() => {
        const gameObj = await quasiGame.create({ playerCount: 2 });
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const kickedPlayer = gameObj.players.find(player => player.id !== gameObj.host.id);
        const args = {};

        const responseObj = await requests.delete(
            `players/kick?playerID=${kickedPlayer.id}`, args, headers,
        );

        expect(responseObj.statusCode).to.not.be.equal(500);
        expect(responseObj.statusCode).to.be.greaterThan(299);
    });

    it('Will add a bot', async() => {
        const gameObj = await quasiGame.create();
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const args = { botCount: 1 };

        const responseObj = await requests.post('players/bots', args, headers);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.players).to.be.an('array');
        expect(responseObj.json.response.setup).to.be.an('object');
    });
});
