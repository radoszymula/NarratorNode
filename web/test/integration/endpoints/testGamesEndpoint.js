require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const replayRepo = require('../../../repos/replayRepo');
const setupRepo = require('../../../repos/setupRepo');

const actionService = require('../../../services/actionService');
const gameService = require('../../../services/gameService');
const moderatorService = require('../../../services/moderatorService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const gameSchema = require('../../schemas/gameSchema');


describe('Games', () => {
    let clock;

    afterEach(() => {
        if(clock)
            clock.restore();
    });

    it('Creates a game when setup is specified', async() => {
        await quasiUser.createUser();
        const headers = authHelpers.fakeFirebaseHeader();
        const hostName = 'testHost';
        const setupName = 'fivep';

        const request = { name: hostName, setupName };
        const responseObj = await requests.post('games', request, headers);
        const game = await replayRepo.getByID(responseObj.json.response.id);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(!!responseObj.json.response.id).to.be.equal(true);
        expect(responseObj.json.response.setup.name).to.be.equal('5 Player Mafia');
        expect(responseObj.json.response.players[0].name).to.be.equal(hostName);
        expect(!!responseObj.json.response.setup.factions[0].id).to.be.equal(true);
        gameSchema.check(responseObj.json.response);
        expect(game).to.not.be.a('null');
    });

    it('Creates a custom game when no setup is specified', async() => {
        await quasiUser.createUser();
        const headers = authHelpers.fakeFirebaseHeader();
        const hostName = 'testHost';

        const responseObj = await requests.post('games', { name: hostName }, headers);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.setup.name).to.be.equal('Custom');
        gameSchema.check(responseObj.json.response);
    });

    it('Does not create a setup when a setup is specified', async() => {
        const user = await quasiUser.createUser();
        const userSetup = await quasiSetup.create(user.id);
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const allSetups = await setupRepo.getAll();
        const args = {
            setupID: userSetup.id,
        };

        const responseObj = await requests.post('games', args, headers);
        const allSetups2 = await setupRepo.getAll();

        expect(responseObj.statusCode).to.be.equal(200);
        expect(allSetups.length).to.be.equal(allSetups2.length);
    });

    it('Creates a game when no host name is specified', async() => {
        await quasiUser.createUser();
        const headers = authHelpers.fakeFirebaseHeader();

        const responseObj = await requests.post('games', null, headers);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.players[0]).to.not.be.a('null');
    });

    it('Does not create a game when no header is specified', async() => {
        await quasiUser.createUser();
        const hostName = 'testHost';

        const responseObj = await requests.post('games', { name: hostName });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Does not error when no json is specified', async() => {
        await quasiUser.createUser();
        const headers = authHelpers.fakeFirebaseHeader();

        const responseObj = await requests.post('games', 'Not a json string', headers);

        expect(responseObj.statusCode).to.be.equal(400);
    });

    it('Deletes a game from memory', async() => {
        let gameObj = await quasiGame.create();
        const modUser = await quasiUser.createMod();
        const modUserHeader = authHelpers.fakeTempAuthHeader(modUser.id);

        const responseObj = await requests.delete(`games/${gameObj.joinID}`, null, modUserHeader);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(responseObj.data).to.equal('');
        expect(gameObj).to.be.a('null');
    });

    it('Protects games from being deleted without \'Game editing privileges\'.', async() => {
        let gameObj = await quasiGame.create();
        const user = await quasiUser.createUser();
        const userHeader = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.delete(`games/${gameObj.joinID}`, null, userHeader);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(403);
        expect(gameObj).to.not.be.a('null');
    });

    it('Gets game data for moderators in a lobby with no players', async() => {
        const gameObj = await quasiGame.create({ setupKey: 'ffa' });
        const hostID = gameObj.host.id;
        const userHeader = authHelpers.fakeTempAuthHeader(hostID);
        await moderatorService.create(hostID);

        const responseObj = await requests.get(`games?user_id=${hostID}`, null, userHeader);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(Object.keys(responseObj.json.response).length).to.not.be.equal(0);
    });

    it('Filters ended night data if ended night is before 2/3s', async() => {
        clock = sinon.useFakeTimers();
        const gameObj = await quasiGame.create({
            isStarted: true,
            playerCount: 7,
            setupModifiers: [{
                name: 'NIGHT_LENGTH',
                value: 60,
            }],
        });
        const nonHostID = gameObj.users.find(user => user.id !== gameObj.host.id).id;
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        await actionService.submitActionMessage(nonHostID, { message: 'end night' });

        advanceSeconds(39);
        const serverResponse = await requests.get(`games/${gameObj.id}`, null, headers);

        expect(serverResponse.statusCode).to.equal(200);
        expect(serverResponse.json.response.players.some(player => player.endedNight))
            .to.equal(false);
    });

    it('Does not filters ended night data if ended night is after 2/3s', async() => {
        clock = sinon.useFakeTimers();
        const gameObj = await quasiGame.create({
            isStarted: true,
            playerCount: 7,
            setupModifiers: [{
                name: 'NIGHT_LENGTH',
                value: 60,
            }],
        });
        const nonHostID = gameObj.users.find(user => user.id !== gameObj.host.id).id;
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        await actionService.submitActionMessage(nonHostID, { message: 'end night' });

        advanceSeconds(41);
        const serverResponse = await requests.get(`games/${gameObj.id}`, null, headers);

        expect(serverResponse.statusCode).to.equal(200);
        expect(serverResponse.json.response.players.some(player => player.endedNight))
            .to.equal(true);
    });

    function advanceSeconds(minutes){
        clock.tick(minutes * 1000);
    }
});
