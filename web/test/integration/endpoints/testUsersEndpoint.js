require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');


describe('Users', async() => {
    it('Gets a user\'s ID given a firebase auth header', async() => {
        // NO GIVEN

        const responseObj = await requests.get('users/id', null, authHelpers.fakeFirebaseHeader());

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.id).to.be.a('number');
    });

    it('Will fail gracefully when the authToken no longer exists', async() => {
        const authToken = new Array(128 + 1).join('A');

        const responseObj = await requests.get(`users/?auth_token=${authToken}`);

        expect(responseObj.statusCode).to.be.equal(422);
    });
});
