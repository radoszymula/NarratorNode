require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const setupService = require('../../../services/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');

describe('Faction Role Modifier Endpoint', async() => {
    it('Will update the faction role\'s modifier (boolean)', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        let gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments(gameObject,
            'boolean');
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            name: prevModifier.name,
            value: !prevModifier.value,
        };

        const responseObj = await requests.put(`factionRoles/${factionRole.id}/modifiers`,
            args, headers);
        gameObject = await gameService.getByJoinID(gameObject.joinID);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = getNewModifier(gameObject, prevModifier);
        expect(newModifier.value).to.be.equal(args.value);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will update the faction role\'s modifier (integer)', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        let gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments(gameObject,
            'number');
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            name: prevModifier.name,
            value: prevModifier.value + 1,
        };

        const responseObj = await requests.put(`factionRoles/${factionRole.id}/modifiers`,
            args, headers);
        gameObject = await gameService.getByJoinID(gameObject.joinID);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = getNewModifier(gameObject, prevModifier);
        expect(newModifier.value).to.be.equal(args.value);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will not allow updates if the user doesn\'t own the setup', async() => {
        const user = await quasiUser.createUser();
        const user2 = await quasiUser.createUser();
        const setup = await setupService.create(user2.id);
        let gameObject = await quasiGame.create({ setupID: setup.id, hostID: user2.id });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments(gameObject,
            'boolean');
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            name: prevModifier.name,
            value: !prevModifier.value,
        };

        const responseObj = await requests.put(`factionRoles/${factionRole.id}/modifiers`,
            args, headers);
        gameObject = await gameService.getByJoinID(gameObject.joinID);

        expect(responseObj.statusCode).to.be.equal(422);
        const newModifier = getNewModifier(gameObject, prevModifier);
        expect(newModifier.value).to.be.equal(prevModifier.value);
    });

    it('Will gracefully fail when the factionRoleID doesn\'t exist', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments(gameObject,
            'number');
        const factionRoleID = factionRole.id * 10;
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            name: prevModifier.name,
            value: prevModifier.value + 1,
        };

        const responseObj = await requests.put(`factionRoles/${factionRoleID}/modifiers`,
            args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the modifier name is wrong', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments(gameObject,
            'boolean');
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            name: prevModifier.name + prevModifier.name,
            value: !prevModifier.value,
        };

        const responseObj = await requests.put(`factionRoles/${factionRole.id}/modifiers`,
            args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the value', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments(gameObject,
            'boolean');
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = { name: prevModifier.name };

        const responseObj = await requests.put(`factionRoles/${factionRole.id}/modifiers`,
            args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the name', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments(gameObject,
            'boolean');
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = { value: !prevModifier.value };

        const responseObj = await requests.put(`factionRoles/${factionRole.id}/modifiers`,
            args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be boolean', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments(gameObject,
            'boolean');
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            name: prevModifier.name,
            value: 300,
        };

        const responseObj = await requests.put(`factionRoles/${factionRole.id}/modifiers`,
            args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be an integer', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments(gameObject,
            'number');
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            name: prevModifier.name,
            value: true,
        };

        const responseObj = await requests.put(`factionRoles/${factionRole.id}/modifiers`,
            args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });
});

function getFactionRoleModifierArguments(gameObject, typeString){
    let returnValue;
    gameObject.setup.factions.forEach(faction => {
        faction.factionRoles.forEach(factionRole => {
            factionRole.modifiers.forEach(modifier => {
                if(typeof(modifier.value) === typeString) // eslint-disable-line valid-typeof
                    returnValue = {
                        factionRole,
                        prevModifier: modifier,
                    };
            });
        });
    });
    return returnValue;
}

function getNewModifier(gameObject, prevModifier){
    let returnValue;
    gameObject.setup.factions.forEach(faction => {
        faction.factionRoles.forEach(factionRole => {
            factionRole.modifiers.forEach(modifier => {
                if(modifier.name === prevModifier.name)
                    returnValue = modifier;
            });
        });
    });
    return returnValue;
}
