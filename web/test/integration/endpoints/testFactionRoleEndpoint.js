require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const setupService = require('../../../services/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');


describe('Faction Roles endpoint', async() => {
    it('Will give specific faction role information', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const factionRole = gameObject.setup.factions[0].factionRoles[0];
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.get(`factionRoles/${factionRole.id}`, null, headers);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.name).to.be.equal(factionRole.name);
    });

    it('Will fail gracefully when the factionRoleID doesn\'t exist', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const factionRole = gameObject.setup.factions[0].factionRoles[0];
        const factionRoleID = factionRole.id * 10;
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.get(`factionRoles/${factionRoleID}`, null, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully when the roleID is not a number', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const factionRoleID = 'ABCD';
        const headers = authHelpers.fakeTempAuthHeader(user.id);

        const responseObj = await requests.get(`factionRoles/${factionRoleID}`, null, headers);

        expect(responseObj.statusCode).to.be.equal(404);
    });
});
