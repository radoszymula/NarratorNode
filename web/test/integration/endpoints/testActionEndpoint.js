require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');
const testHelpers = require('../../testHelpers');

const actionService = require('../../../services/actionService');
const gameService = require('../../../services/gameService');
const hiddenService = require('../../../services/hiddenService');
const setupHiddenService = require('../../../services/setupHiddenService');
const voteService = require('../../../services/voteService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');
const quasiWebSocket = require('../../quasiWebSocket');


describe('Action Controller', () => {
    let clock;
    let ws;

    afterEach(() => {
        if(clock)
            clock.restore();
        if(ws)
            ws.close();
    });

    it('Will submit a skip vote', async() => {
        const gameObj = await quasiGame.create({
            isStarted: true,
            playerCount: 5,
            setupKey: 'fivep', // for the day start
        });
        const message = 'skip day';
        const headers = authHelpers.fakeTempAuthHeader(gameObj.host.id);

        const serverResponse = await requests.put('actions', { message }, headers);
        const votes = await voteService.getVotes(gameObj.joinID);

        expect(serverResponse.statusCode).to.equal(200);
        expect(serverResponse.json.response.isSkipping).to.equal(true); // this is probably unneeded
        expect(serverResponse.json.response.voteInfo.currentVotes[gameObj.host.name])
            .to.equal('Skip Day');
        expect(votes.currentVotes[gameObj.host.name]).to.equal('Skip Day');
    });

    it('Will submit a kill action', async() => {
        const gameObj = await quasiGame.create({
            isStarted: true,
            playerCount: 7,
        });
        const goonProfile = await testHelpers.getProfileWithRole(gameObj, 'Goon');
        const citProfile = await testHelpers.getProfileWithRole(gameObj, 'Citizen');
        const headers = authHelpers.fakeTempAuthHeader(goonProfile.userID);
        const args = {
            command: 'kill',
            targets: [citProfile.name],
        };

        const serverResponse = await requests.put('actions', args, headers);

        expect(serverResponse.statusCode).to.equal(200);
    });

    it('Will submit a frame action', async() => {
        const [gameObj, factionName] = await createGameWithFramer();
        const framerProfile = await testHelpers.getProfileWithRole(gameObj, 'Framer');
        const playerName = gameObj.players
            .find(player => player.userID !== framerProfile.userID).name;
        const headers = authHelpers.fakeTempAuthHeader(framerProfile.userID);
        const args = {
            command: 'frame',
            option: factionName,
            targets: [playerName],
        };

        const serverResponse = await requests.put('actions', args, headers);

        expect(serverResponse.statusCode).to.equal(200);
    });

    it('Will gracefully fail when no team is provided', async() => {
        const [gameObj] = await createGameWithFramer();
        const framerProfile = await testHelpers.getProfileWithRole(gameObj, 'Framer');
        const headers = authHelpers.fakeTempAuthHeader(framerProfile.userID);
        const args = {
            command: 'frame',
            targets: [framerProfile.name],
        };

        const serverResponse = await requests.put('actions', args, headers);

        expect(serverResponse.statusCode).to.equal(422);
        expect(serverResponse.json.errors[0]).to.equal('Unknown team: null');
    });

    it('Will cancel an action', async() => {
        const gameObj = await quasiGame.create({
            isStarted: true,
            playerCount: 7,
        });
        const goonProfile = await testHelpers.getProfileWithRole(gameObj, 'Goon');
        const citProfile = await testHelpers.getProfileWithRole(gameObj, 'Citizen');
        await actionService.submitActionMessage(goonProfile.userID, {
            command: 'kill',
            targets: [citProfile.name],
        });
        const headers = authHelpers.fakeTempAuthHeader(goonProfile.userID);

        const serverResponse = await requests.delete('actions?actionIndex=0&command=kill', null,
            headers);
        const userState = await gameService.getUserState(goonProfile.userID);

        expect(serverResponse.statusCode).to.equal(204);
        expect(userState.actions.actionList.length).to.equal(0);
    });

    it('Will relay a motion to end the night if within 2/3rds time limit', async() => {
        clock = sinon.useFakeTimers();
        const gameObj = await quasiGame.create({
            isStarted: true,
            playerCount: 7,
            setupModifiers: [{
                name: 'DAY_LENGTH',
                value: 10000,
            }, {
                name: 'NIGHT_LENGTH',
                value: 60,
            }],
        });
        const nonHostID = gameObj.users.find(user => user.id !== gameObj.host.id).id;
        const hostHeaders = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const nonHostHeaders = authHelpers.fakeTempAuthHeader(nonHostID);
        const messages = await addWebSocket(nonHostHeaders);

        advanceSeconds(41);
        clock.restore();
        const serverResponse = await requests.put('actions', { message: 'end night' }, hostHeaders);
        await new Promise(resolve => {
            setTimeout(resolve, 100);
        });

        expect(serverResponse.statusCode).to.equal(200);
        const hasEndNightUpdate = messages.some(message => message.event === 'phaseEndBid'
            && message.players.some(player => player.endedNight
                && player.userID === gameObj.host.id));
        expect(hasEndNightUpdate).to.be.equal(true);
    });

    it('Will not relay a motion to end the night if within 2/3rds time limit', async() => {
        const gameObj = await quasiGame.create({
            isStarted: true,
            playerCount: 7,
            setupModifiers: [{
                name: 'NIGHT_LENGTH',
                value: 60,
            }],
        });
        const nonHostID = gameObj.users.find(user => user.id !== gameObj.host.id).id;
        const hostHeaders = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const nonHostHeaders = authHelpers.fakeTempAuthHeader(nonHostID);
        const messages = await addWebSocket(nonHostHeaders);

        const serverResponse = await requests.put('actions', { message: 'end night' }, hostHeaders);
        await new Promise(resolve => {
            setTimeout(resolve, 1000);
        });

        expect(serverResponse.statusCode).to.equal(200);
        const hasEndNightUpdate = messages.some(message => message.event === 'phaseEndBid'
            && message.players.some(player => player.endedNight
                && player.userID === gameObj.host.id));
        expect(hasEndNightUpdate).to.be.equal(false);
    });

    it('Will push ended night players at 2/3rds mark', async() => {
        clock = sinon.useFakeTimers();
        const gameObj = await quasiGame.create({
            playerCount: 7,
            setupModifiers: [{
                name: 'DAY_LENGTH',
                value: 10000,
            }, {
                name: 'NIGHT_LENGTH',
                value: 2,
            }],
        });
        const nonHostID = gameObj.users.find(user => user.id !== gameObj.host.id).id;
        const hostHeaders = authHelpers.fakeTempAuthHeader(gameObj.host.id);
        const nonHostHeaders = authHelpers.fakeTempAuthHeader(nonHostID);
        const messages = await addWebSocket(nonHostHeaders);
        await gameService.start(gameObj.host.id);
        const args = { message: 'end night' };

        const serverResponse = await requests.put('actions', args, hostHeaders);
        advanceSeconds(4 / 3);
        clock.restore();
        await new Promise(resolve => {
            setTimeout(resolve, 100);
        });

        expect(serverResponse.statusCode).to.equal(200);
        const endNightEvent = messages.find(message => message.event === 'phaseEndBid');
        expect(!!endNightEvent).to.be.equal(true);
        const hasVisibleEndNightUpdate = messages.some(message => message.event === 'phaseEndBid'
            && message.players.some(player => player.endedNight));
        expect(hasVisibleEndNightUpdate).to.be.equal(true);
    });

    function advanceSeconds(minutes){
        clock.tick(minutes * 1000);
    }

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }

    async function createGameWithFramer(){
        const user = await quasiUser.createUser();
        const setup = await quasiSetup.create(user.id);
        const gameObj = await quasiGame.create({
            hostID: user.id,
            playerCount: 3,
            setupID: setup.id,
        });

        const framer = testHelpers.getFactionRoleWithName(gameObj, 'Framer');
        const framerFaction = testHelpers.getFactionByID(gameObj, framer.factionID);
        const hiddenFramer = await hiddenService.create(user.id, {
            name: 'Framer',
            factionRoleIDs: [framer.id],
        });
        const citizen = testHelpers.getFactionRoleWithName(gameObj, 'Citizen');
        const hiddenCitizen = await hiddenService.create(user.id, {
            name: 'Citizen',
            factionRoleIDs: [citizen.id],
        });

        await Promise.all([
            setupHiddenService.add(user.id, hiddenFramer.id),
            setupHiddenService.add(user.id, hiddenCitizen.id),
            setupHiddenService.add(user.id, hiddenFramer.id),
        ]);

        return [await gameService.start(user.id), framerFaction.name];
    }
});
