require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const setupService = require('../../../services/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const HIDDEN_NAME = require('../../../../cypress/fixtures/fakeConstants').HIDDEN_NAME;

describe('Hidden Endpoint', async() => {
    it('Will create a hidden', async() => {
        const user = await quasiUser.createUser();
        const setup = await setupService.create(user.id);
        let gameObject = await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const hiddenLength = gameObject.setup.hiddens.length;
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const factionRoleID = gameObject.setup.factions[0].factionRoles[0].id;
        const hiddenBody = {
            name: HIDDEN_NAME,
            factionRoleIDs: [factionRoleID],
        };

        const responseObj = await requests.post('hiddens', hiddenBody, headers);
        gameObject = await gameService.getByJoinID(gameObject.joinID);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(!!responseObj.json.response.id).to.be.equal(true);
        expect(gameObject.setup.hiddens.length).to.be.equal(hiddenLength + 1);
        expect(!!gameObject.setup.hiddens[0].id).to.be.equal(true);
        const newHidden = gameObject.setup.hiddens
            .filter(hidden => hidden.id === responseObj.json.response.id)[0];
        expect(newHidden.factionRoleIDs[0]).to.be.equal(factionRoleID);
    });

    it('Will not create a hidden if the hidden doesn\'t belong to the user', async() => {
        const user1 = await quasiUser.createUser();
        const user2 = await quasiUser.createUser();
        const setup = await quasiSetup.create(user1.id);
        let gameObject = await quasiGame.create({ setupID: setup.id, hostID: user1.id });
        const hiddenLength = gameObject.setup.hiddens.length;
        const headers = authHelpers.fakeTempAuthHeader(user2.id);
        const factionRoleID = gameObject.setup.factions[0].factionRoles[0].id;
        const hiddenBody = {
            name: HIDDEN_NAME,
            factionRoleIDs: [factionRoleID],
        };

        const responseObj = await requests.post('hiddens', hiddenBody, headers);
        gameObject = await gameService.getByJoinID(gameObject.joinID);

        expect(responseObj.statusCode).to.be.equal(422);
        expect(gameObject.setup.hiddens.length).to.be.equal(hiddenLength);
    });

    it('Will not create a hidden if the request is missing a name', async() => {
        const user = await quasiUser.createUser();
        const setup = await quasiSetup.create(user.id);
        await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            factionRoleIDs: [setup.factions[0].factionRoles[0].id],
        };

        const responseObj = await requests.post('hiddens', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not create a hidden if the request\'s name is too short', async() => {
        const user = await quasiUser.createUser();
        const setup = await quasiSetup.create(user.id);
        await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            name: '',
            factionRoleIDs: [setup.factions[0].factionRoles[0].id],
        };

        const responseObj = await requests.post('hiddens', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not create a hidden if the request\'s roleIDs is an empty array', async() => {
        const user = await quasiUser.createUser();
        const setup = await quasiSetup.create(user.id);
        await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            name: [setup.factions[0].factionRoles[0].name],
            factionRoleIDs: [],
        };

        const responseObj = await requests.post('hiddens', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not create a hidden if the request is missing roleIDs', async() => {
        const user = await quasiUser.createUser();
        const setup = await quasiSetup.create(user.id);
        await quasiGame.create({ setupID: setup.id, hostID: user.id });
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            name: [setup.factions[0].factionRoles[0].name],
        };

        const responseObj = await requests.post('hiddens', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });
});
