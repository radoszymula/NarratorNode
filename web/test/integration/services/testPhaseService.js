require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const gamePhase = require('../../../models/enums/gamePhase');

const actionService = require('../../../services/actionService');
const gameService = require('../../../services/gameService');
const setupModifierService = require('../../../services/setupModifierService');

const quasiGame = require('../../quasiModels/quasiGame');


describe('Phases', async() => {
    let clock;
    beforeEach(() => {
        clock = sinon.useFakeTimers();
    });

    afterEach(() => {
        clock.restore();
    });

    it('Should end the role picking phase', async() => {
        const rolePickingLength = 1;
        const discussionLength = 1;
        let gameObj = await quasiGame.create({ playerCount: 8, setupKey: 'liars' });
        await setDiscussionMinuteLength(gameObj, discussionLength);
        await setPluralityVoting(gameObj);
        await setRolePickingMinuteLength(gameObj, rolePickingLength);
        await gameService.start(gameObj.host.id);

        advanceMinutes(rolePickingLength);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_CLOSED);
        expect(gameObj.dayNumber).to.be.equal(1);
    });

    it('Should not end the role picking phase if time hasn\'t run out', async() => {
        const rolePickingLength = 2;
        let gameObj = await quasiGame.create({ playerCount: 8, setupKey: 'liars' });
        await setPluralityVoting(gameObj);
        await setRolePickingMinuteLength(gameObj, rolePickingLength);
        await gameService.start(gameObj.host.id);

        advanceMinutes(rolePickingLength / 2);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.ROLE_PICKING_PHASE);
        expect(gameObj.dayNumber).to.be.equal(1);
    });

    it('Should end the discussion phase', async() => {
        const discussionLength = 5;
        let gameObj = await quasiGame.create({ playerCount: 7, setupKey: 'millercop7' });
        expect(gameObj.setup.setupHiddens.length).to.be.equal(7);
        await setPluralityVoting(gameObj);
        await setDiscussionMinuteLength(gameObj, discussionLength);
        await gameService.start(gameObj.host.id);

        advanceMinutes(discussionLength);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_OPEN);
        expect(gameObj.dayNumber).to.be.equal(1);
    });

    it('Should auto end the night', async() => {
        const nightLength = 44;
        const discussionLength = 0;
        let gameObj = await quasiGame.create({ playerCount: 8 });
        await setDiscussionMinuteLength(gameObj, discussionLength);
        await setNightMinuteLength(gameObj, nightLength);
        await gameService.start(gameObj.host.id);

        advanceMinutes(nightLength);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_OPEN);
        expect(gameObj.dayNumber).to.be.equal(1);
    });

    it('Should not auto the next phase if everyone ended night', async() => {
        const nightLength = 44;
        const discussionLength = nightLength * 2;
        let gameObj = await quasiGame.create({ playerCount: 8 });
        await setDiscussionMinuteLength(gameObj, discussionLength);
        await setNightMinuteLength(gameObj, nightLength);
        await gameService.start(gameObj.host.id);

        await Promise.all(gameObj.players.map(endNight));
        advanceMinutes(nightLength);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_CLOSED);
        expect(gameObj.dayNumber).to.be.equal(1);
    });

    it('Should keep the game in the voting phase if there\'s a tie', async() => {
        const discussionLength = 0;
        const dayLength = 50;
        let gameObj = await quasiGame.create({ playerCount: 7, setupKey: 'millercop7' });
        await setDayMinuteLength(gameObj, dayLength);
        await setDiscussionMinuteLength(gameObj, discussionLength);
        await gameService.start(gameObj.host.id);

        await submitVote(gameObj.players[0], gameObj.players[1]);
        await submitVote(gameObj.players[1], gameObj.players[0]);
        advanceMinutes(dayLength);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_OPEN);
        expect(gameObj.dayNumber).to.be.equal(1);

        await submitVote(gameObj.players[2], gameObj.players[1]);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.NIGHTTIME);
    });

    it('Should end trials', async() => {
        const discussionLength = 0;
        const dayLength = 50;
        const trialLength = 22;
        let gameObj = await quasiGame.create({ playerCount: 8, setupKey: 'ffa' });
        await setDiscussionMinuteLength(gameObj, discussionLength);
        await setDayMinuteLength(gameObj, dayLength);
        await setTrialMinuteLength(gameObj, trialLength);
        await gameService.start(gameObj.host.id);

        await submitVote(gameObj.players[0], gameObj.players[2]);
        await submitVote(gameObj.players[1], gameObj.players[2]);
        advanceMinutes(dayLength);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.TRIALS);
        expect(gameObj.dayNumber).to.be.equal(1);

        await advanceMinutes(trialLength);
        gameObj = await gameService.getByJoinID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_OPEN);
        expect(gameObj.dayNumber).to.be.equal(1);

        await submitVote(gameObj.players[2], gameObj.players[0]);
        await submitVote(gameObj.players[1], gameObj.players[0]);
    });

    function advanceMinutes(minutes){
        clock.tick(minutes * 60 * 1000);
    }
});

function setPluralityVoting(gameObj){
    const userID = gameObj.host.id;
    return setupModifierService.edit(userID, { name: 'VOTE_SYSTEM', value: 1 });
}

function setDayMinuteLength(gameObj, minutes){
    const userID = gameObj.host.id;
    const value = minutes * 60;
    return setupModifierService.edit(userID, { name: 'DAY_LENGTH', value });
}

function setTrialMinuteLength(gameObj, minutes){
    const userID = gameObj.host.id;
    const value = minutes * 60;
    return setupModifierService.edit(userID, { name: 'TRIAL_LENGTH', value });
}

function setNightMinuteLength(gameObj, minutes){
    const userID = gameObj.host.id;
    const value = minutes * 60;
    return setupModifierService.edit(userID, { name: 'NIGHT_LENGTH', value });
}

function setDiscussionMinuteLength(gameObj, minutes){
    const userID = gameObj.host.id;
    const value = minutes * 60;
    return setupModifierService.edit(userID, { name: 'DISCUSSION_LENGTH', value });
}

function setRolePickingMinuteLength(gameObj, minutes){
    const userID = gameObj.host.id;
    const value = minutes * 60;
    return setupModifierService.edit(userID, { name: 'ROLE_PICKING_LENGTH', value });
}

function endNight(player){
    return actionService.submitActionMessage(player.userID, { message: 'end night' });
}

async function submitVote(voter, voted){
    const { userID } = voter;
    const message = `vote ${voted.name}`;
    return actionService.submitActionMessage(userID, { message });
}
