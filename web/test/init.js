const mock = require('mock-require');

mock('firebase-admin', './fakeFirebase');
mock('discord.js', './quasiDiscord/quasiDiscordClient');
mock('../channels/sc2mafia/vBulletinClient', './fakeVBulletin');

const app = require('../app');

const gameService = require('../services/gameService');

const setupRepo = require('../repos/setupRepo');
const userRepo = require('../repos/userRepo');

const sc2mafiaGameRepo = require('../channels/sc2mafia/repos/sc2mafiaGameRepo');

const quasiDiscordClient = require('./quasiDiscord/quasiDiscordClient');


before(done => {
    app.start(4502).then(done);
});

beforeEach(async() => {
    const games = await gameService.getAll();
    await Promise.all(games.map(game => gameService.deleteGame(game.joinID)));

    const setupsDelete = setupRepo.deleteAll();
    const usersDelete = userRepo.deleteAll();
    const sc2mafiaGamesDelete = sc2mafiaGameRepo.deleteAll();

    await Promise.all([setupsDelete, usersDelete, sc2mafiaGamesDelete]);
});

afterEach(() => {
    quasiDiscordClient.resetCounter();
});
