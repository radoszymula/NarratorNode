const userRepo = require('../../repos/userRepo');
const userPermissionRepo = require('../../repos/userPermissionRepo');

const userPermissions = require('../../models/enums/userPermissions');

const { QuasiDiscordUser } = require('../quasiDiscord/quasiDiscordUser');


async function createMod(){
    const user = await userRepo.create();
    await userPermissionRepo.create(user.id, userPermissions.GAME_EDITING);
    await userPermissionRepo.create(user.id, userPermissions.ADMIN);
    return user;
}

function createUser(){
    return userRepo.create();
}

function createDiscordUser(){
    return new QuasiDiscordUser();
}

function createDiscordUsers(count){
    const ret = [];
    for(let i = 0; i < count; i++)
        ret.push(new QuasiDiscordUser());
    return ret;
}

module.exports = {
    createDiscordUser,
    createDiscordUsers,
    createMod,
    createUser,
};
