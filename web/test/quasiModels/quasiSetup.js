const setupRepo = require('../../repos/setupRepo');
const setupService = require('../../services/setupService');


async function create(ownerID, args){
    args = args || {};
    const setup = await setupService.create(ownerID);
    if(typeof(args.isActive) !== 'undefined' && !args.isActive)
        await setupRepo.deactivateNoneditableSetups(setup.id, ownerID);
    return setup;
}

module.exports = {
    create,
};
