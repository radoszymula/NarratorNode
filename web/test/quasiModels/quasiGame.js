const gameService = require('../../services/gameService');
const playerService = require('../../services/playerService');
const setupModifierService = require('../../services/setupModifierService');


async function create(gameOptions = {}){
    if(!gameOptions.setupKey && !gameOptions.setupID)
        gameOptions.setupKey = 'classic';
    const playerCount = gameOptions.playerCount || 1;

    const quasiUser = require('./quasiUser');
    const hostID = gameOptions.hostID || (await quasiUser.createUser()).id;
    const game = await gameService.create(hostID, 'host', gameOptions.setupKey,
        gameOptions.setupID);

    await addPlayersToGame(game.joinID, playerCount - 1);

    await setupModifierService.edit(hostID, { id: 'DISCUSSION_LENGTH', value: 0 });
    const modifierPromises = (gameOptions.setupModifiers || [])
        .map(modifier => setupModifierService.edit(hostID, modifier));
    await Promise.all(modifierPromises);
    if(gameOptions.isStarted)
        await gameService.start(hostID);

    return gameService.getByJoinID(game.joinID);
}

async function addPlayersToGame(joinID, playerCount){
    const quasiUser = require('./quasiUser');
    let promises = [];
    for(let i = 0; i < playerCount; i++)
        promises.push(quasiUser.createUser());

    const users = await Promise.all(promises);
    promises = users
        .map((user, index) => playerService.create(user.id, `guest${index + 1}`, joinID));
    return Promise.all(promises);
}

module.exports = {
    addPlayersToGame,
    create,
};
