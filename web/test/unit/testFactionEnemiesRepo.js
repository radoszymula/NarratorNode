require('../init');
const { expect } = require('chai');

const factionRepo = require('../../repos/factionRepo');
const factionEnemiesRepo = require('../../repos/factionEnemiesRepo');

const quasiSetup = require('../quasiModels/quasiSetup');
const quasiUser = require('../quasiModels/quasiUser');

const sqlErrors = require('../sqlErrors');


describe('Faction Enemies Repo', () => {
    let setup;
    beforeEach(async() => {
        const user = await quasiUser.createUser();
        setup = await quasiSetup.create(user.id);
    });

    it('Should not allow non existant factions in the first column', async() => {
        const [faction1] = setup.factions;
        await factionEnemiesRepo.deleteAll();

        try{
            await factionEnemiesRepo.setEnemies(faction1.id, faction1.id * 100);
            throw new Error('Expected an error and didn\'t get one!');
        }catch(err){
            expect(err.errno).to.be.equal(sqlErrors.NONEXISTANT_ID);
        }
    });

    it('Should not allow non existant factions in the second column', async() => {
        const [faction1] = setup.factions;
        await factionEnemiesRepo.deleteAll();

        try{
            await factionEnemiesRepo.setEnemies(faction1.id * 100, faction1.id);
            throw new Error('Expected an error and didn\'t get one!');
        }catch(err){
            expect(err.errno).to.be.equal(sqlErrors.NONEXISTANT_ID);
        }
    });

    it('Shold not allow duplicate entries in the db', async() => {
        const [faction1, faction2] = setup.factions;
        await factionEnemiesRepo.deleteAll();
        await factionEnemiesRepo.setEnemies(faction1.id, faction2.id);

        try{
            await factionEnemiesRepo.setEnemies(faction1.id, faction2.id);
        }catch(err){
            expect(err.errno).to.be.equal(sqlErrors.DUPLICATE_ROW);
        }
    });

    it('Shold not allow reversed duplicate entries in the db', async() => {
        const [faction1, faction2] = setup.factions;
        await factionEnemiesRepo.deleteAll();
        await factionEnemiesRepo.setEnemies(faction2.id, faction1.id);

        try{
            await factionEnemiesRepo.setEnemies(faction1.id, faction2.id);
        }catch(err){
            expect(err.errno).to.be.equal(sqlErrors.DUPLICATE_ROW);
        }
    });

    it('Should delete faction enemy entries on faction delete', async() => {
        const [faction1, faction2] = setup.factions;
        await factionEnemiesRepo.setEnemies(faction2.id, faction1.id);

        await factionRepo.deleteByID(setup.id, faction1.id);
        const factionEnemyIDs = await factionEnemiesRepo.getBySetupID(setup.id);

        factionEnemyIDs.forEach(([enemyID1, enemyID2]) => {
            expect(faction1).to.not.be.equal(enemyID1);
            expect(faction1).to.not.be.equal(enemyID2);
        });
    });
});
