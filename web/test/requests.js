const http = require('http');

const config = require('../../config');


function get(endpoint, args, headers){
    return serverRequest(endpoint, args, headers, 'GET');
}

function post(endpoint, args, headers){
    return serverRequest(endpoint, args, headers, 'POST');
}

function put(endpoint, args, headers){
    return serverRequest(endpoint, args, headers, 'PUT');
}

function del(endpoint, args, headers){
    return serverRequest(endpoint, args, headers, 'DELETE');
}

function serverRequest(endpoint, args, headers, method){
    if(args && typeof(args) === 'object')
        args = JSON.stringify(args);

    const options = {
        host: 'localhost',
        path: `/${endpoint}`,
        port: config.port_number,
        method,
        headers: headers || {},
    };

    return new Promise(resolve => {
        const req = http.request(options, response => {
            let data = '';
            response.on('data', dataChunk => {
                data += dataChunk;
            });
            response.on('end', () => {
                let json = null;
                try{
                    json = JSON.parse(data);
                }catch(err){}
                resolve({
                    statusCode: response.statusCode,
                    data,
                    json,
                });
            });
        });
        if(args)
            req.write(args);
        req.end();
    });
}

module.exports = {
    delete: del,
    get,
    post,
    put,
};
