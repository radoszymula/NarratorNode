const mysql = require('mysql');

const config = require('../config.json');

let logging = false;

function setLogging(loggingInput){
    logging = loggingInput;
}

const dbConnection = mysql.createPool({
    connectionLimit: 5,
    host: config.db_hostname,
    user: config.db_username,
    password: config.db_password,
    database: config.db_name,
    supportBigNumbers: true,
    bigNumberStrings: true,
});


// https://github.com/brianc/node-postgres/wiki/Parameterized-queries-and-Prepared-Statements
function query(qString, params){
    if(!params)
        params = [];

    return new Promise((resolve, reject) => {
        dbConnection.query(qString, params, (err, results) => {
            if(err && err.code !== 'ER_EMPTY_QUERY')
                return reject(err);

            if(err && err.code === 'ER_EMPTY_QUERY')
                results = [];

            if(logging){
                console.log(qString, params); /* eslint-disable-line no-console */
                console.log(`\t${JSON.stringify(results)}`); /* eslint-disable-line no-console */
            }
            resolve(results);
        });
    });
}

function close(){
    dbConnection.end();
}

module.exports = {
    query,
    close,
    setLogging,
};
