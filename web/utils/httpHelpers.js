const helpers = require('./helpers');


function getJson(req, allowNone){
    return new Promise((resolve, reject) => {
        let bodyStr = '';
        req.on('data', chunk => {
            bodyStr += chunk.toString();
        });
        req.on('end', () => {
            if(!bodyStr && allowNone)
                return resolve({});

            try{
                resolve(JSON.parse(bodyStr));
            }catch(err){
                reject(helpers.httpError('Body was not of json type.', 400));
            }
        });
    });
}

module.exports = {
    getJson,
};
