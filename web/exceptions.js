function dbNotFoundException(entityID){
    const err = `Entity was not found with that id: ${entityID}`;
    throw err;
}

module.exports = {
    dbNotFoundException,
};
