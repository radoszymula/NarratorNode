const { spawn } = require('child_process');

const config = require('../config.json');
const helpers = require('./utils/helpers');

const mariadbLocation = 'mariadb-java-client-2.3.0.jar';

let argSeparator;
if(config.pc_env)// indicates windows system
    argSeparator = ';';
else
    argSeparator = ':';

process.env.NARRATOR_CONFIG_LOCATION = `${__dirname}/../config.json`;

const args = ['-cp', `../../../libs/${mariadbLocation}${argSeparator}.`, 'nnode/SeedFinder'];
const proc = spawn('java', args, { cwd: 'src/main/java' })
    .on('exit', code => {
        helpers.log(code);
        process.exit();
    })
    .on('error', helpers.log);

proc.stdout.on('data', data => { /* eslint-disable-line no-unused-vars */
    // console.log(data);
});

proc.stderr.on('data', data => { /* eslint-disable-line no-unused-vars */
    // console.log(data.toString());
});
