const baseValidator = require('./baseValidator');


const updateSetupRequestSchema = {
    type: 'object',
    properties: {
        setupID: {
            type: 'number',
        },
    },
};

function setSetupRequest(query){
    baseValidator.validate(query, updateSetupRequestSchema);
}

module.exports = {
    setSetupRequest,
};
