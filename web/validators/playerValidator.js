const baseValidator = require('./baseValidator');


const addBotsRequestSchema = {
    type: 'object',
    properties: {
        botCount: {
            type: 'number',
            required: true,
        },
    },
};

const kickRequestSchema = {
    type: 'number',
    required: true,
};

function addBotsRequest(query){
    baseValidator.validate(query, addBotsRequestSchema);
}

function kickUserRequest(query){
    baseValidator.validate(query, kickRequestSchema);
}

module.exports = {
    addBotsRequest,
    kickUserRequest,
};
