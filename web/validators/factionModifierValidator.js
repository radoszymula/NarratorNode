const baseValidator = require('./baseValidator');


const updateFactionModifierRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        value: {
            type: ['number', 'boolean'],
            required: true,
        },
    },
};

function updateFactionModifierRequest(query){
    baseValidator.validate(query, updateFactionModifierRequestSchema);
}

module.exports = {
    updateFactionModifierRequest,
};
