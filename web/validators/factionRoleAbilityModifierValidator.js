const baseValidator = require('./baseValidator');


const updateFactionRoleAbilityModifierRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        value: {
            type: ['number', 'boolean'],
            required: true,
        },
    },
};

function updateFactionRoleAbilityModifierRequest(query){
    baseValidator.validate(query, updateFactionRoleAbilityModifierRequestSchema);
}

module.exports = {
    updateFactionRoleAbilityModifierRequest,
};
