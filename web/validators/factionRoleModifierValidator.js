const baseValidator = require('./baseValidator');


const updateFactionRoleModifierRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        value: {
            type: ['number', 'boolean'],
            required: true,
        },
    },
};

function updateFactionRoleModifierRequest(query){
    baseValidator.validate(query, updateFactionRoleModifierRequestSchema);
}

module.exports = {
    updateFactionRoleModifierRequest,
};
