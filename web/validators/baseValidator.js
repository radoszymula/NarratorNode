const { validate } = require('jsonschema'); // https://www.npmjs.com/package/jsonschema


function raiseValidationError(object, schema){
    const validateResponse = validate(object, schema);
    if(!validateResponse.errors.length)
        return;

    const errorObject = {
        statusCode: 422,
        errors: validateResponse.errors.map(error => error.stack),
    };
    throw errorObject;
}

module.exports = {
    validate: raiseValidationError,
};
