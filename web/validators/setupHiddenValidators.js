const baseValidator = require('./baseValidator');


const createHiddenRequestSchema = {
    type: 'object',
    properties: {
        hiddenID: {
            type: 'integer',
            required: true,
        },
    },
};

function createSetupHiddenRequest(query){
    baseValidator.validate(query, createHiddenRequestSchema);
}

module.exports = {
    createSetupHiddenRequest,
};
