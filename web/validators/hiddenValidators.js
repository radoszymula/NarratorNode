const baseValidator = require('./baseValidator');


const createHiddenRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        factionRoleIDs: {
            type: 'array',
            required: true,
            items: {
                type: 'integer',
            },
        },
    },
};

function createHiddenRequest(query){
    baseValidator.validate(query, createHiddenRequestSchema);
}

module.exports = {
    createHiddenRequest,
};
