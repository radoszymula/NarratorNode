const baseValidator = require('./baseValidator');


const getActiveUserIDsRequestSchema = {
    type: 'number',
    required: true,
};

const getCondorcetVotesRequestSchema = {
    type: 'number',
    required: true,
};

function getActiveUserIDsRequest(query){
    baseValidator.validate(query, getActiveUserIDsRequestSchema);
}

function getCondorcetVotesRequest(query){
    baseValidator.validate(query, getCondorcetVotesRequestSchema);
}

module.exports = {
    getActiveUserIDsRequest,
    getCondorcetVotesRequest,
};
