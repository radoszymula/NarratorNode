const baseValidator = require('./baseValidator');


const joinIDRequestSchema = {
    type: 'string',
    required: 'true',
    minLength: 4,
    maxLength: 4,
};

function joinIDRequest(query){
    baseValidator.validate(query, joinIDRequestSchema);
}

module.exports = {
    joinIDRequest,
};
