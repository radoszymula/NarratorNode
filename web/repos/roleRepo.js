const db = require('../db_communicator');


async function getByID(roleID){
    const queryText = 'SELECT name FROM roles WHERE id = ?;';
    const results = await db.query(queryText, [roleID]);
    if(!results.length)
        return null;
    return {
        id: roleID,
        name: results[0].name,
    };
}

module.exports = {
    getByID,
};
