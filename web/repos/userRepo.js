const db = require('../db_communicator');
const exceptions = require('../exceptions');
const userTypes = require('../models/enums/userTypes');


async function create(isGuest, externalID, integrationType){
    const results = await db.query('INSERT INTO users (is_guest) VALUES (?);', [!!isGuest]);
    const user = {
        id: parseInt(results.insertId, 10),
        isGuest,
    };
    if(!externalID)
        return user;

    const userIntegrationsRepo = require('./userIntegrationsRepo');
    await userIntegrationsRepo.add(user.id, externalID, integrationType);
    user.integrations = [{
        externalID,
        integrationType,
    }];
    return user;
}

function deleteAll(){
    return db.query('DELETE FROM users;');
}

async function getByFirebaseToken(externalID){
    return getByExternalID(externalID, userTypes.FIREBASE);
}

async function getByExternalID(externalID, userType){
    const queryText = 'SELECT users.id as id, users.is_guest as is_guest '
        + 'FROM user_integrations '
        + 'LEFT JOIN users '
        + 'ON users.id = user_integrations.user_id '
        + 'WHERE user_integrations.external_id = ? '
        + 'AND user_integrations.integration_type = ?;';
    const results = await db.query(queryText, [externalID, userType]);
    if(!results.length || results[0].id === null)
        return null;
    return {
        id: parseInt(results[0].id, 10),
        externalID,
        isGuest: results[0].is_guest,
    };
}

async function getByID(userID, defaultValue){
    const results = await db.query('SELECT * from users WHERE id = ?;', [userID]);
    if(results.length)
        return {
            id: parseInt(results[0].id, 10),
            isGuest: results[0].is_guest,
        };

    if(defaultValue)
        return defaultValue;
    exceptions.dbNotFoundException(userID);
}

async function getByJoinID(joinID){
    const userQuery = db.query('SELECT user_id '
        + 'FROM players '
        + 'LEFT JOIN replays '
        + 'ON replays.id = players.replay_id '
        + 'WHERE replays.instance_id = ?;', [joinID]);
    const hostQuery = db.query('SELECT host_id AS hostID '
        + 'FROM replays '
        + 'WHERE instance_id = ?;', [joinID]);
    const [userResults, hostResult] = await Promise.all([userQuery, hostQuery]);
    const userSet = new Set(userResults.map(result => parseInt(result.user_id, 10)));
    if(hostResult.length)
        userSet.add(parseInt(hostResult[0].hostID, 10));
    return userSet;
}

async function getByGameID(gameID){
    const userQuery = db.query('SELECT user_id '
        + 'FROM players '
        + 'LEFT JOIN replays '
        + 'ON replays.id = players.replay_id '
        + 'WHERE replays.id = ?;', [gameID]);
    const hostQuery = db.query('SELECT host_id AS hostID '
        + 'FROM replays '
        + 'WHERE id = ?;', [gameID]);
    const [userResults, hostResult] = await Promise.all([userQuery, hostQuery]);
    const userSet = new Set(userResults.map(result => parseInt(result.user_id, 10)));
    if(hostResult.length)
        userSet.add(parseInt(hostResult[0].hostID, 10));
    return userSet;
}

async function getBySiblingUserID(userID){
    const userQuery = db.query('SELECT user_id '
        + 'FROM players '
        + 'LEFT JOIN replays '
        + 'ON replays.id = players.replay_id '
        + 'WHERE replays.host_id = ? '
        + 'AND replays.instance_id iS NOT NULL;', [userID]);
    const hostQuery = db.query('SELECT host_id AS hostID '
        + 'FROM replays '
        + 'WHERE instance_id IS NOT NULL '
        + 'AND id IN ('
        + '  SELECT replay_id '
        + '  FROM players '
        + '  WHERE user_id = ?'
        + ');', [userID]);
    const [userResults, hostResult] = await Promise.all([userQuery, hostQuery]);
    const userSet = new Set(userResults.map(result => parseInt(result.user_id, 10)));
    if(hostResult.length)
        userSet.add(parseInt(hostResult[0].hostID, 10));
    return userSet;
}

// async function saveUser(externalID, userType, isGuest){
//     var result = await db.query('INSERT INTO users (token, type, is_guest) VALUES (?, ?, ?);', [externalID, userType, isGuest]);
//     return {
//         id: parseInt(result.insertId),
//         token: externalID,
//         isGuest: isGuest
//     }
// }

module.exports = {
    create,
    deleteAll,
    getByExternalID,
    getByFirebaseToken,
    getByGameID,
    getByID,
    getByJoinID,
    getBySiblingUserID,
    // saveUser: saveUser
};
