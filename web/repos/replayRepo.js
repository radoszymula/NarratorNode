const db = require('../db_communicator');


async function getByID(gameID){
    const queryText = 'SELECT setup_id as setupID, host_id as hostID FROM replays '
        + 'WHERE id = ?;';
    const results = await db.query(queryText, [gameID]);
    if(!results.length)
        return null;
    return results[0];
}

function deleteAll(){
    return db.query('DELETE FROM replays;');
}

module.exports = {
    getByID,
    deleteAll,
};
