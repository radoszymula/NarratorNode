const userPermissions = require('../models/enums/userPermissions');
const db = require('../db_communicator');


async function create(setup){
    const queryText = 'INSERT INTO setups '
        + '(owner_id, name, is_active) '
        + 'VALUES (?, ?, ?);';
    const params = [setup.ownerID, setup.name, setup.isActive];
    const result = await db.query(queryText, params);
    return {
        id: result.insertId,
        ownerID: setup.ownerID,
        name: setup.name,
    };
}

async function getByID(setupID){
    const queryText = 'SELECT name, owner_id as ownerID '
        + 'FROM setups '
        + 'WHERE id = ?;';
    const results = await db.query(queryText, [setupID]);
    if(!results.length)
        return null;
    return {
        id: setupID,
        name: results[0].name,
        ownerID: results[0].ownerID,
    };
}

function getAll(){
    const queryText = 'SELECT name, owner_id, id '
        + 'FROM setups';
    return db.query(queryText);
}

function deleteEditableSetups(setupID, userID){
    const queryText = 'DELETE FROM setups '
        + 'WHERE ('
        + '  owner_id = ? OR '
        + '  ? in (SELECT user_id FROM user_permissions WHERE permission_type = ?) '
        + ') '
        + 'AND id = ? '
        + 'AND id NOT IN ('
        + '  SELECT setup_id FROM replays);';
    return db.query(queryText, [userID, userID, userPermissions.ADMIN, setupID]);
}

function deactivateNoneditableSetups(setupID, userID){
    const queryText = 'UPDATE setups '
        + 'SET is_active = false '
        + 'WHERE id = ? '
        + 'AND owner_id = ?;';
    return db.query(queryText, [setupID, userID]);
}

function deleteAll(){
    return db.query('DELETE FROM setups;');
}

module.exports = {
    create,
    getByID,
    deleteEditableSetups,
    deactivateNoneditableSetups,

    getAll,
    deleteAll,
};
