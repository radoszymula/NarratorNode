const db = require('../db_communicator');

const helpers = require('../utils/helpers');


function getLeaderboardQueryParameters(column){
    const pointsBaseQuery = 'SELECT '
        + 'players.name, '
        + 'users.points AS value, '
        + 'users.id '
        + 'FROM users JOIN players '
        + 'ON players.user_id = users.id '
        + 'WHERE users.accountType != \'guest\' '
        + 'GROUP BY users.id '
        + 'ORDER BY value DESC '
        + 'LIMIT 5;';
    const pointsAboveQuery = 'SELECT players.name, '
        + 'users.id, '
        + 'users.points AS value '
        + 'FROM users JOIN players '
        + 'ON players.user_id = users.id '
        + 'WHERE users.points > ? '
        + 'AND users.accountType != \'guest\' '
        + 'GROUP BY users.id '
        + 'ORDER BY value '
        + 'LIMIT 2;';
    const pointsBelowQuery = 'SELECT '
        + 'players.name, '
        + 'users.id, '
        + 'users.points AS value '
        + 'FROM users JOIN players '
        + 'ON players.user_id = users.id '
        + 'WHERE users.points < ? '
        + 'AND users.accountType != \'guest\' '
        + 'GROUP BY users.id '
        + 'ORDER BY value DESC '
        + 'LIMIT 2;';
    const points = {
        baseQuery: pointsBaseQuery,
        meQuery: 'SELECT points AS value FROM users WHERE token = ? LIMIT 1;',
        aboveQuery: pointsAboveQuery,
        belowQuery: pointsBelowQuery,
        countQuery: 'SELECT points FROM users WHERE points > ? AND accountType != \'guest\';',
    };

    const gamesPlayedBaseQuery = 'SELECT '
        + 'players.name, '
        + 'COUNT(1) AS value, '
        + 'users.id '
        + 'FROM players JOIN users '
        + 'ON players.user_id = users.id '
        + 'WHERE users.accountType != \'guest\' '
        + 'GROUP BY users.id '
        + 'ORDER BY value DESC '
        + 'LIMIT 5;';
    const gamesPlayedAboveQuery = 'SELECT '
        + 'players.name, users.id, COUNT(1) AS value '
        + 'FROM players JOIN users '
        + 'ON players.user_id = users.id '
        + 'AND users.accountType != \'guest\' '
        + 'GROUP BY users.id '
        + 'HAVING COUNT(1) > ? '
        + 'ORDER BY value '
        + 'LIMIT 2;';
    const gamesPlayedBelowQuery = 'SELECT '
        + 'players.name, users.id, COUNT(1) AS value '
        + 'FROM players JOIN users '
        + 'ON (players.user_id = users.id '
        + 'AND users.accountType != \'guest\') '
        + 'GROUP BY users.id '
        + 'HAVING COUNT(1) < ? '
        + 'ORDER BY value DESC '
        + 'LIMIT 2;';
    const gamesPlayedCountQuery = 'SELECT COUNT(1) '
        + 'FROM players '
        + 'JOIN users '
        + 'ON(players.user_id = users.id '
        + 'AND users.accountType != \'guest\') '
        + 'GROUP BY users.id '
        + 'HAVING COUNT(1) > ?;';
    const gamesPlayed = {
        baseQuery: gamesPlayedBaseQuery,
        meQuery: 'SELECT COUNT(1) AS value FROM players WHERE token = ? LIMIT 1;',
        aboveQuery: gamesPlayedAboveQuery,
        belowQuery: gamesPlayedBelowQuery,
        countQuery: gamesPlayedCountQuery,
    };

    const winRateBaseQuery = 'SELECT players.name, users.winrate AS value, users.id '
        + 'FROM users '
        + 'JOIN players '
        + 'ON players.user_id = users.id '
        + 'WHERE users.accountType != \'guest\' '
        + 'GROUP BY users.id '
        + 'ORDER BY value DESC '
        + 'LIMIT 5;';
    const aboveQuery = 'SELECT players.name, users.id, users.winrate AS value '
        + 'FROM users JOIN players '
        + 'ON players.user_id = users.id '
        + 'WHERE users.winrate > ? '
        + 'AND users.accountType != \'guest\' '
        + 'GROUP BY users.id '
        + 'ORDER BY value '
        + 'LIMIT 2;';
    const belowQuery = 'SELECT players.name, users.id, users.winrate AS value '
        + 'FROM users JOIN players '
        + 'ON players.user_id = users.id '
        + 'WHERE users.winrate < ? '
        + 'AND users.accountType != \'guest\' '
        + 'GROUP BY users.id '
        + 'ORDER BY value DESC '
        + 'LIMIT 2;';
    const winRate = {
        baseQuery: winRateBaseQuery,
        meQuery: 'SELECT winrate AS value FROM users WHERE token = ? LIMIT 1;',
        aboveQuery,
        belowQuery,
        countQuery: 'SELECT winrate FROM users WHERE winrate > ? AND accountType != "guest"',
    };

    // count query optimization:
    // SELECT count(1) FROM (SELECT count(1) FROM db1 JOIN db2 ON db1.USER = db2.USER GROUP BY db2.USER HAVING count(db2.USER) > 2) src


    const columns = {
        points,
        gamesPlayed,
        winRate,
    };

    column = columns[column];
    if(column)
        return column;

    const err = 'column name not found';
    throw err;
}

function getPersonalStats(token){
    let stats = {};
    let points;
    let winRate;

    function appendToStats(data, change, columnName){
        if(!columnName)
            columnName = 'baseName';
        let role;
        let dataPoint;
        for(let i = 0; i < data.length; i++){
            dataPoint = data[i];
            role = stats[dataPoint[columnName]];
            if(!role){
                const err = 'null role';
                throw err;
            }
            change(role, dataPoint);
            // stats[data[i].baseName][propertyName] = data[i][columnName];
        }
    }
    let queryText = 'SELECT '
        + 'roles.name AS baseName, '
        + 'AVG(players.death_day) AS `avg(deathday)`, '
        + 'COUNT(roles.name) AS `COUNT(baseName)` '
        + 'FROM players '
        + 'LEFT JOIN faction_roles '
        + 'ON faction_roles.id = players.faction_role_id '
        + 'LEFT JOIN roles '
        + 'ON faction_roles.role_id = roles.id '
        + 'LEFT JOIN replays '
        + 'ON players.replay_id = replays.id '
        + 'WHERE players.user_id = ? '
        + 'AND replays.instance_id IS NULL '
        + 'GROUP BY roles.name;';
    return db.query(queryText, [token])
        .then(data => {
            for(let i = 0; i < data.length; i++)
                stats[data[i].baseName] = data[i];

            queryText = 'SELECT '
                + 'COUNT(roles.name) AS `COUNT(baseName)`, '
                + 'roles.name AS baseName, '
                + 'player_deaths.death_type AS death_type '
                + 'FROM player_deaths '
                + 'LEFT JOIN players '
                + 'ON players.id = player_deaths.player_id '
                + 'LEFT JOIN faction_roles '
                + 'ON players.faction_role_id = faction_roles.id '
                + 'LEFT JOIN roles '
                + 'ON roles.id = faction_roles.role_id '
                + 'LEFT JOIN replays '
                + 'ON players.replay_id = replays.id '
                + 'WHERE players.user_id = ? '
                + 'AND (players.deaths.death_type = \'lynch\' '
                + 'OR player_deaths.death_type = \'survivor\') '
                + 'AND replays.instance_id IS NULL '
                + 'GROUP BY roles.name, death_type;';
            return db.query(queryText, [token]);
        }).then(data => {
            appendToStats(data, (role, dataPoint) => {
                role[dataPoint.death_type] = dataPoint['COUNT(baseName)'];
            });

            queryText = 'SELECT '
                + 'COUNT(roles.name) AS `COUNT(baseName)`, '
                + 'roles.name as baseName '
                + 'FROM players '
                + 'LEFT JOIN faction_roles '
                + 'ON faction_roles.id = players.faction_role_id '
                + 'LEFT JOIN roles '
                + 'ON faction_roles.role_id = roles.id '
                + 'LEFT JOIN replays '
                + 'ON players.replay_id = replays.id '
                + 'WHERE replays.instance_id IS NULL '
                + 'AND players.is_winner = true '
                + ' AND players.user_id = ? '
                + ' GROUP BY roles.name;';
            return db.query(queryText, [token]);
        }).then(data => {
            appendToStats(data, (role, dataPoint) => {
                role.wins = dataPoint['COUNT(baseName)'];
            });
            queryText = 'SELECT '
                + 'replays.id AS game_id, '
                + 'replays.created_at AS `date`, '
                + 'roles.name AS role '
                + 'FROM players '
                + 'LEFT JOIN replays '
                + 'ON replays.id = players.replay_id '
                + 'LEFT JOIN faction_roles '
                + 'ON faction_roles.id = players.faction_role_id '
                + 'LEFT JOIN roles '
                + 'ON roles.id = faction_roles.role_id '
                + 'WHERE players.user_id = ? '
                + 'AND replays.instance_id IS NULL;';
            return db.query(queryText, [token]);
        })
        .then(data => {
            let game;
            appendToStats(data, (role, dataPoint) => {
                if(!role.games)
                    role.games = [];

                game = {};

                game.game_id = dataPoint.game_id;
                game.date = dataPoint.date;
                game.role = dataPoint.role;

                role.games.push(game);
            });

            return db.query('SELECT points, win_rate FROM users WHERE id = ?', [token]);
        })
        .then(data => {
            if(data.length){
                points = data[0].points;
                winRate = data[0].winrate;
            }else{
                points = 0;
                winRate = data[0].winrate;
            }
            return helpers.runJavaProg('EmporiumData');
        })
        .then(roleData => {
            roleData = JSON.parse(roleData);

            const roleNames = Object.keys(roleData);

            let roleName;
            for(let i = 0; i < roleNames.length; i++){
                roleName = roleNames[i];
                if(stats[roleName])
                    stats[roleName].roleName = roleData[roleName].roleName;
            }
            stats = {
                roleStats: stats,
            };
            stats.achievements = [];
            stats.points = points;
            stats.winRate = winRate;
            return stats;
        });
}

function getLeaderboardData(data){
    const token = data[0];
    let column = data[1];
    column = getLeaderboardQueryParameters(column);

    const lData = [];
    const seenTokens = [];
    let myValue;

    const basePromise = db.query(column.baseQuery)
        .then(results => {
            for(let i = 0; i < results.length; i++){
                results[i].rank = i + 1;
                seenTokens.push(results[i].token);
                if(results[i].token === token)
                    delete results[i].name;

                delete results[i].token;
                lData.push(results[i]);
            }
            return lData;
        });

    if(!token)
        return basePromise;

    return basePromise
        .then(db.query.bind(null, column.meQuery, [token]))
        .then(results => {
            if(!results.length){
                const escapeError = 'done';
                throw escapeError;
            }

            myValue = results[0].value;
            const abovePromise = db.query(column.aboveQuery, [myValue]);
            const belowPromise = db.query(column.belowQuery, [myValue]);

            return Promise.all([abovePromise, belowPromise]);
        }).then(promises => {
            let res;
            for(let i = promises[0].length - 1; i >= 0; i--){
                res = promises[0][i];
                if(seenTokens.indexOf(res.token) === -1){
                    lData.push(res);
                    seenTokens.push(res.token);
                }
            }

            const notInTop5 = seenTokens.indexOf(token) === -1;
            if(notInTop5)
                lData.push(null);


            for(let i = 0; i < promises[1].length; i++){
                res = promises[1][i];
                if(seenTokens.indexOf(res.token) === -1){
                    lData.push(res);
                    seenTokens.push(res.token);
                }
            }


            return db.query(column.countQuery, [myValue]);
        }).then(result => {
            let rank = result.length - 1; // wins query is tough to figure out
            rank = Math.max(rank, 6);

            for(let i = 5; i < lData.length; i++){
                if(lData[i])
                    lData[i].rank = rank;
                else
                    lData[i] = {
                        rank,
                        value: myValue,
                    };


                rank++;


                if(lData[i].rank <= 5){
                    lData.splice(i, 1);
                    i--;
                }
            }

            for(let i = 0; i < lData.length; i++)
                if(!lData[i]){
                    lData.splice(i, 1);
                    i--;
                }


            return lData;
        })
        .catch(err => {
            if(err === 'done')
                return;
            helpers.log(err);
            throw err;
        });
}

module.exports = {
    getLeaderboardData,
    getPersonalStats,
};
