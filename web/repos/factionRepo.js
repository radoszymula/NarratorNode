const db = require('../db_communicator');


function deleteByID(setupID, factionID){
    const queryText = 'DELETE FROM factions WHERE setup_id = ? AND id = ?;';
    return db.query(queryText, [setupID, factionID]);
}

module.exports = {
    deleteByID,
};
