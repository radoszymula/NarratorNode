function logicToRequest({ hiddenID, id }){
    return {
        event: 'setupHiddenAdd',
        hiddenID,
        setupHiddenID: id,
    };
}

module.exports = {
    logicToRequest,
};
