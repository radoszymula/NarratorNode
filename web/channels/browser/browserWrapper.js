const firebase = require('./firebase');

const helpers = require('../../utils/helpers');
const setHelpers = require('../../utils/setHelpers');

const userRepo = require('../../repos/userRepo');

const setupHiddenAddEvent = require('./eventMappers/setupHiddenAddEvent');
const setupHiddenRemoveEvent = require('./eventMappers/setupHiddenRemoveEvent');


async function getActiveGameUsersByUserID(gameID){
    const userIDs = await userRepo.getByGameID(gameID);
    const app = require('../../app');
    const webSocketIDs = app.getSocketUserIDs();
    return setHelpers.intersection(userIDs, webSocketIDs);
}

async function onUserActivityStatusChange(changedUserID, isActive){
    const app = require('../../app');
    const userIDs = await userRepo.getBySiblingUserID(changedUserID);
    let request = {
        event: 'playerStatusChange',
        isActive,
        userID: changedUserID,
    };
    request = JSON.stringify(request);
    [...userIDs]
        .filter(userID => userID !== changedUserID)
        .forEach(userID => {
            app.socketPush(userID, request);
        });
}

// specific functions above
// overloaded functions below

async function getUserInfoByAuthToken(authToken){
    if(helpers.internetEnabled())
        return firebase.getUserInfoByAuthToken(authToken);
    return {
        isGuest: false,
        externalID: 'externalID',
    };
}

function onActionSubmit(){}

function onBroadcast(){}

function onDayDeath(){}

function onDayStart(request){
    pushPhaseChange(request);
    pushTimer(request);
}

function onGameStart(){}

async function onGameEnd(request){
    const app = require('../../app');
    const userIDs = await userRepo.getByGameID(request.gameID);
    request = JSON.stringify(request);
    [...userIDs].forEach(userID => {
        app.socketPush(userID, request);
    });
}

async function onHostChange(request){
    const app = require('../../app');
    const userIDs = await userRepo.getByJoinID(request.joinID);
    request = JSON.stringify(request);
    [...userIDs].forEach(userID => {
        app.socketPush(userID, request);
    });
}

function onLobbyClose(){}

async function onNewPlayer(request){
    const app = require('../../app');
    const userIDs = await userRepo.getByJoinID(request.joinID);
    request = { ...request, event: 'playerAdded' };
    request = JSON.stringify(request);
    [...userIDs]
        .filter(userID => userID !== request.userID)
        .forEach(userID => {
            app.socketPush(userID, request);
        });
}

function onNewVote(){}

function onNightStart(request){
    pushPhaseChange(request);
    pushTimer(request);
}

async function onPhaseEndBid(request){
    const app = require('../../app');
    const playerService = require('../../services/playerService');
    const userIDs = await userRepo.getByGameID(request.gameID);
    [...userIDs]
        .forEach(async userID => {
            const requestString = JSON.stringify({
                ...request,
                players: await playerService.filterKeys(request, userID),
            });
            app.socketPush(userID, requestString);
        });
}

function onPhaseReset(request){
    pushTimer(request);
}

async function onPlayerRemove(request){
    const app = require('../../app');
    const userIDs = await userRepo.getByJoinID(request.joinID);
    const kickedID = request.userID;
    request = { ...request, event: 'playerExit' };
    request = JSON.stringify(request);
    [...userIDs]
        .forEach(userID => {
            app.socketPush(userID, request);
        });
    const kickEvent = {
        event: 'kicked',
        userID: kickedID,
    };
    app.socketPush(kickedID, JSON.stringify(kickEvent));
}

function onRoleCardUpdate(){}

async function onSetupChange(request){
    const app = require('../../app');
    const userIDs = await userRepo.getByJoinID(request.joinID);
    request = { ...request };
    request.event = 'setupChange';
    request = JSON.stringify(request);
    userIDs.forEach(userID => {
        app.socketPush(userID, request);
    });
}

async function onSetupHiddenAdd({ gameID, hiddenID, id }){
    const app = require('../../app');
    const userIDs = await userRepo.getByGameID(gameID);
    let event = setupHiddenAddEvent.logicToRequest({ hiddenID, id });
    event = JSON.stringify(event);
    userIDs.forEach(userID => {
        app.socketPush(userID, event);
    });
}

async function onSetupHiddenRemove({ gameID, setupHiddenID }){
    const app = require('../../app');
    const userIDs = await userRepo.getByGameID(gameID);
    let event = setupHiddenRemoveEvent.logicToRequest({ setupHiddenID });
    event = JSON.stringify(event);
    userIDs.forEach(userID => {
        app.socketPush(userID, event);
    });
}

function onTrialStart(request){
    pushTimer(request);
}

function onVotePhaseReset(request){
    pushTimer(request);
}

function onVotePhaseStart(request){
    pushTimer(request);
    pushPhaseChange(request);
}

module.exports = {
    getActiveGameUsersByUserID,
    onUserActivityStatusChange,

    getUserInfoByAuthToken,

    onActionSubmit,
    onBroadcast,
    onDayDeath,
    onDayStart,
    onGameStart,
    onGameEnd,
    onHostChange,
    onLobbyClose,
    onNewPlayer,
    onNewVote,
    onNightStart,
    onPhaseEndBid,
    onPhaseReset,
    onPlayerRemove,
    onRoleCardUpdate,
    onSetupChange,
    onSetupHiddenAdd,
    onSetupHiddenRemove,
    onTrialStart,
    onVotePhaseReset,
    onVotePhaseStart,
};

async function pushTimer(request){
    const userIDs = await userRepo.getByJoinID(request.joinID);
    const app = require('../../app');
    const phaseService = require('../../services/phaseService');
    const message = JSON.stringify({
        timer: phaseService.getEndTime(request.joinID),
        guiUpdate: true,
    });
    userIDs.forEach(userID => {
        app.socketPush(userID, message);
    });
}

async function pushPhaseChange(request){
    const userIDs = await userRepo.getByJoinID(request.joinID);
    const app = require('../../app');
    const profileMap = {};
    (request.profiles || []).forEach(profile => {
        profileMap[profile.userID] = profile;
    });
    delete request.profiles;
    userIDs.forEach(userID => {
        request.profile = profileMap[userID] || null;
        const stringRequest = JSON.stringify(request);
        app.socketPush(userID, stringRequest);
    });
}
