const firebase = require('firebase-admin');

const firebaseFile = require('../../../firebase.json');
const helpers = require('../../utils/helpers');


firebase.initializeApp({
    credential: firebase.credential.cert(firebaseFile),
    databaseURL: 'https://narrator-119be.firebaseio.com',
});

function getUserInfoByAuthToken(authToken){
    return firebase.auth().verifyIdToken(authToken)
        .then(decodedClaims => ({
            externalID: decodedClaims.user_id,
            isGuest: decodedClaims.firebase.sign_in_provider === 'anonymous',
        })).catch(err => {
            helpers.log(err, 'Unable to decode auth token.');
        });
}

module.exports = {
    getUserInfoByAuthToken,
};
