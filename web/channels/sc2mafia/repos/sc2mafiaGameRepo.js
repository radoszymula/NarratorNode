const db = require('../../../db_communicator');


function deleteAll(){
    return db.query('DELETE FROM sc2mafia_games;');
}

function deleteByJoinID(joinID){
    return db.query('DELETE FROM sc2mafia_games '
        + 'WHERE join_id = ?;', [joinID]);
}

async function insert(joinID, setupThreadID, signupThreadID, postID){
    if(!postID)
        postID = 1;
    const query = 'INSERT INTO sc2mafia_games '
        + '(join_id, setup_thread_id, signup_thread_id, latest_post_id) '
        + 'VALUES (?, ?, ?, ?);';

    await db.query(query, [joinID, setupThreadID, signupThreadID, postID]);
}

async function getByJoinID(joinID){
    const query = 'SELECT * '
        + 'FROM sc2mafia_games '
        + 'WHERE join_id = ?;';

    const results = await db.query(query, [joinID]);
    if(!results.length)
        return null;
    return {
        joinID,
        setupThreadID: results[0].setup_thread_id,
        signupThreadID: results[0].signup_thread_id,
        latestPostID: results[0].latest_post_id,
        gameThreadID: results[0].game_thread_id,
    };
}

async function getByThreadID(threadID){
    const query = 'SELECT * '
        + 'FROM sc2mafia_games '
        + 'WHERE signup_thread_id = ?;';
    const results = await db.query(query, [threadID]);
    if(!results.length)
        return null;
    return {
        latestPostID: results[0].latest_post_id,
        joinID: results[0].join_id,
    };
}

function updateLatestPostID(joinID, latestPostID){
    const query = 'UPDATE sc2mafia_games '
        + 'SET latest_post_id = ? '
        + 'WHERE join_id = ?;';
    return db.query(query, [latestPostID, joinID]);
}

function updateGameThreadID(joinID, gameThreadID){
    const query = 'UPDATE sc2mafia_games '
        + 'SET game_thread_id = ? '
        + 'WHERE join_id = ?;';
    return db.query(query, [gameThreadID, joinID]);
}

module.exports = {
    deleteAll,
    deleteByJoinID,
    getByJoinID,
    getByThreadID,
    insert,
    updateLatestPostID,
    updateGameThreadID,
};
