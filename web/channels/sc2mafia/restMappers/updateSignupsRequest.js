const postSignupsRequest = require('./postSignupsRequest');


function logicToRequest(game, signupPostID, setupThreadID){
    const message = postSignupsRequest.logicToRequest(game, setupThreadID).message;
    return {
        postid: signupPostID,
        message,
        reason: 'A new player joined or someone left.  I can\'t tell which right now.',
    };
}

module.exports = {
    logicToRequest,
};
