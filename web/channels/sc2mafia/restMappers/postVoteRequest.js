const utils = require('./utils');


function logicToRequest(voteRequest, gameThreadID){
    let message = voteRequest.voteText;
    if(voteRequest.voter){
        const voterName = voteRequest.voter.name;
        message = message.replace(`${voterName} `, `${utils.wrapBold(voterName)} `);
    }
    if(voteRequest.voteTarget){
        const { voteTarget } = voteRequest;
        message = message.replace(`${voteTarget}.`, `${utils.wrapBold(voteTarget)}.`);
    }
    return {
        threadid: gameThreadID,
        message,
        signature: true,
    };
}

module.exports = {
    logicToRequest,
};
