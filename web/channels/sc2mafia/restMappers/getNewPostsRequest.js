function logicToRequest(threadID, latestPostID, pageNumber){
    const request = {
        threadid: threadID,
    };
    if(pageNumber)
        request.page = pageNumber;
    else
        request.postid = latestPostID;
    return request;
}

function responseToLogic(response, latestPostID){
    let { postbits } = response;
    if(!Array.isArray(postbits))
        postbits = [postbits];
    const posts = postbits.map(post => (
        {
            externalID: parseInt(post.post.postid, 10),
            externalUserID: post.post.username,
            message: post.post.message_bbcode,
            playerName: post.post.username,
        }
    )).filter(post => !latestPostID || post.externalID > latestPostID);
    const pagenav = response.pagenav;
    const isLastPage = !pagenav || pagenav.pagenumber === pagenav.totalpages;
    return {
        isLastPage,
        lastPage: Math.ceil(response.totalposts / response.perpage),
        nextPage: !isLastPage && response.pagenav.nextpage,
        posts,
    };
}

module.exports = {
    logicToRequest,
    responseToLogic,
};
