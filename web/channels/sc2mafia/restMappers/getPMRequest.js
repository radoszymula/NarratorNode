function logicToRequest(pm){
    return {
        pmid: pm.pmID,
    };
}

function responseToLogic(response){
    const pm = response.response.HTML;
    return {
        id: pm.pm.pmid,
        createdAt: new Date(parseInt(pm.postbit.post.posttime, 10) * 1000),
        externalUserID: pm.postbit.post.userid,
        message: pm.postbit.post.message,
        name: pm.postbit.post.username,
    };
}

module.exports = {
    logicToRequest,
    responseToLogic,
};
