function responseToLogic(response){
    if(!response.response.HTML)
        return [];
    let periodGroups = response.response.HTML.messagelist_periodgroups;
    if(!periodGroups)
        return [];
    if(!Array.isArray(periodGroups))
        periodGroups = [periodGroups];
    return periodGroups.map(periodGroup => {
        let messageBits = periodGroup.messagelistbits;
        if(!Array.isArray(messageBits))
            messageBits = [messageBits];
        return messageBits;
    })
        .reduce((a, b) => a.concat(b), [])
        .map(pmResponse => ({
            pmID: pmResponse.pm.pmid,
        }));
}

module.exports = {
    responseToLogic,
};
