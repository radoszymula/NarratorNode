function logicToRequest(pmID){
    return {
        pmid: pmID,
    };
}

module.exports = {
    logicToRequest,
};
