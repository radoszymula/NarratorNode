const CIRCLE_JERK = 336;

const updateGameStatusRequest = require('./updateGameStatusRequest');


function logicToRequest(game){
    return {
        forumid: CIRCLE_JERK,
        subject: `A-FM ${game.setup.name} (Game)`,
        message: updateGameStatusRequest.logicToRequest(game).message,
    };
}

function responseToLogic(response){
    return response.threadid;
}

module.exports = {
    logicToRequest,
    responseToLogic,
};
