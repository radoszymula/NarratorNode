function map(newPosts){
    return newPosts
        .filter(extractVote)
        .map(newPost => ({
            externalID: newPost.externalUserID,
            joinID: newPost.joinID,
            voteTarget: extractVote(newPost),
        }));
}

module.exports = {
    map,
};

function extractVote(post){
    const message = post.message.toLowerCase();
    if(!message.endsWith('[/vote]'))
        return false;
    if(!message.startsWith('[vote]'))
        return false;
    return message.substring(6, message.length - 7).trim();
}
