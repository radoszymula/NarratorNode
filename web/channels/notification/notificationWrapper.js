/* eslint-disable no-unused-vars, func-names */

function NotificationWrapper(){

}

NotificationWrapper.prototype.onActionSubmit = function(request){
    return request.pmContents;
};

NotificationWrapper.prototype.onBroadcast = function(request){
    return request.pmContents;
};

NotificationWrapper.prototype.onDayDeath = function(request){
    return request.contents;
};

NotificationWrapper.prototype.onDayStart = function(request){

};

NotificationWrapper.prototype.onGameStart = function(request){
    return 'Game has started';
};

NotificationWrapper.prototype.onHostChange = function(){};

NotificationWrapper.prototype.onLobbyClose = function(request){
    return 'Game canceled';
};

NotificationWrapper.prototype.onNewPlayer = function(request){
    return `${request.playerName} has joined the lobby`;
};

NotificationWrapper.prototype.onNightStart = function(request){

};

NotificationWrapper.prototype.onPhaseEndBid = function(){};

NotificationWrapper.prototype.onPlayerRemove = function(request){

};

NotificationWrapper.prototype.onRoleCardUpdate = function(request){

};

NotificationWrapper.prototype.onSetupChange = function(request){

};

NotificationWrapper.prototype.onSetupHiddenAdd = function(request){

};

NotificationWrapper.prototype.onSetupHiddenRemove = function(request){

};

NotificationWrapper.prototype.onTrialStart = function(request){

};

NotificationWrapper.prototype.onVote = function(request){
    return request.voteText;
};

NotificationWrapper.prototype.onVotePhaseReset = function(request){

};

NotificationWrapper.prototype.onVotePhaseStart = function(request){

};

module.exports = {
    NotificationWrapper,
};
