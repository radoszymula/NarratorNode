const messageService = require('./discordServices/messageService');

const discordJsonClient = require('./discordJsonClient');


function DiscordInstance(channelID, joinID, hostUserID, channelCommand){
    this.channelCommand = channelCommand;
    this.channelID = channelID;
    this.cleanupMessages = [];
    this.hostUserID = hostUserID;
    this.isStarted = false;
    this.isDay = null;
    this.joinID = joinID;
    this.talkers = [];
    this.userIDs = [hostUserID]; // i need to know this so i delete messages that shouldn't be in games
}

DiscordInstance.prototype.setAllowedTalkers = function(talkers){
    this.talkers = talkers;
};

DiscordInstance.prototype.isAdminAccidentalTalking = async function(m){
    if(!this.isStarted)
        return false;
    const discordClient = require('./discordWrapper');
    const talkerID = discordClient.getUserIDByDiscordID(m.author.id);
    if(this.talkers.includes(talkerID))
        return false;
    await messageService.deleteMessage(m);
    let message;
    if(this.isPartOfGame(m.author.id))
        message = 'You shouldn\'t be talking in that channel right now!';
    else
        message = 'You shouldn\'t be talking in that channel.  There\'s a game going on!';
    discordJsonClient.sendDirectMessage(m.author.id, message);
    return true;
};

DiscordInstance.prototype.isPartOfGame = function(discordID){
    const discordClient = require('./discordWrapper');
    const userID = discordClient.getUserIDByDiscordID(discordID);
    return this.userIDs.includes(userID);
};

module.exports = {
    DiscordInstance,
};
