const db = require('../../../db_communicator');


async function getByGuildID(guildID){
    const query = 'SELECT user_id '
        + 'FROM discord_subscribers '
        + 'WHERE guild_id = ?;';

    const results = await db.query(query, [guildID]);
    return results.map(result => result.user_id);
}

async function getByUserID(userID){
    const query = 'SELECT guild_id '
        + 'FROM discord_subscribers '
        + 'WHERE user_id = ?;';

    const results = await db.query(query, [userID]);
    return results.map(result => result.guildID);
}

function insert(guildID, userID){
    const query = 'INSERT IGNORE INTO discord_subscribers '
        + '(guild_id, user_id) '
        + 'VALUES (?, ?);';
    return db.query(query, [guildID, userID]);
}

function deleteEntry(guildID, userID){
    const query = 'DELETE FROM discord_subscribers '
        + 'WHERE guild_id = ? '
        + 'AND user_id = ?;';
    return db.query(query, [guildID, userID]);
}

module.exports = {
    getByUserID,
    getByGuildID,
    insert,
    deleteEntry,
};
