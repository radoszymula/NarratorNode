const db = require('../../../db_communicator');


async function getAll(client){
    const channelIDs = await db.query('SELECT channel_id FROM discord_edited_channels;');
    return channelIDs.map(result => client.channels.get(result.channel_id));
}

function deleteAll(){
    return db.query('DELETE FROM discord_edited_channels');
}

function save(channelID){
    return db.query('INSERT INTO discord_edited_channels (channel_id) VALUES (?);', [channelID]);
}

module.exports = {
    deleteAll,
    getAll,
    save,
};
