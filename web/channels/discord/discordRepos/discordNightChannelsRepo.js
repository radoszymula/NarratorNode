const db = require('../../../db_communicator');

// UNUSED ??

async function getAll(){
    const results = await db.query('SELECT channel_id FROM discord_night_channels;');
    return results.map(result => result.channel_id);
}

function deleteAll(){
    return db.query('DELETE FROM discord_night_channels;');
}

async function save(discordRoomID, replayID){
    const query = 'INSERT INTO discord_night_channels (channel_id, game_id) VALUES (?, ?)';
    return db.query(query, [discordRoomID, replayID]);
}

module.exports = {
    deleteAll,
    getAll,
    save,
};
