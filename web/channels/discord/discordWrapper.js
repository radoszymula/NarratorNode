const db = require('../../db_communicator');
const config = require('../../../config');
const { DiscordInstance } = require('./discordInstance');

const javaRequests = require('../../utils/javaRequests');

const messageService = require('./discordServices/messageService');

const editedChannelsRepo = require('./discordRepos/discordEditedChannelsRepo');
// const nightChannelsRepo = require('./discordRepos/discordNightChannelsRepo');

const gameService = require('../../services/gameService');
const playerService = require('../../services/playerService');
const timerService = require('../../services/timerService');
const voteService = require('../../services/voteService');

const helpers = require('../../utils/helpers');

const discordJsonClient = require('./discordJsonClient');
const mappers = require('./mappers/mappers');
const onGuildMemberAdd = require('./events/external/onGuildMemberAdd');

const greetings = [
    'I\'m back online!',
    'Who missed me?',
    'I was here all along.',
    'Self-awareness attained.',
    'Is it me that was offline, or were you just online?',
    'Ready to host, never to play.',
];

const channelIDToGame = {};
const joinIDToGame = {};
const discordIDToGame = {};
const discordIDToUserID = {};
const userIDToDiscordID = {};

const allowedPlayerCommands = {};
const modkilledUsers = {};

function DiscordWrapper(){
    this.gameBeingDeleted = {};
    this.nightChats = {};
    this.afterGuildMemberAdd = {};
    this.autoTagInfo = {};

    this.client = discordJsonClient.client;
}

DiscordWrapper.prototype.connect = async function(){
    await discordJsonClient.connect();

    this.client.on('messageUpdate', (oldMessage, newMessage) => {
        this.onMessageUpdate(newMessage, oldMessage);
    });

    this.client.on('messageDelete', message => {
        this.onMessageUpdate(message);
    });

    this.client.on('guildMemberAdd', gMember => {
        onGuildMemberAdd.eventToLogic(this, gMember);
    });

    const channel = this.client.channels.get(config.discord.reboot_channel_id);
    const rText = greetings[Math.floor(Math.random() * greetings.length)];
    return Promise.all([
        channel.send(rText),
        restoreUnfinishedGameChannels(this),
        deleteNightChannels(this),
    ]);
};

DiscordWrapper.prototype.onMessageUpdate = async function(newMessage, oldMessage){
    const discordID = newMessage.author.id;
    if(this.client.user.id === discordID)
        return;
    if(newMessage.channel.type !== 'text')
        return;
    const channelID = newMessage.channel.id;
    const game = getGameByChannelID(newMessage.channel.id);
    if(!game)
        return;

    if(oldMessage && newMessage.content === oldMessage.content)
        return;
    if(!oldMessage && newMessage.content.startsWith
        && newMessage.content.startsWith(game.channelCommand))
        return;

    if(!discordIDToUserID[discordID])
        return;

    if(modkilledUsers[discordID] > 1)
        return;

    if(messageService.unmarkSafeDeletedMessage(newMessage.id))
        return;

    if(modkilledUsers[discordID]){
        try{
            await playerService.modkill(discordIDToUserID[discordID]);
            modkilledUsers[discordID]++;
        }catch(err){
            helpers.log(err, 'Couldn\'t modkill in discord');
            const message = `I'm having trouble stopping <@!${discordID}> `
                + 'from talking when they\'re not supposed to.';
            return discordJsonClient.sendPublicMessage(channelID, message);
        }
    }else{
        modkilledUsers[discordID] = 1;
        newMessage.reply('stop editing your commands.  '
            + '**If you edit or delete your messages again, I will modkill you.**');
    }
};

DiscordWrapper.prototype.expireGame = function(channelID){
    const { joinID } = getGameByChannelID(channelID);

    if(!joinID || this.gameBeingDeleted[channelID])// was deleted
        return;

    this.gameBeingDeleted[channelID] = 1;

    return gameService.deleteGame(joinID);
};

/*
 *  Server methods
 */

function cleanupPlayer(userID){
    if(!getGameByUserID(userID))
        return;
    const discordID = getDiscordIDByUserID(userID);

    delete allowedPlayerCommands[discordID];
    delete modkilledUsers[discordID];
    delete discordIDToGame[discordID];
}

DiscordWrapper.prototype.onPhaseEndBid = function(){};

DiscordWrapper.prototype.onPlayerRemove = function(){};

DiscordWrapper.prototype.onRoleCardUpdate = async function(jo){
    const dUserID = getDiscordIDByUserID(jo.userID);
    if(!dUserID)
        return;

    let label = '**';
    if(jo.isDay)
        label += 'Day ';
    else
        label += 'Night ';
    label += `${jo.dayNumber}**\n`;

    let output = label;
    output += `You are a **${jo.teamName} ${jo.roleName}**.\n`;
    if(typeof jo.enemy === 'object')
        if(jo.enemy.length === 1)
            output += `You must eliminate **${jo.enemy[0]}**.`;
        else{
            output += 'You must eliminate the following : ';
            for(let i = 0; i < jo.enemy.length; i++){
                output += `**${jo.enemy[i]}**`;
                if(i !== jo.enemy.length - 1)
                    output += ', ';
            }
        }
    else if(jo.enemy.indexOf(' ') !== -1)
        output += `To win, you must get **${jo.enemy}** lynched.`;
    else
        output += jo.enemy;

    // const gameObj = await gameService.getByJoinID(jo.joinID);
    const { allies } = jo;
    if(allies.length){
        output += '\n';
        if(allies.length === 1)
            output += 'Your ally is ';
        else
            output += 'Your allies are : ';

        allies.forEach((ally, index) => {
            if(index)
                output += ', ';
            output += `**${ally.name}(${ally.roleName})**`;
            // TODO use roleID and lookup.  game logic isn't there yet
        });
        output += '.';
    }

    output += '\nGood luck!';

    await discordJsonClient.sendDirectMessage(dUserID, output);
    const game = getGameByDiscordID(dUserID);
    if(game)
        timerService.setGameExpiration(game.channelID);
};

DiscordWrapper.prototype.cleanupChannel = function(channelID){
    this.stopAutoTag(channelID);
    this.cleanupSubscriptionMessages(channelID);

    const game = getGameByChannelID(channelID);
    game.userIDs.forEach(userID => cleanupPlayer(userID));

    delete this.gameBeingDeleted[channelID];
    delete channelIDToGame[channelID];
    delete joinIDToGame[game.joinID];

    const channel = this.client.channels.get(channelID);

    return db.query('DELETE FROM discord_edited_channels WHERE channel_id = ?;', [channelID])
        .then(() => setDefaultPermission(channel)).catch(helpers.log).then(() => game);
};

DiscordWrapper.prototype.cleanupSubscriptionMessages = function(channelID){
    const cleanupMessages = getGameByChannelID(channelID).cleanupMessages;
    cleanupMessages.forEach(discordJsonClient.deleteMessageByIDs);
};

DiscordWrapper.prototype.stopAutoTag = function(channelID){
    const autoTag = this.autoTagInfo[channelID];
    if(!autoTag)
        return;
    autoTag.stop();
    delete this.autoTagInfo[channelID];
};

DiscordWrapper.prototype.removeLivingRole = function(channelID, dToken){
    const channel = this.client.channels.get(channelID);
    return removeChatPermission(channel, dToken);
};

function removeChatPermission(channel, snowflake){
    return channel.overwritePermissions(snowflake, { SEND_MESSAGES: false }).catch(helpers.log);
}

function addChatPermission(channel, snowflake){
    return channel.overwritePermissions(snowflake, { SEND_MESSAGES: true }).catch(helpers.log);
}

async function setDefaultPermission(channel){
    const permissions = channel.permissionOverwrites
        .filter(permission => permission.type === 'member');
    await Promise.all(permissions.map(permission => permission.delete()));
    return addChatPermission(channel, channel.guild.defaultRole.id);
}

DiscordWrapper.prototype.refreshChatPermissions = function(jo){
    const channelID = getGameByJoinID(jo.joinID);
    const nightChatsGuild = this.client.guilds.get(config.discord.night_chat_server);

    let chatIDLookup = this.nightChats[channelID];
    if(!chatIDLookup)
        chatIDLookup = this.nightChats[channelID] = {};

    const notifiedThisPhase = [];
    const bot = this;

    const promises = jo.nightChatInfo.map(async nChat => {
        let chatName = nChat.name.replace(/\W+/g, '-');
        if(chatName.endsWith('-'))
            chatName = chatName.substring(0, chatName.length - 1);
        let nChatChannelID = chatIDLookup[chatName];

        if(!nChatChannelID){
            const discordChatRoom = await nightChatsGuild.createChannel(chatName, 'text');
            // await nightChannelsRepo.save(discordChatRoom.id, jo.replayID);
            nChatChannelID = chatIDLookup[chatName] = discordChatRoom.id;
        }


        const ps = [];
        const nChatChannel = nightChatsGuild.channels.get(nChatChannelID);
        const allowedMembers = Object.keys(nChat.members);
        allowedMembers.forEach(userID => {
            const discordID = getDiscordIDByUserID(userID);
            if(!discordID)
                return;
            const inGuild = nightChatsGuild.members.get(discordID);
            const nightChatPermission = getNightChatPermission(nChat.active);
            if(!inGuild){
                if(notifiedThisPhase.indexOf(discordID) === -1){
                    notifiedThisPhase.push(discordID);
                    if(nChatChannel){
                        const p = discordJsonClient.getInviteURL(nChatChannelID)
                            .then(inviteURL => {
                                const text = 'You can access '
                                    + `your secret night chats here: ${inviteURL}`;
                                return discordJsonClient.sendDirectMessage(discordID, text);
                            });
                        ps.push(p);
                    }else{
                        helpers.log(`nChatChannel was null ${nChatChannelID}, ${chatName}`);
                    }
                }
                let currentInvites = bot.afterGuildMemberAdd[discordID];
                if(!currentInvites)
                    currentInvites = bot.afterGuildMemberAdd[discordID] = {};

                currentInvites[nChatChannelID] = nightChatPermission;
            }else if(nChatChannel){
                const p = nChatChannel.overwritePermissions(discordID, nightChatPermission);
                ps.push(p);
            }else{
                helpers.log(`nChatChannel was null ${nChatChannelID}, ${chatName}`);
            }
        });

        if(nChatChannel){
            const currentMembers = nChatChannel.permissionOverwrites.keyArray();
            currentMembers.forEach(userID => {
                const discordID = getDiscordIDByUserID(userID);
                if(!discordID)
                    return;
                if(allowedMembers.indexOf(discordID) !== -1)
                    return;
                const inGuild = nightChatsGuild.members.get(discordID);
                const nightChatPermission = getNightChatPermission(false, true);
                if(!inGuild){
                    let currentInvites = bot.afterGuildMemberAdd[discordID];
                    if(!currentInvites)
                        currentInvites = bot.afterGuildMemberAdd[discordID] = {};

                    currentInvites[nChatChannelID] = nightChatPermission;
                }else{
                    const p = nChatChannel.overwritePermissions(discordID, nightChatPermission);
                    ps.push(p);
                }
            });
        }else{
            helpers.log(`nChatChannel was null ${nChatChannelID}, ${chatName}`);
        }
    });
    return Promise.all(promises).catch(helpers.log);
};

function getNightChatPermission(active, removing){
    return {
        SEND_MESSAGES: !!active,
        READ_MESSAGE_HISTORY: !removing,
        VIEW_CHANNEL: !removing,
    };
}

DiscordWrapper.prototype.saveAllowedPlayerCommands = function(jo){
    let discordID;
    let commands;
    return Object.keys(jo.playerCommands)
        .filter(getDiscordIDByUserID)
        .map(userID => {
            discordID = getDiscordIDByUserID(userID);
            commands = jo.playerCommands[userID];
            allowedPlayerCommands[discordID] = commands;
            return discordID;
        });
};

function nightChangingText(nightEnd, message, ogMessage, first, channelID){
    const remTime = nightEnd - new Date().getTime();

    if(remTime > 0){
        let toWait = 5000;
        if(first)
            toWait -= 1000;
        if(getGameByChannelID(channelID).isNight)
            setTimeout(() => {
                nightChangingText(nightEnd, message, ogMessage, null, channelID);
            }, toWait);

        let seconds = Math.floor(remTime / 1000);
        let minutes = Math.floor(seconds / 60);
        const hour = Math.floor(minutes / 60);

        let text = '';
        if(hour > 0){
            text += `${hour}:`;
            minutes %= 60;
            if(minutes < 10)
                minutes = `0${minutes}`;
        }else{
            text += '00:';
        }
        if(minutes < 10)
            text += '0';
        text += `${minutes}:`;
        seconds = (seconds % 60).toString();
        if(seconds < 10)
            seconds = `0${seconds}`;

        text += seconds;
        if(!getGameByChannelID(channelID).isDay)
            text = '00:00:00';

        message.edit(`${ogMessage}**${text}**`).catch(helpers.log);
    }
}

async function tryDayMessage(m){
    const text = m.args;
    if(text.toLowerCase() === 'help')
        return this.help(m);
    const game = getGameByChannelID(m.channelID);
    if(!game)
        return;
    const userID = discordIDToUserID[m.discordID];
    if(!userID)
        return;

    if(game.isAdminAccidentalTalking(m.raw))
        return;

    const o = {
        message: 'discordDayTalk',
        text,
        userID,
    };

    javaRequests.sendInstanceRequest(o);

    await removeChatPermission(m.raw.channel, m.discordID).catch(helpers.log);
    setTimeout(() => {
        addChatPermission(m.raw.channel, m.discordID)
            .catch(helpers.log);
    }, 3000);
}

DiscordWrapper.prototype.setActiveChannelTalkingStatus = function(channelID, allowedUserIDs){
    const channel = this.client.channels.get(channelID);
    const game = getGameByChannelID(channelID);
    const userIDs = game.userIDs.slice();

    const ps = userIDs.map(userID => {
        const discordID = getDiscordIDByUserID(userID);
        if(!discordID)
            return Promise.resolve();
        if(allowedUserIDs.includes(userID))
            return addChatPermission(channel, discordID);
        return removeChatPermission(channel, discordID);
    });
    return Promise.all(ps).catch(helpers.log);
};

async function restoreUnfinishedGameChannels(wrapper){
    const editedChannels = await editedChannelsRepo.getAll(wrapper.client);
    await Promise.all(editedChannels.map(channel => setDefaultPermission(channel)));
}

async function deleteNightChannels(wrapper){
    const guild = wrapper.client.guilds.get(config.discord.night_chat_server);
    const privateChatGuildRooms = guild.channels;
    return Promise.all(privateChatGuildRooms.map(channel => channel.delete()));
}

function getMention(userID, name){
    const discordID = getDiscordIDByUserID(userID);
    if(!discordID)
        return name;

    return `<@!${discordID}>`;
}

// approved prototypes

DiscordWrapper.prototype.onActionSubmit = function(request){
    const discordID = getDiscordIDByUserID(request.userID);
    if(!discordID)
        return;
    const message = mappers.onActionSubmitMessage(request, discordID);
    discordJsonClient.sendMessage(message);
};

// this'll be a future refactor.  let the wrappers dictate what's sent to the clients
DiscordWrapper.prototype.onBroadcast = function(request){
    const game = getGameByJoinID(request.joinID);
    const message = mappers.onBroadcastMessage(request, game);
    discordJsonClient.sendMessage(message);
};

DiscordWrapper.prototype.onDayDeath = async function(request){
    const game = getGameByJoinID(request.joinID);
    const message = mappers.onDayDeathMessage(request, game);

    let dToken; const
        channel = this.client.channels.get(game.channelID);

    const removals = request.userIDs.map(gToken => {
        dToken = getDiscordIDByUserID(gToken);
        return removeChatPermission(channel, dToken);
    });

    await Promise.all(removals);
    discordJsonClient.sendMessage(message);
};

DiscordWrapper.prototype.onDayStart = async function(jo){
    const { channelID } = getGameByJoinID(jo.joinID);
    let output = `**Day ${jo.dayNumber}**\n`;

    if(jo.graveYard){
        let deadPerson;
        for(let i = 0; i < jo.graveYard.length; i++){
            deadPerson = jo.graveYard[i];

            output += `${getMention(deadPerson.userID, deadPerson.playerName)} `;

            if(deadPerson.deathType){
                output += 'was ';
                for(let j = 0; j < deadPerson.deathType.length; j++){
                    output += deadPerson.deathType[j].toLowerCase();
                    if(j !== deadPerson.deathType.length - 1)
                        output += ', ';
                }
                output += '. \n';
                output += `They were a **${deadPerson.teamName} ${deadPerson.roleName}**.`;
                if(deadPerson.lastWill)
                    output += `They left a last will:\n*${deadPerson.lastWill}*`;
                output += '\n';
            }else{
                output += 'died!\nWe could not determine what they were.\n';
            }
        }
        this.onPhaseDeath(channelID, jo.graveYard);
    }else if(!jo.firstPhase){
        output += 'No one died!\n';
    }

    if(jo.snitchReveals)
        jo.snitchReveals.forEach(reveal => {
            output += 'A reliable source has revealed ';
            output += getMention(reveal.userID, reveal.name);
            output += `to be a **${reveal.teamName} ${reveal.roleName}**.\n`;
        });


    if(jo.discussionLength)
        output += `Voting polls will open in **${jo.discussionLength}** minutes.`;
    else
        output += `Voting polls will close in **${jo.dayLength}** minutes.`;

    const actioneers = this.saveAllowedPlayerCommands(jo);
    await this.refreshChatPermissions(jo);

    for(let i = 0; i < actioneers.length; i++){
        const userID = discordIDToUserID[actioneers[i]];
        if(userID)
            output += ` ${getMention(userID)},`;
    }

    discordJsonClient.sendPublicMessage(channelID, output);

    // living is all the people that can talk during the day, including the not blackmailed people
    this.setActiveChannelTalkingStatus(channelID, jo.living);
    getGameByChannelID(channelID).setAllowedTalkers(jo.living);

    const game = getGameByChannelID(channelID);
    game.isStarted = true;
    game.isDay = true;
};

DiscordWrapper.prototype.onLobbyClose = async function(request){
    const game = getGameByJoinID(request.joinID);
    await this.cleanupChannel(game.channelID);
    const message = mappers.onLobbyCloseMessage(request, game);
    discordJsonClient.sendMessage(message);

    // old code
    let queryText = 'SELECT channel_id FROM discord_night_channels WHERE game_id = ?;';
    const data = await db.query(queryText, [request.replayID]);
    const ps = data.map(d => {
        const guild = this.client.guilds.get(config.discord.night_chat_server);
        const channel = guild.channels.get(d.channel_id);
        if(channel)
            return channel.delete();
        return null;
    });
    await Promise.all(ps);

    queryText = 'DELETE FROM discord_night_channels WHERE game_id = ?;';
    return db.query(queryText, [request.replayID]);
};

DiscordWrapper.prototype.onGameEnd = async function(request){
    gameService.deleteGame(request.joinID);
};

DiscordWrapper.prototype.onGameStart = async function(jo){
    const { channelID } = getGameByJoinID(jo.joinID);
    const channel = this.client.channels.get(channelID);
    const everyoneRoleID = channel.guild.defaultRole.id;

    this.stopAutoTag(channelID);

    const moderators = jo.moderators
        .map(user => user.id)
        .filter(getDiscordIDByUserID)
        .map(getDiscordIDByUserID);

    const message = mappers.onGameStartMessage(moderators, channelID);
    discordJsonClient.sendMessage(message);

    await editedChannelsRepo.save(channelID);

    await removeChatPermission(channel, everyoneRoleID);

    if(jo.dayStart){
        const ps = jo.living.filter(getDiscordIDByUserID)
            .map(getDiscordIDByUserID)
            .map(discordID => addChatPermission(channel, discordID));
        await Promise.all(ps);
    }
    return this.cleanupSubscriptionMessages(channelID);
};

DiscordWrapper.prototype.onHostChange = function(){};

DiscordWrapper.prototype.onNewPlayer = async function(request){
    const game = getGameByJoinID(request.joinID);
    const message = mappers.onPlayerJoinMessage(request, game);
    discordJsonClient.sendMessage(message);
};

DiscordWrapper.prototype.onNewVote = async function(request){
    const game = getGameByJoinID(request.joinID);
    const message = mappers.onNewVoteMessage(request, game);
    discordJsonClient.sendMessage(message);
};

DiscordWrapper.prototype.onNightStart = async function(request){
    const dGame = getGameByJoinID(request.joinID);
    const { channelID } = dGame;
    dGame.isStarted = true;
    dGame.isDay = false;

    let canSpeak;
    if(request.moderatorID)
        canSpeak = [request.moderatorID];
    else
        canSpeak = [];
    this.setActiveChannelTalkingStatus(channelID, canSpeak);

    this.refreshChatPermissions(request);
    const peopleToPrompt = this.saveAllowedPlayerCommands(request);
    for(let i = 0; i < peopleToPrompt.length; i++)
        discordJsonClient.sendDirectMessage(peopleToPrompt[i],
            'Submit your night action!  '
            + 'To see your available night actions use '
            + `**${dGame.channelCommand}commands**.`);

    const votes = await voteService.getVotes(request.joinID);
    const game = await gameService.getByJoinID(request.joinID);
    let message = mappers.onDayEndMessage(votes, game, dGame);
    discordJsonClient.sendMessage(message);

    request.graveYard.forEach(dead => {
        message = mappers.onDayEndDeathMessage(dead, channelID);
        discordJsonClient.sendMessage(message);
        if(!dead.lastWill)
            return;
        const playerMention = getMention(dead.userID, dead.playerName);
        discordJsonClient.sendPublicMessage(channelID,
            `${playerMention} left a last will:\n*${dead.lastWill}*`);
    });

    const nightEnd = new Date().getTime() + (request.length * 60 * 1000);

    const discordMessage = await discordJsonClient.sendPublicMessage(channelID, request.broadcast);
    nightChangingText(nightEnd, discordMessage, request.broadcast, true, channelID);

    this.onPhaseDeath(channelID, request.graveYard);
};

DiscordWrapper.prototype.onPhaseReset = function(){ // param: request

};

DiscordWrapper.prototype.onPhaseDeath = function(channelID, graveYard){
    if(!graveYard)
        return;

    const channel = this.client.channels.get(channelID);

    const ps = [];

    for(let j = 0; j < graveYard.length; j++){
        const dID = getDiscordIDByUserID(graveYard[j].userID);
        if(dID)
            ps.push(removeChatPermission(channel, dID));
    }

    return Promise.all(ps).catch(helpers.log);
};

DiscordWrapper.prototype.onSetupChange = function(){};

DiscordWrapper.prototype.onSetupHiddenAdd = function(){};

DiscordWrapper.prototype.onSetupHiddenRemove = function(){};

DiscordWrapper.prototype.onTrialStart = async function(request){
    this.saveAllowedPlayerCommands(request);
    const { channelID } = getGameByJoinID(request.joinID);
    const game = await gameService.getByJoinID(request.joinID);
    const userIDToDiscordIDMap = {};
    request.trialedUsers.forEach(user => {
        userIDToDiscordIDMap[user.userID] = getDiscordIDByUserID(user.userID);
    });
    const votes = await voteService.getVotes(request.joinID);
    const message = mappers.onTrialStart(request, game, channelID, userIDToDiscordIDMap, votes);
    discordJsonClient.sendMessage(message);
};

DiscordWrapper.prototype.onVotePhaseStart = async function(request){
    this.saveAllowedPlayerCommands(request);
    const dGame = getGameByJoinID(request.joinID);
    const game = await gameService.getByJoinID(request.joinID);
    const votes = await voteService.getVotes(request.joinID);
    const message = mappers.onVotePhaseStart(request, votes, game, dGame);
    discordJsonClient.sendMessage(message);
};

DiscordWrapper.prototype.onVotePhaseReset = async function(request){
    const dGame = getGameByJoinID(request.joinID);
    const game = await gameService.getByJoinID(request.joinID);
    const message = mappers.onVotePhaseReset(request, game, dGame);
    discordJsonClient.sendMessage(message);
};

module.exports = {
    addDiscordGame,
    addUserToGame,

    cleanupPlayer,

    getDiscordIDByUserID,
    getUserIDByDiscordID,
    getGameByChannelID,
    getGameByDiscordID,

    getPlayerCommandsByExternalID,
    tryDayMessage,

    DiscordWrapper,
};

function addDiscordGame(m, joinID, userID){
    const externalGame = new DiscordInstance(m.channelID, joinID, userID, m.channelCommand);

    channelIDToGame[m.channelID] = externalGame;
    joinIDToGame[joinID] = externalGame;
    addUserToGame(userID, m.discordID, joinID);
    return externalGame;
}

function addUserToGame(userID, discordID, joinID){
    discordIDToUserID[discordID] = userID;
    userIDToDiscordID[userID] = discordID;
    discordIDToGame[discordID] = joinIDToGame[joinID];
}

function getGameByChannelID(channelID){
    return channelIDToGame[channelID];
}

function getGameByUserID(userID){
    const discordID = getDiscordIDByUserID(userID);
    return getGameByDiscordID(discordID);
}

function getGameByJoinID(joinID){
    return joinIDToGame[joinID];
}

function getGameByDiscordID(discordID, defaultValue){
    return discordIDToGame[discordID] || defaultValue;
}

function getDiscordIDByUserID(userID){
    return userIDToDiscordID[userID.toString()];
}

function getUserIDByDiscordID(discordID){
    return discordIDToUserID[discordID];
}

function getPlayerCommandsByExternalID(discordID){
    return allowedPlayerCommands[discordID];
}
