module.exports = {
    echo: 'echo',
    help: 'help',
    commands: 'commands',
    status: 'status',
    info: 'info',
    invite: 'invite',
    changecommand: 'changecommand',

    host: 'host',
    in: 'in',
    play: 'play',
    join: 'join',

    out: 'out',
    exit: 'exit',
    quit: 'quit',
    leave: 'leave',

    subscribe: 'subscribe',
    unsubscribe: 'unsubscribe',

    autotag: 'autotag',

    name: 'name',
    prefer: 'prefer',
    setups: 'setups',
    setup: 'setup',
    moderate: 'moderate',

    nightlength: 'nightLength',
    daylength: 'dayLength',
    day: 'day',
    night: 'night',
    daystart: 'dayStart',
    nightstart: 'nightStart',
    punch: 'punch',
    ita: 'ita',

    start: 'start',

    lw: 'lw',
    votes: 'votecount',
    votecount: 'voteCount',
    vc: 'vc',
    living: 'living',
    alive: 'alive',

    stats: 'stats',
    history: 'history',
    leaderboard: 'leaderboard',

    skip: 'skip',
    end: 'end',
    usage: 'usage',
};
