const messageService = require('../../discordServices/messageService');

const channelCommandsRepo = require('../../discordRepos/discordChannelCommandsRepo');

const discordClient = require('../../discordWrapper');
const discordJsonClient = require('../../discordJsonClient');
const discordUserClient = require('../../discordUserClient');

const helpers = require('../../../../utils/helpers');

const Constants = require('../../Constants');


const ALLOWED_NOT_JOINED_COMMANDS = [
    Constants.setup,
    Constants.commands,
    Constants.help,
    Constants.echo,
    Constants.info,
    Constants.status,
    Constants.host,
    Constants.join,
    Constants.play,
    Constants.in,
    Constants.autotag,
    Constants.setups,
    Constants.changecommand,
    Constants.stats,
    Constants.history,
    Constants.leaderboard,
    Constants.subscribe,
    Constants.unsubscribe,
    Constants.invite,
];

const SETUP_COMMANDS = [
    Constants.name,
    Constants.prefer,
    Constants.nightlength.toLowerCase(),
    Constants.daylength.toLowerCase(),
    Constants.day,
    Constants.night,
    Constants.daystart.toLowerCase(),
    Constants.nightstart.toLowerCase(),
    Constants.moderate,
];


async function eventToLogic(m){
    const channelCommand = await getChannelCommand(m);
    const text = m.content;
    if(text === channelCommand)
        return;
    const guildID = m.channel.type === 'text' ? m.channel.guild.id : null;
    m = {
        args: m.content,
        channelCommand,
        channelID: getChannelID(m),
        discordID: m.author.id,
        externalID: m.id,
        guildID,
        isDM: m.channel.type !== 'text',
        originChannelID: m.channel.id,
        raw: m,
    };
    if(!m.isDM && !text.startsWith(channelCommand))
        return discordClient.tryDayMessage(m);

    let starter;
    if(text.startsWith(channelCommand))
        starter = channelCommand.length;
    else
        starter = 0;

    let ind = text.indexOf(' ');
    let command;
    if(ind === -1)
        ind = text.length;
    command = text.substring(starter, ind).toLowerCase();
    if(command === 'end')// accounts for end phase
        command = text.substring(starter, text.length).toLowerCase();

    // function names must be lowercase
    if(Constants[command]){
        const game = discordClient.getGameByDiscordID(m.discordID);
        if(!game && ALLOWED_NOT_JOINED_COMMANDS.indexOf(command) === -1)
            return m.raw.reply('You must be part of the game to submit this command.')
                .catch(helpers.log);
        if(SETUP_COMMANDS.includes(command) && game && game.isStarted)
            return m.raw.reply("You can't edit game settings after game start.");
        m.args = text.substring(command.length + starter + 1, text.length);
        try{
            await discordUserClient[command](m);
        }catch(err){
            if(err.userErrorText)
                m.raw.reply(err.userErrorText);
            else
                helpers.log(err, 'Unhandled discord exception on user interaction');
        }
        return m;
    }

    if(!m.isDM)
        await messageService.deleteMessage(m.raw);

    const allowedCommands = discordClient.getPlayerCommandsByExternalID(m.discordID);
    if(!allowedCommands || !allowedCommands[command]){
        await unknownCommandPrompt(m, command);
        return m;
    }

    if(!allowedCommands[command].publicCommand && !m.isDM){
        await unknownCommandPrompt(m, command);
        return m;
    }

    let rem = text.substring(starter, text.length);
    if(m.isDM)
        rem = rem.replace('@', '').replace(/#[0-9]*/, '');

    rem = stripDiscordMentions(rem);

    try{
        await discordUserClient.instanceCommand(m, rem);
    }catch(err){
        if(err.userErrorText)
            m.raw.reply(err.userErrorText);
        else
            helpers.log(err, 'Unhandled discord exception on user interaction');
    }

    return m;
}

module.exports = {
    eventToLogic,
};

function stripDiscordMentions(rem){
    const start = rem.indexOf('<@');
    if(start === -1)
        return rem;
    const end = rem.indexOf('>', start);
    if(end === -1)
        return rem;

    let discordID = rem.substring(start + 2, end);
    if(discordID.startsWith('!'))
        discordID = discordID.substring(1, discordID.length);
    const userID = discordClient.getUserIDByDiscordID(discordID);
    if(!userID)
        return rem;

    const search = rem.substring(start, end + 1);
    return rem.replace(new RegExp(search, 'g'), userID.toString());
}

async function getChannelCommand(m){
    const channelID = getChannelID(m);
    if(!channelID)
        return null;
    const game = discordClient.getGameByDiscordID(channelID);
    if(game)
        return game.channelCommand;
    return (await channelCommandsRepo.get(channelID)) || '!';
}

function getChannelID(m){
    if(m.channel.type === 'text')
        return m.channel.id;
    try{
        return discordClient.getGameByDiscordID(m.author.id).channelID;
    }catch(err){
        return null;
    }
}

function unknownCommandPrompt(m, command){
    const { channelCommand } = m;
    const message = `Unknown command '${command}'! Use **${channelCommand}`
        + 'commands** to see available commands.';
    return discordJsonClient.sendPublicMessage(m.channelID, message);
}
