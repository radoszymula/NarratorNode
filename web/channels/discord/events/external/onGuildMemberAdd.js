const config = require('../../../../../config');

const helpers = require('../../../../utils/helpers');

function eventToLogic(clientObj, gMember){
    if(gMember.guild.id !== config.discord.night_chat_server)
        return;

    const ps = [];
    let p;
    const memberID = gMember.id;

    const channelConfig = clientObj.afterGuildMemberAdd[memberID];
    if(!channelConfig){
        helpers.log('channelConfig was null: ', memberID);
        return;
    }
    const channelsToAdd = Object.keys(channelConfig);
    channelsToAdd.forEach(cID => {
        p = clientObj.client.channels.get(cID);
        if(!p){
            helpers.log('guild member channel was null', cID);
            return;
        }
        p = p.overwritePermissions(memberID, channelConfig[cID]);
        ps.push(p);
    });

    Promise.all(ps).catch(helpers.log);

    delete clientObj.afterGuildMemberAdd[memberID];
}

module.exports = {
    eventToLogic,
};
