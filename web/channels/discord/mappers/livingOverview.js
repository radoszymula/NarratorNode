const mapperUtils = require('./mapperUtils');


function getMessage(game){
    let playerOutput = '';
    game.players.forEach((x, index) => {
        if(!isDead(game, x.name))
            playerOutput += `**[${index + 1}]** ${x.name}\n`;
    });
    const output = {
        fields: [{
            name: '**The Living**',
            value: playerOutput,
            inline: false,
        }],
    };
    return mapperUtils.wrapEmbed(output);
}

module.exports = {
    getMessage,
};

function isDead(game, name){
    for(let i = 0; i < game.graveyard.length; i++){
        const deadPlayer = game.graveyard[i];
        if(deadPlayer.name === name)
            return true;
    }
    return false;
}
