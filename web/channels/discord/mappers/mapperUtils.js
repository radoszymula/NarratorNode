function wrapEmbed(input){
    input.footer = { text: 'Hosted by The Narrator' };
    return { embed: input };
}

module.exports = {
    wrapEmbed,
};
