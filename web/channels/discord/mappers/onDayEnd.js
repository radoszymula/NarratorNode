function getMessage(votes, game, dGame){
    const voteOverviewMapper = require('./voteOverview');
    return {
        channelID: dGame.channelID,
        message: voteOverviewMapper.getMessage(votes, game, dGame.channelID).message,
    };
}

module.exports = {
    getMessage,
};
