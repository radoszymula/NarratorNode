function getMessage(request, game, dGame){
    const voteOverviewMapper = require('./voteOverview');
    const voteCount = {
        channelID: dGame.channelID,
        message: voteOverviewMapper.getMessage(request.votes, game, dGame.channelID).message,
    };

    let message = 'There is no vote consensus.  Votes have been reset.';
    if(request.resetsRemaining === 1)
        message += '  This is your last chance before everyone dies.';
    else
        message += `  You have ${request.resetsRemaining} more chances before everyone dies.`;

    return [voteCount, {
        channelID: dGame.channelID,
        message,
    }];
}

module.exports = {
    getMessage,
};
