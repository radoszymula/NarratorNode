function getMessage(request, game){
    return {
        channelID: game.channelID,
        message: 'Game canceled.',
    };
}

module.exports = {
    getMessage,
};
