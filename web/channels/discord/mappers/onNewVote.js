function getMessage(request, game){
    const wrapper = require('../discordWrapper');
    let messages = [];
    if(!request.settings.secretVoting)
        messages.push({
            channelID: game.channelID,
            message: request.voteText,
        });
    else if(request.voter)
        messages.push({
            discordID: wrapper.getDiscordIDByUserID(request.voter.id),
            message: request.voteText,
        });

    const moderatorPMs = request.moderators
        .filter(user => wrapper.getDiscordIDByUserID(user.id))
        .map(user => ({
            discordID: wrapper.getDiscordIDByUserID(user.id),
            message: request.voteText,
        }));
    if(request.settings.secretVoting){
        moderatorPMs.push(messages[0]);
        messages = moderatorPMs;
    }

    return messages;
}

module.exports = {
    getMessage,
};
