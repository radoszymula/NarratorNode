function getMessage(moderatorDiscordIDs, channelID){
    if(!moderatorDiscordIDs.length)
        return [];
    return {
        channelID,
        message: `<@!${moderatorDiscordIDs[0]}>'s game has begun!`,
    };
}

module.exports = {
    getMessage,
};
