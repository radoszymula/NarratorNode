function getMessage(request, game){
    return {
        channelID: game.channelID,
        message: request.pmContents,
    };
}

module.exports = {
    getMessage,
};
