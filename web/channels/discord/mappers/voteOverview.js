function getMessage(votes, game, requesterName, channelID, requesterDiscordID){
    const hiddenVotes = isHiddenVotes(votes, game, requesterDiscordID);
    const cVotes = votes.currentVotes;
    const wVotes = {};
    const voters = Object.keys(cVotes);
    const deadNames = game.graveyard.map(player => player.name);
    voters.sort();
    for(let i = 0; i < voters.length; i++){
        const voter = voters[i];
        let target = cVotes[voter];
        if(!target)// not voting anyone
            target = 'Not Voting';

        if(!wVotes[target])
            wVotes[target] = [];

        if(!hiddenVotes || voter === requesterName)
            wVotes[target].push(voter);
    }
    game.players.map(player => player.name)
        .filter(playerName => !deadNames.includes(playerName))
        .filter(playerName => !wVotes[playerName])
        .forEach(playerName => {
            wVotes[playerName] = [];
        });

    const playerNames = game.players.map(player => player.name);
    const voted = Object.keys(wVotes);
    const fields = voted.filter(target => {
        if(target === 'Skip Day')
            return true;
        if(target === 'Not Voting')
            return true;
        return votes.allowedTargets.includes(target);
    }).map(target => {
        const value = getVoteSubListEndingText(target, voters, votes.settings, wVotes);
        if(target !== 'Skip Day' && target !== 'Not Voting')
            target = `[${playerNames.indexOf(target) + 1}] ${target}`;
        return {
            name: `**${target}**`,
            value,
            inline: target !== 'Not Voting',
        };
    }).filter(field => field.value);
    fields.sort(voteListSorter);

    const response = {
        message: {
            embed: {
                description: '**Vote Counts**',
                fields,
                footer: {
                    text: 'Hosted by The Narrator',
                },
            },
        },
    };
    if(requesterDiscordID)
        response.discordID = requesterDiscordID;
    else
        response.channelID = channelID;
    return response;
}

module.exports = {
    getMessage,
};

function getVoteSubListEndingText(target, voters, voteSettings, votes){
    let targetVoters = votes[target];
    targetVoters.sort();
    const value = targetVoters.join('\n');
    if(target === 'Not Voting')
        return value;
    targetVoters = votes[target];
    if(voteSettings.voteSystemType !== 'diminishing_pool'){
        const voteThreshhold = Math.floor(voters.length / 2) + 1;
        return `${value}\n**L - ${voteThreshhold - targetVoters.length}**`;
    }
    if(targetVoters.length)
        return value;
    return `${value}*No votes*`;
}

function voteListSorter(x, y){
    if(x.name === '**Not Voting**')
        return 1;
    if(y.name === '**Not Voting**')
        return -1;
    return y.value.split('\n').length - x.value.split('\n').length;
}

function isHiddenVotes(votes, game, requesterDiscordID){
    if(!votes.settings.secretVoting)
        return false;
    if(!requesterDiscordID)
        return false;
    const wrapper = require('../discordWrapper');
    const requesterUserID = wrapper.getUserIDByDiscordID(requesterDiscordID);
    return !game.moderators.map(moderator => moderator.id).includes(requesterUserID);
}
