const config = require('../../../../config');


const url = `<http://${config.discord.app_url}/storyRecap/`;

function getMessage(results, discordName, game, discordID){
    if(!results.length)
        return 'No games found';

    let message = `**${discordName}'s Game History**`;
    results.forEach(result => {
        result = `\n**${result.roleName}** - ${url}${result.game_id}>`;
        if(message.length + result.length < 2000)
            message += result;
    });

    const request = {
        message,
    };
    if(game && game.isStarted)
        request.discordID = discordID;
    else
        request.channelID = game.channelID;
    return message;
}

module.exports = {
    getMessage,
};
