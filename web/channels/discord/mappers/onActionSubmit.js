function getMessage(request, discordID){
    return {
        discordID,
        message: request.feedbackText,
    };
}

module.exports = {
    getMessage,
};
