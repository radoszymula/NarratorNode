function getMessage(request, votes, game, dGame){
    const voteOverviewMapper = require('./voteOverview');
    const m1 = {
        channelID: dGame.channelID,
        message: voteOverviewMapper.getMessage(votes, game, dGame.channelID).message,
    };


    const exampleDiscordID = getExampleDiscordID(request);
    let message = `Voting polls are now open and will close in **${request.dayLength}** minutes.`;
    message += `\nTo vote, type ${dGame.channelCommand}vote <mention player>.\n`;
    if(exampleDiscordID)
        message += `For example, !vote <@!${exampleDiscordID}>\n`;
    message += 'Don\'t worry, your vote will be hidden.';
    const m2 = {
        channelID: dGame.channelID,
        message,
    };
    return [m1, m2];
}

module.exports = {
    getMessage,
};

function getExampleDiscordID(request){
    const wrapper = require('../discordWrapper');
    const userIDs = Object.keys(request.playerCommands);

    // get the moderator to be first
    userIDs.sort((x, y) => {
        if(request.playerCommands[x].vote)
            return 1;
        if(request.playerCommands[y].vote)
            return -1;
        return 0;
    });

    for(let i = 0; i < userIDs.length; i++){
        const discordID = wrapper.getDiscordIDByUserID(userIDs[i]);
        if(discordID)
            return discordID;
    }
    return null;
}
