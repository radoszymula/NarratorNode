const colorUtils = require('../utils/colorUtils');
const setHelpers = require('../../../utils/setHelpers');


function getMessage(game, requesterUserID){
    let description;
    if(game.isStarted)
        description = 'Game in progress';
    else
        description = 'Waiting for game to start';

    const showRoles = isModeratorRequest(game, requesterUserID);

    const output = {
        title: `#${game.joinID} Status`,
        description,

        fields: [],

        footer: {
            text: 'Hosted by The Narrator',
        },
    };

    let rolesList = '';
    for(let i = 0; i < game.setup.setupHiddens.length; i++){
        const setupHidden = game.setup.setupHiddens[i];
        if(i)
            rolesList += '\n';
        rolesList += getSetupHiddenEntry(setupHidden, game.setup);
    }
    if(!rolesList)
        rolesList = '**No roles added**';
    output.fields.push({
        name: 'Roles List',
        value: rolesList,
        inline: true,
    });

    if(game.isStarted){
        output.fields.push({
            name: 'Player List',
            value: getPlayerList(game, showRoles),
            inline: true,
        });
    }else{
        const lobbyList = game.players
            .map(player => player.name)
            .join('\n');
        let min = game.setup.minPlayerCount;
        let max = game.setup.maxPlayerCount;
        const playerCount = game.players.length;
        let flourish = '';
        if(playerCount < min){
            min -= playerCount;
            flourish = `\n*${min} more to start*`;
        }else if(playerCount !== max){
            max -= playerCount;
            flourish = `\n-*You may add ${max} more players*`;
        }
        output.fields.push({
            name: 'Player Lobby',
            value: lobbyList + flourish,
            inline: true,
        });

        output.fields.push({
            name: 'Setup',
            value: game.setup.name,
            inline: true,
        });
    }
    return { embed: output };
}

function getPlayerList(game, showRoles){
    const players = game.players.map((player, index) => {
        const p = {
            name: player.name,
            index: index + 1,
        };
        if(showRoles){
            p.color = player.factionRole.team.color;
            p.roleName = player.factionRole.name;
        }else if(isDead(game, player.name)){
            p.color = player.flip.color;
            p.roleName = player.flip.name;
        }
        return p;
    });

    players.sort((x, y) => {
        if(x.color && y.color)
            return x.color.localeCompare(y.color);
        if(x.color)
            return 1;
        if(y.color)
            return -1;
        return x.name.toLowerCase().localeCompare(y.name.toLowerCase());
    });

    let ret = '';
    for(let i = 0; i < players.length; i++){
        const p = players[i];
        if(p.color)
            ret += colorUtils.getSymbolByColor(p.color);
        else
            ret += `**[${p.index}]**`;
        ret += ` ${p.name}`;
        if(p.roleName)
            ret += ` - *${p.roleName}*`;
        ret += '\n';
    }

    return ret;
}

function isDead(game, name){
    let d;
    for(let i = 0; i < game.graveyard.length; i++){
        d = game.graveyard[i];
        if(d.name === name)
            return true;
    }
    return false;
}

function getSetupHiddenEntry(setupHidden, setup){
    const hiddenObj = setup.hiddens.filter(hidden => hidden.id === setupHidden.hiddenID)[0];
    const hiddenFactionRoleIDSet = new Set(hiddenObj.factionRoleIDs);
    const factions = setup.factions
        .filter(faction => {
            const factionFactionRoleIDs = new Set(faction.factionRoles
                .map(factionRole => factionRole.id));
            return setHelpers.intersection(factionFactionRoleIDs, hiddenFactionRoleIDSet).size;
        });
    const colors = factions.map(faction => faction.color);
    const color = colors.length === 1 ? colors[0] : null;
    const symbol = colorUtils.getSymbolByColor(color);
    return `${symbol} ${hiddenObj.name}`;
}

function isModeratorRequest(game, requesterUserID){
    return requesterUserID && game.moderators.filter(user => requesterUserID === user.id).length;
}

module.exports = {
    getMessage,
};
