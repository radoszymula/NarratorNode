function getMessage(request, game, channelID, trialedUsersMap, votes){
    if(request.trialedUsers[0].userID === request.trialedUser.userID)
        return firstRoundTrial(request, game, channelID, trialedUsersMap, votes);
    return notFirstRoundTrial(request, channelID, trialedUsersMap);
}

module.exports = {
    getMessage,
};

function firstRoundTrial(request, game, channelID, trialedUsersMap, votes){
    const voteOverviewMapper = require('./voteOverview');
    const endOfDayVoteCount = voteOverviewMapper.getMessage(votes, game, channelID).message;

    let trialedUsersMessage = 'The voting round has concluded.\n';
    const trialedUserTag = getPlayerTag(request.trialedUsers[0], trialedUsersMap);
    if(request.trialedUsers.length === 1){
        trialedUsersMessage += `${trialedUserTag} will now give their defense.`;
    }else{
        trialedUsersMessage += `${trialedUserTag} will give their defense first, followed by `;
        trialedUsersMessage += request.trialedUsers
            .filter((_, index) => index)
            .map(user => getPlayerTag(user, trialedUsersMap)).join(', ');
        trialedUsersMessage += '.';
    }

    const messages = [endOfDayVoteCount, trialedUsersMessage];

    return messages.map(message => ({
        channelID,
        message,
    }));
}

function notFirstRoundTrial(request, channelID, trialedUsersMap){
    const trialedUserTag = getPlayerTag(request.trialedUser, trialedUsersMap);
    return {
        channelID,
        message: `${trialedUserTag} is now on trial for crimes against humanity.`,
    };
}

function getPlayerTag(trialedUser, userDiscordMap){
    const discordID = userDiscordMap[trialedUser.userID];
    if(discordID)
        return `<@!${discordID}>`;
    return trialedUser.name;
}
