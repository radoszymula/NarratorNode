function getMessage(request, game){
    return {
        channelID: game.channelID,
        message: request.contents,
    };
}

module.exports = {
    getMessage,
};
