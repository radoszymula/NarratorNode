function getMessage(request, game){
    return {
        channelID: game.channelID,
        message: `${request.playerName} has joined the lobby.`,
    };
}

module.exports = {
    getMessage,
};
