const stringSimilarity = require('string-similarity');

const config = require('../../../config');
const helpers = require('../../utils/helpers');

const javaRequests = require('../../utils/javaRequests');

const historyRepo = require('../../repos/historyRepo');
const userIntegrationsRepo = require('../../repos/userIntegrationsRepo');

const actionService = require('../../services/actionService');
const gameIntegrationService = require('../../services/gameIntegrationService');
const gameService = require('../../services/gameService');
const moderatorService = require('../../services/moderatorService');
const playerService = require('../../services/playerService');
const setupModifierService = require('../../services/setupModifierService');
const setupService = require('../../services/setupService');
const userService = require('../../services/userService');
const voteService = require('../../services/voteService');

const integrationTypes = require('../../models/enums/integrationTypes');
const userTypes = require('../../models/enums/userTypes');

const discordJsonClient = require('./discordJsonClient');
const discordClient = require('./discordWrapper');

const messageService = require('./discordServices/messageService');

const channelCommandsRepo = require('./discordRepos/discordChannelCommandsRepo');
const subscribersRepo = require('./discordRepos/discordSubscribersRepo');

const mappers = require('./mappers/mappers');


function echo(m){
    if(m.isDM)
        return discordJsonClient.sendDirectMessage(m.discordID, m.args);
    return discordJsonClient.sendPublicMessage(m.channelID, m.args);
}

async function help(m){
    const prefixCharacter = await channelCommandsRepo.get(m.channelID);
    const prefix = `- ** ${prefixCharacter}`;

    let output = '*This bot uses a web interface as well as discord to host the game. '
        + 'When you join, the bot will DM you a link - click the link after game start, '
        + 'and you can do night actions or chat there instead. '
        + 'You can also DM the bot your night actions in discord '
        + 'if you do not wish to use the web interface.*\n\n';

    output += 'To quickly start a game:\n';
    output += `${prefix}in** to create/join a game\n`;
    output += `${prefix}setup setupName>** to pick a setup *ex: !setup mash*\n`;
    output += `${prefix}daylength 3** to set the length of the day *!nightlength for night*\n`;
    output += `${prefix}start** to start, duH\n\n`;

    output += 'In game\n';
    output += `${prefix}vote** to vote people\n`;
    output += `${prefix}commands** as a pm *to the bot*`
        + ' to get a list of all your available actions\n\n';

    output += 'Lastly\n';
    output += `${prefix}commands** to see more available commands\n`;
    output += `${prefix}subscribe** in a channel to get notified when a game's starting!`;

    return discordJsonClient.sendPublicMessage(m.originChannelID, output);
}

async function commands(m){
    const prefix = `**${m.channelCommand}`;
    if(m.args)
        m.args = m.args.toLowerCase();

    let output = '__Bot commands__\n';
    // if(m.args === 'autotag'){
    //     prefix = `${prefix}autotag `;

    //     output += `${prefix}on** - Turns on autotag or adds your available tags\n`;
    //     output += `${prefix}off** - Turns off your autotag contribution\n`;
    //     output += `${prefix}ignorehost** - Blocks hosts from autotagging you ever again\n`;
    //     output += `${prefix}dnd** - Blocks autotags\n`;
    //     output += `${prefix}enable** - Removes name from Do not Disturb list\n`;
    //     return reply(m, output);
    // }


    const channel = m.channelID;
    const discordGame = discordClient.getGameByDiscordID(m.discordID);

    if(discordGame){
        if(!discordGame.isStarted){ // game not started
            // output += prefix + "autotag help** - Displayes possible autotag commands\n";
            output += `${prefix}dayLength <number> ** - Changes day length\n`;
            output += `${prefix}dayStart** - Makes game start at day\n`;
            output += `${prefix}leave** - Leaves current game\n`;
            output += `${prefix}name <newName> ** - Changes your game name to the one specified\n`;
            output += `${prefix}nightLength <number> ** - Changes night length\n`;
            output += `${prefix}nightStart** - Makes game start at night\n`;
            output += `${prefix}skip <on/off>** - Sets whether a day can be skipped\n`;
            output += `${prefix}moderate <on/off>** - `
                + 'Sets whether the host is participating in the game\n';
            output += `${prefix}punch <on/off> ** - Enables daytime punches/ITA\n`;
            output += `${prefix}setup <name>** - Changes current setup\n`;
            output += `${prefix}setup** displays available auto-setups\n`;
            output += `${prefix}start** - Starts game\n`;
        }else{
            const playerCommands = discordClient.getPlayerCommandsByExternalID(m.discordID) || {};
            const commandParts = Object.keys(playerCommands); let
                command;
            let added = false;
            for(let i = 0; i < commandParts.length; i++){
                command = playerCommands[commandParts[i]];

                if(command.publicCommand || m.isDM){
                    output += `${prefix + commandParts[i]}** - ${command.description}\n`;
                    added = true;
                }
            }
            if(discordGame.isDay)
                output += `${prefix}voteCount** - Lists current vote counts\n`;
            output += `${prefix}living** - Lists living people\n`;

            if(added)
                output += '\n**To see an example** of command usage, '
                    + `use ${prefix}usage *command***`;
        }
    }else{
        output += `${prefix}info** - Displays current game status\n`;
        output += `${prefix}invite** - Instructions on inviting this bot to your server\n`;
        output += `${prefix}changeCommand <command>** - changes the command prefix for the bot\n`;
        output += `${prefix}stats** - view your basic stats with Narrator\n`;
        output += `${prefix}leaderboard** - view the global leaderboard within Narrator\n`;
        if(!m.isDM){
            output += `${prefix}subscribe** - subscribe to this channel's started games\n`;
            output += `${prefix}unsubscribe** - stop being notified of channel's started games\n`;
        }
        if(!discordClient.getGameByChannelID(channel))
            output += `${prefix}host** - Hosts a new game in the current channel\n`;
        else
            output += `${prefix}join** - Joins current game\n`;
    }

    discordJsonClient.sendPublicMessage(m.originChannelID, output);
}

async function info(m){
    const dGame = discordClient.getGameByChannelID(m.channelID);
    if(!dGame)
        throw userError('No active games!');

    const game = await gameService.getByJoinID(dGame.joinID);
    const userID = m.isDM && discordClient.getUserIDByDiscordID(m.discordID);
    const overviewJson = mappers.setupOverview(game, userID);

    try{
        if(game.isStarted)
            await discordJsonClient.sendDirectMessage(m.discordID, overviewJson);
        else
            await discordJsonClient.sendPublicMessage(m.channelID, overviewJson);
    }catch(err){
        helpers.log(err);
    }
}

async function invite(m){
    const inviteURL = await discordJsonClient.getInviteURL(config.discord.home_channel_id);
    let output = '**Invite Instructions**\n'
        + 'You can find the link and _much needed setup instructions_ '
        + 'on how to invite me to your discord group here:\n';
    output += 'https://github.com/vossnarrator/NarratorNode/blob/master/README.md\n';
    output += 'If you find any bugs or have questions, feel free to visit the home channel:\n';
    output += inviteURL;

    return discordJsonClient.sendPublicMessage(m.channelID, output);
}

function changeCommand(m){
    const command = m.args;
    if(!command)
        throw userError(`You need to specify a prefix to use this! "
            + "For example **${m.channelCommand}changeCommand !** "
            + "or **${m.channelCommand}changeCommand w.in**`);
    if(command.length > 2)
        throw userError('Command prefixes longer than 2 characters are currently unsupported');

    if(!m.channelID)
        throw userError("I'm unsure of which channel you want to change the command for");

    const game = discordClient.getGameByChannelID(m.channelID);

    game.channelCommand[m.channelID] = command;
    discordJsonClient.sendPublicMessage(m.channelID, `Command changed to '${command}'.`);
    return channelCommandsRepo.save(m.channelID, command);
}

async function host(m){
    if(m.isDM){ // is private message
        const text = "I can't start games here.  Add me to a channel and '!host' there!";
        throw userError(text);
    }

    let game = discordClient.getGameByChannelID(m.channelID);
    if(game){
        const hostDiscordID = discordClient.getDiscordIDByUserID(game.hostUserID);
        if(m.discordID === hostDiscordID)
            throw userError('You are already hosting a game!');

        const hostName = await discordJsonClient.getName(hostDiscordID, m.channelID);
        throw userError(`${hostName} is already hosting a game.`);
    }

    if(discordClient.getGameByDiscordID(m.discordID))
        throw userError("You're already in a game!");

    const user = await userService.getByExternalID(false, m.discordID, userTypes.DISCORD);
    const tempAuthToken = userService.addTempAuth(user.id);

    const rules = {
        CHAT_ROLES: false,
        HOST_VOTING: false,
        NIGHT_LENGTH: 60, // seconds
    };
    const nickname = await discordJsonClient.getName(m.discordID, m.channelID);
    const setupID = null;
    game = await gameService.create(user.id, nickname, 'classic', setupID, rules);

    gameIntegrationService.register(game.joinID, integrationTypes.DISCORD);
    const externalGame = discordClient.addDiscordGame(m, game.joinID, user.id);

    discordJsonClient.sendPublicMessage(m.channelID, `A game is being started by ${nickname}.`);
    const message = mappers.newPlayerPmMessage(tempAuthToken, game);
    discordJsonClient.sendDirectMessage(m.discordID, message);

    const url = await discordJsonClient.getInviteURL(m.channelID);

    const subscribers = (await subscribersRepo.getByGuildID(m.guildID))
        .filter(userID => userID !== user.id); // filters out the host

    const promises = subscribers.map(async userID => {
        const discordID = await userIntegrationsRepo.getDiscordID(userID);
        if(!discordID)
            return;
        const status = await discordJsonClient.getUserStatus(discordID);
        if(status === 'dnd' || status === 'offline')
            return;
        let output = `${nickname} has started a Narrator Mafia Game!`;
        if(url)
            output += `\n${url}`;
        const cleanupMessage = await discordJsonClient.sendDirectMessage(discordID, output);
        externalGame.cleanupMessages.push({
            channelID: cleanupMessage.channel.id,
            messageID: cleanupMessage.id,
        });
    });
    return Promise.all(promises);
}

async function join(m){
    const game = discordClient.getGameByChannelID(m.channelID);
    if(!game)
        return host(m);

    if(m.isDM)
        throw userError(`You can't join games in here.  "
            + "Join a channel where a game is hosted, and then type '${m.channelCommand}join'.`);

    if(discordClient.getGameByDiscordID(m.discordID))
        throw userError('You\'re already in a game!');


    const user = await userService.getByExternalID(false, m.discordID, userTypes.DISCORD);
    const discordName = await discordJsonClient.getName(m.discordID, m.channelID);
    let gameDict;
    try{
        gameDict = await playerService.create(user.id, discordName, game.joinID);
    }catch(errorObj){
        let message = errorObj.errors[0];
        if(message === 'User is already in a game.')
            message = 'You\'re already in a game!';
        throw userError(message);
    }

    discordClient.addUserToGame(user.id, m.discordID, game.joinID);

    game.userIDs.push(user.id);
    const tempAuthToken = userService.addTempAuth(user.id);
    const message = mappers.newPlayerPmMessage(tempAuthToken, gameDict);
    await discordJsonClient.sendDirectMessage(m.discordID, message);
}

async function leave(m){
    const userID = discordClient.getUserIDByDiscordID(m.discordID);
    try{
        await playerService.leave(userID);
    }catch(httpError){
        throw userError(httpError.errors[0]);
    }

    await discordClient.cleanupPlayer(userID);

    const nickname = await discordJsonClient.getName(m.discordID, m.channelID);
    const message = mappers.onPlayerRemove(nickname, m.channelID);
    discordJsonClient.sendMessage(message);
}

async function subscribe(m){
    if(m.isDM)
        throw userError("I'm unsure of what channel you would like to subscribe to!");

    const user = await userService.getByExternalID(false, m.discordID, userTypes.DISCORD);
    await subscribersRepo.insert(m.raw.channel.guild.id, user.id);

    const message = 'I will let you know if any games get created in this channel! '
        + `If you'd like me to stop, use **${m.channelCommand}unsubscribe**.`;
    discordJsonClient.sendPublicMessage(m.channelID, message);
}

async function unsubscribe(m){
    if(m.isDM)
        throw userError("I'm unsure of what channel you would like to unsubscribe from!");

    const user = await userService.getByExternalID(false, m.discordID, userTypes.DISCORD);
    await subscribersRepo.deleteEntry(m.channel.guild.id, user.id);

    discordJsonClient.reply(m, 'You are unsubscribed from games that pop up in this channel.');
}

// eslint-disable-next-line no-unused-vars
function autotag(m){
    throw userError('This feature is temporarily disabled.');
    // let suffix = m.args;
    // if(!suffix || suffix.toLowerCase() === 'help')
    //     return this.commands(m, 'autotag');

    // suffix = suffix.toLowerCase();

    // if(suffix === 'on'){
    //     if(this.channelStartedGame[m.channelID])
    //         return reply(m, "Can't do that right now.");
    //     if(this.isAutotagging(m))
    //         return reply(m, "I'm already autotagging for you.");

    //     var qString = `SELECT 1 FROM discord_last_tagged WHERE timeStamp > DATE_ADD(NOW(), `
    // `INTERVAL 2 HOUR) AND user_id = '${m.discordID}';`;

    //     return db.query(qString)
    //         .then(results => {
    //             if(results.length)
    //                 return reply(m, "You cannot use the autotag if you're
    // "on the _Do not Disturb_ list.");

    //             reply(m, "I'll begin tagging people you've played with before.");

    //             const channelAutoTag = this.autoTagInfo[channelID];
    //             if(!channelAutoTag)
    //                 this.autoTagInfo[channelID] = new AutoTag(this, channelID,
    // m.discordID, narrRequest);
    //             else
    //                 channelAutoTag.addSearcher(m.discordID);
    //         }).catch(helpers.log);
    // }if(suffix === 'off'){
    //     if(this.channelStartedGame[channelID])
    //         return reply(m, "Can't do that right now.");
    //     if(!this.isAutotagging(m))
    //         return reply(m, "I'm already not autotagging anyone for you.");

    //     reply(m, "I'll stop tagging people you've played with before.");
    //     this.autoTagInfo[channelID].removeSearcher(m.discordID);
    // }else if(suffix === 'dnd'){
    //     if(this.isAutotagging(m))
    //         return reply("You cannot use the autotag if you're on the _Do not Disturb_ list.");
    //     reply(m, "I'll put you on the do not disturb list.");

    // return db.query(`INSERT INTO discord_last_tagged (user_id, timestamp) `
    //     + `VALUES ('${m.discordID}', DATE_ADD(NOW(), INTERVAL 2 MONTH)) `
    //     + `ON DUPLICATE KEY UPDATE timestamp = DATE_ADD(NOW(), INTERVAL 2 MONTH);`)
    //         .catch(helpers.log);
    // }else if(suffix === 'enable'){
    //     reply(m, "I'll remove you from the do not disturb list.");

    //     return db.query(`DELETE FROM discord_last_tagged WHERE user_id = '${m.discordID}';`)
    //         .catch(helpers.log);
    // }else if(suffix === 'ignorehost'){
    //     if(this.isAutotagging(m))
    //         return reply(m, "You can't use this if you're currently autotagging people.");
    //     if(!this.autoTagInfo[channelID] || !this.autoTagInfo[channelID].searchers.length)
    //         return reply(m, 'No one is using autotag right now.');

    //     reply(m, "I'll make sure to never tag you if the current auto taggers are searching.");

    //     var qString = 'INSERT INTO discord_autotag_block (blocked, blocker) VALUES ';
    //     this.autoTagInfo[channelID].searchers.forEach((seeker, i) => {
    //         if(i)
    //             qString += ', ';
    //         qString += `('${seeker}', '${m.discordID}')`;
    //     });
    //     db.query(qString).catch(helpers.log);
    // }else{
    // reply(m, `Unknown autotag command.  Use **${m.channelCommand}autotag help** "
    //     + "to see all available autotag commands`);
    // }
}

// DiscordWrapper.prototype.isAutotagging = function(m){
//     if(!this.autoTagInfo[m.channelID])
//         return false;

//     return this.autoTagInfo[m.channelID].isSearcher(m.discordID);
// };

function name(m){
    const game = ensureGameInChannel(m);
    const newName = m.args;
    if(!newName)
        throw userError(`Must specify a name. For example **${m.channelCommand}name IamNotMafia**`);

    if(game.isStarted)
        throw userError("You can't change your name once the game's started!");

    if(newName.indexOf(' ') >= 0)
        throw userError('No spaces in names!');

    const similarity = stringSimilarity.compareTwoStrings(newName, m.member.nickname);
    if(config.discord.word_similarity_threshold > similarity)
        throw userError("That name isn't similar enough to your Discord name.  "
            + "It'll only confuse other people!");


    const userID = discordClient.getUserIDByDiscordID(m.discordID);

    const o = {};
    o.message = 'nameChange';
    o.nameChange = newName;
    o.userID = userID;
    o.discord = true;

    javaRequests.sendInstanceRequest(o);
}

function prefer(m){
    ensureGameInChannel(m);

    const userID = discordClient.getUserIDByDiscordID(m.discordID);
    if(!userID)
        throw userError('Must be in a game to prefer anything!');

    const role = m.args;
    if(!role)
        throw userError(`No role selected! Usage : **${m.channelCommand}prefer Doctor**`);

    if(!m.isDM)
        messageService.delete(m.raw);

    const dObject = {
        message: `say everyone -prefer ${role}`,
        userID,
    };
    javaRequests.sendInstanceRequest(dObject);
}

async function setups(m){
    let setupName = m.args;
    const game = discordClient.getGameByChannelID(m.channelID);

    if(game && game.isStarted && !setupName)
        throw userError("I'll show you other setups once this game completes.");

    if(game && game.isStarted && setupName)
        throw userError('How do you expect me to change the setup midgame?');

    if(!setupName){
        const featuredSetups = await setupService.getFeaturedSetups();
        let output = '**Setup List**:\n';
        for(let i = 0; i < featuredSetups.length; i++)
            if(featuredSetups[i].key === 'mash')
                output += `${i + 1}. *${featuredSetups[i].key}* - **${featuredSetups[i].short}**\n`;
            else
                output += `${i + 1}. *${featuredSetups[i].key}* - ${featuredSetups[i].short}\n`;

        output += 'You may also do **custom** to setup a custom game.  '
            + 'However, you have to use the link the bot sends you to edit the game.';
        discordJsonClient.sendPublicMessage(m.channelID, output);

        output = `**Changing Setups Example**\n${m.channelCommand}setup vanillab\n"`
            + `"${m.channelCommand}setup 3`;
        return discordJsonClient.sendPublicMessage(m.channelID, output);
    }
    if(!discordClient.getGameByDiscordID(m.discordID))
        throw userError('You must be part of the game to submit this command.');
    const ogSetupName = setupName;
    setupName = setupName.replace(/ /g, '').toLowerCase();

    if(setupName === 'custom')
        return setupService.setSetup({
            joinID: game.joinID,
            setupKey: 'custom',
        });

    const featuredSetups = await setupService.getFeaturedSetups();
    for(let i = 0; i < featuredSetups.length; i++)
        if(setupName === featuredSetups[i].key || parseInt(setupName, 10) === (i + 1))
            return setupService.setSetup({ setupKey: featuredSetups[i].key, joinID: game.joinID });

    discordJsonClient.sendPublicMessage(m.channelID, `Unknown setup '${ogSetupName}'`);
}

async function moderate(m){
    const externalGame = ensureGameInChannel(m);

    let rem = m.args;
    if(!rem)
        rem = 'on';
    rem = rem.toLowerCase();
    if(rem !== 'on' && rem !== 'off')
        throw userError(`Example usage for ${m.channelCommand} moderate**\n"
            + "${m.channelCommand}moderate on/off`);

    const userID = discordClient.getUserIDByDiscordID(m.discordID);
    if(userID !== externalGame.hostUserID)
        throw userError('Only hosts can use this command.');

    const discordName = await discordJsonClient.getName(m.discordID, m.channelID);
    if(rem === 'on'){
        await moderatorService.create(userID);
        throw userError(`${discordName} will be moderating this game.`);
    }else{
        await moderatorService.delete(userID, discordName);
        throw userError(`${discordName} will no longer be moderating this game.`);
    }
}

async function nightLength(m){
    const min = parseInt(m.args, 10);
    if(!Number.isInteger(min))
        throw userError(`**Example usage for ${m.channelCommand}nightLength**\n"
            + "${m.channelCommand}nightLength 14`);

    ensureGameInChannel(m);

    await editRules(m, { name: 'NIGHT_LENGTH', value: min * 60 });

    return discordJsonClient.sendPublicMessage(m.channelID,
        `Night length changed to ${min} minutes.`);
}

async function dayLength(m){
    const min = parseInt(m.args, 10);
    if(!Number.isInteger(min))
        throw userError(`**Example usage for ${m.channelCommand}dayLength**\n"
            + "${m.channelCommand}dayLength 13`);

    ensureGameInChannel(m);
    await editRules(m, { name: 'DAY_LENGTH', value: min * 60 });

    return discordJsonClient.sendPublicMessage(m.channelID,
        `Day length changed to ${min} minutes.`);
}

function day(m){
    const rem = m.args;
    if(!rem || rem.toLowerCase() !== 'start')
        throw userError(`**Example usage for ${m.channelCommand}day** : "
            + "${m.channelCommand}day start"`);
    return dayStart(m);
}

async function dayStart(m){
    ensureGameInChannel(m);
    await editRules(m, { name: 'DAY_START', value: true });

    discordJsonClient.sendMessage({
        channelID: m.channelID,
        message: 'Game will now start at daytime.',
    });
}

function night(m){
    const rem = m.args;
    if(!rem || rem.toLowerCase() !== 'start')
        throw userError(`**Example usage for ${m.channelCommand}night** : "
            + "${m.channelCommand}night start`);

    return nightStart(m);
}

async function nightStart(m){
    ensureGameInChannel(m);
    await editRules(m, { name: 'DAY_START', value: false });

    discordJsonClient.sendMessage({
        channelID: m.channelID,
        message: 'Game will now start at nighttime.',
    });
}

async function punch(m){
    const game = ensureGameInChannel(m);
    let rem = m.args;

    if(game.isStarted)
        return instanceCommand(m, `punch ${rem}`);

    if(!rem)
        throw userError(`**Example usage for ${m.channelCommand}punch**\n`
            + `${m.channelCommand}punch on`);

    rem = rem.toLowerCase();
    if(rem !== 'on' && rem !== 'off')
        throw userError("Game hasn't started yet!  Unless you meant to toggle it **on/off**.");

    await editRules(m, { name: 'PUNCH_ALLOWED', value: rem === 'on' });

    const allowedText = 'Day punches are now allowed.';
    const disallowedText = 'Day punches are no longer allowed.';
    const punchReply = rem === 'on' ? allowedText : disallowedText;

    discordJsonClient.sendMessage({
        channelID: m.channelID,
        message: punchReply,
    });
}

async function start(m){
    ensureGameInChannel(m);

    const hostUserID = getHostUserID(m);
    if(!hostUserID)// was deleted
        return;
    try{
        await gameService.start(hostUserID);
    }catch(responseObj){
        throw userError(responseObj.errors[0]);
    }
}

async function lw(m){
    const game = ensureGameInChannel(m);
    let rem = m.args || '';

    if(!game.isStarted){
        rem = rem.toLowerCase();
        if(rem !== 'on' && rem !== 'off')
            throw userError("Game hasn't started yet!  Unless you meant to toggle it **on/off**.");

        await editRules(m, { name: 'LAST_WILL', value: rem === 'on' });

        let lwReply;
        if(rem === 'on')
            lwReply = 'Last wills are now enabled.';
        else
            lwReply = 'Last wills are now disabled.';
        discordJsonClient.sendMessage({
            channelID: m.channelID,
            message: lwReply,
        });
    }

    if(!rem.length && !m.isDM)
        throw userError('I cannot reveal your last will until you die!');
    else if(!rem.length)
        return gameService.getUserState(getHostUserID(m))
            .then(data => {
                if(data.rules.LAST_WILL.val)
                    return m.channel.send(`Your current last will is:\n*${data.lastWill}*`);
            }).catch(helpers.log);


    const userID = discordClient.getDiscordIDByUserID(m.discordID);
    if(!userID)
        return;

    javaRequests.sendInstanceRequest({
        willText: rem,
        message: 'lastWill',
        userID,
    });
}

async function voteCount(m){
    let game = ensureGameInChannel(m);
    if(!game.isDay)
        throw userError("It isn't daytime");

    const userID = discordClient.getUserIDByDiscordID(m.discordID);
    if(!userID)
        return;
    const joinID = game.joinID;
    game = await gameService.getByJoinID(joinID);
    const votes = await voteService.getVotes(joinID);

    const isPublic = !m.isDM && game.moderators.map(moderator => moderator.id).includes(userID);
    const playerName = await discordJsonClient.getName(m.discordID, m.channelID);
    const requestDiscordID = isPublic ? null : m.discordID;
    const message = mappers.voteOverview(votes, game, playerName, m.channelID, requestDiscordID);
    discordJsonClient.sendMessage(message);
}

async function living(m){
    const externalGame = discordClient.getGameByChannelID(m.channelID);
    if(!externalGame)
        throw userError('I can\'t tell you who\'s alive if the game\'s not started.');

    const game = await gameService.getByJoinID(externalGame.joinID);
    const output = mappers.livingOverview(game);

    try{
        if(game.isStarted)
            return discordJsonClient.sendDirectMessage(m.discordID, output);
        discordJsonClient.sendPublicMessage(m.originChannelID, output);
    }catch(err){
        helpers.log(err);
    }
}

// eslint-disable-next-line no-unused-vars
function stats(m){
    throw userError('This feature is temporarily disabled.');
    // narrRequest('personal_stats', m.discordID)
    //     .then(data => {
    //         const output = {
    //             description: `${m.member.nickname}'s stats`,

    //             fields: [],

    //             footer: {
    //                 text: 'Hosted by The Narrator',
    //             },
    //         };

    //         output.fields.push({
    //             name: 'Total Points',
    //             value: data.points,
    //             inline: true,
    //         });

    //         const values = Object.keys(data.roleStats).map(k => data.roleStats[k]);
    //         let bestRole = '';
    //         let stat = null;
    //         let name = 'Best Role';
    //         let totalGames = 0;
    //         values.forEach(v => {
    //             if(!stat){
    //                 stat = parseInt(v.wins, 10) / v.games.length;
    //                 bestRole += `${v.baseName}\n`;
    //             }else if(stat === parseInt(v.wins, 10) / v.games.length){
    //                 bestRole += `${v.baseName}\n`;
    //                 name = 'Best Roles';
    //             }else if(stat < parseInt(v.wins, 10) / v.games.length){
    //                 stat = parseInt(v.wins, 10) / v.games.length;
    //                 bestRole = `${v.baseName}\n`;
    //                 name = 'Best Role';
    //             }
    //             totalGames += v.games.length;
    //         });

    //         if(bestRole)
    //             output.fields.push({
    //                 name: name,
    //                 value: bestRole,
    //                 inline: true,
    //             });

    //         output.fields.push({
    //             name: 'Total Games Played',
    //             value: totalGames,
    //             inline: true,
    //         });

    //         output.fields.push({
    //             name: 'Win Rate',
    //             value: `${data.winRate}%`,
    //             inline: true,
    //         });

    //         m.channel.send({ embed: output }).catch(helpers.log);
    //     });
}

// eslint-disable-next-line no-unused-vars
function leaderboard(m){
    throw userError('This feature is temporarily disabled.');
    // const p1 = narrRequest('leaderboard', null, { userID: m.discordID, columnName: 'points' });
    // const p2 = narrRequest('leaderboard', null, { userID: m.discordID, columnName: 'winRate' });
    // const p3 = narrRequest('leaderboard', null, {
    //     userID: m.discordID,
    //     columnName: 'gamesPlayed' }
    // );
    // return Promise.all([p1, p2, p3])
    //     .then(data => {
    //         let hadData = false;
    //         const points = data[0];
    //         const winRate = data[1];
    //         const gamesPlayed = data[2];

    //         const titles = ['Most Points', 'Best Winrate', 'Most Games Played'];

    //         const output = {
    //             description: 'Narrator Leaderboard',
    //             fields: [],
    //             footer: {
    //                 text: 'Hosted by The Narrator',
    //             },
    //         };

    //         let board; let value; let
    //             title;
    //         for(let i = 0; i < data.length; i++){
    //             board = data[i];
    //             title = titles[i];
    //             value = '';

    //             board.forEach(rank => {
    //                 value += `${rank.rank}. `;
    //                 if(!rank.name)
    //                     value += `**${m.member.nickname}**`;
    //                 else
    //                     value += rank.name;
    //                 value += `\t **${rank.value}`;
    //                 value += '**\n';
    //             });

    //             hadData = hadData || value;
    //             value = value || 'No Data';

    //             output.fields.push({
    //                 name: title,
    //                 value: value,
    //                 inline: (i % 3 !== 2),
    //             });
    //         }

    //         if(!hadData)
    //             throw userError('No leaderboard data available yet.');

    //         return m.channel.send({ embed: output });
    //     }).catch(helpers.log);
}

async function history(m){
    let limit = 0;

    const rem = m.args;
    const prefix = (await channelCommandsRepo.get(m.channelID)) || '!';
    if(rem === '--help'){
        const game = discordClient.getGameByChannelID(m.channelID);

        const text = `${prefix}history <number>** - list _x_ amount of recaps\n`
            + `${prefix}history --all** - list as many recaps as fits in the screen\n`
            + ' *more history commands coming soon!* ';


        if(game && game.isStarted)
            return discordJsonClient.sendDirectMessage(m.discordID, text);
        return discordJsonClient.sendPublicMessage(m.originChannelID, text);
    }
    if(helpers.isInt(rem)){
        limit = parseInt(rem, 10);
    }else if(!rem){
        limit = 10;
    }else if(rem !== 'all'){
        const replyText = `Unknown history flag. Type ${prefix}history --help `
            + 'to get more options on history commands.';
        throw userError(replyText);
    }

    const user = await userIntegrationsRepo.getByExternalID(m.discordID, userTypes.DISCORD);
    let results;
    if(user)
        results = await historyRepo.get(user.id, limit);
    else
        results = [];

    const discordName = discordJsonClient.getName(m.discordID);
    const game = discordClient.getGameByChannelID(m.originChannelID);
    const message = mappers.historyOverview(results, discordName, game, m.discordID);
    discordJsonClient.sendMessage(message);
}

function skip(m){
    const game = ensureGameInChannel(m);
    if(game.isStarted)
        return endPhase(m);
    return setSkipRule(m);
}

function end(m){
    const rem = m.args;
    if(!rem || rem.toLowerCase() !== 'night')
        throw userError(`Unknown command: end ${rem}.`);
    skip(m);
}

function usage(m){
    if(!m.args)
        throw userError(`To see an example of command usage, "
            + "use ${m.channelCommand}usage *command*\n"
            + "To see all available commands, use **${m.channelCommand}commands**`);

    let commandText = m.args;
    commandText = commandText.toLowerCase().replace(/ /g, '');
    const playerCommands = discordClient.getPlayerCommandsByExternalID(m.discordID);
    if(!playerCommands || !(Object.keys(playerCommands).length))
        throw userError('There are no usage examples for you');

    const command = playerCommands[commandText];
    if(!command || !command.usage)
        throw userError('No usage for this command');

    let message = command.usage.split('\n');
    message = message.map(x => m.channelCommand + x);
    discordJsonClient.sendPublicMessage(m.originChannelID, message.join('\n'));
}

module.exports = {
    echo,
    help,
    commands,
    status: info,
    info,
    invite,
    changecommand: changeCommand,

    host,
    in: join,
    play: join,
    join,

    out: leave,
    exit: leave,
    quit: leave,
    leave,

    subscribe,
    unsubscribe,

    autotag,

    name,
    prefer,
    setup: setups,
    setups,
    moderate,

    nightlength: nightLength,
    daylength: dayLength,
    day,
    daystart: dayStart,
    night,
    nightstart: nightStart,
    ita: punch,
    punch,

    start,

    lw,
    votes: voteCount,
    vc: voteCount,
    votecount: voteCount,
    alive: living,
    living,

    stats,
    leaderboard,
    history,

    skip,
    end,
    usage,

    instanceCommand, // if Constants.js were to be removed, make sure this can't be hit
};

function userError(err){
    return {
        userErrorText: err,
    };
}

function ensureGameInChannel(m){
    const game = discordClient.getGameByChannelID(m.channelID);
    if(!game)
        throw userError('There aren\'t active games in this channel!');
    return game;
}

function getHostUserID(m){
    const game = discordClient.getGameByChannelID(m.channelID);
    return game.hostUserID;
}

async function editRules(m, setupModifierChangeRequest){
    ensureGameInChannel(m);

    const hostUserID = getHostUserID(m);
    if(hostUserID)// was deleted
        await setupModifierService.edit(hostUserID, setupModifierChangeRequest);
}

async function instanceCommand(m, command){
    command = command.split(' ').map(val => {
        if(val.startsWith('<@') && val.endsWith('>')){
            let temp = val.substring(2, val.length - 1);
            if(temp.startsWith('!'))
                temp = temp.substring(1);
            if(temp.match('^[0-9]*$'))
                return temp;
        }
        return val;
    }).join(' ');

    if(command.toLowerCase().startsWith('vote')){
        let afterVote = command.substring(5);
        afterVote = afterVote.split(' ').join('');
        command = `vote ${afterVote}`;
    }

    command = command.split('_').join('');

    const userID = discordClient.getUserIDByDiscordID(m.discordID);

    try{
        await actionService.submitActionMessage(userID, { message: command });
    }catch(responseObj){
        throw userError(responseObj.errors[0]);
    }
}

function endPhase(m){
    const game = ensureGameInChannel(m);
    if(!game.isStarted)
        throw userError('I cannot submit this command in a nonstarted game.');
    let message;
    if(game.isDay)
        message = 'skip day';
    else
        message = 'end night';
    return actionService.submitActionMessage(
        discordClient.getUserIDByDiscordID(m.discordID),
        { message },
    );
}

async function setSkipRule(m){
    ensureGameInChannel(m);

    let rem = m.args;
    if(!rem)
        rem = 'on';
    rem = rem.toLowerCase();
    if(rem !== 'on' && rem !== 'off')
        throw userError(`Example usage for ${m.channelCommand} skip**\n`
            + `${m.channelCommand}skip on/off`);
    await editRules(m, { name: 'SKIP_VOTE', value: rem === 'on' });

    let message;
    if(rem === 'on')
        message = 'Players may skip the day, avoiding a public execution.';
    else
        message = 'Players must vote someone out to end the day.';
    discordJsonClient.sendPublicMessage(m.channelID, message);
}
