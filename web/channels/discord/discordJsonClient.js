const discord = require('discord.js');

const config = require('../../../config');

const helpers = require('../../utils/helpers');


const client = new discord.Client();

client.on('error', err => {
    if(typeof err === 'object' && err.code === 'ECONNRESET')
        return;

    helpers.log(err, 'Discord Json Client error');
});

async function connect(){
    const discordClient = require('./discordWrapper');
    const onMessage = require('./events/external/onMessage');
    const timerService = require('../../services/timerService.js');
    await new Promise(resolve => {
        client.on('ready', resolve);
        client.login(config.discord.master_key);
    });

    client.on('message', async m => {
        if(this.client.user.id === m.author.id)
            return;

        m = await onMessage.eventToLogic(m);
        if(!m) // these are commands the wrapper sent
            return;

        const game = discordClient.getGameByDiscordID(m.discordID);
        if(game)
            timerService.setGameExpiration(game.joinID);
    });
}

function deleteMessage(discordMessage){
    const err = "I'd like to delete a message but I currently don't have permission to do that. "
    + 'Please consider asking the server owner to give me permission to do this!';
    return discordMessage.delete()
        .catch(() => sendDirectMessage(discordMessage.author.id, err));
}

async function deleteMessageByIDs(obj){
    const channel = client.channels.get(obj.channelID);
    const message = await channel.fetchMessage(obj.messageID);
    const messageService = require('./discordServices/messageService');
    return messageService.deleteMessage(message);
}

function getInviteURL(channelID){
    return client.channels.get(channelID).createInvite()
        .then(invite => invite.toString());
}

function getName(discordID, channelID){
    const channel = client.channels.get(channelID);
    const { nickname } = channel.guild.members.get(discordID);
    if(nickname)
        return Promise.resolve(nickname);
    return client.fetchUser(discordID)
        .then(user => user.username);
}

function sendDirectMessage(discordID, message){
    return client.fetchUser(discordID)
        .then(user => user.createDM()).then(dm => dm.send(message)).catch(err => {
            if(typeof err !== 'object' || err.code !== 50007)
                helpers.log(err, 'failed to send discord direct message');
        });
}

async function sendMessage(request){
    if(Array.isArray(request)){
        for(let i = 0; i < request.length; i++)
            await sendMessage(request[i]);
        return;
    }
    if(request.channelID)
        return sendPublicMessage(request.channelID, request.message);

    if(request.discordID)
        return sendDirectMessage(request.discordID, request.message);
}

function sendPublicMessage(channelID, message){
    return client.channels.get(channelID).send(message);
}

function reply(m, message){
    return m.raw.reply(message).catch(helpers.log);
}

async function getUserStatus(discordID){
    const user = await client.fetchUser(discordID);
    return user.presence.status;
}

module.exports = {
    client,

    connect,
    deleteMessage,
    deleteMessageByIDs,

    getInviteURL,
    getName,

    reply,
    sendDirectMessage,
    sendMessage,
    sendPublicMessage,

    getUserStatus,
};
