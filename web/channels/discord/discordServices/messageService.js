const discordJsonClient = require('../discordJsonClient');


const safeDeletes = new Set();

// expects the actual message object from discordJS,
// not narrator's mapped object
function deleteMessage(message){
    safeDeletes.add(message.id);
    return discordJsonClient.deleteMessage(message);
}

function unmarkSafeDeletedMessage(messageExternalID){
    return safeDeletes.delete(messageExternalID);
}

module.exports = {
    deleteMessage,
    unmarkSafeDeletedMessage,
};
