const express = require('express');

const { app } = require('../app');
const setupValidator = require('../validators/setupValidator');
const setupService = require('../services/setupService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


const setups = express();

setups.post('/', async(request, response, next) => {
    let user;
    try{
        user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
    }catch(err){
        return next(err);
    }
    const setup = await setupService.create(user.id);
    response.json({
        errors: [],
        response: setup,
    });
});

setups.get('/:setupID([1-9][0-9]*)', async(request, response, next) => {
    try{
        const setupID = parseInt(request.params.setupID, 10);
        const setup = await setupService.getByID(setupID);
        response.json({
            response: setup,
            errors: [],
        });
    }catch(err){
        next(err);
    }
});

setups.get('/', async(request, response) => {
    let userID;
    try{
        const user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
        userID = user.id;
    }catch(err){
        userID = null;
    }

    const featuredSetups = await setupService.getFeaturedSetups(userID);
    const responseObj = { errors: [], response: featuredSetups };
    response.json(responseObj);
});

setups.put('/', async(request, response, next) => {
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const dataPromise = httpHelpers.getJson(request);
    let userAndJson;

    try{
        userAndJson = await Promise.all([userPromise, dataPromise]);
    }catch(err){
        return next(err);
    }

    const user = userAndJson[0];
    const json = userAndJson[1];

    try{
        const args = {
            setupID: json.setupID,
            setupKey: json.setupKey,
            userID: user.id,
        };
        setupValidator.setSetupRequest(args);
        const setup = await setupService.setSetup(args);
        const responseData = {
            errors: [],
            response: setup,
        };
        response.json(responseData);
    }catch(err){
        next(err);
    }
});

setups.delete('/:setupID([1-9][0-9]*)', async(request, response, next) => {
    let user;
    try{
        user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
    }catch(err){
        return next(err);
    }
    const setupID = request.url.substring(1);
    await setupService.deleteSetup(setupID, user.id);

    response.status(204).end();
});

app.use('/setups', setups);
