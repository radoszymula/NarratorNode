const express = require('express');

const { app } = require('../app');
const actionService = require('../services/actionService');
const userService = require('../services/userService');

const actionValidator = require('../validators/actionValidators');

const httpHelpers = require('../utils/httpHelpers');


const actionsRoute = express();

actionsRoute.put('/', async(request, response, next) => {
    try{
        const [user, data] = await Promise.all([
            userService.getByAuthToken({
                token: request.headers.auth,
                wrapperType: request.headers.authtype, // headers are lowercased
            }),
            httpHelpers.getJson(request),
        ]);
        response.json({
            errors: [],
            response: await actionService.submitActionMessage(user.id, data),
        });
    }catch(err){
        next(err);
    }
});

actionsRoute.delete('/', async(request, response, next) => {
    try{
        const user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
        request.query.actionIndex = parseInt(request.query.actionIndex, 10);
        actionValidator.cancelActionRequest(request.query);
        await actionService.cancelAction(user.id, request.query.actionIndex, request.query.command);

        response.status(204).end();
    }catch(err){
        return next(err);
    }
});

app.use('/actions', actionsRoute);
