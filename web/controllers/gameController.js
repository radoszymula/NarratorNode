const express = require('express');

const userPermissions = require('../models/enums/userPermissions');

const { app } = require('../app');
const authService = require('../services/authService');
const gameService = require('../services/gameService');
const userService = require('../services/userService');

const helpers = require('../utils/helpers');
const httpHelpers = require('../utils/httpHelpers');

const gameValidators = require('../validators/gameValidators');


const gameController = express();

gameController.get('/userState', async(request, response, next) => {
    try{
        const user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
        const gameState = await gameService.getUserState(user.id);
        response.json({
            errors: [],
            response: gameState,
        });
    }catch(err){
        return next(err); // probalby should return a failed authentication error
    }
});

gameController.get('/', async(request, response, next) => {
    if(helpers.isEmptyObject(request.query))
        return getActiveGameOverviews(response, next);

    if(request.query.join_id)
        response.json({ errors: [], response: await getByJoinID(request) });
    else
        return getActiveGameOverviews(response, next);
});

gameController.get('/:gameID([1-9][0-9]*)', async(request, response) => {
    let userID;
    try{
        userID = (await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        })).id;
    }catch(err){
        userID = -1;
    }
    const gameID = parseInt(request.params.gameID, 10);
    const gameObj = await gameService.getByID(gameID, userID);
    response.json({
        errors: [],
        response: gameObj,
    });
});

gameController.post('/:gameID([1-9][0-9]*)/start', async(request, response, next) => {
    let user;
    try{
        user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
    }catch(err){
        return next(err);
    }
    try{
        const gameObj = await gameService.start(user.id);
        response.json({
            errors: [],
            response: gameObj,
        });
    }catch(err){
        next(err);
    }
});

gameController.post('/', async(request, response, next) => {
    const allowNone = true;
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const dataPromise = httpHelpers.getJson(request, allowNone);
    try{
        const [user, json] = await Promise.all([userPromise, dataPromise]);
        const game = await gameService.create(user.id, json.name, json.setupName, json.setupID,
            json.rules);
        response.json({
            errors: [],
            response: game,
        });
    }catch(err){
        next(err);
    }
});

gameController.delete('/:userId([a-zA-Z]{4})', async(request, response, next) => {
    const joinID = request.url.substring(1);
    try{
        await authService.validateModRequest(request, userPermissions.GAME_EDITING);
    }catch(err){
        return next(err);
    }
    await gameService.deleteGame(joinID);
    response.status(204).end();
});

async function getActiveGameOverviews(response, next){
    try{
        const gameList = await gameService.getAll();
        response.json({
            errors: [],
            response: gameList,
        });
    }catch(err){
        return next(err);
    }
}

async function getByJoinID(request){
    const joinID = request.query.join_id;
    let userID;
    try{
        userID = (await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        })).id;
    }catch(err){
        userID = -1;
    }
    try{
        gameValidators.joinIDRequest(joinID);
    }catch(err){
        return {};
    }
    return gameService.getByJoinID(joinID, userID);
}

app.use('/games', gameController);
