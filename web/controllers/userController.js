const express = require('express');

const { app } = require('../app');
const helpers = require('../utils/helpers');
const userService = require('../services/userService');


const users = express();

users.get('/', (request, response, next) => {
    if(isAuthUserLookup(request))
        return getAuthTokenUserID(request, response);
    return next();
});

users.get('/id', (request, response, next) => {
    userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    }).then(user => {
        response.json({
            errors: [],
            response: user,
        });
    }).catch(next);
});

users.post('/:userID/auth_token', async(request, response, next) => {
    if(!helpers.isInt(request.params.userID))
        return next();
    const userID = parseInt(request.params.userID, 10);
    const authToken = await userService.addTempAuth(userID);
    response.json({
        errors: [],
        response: {
            authToken,
        },
    });
});

app.use('/users', users);

function isAuthUserLookup(request){
    if(!request.query.auth_token)
        return;

    const authToken = request.query.auth_token;
    if(authToken.length !== 128)
        return false;
    return helpers.isAlphaNumeric(authToken);
}

function getAuthTokenUserID(request, response){
    const userID = userService.getIDByAuthToken(request.query.auth_token);
    if(!userID)
        response.status(422).json({
            errors: ['Auth token not found.'],
            response: {},
        });
    else
        response.json({
            errors: [],
            response: {
                userID,
            },
        });
}
