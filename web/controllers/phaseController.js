const express = require('express');

const { app } = require('../app');
const phaseValidator = require('../validators/phaseValidtator');
const phaseService = require('../services/phaseService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


const phaseRoute = express();

phaseRoute.put('/', async(request, response, next) => {
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const dataPromise = httpHelpers.getJson(request);
    let userAndJson;

    try{
        userAndJson = await Promise.all([userPromise, dataPromise]);
    }catch(err){
        return next(err);
    }

    const [user, args] = userAndJson;

    try{
        phaseValidator.updatePhaseRemainingTimeRequest(args);
    }catch(err){
        return next(err);
    }

    try{
        const newEndTime = await phaseService.setExpirationViaUser(user.id, args.joinID,
            args.seconds);
        const responseObj = {
            endTime: newEndTime,
        };
        response.json(responseObj);
    }catch(err){
        return next(err);
    }
});

app.use('/phases', phaseRoute);
