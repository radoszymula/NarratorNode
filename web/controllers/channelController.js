const express = require('express');

const { app } = require('../app');
const channelService = require('../services/channelService');

const channelValidators = require('../validators/channelValidators');

const httpHelpers = require('../utils/httpHelpers');

const channels = express();

channels.post('/sc2mafia/thread', async(request, response, next) => {
    const json = await httpHelpers.getJson(request);
    const promises = [];
    if(json.threadID)
        promises.push(channelService.sc2mafiaNewPost(json.threadID));
    try{
        if(json.waitForCompletion)
            await Promise.all(promises);
    }catch(err){
        return next(err);
    }
    response.status(204).end();
});

channels.get('/browser/activeUserIDs', async(request, response, next) => {
    const gameID = parseInt(request.query.game_id, 10);

    try{
        channelValidators.getActiveUserIDsRequest(gameID);
        const userIDs = await channelService.getActiveBrowserUserIDs(gameID);
        response.json({ response: [...userIDs] });
    }catch(err){
        return next(err);
    }
});

channels.get('/sc2mafia/condorcet', async(request, response, next) => {
    const threadID = parseInt(request.query.threadID, 10);
    try{
        channelValidators.getCondorcetVotesRequest(threadID);
        const serviceResponse = await channelService.getCondorcetVotes(threadID);
        response.json({ response: serviceResponse });
    }catch(err){
        return next(err);
    }
});

channels.delete('/sc2mafia/condorcet', (request, response, next) => {
    try{
        channelService.clearCache(request.query.threadid);
        response.status(204).end();
    }catch(err){
        next(err);
    }
});

app.use('/channels', channels);
