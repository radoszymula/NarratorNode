const express = require('express');

const { app } = require('../app');
const setupHiddenValidators = require('../validators/setupHiddenValidators');
const setupHiddenService = require('../services/setupHiddenService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


const setupHiddenRoute = express();

setupHiddenRoute.post('/', async(request, response, next) => {
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const dataPromise = httpHelpers.getJson(request);
    let userAndJson;

    try{
        userAndJson = await Promise.all([userPromise, dataPromise]);
    }catch(err){
        return next(err);
    }

    const user = userAndJson[0];
    const json = userAndJson[1];
    try{
        setupHiddenValidators.createSetupHiddenRequest(json);
    }catch(err){
        return next(err);
    }
    try{
        const setupHidden = await setupHiddenService.add(user.id, json.hiddenID);
        const responseData = {
            errors: [],
            response: setupHidden,
        };
        response.json(responseData);
    }catch(err){
        next(err);
    }
});

setupHiddenRoute.delete('/:setupHiddenID([1-9][0-9]*)', async(request, response, next) => {
    try{
        const user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
        const setupHiddenID = parseInt(request.params.setupHiddenID, 10);
        await setupHiddenService.remove(user.id, setupHiddenID);
        response.status(204).end();
    }catch(err){
        next(err);
    }
});

app.use('/setupHiddens', setupHiddenRoute);
