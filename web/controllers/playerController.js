const express = require('express');

const { app } = require('../app');
const playerService = require('../services/playerService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');

const gameValidators = require('../validators/gameValidators');
const playerValidator = require('../validators/playerValidator');


const players = express();


players.post('/bots', async(request, response, next) => {
    try{
        const [user, data] = await Promise.all([
            userService.getByAuthToken({
                token: request.headers.auth,
                wrapperType: request.headers.authtype, // headers are lowercased
            }),
            httpHelpers.getJson(request),
        ]);
        playerValidator.addBotsRequest(data);
        const serviceResponse = await playerService.addBots(user.id, data.botCount);
        response.json({ response: serviceResponse });
    }catch(err){
        next(err);
    }
});

players.post('/', async(request, response, next) => {
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });

    const allowNone = false;
    const dataPromise = httpHelpers.getJson(request, allowNone);
    let userAndJson;

    try{
        userAndJson = await Promise.all([userPromise, dataPromise]);
    }catch(err){
        return next(err);
    }

    const user = userAndJson[0];
    const json = userAndJson[1];

    try{
        gameValidators.joinIDRequest(json.joinID);
    }catch(err){
        return next(err);
    }

    try{
        const game = await playerService.create(user.id, json.playerName, json.joinID);
        response.json({
            errors: [],
            response: game,
        });
    }catch(err){
        next(err);
    }
});

players.delete('/kick', async(request, response, next) => {
    try{
        const user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
        const playerID = parseInt(request.query.playerID, 10);
        playerValidator.kickUserRequest(playerID);
        const gameResponse = await playerService.kick(user.id, playerID);
        response.json({ response: gameResponse });
    }catch(err){
        next(err);
    }
});

players.delete('/', async(request, response, next) => {
    try{
        const user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
        await playerService.leave(user.id);
        response.status(204).end();
    }catch(err){
        return next(err);
    }
});

app.use('/players', players);
