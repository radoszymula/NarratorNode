const express = require('express');

const { app } = require('../app');
const factionService = require('../services/factionService');
const userService = require('../services/userService');


const factionRoute = express();

factionRoute.delete('/:factionID([1-9][0-9]*)', async(request, response, next) => {
    try{
        const user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
        const factionID = parseInt(request.url.substring(1), 10);
        await factionService.deleteFaction({ factionID, userID: user.id });

        response.status(204).end();
    }catch(err){
        return next(err);
    }
});

app.use('/factions', factionRoute);
