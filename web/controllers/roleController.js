const express = require('express');

const { app } = require('../app');
const roleService = require('../services/roleService');
const userService = require('../services/userService');


const roleRoute = express();

roleRoute.get('/:roleID([1-9][0-9]*)', async(request, response, next) => {
    let user;
    try{
        user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
    }catch(err){
        return next(err);
    }
    const roleID = parseInt(request.url.substring(1), 10);
    let roleResponse;
    try{
        roleResponse = await roleService.get(user.id, roleID);
    }catch(err){
        return next(err);
    }

    const responseData = {
        errors: [],
        response: roleResponse,
    };
    response.json(responseData);
});

app.use('/roles', roleRoute);
