const express = require('express');

const { app } = require('../app');
const hiddenValidators = require('../validators/hiddenValidators');
const hiddenService = require('../services/hiddenService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


const hiddenRoute = express();

hiddenRoute.post('/', async(request, response, next) => {
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const dataPromise = httpHelpers.getJson(request);
    let userAndJson;

    try{
        userAndJson = await Promise.all([userPromise, dataPromise]);
    }catch(err){
        return next(err);
    }

    const user = userAndJson[0];
    const json = userAndJson[1];
    try{
        hiddenValidators.createHiddenRequest(json);
    }catch(err){
        return next(err);
    }
    try{
        const setupHidden = await hiddenService.create(user.id, json);
        const responseData = {
            errors: [],
            response: setupHidden,
        };
        response.json(responseData);
    }catch(err){
        next(err);
    }
});

app.use('/hiddens', hiddenRoute);
