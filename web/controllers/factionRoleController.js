const express = require('express');

const { app } = require('../app');

const factionRoleService = require('../services/factionRoleService');
const factionRoleAbilityModifierService = require('../services/factionRoleAbilityModifierService');
const factionRoleModifierService = require('../services/factionRoleModifierService');
const userService = require('../services/userService');

const factionRoleModifierValidator = require('../validators/factionRoleModifierValidator');
const factionRoleAbilityModifierValidator = require('../validators/factionRoleAbilityModifierValidator'); // eslint-disable-line max-len

const httpHelpers = require('../utils/httpHelpers');


const factionRoleRoute = express();

factionRoleRoute.get('/:factionRoleID([1-9][0-9]*)', async(request, response, next) => {
    try{
        const factionRoleID = parseInt(request.url.substring(1), 10);
        const user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
        const roleResponse = await factionRoleService.get(user.id, factionRoleID);
        const responseData = {
            errors: [],
            response: roleResponse,
        };
        response.json(responseData);
    }catch(err){
        return next(err);
    }
});

factionRoleRoute.put('/:factionRoleID([1-9][0-9]*)/abilities/:abilityID([1-9][0-9]*)/modifiers',
    async(request, response, next) => {
        const userPromise = userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
        const dataPromise = httpHelpers.getJson(request);

        try{
            const [user, args] = await Promise.all([userPromise, dataPromise]);
            factionRoleAbilityModifierValidator.updateFactionRoleAbilityModifierRequest(args);
            const factionRoleID = parseInt(request.params.factionRoleID, 10);
            const abilityID = parseInt(request.params.abilityID, 10);
            const modifierResponse = await factionRoleAbilityModifierService.update(user.id,
                factionRoleID, abilityID, args);
            response.json({
                errors: [],
                response: modifierResponse,
            });
        }catch(err){
            return next(err);
        }
    });

factionRoleRoute.put('/:factionRoleID([1-9][0-9]*)/modifiers',
    async(request, response, next) => {
        const userPromise = userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
        const dataPromise = httpHelpers.getJson(request);

        try{
            const [user, args] = await Promise.all([userPromise, dataPromise]);
            factionRoleModifierValidator.updateFactionRoleModifierRequest(args);

            const factionRoleID = parseInt(request.params.factionRoleID, 10);
            const modifierResponse = await factionRoleModifierService.update(user.id,
                factionRoleID, args);

            response.json({
                errors: [],
                response: modifierResponse,
            });
        }catch(err){
            return next(err);
        }
    });

app.use('/factionRoles', factionRoleRoute);
