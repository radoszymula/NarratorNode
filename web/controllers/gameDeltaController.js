const express = require('express');

const { app } = require('../app');
const gameDeltasService = require('../services/gameDeltasService');


const gameDeltas = express();

gameDeltas.post('/', async(request, response, next) => {
    try{
        const externalGames = await gameDeltasService.findGameDeltas();
        response.json({
            errors: [],
            response: externalGames,
        });
    }catch(err){
        next(err);
    }
});

app.use('/game_deltas', gameDeltas);
