const express = require('express');
const path = require('path');

const { app } = require('../app');
const helpers = require('../utils/helpers');


const staticRoute = express.static('../public', {
    dotfiles: 'deny',
});

app.use('/', staticRoute);

app.get('/:gameJoinID', async(request, response, next) => {
    const { gameJoinID } = request.params;
    if(!gameJoinID || gameJoinID.length !== 4 || !helpers.isAlpha(gameJoinID))
        return next();

    response.sendFile(path.join(__dirname, '../../public', 'index.html'));
});
