const express = require('express');

const { app } = require('../app');
const moderatorService = require('../services/moderatorService');
const moderatorValidators = require('../validators/moderatorValidators');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


const moderatorRoute = express();

moderatorRoute.post('/', async(request, response, next) => {
    let user;
    try{
        user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
    }catch(err){
        return next(err);
    }

    try{
        const game = await moderatorService.create(user.id);
        response.json({
            errors: [],
            response: game,
        });
    }catch(err){
        next(err);
    }
});

moderatorRoute.post('/repick', async(request, response, next) => {
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const dataPromise = httpHelpers.getJson(request);
    let user;
    let args;

    try{
        [user, args] = await Promise.all([userPromise, dataPromise]);
    }catch(err){
        return next(err);
    }

    try{
        moderatorValidators.repickRequest(args);
    }catch(err){
        return next(err);
    }

    const repickTarget = args && args.repickTarget;
    try{
        const host = await moderatorService.repick(args.gameID, user.id, repickTarget);
        response.json({ response: host });
    }catch(err){
        return next(err);
    }
});

moderatorRoute.delete('/', async(request, response, next) => {
    let user;
    try{
        user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
    }catch(err){
        return next(err);
    }

    try{
        await moderatorService.delete(user.id);
        response.status(204).end();
    }catch(err){
        next(err);
    }
});

app.use('/moderators', moderatorRoute);
