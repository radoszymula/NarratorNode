const express = require('express');

const { app } = require('../app');
const setupModifierValidator = require('../validators/setupModifierValidator');
const setupModifierService = require('../services/setupModifierService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


const setupModifierRoute = express();

setupModifierRoute.put('/', async(request, response, next) => {
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const dataPromise = httpHelpers.getJson(request);

    try{
        const [user, args] = await Promise.all([userPromise, dataPromise]);
        setupModifierValidator.updateSetupModifierRequest(args);
        const modifierResponse = await setupModifierService.edit(user.id, args);
        response.json({
            errors: [],
            response: modifierResponse,
        });
    }catch(err){
        return next(err);
    }
});

app.use('/setupModifiers', setupModifierRoute);
