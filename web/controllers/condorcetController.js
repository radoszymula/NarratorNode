const express = require('express');

const { app } = require('../app');
const condorcetService = require('../services/condorcetService');

const httpHelpers = require('../utils/httpHelpers');


const condorcetRoutes = express();

condorcetRoutes.post('/', async(request, response, next) => {
    try{
        const data = await httpHelpers.getJson(request);
        const condorcetOutput = await condorcetService.resolveInput(data);
        response.json({
            errors: [],
            response: condorcetOutput,
        });
    }catch(err){
        next(err);
    }
});

app.use('/condorcet', condorcetRoutes);
