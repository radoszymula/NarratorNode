const express = require('express');

const { app } = require('../app');

const leaderboardService = require('../services/leaderboardService');


const leaderboards = express();

// todo
leaderboards.get('/', async(request, response) => {
    const { column } = request.query;
    const data = await leaderboardService.getColumn(column); /* eslint-disable-line no-unused-vars */
    response.json({
        errors: [],
        response: {},
    });
});

app.use('/leaderboards', leaderboards);
