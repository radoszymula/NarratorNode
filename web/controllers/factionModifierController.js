const express = require('express');

const { app } = require('../app');
const factionModifierValidator = require('../validators/factionModifierValidator');
const factionModifierService = require('../services/factionModifierService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


const factionModifierRoute = express();

factionModifierRoute.put('/:factionID([1-9][0-9]*)/modifiers', async(request, response, next) => {
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const dataPromise = httpHelpers.getJson(request);

    const factionID = parseInt(request.url.substring(1), 10);
    try{
        const [user, args] = await Promise.all([userPromise, dataPromise]);
        factionModifierValidator.updateFactionModifierRequest(args);
        const modifierResponse = await factionModifierService.update(user.id, factionID, args);
        const responseData = {
            errors: [],
            response: modifierResponse,
        };
        response.json(responseData);
    }catch(err){
        return next(err);
    }
});

app.use('/factions', factionModifierRoute);
