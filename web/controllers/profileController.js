const express = require('express');

const { app } = require('../app');
const profileService = require('../services/profileService');
const userService = require('../services/userService');


const profileRoute = express();

profileRoute.get('/', async(request, response, next) => {
    let user;
    try{
        user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
    }catch(err){
        return next(err);
    }

    const profile = await profileService.get(user.id);
    response.json({
        errors: [],
        response: profile,
    });
});

app.use('/profiles', profileRoute);
