const express = require('express');

const { app } = require('../app');
const chatService = require('../services/chatService');
const userService = require('../services/userService');


const chatRoutes = express();

chatRoutes.get('/', async(request, response, next) => {
    try{
        const user = await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        });
        const userChats = await chatService.getUserChat(user.id);
        response.json({
            errors: [],
            response: userChats,
        });
    }catch(err){
        next(err);
    }
});

app.use('/chats', chatRoutes);
