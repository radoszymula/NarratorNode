const express = require('express');

const { app } = require('../app');

const userIntegrationService = require('../services/userIntegrationService');


const userIntegrations = express();

userIntegrations.post('/', async(request, response, next) => {
    const authType = request.headers.authtype;
    const { auth } = request.headers;
    const authToken = request.headers.auth_token;
    if(!auth || !authType || !authToken)
        return next();
    userIntegrationService.addAuthToken(auth, authType, authToken)
        .then(user => {
            response.json({
                errors: [],
                response: user,
            });
        }).catch(next);
});

app.use('/user_integrations', userIntegrations);
