CREATE TABLE discord_channel_commands (
    channel_id VARCHAR(36) NOT NULL,
    prefix VARCHAR(2) NOT NULL
);

ALTER TABLE discord_channel_commands ADD CONSTRAINT discord_channel_commands_unique UNIQUE(channel_id, prefix);
