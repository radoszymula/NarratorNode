INSERT INTO role_ability_bool_modifiers
SELECT role_abilities.id AS role_ability_id, 'back_to_back' AS name, false AS value FROM role_abilities
    LEFT JOIN roles ON roles.id = role_abilities.role_id
    LEFT JOIN setups ON setups.id = roles.setup_id
    INNER JOIN (
    SELECT * FROM setup_bool_modifiers sbm
    WHERE sbm.name = 'ghost_back_to_back' AND value = 0
) sub_sbm ON sub_sbm.setup_id = setups.id
WHERE LOWER(role_abilities.name) = 'ghost';

DELETE FROM setup_bool_modifiers
WHERE name = 'ghost_back_to_back'
OR name = 'vent_back_to_back';
