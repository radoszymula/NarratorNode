ALTER TABLE replays ADD COLUMN is_started BOOLEAN;
UPDATE replays SET is_started = true;
ALTER TABLE replays CHANGE COLUMN is_started is_started BOOLEAN NOT NULL;
