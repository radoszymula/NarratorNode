ALTER TABLE players DROP FOREIGN KEY players_on_role_delete;
ALTER TABLE players MODIFY role_id BIGINT;
ALTER TABLE players DROP FOREIGN KEY players_on_hidden_delete;
ALTER TABLE players MODIFY hidden_id BIGINT;
