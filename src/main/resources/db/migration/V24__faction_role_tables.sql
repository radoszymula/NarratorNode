# migrate zero weighted ability modifiers
INSERT INTO faction_ability_bool_modifiers
SELECT faction_ability_id, name, value from faction_ability_int_modifiers WHERE LOWER(name) = 'zero_weighted';

DELETE FROM faction_ability_int_modifiers WHERE LOWER(name) = 'zero_weighted';
DELETE FROM faction_abilities WHERE lower(name) = 'factionsend';

#######################
# Add setup_id to roles
#######################
ALTER TABLE roles ADD COLUMN setup_id BIGINT UNSIGNED;
UPDATE roles r
INNER JOIN factions f ON f.id = r.faction_id
SET r.setup_id = f.setup_id;
ALTER TABLE roles CHANGE COLUMN setup_id setup_id BIGINT UNSIGNED NOT NULL;

ALTER TABLE roles ADD CONSTRAINT roles_on_setup_delete FOREIGN KEY (setup_id) REFERENCES setups(id) ON DELETE CASCADE;

###############
# Faction Roles
###############
CREATE TABLE faction_roles (
	id SERIAL PRIMARY KEY,
	role_id BIGINT UNSIGNED NOT NULL,
	faction_id BIGINT UNSIGNED NOT NULL,
	setup_id BIGINT UNSIGNED NOT NULL,
	received_faction_role_id BIGINT UNSIGNED
);
ALTER TABLE faction_roles ADD CONSTRAINT role_id_faction_id UNIQUE(role_id, faction_id);
ALTER TABLE faction_roles ADD CONSTRAINT faction_roles_on_faction_delete FOREIGN KEY (faction_id) REFERENCES factions(id) ON DELETE CASCADE;
ALTER TABLE faction_roles ADD CONSTRAINT faction_roles_on_roles_delete FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;

INSERT INTO faction_roles (role_id, faction_id, setup_id)
SELECT id AS role_id, faction_id, setup_id FROM roles;

#############################
# Faction Role Role Modifiers
#############################
CREATE TABLE faction_role_role_int_modifiers (
	faction_role_id BIGINT UNSIGNED NOT NULL,
	name VARCHAR(40) NOT NULL,
	value MEDIUMINT NOT NULL
);

ALTER TABLE faction_role_role_int_modifiers
	ADD CONSTRAINT faction_role_id_name
	UNIQUE(faction_role_id, name);
ALTER TABLE faction_role_role_int_modifiers
	ADD CONSTRAINT faction_role_role_int_modifiers_on_faction_role_delete
	FOREIGN KEY (faction_role_id)
	REFERENCES faction_roles(id)
	ON DELETE CASCADE;


CREATE TABLE faction_role_role_bool_modifiers (
	faction_role_id BIGINT UNSIGNED NOT NULL,
	name VARCHAR(40) NOT NULL,
	value BOOLEAN NOT NULL
);

ALTER TABLE faction_role_role_bool_modifiers
	ADD CONSTRAINT faction_role_id_name
	UNIQUE(faction_role_id, name);
ALTER TABLE faction_role_role_bool_modifiers
	ADD CONSTRAINT faction_role_role_bool_modifiers_on_faction_role_delete
	FOREIGN KEY (faction_role_id)
	REFERENCES faction_roles(id)
	ON DELETE CASCADE;

################################
# Faction Role Ability Modifiers
################################
CREATE TABLE faction_role_ability_int_modifiers (
	faction_role_id BIGINT UNSIGNED NOT NULL,
	role_ability_id BIGINT UNSIGNED NOT NULL,
	name VARCHAR(40) NOT NULL,
	value MEDIUMINT NOT NULL
);

CREATE TABLE faction_role_ability_bool_modifiers (
	faction_role_id BIGINT UNSIGNED NOT NULL,
	role_ability_id BIGINT UNSIGNED NOT NULL,
	name VARCHAR(40) NOT NULL,
	value BOOLEAN NOT NULL
);

ALTER TABLE faction_role_ability_int_modifiers
	ADD CONSTRAINT faction_role_id_role_ability_id_name
	UNIQUE(faction_role_id, role_ability_id, name);
ALTER TABLE faction_role_ability_bool_modifiers
	ADD CONSTRAINT faction_role_id_role_ability_id_name
	UNIQUE(faction_role_id, role_ability_id, name);

# on faction role delete
ALTER TABLE faction_role_ability_bool_modifiers
	ADD CONSTRAINT faction_role_ability_bool_modifiers_on_faction_role_delete
	FOREIGN KEY (faction_role_id)
	REFERENCES faction_roles(id)
	ON DELETE CASCADE;
ALTER TABLE faction_role_ability_int_modifiers
	ADD CONSTRAINT faction_role_ability_int_modifiers_on_faction_role_delete
	FOREIGN KEY (faction_role_id)
	REFERENCES faction_roles(id)
	ON DELETE CASCADE;

# on role ability delete
ALTER TABLE faction_role_ability_bool_modifiers
	ADD CONSTRAINT faction_role_ability_bool_modifiers_on_role_ability_delete
	FOREIGN KEY (role_ability_id)
	REFERENCES role_abilities(id)
	ON DELETE CASCADE;
ALTER TABLE faction_role_ability_int_modifiers
	ADD CONSTRAINT faction_role_ability_int_modifiers_on_role_ability_delete
	FOREIGN KEY (role_ability_id)
	REFERENCES role_abilities(id)
	ON DELETE CASCADE;

######################
# Hidden Faction Roles
######################
RENAME TABLE hidden_roles TO hidden_faction_roles;
ALTER TABLE hidden_faction_roles ADD COLUMN faction_role_id BIGINT UNSIGNED;
UPDATE hidden_faction_roles hfr
INNER JOIN faction_roles fr ON hfr.role_id = fr.role_id
SET hfr.faction_role_id = fr.id;
ALTER TABLE hidden_faction_roles CHANGE COLUMN faction_role_id faction_role_id BIGINT UNSIGNED NOT NULL;

ALTER TABLE hidden_faction_roles ADD COLUMN setup_id BIGINT UNSIGNED;
UPDATE hidden_faction_roles hfr
INNER JOIN hiddens h ON hfr.hidden_id = h.id
SET hfr.setup_id = h.setup_id;
ALTER TABLE hidden_faction_roles CHANGE COLUMN setup_id setup_id BIGINT UNSIGNED NOT NULL;

ALTER TABLE hidden_faction_roles ADD CONSTRAINT hidden_id_faction_role_id UNIQUE(hidden_id, faction_role_id);
ALTER TABLE hidden_faction_roles ADD CONSTRAINT hidden_faction_roles_on_hidden_delete FOREIGN KEY (hidden_id) REFERENCES hiddens(id) ON DELETE CASCADE;
ALTER TABLE hidden_faction_roles ADD CONSTRAINT hidden_faction_roles_on_faction_role_delete FOREIGN KEY (faction_role_id) REFERENCES faction_roles(id) ON DELETE CASCADE;

ALTER TABLE narrator.hidden_faction_roles DROP FOREIGN KEY hidden_roles_on_role_delete;
ALTER TABLE narrator.hidden_faction_roles DROP INDEX hidden_roles_on_role_delete;
ALTER TABLE narrator.hidden_faction_roles DROP INDEX hidden_id_role_id;
ALTER TABLE hidden_faction_roles DROP COLUMN role_id;

#########
# Players
#########
ALTER TABLE players ADD COLUMN faction_role_id BIGINT UNSIGNED AFTER hidden_id;
UPDATE players p
INNER JOIN faction_roles fr ON fr.role_id = p.role_id
SET p.faction_role_id = fr.id;
ALTER TABLE players DROP COLUMN role_id;

#######
# Roles
#######

ALTER TABLE roles DROP COLUMN internal_name;
ALTER TABLE roles DROP FOREIGN KEY roles_on_faction_delete;
ALTER TABLE roles DROP COLUMN faction_id;

# move role_int_modifiers to faction_role_int_modifiers
INSERT INTO faction_role_role_int_modifiers
SELECT faction_roles.id AS faction_role_id, UPPER(role_int_modifiers.name), role_int_modifiers.value
FROM role_int_modifiers
LEFT JOIN roles ON role_int_modifiers.role_id = roles.id
LEFT JOIN faction_roles ON faction_roles.role_id = roles.id;

DELETE FROM role_int_modifiers;

# move role_bool_modifiers to faction_role_int_modifiers
INSERT INTO faction_role_role_bool_modifiers
SELECT faction_roles.id AS faction_role_id, UPPER(role_bool_modifiers.name), role_bool_modifiers.value
FROM role_bool_modifiers
LEFT JOIN roles ON role_bool_modifiers.role_id = roles.id
LEFT JOIN faction_roles ON faction_roles.role_id = roles.id;

DELETE FROM role_bool_modifiers;

# update the duplicated role ids in faction roles to the non duplicated role_id
UPDATE faction_roles fr
    INNER JOIN roles r ON r.id = fr.role_id
    INNER JOIN (
        SELECT r.id, r.name, r.setup_id, r2.min_id FROM roles r
        INNER JOIN (
            SELECT MIN(ir.id) AS min_id, ir.id, ir.setup_id, ir.name, COUNT(*)
            FROM roles ir GROUP BY ir.name, ir.setup_id
        ) AS r2 ON r2.name = r.name AND r2.setup_id = r.setup_id
    ) AS mr ON mr.id = r.id
SET fr.role_id = mr.min_id;

# drop the duplicated roles
DELETE dr
FROM roles dr
         INNER JOIN (
    SELECT r.id, r.name, r.setup_id, r2.min_id FROM roles r
                                                        INNER JOIN (
        SELECT MIN(ir.id) AS min_id, ir.id, ir.setup_id, ir.name, COUNT(*)
        FROM roles ir GROUP BY ir.name, ir.setup_id
    ) AS r2 ON r2.name = r.name AND r2.setup_id = r.setup_id
) ar ON ar.id = dr.id
WHERE ar.id != ar.min_id;

ALTER TABLE roles ADD CONSTRAINT roles_name_setup_id UNIQUE (name, setup_id);

UPDATE players
SET faction_role_id = 22444
WHERE id = 983 OR id = 1121;

DELETE FROM commands WHERE counter = 38 AND replay_id = 523;
DELETE FROM commands WHERE counter = 16 AND replay_id = 535;
DELETE FROM commands WHERE counter = 190 AND replay_id = 536;
DELETE FROM commands WHERE counter = 144 AND replay_id = 543;
DELETE FROM commands WHERE counter = 76 AND replay_id = 547;
DELETE FROM commands WHERE counter = 24 AND replay_id = 548;
DELETE FROM replays WHERE id = 791;
DELETE FROM replays WHERE id = 793;
DELETE FROM replays WHERE id = 815;
DELETE FROM replays WHERE id = 862;
DELETE FROM replays WHERE id = 863;
DELETE FROM replays WHERE id = 1063;

#####################
# Faction Auto Hidden
#####################
ALTER TABLE factions ADD COLUMN hasAutoHidden BOOLEAN;
UPDATE factions SET hasAutoHidden = false;
ALTER TABLE factions CHANGE COLUMN hasAutoHidden hasAutoHidden BOOLEAN NOT NULL;
