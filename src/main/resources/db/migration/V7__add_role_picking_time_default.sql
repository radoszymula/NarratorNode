INSERT INTO setup_int_modifiers (setup_id, name, value)
SELECT id, 'ROLE_PICKING_LENGTH', 30 from setups;

UPDATE setup_int_modifiers SET value = value * 60 
WHERE name = 'DAY_LENGTH' 
OR name = 'NIGHT_LENGTH'
OR name = 'DISCUSSION_LENGTH'
OR name = 'TRIAL_LENGTH';