TRUNCATE bad_seeds;

UPDATE setup_bool_modifiers SET value = 1 WHERE name = 'SKIP_VOTE' AND setup_id <= 578;
ALTER TABLE roles DROP FOREIGN KEY roles_on_setup_delete;
ALTER TABLE roles DROP COLUMN setup_id;
DELETE FROM setups WHERE setups.id in (
    SELECT setup_id FROM replays WHERE replays.id in (7, 51, 320, 366, 584, 593, 595, 751)
);
-- 7 because can't attack allies
-- 51 because role id doesn't match correct roles
-- 320 never finished
-- 366 never finished
-- 584 never finished
-- 593 never finished
-- 595 never finished
-- 751 stopped db recording

-- no players
DELETE FROM replays WHERE id NOT IN (SELECT replay_id from players);

-- split commands
ALTER TABLE commands ADD COLUMN player_id BIGINT UNSIGNED AFTER counter;
UPDATE commands c
INNER JOIN players p ON p.replay_id = c.replay_id
AND p.name = (SUBSTRING_INDEX(c.command, ':\t', 1))
SET player_id = p.id;
UPDATE commands c
SET command = (SUBSTRING_INDEX(c.command, ':\t', -1));

-- delete duplicate roles
UPDATE hidden_roles SET role_id = 708 WHERE role_id = 15685;
UPDATE hidden_roles SET role_id = 686 WHERE role_id = 15684;
UPDATE hidden_roles SET role_id = 734 WHERE role_id = 15688;
UPDATE hidden_roles SET role_id = 909 WHERE role_id = 15701;
UPDATE hidden_roles SET role_id = 1100 WHERE role_id = 15704;
UPDATE hidden_roles SET role_id = 1256 WHERE role_id = 15711;
UPDATE hidden_roles SET role_id = 1377 WHERE role_id = 15719;
UPDATE hidden_roles SET role_id = 1451 WHERE role_id = 15720;
UPDATE hidden_roles SET role_id = 1502 WHERE role_id = 15731;
UPDATE hidden_roles SET role_id = 1578 WHERE role_id = 15740;
UPDATE hidden_roles SET role_id = 1613 WHERE role_id = 15741;
UPDATE hidden_roles SET role_id = 4583 WHERE role_id = 15780;
UPDATE hidden_roles SET role_id = 4637 WHERE role_id = 15784;
UPDATE hidden_roles SET role_id = 4660 WHERE role_id = 15790;
UPDATE hidden_roles SET role_id = 4923 WHERE role_id = 15819;
UPDATE hidden_roles SET role_id = 4969 WHERE role_id = 15831;
UPDATE hidden_roles SET role_id = 5276 WHERE role_id = 16007;
UPDATE hidden_roles SET role_id = 5305 WHERE role_id = 16008;
UPDATE hidden_roles SET role_id = 5344 WHERE role_id = 16011;
UPDATE hidden_roles SET role_id = 5472 WHERE role_id = 16036;
UPDATE hidden_roles SET role_id = 5543 WHERE role_id = 16052;
UPDATE hidden_roles SET role_id = 5650 WHERE role_id = 16107;
UPDATE hidden_roles SET role_id = 8009 WHERE role_id = 8033;
UPDATE hidden_roles SET role_id = 8007 WHERE role_id = 8031;
UPDATE hidden_roles SET role_id = 8008 WHERE role_id = 8032;
UPDATE hidden_roles SET role_id = 7999 WHERE role_id = 8023;
UPDATE hidden_roles SET role_id = 8006 WHERE role_id = 8030;
UPDATE hidden_roles SET role_id = 8001 WHERE role_id = 8025;
UPDATE hidden_roles SET role_id = 8005 WHERE role_id = 8029;
UPDATE hidden_roles SET role_id = 8010 WHERE role_id = 8034;
UPDATE hidden_roles SET role_id = 8004 WHERE role_id = 8028;
UPDATE hidden_roles SET role_id = 8003 WHERE role_id = 8027;
UPDATE hidden_roles SET role_id = 8002 WHERE role_id = 8026;
UPDATE hidden_roles SET role_id = 8000 WHERE role_id = 8024;
UPDATE hidden_roles SET role_id = 8012 WHERE role_id = 8055;
UPDATE hidden_roles SET role_id = 8014 WHERE role_id = 8057;
UPDATE hidden_roles SET role_id = 8013 WHERE role_id = 8056;
UPDATE hidden_roles SET role_id = 8022 WHERE role_id = 8065;
UPDATE hidden_roles SET role_id = 8018 WHERE role_id = 8061;
UPDATE hidden_roles SET role_id = 8011 WHERE role_id = 8054;
UPDATE hidden_roles SET role_id = 8017 WHERE role_id = 8060;
UPDATE hidden_roles SET role_id = 8016 WHERE role_id = 8059;
UPDATE hidden_roles SET role_id = 8015 WHERE role_id = 8058;
UPDATE hidden_roles SET role_id = 8021 WHERE role_id = 8064;
UPDATE hidden_roles SET role_id = 8019 WHERE role_id = 8062;
UPDATE hidden_roles SET role_id = 8020 WHERE role_id = 8063;
UPDATE hidden_roles SET role_id = 8303 WHERE role_id = 8327;
UPDATE hidden_roles SET role_id = 8299 WHERE role_id = 8323;
UPDATE hidden_roles SET role_id = 8300 WHERE role_id = 8324;
UPDATE hidden_roles SET role_id = 8309 WHERE role_id = 8333;
UPDATE hidden_roles SET role_id = 8301 WHERE role_id = 8325;
UPDATE hidden_roles SET role_id = 8306 WHERE role_id = 8330;
UPDATE hidden_roles SET role_id = 8308 WHERE role_id = 8332;
UPDATE hidden_roles SET role_id = 8304 WHERE role_id = 8328;
UPDATE hidden_roles SET role_id = 8307 WHERE role_id = 8331;
UPDATE hidden_roles SET role_id = 8305 WHERE role_id = 8329;
UPDATE hidden_roles SET role_id = 8302 WHERE role_id = 8326;
UPDATE hidden_roles SET role_id = 8298 WHERE role_id = 8322;
UPDATE hidden_roles SET role_id = 8311 WHERE role_id = 8354;
UPDATE hidden_roles SET role_id = 8313 WHERE role_id = 8356;
UPDATE hidden_roles SET role_id = 8312 WHERE role_id = 8355;
UPDATE hidden_roles SET role_id = 8318 WHERE role_id = 8361;
UPDATE hidden_roles SET role_id = 8310 WHERE role_id = 8353;
UPDATE hidden_roles SET role_id = 8317 WHERE role_id = 8360;
UPDATE hidden_roles SET role_id = 8316 WHERE role_id = 8359;
UPDATE hidden_roles SET role_id = 8315 WHERE role_id = 8358;
UPDATE hidden_roles SET role_id = 8314 WHERE role_id = 8357;
UPDATE hidden_roles SET role_id = 8321 WHERE role_id = 8364;
UPDATE hidden_roles SET role_id = 8319 WHERE role_id = 8362;
UPDATE hidden_roles SET role_id = 8320 WHERE role_id = 8363;
UPDATE hidden_roles SET role_id = 11085 WHERE role_id = 16232;
UPDATE hidden_roles SET role_id = 11120 WHERE role_id = 16237;
UPDATE hidden_roles SET role_id = 12026 WHERE role_id = 16244;
UPDATE hidden_roles SET role_id = 12301 WHERE role_id = 16252;
UPDATE hidden_roles SET role_id = 12329 WHERE role_id = 16254;
UPDATE hidden_roles SET role_id = 14274 WHERE role_id = 16309;
UPDATE hidden_roles SET role_id = 15511 WHERE role_id = 15584;
UPDATE hidden_roles SET role_id = 15496 WHERE role_id = 15529;
UPDATE hidden_roles SET role_id = 15504 WHERE role_id = 15570;
UPDATE hidden_roles SET role_id = 15497 WHERE role_id = 15530;
UPDATE hidden_roles SET role_id = 15507 WHERE role_id = 15580;
UPDATE hidden_roles SET role_id = 15487 WHERE role_id = 15550;
UPDATE hidden_roles SET role_id = 15486 WHERE role_id = 15514;
UPDATE hidden_roles SET role_id = 15499 WHERE role_id = 15562;
UPDATE hidden_roles SET role_id = 15501 WHERE role_id = 15567;
UPDATE hidden_roles SET role_id = 15503 WHERE role_id = 15538;
UPDATE hidden_roles SET role_id = 15510 WHERE role_id = 15582;
UPDATE hidden_roles SET role_id = 15495 WHERE role_id = 15526;
UPDATE hidden_roles SET role_id = 15488 WHERE role_id = 15552;
UPDATE hidden_roles SET role_id = 15489 WHERE role_id = 15515;
UPDATE hidden_roles SET role_id = 15500 WHERE role_id = 15535;
UPDATE hidden_roles SET role_id = 15492 WHERE role_id = 15521;
UPDATE hidden_roles SET role_id = 15493 WHERE role_id = 15525;
UPDATE hidden_roles SET role_id = 15506 WHERE role_id = 15576;
UPDATE hidden_roles SET role_id = 15490 WHERE role_id = 15553;
UPDATE hidden_roles SET role_id = 15494 WHERE role_id = 15560;
UPDATE hidden_roles SET role_id = 15494 WHERE role_id = 16349;
UPDATE hidden_roles SET role_id = 15484 WHERE role_id = 15512;
UPDATE hidden_roles SET role_id = 15505 WHERE role_id = 15571;
UPDATE hidden_roles SET role_id = 15509 WHERE role_id = 15544;
UPDATE hidden_roles SET role_id = 15491 WHERE role_id = 15556;
UPDATE hidden_roles SET role_id = 15578 WHERE role_id = 15594;
UPDATE hidden_roles SET role_id = 15542 WHERE role_id = 15589;
UPDATE hidden_roles SET role_id = 15528 WHERE role_id = 15598;
UPDATE hidden_roles SET role_id = 15569 WHERE role_id = 15588;
UPDATE hidden_roles SET role_id = 15557 WHERE role_id = 15595;
UPDATE hidden_roles SET role_id = 15554 WHERE role_id = 15590;
UPDATE hidden_roles SET role_id = 15545 WHERE role_id = 15597;
UPDATE hidden_roles SET role_id = 15551 WHERE role_id = 15586;
UPDATE hidden_roles SET role_id = 15527 WHERE role_id = 15596;
UPDATE hidden_roles SET role_id = 15540 WHERE role_id = 15587;
UPDATE hidden_roles SET role_id = 15547 WHERE role_id = 15585;
UPDATE hidden_roles SET role_id = 15572 WHERE role_id = 15591;
UPDATE hidden_roles SET role_id = 15522 WHERE role_id = 15593;
UPDATE players SET role_id = 708 WHERE role_id = 15685;
UPDATE players SET role_id = 686 WHERE role_id = 15684;
UPDATE players SET role_id = 734 WHERE role_id = 15688;
UPDATE players SET role_id = 909 WHERE role_id = 15701;
UPDATE players SET role_id = 1100 WHERE role_id = 15704;
UPDATE players SET role_id = 1256 WHERE role_id = 15711;
UPDATE players SET role_id = 1377 WHERE role_id = 15719;
UPDATE players SET role_id = 1451 WHERE role_id = 15720;
UPDATE players SET role_id = 1502 WHERE role_id = 15731;
UPDATE players SET role_id = 1578 WHERE role_id = 15740;
UPDATE players SET role_id = 1613 WHERE role_id = 15741;
UPDATE players SET role_id = 4583 WHERE role_id = 15780;
UPDATE players SET role_id = 4637 WHERE role_id = 15784;
UPDATE players SET role_id = 4660 WHERE role_id = 15790;
UPDATE players SET role_id = 4923 WHERE role_id = 15819;
UPDATE players SET role_id = 4969 WHERE role_id = 15831;
UPDATE players SET role_id = 5276 WHERE role_id = 16007;
UPDATE players SET role_id = 5305 WHERE role_id = 16008;
UPDATE players SET role_id = 5344 WHERE role_id = 16011;
UPDATE players SET role_id = 5472 WHERE role_id = 16036;
UPDATE players SET role_id = 5543 WHERE role_id = 16052;
UPDATE players SET role_id = 5650 WHERE role_id = 16107;
UPDATE players SET role_id = 8009 WHERE role_id = 8033;
UPDATE players SET role_id = 8007 WHERE role_id = 8031;
UPDATE players SET role_id = 8008 WHERE role_id = 8032;
UPDATE players SET role_id = 7999 WHERE role_id = 8023;
UPDATE players SET role_id = 8006 WHERE role_id = 8030;
UPDATE players SET role_id = 8001 WHERE role_id = 8025;
UPDATE players SET role_id = 8005 WHERE role_id = 8029;
UPDATE players SET role_id = 8010 WHERE role_id = 8034;
UPDATE players SET role_id = 8004 WHERE role_id = 8028;
UPDATE players SET role_id = 8003 WHERE role_id = 8027;
UPDATE players SET role_id = 8002 WHERE role_id = 8026;
UPDATE players SET role_id = 8000 WHERE role_id = 8024;
UPDATE players SET role_id = 8012 WHERE role_id = 8055;
UPDATE players SET role_id = 8014 WHERE role_id = 8057;
UPDATE players SET role_id = 8013 WHERE role_id = 8056;
UPDATE players SET role_id = 8022 WHERE role_id = 8065;
UPDATE players SET role_id = 8018 WHERE role_id = 8061;
UPDATE players SET role_id = 8011 WHERE role_id = 8054;
UPDATE players SET role_id = 8017 WHERE role_id = 8060;
UPDATE players SET role_id = 8016 WHERE role_id = 8059;
UPDATE players SET role_id = 8015 WHERE role_id = 8058;
UPDATE players SET role_id = 8021 WHERE role_id = 8064;
UPDATE players SET role_id = 8019 WHERE role_id = 8062;
UPDATE players SET role_id = 8020 WHERE role_id = 8063;
UPDATE players SET role_id = 8303 WHERE role_id = 8327;
UPDATE players SET role_id = 8299 WHERE role_id = 8323;
UPDATE players SET role_id = 8300 WHERE role_id = 8324;
UPDATE players SET role_id = 8309 WHERE role_id = 8333;
UPDATE players SET role_id = 8301 WHERE role_id = 8325;
UPDATE players SET role_id = 8306 WHERE role_id = 8330;
UPDATE players SET role_id = 8308 WHERE role_id = 8332;
UPDATE players SET role_id = 8304 WHERE role_id = 8328;
UPDATE players SET role_id = 8307 WHERE role_id = 8331;
UPDATE players SET role_id = 8305 WHERE role_id = 8329;
UPDATE players SET role_id = 8302 WHERE role_id = 8326;
UPDATE players SET role_id = 8298 WHERE role_id = 8322;
UPDATE players SET role_id = 8311 WHERE role_id = 8354;
UPDATE players SET role_id = 8313 WHERE role_id = 8356;
UPDATE players SET role_id = 8312 WHERE role_id = 8355;
UPDATE players SET role_id = 8318 WHERE role_id = 8361;
UPDATE players SET role_id = 8310 WHERE role_id = 8353;
UPDATE players SET role_id = 8317 WHERE role_id = 8360;
UPDATE players SET role_id = 8316 WHERE role_id = 8359;
UPDATE players SET role_id = 8315 WHERE role_id = 8358;
UPDATE players SET role_id = 8314 WHERE role_id = 8357;
UPDATE players SET role_id = 8321 WHERE role_id = 8364;
UPDATE players SET role_id = 8319 WHERE role_id = 8362;
UPDATE players SET role_id = 8320 WHERE role_id = 8363;
UPDATE players SET role_id = 11085 WHERE role_id = 16232;
UPDATE players SET role_id = 11120 WHERE role_id = 16237;
UPDATE players SET role_id = 12026 WHERE role_id = 16244;
UPDATE players SET role_id = 12301 WHERE role_id = 16252;
UPDATE players SET role_id = 12329 WHERE role_id = 16254;
UPDATE players SET role_id = 14274 WHERE role_id = 16309;
UPDATE players SET role_id = 15511 WHERE role_id = 15584;
UPDATE players SET role_id = 15496 WHERE role_id = 15529;
UPDATE players SET role_id = 15504 WHERE role_id = 15570;
UPDATE players SET role_id = 15497 WHERE role_id = 15530;
UPDATE players SET role_id = 15507 WHERE role_id = 15580;
UPDATE players SET role_id = 15487 WHERE role_id = 15550;
UPDATE players SET role_id = 15486 WHERE role_id = 15514;
UPDATE players SET role_id = 15499 WHERE role_id = 15562;
UPDATE players SET role_id = 15501 WHERE role_id = 15567;
UPDATE players SET role_id = 15503 WHERE role_id = 15538;
UPDATE players SET role_id = 15510 WHERE role_id = 15582;
UPDATE players SET role_id = 15495 WHERE role_id = 15526;
UPDATE players SET role_id = 15488 WHERE role_id = 15552;
UPDATE players SET role_id = 15489 WHERE role_id = 15515;
UPDATE players SET role_id = 15500 WHERE role_id = 15535;
UPDATE players SET role_id = 15492 WHERE role_id = 15521;
UPDATE players SET role_id = 15493 WHERE role_id = 15525;
UPDATE players SET role_id = 15506 WHERE role_id = 15576;
UPDATE players SET role_id = 15490 WHERE role_id = 15553;
UPDATE players SET role_id = 15494 WHERE role_id = 15560;
UPDATE players SET role_id = 15494 WHERE role_id = 16349;
UPDATE players SET role_id = 15484 WHERE role_id = 15512;
UPDATE players SET role_id = 15505 WHERE role_id = 15571;
UPDATE players SET role_id = 15509 WHERE role_id = 15544;
UPDATE players SET role_id = 15491 WHERE role_id = 15556;
UPDATE players SET role_id = 15578 WHERE role_id = 15594;
UPDATE players SET role_id = 15542 WHERE role_id = 15589;
UPDATE players SET role_id = 15528 WHERE role_id = 15598;
UPDATE players SET role_id = 15569 WHERE role_id = 15588;
UPDATE players SET role_id = 15557 WHERE role_id = 15595;
UPDATE players SET role_id = 15554 WHERE role_id = 15590;
UPDATE players SET role_id = 15545 WHERE role_id = 15597;
UPDATE players SET role_id = 15551 WHERE role_id = 15586;
UPDATE players SET role_id = 15527 WHERE role_id = 15596;
UPDATE players SET role_id = 15540 WHERE role_id = 15587;
UPDATE players SET role_id = 15547 WHERE role_id = 15585;
UPDATE players SET role_id = 15572 WHERE role_id = 15591;
UPDATE players SET role_id = 15522 WHERE role_id = 15593;
DELETE FROM roles WHERE id = 15685;
DELETE FROM roles WHERE id = 15684;
DELETE FROM roles WHERE id = 15688;
DELETE FROM roles WHERE id = 15701;
DELETE FROM roles WHERE id = 15704;
DELETE FROM roles WHERE id = 15711;
DELETE FROM roles WHERE id = 15719;
DELETE FROM roles WHERE id = 15720;
DELETE FROM roles WHERE id = 15731;
DELETE FROM roles WHERE id = 15740;
DELETE FROM roles WHERE id = 15741;
DELETE FROM roles WHERE id = 15780;
DELETE FROM roles WHERE id = 15784;
DELETE FROM roles WHERE id = 15790;
DELETE FROM roles WHERE id = 15819;
DELETE FROM roles WHERE id = 15831;
DELETE FROM roles WHERE id = 16007;
DELETE FROM roles WHERE id = 16008;
DELETE FROM roles WHERE id = 16011;
DELETE FROM roles WHERE id = 16036;
DELETE FROM roles WHERE id = 16052;
DELETE FROM roles WHERE id = 16107;
DELETE FROM roles WHERE id = 8033;
DELETE FROM roles WHERE id = 8031;
DELETE FROM roles WHERE id = 8032;
DELETE FROM roles WHERE id = 8023;
DELETE FROM roles WHERE id = 8030;
DELETE FROM roles WHERE id = 8025;
DELETE FROM roles WHERE id = 8029;
DELETE FROM roles WHERE id = 8034;
DELETE FROM roles WHERE id = 8028;
DELETE FROM roles WHERE id = 8027;
DELETE FROM roles WHERE id = 8026;
DELETE FROM roles WHERE id = 8024;
DELETE FROM roles WHERE id = 8055;
DELETE FROM roles WHERE id = 8057;
DELETE FROM roles WHERE id = 8056;
DELETE FROM roles WHERE id = 8065;
DELETE FROM roles WHERE id = 8061;
DELETE FROM roles WHERE id = 8054;
DELETE FROM roles WHERE id = 8060;
DELETE FROM roles WHERE id = 8059;
DELETE FROM roles WHERE id = 8058;
DELETE FROM roles WHERE id = 8064;
DELETE FROM roles WHERE id = 8062;
DELETE FROM roles WHERE id = 8063;
DELETE FROM roles WHERE id = 8327;
DELETE FROM roles WHERE id = 8323;
DELETE FROM roles WHERE id = 8324;
DELETE FROM roles WHERE id = 8333;
DELETE FROM roles WHERE id = 8325;
DELETE FROM roles WHERE id = 8330;
DELETE FROM roles WHERE id = 8332;
DELETE FROM roles WHERE id = 8328;
DELETE FROM roles WHERE id = 8331;
DELETE FROM roles WHERE id = 8329;
DELETE FROM roles WHERE id = 8326;
DELETE FROM roles WHERE id = 8322;
DELETE FROM roles WHERE id = 8354;
DELETE FROM roles WHERE id = 8356;
DELETE FROM roles WHERE id = 8355;
DELETE FROM roles WHERE id = 8361;
DELETE FROM roles WHERE id = 8353;
DELETE FROM roles WHERE id = 8360;
DELETE FROM roles WHERE id = 8359;
DELETE FROM roles WHERE id = 8358;
DELETE FROM roles WHERE id = 8357;
DELETE FROM roles WHERE id = 8364;
DELETE FROM roles WHERE id = 8362;
DELETE FROM roles WHERE id = 8363;
DELETE FROM roles WHERE id = 16232;
DELETE FROM roles WHERE id = 16237;
DELETE FROM roles WHERE id = 16244;
DELETE FROM roles WHERE id = 16252;
DELETE FROM roles WHERE id = 16254;
DELETE FROM roles WHERE id = 16309;
DELETE FROM roles WHERE id = 15584;
DELETE FROM roles WHERE id = 15529;
DELETE FROM roles WHERE id = 15570;
DELETE FROM roles WHERE id = 15530;
DELETE FROM roles WHERE id = 15580;
DELETE FROM roles WHERE id = 15550;
DELETE FROM roles WHERE id = 15514;
DELETE FROM roles WHERE id = 15562;
DELETE FROM roles WHERE id = 15567;
DELETE FROM roles WHERE id = 15538;
DELETE FROM roles WHERE id = 15582;
DELETE FROM roles WHERE id = 15526;
DELETE FROM roles WHERE id = 15552;
DELETE FROM roles WHERE id = 15515;
DELETE FROM roles WHERE id = 15535;
DELETE FROM roles WHERE id = 15521;
DELETE FROM roles WHERE id = 15525;
DELETE FROM roles WHERE id = 15576;
DELETE FROM roles WHERE id = 15553;
DELETE FROM roles WHERE id = 15560;
DELETE FROM roles WHERE id = 16349;
DELETE FROM roles WHERE id = 15512;
DELETE FROM roles WHERE id = 15571;
DELETE FROM roles WHERE id = 15544;
DELETE FROM roles WHERE id = 15556;
DELETE FROM roles WHERE id = 15594;
DELETE FROM roles WHERE id = 15589;
DELETE FROM roles WHERE id = 15598;
DELETE FROM roles WHERE id = 15588;
DELETE FROM roles WHERE id = 15595;
DELETE FROM roles WHERE id = 15590;
DELETE FROM roles WHERE id = 15597;
DELETE FROM roles WHERE id = 15586;
DELETE FROM roles WHERE id = 15596;
DELETE FROM roles WHERE id = 15587;
DELETE FROM roles WHERE id = 15585;
DELETE FROM roles WHERE id = 15591;
DELETE FROM roles WHERE id = 15593;

-- fixing 52
INSERT IGNORE INTO roles (name, faction_id) VALUES ('Assassin', 73);
INSERT INTO role_abilities (role_id, name)
	SELECT roles.id, 'Assassin' FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Assassin'
	AND replays.id = 52;
DELETE FROM hidden_roles WHERE hidden_id = 1093;
INSERT INTO hidden_roles (hidden_id, role_id)
	SELECT 1093, roles.id FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Assassin'
	AND replays.id = 52;
UPDATE players p
	INNER JOIN replays r ON r.id = p.replay_id
	INNER JOIN factions f ON f.setup_id = r.setup_id
	INNER JOIN roles ro ON ro.faction_id = f.id
	SET p.role_id = ro.id
	WHERE p.hidden_id = 1093
	AND ro.name = 'Assassin';

-- fixing 110
INSERT IGNORE INTO roles (name, faction_id) VALUES ('Sheriff', 170);
INSERT INTO role_abilities (role_id, name)
	SELECT roles.id, 'Sheriff' FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Sheriff'
	AND replays.id = 110;
DELETE FROM hidden_roles WHERE hidden_id = 1118;
INSERT INTO hidden_roles (hidden_id, role_id)
	SELECT 1118, roles.id FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Sheriff'
	AND replays.id = 110;
UPDATE players p
	INNER JOIN replays r ON r.id = p.replay_id
	INNER JOIN factions f ON f.setup_id = r.setup_id
	INNER JOIN roles ro ON ro.faction_id = f.id
	SET p.role_id = ro.id
	WHERE p.hidden_id = 1118
	AND ro.name = 'Sheriff';

-- fixing 123
INSERT IGNORE INTO roles (name, faction_id) VALUES ('Sheriff', 186);
INSERT INTO role_abilities (role_id, name)
	SELECT roles.id, 'Sheriff' FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Sheriff'
	AND replays.id = 123;
DELETE FROM hidden_roles WHERE hidden_id = 1134;
INSERT INTO hidden_roles (hidden_id, role_id)
	SELECT 1134, roles.id FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Sheriff'
	AND replays.id = 123;
UPDATE players p
	INNER JOIN replays r ON r.id = p.replay_id
	INNER JOIN factions f ON f.setup_id = r.setup_id
	INNER JOIN roles ro ON ro.faction_id = f.id
	SET p.role_id = ro.id
	WHERE p.hidden_id = 1134
	AND ro.name = 'Sheriff';

-- fixing 181
UPDATE hidden_roles SET role_id = 4946 WHERE hidden_id = 1189;
UPDATE players SET role_id = 4946 WHERE hidden_id = 1189;

-- fixing 204
INSERT IGNORE INTO roles (name, faction_id) VALUES ('Mayor', 309);
INSERT INTO role_abilities (role_id, name)
	SELECT roles.id, 'Mayor' FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Mayor'
	AND replays.id = 204;
DELETE FROM hidden_roles WHERE hidden_id = 1314;
INSERT INTO hidden_roles (hidden_id, role_id)
	SELECT 1314, roles.id FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Mayor'
	AND replays.id = 204;
UPDATE players p
	INNER JOIN replays r ON r.id = p.replay_id
	INNER JOIN factions f ON f.setup_id = r.setup_id
	INNER JOIN roles ro ON ro.faction_id = f.id
	SET p.role_id = ro.id
	WHERE p.hidden_id = 1314
	AND ro.name = 'Mayor';

-- fixing 207
INSERT IGNORE INTO roles (name, faction_id) VALUES ('Citizen', 315);
INSERT INTO role_abilities (role_id, name)
	SELECT roles.id, 'Citizen' FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Citizen'
	AND replays.id = 207;
DELETE FROM hidden_roles WHERE hidden_id = 1319;
INSERT INTO hidden_roles (hidden_id, role_id)
	SELECT 1319, roles.id FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Citizen'
	AND replays.id = 207;
UPDATE players p
	INNER JOIN replays r ON r.id = p.replay_id
	INNER JOIN factions f ON f.setup_id = r.setup_id
	INNER JOIN roles ro ON ro.faction_id = f.id
	SET p.role_id = ro.id
	WHERE p.hidden_id = 1319
	AND ro.name = 'Citizen';

-- fixing 209
INSERT IGNORE INTO roles (name, faction_id) VALUES ('Mayor', 319);
INSERT INTO role_abilities (role_id, name)
	SELECT roles.id, 'Mayor' FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Mayor'
	AND replays.id = 209;
DELETE FROM hidden_roles WHERE hidden_id = 1326;
INSERT INTO hidden_roles (hidden_id, role_id)
	SELECT 1326, roles.id FROM roles
	LEFT JOIN factions ON factions.id = roles.faction_id
	LEFT JOIN replays ON replays.setup_id = factions.setup_id
	WHERE roles.name = 'Mayor'
	AND replays.id = 209;
UPDATE players p
	INNER JOIN replays r ON r.id = p.replay_id
	INNER JOIN factions f ON f.setup_id = r.setup_id
	INNER JOIN roles ro ON ro.faction_id = f.id
	SET p.role_id = ro.id
	WHERE p.hidden_id = 1326
	AND ro.name = 'Mayor';

-- fixing 212
UPDATE replays SET setup_id = 209 WHERE id = 212;
UPDATE players p
	LEFT JOIN hiddens h ON p.hidden_id = h.id
	SET p.hidden_id = 1325, p.role_id = 15961
	WHERE h.name = 'Citizen'
	AND p.replay_id = 212;
UPDATE players p
	LEFT JOIN hiddens h ON p.hidden_id = h.id
	SET p.hidden_id = 1326, p.role_id = 29167
	WHERE h.name = 'Mayor'
	AND p.replay_id = 212;
UPDATE players p
	LEFT JOIN hiddens h ON p.hidden_id = h.id
	SET p.hidden_id = 1327, p.role_id = 15963
	WHERE h.name = 'Goon'
	AND p.replay_id = 212;

-- fixing 236
UPDATE replays SET setup_id = 209 WHERE id = 236;
UPDATE players p
	LEFT JOIN hiddens h ON p.hidden_id = h.id
	SET p.hidden_id = 1325, p.role_id = 15961
	WHERE h.name = 'Citizen'
	AND p.replay_id = 236;
UPDATE players p
	LEFT JOIN hiddens h ON p.hidden_id = h.id
	SET p.hidden_id = 1326, p.role_id = 29167
	WHERE h.name = 'Mayor'
	AND p.replay_id = 236;
UPDATE players p
	LEFT JOIN hiddens h ON p.hidden_id = h.id
	SET p.hidden_id = 1327, p.role_id = 15963
	WHERE h.name = 'Goon'
	AND p.replay_id = 236;

-- fixing 354
UPDATE commands SET counter = counter + 10
    WHERE counter > 98
    AND counter != 103
    AND replay_id = 354;

-- fixing 367
UPDATE commands SET counter = counter + 10
    WHERE counter > 173
    AND counter != 180
    AND replay_id = 367;
UPDATE commands SET counter = 181
	WHERE counter = 168
	AND replay_id = 367;

-- fixing 409
DELETE FROM hidden_roles WHERE role_id = 15942 AND hidden_id = 458;

-- fixing 562
DELETE FROM hidden_roles WHERE role_id = 4583 AND hidden_id = 631;
DELETE FROM hidden_roles WHERE role_id = 4637 AND hidden_id = 631;
DELETE FROM hidden_roles WHERE role_id = 4660 AND hidden_id = 631;
DELETE FROM hidden_roles WHERE role_id = 4923 AND hidden_id = 633;
DELETE FROM hidden_roles WHERE role_id = 4946 AND hidden_id = 633;
DELETE FROM hidden_roles WHERE role_id = 4969 AND hidden_id = 633;

-- fixing 616
DELETE FROM commands WHERE replay_id = 616 AND (counter = 9 OR counter = 10);

-- fixing 618
INSERT IGNORE INTO commands (counter, player_id, command, replay_id) VALUES (103, null, 'end phase', 618);

-- fixing 670
INSERT IGNORE INTO commands (counter, player_id, command, replay_id) VALUES (103, null, 'end phase', 670);
