ALTER TABLE setups ADD COLUMN is_active BOOLEAN;
UPDATE setups SET is_active = false;
ALTER TABLE setups CHANGE COLUMN is_active is_active BOOLEAN NOT NULL;
