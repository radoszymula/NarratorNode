package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FactionEnemyRepo extends BaseRepo {

    public static ArrayList<String[]> getEnemyColors(Connection connection, long setupID) throws SQLException {
        String query = "SELECT a.color enemy1, b.color enemy2 " + "FROM faction_enemies " + "LEFT JOIN factions a "
                + "ON a.id = faction_enemies.enemy_a " + "LEFT JOIN factions b " + "ON b.id = faction_enemies.enemy_b "
                + "WHERE a.setup_id = ?;";
        ResultSet rs = executeQuery(connection, query, setupID);
        ArrayList<String[]> enemyPairings = new ArrayList<>();
        String[] enemyPairing;
        while (rs.next()){
            enemyPairing = new String[] { rs.getString("enemy1"), rs.getString("enemy2")
            };
            enemyPairings.add(enemyPairing);
        }
        rs.close();
        return enemyPairings;
    }

    public static void create(Connection c, long factionID, long factionEnemyID) throws SQLException {
    	if(factionID > factionEnemyID)
    		create(c, factionEnemyID, factionID);
    	else
    		execute(c, "INSERT IGNORE INTO faction_enemies (enemy_a, enemy_b) VALUES (?, ?);", factionID, factionEnemyID);
    }

}
