package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import game.roles.Ability;
import models.schemas.FactionAbilitySchema;

public class FactionAbilityRepo extends AbilityRepo {

    public static Map<Long, Map<Long, FactionAbilitySchema>> getBySetupID(Connection connection, long setupID)
            throws SQLException {
        Map<Long, Map<Long, FactionAbilitySchema>> factionAbilities = new HashMap<>();
        StringBuilder query = new StringBuilder();
        query.append("SELECT faction_id, fa.id, fa.name FROM faction_abilities AS fa ");
        query.append("LEFT JOIN factions ON factions.id = fa.faction_id ");
        query.append("WHERE setup_id=?;");
        ResultSet rs = executeQuery(connection, query.toString(), setupID);

        long factionID;
        FactionAbilitySchema factionAbility;
        while (rs.next()){
            factionID = rs.getLong("faction_id");
            factionAbility = new FactionAbilitySchema();
            factionAbility.name = rs.getString("name");
            factionAbility.id = rs.getLong("id");
            if(!factionAbilities.containsKey(factionID))
                factionAbilities.put(factionID, new HashMap<>());
            factionAbilities.get(factionID).put(factionAbility.id, factionAbility);
        }
        rs.close();

        return factionAbilities;
    }

    public static ArrayList<Long> create(Connection connection, ArrayList<Long> factionAbilityFactionIDs,
            ArrayList<Ability> factionAbilities) throws SQLException {
        return insertAbilities(connection, "faction_abilities", "faction_id", factionAbilityFactionIDs,
                factionAbilities);
    }

}
