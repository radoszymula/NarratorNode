package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.schemas.PlayerSchema;

public class PlayerRepo extends BaseRepo {

    public static void setExited(Connection c, long replayID, long userID) throws SQLException {
        execute(c, "UPDATE players SET is_exited = true WHERE replay_id=? and user_id=?;", replayID, userID);
    }

    public static void setExitedForAllPlayers(Connection c, long replayID) throws SQLException {
        execute(c, "UPDATE players SET is_exited = true WHERE replay_id=?;", replayID);
    }

    public static boolean getIsExited(Connection c, long replayID, long userID) throws SQLException {
        ResultSet rs = executeQuery(c, "SELECT is_exited FROM players WHERE replay_id=? AND user_id=?;", replayID,
                userID);
        if(!rs.next()){
            rs.close();
            return false;
        }
        boolean isExited = rs.getBoolean("is_exited");
        rs.close();
        return isExited;
    }

    public static ArrayList<PlayerSchema> getByReplayID(Connection conn, long replayID) throws SQLException {
        ResultSet rs = executeQuery(conn,
                "SELECT id, name, faction_role_id, hidden_id, user_id, is_exited FROM players WHERE replay_id = ?;",
                replayID);
        ArrayList<PlayerSchema> players = new ArrayList<>();
        PlayerSchema player;
        while (rs.next()){
            player = new PlayerSchema();
            player.id = rs.getLong("id");
            player.name = rs.getString("name");
            player.factionRoleID = rs.getLong("faction_role_id");
            player.hiddenID = rs.getLong("hidden_id");
            player.userID = rs.getLong("user_id");
            player.isExited = rs.getBoolean("is_exited");
            players.add(player);
        }
        rs.close();
        return players;

    }

    public static void updateDeathDayAndIsWinner(Connection connection, long replayID, String name, short deathDay,
            boolean isWinner) throws SQLException {
        execute(connection, "UPDATE players SET is_winner = ?, death_day = ? WHERE replay_id = ? AND name = ?;",
                isWinner, deathDay, replayID, name);
    }

    public static long getUserID(Connection connection, long replayID, String name) throws SQLException {
        ResultSet rs = executeQuery(connection, "SELECT user_id FROM players WHERE replay_id = ? and NAME = ?;",
                replayID, name);
        rs.next();
        long userID = rs.getLong("user_id");
        rs.close();
        return userID;
    }

    public static void clearIsWinnerAndDeathDay(Connection connection) throws SQLException {
        execute(connection, "UPDATE players SET is_winner = null, death_day = null;");
    }

    public static long create(Connection c, long replayID, long userID, String name, long hiddenID, long factionRoleID,
            boolean isComputer) throws SQLException {
        return executeInsertQuery(c,
                "INSERT INTO players (replay_id, user_id, name, hidden_id, faction_role_id, is_exited) VALUES (?, ?, ?, ?, ?, ?);",
                replayID, userID, name, hiddenID, factionRoleID, isComputer).get(0);
    }

    public static long create(Connection c, long replayID, long userID, String name, boolean isComputer)
            throws SQLException {
        return executeInsertQuery(c, "INSERT INTO players (replay_id, user_id, name, is_exited) VALUES (?, ?, ?, ?);",
                replayID, userID, name, isComputer).get(0);
    }

    public static void deleteByReplayID(Connection connection, long replayID) throws SQLException {
        execute(connection, "DELETE FROM players WHERE replay_id = ?;", replayID);
    }

    public static boolean existsByGameIDAndUserID(Connection connection, long replayID, long userID)
            throws SQLException {
        ResultSet rs = executeQuery(connection, "SELECT (1) FROM players WHERE replay_id = ? AND user_id = ?;",
                replayID, userID);
        boolean result = rs.first();
        rs.close();
        return result;
    }

    public static void updateRoleAssignment(Connection connection, long gameID, long userID, long hiddenID,
            long factionRoleID) throws SQLException {
        execute(connection,
                "UPDATE players SET hidden_id = ?, faction_role_id = ? WHERE replay_id = ? AND user_id = ?;", hiddenID,
                factionRoleID, gameID, userID);

    }

    public static void deleteByUserAndGame(Connection connection, long leaverID, long replayID) throws SQLException {
        execute(connection, "DELETE FROM players WHERE replay_id = ? AND user_id = ?;", replayID, leaverID);
    }

    public static long getByNameAndGameID(Connection connection, String command, String playerName, long game_id)
            throws SQLException {
        ResultSet rs = executeQuery(connection, "SELECT id FROM players WHERE replay_id = ? AND name = ?;", game_id,
                playerName);
        long playerID = rs.getLong("id");
        rs.close();
        return playerID;
    }
}
