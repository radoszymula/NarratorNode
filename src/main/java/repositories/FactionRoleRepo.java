package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.FactionRole;
import models.schemas.FactionRoleSchema;

public class FactionRoleRepo extends BaseRepo {

    public static long create(Connection connection, long roleID, long factionID, long setupID) throws SQLException {
        return executeInsertQuery(connection,
                "INSERT INTO faction_roles (faction_id, role_id, setup_id) VALUES (?, ?, ?);", factionID, roleID,
                setupID).get(0);
    }

    public static ArrayList<Long> create(Connection connection, long setupID, List<FactionRole> factionRoles)
            throws SQLException {
        if(factionRoles.isEmpty())
            return new ArrayList<>();
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO faction_roles (faction_id, role_id, setup_id) VALUES ");
        for(FactionRole factionRole: factionRoles){
            query.append("(");
            query.append(factionRole.faction.id);
            query.append(", ");
            query.append(factionRole.role.getID());
            query.append(", ");
            query.append(setupID);
            query.append("), ");
        }
        query.replace(query.length() - 2, query.length(), ";");
        return executeInsertQuery(connection, query.toString());
    }

    public static Map<Long, FactionRoleSchema> getBySetupID(Connection connection, long setupID) throws SQLException {
        ResultSet results = executeQuery(connection,
                "SELECT faction_id, role_id, id, received_faction_role_id FROM faction_roles WHERE setup_id = ?;",
                setupID);
        Map<Long, FactionRoleSchema> factionRoles = new HashMap<>();
        FactionRoleSchema factionRole;
        while (results.next()){
            factionRole = new FactionRoleSchema();
            factionRole.factionID = results.getLong("faction_id");
            factionRole.roleID = results.getLong("role_id");
            factionRole.id = results.getLong("id");
            factionRole.receivedFactionRoleID = results.getLong("received_faction_role_id");
            if(results.wasNull())
                factionRole.receivedFactionRoleID = null;
            factionRoles.put(factionRole.id, factionRole);
        }
        return factionRoles;
    }

    public static void updateRoleReceived(Connection connection, FactionRole factionRole) throws SQLException {
        execute(connection, "UPDATE faction_roles SET received_faction_role_id = ? WHERE id = ?;",
                factionRole.receivedRole.id, factionRole.id);
    }

    public static void deleteSetRoleReceive(Connection connection, long factionRoleID, long setupID)
            throws SQLException {
        execute(connection, "DELETE FROM faction_roles WHERE setup_id = ? AND received_faction_role_id = ?", setupID,
                factionRoleID);
    }

    public static void deleteByIDs(Connection connection, long setupID, ArrayList<Long> factionRoleIDs)
            throws SQLException {
        if(factionRoleIDs.isEmpty())
            return;
        StringBuilder query = new StringBuilder("");
        query.append("DELETE FROM faction_roles WHERE setup_id = ? AND (");

        long factionRoleID;
        for(int i = 0; i < factionRoleIDs.size(); i++){
            if(i != 0)
                query.append(" OR ");
            factionRoleID = factionRoleIDs.get(i);
            query.append("id = ");
            query.append(factionRoleID);
        }

        query.append(");");
        execute(connection, query.toString(), setupID);
    }

}
