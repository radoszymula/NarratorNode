package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import game.logic.Narrator;
import game.logic.support.StoryPackage;
import game.setups.Setup;
import nnode.DBStoryPackage;

public class SetupRepo extends BaseRepo {

    public static Setup create(Connection connection, Setup setup) throws SQLException {
        long setupID = executeInsertQuery(connection,
                "INSERT INTO setups (owner_id, name, is_active) VALUES (?, ?, true)", setup.ownerID, setup.getName())
                        .get(0);
        setup.id = setupID;
        return setup;
    }

    public static List<Setup> getActiveByOwnerID(Connection c, long userID) throws SQLException {
        ResultSet rs = executeQuery(c, "SELECT id, name FROM setups WHERE owner_id = ? AND is_active = true;", userID);
        ArrayList<Setup> setups = new ArrayList<>();
        Setup setup;
        String name;
        Narrator narrator = null;
        while (rs.next()){
            setup = new Setup();
            name = rs.getString("name");
            setup.id = rs.getLong("id");
            setup.init(narrator, name);
            setups.add(setup);
        }
        rs.close();
        return setups;
    }

    public static Setup getSetup(Connection connection, long setupID, Narrator narrator) throws SQLException {
        long setupOwnerID = getOwnerID(connection, setupID);
        DBStoryPackage storyPackage = new DBStoryPackage(connection);
        storyPackage.setupID = setupID;
        Setup setup = new Setup();
        setup.narrator = narrator;
        StoryPackage.getSetupInfo(storyPackage, narrator);
        setup.id = setupID;
        setup.ownerID = setupOwnerID;
        return setup;
    }

    public static void deleteAll(Connection connection) throws SQLException {
        execute(connection, "DELETE FROM setups;");
    }

    public static void deleteByID(Connection connection, Long setupID) throws SQLException {
        execute(connection, "DELETE FROM setups WHERE id = ?;", setupID);
    }

    public static int getCount(Connection connection) throws SQLException {
        ResultSet rs = executeQuery(connection, "SELECT count(*) as count FROM setups;");
        Integer count = null;
        while (rs.next()){
            count = rs.getInt("count");
        }
        rs.close();
        return count;
    }

    public static long getOwnerID(Connection connection, long setupID) throws SQLException {
        ResultSet rs = executeQuery(connection, "SELECT owner_id as ownerID FROM setups WHERE id = ?;", setupID);
        rs.next();
        long ownerID = rs.getLong("ownerID");
        rs.close();
        return ownerID;
    }

}
