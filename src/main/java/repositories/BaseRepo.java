package repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;

public class BaseRepo {
    protected static ResultSet executeQuery(Connection dbConn, String sqlQuery, Object... parameters) throws SQLException {
        if(dbConn == null)
            throw new SQLException("Connection was null for query: " + sqlQuery);
        PreparedStatement s = null;
        try{
            s = dbConn.prepareStatement(sqlQuery, parameters);
            ResultSet rs = s.executeQuery();
            s.close();
            return rs;
        }catch(SQLException sq){
            if(!(sq instanceof SQLTimeoutException))
                throw sq;
        }

        dbConn.attemptRestart();
        s = dbConn.prepareStatement(sqlQuery, parameters);
        ResultSet rs = s.executeQuery();
        s.close();
        return rs;
    }

    protected static void execute(Connection connection, String sqlQuery, Object... parameters) throws SQLException {
        if(connection == null)
            throw new SQLException("Connection was null for query: " + sqlQuery);
        PreparedStatement statement = null;
        try{
            statement = connection.prepareStatement(sqlQuery, parameters);
            statement.execute();
            statement.close();
            return;
        }catch(SQLException sq){
            if(!(sq instanceof SQLTimeoutException))
                throw sq;
        }

        connection.attemptRestart();
        statement = connection.prepareStatement(sqlQuery, parameters);
        statement.execute();
        statement.close();
    }

    protected static ArrayList<Long> executeInsertQuery(Connection dbConn, String sqlQuery, Object... parameters)
            throws SQLException {
        if(dbConn == null)
            throw new SQLException("Connection was null for query: " + sqlQuery);
        PreparedStatement statement = null;
        try{
            statement = dbConn.prepareInsertStatement(sqlQuery, parameters);
            return insertAndGetIDs(statement);

        }catch(SQLException sq){
            if(!(sq instanceof SQLTimeoutException))
                throw sq;
        }

        dbConn.attemptRestart();
        statement = dbConn.prepareInsertStatement(sqlQuery, parameters);
        return insertAndGetIDs(statement);
    }

	private static ArrayList<Long> insertAndGetIDs(PreparedStatement statement) throws SQLException {
		ArrayList<Long> queryIDs = new ArrayList<>();
		statement.executeUpdate();
		ResultSet resultSet = statement.getGeneratedKeys();
		statement.close();

		while(resultSet.next()) {
			queryIDs.add((long) resultSet.getInt(1));
		}
		resultSet.close();

		return queryIDs;
	}
}
