package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Role;
import game.logic.support.rules.RoleModifier;
import models.Modifiers;
import models.schemas.RoleModifierSchema;

public class RoleModifierRepo extends ModifierRepo {

    // it looks like this is only used for emporium data
    public static Set<RoleModifierSchema> getByRoleID(Connection connection, long roleID) throws SQLException {
        Set<RoleModifierSchema> modifiers = new HashSet<>();
        ResultSet rs = executeQuery(connection, "SELECT name, value FROM role_int_modifiers WHERE role_id=?;", roleID);
        RoleModifierSchema modifier;
        while (rs.next()){
            modifier = new RoleModifierSchema();
            modifier.intValue = rs.getInt("value");
            modifier.name = rs.getString("name");
            modifiers.add(modifier);
        }
        rs.close();

        rs = executeQuery(connection, "SELECT name, value FROM role_bool_modifiers WHERE role_id=?;", roleID);
        while (rs.next()){
            modifier = new RoleModifierSchema();
            modifier.boolValue = rs.getBoolean("value");
            modifier.name = rs.getString("name");
            modifiers.add(modifier);
        }
        rs.close();

        return modifiers;
    }

    public static Map<Long, Set<RoleModifierSchema>> getBySetupID(Connection connection, long setupID)
            throws SQLException {
        Map<Long, Set<RoleModifierSchema>> modifiers = new HashMap<>();

        StringBuilder query = new StringBuilder();
        query.append("SELECT role_id, rim.name, value FROM role_int_modifiers AS rim ");
        query.append("LEFT JOIN roles ON roles.id = rim.role_id ");
        query.append("WHERE setup_id = ?;");
        ResultSet rs = executeQuery(connection, query.toString(), setupID);
        RoleModifierSchema modifier;
        long roleID;
        while (rs.next()){
            roleID = rs.getLong("role_id");
            modifier = new RoleModifierSchema();
            modifier.intValue = rs.getInt("value");
            modifier.name = rs.getString("name");
            if(!modifiers.containsKey(roleID))
                modifiers.put(roleID, new HashSet<>());
            modifiers.get(roleID).add(modifier);
        }

        query = new StringBuilder();
        query.append("SELECT role_id, rbm.name, value FROM role_bool_modifiers AS rbm ");
        query.append("LEFT JOIN roles ON roles.id = rbm.role_id ");
        query.append("WHERE setup_id = ?;");
        rs = executeQuery(connection, query.toString(), setupID);
        while (rs.next()){
            roleID = rs.getLong("role_id");
            modifier = new RoleModifierSchema();
            modifier.boolValue = rs.getBoolean("value");
            modifier.name = rs.getString("name");
            if(!modifiers.containsKey(roleID))
                modifiers.put(roleID, new HashSet<>());
            modifiers.get(roleID).add(modifier);
        }

        return modifiers;
    }

    public static void create(Connection connection, List<Role> roles) throws SQLException {
        ArrayList<Long> roleIDs = new ArrayList<>();
        ArrayList<Modifiers<RoleModifier>> modifiersList = new ArrayList<>();
        for(Role role: roles){
            roleIDs.add(role.getID());
            modifiersList.add(role.modifiers);
        }
        insertModifiers(connection, "role_int_modifiers", "role_bool_modifiers", "role_id", roleIDs, modifiersList);
    }

    public static void updateModifier(Connection connection, long roleID, RoleModifier modifier, boolean value)
            throws SQLException {
        execute(connection, "INSERT INTO role_bool_modifiers (role_id, name, value) VALUES (?, ?, ?);", roleID,
                modifier.toString(), value);
    }

    public static void updateModifier(Connection connection, long roleID, RoleModifier modifier, int value)
            throws SQLException {
        execute(connection, "INSERT INTO role_int_modifiers (role_id, name, value) VALUES (?, ?, ?);", roleID,
                modifier.toString(), value);
    }

}
