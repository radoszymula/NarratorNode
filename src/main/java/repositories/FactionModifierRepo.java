package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Faction;
import game.logic.support.rules.FactionModifier;
import models.Modifiers;
import models.schemas.FactionModifierSchema;

public class FactionModifierRepo extends ModifierRepo {

    public static Map<Long, Set<FactionModifierSchema>> getBySetupID(Connection connection, long setupID)
            throws SQLException {
        Map<Long, Set<FactionModifierSchema>> factionsModifiers = new HashMap<>();
        StringBuilder query = new StringBuilder();
        query.append("SELECT faction_id, fim.name, value ");
        query.append("FROM faction_int_modifiers AS fim ");
        query.append("LEFT JOIN factions ON factions.id = fim.faction_id ");
        query.append("WHERE setup_id=?;");
        ResultSet rs = executeQuery(connection, query.toString(), setupID);
        FactionModifierSchema modifier;
        long factionID;
        while (rs.next()){
            factionID = rs.getInt("faction_id");
            modifier = new FactionModifierSchema();
            modifier.intValue = rs.getInt("value");
            modifier.name = rs.getString("name");
            if(!factionsModifiers.containsKey(factionID))
                factionsModifiers.put(factionID, new HashSet<>());
            factionsModifiers.get(factionID).add(modifier);
        }
        rs.close();

        query = new StringBuilder();
        query.append("SELECT faction_id, fib.name, value ");
        query.append("FROM faction_bool_modifiers AS fib ");
        query.append("LEFT JOIN factions ON factions.id = fib.faction_id ");
        query.append("WHERE setup_id=?;");
        rs = executeQuery(connection, query.toString(), setupID);
        while (rs.next()){
            factionID = rs.getInt("faction_id");
            modifier = new FactionModifierSchema();
            modifier.boolValue = rs.getBoolean("value");
            modifier.name = rs.getString("name");
            factionsModifiers.get(factionID).add(modifier);
        }
        rs.close();

        return factionsModifiers;
    }

    public static void create(Connection connection, List<Faction> factions) throws SQLException {
        ArrayList<Modifiers<FactionModifier>> modifiersList = new ArrayList<>();
        ArrayList<Long> factionIDs = new ArrayList<>();
        for(Faction faction: factions){
            modifiersList.add(faction._modifierMap);
            factionIDs.add(faction.id);
        }
        insertModifiers(connection, "faction_int_modifiers", "faction_bool_modifiers", "faction_id", factionIDs,
                modifiersList);
    }

    public static void updateModifier(Connection connection, FactionModifier modifier, boolean val, long factionID)
            throws SQLException {
        execute(connection, "UPDATE faction_bool_modifiers SET value = ? WHERE name = ? AND faction_id = ?;", val,
                modifier.toString(), factionID);
    }

    public static void updateModifier(Connection connection, FactionModifier modifier, int val, long factionID)
            throws SQLException {
        execute(connection, "UPDATE faction_int_modifiers SET value = ? WHERE name = ? AND faction_id = ?;", val,
                modifier.toString(), factionID);
    }

}
