package repositories;

import java.sql.SQLException;

public class PlayerDeathRepo extends BaseRepo {

    public static void createDeath(Connection c, long replayID, String name, String deathType) throws SQLException {
        execute(c,
                "INSERT INTO player_deaths (player_id, death_type) SELECT id, ? FROM players WHERE replay_id = ? AND name = ?;",
                deathType, replayID, name);
        ;
    }

    public static void deleteAll(Connection c) throws SQLException {
        execute(c, "DELETE FROM player_deaths;");
    }

}
