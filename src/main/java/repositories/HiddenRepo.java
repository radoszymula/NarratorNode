package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import game.roles.Hidden;
import models.schemas.HiddenSchema;

public class HiddenRepo extends BaseRepo {

    public static Map<Long, HiddenSchema> getBySetupID(Connection connection, long setupID) throws SQLException {
        ResultSet rs = executeQuery(connection, "SELECT id, name FROM hiddens WHERE setup_id = ?;", setupID);
        Map<Long, HiddenSchema> hiddens = new HashMap<>();
        HiddenSchema hidden;
        while (rs.next()){
            hidden = new HiddenSchema();
            hidden.id = rs.getLong("id");
            hidden.name = rs.getString("name");
            hiddens.put(hidden.id, hidden);
        }
        rs.close();
        return hiddens;
    }

    public static HiddenSchema getByID(Connection conn, long hiddenID) throws SQLException {
        ResultSet rs = executeQuery(conn, "SELECT id, name FROM hiddens WHERE id = ?;", hiddenID);
        HiddenSchema hidden = null;
        while (rs.next()){
            hidden = new HiddenSchema();
            hidden.id = rs.getLong("id");
            hidden.name = rs.getString("name");
        }
        rs.close();
        return hidden;
    }

    public static ArrayList<Long> create(Connection connection, long setupID, ArrayList<Hidden> hiddens)
            throws SQLException {
        if(hiddens.isEmpty())
            return new ArrayList<>();
        Object[] params = new Object[hiddens.size() * 2];
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO hiddens (setup_id, name) VALUES");

        for(int i = 0; i < hiddens.size(); i++){
            if(i != 0)
                query.append(", ");
            params[i * 2] = setupID;
            params[(i * 2) + 1] = hiddens.get(i).getName();
            query.append("(?, ?)");
        }
        query.append(";");
        return BaseRepo.executeInsertQuery(connection, query.toString(), params);
    }

    public static void deleteByIDs(Connection connection, long setupID, ArrayList<Long> hiddenIDs) throws SQLException {
        if(hiddenIDs.isEmpty())
            return;
        StringBuilder query = new StringBuilder("");
        query.append("DELETE FROM hiddens WHERE setup_id = ? AND (");

        long hiddenID;
        for(int i = 0; i < hiddenIDs.size(); i++){
            if(i != 0)
                query.append(" OR ");
            hiddenID = hiddenIDs.get(i);
            query.append("id = ");
            query.append(hiddenID);
        }

        query.append(");");
        execute(connection, query.toString(), setupID);
    }

}
