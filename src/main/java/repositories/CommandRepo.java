package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.Command;

public class CommandRepo extends BaseRepo {

    public static ArrayList<Command> getByReplayID(Connection conn, long setupID) throws SQLException {
        ArrayList<Command> commands = new ArrayList<>();
        ResultSet rs = executeQuery(conn,
                "SELECT command, player_id as playerID from commands WHERE replay_id = ? ORDER BY counter;", setupID);
        String command;
        Long playerID;
        while (rs.next()){
            playerID = rs.getLong("playerID");
            command = rs.getString("command");
            commands.add(new Command(playerID, command));
        }
        rs.close();
        return commands;
    }

    public static void create(Connection conn, long counter, Long playerID, String command, long replayID)
            throws SQLException {
        if(playerID == null)
            execute(conn, "INSERT INTO commands (counter, player_id, command, replay_id) VALUES (?, NULL, ?, ?);",
                    counter, command, replayID);
        else
            execute(conn, "INSERT INTO commands (counter, player_id, command, replay_id) VALUES (?, ?, ?, ?);", counter,
                    playerID, command, replayID);
    }

    public static void deleteByReplayID(Connection connection, long replayID) throws SQLException {
        execute(connection, "DELETE FROM commands WHERE replay_id = ?;", replayID);
    }

}
