package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import game.logic.Role;
import models.schemas.RoleSchema;

public class RoleRepo extends BaseRepo {

    public static Map<Long, RoleSchema> getBySetupID(Connection connection, long setupID) throws SQLException {
        Map<Long, RoleSchema> roles = new HashMap<>();
        String queryText = "SELECT id, name FROM roles WHERE setup_id = ?;";
        ResultSet rs = executeQuery(connection, queryText, setupID);
        RoleSchema roleSchema;
        while (rs.next()){
            roleSchema = new RoleSchema();
            roleSchema.id = rs.getLong("id");
            roleSchema.name = rs.getString("name");
            roles.put(roleSchema.id, roleSchema);
        }
        rs.close();
        return roles;
    }

    public static ArrayList<Long> create(Connection connection, long setupID, ArrayList<Role> roles)
            throws SQLException {
        if(roles.isEmpty())
            return new ArrayList<>();
        Object[] params = new Object[roles.size()];
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO roles (setup_id, name) VALUES");

        Role role;
        for(int i = 0; i < roles.size(); i++){
            role = roles.get(i);
            if(i != 0)
                query.append(", ");
            query.append("(");
            query.append(setupID);
            query.append(", ?)");
            params[i] = role.getName();
        }
        query.append(";");
        return executeInsertQuery(connection, query.toString(), params);
    }

    public static void deleteByIDs(Connection connection, long setupID, ArrayList<Long> roleIDs) throws SQLException {
        if(roleIDs.isEmpty())
            return;
        StringBuilder query = new StringBuilder("");
        query.append("DELETE FROM roles WHERE setup_id = ? AND (");

        long roleID;
        for(int i = 0; i < roleIDs.size(); i++){
            if(i != 0)
                query.append(" OR ");
            roleID = roleIDs.get(i);
            query.append("id = ");
            query.append(roleID);
        }

        query.append(");");
        execute(connection, query.toString(), setupID);
    }

    public static List<RoleSchema> getAllDistinct(Connection connection) throws SQLException {
        List<RoleSchema> roles = new LinkedList<>();
        ResultSet rs = executeQuery(connection, "SELECT roles.id, roles.name FROM roles GROUP BY roles.name;");
        RoleSchema roleSchema;
        while (rs.next()){
            roleSchema = new RoleSchema();
            roleSchema.id = rs.getLong("id");
            roleSchema.name = rs.getString("name");
            roleSchema.modifiers = RoleModifierRepo.getByRoleID(connection, roleSchema.id);
            roleSchema.abilities = RoleAbilityRepo.getByRoleID(connection, roleSchema.id);
            roles.add(roleSchema);
        }
        rs.close();
        return roles;
    }
}
