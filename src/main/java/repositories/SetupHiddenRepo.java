package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.schemas.SetupHiddenSchema;

public class SetupHiddenRepo extends BaseRepo {
    public static ArrayList<SetupHiddenSchema> getBySetupID(Connection connection, long setupID) throws SQLException {
        SetupHiddenSchema setupHiddenSchema;
        ArrayList<SetupHiddenSchema> selectedHiddens = new ArrayList<>();
        ResultSet rs = executeQuery(connection,
                "SELECT hidden_id, id, is_exposed FROM setup_hiddens WHERE setup_id = ?;", setupID);
        while (rs.next()){
            setupHiddenSchema = new SetupHiddenSchema();
            setupHiddenSchema.hiddenID = rs.getLong("hidden_id");
            setupHiddenSchema.id = rs.getLong("id");
            setupHiddenSchema.isExposed = rs.getBoolean("is_exposed");
            selectedHiddens.add(setupHiddenSchema);
        }
        rs.close();
        return selectedHiddens;

    }

    public static long create(Connection connection, long hiddenID, long setupID) throws SQLException {
        return executeInsertQuery(connection, "INSERT INTO setup_hiddens (hidden_id, setup_id) VALUES (?, ?);",
                hiddenID, setupID).get(0);
    }

    public static void delete(Connection connection, long setupHiddenID, long setupID) throws SQLException {
        execute(connection, "DELETE FROM setup_hiddens WHERE setup_id = ? AND id = ?;", setupID, setupHiddenID);
    }

    public static void setExposed(Connection connection, long setupHiddenID, boolean isExposed) throws SQLException {
        execute(connection, "UPDATE setup_hiddens SET is_exposed = ? WHERE id = ?;", isExposed, setupHiddenID);
    }
}
