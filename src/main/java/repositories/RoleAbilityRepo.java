package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import game.roles.Ability;
import models.schemas.RoleAbilitySchema;

public class RoleAbilityRepo extends AbilityRepo {

    public static ArrayList<Long> create(Connection connection, ArrayList<Long> roleAbilityRoleIDs,
            ArrayList<Ability> roleAbilities) throws SQLException {
        return insertAbilities(connection, "role_abilities", "role_id", roleAbilityRoleIDs, roleAbilities);
    }

    public static Map<Long, RoleAbilitySchema> getByRoleID(Connection connection, long roleID) throws SQLException {
        Map<Long, RoleAbilitySchema> roleAbilities = new HashMap<>();
        ResultSet rs = executeQuery(connection, "SELECT id, name FROM role_abilities WHERE role_id = ?;", roleID);
        RoleAbilitySchema roleAbility;
        while (rs.next()){
            roleAbility = new RoleAbilitySchema();
            roleAbility.id = rs.getLong("id");
            roleAbility.name = rs.getString("name");
            roleAbility.modifiers = RoleAbilityModifierRepo.getByRoleAbilityID(connection, roleAbility.id);
            roleAbilities.put(roleAbility.id, roleAbility);
        }
        rs.close();

        return roleAbilities;
    }

    public static Map<Long, Map<Long, RoleAbilitySchema>> getBySetupID(Connection connection, long setupID)
            throws SQLException {
        Map<Long, Map<Long, RoleAbilitySchema>> abilities = new HashMap<>();

        StringBuilder query = new StringBuilder();
        query.append("SELECT role_id, ra.id, ra.name FROM role_abilities AS ra ");
        query.append("LEFT JOIN roles ON roles.id = ra.role_id ");
        query.append("WHERE setup_id = ?;");
        ResultSet rs = executeQuery(connection, query.toString(), setupID);

        long roleID;
        RoleAbilitySchema roleAbility;
        while (rs.next()){
            roleID = rs.getLong("role_id");
            if(!abilities.containsKey(roleID))
                abilities.put(roleID, new HashMap<>());

            roleAbility = new RoleAbilitySchema();
            roleAbility.name = rs.getString("name");
            roleAbility.id = rs.getLong("id");
            abilities.get(roleID).put(roleAbility.id, roleAbility);
        }

        return abilities;
    }

}
