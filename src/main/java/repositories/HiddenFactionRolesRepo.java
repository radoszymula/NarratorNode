package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.roles.Hidden;
import models.FactionRole;

public class HiddenFactionRolesRepo extends BaseRepo {

    public static Map<Long, Set<Long>> getBySetupID(Connection connection, long setupID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT hidden_id, faction_role_id FROM hidden_faction_roles ");
        query.append("WHERE setup_id = ?;");
        ResultSet rs = executeQuery(connection, query.toString(), setupID);
        Map<Long, Set<Long>> roleIDs = new HashMap<>();

        long hiddenID;
        while (rs.next()){
            hiddenID = rs.getLong("hidden_id");
            if(!roleIDs.containsKey(hiddenID))
                roleIDs.put(hiddenID, new HashSet<>());
            roleIDs.get(hiddenID).add(rs.getLong("faction_role_id"));
        }
        rs.close();
        return roleIDs;
    }

    // I don't remember why this needs to be ignore.
    public static void create(Connection connection, long setupID, List<Hidden> hiddens) throws SQLException {
        if(hiddens.isEmpty())
            return;
        StringBuilder query = new StringBuilder();
        query.append("INSERT IGNORE INTO hidden_faction_roles (hidden_id, faction_role_id, setup_id) VALUES ");
        for(Hidden hidden: hiddens){
            for(FactionRole factionRole: hidden.factionRolesMap.values()){
                query.append("(");
                query.append(hidden.getID());
                query.append(", ");
                query.append(factionRole.id);
                query.append(", ");
                query.append(setupID);
                query.append("), ");
            }
        }
        query.replace(query.length() - 2, query.length(), ";");
        execute(connection, query.toString());
    }

}
