package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import game.logic.support.rules.Rule;
import game.logic.support.rules.RuleBool;
import game.logic.support.rules.RuleInt;
import game.logic.support.rules.SetupModifier;

public class SetupModifierRepo extends BaseRepo {

    public static void updateModifier(Connection connection, SetupModifier name, int value, long setupID)
            throws SQLException {
        execute(connection, "UPDATE setup_int_modifiers SET value = ? WHERE name = ? AND setup_id = ?;", value,
                name.toString(), setupID);
    }

    public static void updateModifier(Connection connection, SetupModifier name, boolean value, long setupID)
            throws SQLException {
        execute(connection, "UPDATE setup_bool_modifiers SET value = ? WHERE name = ? AND setup_id = ?;", value,
                name.toString(), setupID);
    }

    public static void create(Connection conn, long setupID, String name, boolean value) throws SQLException {
        execute(conn, "INSERT INTO setup_bool_modifiers (setup_id, name, value) VALUES (?, ?, ?);", setupID, name,
                value);
    }

    public static ArrayList<Rule> getBySetupID(Connection conn, long setupID) throws SQLException {
        ArrayList<Rule> rules = new ArrayList<>();

        ResultSet rs = executeQuery(conn, "SELECT name, value FROM setup_bool_modifiers WHERE setup_id = ?;", setupID);

        String name;
        boolean boolVal;
        while (rs.next()){
            name = rs.getString("name");
            boolVal = rs.getBoolean("value");
            rules.add(new RuleBool(SetupModifier.valueOf(name.toUpperCase()), boolVal));
        }

        rs.close();
        rs = executeQuery(conn, "SELECT name, value FROM setup_int_modifiers WHERE setup_id = ?;", setupID);

        int intVal;
        while (rs.next()){
            name = rs.getString("name");
            intVal = rs.getInt("value");
            rules.add(new RuleInt(SetupModifier.valueOf(name.toUpperCase()), intVal));
        }

        rs.close();

        return rules;
    }

    public static void create(Connection conn, long setupID, String name, int value) throws SQLException {
        execute(conn, "INSERT INTO setup_int_modifiers (setup_id, name, value) VALUES (?, ?, ?);", setupID, name,
                value);
    }

}
