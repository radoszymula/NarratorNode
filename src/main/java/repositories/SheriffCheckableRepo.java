package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.schemas.SheriffCheckableSchema;

public class SheriffCheckableRepo extends BaseRepo {

    public static ArrayList<SheriffCheckableSchema> getSheriffCheckableColors(Connection connection, long setupID)
            throws SQLException {
        String query = "SELECT a.color sheriff_color, b.color detectable_color " + "FROM sheriff_checkables "
                + "LEFT JOIN factions a " + "ON a.id = sheriff_checkables.sheriff_faction_id " + "LEFT JOIN factions b "
                + "ON b.id = sheriff_checkables.detectable_faction_id " + "WHERE a.setup_id = ?;";
        ArrayList<SheriffCheckableSchema> sheriffCheckables = new ArrayList<>();
        ResultSet rs = executeQuery(connection, query, setupID);
        SheriffCheckableSchema sheriffCheckable;
        while (rs.next()){
            sheriffCheckable = new SheriffCheckableSchema();
            sheriffCheckable.sheriffColor = rs.getString("sheriff_color");
            sheriffCheckable.detectableColor = rs.getString("detectable_color");

            sheriffCheckables.add(sheriffCheckable);
        }
        rs.close();
        return sheriffCheckables;
    }

    public static void create(Connection c, long factionID, long factionDetectableID) throws SQLException {
        execute(c, "INSERT INTO sheriff_checkables (sheriff_faction_id, detectable_faction_id) VALUES (?, ?);",
                factionID, factionDetectableID);
    }
}
