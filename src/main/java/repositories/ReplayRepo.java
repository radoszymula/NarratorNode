package repositories;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.schemas.GameOverviewSchema;

public class ReplayRepo extends BaseRepo {

    public static GameOverviewSchema getOverview(Connection conn, long replayID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT seed, host_id, instance_id, is_private FROM replays ");
        query.append("WHERE id = ?;");
        ResultSet rs = executeQuery(conn, query.toString(), replayID);
        rs.next();

        GameOverviewSchema gameOverview = new GameOverviewSchema();
        gameOverview.seed = rs.getLong("seed");
        gameOverview.hostID = rs.getLong("host_id");
        gameOverview.isPrivate = rs.getBoolean("is_private");
        gameOverview.instanceID = rs.getString("instance_id");

        rs.close();
        return gameOverview;
    }

    public static long getSetupID(Connection c, long replayID) throws SQLException {
        ResultSet rs = executeQuery(c, "SELECT setup_id FROM replays WHERE id = ?;", replayID);
        rs.next();
        long setupID = rs.getLong("setup_id");
        rs.close();
        return setupID;
    }

    public static void deleteUnstarted(Connection c) throws SQLException {
        execute(c, "DELETE FROM replays WHERE is_started = false;");
    }

    public static ArrayList<Long> getUnfinishedIDs(Connection c) throws SQLException {
        ResultSet rs = executeQuery(c, "SELECT id FROM replays WHERE instance_id IS NOT NULL;");
        ArrayList<Long> replayIDs = new ArrayList<>();
        while (rs.next())
            replayIDs.add(rs.getLong("id"));

        rs.close();
        return replayIDs;
    }

    public static long create(Connection connection, String instanceID, boolean isPrivate, long hostID, long seed,
            long setupID) throws SQLException {
        Date created_at = new Date(System.currentTimeMillis());
        return executeInsertQuery(connection,
                "INSERT INTO replays (setup_id, seed, created_at, host_id, is_private, instance_id, is_started) "
                        + "VALUES (?, ?, ?, ?, ?, ?, false);",
                setupID, seed, created_at, hostID, isPrivate, instanceID).get(0);
    }

    public static ArrayList<Long> getAllIDs(Connection c) throws SQLException {
        ResultSet rs = executeQuery(c, "SELECT id FROM replays;");
        ArrayList<Long> replayIDs = new ArrayList<>();
        while (rs.next()){
            replayIDs.add(rs.getLong("id"));
        }
        rs.close();
        return replayIDs;
    }

    public static void setSetupID(Connection connection, long replayID, long setupID) throws SQLException {
        execute(connection, "UPDATE replays SET setup_id = ? WHERE id = ?;", setupID, replayID);
    }

    public static void markCompleted(Connection conn, long replayID) throws SQLException {
        execute(conn, "UPDATE replays SET instance_id = NULL WHERE id = ?;", replayID);
    }

    public static void deleteByID(Connection c, Long replayID) throws SQLException {
        execute(c, "DELETE FROM replays WHERE id = ?;", replayID);
    }

    public static void deleteAll(Connection c) throws SQLException {
        execute(c, "DELETE FROM replays;");
    }

    public static void setStarted(Connection connection, long id) throws SQLException {
        execute(connection, "UPDATE replays SET is_started = true where id = ?;", id);
    }

    public static void setHostID(Connection connection, long hostID, long gameID) throws SQLException {
        execute(connection, "UPDATE replays SET host_id = ? WHERE id = ?;", hostID, gameID);
    }

}
