package repositories;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.support.Util;
import game.logic.support.rules.Modifier;
import game.roles.Ability;
import models.Modifiers;

public abstract class ModifierRepo extends BaseRepo{
	protected static <T extends Modifier> void insertModifiers(Connection connection, String tableIntName, String tableBoolName, String columnName, ArrayList<Long> entityIDs, ArrayList<Modifiers<T>> modifiersList) throws SQLException {
		StringBuilder intQuery = new StringBuilder();
		intQuery.append("INSERT INTO ");
		intQuery.append(tableIntName);
		intQuery.append(" (");
		intQuery.append(columnName);
		intQuery.append(", name, value) VALUES ");
        StringBuilder booleanQuery = new StringBuilder();
        booleanQuery.append("INSERT INTO ");
        booleanQuery.append(tableBoolName);
        booleanQuery.append(" (");
        booleanQuery.append(columnName);
        booleanQuery.append(", name, value) VALUES ");

        List<Object> boolModifierArgs = new LinkedList<>();
        List<Object> intModifierArgs = new LinkedList<>();

        long entityID;
        Modifiers<T> modifiers;
        for(int i = 0; i < modifiersList.size(); i++) {
        	entityID = entityIDs.get(i);
        	modifiers = modifiersList.get(i);
	        for(T modifier: modifiers.getModifierIDs()){
	            if(Ability.IsBoolModifier(modifier)){
	                booleanQuery.append("(?, ?, ?), ");
	                boolModifierArgs.add(entityID);
	                boolModifierArgs.add(modifier.toString());
	                boolModifierArgs.add(modifiers.getBoolean(modifier));
	            }else{
	                intQuery.append("(?, ?, ?), ");
	                intModifierArgs.add(entityID);
	                intModifierArgs.add(modifier.toString());
	                intModifierArgs.add(modifiers.getInt(modifier));
	            }
	        }
        }
	
        if(!boolModifierArgs.isEmpty()){
            booleanQuery.delete(booleanQuery.length() - 2, booleanQuery.length() - 1);
            booleanQuery.append(";");
            execute(connection, booleanQuery.toString(), Util.toObjectArray(boolModifierArgs));
        }
        if(!intModifierArgs.isEmpty()){
            intQuery.delete(intQuery.length() - 2, intQuery.length() - 1);
            intQuery.append(";");
            execute(connection, intQuery.toString(), Util.toObjectArray(intModifierArgs));
        }
	}
}
