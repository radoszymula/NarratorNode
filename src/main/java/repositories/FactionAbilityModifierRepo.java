package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import game.logic.support.rules.AbilityModifier;
import models.Modifiers;
import models.schemas.FactionAbilityModifierSchema;

public class FactionAbilityModifierRepo extends ModifierRepo {

    public static Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>> getBySetupID(Connection connection,
            long setupID) throws SQLException {
        Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>> modifiers = new HashMap<>();
        StringBuilder query = new StringBuilder();
        query.append("SELECT faction_id, faction_ability_id, faim.name, value ");
        query.append("FROM faction_ability_int_modifiers AS faim ");
        query.append("INNER JOIN faction_abilities ");
        query.append("ON faction_abilities.id = faim.faction_ability_id ");
        query.append("INNER JOIN factions ");
        query.append("ON factions.id = faction_abilities.faction_id ");
        query.append("WHERE setup_id = ?;");
        ResultSet rs = executeQuery(connection, query.toString(), setupID);

        long factionID, factionAbilityID;
        FactionAbilityModifierSchema modifier;
        while (rs.next()){
            factionID = rs.getLong("faction_id");
            factionAbilityID = rs.getLong("faction_ability_id");

            modifier = new FactionAbilityModifierSchema();
            modifier.intValue = rs.getInt("value");
            modifier.name = rs.getString("name");
            makeSpaceInMap(modifiers, factionID, factionAbilityID);
            modifiers.get(factionID).get(factionAbilityID).add(modifier);
        }
        rs.close();

        query = new StringBuilder();
        query.append("SELECT faction_id, faction_ability_id, fabm.name, value ");
        query.append("FROM faction_ability_bool_modifiers AS fabm ");
        query.append("INNER JOIN faction_abilities ");
        query.append("ON faction_abilities.id = fabm.faction_ability_id ");
        query.append("INNER JOIN factions ");
        query.append("ON factions.id = faction_abilities.faction_id ");
        query.append("WHERE setup_id = ?;");
        rs = executeQuery(connection, query.toString(), setupID);
        while (rs.next()){
            factionID = rs.getLong("faction_id");
            factionAbilityID = rs.getLong("faction_ability_id");

            modifier = new FactionAbilityModifierSchema();
            modifier.boolValue = rs.getBoolean("value");
            modifier.name = rs.getString("name");
            makeSpaceInMap(modifiers, factionID, factionAbilityID);
            modifiers.get(factionID).get(factionAbilityID).add(modifier);
        }
        rs.close();

        return modifiers;
    }

    private static void makeSpaceInMap(Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>> modifiers,
            long factionID, long factionAbilityID) {
        if(!modifiers.containsKey(factionID))
            modifiers.put(factionID, new HashMap<>());
        Map<Long, Set<FactionAbilityModifierSchema>> factionAbilities = modifiers.get(factionID);
        if(!factionAbilities.containsKey(factionAbilityID))
            factionAbilities.put(factionAbilityID, new HashSet<>());
    }

    public static void create(Connection connection, ArrayList<Long> factionAbilityIDs,
            ArrayList<Modifiers<AbilityModifier>> modifiersList) throws SQLException {
        insertModifiers(connection, "faction_ability_int_modifiers", "faction_ability_bool_modifiers",
                "faction_ability_id", factionAbilityIDs, modifiersList);
    }

    public static void updateModifier(Connection connection, long factionAbilityID, AbilityModifier modifier,
            boolean value) throws SQLException {
        execute(connection,
                "INSERT INTO faction_ability_bool_modifiers (faction_ability_id, name, value) VALUES (?, ?, ?);",
                factionAbilityID, modifier.toString(), value);
    }

    public static void updateModifier(Connection connection, long factionAbilityID, AbilityModifier modifier, int value)
            throws SQLException {
        if(value > 0)
            execute(connection,
                    "INSERT INTO faction_ability_int_modifiers (faction_ability_id, name, value) VALUES (?, ?, ?);",
                    factionAbilityID, modifier.toString(), true);
        else
            execute(connection, "DELETE FROM faction_ability_int_modifiers WHERE faction_ability_id = ? AND name = ?;",
                    factionAbilityID, modifier.toString());
    }

}
