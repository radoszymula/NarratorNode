package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserPermissionRepo extends BaseRepo {

    public static boolean hasPermission(Connection c, long userID, int permission) throws SQLException {
        String sqlQuery = "SELECT 1 FROM user_permissions WHERE user_id = ? AND permission_type = ?;";

        ResultSet rs = executeQuery(c, sqlQuery, userID, permission);
        boolean hasPermission = rs.isBeforeFirst();

        rs.close();
        return hasPermission;
    }

    public static void addPermission(Connection c, long userID, int permissionType) throws SQLException {
        String sqlQuery = "INSERT IGNORE INTO user_permissions (user_id, permission_type) VALUES (?, ?);";
        execute(c, sqlQuery, userID, permissionType);
    }

    public static void removeAllPermissions(Connection c, long userID) throws SQLException {
        String sqlQuery = "DELETE FROM user_permissions WHERE user_id = ?;";
        execute(c, sqlQuery, userID);
    }

    public static ArrayList<Long> getPermissions(Connection c, long userID) throws SQLException {
        ArrayList<Long> permissions = new ArrayList<>();
        String sqlQuery = "SELECT permission_type FROM user_permissions WHERE user_id = ?;";
        ResultSet rs = executeQuery(c, sqlQuery, userID);
        while (rs.next()){
            permissions.add(rs.getLong("permission_type"));
        }

        rs.close();

        return permissions;
    }

}
