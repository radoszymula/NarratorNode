package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.support.Util;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.roles.Ability;
import models.FactionRole;
import models.Modifiers;
import models.schemas.AbilityModifierSchema;
import models.schemas.RoleModifierSchema;

public class FactionRoleModifierRepo extends ModifierRepo {

    public static void createRoleModifiers(Connection connection, ArrayList<FactionRole> factionRoles)
            throws SQLException {
        ArrayList<Long> factionRoleIDs = new ArrayList<>();
        ArrayList<Modifiers<RoleModifier>> modifiersList = new ArrayList<>();
        for(FactionRole factionRole: factionRoles){
            factionRoleIDs.add(factionRole.id);
            modifiersList.add(factionRole.roleModifiers);
        }
        insertModifiers(connection, "faction_role_role_int_modifiers", "faction_role_role_bool_modifiers",
                "faction_role_id", factionRoleIDs, modifiersList);
    }

    public static void create(Connection connection, ArrayList<Long> factionRoleIDs, ArrayList<Long> roleAbilityIDs,
            ArrayList<Modifiers<AbilityModifier>> modifiersList) throws SQLException {
        StringBuilder intQuery = new StringBuilder();
        intQuery.append("INSERT INTO faction_role_ability_int_modifiers ");
        intQuery.append("(faction_role_id, role_ability_id, name, value) VALUES ");
        StringBuilder booleanQuery = new StringBuilder();
        booleanQuery.append("INSERT INTO faction_role_ability_bool_modifiers ");
        booleanQuery.append("(faction_role_id, role_ability_id, name, value) VALUES ");

        List<Object> boolModifierArgs = new LinkedList<>();
        List<Object> intModifierArgs = new LinkedList<>();

        Modifiers<AbilityModifier> modifiers;
        for(int i = 0; i < modifiersList.size(); i++){
            modifiers = modifiersList.get(i);
            for(AbilityModifier modifier: modifiers.getModifierIDs()){
                if(Ability.IsBoolModifier(modifier)){
                    booleanQuery.append("(?, ?, ?, ?), ");
                    boolModifierArgs.add(factionRoleIDs.get(i));
                    boolModifierArgs.add(roleAbilityIDs.get(i));
                    boolModifierArgs.add(modifier.toString());
                    boolModifierArgs.add(modifiers.getBoolean(modifier));
                }else{
                    intQuery.append("(?, ?, ?, ?), ");
                    intModifierArgs.add(factionRoleIDs.get(i));
                    intModifierArgs.add(roleAbilityIDs.get(i));
                    intModifierArgs.add(modifier.toString());
                    intModifierArgs.add(modifiers.getInt(modifier));
                }
            }
        }

        if(!boolModifierArgs.isEmpty()){
            booleanQuery.delete(booleanQuery.length() - 2, booleanQuery.length() - 1);
            booleanQuery.append(";");
            execute(connection, booleanQuery.toString(), Util.toObjectArray(boolModifierArgs));
        }
        if(!intModifierArgs.isEmpty()){
            intQuery.delete(intQuery.length() - 2, intQuery.length() - 1);
            intQuery.append(";");
            execute(connection, intQuery.toString(), Util.toObjectArray(intModifierArgs));
        }

    }

    public static Map<Long, Set<RoleModifierSchema>> getRoleModifiers(Connection connection, long setupID)
            throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT faction_role_id, name, value ");
        query.append("FROM faction_role_role_bool_modifiers ");
        query.append("LEFT JOIN faction_roles ");
        query.append("  ON faction_role_role_bool_modifiers.faction_role_id = faction_roles.id ");
        query.append("WHERE setup_id = ?;");
        ResultSet results = executeQuery(connection, query.toString(), setupID);

        Map<Long, Set<RoleModifierSchema>> roleModifierMap = new HashMap<>();
        RoleModifier modifier;
        Object value;
        long factionRoleID;
        RoleModifierSchema modifierSchema;
        while (results.next()){
            modifier = RoleModifier.valueOf(results.getString("name"));
            value = results.getBoolean("value");
            factionRoleID = results.getLong("faction_role_id");
            modifierSchema = new RoleModifierSchema(modifier, value);

            if(!roleModifierMap.containsKey(factionRoleID))
                roleModifierMap.put(factionRoleID, new HashSet<>(Arrays.asList(modifierSchema)));
            else
                roleModifierMap.get(factionRoleID).add(modifierSchema);
        }
        results.close();

        query = new StringBuilder();
        query.append("SELECT faction_role_id, name, value ");
        query.append("FROM faction_role_role_int_modifiers ");
        query.append("LEFT JOIN faction_roles ");
        query.append("  ON faction_role_role_int_modifiers.faction_role_id = faction_roles.id ");
        query.append("WHERE setup_id = ?;");
        results = executeQuery(connection, query.toString(), setupID);

        while (results.next()){
            modifier = RoleModifier.valueOf(results.getString("name"));
            value = results.getInt("value");
            factionRoleID = results.getLong("faction_role_id");
            modifierSchema = new RoleModifierSchema(modifier, value);

            if(roleModifierMap.containsKey(factionRoleID))
                roleModifierMap.get(factionRoleID).add(modifierSchema);
            else
                roleModifierMap.put(factionRoleID, new HashSet<>(Arrays.asList(modifierSchema)));
        }
        results.close();

        return roleModifierMap;
    }

    public static Map<Long, Map<Long, Set<AbilityModifierSchema>>> getAbilityModifiers(Connection connection,
            long setupID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT faction_role_id, role_ability_id, name, value ");
        query.append("FROM faction_role_ability_int_modifiers ");
        query.append("LEFT JOIN faction_roles ");
        query.append("  ON faction_role_ability_int_modifiers.faction_role_id = faction_roles.id ");
        query.append("WHERE setup_id = ?;");

        Map<Long, Map<Long, Set<AbilityModifierSchema>>> resultsMap = new HashMap<>();
        ResultSet results = executeQuery(connection, query.toString(), setupID);
        extractAbilityModifierResults(resultsMap, results);

        query = new StringBuilder();
        query.append("SELECT faction_role_id, role_ability_id, name, value ");
        query.append("FROM faction_role_ability_bool_modifiers ");
        query.append("LEFT JOIN faction_roles ");
        query.append("  ON faction_role_ability_bool_modifiers.faction_role_id = faction_roles.id ");
        query.append("WHERE setup_id = ?;");

        results = executeQuery(connection, query.toString(), setupID);
        extractAbilityModifierResults(resultsMap, results);

        // resultsSet closed in helper method
        return resultsMap;
    }

    public static void update(Connection connection, long factionRoleID, RoleModifier modifier, Object value)
            throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO ");
        if(value instanceof Integer)
            query.append("faction_role_role_int_modifiers ");
        else
            query.append("faction_role_role_bool_modifiers ");
        query.append("(faction_role_id, name, value) VALUES (?, ?, ?) ");
        query.append("ON DUPLICATE KEY UPDATE value = ?;");
        execute(connection, query.toString(), factionRoleID, modifier.toString(), value, value);
    }

    public static void update(Connection connection, long factionRoleID, long roleAbilityID, AbilityModifier modifier,
            Object value) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO ");
        if(value instanceof Integer)
            query.append("faction_role_ability_int_modifiers ");
        else
            query.append("faction_role_ability_bool_modifiers ");
        query.append("(faction_role_id, role_ability_id, name, value) VALUES (?, ?, ?, ?) ");
        query.append("ON DUPLICATE KEY UPDATE value = ?;");
        execute(connection, query.toString(), factionRoleID, roleAbilityID, modifier.toString(), value, value);
    }

    private static void extractAbilityModifierResults(Map<Long, Map<Long, Set<AbilityModifierSchema>>> resultsMap,
            ResultSet results) throws SQLException {
        Object value;
        long factionRoleID;
        long roleAbilityID;
        AbilityModifier modifier;
        AbilityModifierSchema modifierSchema;
        Map<Long, Set<AbilityModifierSchema>> abilityMap;
        while (results.next()){
            modifier = AbilityModifier.valueOf(results.getString("name"));
            value = results.getObject("value");
            factionRoleID = results.getLong("faction_role_id");
            roleAbilityID = results.getLong("role_ability_id");
            modifierSchema = new AbilityModifierSchema(modifier, value);

            if(!resultsMap.containsKey(factionRoleID))
                resultsMap.put(factionRoleID, new HashMap<>());
            abilityMap = resultsMap.get(factionRoleID);
            if(abilityMap.containsKey(roleAbilityID))
                abilityMap.get(roleAbilityID).add(modifierSchema);
            else
                abilityMap.put(roleAbilityID, new HashSet<>(Arrays.asList(modifierSchema)));
        }
        results.close();
    }

}
