package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import game.logic.support.rules.AbilityModifier;
import models.Modifiers;
import models.schemas.AbilityModifierSchema;

public class RoleAbilityModifierRepo extends ModifierRepo {

    public static Set<AbilityModifierSchema> getByRoleAbilityID(Connection connection, long roleAbilityID)
            throws SQLException {
        Set<AbilityModifierSchema> modifiers = new HashSet<>();
        ResultSet rs = executeQuery(connection,
                "SELECT name, value FROM role_ability_int_modifiers WHERE role_ability_id=?;", roleAbilityID);
        AbilityModifierSchema modifier;
        while (rs.next()){
            modifier = new AbilityModifierSchema();
            modifier.intValue = rs.getInt("value");
            modifier.name = rs.getString("name");
            modifiers.add(modifier);
        }
        rs.close();

        rs = executeQuery(connection, "SELECT name, value FROM role_ability_bool_modifiers WHERE role_ability_id=?;",
                roleAbilityID);
        while (rs.next()){
            modifier = new AbilityModifierSchema();
            modifier.boolValue = rs.getBoolean("value");
            modifier.name = rs.getString("name");
            modifiers.add(modifier);
        }
        rs.close();

        return modifiers;
    }

    public static Map<Long, Map<Long, Set<AbilityModifierSchema>>> getBySetupID(Connection connection, long setupID)
            throws SQLException {
        Map<Long, Map<Long, Set<AbilityModifierSchema>>> modifiers = new HashMap<>();
        StringBuilder query = new StringBuilder();
        query.append("SELECT role_id, role_ability_id, raim.name, value ");
        query.append("FROM role_ability_int_modifiers AS raim ");
        query.append("INNER JOIN role_abilities ");
        query.append("ON role_abilities.id = raim.role_ability_id ");
        query.append("INNER JOIN roles ");
        query.append("ON roles.id = role_abilities.role_id ");
        query.append("WHERE setup_id = ?;");
        ResultSet rs = executeQuery(connection, query.toString(), setupID);

        long role, roleAbilityID;
        AbilityModifierSchema modifier;
        while (rs.next()){
            role = rs.getLong("role_id");
            roleAbilityID = rs.getLong("role_ability_id");

            modifier = new AbilityModifierSchema();
            modifier.intValue = rs.getInt("value");
            modifier.name = rs.getString("name");
            makeSpaceInMap(modifiers, role, roleAbilityID);
            modifiers.get(role).get(roleAbilityID).add(modifier);
        }
        rs.close();

        query = new StringBuilder();
        query.append("SELECT role_id, role_ability_id, rabm.name, value ");
        query.append("FROM role_ability_bool_modifiers AS rabm ");
        query.append("INNER JOIN role_abilities ");
        query.append("ON role_abilities.id = rabm.role_ability_id ");
        query.append("INNER JOIN roles ");
        query.append("ON roles.id = role_abilities.role_id ");
        query.append("WHERE setup_id = ?;");
        rs = executeQuery(connection, query.toString(), setupID);
        while (rs.next()){
            role = rs.getLong("role_id");
            roleAbilityID = rs.getLong("role_ability_id");

            modifier = new AbilityModifierSchema();
            modifier.boolValue = rs.getBoolean("value");
            modifier.name = rs.getString("name");
            makeSpaceInMap(modifiers, role, roleAbilityID);
            modifiers.get(role).get(roleAbilityID).add(modifier);
        }
        rs.close();

        return modifiers;
    }

    private static void makeSpaceInMap(Map<Long, Map<Long, Set<AbilityModifierSchema>>> modifiers, long roleID,
            long roleAbilityID) {
        if(!modifiers.containsKey(roleID))
            modifiers.put(roleID, new HashMap<>());
        Map<Long, Set<AbilityModifierSchema>> factionAbilities = modifiers.get(roleID);
        if(!factionAbilities.containsKey(roleAbilityID))
            factionAbilities.put(roleAbilityID, new HashSet<>());
    }

    public static void create(Connection connection, ArrayList<Long> roleAbilityIDs,
            ArrayList<Modifiers<AbilityModifier>> modifiersList) throws SQLException {
        insertModifiers(connection, "role_ability_int_modifiers", "role_ability_bool_modifiers", "role_ability_id",
                roleAbilityIDs, modifiersList);
    }

    public static void updateModifier(Connection connection, long abilityID, AbilityModifier modifier, boolean value)
            throws SQLException {
        if(value)
            execute(connection,
                    "INSERT INTO role_ability_bool_modifiers (role_ability_id, name, value) VALUES (?, ?, ?);",
                    abilityID, modifier.toString(), true);
        else
            execute(connection, "DELETE FROM role_ability_bool_modifiers WHERE role_ability_id = ? AND name = ?;",
                    abilityID, modifier.toString());
    }

    public static void updateModifier(Connection connection, long abilityID, AbilityModifier modifier, int value)
            throws SQLException {
        if(value > 0)
            execute(connection,
                    "INSERT INTO role_ability_int_modifiers (role_ability_id, name, value) VALUES (?, ?, ?);",
                    abilityID, modifier.toString(), true);
        else
            execute(connection, "DELETE FROM role_ability_int_modifiers WHERE role_ability_id = ? AND name = ?;",
                    abilityID, modifier.toString());
    }
}
