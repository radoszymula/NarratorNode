package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRepo extends BaseRepo {

    public static boolean getIsGuest(Connection c, long userID) throws SQLException {
        ResultSet rs = executeQuery(c, "SELECT is_guest FROM users WHERE id = ?;", userID);
        rs.next();

        boolean isGuest = rs.getBoolean("is_guest");
        rs.close();
        return isGuest;
    }

    public static void updatePointsAndWinRate(Connection c, long replayID, String name, int pointIncrease) throws SQLException {
        long userID = PlayerRepo.getUserID(c, replayID, name);
        double userWinRatio = getUserWinRatio(c, userID);
        execute(c, "UPDATE users SET points = points + ?, win_rate = ? WHERE id = ?;", pointIncrease, userWinRatio,
                userID);
    }

    private static double getUserWinRatio(Connection c, long userID) throws SQLException {
        String query = "SELECT COUNT(1), is_winner FROM players WHERE user_id = ? and is_winner is not NULL GROUP BY is_winner;"; // todo

        ResultSet rs = executeQuery(c, query, userID);
        Double wins = null;
        Double losses = null;
        while (rs.next()){
            if(rs.getBoolean(2))
                wins = (double) rs.getInt(1);
            else
                losses = (double) rs.getInt(1);
        }
        rs.close();

        if(wins == null)
            wins = 0.0;
        if(losses == null)
            losses = 0.0;
        if(wins.equals(wins + losses))
            losses++;
        return (wins / (losses + wins)) * 100;
    }

    public static void clearPointsAndWinRate(Connection c) throws SQLException {
        execute(c, "UPDATE users SET points = 0, win_rate = 0;");
    }

    public static Long getByName(Connection c, String name) throws SQLException {
        ResultSet rs = executeQuery(c, "SELECT id FROM users WHERE token = ?;", name);
        if(!rs.next()){
            rs.close();
            return null;
        }
        long userID = rs.getLong("id");
        rs.close();
        return userID;
    }

    public static long create(Connection c, boolean isGuest) throws SQLException {
        return executeInsertQuery(c, "INSERT INTO users (is_guest) VALUES (?);", isGuest).get(0);
    }

    public static void deleteAll(Connection c) throws SQLException {
        execute(c, "DELETE FROM users;");
    }

}
