package repositories;

import java.sql.SQLException;
import java.util.ArrayList;

import game.roles.Ability;

public class AbilityRepo extends BaseRepo{
	public static ArrayList<Long> insertAbilities(Connection connection, String abilityTableName, String entityColumnName, ArrayList<Long> abilityEntityIDs, ArrayList<Ability> abilities) throws SQLException {
		if(abilities.isEmpty())
    		return new ArrayList<>();
    	
    	Object[] queryArgs = new Object[abilities.size() * 2];
    	StringBuilder query = new StringBuilder();
    	query.append("INSERT INTO ");
    	query.append(abilityTableName);
    	query.append(" (");
    	query.append(entityColumnName);
    	query.append(", name) VALUES");
    	
    	for(int i = 0; i < abilities.size(); i++) {
    		if(i != 0)
    			query.append(',');
    		query.append(" (?, ?)");
    		queryArgs[i * 2] = abilityEntityIDs.get(i);
    		queryArgs[(i * 2) + 1] = abilities.get(i).getClass().getSimpleName();
    	}
    	query.append(";");
        return executeInsertQuery(connection, query.toString(), queryArgs);
	}
}
