package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import game.logic.Faction;
import models.schemas.FactionSchema;

public class FactionRepo extends BaseRepo {

    public static Map<Long, FactionSchema> getBySetupID(Connection connection, long setupID) throws SQLException {
        Map<Long, FactionSchema> factions = new HashMap<>();
        ResultSet rs = executeQuery(connection, "SELECT id, name, color, description FROM factions WHERE setup_id=?;",
                setupID);
        FactionSchema faction;
        while (rs.next()){
            faction = new FactionSchema();
            faction.id = rs.getLong("id");
            faction.name = rs.getString("name");
            faction.color = rs.getString("color");
            faction.description = rs.getString("description");
            factions.put(faction.id, faction);
        }
        rs.close();
        return factions;
    }

    public static Long getIDBySetupIDAndColor(Connection connection, long setupID, String color) throws SQLException {
        ResultSet rs = executeQuery(connection, "SELECT id FROM factions WHERE setup_id=? AND color = ?;", setupID,
                color);
        Long id = null;
        while (rs.next()){
            id = rs.getLong("id");
            break;
        }
        rs.close();
        return id;
    }

    public static void editName(Connection c, long factionID, String newName) throws SQLException {
        execute(c, "UPDATE factions SET name = ? WHERE id = ?;", newName, factionID);
    }

    public static void editDescription(Connection c, long factionID, String description) throws SQLException {
        execute(c, "UPDATE factions SET description = ? WHERE id = ?;", description, factionID);
    }

    public static void updateAutoFactionHidden(Connection connection, long setupID, long factionID, boolean isAutoOn)
            throws SQLException {
        execute(connection, "UPDATE factions SET hasAutoHidden = ? WHERE setup_id = ? AND id = ?", isAutoOn, setupID,
                factionID);
    }

    public static List<Long> insertFactions(Connection connection, long setupID, List<Faction> factions)
            throws SQLException {
        StringBuilder sb = new StringBuilder("");
        Object queryArgs[] = new Object[factions.size() * 4];
        int counter = 0;
        sb.append("INSERT INTO factions (setup_id, color, name, description, hasAutoHidden) VALUES");

        Faction faction;
        for(int i = 0; i < factions.size(); i++){
            if(i != 0)
                sb.append(", ");
            faction = factions.get(i);
            sb.append("(?, ?, ?, ?, false)");
            queryArgs[counter++] = setupID;
            queryArgs[counter++] = faction.getColor();
            queryArgs[counter++] = faction.getName();
            queryArgs[counter++] = faction.getDescription();
        }

        sb.append(";");
        return executeInsertQuery(connection, sb.toString(), queryArgs);
    }

    public static void deleteByIDs(Connection connection, long setupID, List<Long> factionIDs) throws SQLException {
        if(factionIDs.isEmpty())
            return;
        StringBuilder sb = new StringBuilder("");
        sb.append("DELETE FROM factions WHERE setup_id = ? AND (");

        long factionID;
        for(int i = 0; i < factionIDs.size(); i++){
            if(i != 0)
                sb.append(" OR ");
            factionID = factionIDs.get(i);
            sb.append("id = ");
            sb.append(factionID);
        }

        sb.append(");");
        execute(connection, sb.toString(), setupID);
    }

}
