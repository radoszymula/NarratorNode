package models.json_output;

import game.logic.Role;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;

public class RoleJson {

    public static JSONObject toJson(Role role) throws JSONException {
        JSONObject roleJson = new JSONObject();
        roleJson.put("abilities", AbilityJson.toJson(role._abilities, null, role.name, role.narrator));
        roleJson.put("id", role.getID());
        roleJson.put("details", role.getDescription());
        roleJson.put("modifiers", ModifierJson.getRoleModifiers(role.narrator, role.modifiers));
        roleJson.put("name", role.getName());
        return roleJson;
    }

    public static JSONArray toJson(Iterable<Role> roles) throws JSONException {
        JSONArray rolesJson = new JSONArray();
        for(Role role: roles)
            rolesJson.put(toJson(role));
        return rolesJson;
    }
};
