package models.json_output;

import java.util.ArrayList;
import java.util.List;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import util.ModifierUtil;

public class FactionRoleJson {

    public static JSONObject toJson(FactionRole factionRole) throws JSONException {
        ArrayList<String> details = factionRole.role.getDescription();
        details.addAll(factionRole.getPublicDescription());
        
        FactionRole factionRoleCopy = factionRole.copy();
        ModifierUtil.resolveOverrides(factionRoleCopy);

        JSONObject factionRoleJson = new JSONObject();
        factionRoleJson.put("abilities", AbilityJson.toJson(factionRoleCopy.role._abilities, factionRole.getColor(), factionRole.getName(), factionRole.role.narrator));
        factionRoleJson.put("details", details);
        factionRoleJson.put("factionID", factionRole.faction.id);
        factionRoleJson.put("id", factionRole.id);
        factionRoleJson.put("modifiers", ModifierJson.getRoleModifiers(factionRole.faction.narrator, factionRoleCopy.roleModifiers));
        factionRoleJson.put("name", factionRole.role.name); // deprecated.  not all roles have ids yet
        factionRoleJson.put("roleID", factionRole.role.getID());
        factionRoleJson.put("team", FactionOverviewJson.toJson(factionRole.faction));
        return factionRoleJson;
    }

    public static JSONArray toJson(List<FactionRole> factionRoles) throws JSONException {
        JSONArray jArray = new JSONArray();
        for(FactionRole factionRole: factionRoles)
            jArray.put(toJson(factionRole));
        return jArray;
    }
}
