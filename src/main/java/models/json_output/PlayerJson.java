package models.json_output;

import game.logic.Player;
import game.logic.PlayerList;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import nnode.Game;
import nnode.User;

public class PlayerJson {

    public static JSONArray toJson(PlayerList players, Game game) throws JSONException {
        JSONArray playersJson = new JSONArray();
        for(Player p: players)
            playersJson.put(toJson(p, game));
        return playersJson;
    }

    public static JSONObject toJson(Player player, Game game) throws JSONException {
        JSONObject playerJson = new JSONObject();
        playerJson.put("endedNight", player.endedNight());
        playerJson.put("flip", GraveyardRole.toJson(player));
        playerJson.put("isComputer", player.isComputer());
        playerJson.put("name", player.getName());

        if(game != null && game.phoneBook.get(player) != null){
            User user = game.phoneBook.get(player);
            playerJson.put("userID", user.id);
        }

        if(game != null && game.narrator.isStarted())
            playerJson.put("factionRole", getFactionRoleJson(player));

        return playerJson;
    }

    public static JSONObject getFactionRoleJson(Player player) throws JSONException {
        if(player.factionRole.receivedRole == null)
            return FactionRoleJson.toJson(player.factionRole);
        return FactionRoleJson.toJson(player.factionRole.receivedRole);
    }
}
