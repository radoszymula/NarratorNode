package models.json_output;

import game.logic.Narrator;
import game.setups.Setup;
import json.JSONException;
import json.JSONObject;

public class SetupJson {
    public static JSONObject toJson(Setup setup) throws JSONException {
        Narrator narrator = setup.narrator;
        JSONObject setupJson = new JSONObject();
        setupJson.put("factions", FactionJson.toJson(narrator.getFactions()));
        setupJson.put("key", setup.key);// deprecated
        setupJson.put("hiddens", HiddenJson.toJson(setup.narrator.hiddens));
        setupJson.put("id", setup.id);
        setupJson.put("isPreset", setup.isPreset());
        setupJson.put("maxPlayerCount", setup.getMaxPlayerCount());
        setupJson.put("minPlayerCount", setup.getMinPlayerCount());
        setupJson.put("name", setup.getName());
        setupJson.put("ownerID", setup.ownerID);
        setupJson.put("roles", RoleJson.toJson(narrator.roles));
        setupJson.put("setupHiddens", SetupHiddenJson.toJson(setup.id, narrator.rolesList));
        setupJson.put("setupModifiers", SetupModifierJson.toJson(narrator._rules, narrator));
        return setupJson;
    }
}
