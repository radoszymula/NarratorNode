package models.json_output;

import game.roles.Hidden;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;

public class HiddenJson {

    public static JSONObject toJson(Hidden hidden) throws JSONException {
        JSONObject hiddenJson = new JSONObject();
        hiddenJson.put("id", hidden.getID());
        hiddenJson.put("name", hidden.getName());
        JSONArray roleIDs = new JSONArray();
        for(FactionRole role: hidden.getFactionRoles())
            roleIDs.put(role.id);
        hiddenJson.put("factionRoleIDs", roleIDs);
        return hiddenJson;
    }

    public static JSONArray toJson(Iterable<Hidden> rolesList) throws JSONException {
        JSONArray setupHiddensJson = new JSONArray();

        for(Hidden setupHidden: rolesList)
            setupHiddensJson.put(HiddenJson.toJson(setupHidden));

        return setupHiddensJson;
    }

}
