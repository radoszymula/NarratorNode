package models.json_output;

import game.logic.Faction;
import game.logic.Player;
import game.logic.PlayerList;
import game.roles.Ability;
import game.roles.Executioner;
import game.roles.Ghost;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import nnode.StateObject;
import nnode.User;

public class ProfileJson {
    public static JSONObject toJson(User user) throws JSONException {
        JSONObject response = new JSONObject();
        if(!user.isInGame())
            return response;

        response.put("gameID", user.game.getID());
        response.put("name", user.player.getName());
        response.put("userID", user.id);

        if(user.game.narrator.isStarted()){
            response.put("allies", getAllies(user.player));
            response.put("isJailed", user.player.isJailed());
            response.put("isPuppeted", user.player.isPuppeted());
            response.put("roleCard", getRoleCard(user.player));
        }
        return response;
    }

    public static JSONObject getRoleCard(Player player) throws JSONException {
        JSONObject response = new JSONObject();
        response.put("abilities", getAbilities(player));
        response.put("details", new JSONArray(player.getRoleSpecs()));
        response.put("enemyFactionIDs", getEnemyFactionIDs(player));
        response.put("factionID", player.getFaction().id);
        response.put("roleName", player.getRoleName());
        response.put("winConditionText", getWinConditionText(player));
        return response;
    }

    public static JSONArray getAllies(Player player) throws JSONException {
        JSONArray jAllies = new JSONArray();
        Object[] ret;
        JSONObject jAlly;
        for(Player ally: player.narrator.getLivePlayers()){
            ret = StateObject.shouldShowPlayer(player, ally);
            if(ret == null)
                continue;
            ally = (Player) ret[0];
            jAlly = new JSONObject();
            jAlly.put("factionID", ((Player) ret[1]).getFaction().id);
            jAlly.put("modifiers", getAllyModifiers((Player) ret[1]));
            jAlly.put("name", ally.getName());
//          jAlly.put("roleID", ((Player) ret[1]).role.getID());
            jAlly.put("roleName", ((Player) ret[1]).getRoleName());
            jAllies.put(jAlly);
        }
        return jAllies;
    }

    private static JSONArray getEnemyFactionIDs(Player player) {
        JSONArray enemyFactionIDs = new JSONArray();
        if(player.is(Ghost.class)){
            if(player.isAlive())
                return enemyFactionIDs;
            Player cause = player.getDeathType().getCause();
            if(cause == player && player.getCause() != null)
                cause = player.getCause();
            if(cause == null || cause == player)
                return enemyFactionIDs;
            Faction faction = cause.getFaction();
            if(faction.isEnemy(faction))
                return enemyFactionIDs;
            if(faction.knowsTeam() || faction._startingSize > 0)
                enemyFactionIDs.put(faction.id);
        }else{
            Faction faction = player.getFaction();
            for(Faction enemyFaction: faction.getEnemyFactions())
                enemyFactionIDs.put(enemyFaction.id);
        }
        return enemyFactionIDs;
    }

    private static String getWinConditionText(Player player) {
        if(player.is(Ghost.class)){
            if(player.isAlive())
                return "The first step to winning is to die.";
            Player cause = player.getDeathType().getCause();
            if(cause == player && player.getCause() != null)
                cause = player.getCause();
            if(cause == null || cause == player)
                return "You've lost the game.";
            Faction faction = cause.getFaction();
            if(faction.isEnemy(faction) || !(faction.knowsTeam() || faction._startingSize > 0))
                return "You must assure the defeat of " + cause.getName() + " to win.";
        }else if(!player.is(Executioner.class))
            return "You don't care about who wins.";
        return null;
    }

    private static JSONObject getAllyModifiers(Player ally) throws JSONException {
        JSONObject modifier = new JSONObject();
        if(ally.isInvulnerable())
            modifier.put("night_kill_immune", true);

        if(!ally.isDetectable())
            modifier.put("undetectable", true);

        if(!ally.isBlockable())
            modifier.put("unblockable", true);

        if(ally.getStartingAutoVestCount() > 0)
            modifier.put("autoVestCount", ally.getStartingAutoVestCount());
        return modifier;
    }

    private static JSONArray getAbilities(Player player) throws JSONException {
        JSONArray abilitiesJson = new JSONArray();
        JSONObject abilityJson;
        PlayerList allowedTargets;
        for(Ability ability: player.getAvailableActions()){
            allowedTargets = player.getAcceptableTargets(ability.getCommand());
            abilityJson = new JSONObject();
            abilityJson.put("canAddAction", player.getActions().canAddAnotherAction(ability.getAbilityNumber()));
            abilityJson.put("command", ability.getCommand().toLowerCase());
            abilityJson.put("options", StateObject.getOptionTree(player, ability.getAbilityNumber()));
            abilityJson.put("targets", getAllowedAbilityTargets(allowedTargets, ability));
            abilitiesJson.put(abilityJson);
        }
        return abilitiesJson;
    }

    private static JSONArray getAllowedAbilityTargets(PlayerList allowedTargets, Ability ability) {
        JSONArray allowedTargetsJson = new JSONArray();
        if(allowedTargets != null)
            for(Player target: allowedTargets)
                allowedTargetsJson.put(target.getName());
        return allowedTargetsJson;
    }
}
