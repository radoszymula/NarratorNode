package models.json_output;

import game.logic.support.rules.SetupModifier;
import json.JSONException;
import json.JSONObject;
import nnode.Game;

public class PhaseJson {

    public static JSONObject toJson(Game game) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("hash", game.phaseHash);
        request.put("length", getLength(game));
        request.put("name", game.narrator.phase.toString());
        return request;
    }

    private static long getLength(Game game) {
        switch (game.narrator.phase){
        case ROLE_PICKING_PHASE:
            return game.narrator.getInt(SetupModifier.ROLE_PICKING_LENGTH);
        case DISCUSSION_PHASE:
            return game.narrator.getInt(SetupModifier.DISCUSSION_LENGTH);
        case TRIAL_PHASE:
            return game.narrator.getInt(SetupModifier.TRIAL_LENGTH);
        case VOTE_PHASE:
            return game.narrator.getInt(SetupModifier.DAY_LENGTH);
        case NIGHT_ACTION_SUBMISSION:
            return game.narrator.getInt(SetupModifier.NIGHT_LENGTH);
        default:
            return 0;
        }
    }

}
