package models.json_output;

import game.logic.RolesList;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.SetupHidden;

public class SetupHiddenJson {

    public static JSONArray toJson(long setupID, RolesList rolesList) throws JSONException {
        JSONArray response = new JSONArray();
        for(SetupHidden hidden: rolesList)
            response.put(toJson(setupID, hidden));
        return response;
    }

    public static JSONObject toJson(long setupID, SetupHidden setupHidden) throws JSONException {
        JSONObject response = new JSONObject();
        response.put("hiddenID", setupHidden.hidden.getID());
        response.put("id", setupHidden.id);
        response.put("setupID", setupID);
        return response;
    }
}
