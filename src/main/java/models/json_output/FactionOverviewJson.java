package models.json_output;

import java.util.Set;

import game.logic.Faction;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;

class FactionOverviewJson {
    public static JSONObject toJson(Faction faction) throws JSONException {
        JSONObject teamJson = new JSONObject();
        teamJson.put("name", faction.getName());
        teamJson.put("color", faction.getColor());
        teamJson.put("id", faction.id);
        return teamJson;
    }

    public static JSONArray toJson(Set<Faction> enemyFactions) throws JSONException {
        JSONArray factionOverviewJson = new JSONArray();
        for(Faction faction: enemyFactions)
            factionOverviewJson.put(toJson(faction));

        return factionOverviewJson;
    }
}
