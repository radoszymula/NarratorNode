package models.json_output;

import game.setups.Setup;
import json.JSONException;
import json.JSONObject;

public class SetupOverview {
    public static JSONObject toJson(Setup setup) throws JSONException {
        JSONObject response = new JSONObject();
        response.put("description", setup.getDescription());
        response.put("id", setup.id);
        response.put("isPreset", setup.isPreset());
        response.put("key", setup.key);
        response.put("max", setup.getMaxPlayerCount());
        response.put("min", setup.getMinPlayerCount());
        response.put("name", setup.getName());
        response.put("ownerID", setup.ownerID);
        response.put("short", setup.getShortDescription());
        return response;
    }
}
