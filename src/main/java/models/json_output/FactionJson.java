package models.json_output;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import game.logic.Faction;
import game.logic.support.rules.FactionModifier;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.Modifiers;

public class FactionJson {

    public static JSONArray toJson(ArrayList<Faction> allTeams) throws JSONException {
        JSONArray factionsJson = new JSONArray();
        for(Faction faction: allTeams)
            factionsJson.put(toJson(faction));
        return factionsJson;
    }

    public static JSONObject toJson(Faction faction) throws JSONException {
        List<FactionRole> factionRoles;
        if(faction.narrator.isStarted())
            factionRoles = faction.narrator.getRolesList().getMembers().getFactionRoles(faction.getColor());
        else{
            factionRoles = new ArrayList<>();
            for(FactionRole role: faction._roleSet.values())
                factionRoles.add(role);
        }

        JSONObject factionJson = new JSONObject();
        factionJson.put("abilities",
                AbilityJson.toJson(faction.getAbilities(), faction.getColor(), faction.getName(), faction.narrator));
        factionJson.put("color", faction.getColor());
        factionJson.put("description", faction.getDescription());
        factionJson.put("details", Faction.getRules(faction));
        factionJson.put("enemies", FactionOverviewJson.toJson(faction.getEnemyFactions()));
        factionJson.put("enemyIDs", getEnemyIDs(faction));
        factionJson.put("factionRoles", FactionRoleJson.toJson(factionRoles));
        factionJson.put("id", faction.id);
        factionJson.put("modifiers", getModifiers(faction));
        factionJson.put("name", faction.getName());
        return factionJson;
    }

    private static JSONArray getEnemyIDs(Faction faction) {
        JSONArray enemyIDs = new JSONArray();
        for(Faction enemyFaction: faction.getEnemyFactions())
            enemyIDs.put(enemyFaction.id);
        return enemyIDs;
    }

    private static JSONArray getModifiers(Faction faction) throws JSONException {
        Modifiers<FactionModifier> modifiers = faction._modifierMap;
        Set<FactionModifier> factionModifiers = modifiers.getModifierIDs();
        JSONArray array = new JSONArray();
        JSONObject object;
        for(FactionModifier modifier: factionModifiers){
            object = new JSONObject();
            object.put("name", modifier.toString());
            object.put("value", modifiers.get(modifier));
            object.put("label", modifier.getLabel(faction.narrator));

            array.put(object);
        }
        return array;
    }
}
