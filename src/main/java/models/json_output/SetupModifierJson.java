package models.json_output;

import game.logic.Narrator;
import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifiers;
import json.JSONException;
import json.JSONObject;

public class SetupModifierJson {

    public static JSONObject toJson(SetupModifiers _rules, Narrator narrator) throws JSONException {
        JSONObject jSetupModifiers = new JSONObject();

        JSONObject jSetupModifier;
        String name;
        Object value;
        for(Rule setupModifier: _rules.rules.values()){
            jSetupModifier = new JSONObject();
            name = setupModifier.id.toString();
            value = setupModifier.getVal();

            jSetupModifier.put("label", setupModifier.id.getLabel(narrator));
            jSetupModifier.put("name", name);
            jSetupModifier.put("value", value);
            jSetupModifiers.put(name, jSetupModifier);
        }
        return jSetupModifiers;
    }

}
