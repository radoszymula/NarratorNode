package models.json_output;

import java.util.Collection;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import nnode.User;

public class UserJson {

    public static JSONObject toJson(User user) throws JSONException {
        JSONObject userJSON = new JSONObject();
        userJSON.put("id", user.id);
        if(user.player != null)
            userJSON.put("name", user.player.getName());
        return userJSON;
    }

    public static JSONObject toJson(long userID) throws JSONException {
        JSONObject userJSON = new JSONObject();
        userJSON.put("id", userID);
        return userJSON;
    }

    public static JSONArray toJson(Collection<User> users) throws JSONException {
        JSONArray usersJson = new JSONArray();
        for(User user: users)
            usersJson.put(toJson(user));
        return usersJson;
    }

}
