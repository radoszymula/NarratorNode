package models.json_output;

import game.logic.Narrator;
import game.logic.support.rules.Modifier;
import game.logic.support.rules.RoleModifier;
import game.roles.Ability;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.Modifiers;

public class ModifierJson {

    static <T extends Modifier> JSONArray getModifiers(Narrator narrator, Modifiers<T> modifiers, T[] roleModifiers)
            throws JSONException {
        JSONArray modifierArray = new JSONArray();
        JSONObject jModifier;
        for(T modifier: roleModifiers){
            jModifier = new JSONObject();
            jModifier.put("label", modifier.getLabel(narrator));
            jModifier.put("name", modifier.toString());
            if(Ability.IsIntModifier(modifier))
                jModifier.put("value", modifiers.getOrDefault(modifier, 0));
            else
                jModifier.put("value", modifiers.getOrDefault(modifier, false));
            modifierArray.put(jModifier);

        }
        return modifierArray;
    }

    public static JSONArray getRoleModifiers(Narrator narrator, Modifiers<RoleModifier> modifiers)
            throws JSONException {
        return getModifiers(narrator, modifiers, RoleModifier.values());
    }

}
