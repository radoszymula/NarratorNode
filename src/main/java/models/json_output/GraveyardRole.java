package models.json_output;

import game.logic.Faction;
import game.logic.Player;
import json.JSONException;
import json.JSONObject;

public class GraveyardRole {

    public static JSONObject toJson(Player player) throws JSONException {
        if(!player.isDead())
            return null;
        Faction graveyardFaction = player.narrator.getFaction(player.getGraveyardColor());
        JSONObject graveyardRole = new JSONObject();
        graveyardRole.put("color", player.getGraveyardColor());
        graveyardRole.put("factionID", graveyardFaction == null ? null : graveyardFaction.id);
        graveyardRole.put("name", player.getGraveyardRoleName());
        return graveyardRole;
    }

}
