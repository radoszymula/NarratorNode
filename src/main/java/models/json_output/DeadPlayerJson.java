package models.json_output;

import game.logic.Player;
import game.logic.PlayerList;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.events.EventHelpers;
import nnode.Game;

public class DeadPlayerJson {

    public static JSONArray toJson(PlayerList players, Game game) throws JSONException {
        JSONArray playersJson = new JSONArray();
        for(Player p: players)
            playersJson.put(toJson(p, game));

        return playersJson;
    }

    public static JSONObject toJson(Player player, Game game) throws JSONException {
        JSONObject playerJson = new JSONObject();
        playerJson.put("name", player.getName());
        playerJson.put("role", GraveyardRole.toJson(player));
        playerJson.put("userID", EventHelpers.getUserID(player, game));
        playerJson.put("deathType", DeathTypeJson.toJson(player.getDeathType()));
        String lastWill = player.getLastWill(null);
        if(lastWill != null)
            playerJson.put("lastWill", lastWill);
        return playerJson;
    }

}
