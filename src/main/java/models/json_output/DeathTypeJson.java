package models.json_output;

import game.logic.DeathType;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;

public class DeathTypeJson {

    public static JSONObject toJson(DeathType deathType) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("dayNumber", deathType.getDeathDay());
        JSONArray types = new JSONArray();
        JSONObject attack;
        for(String[] attacks: deathType.attacks){
            attack = new JSONObject();
            attack.put("text", attacks[1]);
            types.put(attack);
        }
        request.put("attacks", types);
        return request;
    }

}
