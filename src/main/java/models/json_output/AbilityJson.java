package models.json_output;

import game.logic.Narrator;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.AbilityList;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;

public class AbilityJson {

    public static JSONArray toJson(AbilityList abilities, String color, String name, Narrator narrator)
            throws JSONException {
        JSONArray abilitiesJson = new JSONArray();
        if(abilities != null)
            for(Ability ability: abilities)
                abilitiesJson.put(toJson(ability, abilities, color, name, narrator));
        return abilitiesJson;
    }

    private static JSONObject toJson(Ability ability, AbilityList abilities, String color, String name,
            Narrator narrator) throws JSONException {
        JSONObject abilityJson = new JSONObject();
        abilityJson.put("id", ability.id);
        abilityJson.put("description", ability.getAbilityDescription(narrator));
        abilityJson.put("details", ability.getPublicDescription(narrator, color, name, abilities));
        abilityJson.put("modifiers", getModifiers(ability, narrator, abilities.size() == 1));
        abilityJson.put("name", ability.getName());
        abilityJson.put("setupModifierNames", getSetupModifierNames(ability));
        return abilityJson;
    }

    private static JSONArray getSetupModifierNames(Ability ability) {
        JSONArray modifierArray = new JSONArray();
        for(SetupModifier modifier: ability.getSetupModifiers())
            modifierArray.put(modifier.toString());
        return modifierArray;
    }

    private static JSONArray getModifiers(Ability ability, Narrator narrator, boolean singleAbilityRole)
            throws JSONException {
        JSONArray modifierArray = new JSONArray();
        JSONObject jModifier;
        for(AbilityModifier modifier: ability.getAbilityModifiers()){
            jModifier = new JSONObject();
            jModifier.put("label", getLabel(ability, narrator, modifier, singleAbilityRole));
            jModifier.put("name", modifier.toString());
            if(Ability.IsIntModifier(modifier))
                jModifier.put("value", ability.modifiers.getOrDefault(modifier, 0));
            else
                jModifier.put("value",
                        ability.modifiers.getOrDefault(modifier, modifier == AbilityModifier.BACK_TO_BACK));
            modifierArray.put(jModifier);

        }
        return modifierArray;
    }

    private static String getLabel(Ability ability, Narrator narrator, AbilityModifier modifier,
            boolean singleAbility) {
        switch (modifier){
        case BACK_TO_BACK:
            return ability.getBackToBackLabel();
        case CHARGES:
            return ability.getChargeDescription(narrator);
        case COOLDOWN:
            return ability.getCooldownDescription(narrator);
        case HIDDEN:
            return ability.getHiddenStatusRuleLabel();
        case SELF_TARGET:
            return ability.getSelfTargetMemberDescription(singleAbility, true);
        case ZERO_WEIGHTED:
            return ability.getZeroWeightDescription(narrator);
        default:
            return modifier.getLabel(narrator);
        }
    }
}
