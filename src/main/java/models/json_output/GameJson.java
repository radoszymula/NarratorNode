
package models.json_output;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import nnode.Game;

public class GameJson {

    public static JSONObject toJson(Game game) throws JSONException {
        JSONObject gameJson = new JSONObject();
        gameJson.put("dayNumber", game.narrator.getDayNumber()); // TODO remove
        gameJson.put("graveyard", PlayerJson.toJson(game.narrator.getDeadPlayers(), game));
        gameJson.put("host", UserJson.toJson(game.getHost()));
        gameJson.put("id", game.getID());
        gameJson.put("isStarted", game.narrator.isStarted());
        gameJson.put("joinID", game.joinID);
        gameJson.put("moderators", getModerators(game));
        gameJson.put("phase", PhaseJson.toJson(game));
        gameJson.put("players", PlayerJson.toJson(game.narrator._players.sortByName(), game));
        gameJson.put("setup", SetupJson.toJson(game._setup));
        gameJson.put("users", UserJson.toJson(game.phoneBook.values()));
        return gameJson;
    }

    public static JSONArray getModerators(Game game) throws JSONException {
        JSONArray moderators = new JSONArray();
        if(game.hasModerator())
            moderators.put(UserJson.toJson(game.getHost()));
        return moderators;
    }
}
