package models.json_output;

import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import json.JSONException;
import json.JSONObject;
import models.enums.VoteSystemTypes;
import nnode.Game;

public class VoteSettingsJson {

    public static JSONObject toJson(Game game) throws JSONException {
        Narrator narrator = game.narrator;
        JSONObject settings = new JSONObject();
        int voteSystem = narrator.getInt(SetupModifier.VOTE_SYSTEM);
        if(voteSystem == VoteSystemTypes.PLURALITY)
            settings.put("voteSystemType", "plurality");
        else if(voteSystem == VoteSystemTypes.DIMINISHING_POOL)
            settings.put("voteSystemType", "diminishing_pool");
        settings.put("secretVoting", narrator.getBool(SetupModifier.SECRET_VOTES));
        return settings;
    }
}
