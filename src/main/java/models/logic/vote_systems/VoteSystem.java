package models.logic.vote_systems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import game.event.DeathAnnouncement;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.VotingException;
import game.logic.listeners.NarratorListener;
import game.logic.support.HTString;
import game.logic.support.StringChoice;
import game.logic.support.rules.SetupModifier;
import models.enums.GamePhase;

public abstract class VoteSystem {

    protected HashMap<Player, PlayerList> voteList = new HashMap<>();
    protected Narrator narrator;

    public VoteSystem(Narrator narrator) {
        this.narrator = narrator;
    }

    public abstract Player getPlayerOnTrial();

    public abstract PlayerList getPlayersOnTrial();

    public abstract void endPhase();

    public abstract void reset();

    public abstract void vote(Player voter, Player submitter, Player target, String completedCommand);

    public abstract void unvote(Player voter, Player submitter, String completedCommand);

    public abstract void skipVote(Player player, Player submitter, String command);

    public abstract PlayerList getElligibleTargets();

    public abstract void checkVote();

    // deprecated
    public abstract Player getVoteTarget(Player p);

    public void removeFromVotes(PlayerList resets) {
        for(PlayerList list: voteList.values()){
            list.remove(resets);
        }
        for(Player p: resets)
            voteList.remove(p);
    }

    public ArrayList<Message> publicizeVoteEnding(Faction parity) {
        ArrayList<Message> messages = new ArrayList<>();
        Player Skipper = narrator.skipper;
        int dayNumber = narrator.getDayNumber();
        PlayerList lynchedTargets = narrator.getTodaysLynchedTargets();
        DeathAnnouncement e;
        if(lynchedTargets.contains(Skipper)){
            e = new DeathAnnouncement(new PlayerList(), narrator, !DeathAnnouncement.NIGHT_DEATH);
            if(voteList.get(Skipper) == null || voteList.get(Skipper).isEmpty()){
                if(parity == null)
                    e.add("Day " + dayNumber + " ended due to inactivity.");
                else
                    e.add("Day " + dayNumber + " has ended because ", new HTString(parity), " controlled the votes.");
            }else{
                e.add(voteList.get(Skipper), " opted to skip day executions today.");
            }
            e.setDayEndDeath();
            messages.add(e);
        }else{

            for(Player lynched: lynchedTargets){
                StringChoice lyn = new StringChoice(lynched);
                lyn.add(lynched, "You");

                if(lynched.isPoisoned()){
                    e = new DeathAnnouncement(lynched);
                    e.setPicture("poisoner");
                    lyn.add(lynched, "you");

                    StringChoice they = new StringChoice("they");
                    they.add(lynched, "you");

                    e.add(voteList.get(lynched), " attempted to publically execute ", lyn, " but ", they,
                            " were already dead from poison.");
                }else{
                    lyn.add(lynched, "You");
                    e = new DeathAnnouncement(lynched);
                    e.setPicture("executioner");
                    e.add(lyn, " ");
                    StringChoice was = new StringChoice("was");
                    was.add(lynched, "were");
                    e.add(was, " publically executed by ", lynched.getDeathType().getLynchers(), ".");
                }
                e.setDayEndDeath();
                messages.add(e);
            }
        }
        return messages;
    }

    protected void isValidVote(Player voter, Player target) {
        if(narrator.phase != GamePhase.VOTE_PHASE || !narrator.isInProgress())
            throw new VotingException("You cannot vote right now.");

        if(voter == null)
            throw new NullPointerException("Someone has to be a voter.");
        if(voter == target && !narrator.getBool(SetupModifier.SELF_VOTE))
            throw new VotingException("Cannot vote for one's self.");
        if(target == null)
            throw new NullPointerException("target was null.");

        Player skipper = narrator.skipper;
        if(voter == skipper)
            throw new VotingException("Skipper cannot vote.");

        if(voter.isDead())
            throw new VotingException("Dead players cannot vote.  " + voter.getDescription() + " is dead.");
        if(target.isDead())
            throw new VotingException("Dead players cannot be voted.");

        if(voter.isDisenfranchised() && !target.equals(skipper))
            throw new VotingException(voter + ", who is blackmailed, is trying to vote " + target + ".");
    }

    protected void endDiscussion() {
        narrator.phase = GamePhase.VOTE_PHASE;
        List<NarratorListener> listeners = narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onVotePhaseStart();
    }
}
