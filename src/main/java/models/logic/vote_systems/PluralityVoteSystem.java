package models.logic.vote_systems;

import java.util.List;

import game.event.DeathAnnouncement;
import game.event.VoteAnnouncement;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.VotingException;
import game.logic.listeners.NarratorListener;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.logic.support.rules.SetupModifier;
import game.roles.Executioner;
import game.roles.Jester;
import models.enums.GamePhase;
import nnode.Config;

public class PluralityVoteSystem extends VoteSystem {

    public PluralityVoteSystem(Narrator narrator) {
        super(narrator);
        reset();
    }

    @Override
    protected void isValidVote(Player voter, Player target) {
        super.isValidVote(voter, target);
        if(voteList.get(target).contains(voter))
            throw new VotingException(voter + " is already voting this person.");
    }

    @Override
    public Player getPlayerOnTrial() {
        return null;
    }

    @Override
    public PlayerList getPlayersOnTrial() {
        return null;
    }

    @Override
    public void endPhase() {
        if(narrator.phase == GamePhase.DISCUSSION_PHASE){
            endDiscussion();
            return;
        }
        int minLynch = getMinAutoLynchVote();
        checkVote(minLynch, true);
    }

    @Override
    protected void endDiscussion() {
        if(narrator.phase != GamePhase.DISCUSSION_PHASE)
            throw new NarratorException("Game is not in discussion phase.");

        super.endDiscussion();
    }

    // rename to getVoteTarget
    private Player getPrevVote(Player voter) {
        for(Player votedPlayer: voteList.keySet()){
            if(voteList.get(votedPlayer).contains(voter))
                return votedPlayer;
        }
        return null;
    }

    @Override
    public void vote(Player voter, Player submitter, Player target, String completedCommand) {
        isValidVote(voter, target);

        Player prevTarget = getPrevVote(voter);
        VoteAnnouncement e = new VoteAnnouncement(null, voter, target);
        if(completedCommand != null)
            narrator.getEventManager().addCommand(submitter, completedCommand);
        StringChoice sc = new StringChoice(voter);
        sc.add(voter, "You");
        e.add(sc);

        // e.setCommand(voter, CommandHandler.VOTE, target.getName());
        // can't revote
        int toLynch;
        if(prevTarget != null){
            e.prev = prevTarget.getName();
            voteList.get(prevTarget).remove(voter);
            voteList.get(target).add(voter);
            toLynch = getMinAutoLynchVote() - getVoteCountOf(target);

            sc = new StringChoice(target);
            sc.add(target, "you");

            if(prevTarget == narrator.skipper){
                e.add(" decided against skipping the day execution and is now voting for ");
            }else{
                e.add(" changed ", new StringChoice("their").add(voter, "your"), " vote from ", prevTarget, " to ");
            }
            e.add(sc, ". ", numberOfVotesNeeded(toLynch));

            e.finalize();
            checkVote();
            if(narrator.phase == GamePhase.VOTE_PHASE){
                List<NarratorListener> listeners = narrator.getListeners();
                for(int i = 0; i < listeners.size(); i++)
                    listeners.get(i).onVote(voter, target, e);
            }

        }else{
            voteList.get(target).add(voter);
            sc = new StringChoice(target);
            sc.add(target, "you");
            toLynch = getMinAutoLynchVote() - (getVoteCountOf(target));
            ;
            e.add(" voted for ", sc, ". ", numberOfVotesNeeded(toLynch));

            e.finalize();
            checkVote();
            if(narrator.phase == GamePhase.VOTE_PHASE){
                List<NarratorListener> listeners = narrator.getListeners();
                for(int i = 0; i < listeners.size(); i++){
                    try{
                        listeners.get(i).onVote(voter, target, e);
                    }catch(Throwable t){
                        if(Config.getBoolean("test_mode"))
                            throw t;
                        Util.log(t);
                    }
                }
            }
        }
    }

    private Player unvoteHelper(Player voter) {
        Player prevTarget = getPrevVote(voter);
        PlayerList list = voteList.get(prevTarget);
        if(list != null)
            list.remove(voter);
        return prevTarget;
    }

    /**
     * Formatter for number of players needed to hammer the person. This is attached
     * to all events that have the format: Voss has voted Joe (L-3)
     *
     * @param difference the number of players to hammer
     * @return the string to be attached to all vote and unvote events
     */
    public static String numberOfVotesNeeded(int difference) {
        if(difference <= 0)
            return "(Hammer)";
        return "  (L - " + difference + ")";
    }

    private int getVoteCountOf(Player voted) {
        int count = 0;
        PlayerList voters = voteList.get(voted);
        if(voters == null)
            return 0;
        for(Player voter: voters)
            count += voter.getVotePower();
        return count;
    }

    @Override
    public void unvote(Player voter, Player submitter, String completedCommand) {
        if(!narrator.isInProgress())
            throw new VotingException("Vote removal isn't possible right now.");
        if(!voter.isAlive())
            throw new VotingException("Dead players can't vote or unvote.");
        if(submitter == voter.getSkipper())
        	throw new VotingException("Skipper can't vote");

        Player prevVote = getPrevVote(voter);
        if(prevVote == null)
            throw new VotingException(voter + " isn't voting anyone right now.");

        Player prevTarget = unvoteHelper(voter);

        VoteAnnouncement e = new VoteAnnouncement(prevTarget, voter);
        if(completedCommand != null)
            narrator.getEventManager().addCommand(submitter, completedCommand);

        StringChoice sc = new StringChoice(voter);
        sc.add(voter, "You");
        e.add(sc);

        int difference = getMinAutoLynchVote() - getVoteCountOf(prevTarget);

        if(prevTarget == narrator.skipper)
            e.add(" decided against skipping the day execution. " + numberOfVotesNeeded(difference));
        else{
            sc = new StringChoice(prevTarget);
            sc.add(prevTarget, "you");
            e.add(" unvoted ", sc, ". ", numberOfVotesNeeded(difference));
        }

        e.finalize();

        checkVote();
        if(!narrator.isDay())
            return;
        List<NarratorListener> listeners = narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onVoteCancel(voter, prevTarget, e);
    }

    @Override
    public void reset() {
        voteList.clear();
        for(Player p: narrator.getLivePlayers())
            voteList.put(p, new PlayerList());

        voteList.put(narrator.skipper, new PlayerList());
        this.isTied = false;
    }

    public static int getMinLynchVote(Narrator narrator) {
        int minVote = 0;
        int maxVote = 0;
        if(narrator.getBool(SetupModifier.HOST_VOTING)){
            for(Player p: narrator.getLivePlayers()){
                maxVote = Math.max(maxVote, p.getVotePower());
                minVote += p.getVotePower();
            }
            return minVote - maxVote;
        }
        minVote += narrator.getLivePlayers().size();// accounts for the extra person out of skip
        minVote /= 2;
        minVote++;
        return minVote;
    }

    private int getMinAutoLynchVote() {
        return getMinLynchVote(narrator);
    }

    @Override
    public void checkVote() {
        checkVote(getMinAutoLynchVote(), false);
    }

    private boolean isTied = false;

    private Player getHighestVoted() {
        Player target = null;
        int maxCount = 0, count;
        for(Player voted: this.voteList.keySet()){
            count = getVoteCountOf(voted);
            if(maxCount < count){
                maxCount = count;
                target = voted;
            }else if(maxCount == count)
                target = null;
        }

        return target;
    }

    private boolean noOneBeingVoted() {
        for(PlayerList voters: this.voteList.values()){
            if(voters != null && !voters.isEmpty())
                return false;
        }
        return true;
    }

    // should day end
    private void checkVote(int minLynchVote, boolean dayForceEnding) {
        if(narrator.phase != GamePhase.VOTE_PHASE) // todo remove
            throw new VotingException("Votes cannot be submitted right now.");
        // check if target has enough votes to close the booths

        boolean dayEnding = false;
        Player Skipper = narrator.skipper;

        if(narrator.getLiveSize() <= 2){
            narrator.endDay(null);
            return;
        }
        if(minLynchVote == 0){
            if(dayForceEnding)
                narrator.endDay(null);
            return;
        }
        Player target = getHighestVoted();
        if(target == null && dayForceEnding){
            if(noOneBeingVoted() && narrator.getBool(SetupModifier.SKIP_VOTE)){
                narrator.endDay(null);
                return;
            }
            this.isTied = true;
            return;
        }

        if(getVoteCountOf(target) < minLynchVote && !isTied && !dayForceEnding){
            Faction t = narrator.parityReached();
            if(t != null)
                narrator.endDay(t);
            return;
        }
        PlayerList voters = voteList.get(target);
        if(Skipper == target)
            dayEnding = true;
        else if(target == null)
            return;
        else{
            target.setLynchDeath(voters);
            giveExecutionerWin(target);
            narrator._lynches--;
            narrator.checkProgress();
            if(narrator.isFinished())
                return;
            if(narrator._lynches == 0 || narrator.getLiveSize() < 3){
                dayEnding = true;
            }else{
                removeFromVotes(Player.list(target));
            }

            if(!dayForceEnding && !dayEnding && narrator._lynches != 1){
                DeathAnnouncement da = new DeathAnnouncement(target, !DeathAnnouncement.NIGHT_DEATH);
                if(narrator._lynches != 2)
                    da.add("There are " + (narrator._lynches - 1) + " more day executions planned for today.");
                else
                    da.add("There is 1 more day execution planned for today.");
                narrator.announcement(da);
            }
        }

        // jester check
        if(target.is(Jester.class)){
            for(Player p: voters){
                p.votedForJester(true);
            }
        }

        if(dayEnding){
            narrator.endDay(null);
            return;
        }
        Faction t = narrator.parityReached();
        if(t != null)
            narrator.endDay(t);
    }

    private void giveExecutionerWin(Player target) {
        for(Player exec: narrator._players.filter(Executioner.class)){
            Executioner r = exec.getAbility(Executioner.class);
            if(r.getTarget().equals(target))
                r.setWon();
        }
    }

    @Override
    public void skipVote(Player player, Player submitter, String completedCommand) {
        Player skipper = narrator.skipper;
        if(narrator.phase != GamePhase.VOTE_PHASE || !narrator.isInProgress())
            throw new VotingException("Votes cannot be submitted right now.");
        if(!narrator.getBool(SetupModifier.SKIP_VOTE))
            throw new VotingException("Skipping is not allowed.");
        if(player.isDead())
            throw new VotingException(player + " is dead and can't vote.");
        Player prevTarget = this.getPrevVote(player);
        if(prevTarget == skipper)
            throw new VotingException(player.getName() + " is already skipping the day");

        if(player == skipper)
            throw new VotingException("Skip Day cannot vote!");
        if(narrator.getInt(SetupModifier.NIGHT_LENGTH) == 0)
            throw new VotingException("Day cannot be skipped in nightless game");

        StringChoice sc = new StringChoice(player);
        sc.add(player, "You");

        VoteAnnouncement e = new VoteAnnouncement(prevTarget, player, skipper);
        e.add(sc);

        if(prevTarget != null)
            unvoteHelper(player);
        narrator.getEventManager().addCommand(submitter, completedCommand);

        voteList.get(skipper).add(player);

        int difference = getMinAutoLynchVote() - getVoteCountOf(skipper);

        if(prevTarget != null){
            sc = new StringChoice(prevTarget);
            sc.add(prevTarget, "you");
            StringChoice wants = new StringChoice("wants");
            sc.add(player, "voted");

            e.add(" decided against democratically executing ", sc, " and instead ", wants,
                    " to skip the day. " + numberOfVotesNeeded(difference));
            e.prev = prevTarget.getName();
        }else
            e.add(" voted to skip the day. " + numberOfVotesNeeded(difference));

        e.finalize();
        checkVote();

        if(!narrator.isDay())
            return;
        List<NarratorListener> listeners = narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onVote(player, skipper, e);

    }

    @Override
    public Player getVoteTarget(Player p) {
        for(Player votedPlayer: this.voteList.keySet()){
            if(this.voteList.get(votedPlayer).contains(p))
                return votedPlayer;
        }
        return null;
    }

    @Override
    public PlayerList getElligibleTargets() {
        return narrator.getLivePlayers();
    }
}
