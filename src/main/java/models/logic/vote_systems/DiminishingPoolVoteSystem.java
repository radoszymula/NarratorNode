package models.logic.vote_systems;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import game.event.Announcement;
import game.event.DeathAnnouncement;
import game.event.VoteAnnouncement;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.VotingException;
import game.logic.listeners.NarratorListener;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.logic.support.rules.SetupModifier;
import models.enums.GamePhase;
import nnode.Config;

public class DiminishingPoolVoteSystem extends VoteSystem {

    private PlayerList playersOnTrial;
    private PlayerList alreadyTrialedPlayers = new PlayerList();
    private int trialIndex = -1;
    private int tieCount = 3;

    public DiminishingPoolVoteSystem(Narrator narrator) {
        super(narrator);
        reset();
    }

    @Override
    public Player getPlayerOnTrial() {
        if(playersOnTrial == null || playersOnTrial.isEmpty())
            return null;
        return playersOnTrial.get(trialIndex);
    }

    @Override
    public PlayerList getPlayersOnTrial() {
        return playersOnTrial.copy();
    }

    @Override
    public void endPhase() {
        if(narrator.phase == GamePhase.DISCUSSION_PHASE || narrator.phase == GamePhase.TRIAL_PHASE){
            trialIndex++;
            if(trialIndex == 0){
                super.endDiscussion();
            }else if(trialIndex == playersOnTrial.size()){
                super.endDiscussion();
                this.alreadyTrialedPlayers.add(playersOnTrial);
                if(alreadyTrialedPlayers.size() == 1 && playersOnTrial.size() == 1){
                    playersOnTrial = narrator.getLivePlayers();
                }else{
                    playersOnTrial = alreadyTrialedPlayers.copy();
                }
                if(narrator.getBool(SetupModifier.SKIP_VOTE))
                    playersOnTrial.add(narrator.skipper);
                this.setVoteTargets(playersOnTrial);
                playersOnTrial = null;
            }else{
                startTrial(playersOnTrial.get(trialIndex));
            }
        }else{
            playersOnTrial = new PlayerList();
            PlayerList voters;
            for(Player voted: voteList.keySet()){
                voters = voteList.get(voted);
                if(voted != narrator.skipper && voters != null && voters.size() >= 2
                        && !alreadyTrialedPlayers.contains(voted)){
                    playersOnTrial.add(voted);
                }
            }

            if(playersOnTrial.isEmpty() || narrator.getInt(SetupModifier.TRIAL_LENGTH) == 0){
                killHighestVotedPlayer();
            }else{
                if(playersOnTrial.size() > 1)
                    playersOnTrial.shuffle(new Random(), "Shuffling trialed players");
                this.narrator.phase = GamePhase.TRIAL_PHASE;
                this.trialIndex = 0;
                startTrial(playersOnTrial.get(0));
            }
        }
    }

    private void startTrial(Player playerOnTrial) {
        Announcement e = new Announcement(narrator);
        StringChoice sc = new StringChoice("");
        sc.add(playerOnTrial, "Your trial has begun.");
        e.add(sc);

        sc = new StringChoice("The trial of ");
        sc.add(playerOnTrial, "");
        e.add(sc);

        sc = new StringChoice(playerOnTrial);
        sc.add(playerOnTrial, "");
        e.add(sc);

        sc = new StringChoice(" has begun.");
        sc.add(playerOnTrial, "");
        e.add(sc);

        e.finalize();
        List<NarratorListener> listeners = narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onTrialStart(playerOnTrial);

    }

    private void endGameInDraw() {
        narrator.modkill(narrator.getLivePlayers());
    }

    private boolean closingVotePool() {
        HashMap<Player, Integer> voteToCount = new HashMap<>();
        ArrayList<Player> votedPlayers = new ArrayList<>();
        int totalSubmittedVotes = 0, count;
        for(Player voted: this.voteList.keySet()){
            count = getVoteCountOf(voted);
            totalSubmittedVotes += count;
            voteToCount.put(voted, count);
            if(count != 0)
                votedPlayers.add(voted);
        }

        votedPlayers.sort(new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                return voteToCount.get(o2) - voteToCount.get(o1);
            }
        });
        if(votedPlayers.size() < 2)
            return false;
        Player topVoted = votedPlayers.get(0);
        Player leastVoted = votedPlayers.get(votedPlayers.size() - 1);
        int topVotedVoteCount = voteToCount.get(topVoted);
        if(topVotedVoteCount * 2 > totalSubmittedVotes)
            return false;
        int minVote = voteToCount.get(leastVoted);
        while (voteToCount.get(leastVoted) == minVote){
            votedPlayers.remove(leastVoted);
            if(votedPlayers.size() < 2)
                return false;
            leastVoted = votedPlayers.get(votedPlayers.size() - 1);
        }
        if(votedPlayers.size() < 2)
            return false;

        this.setVoteTargets(new PlayerList(votedPlayers));
        return true;
    }

    private void removeNotVotedPlayers() {
        PlayerList votedPlayers = new PlayerList();
        int count;
        for(Player voted: this.voteList.keySet()){
            count = getVoteCountOf(voted);
            if(count != 0)
                votedPlayers.add(voted);
        }
        if(votedPlayers.size() > 1)
            this.setVoteTargets(votedPlayers);
    }

    private void killHighestVotedPlayer() {
        Player highestVoted = getHighestVoted();
        if(closingVotePool())
            return;
        if(highestVoted == null){
            tieCount--;
            if(tieCount == 0)
                endGameInDraw();
            else
                pushVoteResetEvent();
            removeNotVotedPlayers();
            return;
        }
        removeNotVotedPlayers();
        boolean dayEnding = false;
        if(highestVoted != narrator.skipper)
            highestVoted.setLynchDeath(voteList.get(highestVoted));
        narrator._lynches--;
        narrator.checkProgress();
        if(narrator._lynches == 0 || narrator.getLiveSize() < 3){
            dayEnding = true;
        }else{
            removeFromVotes(Player.list(highestVoted));
        }

        if(!dayEnding && narrator._lynches != 1){
            boolean nightDeath = false;
            DeathAnnouncement da = new DeathAnnouncement(highestVoted, nightDeath);
            if(narrator._lynches != 2)
                da.add("There are " + (narrator._lynches - 1) + " more day executions planned for today.");
            else
                da.add("There is 1 more day execution planned for today.");
            narrator.announcement(da);
        }

        if(dayEnding){
            narrator.endDay(narrator.parityReached());
        }
    }

    private int getVoteCountOf(Player voted) {
        int count = 0;
        PlayerList voters = voteList.get(voted);
        if(voters == null)
            return 0;
        for(Player voter: voters)
            count += voter.getVotePower();
        return count;
    }

    private Player getHighestVoted() {
        Player target = null;
        int maxCount = 0, count, totalVotes = 0;
        for(Player voted: this.voteList.keySet()){
            if(voted == narrator.skipper && !narrator.getBool(SetupModifier.SKIP_VOTE))
                continue;
            count = getVoteCountOf(voted);
            totalVotes += count;
            if(maxCount < count){
                maxCount = count;
                target = voted;
            }else if(maxCount == count)
                target = null;
        }

        if(maxCount == 0 && narrator.getBool(SetupModifier.SKIP_VOTE))
            return narrator.skipper;
        if(target == null && this.alreadyTrialedPlayers.size() == 1)
            return this.alreadyTrialedPlayers.getFirst();
        if(maxCount * 2 <= totalVotes)
            target = null;

        return target;
    }

    @Override
    public void reset() {
        PlayerList allowedPlayers = narrator.getLivePlayers();
        if(narrator.getBool(SetupModifier.SKIP_VOTE))
            allowedPlayers.add(narrator.skipper);
        setVoteTargets(allowedPlayers);

        playersOnTrial = null;
        alreadyTrialedPlayers.clear();
        if(narrator.getInt(SetupModifier.DISCUSSION_LENGTH) == 0)
            trialIndex = 0;
        else
            trialIndex = -1;
        tieCount = 3;
    }

    private void setVoteTargets(PlayerList allowedVoteTargets) {
        voteList.clear();
        for(Player p: allowedVoteTargets)
            voteList.put(p, new PlayerList());
    }

    // rename to getVoteTarget
    private Player getPrevVote(Player voter) {
        for(Player votedPlayer: voteList.keySet()){
            if(voteList.get(votedPlayer).contains(voter))
                return votedPlayer;
        }
        return null;
    }

    @Override
    public void vote(Player voter, Player submitter, Player target, String completedCommand) {
        isValidVote(voter, target);

        Player prevTarget = getPrevVote(voter);
        VoteAnnouncement e = new VoteAnnouncement(null, voter, target);
        if(completedCommand != null)
            narrator.getEventManager().addCommand(submitter, completedCommand);
        StringChoice sc = new StringChoice(voter);
        sc.add(voter, "You");
        e.add(sc);

        // e.setCommand(voter, CommandHandler.VOTE, target.getName());
        // can't revote
        if(prevTarget != null){
            e.prev = prevTarget.getName();
            voteList.get(prevTarget).remove(voter);
            voteList.get(target).add(voter);

            sc = new StringChoice(target);
            sc.add(target, "you");

            if(prevTarget == narrator.skipper){
                e.add(" decided against skipping the day execution and is now voting for ");
            }else{
                e.add(" changed ", new StringChoice("their").add(voter, "your"), " vote from ", prevTarget, " to ");
            }
            e.add(sc, ". ");

            e.finalize();
            checkVote();
            if(narrator.phase == GamePhase.VOTE_PHASE){
                List<NarratorListener> listeners = narrator.getListeners();
                for(int i = 0; i < listeners.size(); i++)
                    listeners.get(i).onVote(voter, target, e);
            }

        }else{
            voteList.get(target).add(voter);
            sc = new StringChoice(target);
            sc.add(target, "you");
            e.add(" voted for ", sc, ". ");

            e.finalize();
            checkVote();
            if(narrator.phase == GamePhase.VOTE_PHASE){
                List<NarratorListener> listeners = narrator.getListeners();
                for(int i = 0; i < listeners.size(); i++){
                    try{
                        listeners.get(i).onVote(voter, target, e);
                    }catch(Throwable t){
                        if(Config.getBoolean("test_mode"))
                            throw t;
                        Util.log(t);
                    }
                }
            }
        }
    }

    private Player unvoteHelper(Player voter) {
        Player prevTarget = getPrevVote(voter);
        PlayerList list = voteList.get(prevTarget);
        if(list != null)
            list.remove(voter);
        return prevTarget;
    }

    @Override
    public void unvote(Player voter, Player submitter, String completedCommand) {
        if(!narrator.isInProgress())
            throw new VotingException("Vote removal isn't possible right now.");
        if(!voter.isAlive())
            throw new VotingException("Dead players can't vote or unvote.");

        Player prevVote = getPrevVote(voter);
        if(prevVote == null)
            throw new VotingException(voter + " isn't voting anyone right now.");

        Player prevTarget = unvoteHelper(voter);

        VoteAnnouncement e = new VoteAnnouncement(prevTarget, voter);
        if(completedCommand != null)
            narrator.getEventManager().addCommand(submitter, completedCommand);

        StringChoice sc = new StringChoice(voter);
        sc.add(voter, "You");
        e.add(sc);

        if(prevTarget == narrator.skipper)
            e.add(" decided against skipping the day execution. ");
        else{
            sc = new StringChoice(prevTarget);
            sc.add(prevTarget, "you");
            e.add(" unvoted ", sc, ". ");
        }

        e.finalize();

        checkVote();
        if(narrator.isDay()){
            List<NarratorListener> listeners = narrator.getListeners();
            for(int i = 0; i < listeners.size(); i++)
                listeners.get(i).onVoteCancel(voter, prevTarget, e);
        }
    }

    @Override
    public void skipVote(Player player, Player submitter, String completedCommand) {
        Player skipper = narrator.skipper;
        if(narrator.phase != GamePhase.VOTE_PHASE || !narrator.isInProgress())
            throw new VotingException("Votes cannot be submitted right now.");
        if(!narrator.getBool(SetupModifier.SKIP_VOTE))
            throw new VotingException("Skipping is not allowed.");
        if(player.isDead())
            throw new VotingException(player + " is dead and can't vote.");
        Player prevTarget = this.getPrevVote(player);
        if(prevTarget == skipper)
            throw new VotingException(player.getName() + " is already skipping the day");

        if(player == skipper)
            throw new VotingException("Skip Day cannot vote!");
        if(narrator.getInt(SetupModifier.NIGHT_LENGTH) == 0)
            throw new VotingException("Day cannot be skipped in nightless game");

        StringChoice sc = new StringChoice(player);
        sc.add(player, "You");

        VoteAnnouncement e = new VoteAnnouncement(prevTarget, player, skipper);
        e.add(sc);

        if(prevTarget != null)
            unvoteHelper(player);
        narrator.getEventManager().addCommand(submitter, completedCommand);

        voteList.get(skipper).add(player);

        if(prevTarget != null){
            sc = new StringChoice(prevTarget);
            sc.add(prevTarget, "you");
            StringChoice wants = new StringChoice("wants");
            sc.add(player, "voted");

            e.add(" decided against democratically executing ", sc, " and instead ", wants, " to skip the day. ");
            e.prev = prevTarget.getName();
        }else
            e.add(" voted to skip the day.");

        e.finalize();
        checkVote();

        if(!narrator.isDay())
            return;
        List<NarratorListener> listeners = narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onVote(player, skipper, e);
    }

    @Override
    public void checkVote() {
        if(!narrator.isInProgress() || narrator.getLiveSize() > 2)
            return;
        narrator.endDay(narrator.parityReached());
    }

    @Override
    public Player getVoteTarget(Player p) {
        for(Player voted: this.voteList.keySet()){
            if(this.voteList.get(voted).contains(p))
                return voted;
        }
        return null;
    }

    @Override
    protected void isValidVote(Player voter, Player target) {
        super.isValidVote(voter, target);
        if(!voteList.containsKey(target))
            throw new VotingException("This person may not be voted anymore.");
        if(voteList.get(target).contains(voter))
            throw new VotingException(voter + " is already voting this person.");
    }

    private void pushVoteResetEvent() {
        List<NarratorListener> listeners = narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onVotePhaseReset(tieCount);
    }

    @Override
    public PlayerList getElligibleTargets() {
        return new PlayerList(voteList.keySet());
    }

}
