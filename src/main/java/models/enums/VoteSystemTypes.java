package models.enums;

public class VoteSystemTypes {

    public static final int PLURALITY = 1;
    public static final int DIMINISHING_POOL = 2;
}
