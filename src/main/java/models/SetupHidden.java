package models;

import java.util.Comparator;
import java.util.Set;

import game.logic.support.Constants;
import game.roles.Hidden;
import game.setups.Setup;

public class SetupHidden {

    public Setup setup;
    public Hidden hidden;
    public long id;
    public boolean isExposed = false;

    public SetupHidden(Hidden hidden, Setup setup) {
        this.setup = setup;
        this.hidden = hidden;
    }

    public SetupHidden(Hidden hidden) {
        this.hidden = hidden;
    }

    public static Comparator<? super SetupHidden> RandomComparator() {
        return new Comparator<SetupHidden>() {
            @Override
            public int compare(SetupHidden r1, SetupHidden r2) {
                Set<String> color1 = r1.hidden.getColors();
                Set<String> color2 = r2.hidden.getColors();

                if(color1.size() != color2.size())
                    return color1.size() - color2.size();
                if(!color1.equals(color2))
                    return teamColorOrdering(color1.iterator().next(), color2.iterator().next());

                boolean l1 = r1.hidden.isRandom();
                boolean l2 = r2.hidden.isRandom();
                if(l1 && !l2)
                    return 1;
                if(!l1 && l2)
                    return -1;

                return r1.hidden.getName().compareTo(r2.hidden.getName());
            }
        };
    }

    public static int teamColorOrdering(String t1, String t2) {
        int i = java.util.Arrays.asList(teamOrderings).indexOf(t1);
        int j = java.util.Arrays.asList(teamOrderings).indexOf(t2);
        if(i == -1 && j == -1){
            return t1.compareTo(t2);
        }
        return difference(i, j);
    }

    public static final String[] teamOrderings = new String[] {
            Setup.TOWN_C, Setup.MAFIA_C, Setup.YAKUZA_C, Setup.BENIGN_C, Setup.CULT_C, Setup.OUTCAST_C, Setup.THREAT_C,
            Constants.A_NEUTRAL, Constants.A_RANDOM
    };

    public static int difference(int i, int j) {
        if(i < j)
            return -1;
        if(i > j)
            return 1;

        return 0;
    }

    @Override
    public int hashCode() {
        return hidden.hashCode();
    }

    @Override
    public String toString() {
        return hidden.toString();
    }
}
