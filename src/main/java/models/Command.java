package models;

import java.util.ArrayList;
import java.util.List;

import game.logic.Player;

public class Command {

    public Long playerID;
    public String text;

    public Command(Player player, String text) {
        if(player != null && player != player.getSkipper())
            this.playerID = player.databaseID;
        this.text = text;
    }

    public Command(Long playerID, String text) {
        this.playerID = playerID;
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!Command.class.equals(o.getClass()))
            return false;
        Command command = (Command) o;
        return this.playerID.equals(command.playerID) && this.text.equals(command.text);
    }

    @Override
    public int hashCode() {
        return (playerID + " " + text).hashCode();
    }

    @Override
    public String toString() {
        return playerID + ":\t" + text + "\n";
    }

    public static List<String> getText(ArrayList<Command> commands) {
        List<String> texts = new ArrayList<>();
        for(Command command: commands)
            texts.add(command.text);
        return texts;
    }
}
