package models.serviceResponse;

import json.JSONObject;

public class ActionResponse {
    public boolean isSkipping;
    public JSONObject voteInfo;
    public JSONObject actions;
}
