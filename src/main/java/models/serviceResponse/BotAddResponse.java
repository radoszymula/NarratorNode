package models.serviceResponse;

import game.logic.PlayerList;
import game.setups.Setup;
import nnode.Game;

public class BotAddResponse {
    public Game game;
    public Setup setup;
    public PlayerList players;

    public BotAddResponse(Game game, PlayerList players) {
        this.game = game;
        this.setup = game._setup;
        this.players = players;
    }
}
