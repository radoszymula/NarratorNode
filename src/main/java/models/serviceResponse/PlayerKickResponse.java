package models.serviceResponse;

import game.setups.Setup;
import nnode.User;

public class PlayerKickResponse {
    public User host;
    public Setup setup;

    public PlayerKickResponse(User host, Setup setup) {
        this.host = host;
        this.setup = setup;
    }
}
