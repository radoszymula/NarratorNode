package models.serviceResponse;

public class ModifierServiceResponse {
    public long setupID;
    public Object value;

    public ModifierServiceResponse(long setupID, Object value) {
        this.setupID = setupID;
        this.value = value;
    }

    public ModifierServiceResponse(long setupID, boolean value) {
        this.setupID = setupID;
        this.value = value;
    }

    public ModifierServiceResponse(long setupID, int value) {
        this.setupID = setupID;
        this.value = value;
    }
}
