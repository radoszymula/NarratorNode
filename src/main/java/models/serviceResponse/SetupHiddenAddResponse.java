package models.serviceResponse;

import models.SetupHidden;

public class SetupHiddenAddResponse {
    public SetupHidden setupHidden;
    public long setupID;
    public long gameID;

    public SetupHiddenAddResponse(SetupHidden setupHidden, long setupID, long gameID) {
        this.setupHidden = setupHidden;
        this.setupID = setupID;
        this.gameID = gameID;
    }
}
