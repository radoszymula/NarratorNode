package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import game.logic.Faction;
import game.logic.Role;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.roles.Ability;
import util.ModifierUtil;

public class FactionRole {

    public Modifiers<RoleModifier> roleModifiers;
    public Map<Class<? extends Ability>, Modifiers<AbilityModifier>> abilityModifiers;
    public Faction faction;
    public Role role;
    public long id;
    public FactionRole receivedRole;

    public FactionRole(Faction faction, Role role) {
        this.faction = faction;
        this.role = role;
        this.roleModifiers = new Modifiers<>();
        this.abilityModifiers = new HashMap<>();
    }

    @Override
    public String toString() {
        return faction.getName() + " " + role.getName();
    }

    public String getColor() {
        return faction.getColor();
    }

    public String getName() {
        return role.getName();
    }
    
    public boolean isUnique() {
    	return roleModifiers.getOrDefault(RoleModifier.UNIQUE, false)
    			|| role.modifiers.getOrDefault(RoleModifier.UNIQUE, false);
    }

    public FactionRole copy() {
        FactionRole factionRole = new FactionRole(faction, role.deepCopy());
        factionRole.roleModifiers = Modifiers.copy(roleModifiers);
        Modifiers<AbilityModifier> abilityModifiers;
        for(Class<? extends Ability> abilityClass: this.abilityModifiers.keySet()) {
        	abilityModifiers = this.abilityModifiers.get(abilityClass);
        	factionRole.abilityModifiers.put(abilityClass, Modifiers.copy(abilityModifiers));
        }
        if(this.receivedRole != null)
        	factionRole.receivedRole = this.receivedRole.copy();
        return factionRole;
    }

    public ArrayList<String> getPublicDescription() {
    	FactionRole factionRoleCopy = this.copy();
    	ModifierUtil.resolveOverrides(factionRoleCopy);
    	
    	Role role = factionRoleCopy.role;
        ArrayList<String> ruleTexts = new ArrayList<>();

        for(Ability a: role._abilities)
            ruleTexts.addAll(a.getPublicDescription(role.narrator, getColor(), role.name, role._abilities));

        ruleTexts.addAll(Ability.GetModifierRuleTexts(role.modifiers));

        return ruleTexts;
    }

    public FactionRole getPlayerVisibleFactionRole() {
        if(this.receivedRole == null)
            return this;
        return this.receivedRole;
    }
}
