package models.schemas;

import game.logic.support.rules.AbilityModifier;

public class AbilityModifierSchema extends ModifierSchema<AbilityModifier> {

    public AbilityModifierSchema() {
    }

    public AbilityModifierSchema(AbilityModifier abilityModifier, Object value) {
        super(abilityModifier, value);
    }

}
