package models.schemas;

import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.Rule;
import game.logic.support.rules.RuleBool;
import game.logic.support.rules.RuleInt;

public class FactionModifierSchema extends ModifierSchema<FactionModifier> {

    public Rule createObject() {
        if(this.intValue != null)
            return new RuleInt(FactionModifier.valueOf(this.name.toUpperCase()), this.intValue);
        return new RuleBool(FactionModifier.valueOf(this.name.toUpperCase()), this.boolValue);
    }

}
