package models.schemas;

import java.util.HashMap;

import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifier;

public class GameSchema {
    public String hostName;
    public String setupKey;
    public Long setupID;
    public HashMap<SetupModifier, Rule> rules = new HashMap<>();
    public boolean isPublic;
}
