package models.schemas;

import java.util.Set;

public class FactionAbilitySchema {
    public long id;
    public String name;
    public Set<FactionAbilityModifierSchema> modifiers;

    @Override
    public String toString() {
        return name;
    }
}
