package models.schemas;

import game.logic.support.rules.RoleModifier;

public class RoleModifierSchema extends ModifierSchema<RoleModifier> {

	public RoleModifierSchema() {
        super();
    }

    public RoleModifierSchema(RoleModifier modifier, Object value) {
        super(modifier, value);
    }

}
