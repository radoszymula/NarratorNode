package models.schemas;

public class SetupHiddenSchema {
    public long hiddenID;
    public long id;
    public boolean isExposed;
}
