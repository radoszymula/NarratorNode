package models.schemas;

import game.logic.support.rules.AbilityModifier;

public class FactionAbilityModifierSchema extends ModifierSchema<AbilityModifier> {
    public FactionAbilityModifierSchema() {
        super();
    }
}
