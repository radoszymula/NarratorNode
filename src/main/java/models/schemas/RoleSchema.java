package models.schemas;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Role;
import game.logic.support.rules.RoleModifier;
import game.roles.Ability;

public class RoleSchema {
    public long id;
    public String name;
    public Map<Long, RoleAbilitySchema> abilities;
    public Set<RoleModifierSchema> modifiers;

    public RoleSchema() {
    }

    public RoleSchema(Role role) {
        this.id = role.getID();
        this.name = role.getName();
        this.abilities = new HashMap<>();
        this.modifiers = new HashSet<>();
        for(Ability ability: role._abilities)
            this.abilities.put(ability.id, new RoleAbilitySchema(ability));
        Object value;
        for(RoleModifier roleModifier: role.modifiers.getModifierIDs()){
            value = role.modifiers.get(roleModifier);
            this.modifiers.add(new RoleModifierSchema(roleModifier, value));
        }
    }

    @Override
    public String toString() {
        return name;
    }

    public static class RoleSchemaSorter implements Comparator<Role> {
        @Override
        public int compare(Role a, Role b) {
            return a.toString().compareTo(b.toString());
        }
    }

    @Override
    public int hashCode() {
        List<RoleAbilitySchema> abilityList = new LinkedList<>(abilities.values());
        Collections.sort(abilityList, new Comparator<RoleAbilitySchema>() {
            @Override
            public int compare(RoleAbilitySchema o1, RoleAbilitySchema o2) {
                return o1.name.compareTo(o2.name);
            }

        });

        List<RoleModifierSchema> roleModifiersList = new LinkedList<>(modifiers);
        Collections.sort(roleModifiersList, new Comparator<RoleModifierSchema>() {
            @Override
            public int compare(RoleModifierSchema o1, RoleModifierSchema o2) {
                return o1.name.compareTo(o2.name);
            }

        });

        String abilitiesString = abilityList.toString();
        String modifiersString = roleModifiersList.toString();
        return (name + abilitiesString + modifiersString).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o.getClass() != getClass())
            return false;
        return o.hashCode() == hashCode();
    }
}
