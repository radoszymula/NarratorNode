package models.schemas;

public class GameOverviewSchema {

    public long hostID;
    public String instanceID;
    public boolean isPrivate;
    public long seed;

}
