package models.schemas;

import java.util.Map;
import java.util.Set;

public class FactionRoleSchema {

    public long factionID, roleID, id;
    public Long receivedFactionRoleID;
    public Map<Long, Set<AbilityModifierSchema>> abilityModifiers;
    public Set<RoleModifierSchema> roleModifiers;
}
