package models.schemas;

import java.util.Map;
import java.util.Set;

public class FactionSchema {
    public long id;
    public String color;
    public String name;
    public String description;

    public Map<Long, FactionAbilitySchema> abilities;
    public Set<FactionModifierSchema> modifiers;
}
