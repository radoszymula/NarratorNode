package models.schemas;

import java.util.HashSet;
import java.util.Set;

import game.logic.support.rules.AbilityModifier;
import game.roles.Ability;

public class RoleAbilitySchema {
    public long id;
    public String name;
    public Set<AbilityModifierSchema> modifiers;

    public RoleAbilitySchema() {
    }

    public RoleAbilitySchema(Ability ability) {
        this.id = ability.id;
        this.name = ability.getClass().getSimpleName();
        this.modifiers = new HashSet<>();
        Object value;
        if(ability.modifiers == null)
            return;
        for(AbilityModifier abilityModifier: ability.modifiers.getModifierIDs()){
            value = ability.modifiers.get(abilityModifier);
            this.modifiers.add(new AbilityModifierSchema(abilityModifier, value));
        }
    }

    public Ability getAbility() {
        Ability ability = Ability.CREATOR(name);
        ability.id = id;
        return ability;
    }

    @Override
    public String toString() {
        if(modifiers == null)
            return name;
        return name + modifiers.toString();
    }
}
