package models.schemas;

import java.util.Set;

public class HiddenSchema {
    public long id;
    public String name;
    public Set<Long> factionRoleIDs;

    @Override
    public String toString() {
        return name;
    }
}
