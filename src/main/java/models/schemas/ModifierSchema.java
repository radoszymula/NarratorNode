package models.schemas;

import java.util.Comparator;

public abstract class ModifierSchema<T> {

    public String name;
    public Boolean boolValue;
    public Integer intValue;
    public T modifier;

    public ModifierSchema() {
    }

    public ModifierSchema(T modifier, Object rule) {
        this.name = modifier.toString();
        this.modifier = modifier;
        if(rule instanceof Boolean)
            this.boolValue = (Boolean) rule;
        else
            this.intValue = (Integer) rule;
    }

    public boolean isInt() {
        return intValue != null;
    }

    public static class ModifierSchemaSorter<T> implements Comparator<ModifierSchema<T>> {
		@Override
		public int compare(ModifierSchema<T> a, ModifierSchema<T> b) {
			return a.name.compareTo(b.name);
		}
    }

    @Override
    public String toString() {
        if(boolValue != null)
            return name + boolValue;
        return name + intValue;
    }

}
