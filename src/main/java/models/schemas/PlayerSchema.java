package models.schemas;

public class PlayerSchema {

    public long id;
    public String name;
    public long factionRoleID, hiddenID, userID;
    public boolean isExited;

    public String toString() {
        return "PlayerSchema " + name + "{" + id + "}";
    }
}
