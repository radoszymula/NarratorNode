package models.events;

import json.JSONException;
import json.JSONObject;
import models.json_output.PhaseJson;
import nnode.Game;
import services.VoteService;

public class VoteResetEvent {

    public static JSONObject request(Game game, int resetsRemaining) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("joinID", game.joinID);
        request.put("resetsRemaining", resetsRemaining);
        request.put("event", "votePhaseReset");
        request.put("phase", PhaseJson.toJson(game));
        request.put("votes", VoteService.getVotes(game));
        return request;
    }

}
