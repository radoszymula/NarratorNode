package models.events;

import json.JSONException;
import json.JSONObject;
import models.json_output.PlayerJson;
import nnode.Game;

public class PhaseEndBidEvent {
    public static JSONObject request(Game game) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "phaseEndBid");
        request.put("gameID", game.getID());
        request.put("isStarted", game.narrator.isStarted());
        request.put("joinID", game.joinID);
        request.put("players", PlayerJson.toJson(game.narrator._players, game));
        return request;
    }
}
