package models.events;

import game.logic.Faction;
import game.logic.Player;
import game.roles.Executioner;
import game.roles.Jester;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.json_output.ProfileJson;
import nnode.Game;
import nnode.StateObject;
import nnode.User;

public class RoleCardUpdate {
    public static JSONObject request(Game game, User user) throws JSONException {
        Player player = user.player;

        JSONObject request = new JSONObject();
        request.put("allies", ProfileJson.getAllies(player));
        request.put("event", "roleCardUpdate");
        request.put("joinID", game.joinID);
        request.put("profile", ProfileJson.toJson(user));
        request.put("userID", user.id);
        request.put(StateObject.isDay, player.narrator.isDay());
        request.put(StateObject.dayNumber, player.narrator.getDayNumber());

        request.put(StateObject.roleName, player.factionRole.getPlayerVisibleFactionRole().getName());
        request.put(StateObject.teamName, player.getFaction().getName());
        request.put(StateObject.color, player.getFaction().getColor());

        if(player.getFaction().hasEnemies()){
            JSONArray enemies = new JSONArray();
            Faction t;
            for(String t_color: player.getFaction().getEnemyColors()){
                t = player.narrator.getFaction(t_color);
                enemies.put(t.getName());
            }
            request.put(StateObject.enemy, enemies);
        }else if(player.hasAbility(Jester.class)){
            request.put(StateObject.enemy, "To win, you must get yourself publicly executed.");
        }else if(player.hasAbility(Executioner.class)){
            request.put(StateObject.enemy, Executioner.GetTarget(player).getName());
        }else{
            request.put(StateObject.enemy, "You must survive until the end of the game.");
        }
        return request;
    }
}
