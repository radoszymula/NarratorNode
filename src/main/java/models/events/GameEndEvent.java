package models.events;

import game.logic.Player;
import game.logic.PlayerList;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.json_output.PhaseJson;
import models.json_output.PlayerJson;
import nnode.Game;
import nnode.User;

public class GameEndEvent {

    public static JSONObject request(Game game) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "gameEnd");
        request.put("gameID", game.getID());
        request.put("joinID", game.joinID);
        request.put("phase", PhaseJson.toJson(game));
        request.put("userIDs", game.phoneBook.values());
        request.put("winners", getWinners(game.narrator._players, game));
        request.put("winnerUserIDs", getWinnerIDs(game));
        return request;
    }

    private static JSONArray getWinnerIDs(Game game) {
        JSONArray winnerIDs = new JSONArray();
        User user;
        for(Player player: game.narrator._players){
            if(!player.isWinner())
                continue;
            user = game.phoneBook.get(player);
            if(user != null)
                winnerIDs.put(user.id);
        }
        return winnerIDs;
    }

    private static JSONArray getWinners(PlayerList players, Game game) throws JSONException {
        JSONArray winners = new JSONArray();
        for(Player player: players){
            if(player.isWinner())
                winners.put(PlayerJson.toJson(player, game));
        }
        return winners;
    }
}
