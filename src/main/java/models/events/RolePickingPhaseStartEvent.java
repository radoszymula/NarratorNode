package models.events;

import json.JSONException;
import json.JSONObject;
import models.json_output.PhaseJson;
import nnode.Game;

public class RolePickingPhaseStartEvent {

    public static JSONObject request(Game game) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "rolePickingStart");
        request.put("phase", PhaseJson.toJson(game));
        request.put("joinID", game.joinID);
        return request;
    }

}
