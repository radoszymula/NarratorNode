package models.events;

import json.JSONException;
import json.JSONObject;
import models.json_output.PlayerJson;
import models.json_output.SetupJson;
import nnode.Game;
import nnode.User;

public class PlayerAddedEvent {
    public static JSONObject request(Game game, User user) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "newPlayer");
        request.put("joinID", game.joinID);
        request.put("player", PlayerJson.toJson(user.player, game));
        request.put("playerName", user.player.getName()); // deprecated (i think its just discord)
        request.put("players", PlayerJson.toJson(game.narrator._players.sortByName(), game));
        request.put("setup", SetupJson.toJson(game._setup));
        request.put("userID", user.id);

        return request;
    }
}
