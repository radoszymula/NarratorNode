package models.events;

import json.JSONException;
import json.JSONObject;
import models.json_output.UserJson;
import nnode.Game;

public class HostChangeEvent {
    public static JSONObject request(Game game) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "hostChange");
        request.put("host", UserJson.toJson(game.getHost()));
        request.put("joinID", game.joinID);

        return request;
    }
}
