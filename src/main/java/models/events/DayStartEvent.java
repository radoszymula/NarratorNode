package models.events;

import java.util.ArrayList;

import game.event.DayChat;
import game.event.EventLog;
import game.event.SnitchAnnouncement;
import game.event.VoidChat;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.CommandHandler;
import game.logic.support.Random;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.Punch;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.enums.GamePhase;
import models.json_output.PhaseJson;
import models.json_output.PlayerJson;
import models.json_output.ProfileJson;
import models.json_output.SetupJson;
import nnode.Game;
import nnode.StateObject;
import nnode.User;

public class DayStartEvent {
    public static JSONObject request(Game game, PlayerList newDead, ArrayList<SnitchAnnouncement> sAnnoucements)
            throws JSONException {
        Narrator narrator = game.narrator;

        JSONObject request = new JSONObject();
        request.put("event", "dayStart");
        request.put("joinID", game.joinID);
        request.put(StateObject.dayNumber, narrator.getDayNumber());
        request.put(StateObject.isDay, narrator.isDay());
        request.put("discussionLength", narrator.getInt(SetupModifier.DISCUSSION_LENGTH));
        request.put("dayLength", narrator.getInt(SetupModifier.DAY_LENGTH));
        request.put("firstPhase", narrator.isFirstPhase());
        request.put("phase", PhaseJson.toJson(game));
        request.put("players", PlayerJson.toJson(game.narrator._players, game));
        request.put("profiles", getProfiles(game));
        request.put("setup", SetupJson.toJson(game._setup));

        request.put("snitchReveals", addSnitchAnnouncements(game, sAnnoucements));
        if(newDead != null && !newDead.isEmpty()){
            JSONArray jDeadList = new JSONArray();
            JSONObject jDead;
            JSONArray deathType;
            for(Player dead: newDead){
                jDead = new JSONObject();
                jDead.put(StateObject.playerName, dead.getName());
                jDead.put(StateObject.userID, EventHelpers.getUserID(dead, game));
                if(!dead.getDeathType().isCleaned()){
                    deathType = new JSONArray();
                    for(String[] dType: dead.getDeathType().attacks){
                        deathType.put(dType[1]);
                    }
                    jDead.put("deathType", deathType);
                    jDead.put(StateObject.roleName, dead.getGraveyardRoleName());
                    jDead.put(StateObject.teamName, dead.getGraveyardTeamName());
                    jDead.put(StateObject.color, dead.getGraveyardColor());
                    jDead.put(StateObject.lastWill, dead.getLastWill(null));
                }
                jDeadList.put(jDead);
            }
            request.put(StateObject.graveYard, jDeadList);
        }

        // living is all the people that can talk during the day, including the not
        // blackmailed people
        JSONArray living = new JSONArray();

        for(Player player: game.narrator.getLivePlayers()){
            if(player.isSilenced() || player.isPuppeted())
                continue;
            if(EventHelpers.hasUserID(player, game))
                living.put(game.phoneBook.get(player).id);
        }
        if(game.hasModerator())
            living.put(game.hostID);
        request.put("living", living);

        addPlayerCommands(request, game);
        addNightChatInfo(request, game);

        return request;
    }

    public static JSONArray getProfiles(Game game) throws JSONException {
        JSONArray profiles = new JSONArray();
        for(User user: game.phoneBook.values()){
            profiles.put(ProfileJson.toJson(user));
        }
        return profiles;
    }

    private static JSONArray addSnitchAnnouncements(Game game, ArrayList<SnitchAnnouncement> sAnnoucements)
            throws JSONException {
        JSONArray snitchAnnouncements = new JSONArray();
        JSONObject jo;
        for(SnitchAnnouncement sn: sAnnoucements){
            jo = new JSONObject();
            jo.put(StateObject.userID, EventHelpers.getUserID(sn.revealed, game));
            jo.put(StateObject.teamName, sn.teamName);
            jo.put(StateObject.roleName, sn.roleName);
            snitchAnnouncements.put(jo);
        }
        return snitchAnnouncements;
    }

    static void addNightChatInfo(JSONObject obj, Game game) throws JSONException {
        Narrator narrator = game.narrator;
        JSONArray jo = new JSONArray();

        JSONObject chatO;
        JSONObject members;
        for(EventLog el: narrator.getEventManager().getAllChats()){
            if(el instanceof DayChat)
                continue;
            if(el instanceof VoidChat)
                continue;

            chatO = new JSONObject();
            chatO.put("name", el.getName());
            chatO.put("type", el.getClass().getSimpleName());
            chatO.put("active", el.isActive());

            members = new JSONObject();
            for(Player p: el.getMembers()){
                if(!EventHelpers.hasUserID(p, game))
                    continue;
                members.put(Long.toString(EventHelpers.getUserID(p, game)), el.getKey(p));
            }
            chatO.put("members", members);

            jo.put(chatO);
        }

        obj.put("nightChatInfo", jo);
        obj.put("replayID", game.getID());
    }

    static void addPlayerCommands(JSONObject obj, Game game) throws JSONException {
        Narrator narrator = game.narrator;
        Random r = new Random();
        JSONObject playerCommands = new JSONObject();
        JSONObject commands;
        Long userID;
        ArrayList<Ability> abilities;
        String exampleName;
        boolean lastWillEnabled = narrator.getBool(SetupModifier.LAST_WILL);
        for(Player p: narrator.getLivePlayers()){ // don't need to worry about ghost case here
            if(!EventHelpers.hasUserID(p, game))
                continue;
            userID = EventHelpers.getUserID(p, game);
            commands = new JSONObject();
            if(narrator.isDay()){
                exampleName = narrator.getLivePlayers().getRandom(r).getName();
                if(p.isAlive() && game.narrator.phase == GamePhase.VOTE_PHASE){
                    addCommandObj(commands, CommandHandler.VOTE, "vote for a person",
                            CommandHandler.VOTE + " " + exampleName, true);
                    if(narrator.getBool(SetupModifier.SKIP_VOTE))
                        addCommandObj(commands, CommandHandler.SKIP_VOTE,
                                "opt to forgo public executuions, and going to the night phase",
                                CommandHandler.SKIP_VOTE, true);
                    addCommandObj(commands, CommandHandler.UNVOTE, "cancel your previous vote", CommandHandler.UNVOTE,
                            true);
                }
                abilities = p.getDayAbilities();
            }else{
                abilities = p.getAvailableActions();
                addCommandObj(commands, CommandHandler.END_NIGHT, "skip night, make morning arrive sooner",
                        CommandHandler.END_NIGHT, true);
            }
            if(lastWillEnabled)
                addCommandObj(commands, "lw", "set your last will", "lw *Looks like I'm dead*", true);

            String usage;
            for(Ability command: abilities){
                try{
                    usage = command.getUsage(p, r);
                }catch(Throwable e){
                    usage = "Usage broken for this command.  Please report it!";
                }
                addCommandObj(commands, command.getCommand(), command.getAbilityDescription(narrator), usage,
                        command.is(Punch.MAIN_ABILITY));
            }
            playerCommands.put(Long.toString(userID), commands);
        }
        if(game.hasModerator()){
            commands = new JSONObject();
            exampleName = narrator.getLivePlayers().getRandom(r).getName();
            addCommandObj(commands, CommandHandler.MODKILL, "prematurely stop a player from playing",
                    CommandHandler.MODKILL + " " + exampleName, true);
            addCommandObj(commands, CommandHandler.END_PHASED_SPACED, "ends the current phase, and starts the next one",
                    CommandHandler.END_PHASED_SPACED, true);
            playerCommands.put(Long.toString(game.hostID), commands);
        }
        obj.put("playerCommands", playerCommands);
    }

    private static void addCommandObj(JSONObject holder, String command, String description, String usage,
            boolean publicCommand) throws JSONException {
        JSONObject c = new JSONObject();
        c.put(StateObject.command, command);
        c.put(StateObject.description, description);
        c.put("usage", usage);
        if(publicCommand)
            c.put("publicCommand", 1);
        holder.put(command.toLowerCase(), c);
    }
}
