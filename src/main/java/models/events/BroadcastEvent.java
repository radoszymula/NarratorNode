package models.events;

import game.event.Message;
import json.JSONException;
import json.JSONObject;
import nnode.Game;

public class BroadcastEvent {
    public static JSONObject request(Game game, String s, Message e) throws JSONException {
        if(e != null)
            s = e.access(Message.PUBLIC);// , DiscordIntegration.decoder);

        JSONObject request = new JSONObject();
        request.put("event", "broadcast");
        request.put("joinID", game.joinID);
        request.put("pmContents", s);

        return request;
    }
}
