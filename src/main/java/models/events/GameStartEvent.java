package models.events;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.json_output.GameJson;
import models.json_output.PlayerJson;
import models.json_output.SetupJson;
import nnode.Game;
import nnode.User;

public class GameStartEvent {
    public static JSONObject request(Game game) throws JSONException {
        JSONObject request = new JSONObject();

        JSONArray userIDs = new JSONArray();
        for(User user: game.phoneBook.values()){
            userIDs.put(user.id);
        }

        request.put("event", "gameStart");
        request.put("isDay", game.narrator.isDay());
        request.put("joinID", game.joinID);
        request.put("living", userIDs); // this NEEDS to be playerIDs or something
        request.put("moderators", GameJson.getModerators(game));
        request.put("players", PlayerJson.toJson(game.narrator._players.sortByName(), game));
        request.put("setup", SetupJson.toJson(game._setup));

        return request;
    }
}
