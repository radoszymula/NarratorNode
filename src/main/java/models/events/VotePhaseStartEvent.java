package models.events;

import game.logic.support.rules.SetupModifier;
import json.JSONException;
import json.JSONObject;
import models.json_output.PhaseJson;
import nnode.Game;

public class VotePhaseStartEvent {
    public static JSONObject request(Game game) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "votePhaseStart");
        request.put("joinID", game.joinID);
        request.put("dayLength", game.narrator.getInt(SetupModifier.DAY_LENGTH));
        request.put("isFirstPhase", game.narrator.isFirstPhase());
        request.put("phase", PhaseJson.toJson(game));
        request.put("profiles", DayStartEvent.getProfiles(game));

        DayStartEvent.addPlayerCommands(request, game);

        return request;
    }
}
