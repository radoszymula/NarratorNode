package models.events;

import game.logic.Player;
import game.logic.support.rules.SetupModifier;
import json.JSONException;
import json.JSONObject;
import models.json_output.PhaseJson;
import models.json_output.PlayerJson;
import nnode.Game;

public class TrialPhaseStartEvent {

    public static JSONObject request(Game game, Player trialedPerson) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("joinID", game.joinID);
        request.put("event", "trialStart");
        request.put("trialLength", game.narrator.getInt(SetupModifier.TRIAL_LENGTH));
        request.put("trialedUser", PlayerJson.toJson(trialedPerson, game));
        request.put("trialedUsers", PlayerJson.toJson(game.narrator.voteSystem.getPlayersOnTrial(), game));
        request.put("phase", PhaseJson.toJson(game));
        DayStartEvent.addPlayerCommands(request, game);
        return request;
    }

}
