package models.events;

import game.event.Message;
import game.event.VoteAnnouncement;
import json.JSONException;
import json.JSONObject;
import models.json_output.GameJson;
import models.json_output.UserJson;
import models.json_output.VoteSettingsJson;
import nnode.Game;

public class NewVoteEvent {

    public static JSONObject request(Game game, VoteAnnouncement event) throws JSONException {
        JSONObject voteJson = new JSONObject();
        voteJson.put("event", "newVote");
        voteJson.put("joinID", game.joinID);
        if(game.phoneBook.containsKey(event.voter))
            voteJson.put("voter", UserJson.toJson(game.phoneBook.get(event.voter)));
        voteJson.put("voteTarget", event.voteTarget);
        voteJson.put("voteText", event.access(Message.PUBLIC));
        voteJson.put("settings", VoteSettingsJson.toJson(game));
        voteJson.put("moderators", GameJson.getModerators(game));
        return voteJson;
    }

}
