package models.events;

import game.logic.PlayerList;
import game.logic.support.rules.SetupModifier;
import json.JSONException;
import json.JSONObject;
import models.json_output.DeadPlayerJson;
import models.json_output.PhaseJson;
import models.json_output.PlayerJson;
import models.json_output.SetupJson;
import nnode.Game;
import nnode.StateObject;

public class NightStartEvent {
    public static JSONObject request(Game game, PlayerList votedOutPlayers, PlayerList poisonedPlayers)
            throws JSONException {
        PlayerList deadPlayers = new PlayerList();
        if(votedOutPlayers != null)
            deadPlayers.add(votedOutPlayers);
        if(poisonedPlayers != null)
            deadPlayers.add(poisonedPlayers);

        JSONObject request = new JSONObject();

        request.put("nightStart", 1);
        request.put("event", "nightStart");
        request.put("joinID", game.joinID);
        request.put("broadcast", "It is now nighttime.  Day will resume in ");
        request.put("length", game.narrator.getInt(SetupModifier.NIGHT_LENGTH));
        request.put("players", PlayerJson.toJson(game.narrator._players, game));
        request.put("phase", PhaseJson.toJson(game));
        request.put("profiles", DayStartEvent.getProfiles(game));
        request.put("setup", SetupJson.toJson(game._setup));
        request.put(StateObject.graveYard, DeadPlayerJson.toJson(deadPlayers, game));
        if(game.hasModerator())
            request.put("moderatorID", game.hostID);

        DayStartEvent.addPlayerCommands(request, game);
        DayStartEvent.addNightChatInfo(request, game);

        return request;
    }
}
