package models.events;

import game.logic.Player;
import nnode.Game;

public class EventHelpers {
    public static Long getUserID(Player p, Game g) {
        if(g.phoneBook.containsKey(p))
            return g.phoneBook.get(p).id;
        return null;
    }

    public static boolean hasUserID(Player p, Game g) {
        return g.phoneBook.containsKey(p);
    }
}
