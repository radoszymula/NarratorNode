package models.events;

import json.JSONException;
import json.JSONObject;
import models.json_output.SetupJson;
import models.json_output.UserJson;
import nnode.Game;
import nnode.User;

public class PlayerRemovedEvent {
    public static JSONObject request(Game game, User user, String playerName) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "playerRemove");
        request.put("host", UserJson.toJson(game.getHost()));
        request.put("joinID", game.joinID);
        request.put("playerName", playerName);
        request.put("setup", SetupJson.toJson(game._setup));
        request.put("userID", user.id);
        return request;
    }
}
