package models.events;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import nnode.Game;
import nnode.User;

public class CloseLobbyEvent {

    public static JSONObject request(Game game) throws JSONException {
        JSONArray usersJson = new JSONArray();
        for(User user: game.observers)
            usersJson.put(getUserJson(user));
        for(User user: game.phoneBook.values())
            usersJson.put(getUserJson(user));

        JSONObject request = new JSONObject();
        request.put("event", "closeLobby");
        request.put("joinID", game.joinID);
        request.put("users", usersJson);
        request.put("replayID", game.getID());
        return request;
    }

    private static JSONObject getUserJson(User user) throws JSONException {
        JSONObject userJson = new JSONObject();
        userJson.put("id", user.id);
        return userJson;
    }

}
