package models.events;

import game.event.SelectionMessage;
import json.JSONException;
import json.JSONObject;
import nnode.Game;
import nnode.User;

public class ActionSubmitEvent {

    public static JSONObject request(Game game, User user, SelectionMessage selectionMessage) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "actionSubmit");
        request.put("joinID", game.joinID);
        request.put("userID", user.id);
        request.put("feedbackText", selectionMessage.access(user.player));
        return request;
    }

}
