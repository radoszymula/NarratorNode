package models.events;

import game.event.DeathAnnouncement;
import game.event.Message;
import game.logic.Player;
import game.logic.PlayerList;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import nnode.Game;

public class DayDeathEvent {

    public static JSONObject request(Game game, PlayerList dead, String announcement) throws JSONException {
        JSONArray userIDs = new JSONArray();
        for(Player p: dead){
            if(EventHelpers.hasUserID(p, game))
                userIDs.put(EventHelpers.getUserID(p, game));
        }

        JSONObject jo = new JSONObject();
        jo.put("joinID", game.joinID);
        jo.put("contents", announcement);
        jo.put("userIDs", userIDs);
        jo.put("event", "dayDeath");
        return jo;
    }

    public static JSONObject request(Game game, DeathAnnouncement announcement) throws JSONException {
        return request(game, announcement.dead, announcement.access(Message.PUBLIC));
    }
}
