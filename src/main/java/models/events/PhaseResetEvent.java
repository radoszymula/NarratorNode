package models.events;

import json.JSONException;
import json.JSONObject;
import models.json_output.PhaseJson;
import nnode.Game;

public class PhaseResetEvent {

    public static JSONObject request(Game game) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "phaseReset");
        request.put("joinID", game.joinID);
        request.put("phase", PhaseJson.toJson(game));
        return request;
    }

}
