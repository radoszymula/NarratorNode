package models;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import game.logic.support.rules.Modifier;

public class Modifiers<T extends Modifier> {

    private Map<T, Object> modifierMap = new HashMap<>();

    public Modifiers() {
    	
    }

    public void add(T modifier, boolean value) {
        modifierMap.put(modifier, value);
    }

    public void add(T modifier, int value) {
        modifierMap.put(modifier, value);
    }

    public Object get(T modifier) {
        return modifierMap.get(modifier);
    }

    public boolean getBoolean(T modifier) {
        return (boolean) modifierMap.get(modifier);
    }

    public int getInt(T modifier) {
        return (int) modifierMap.get(modifier);
    }

	public Set<T> getModifierIDs() {
		return modifierMap.keySet();
	}

    public int getOrDefault(T modifier, int defaultValue) {
        if(modifierMap.containsKey(modifier))
            return (int) modifierMap.get(modifier);
        return defaultValue;
    }

    public boolean getOrDefault(T modifier, boolean defaultValue) {
        if(modifierMap.containsKey(modifier))
            return (boolean) modifierMap.get(modifier);
        return defaultValue;
    }
    
    public Object getOrDefault(T modifier, Object defaultValue) {
    	if(modifierMap.containsKey(modifier))
    		return modifierMap.get(modifier);
    	return defaultValue;
    }

    public boolean hasKey(T modifier) {
        return modifierMap.containsKey(modifier);
    }

	public void remove(T modifier) {
		modifierMap.remove(modifier);
	}
    
    @Override
    public boolean equals(Object o) {
    	if(o == null || !o.getClass().equals(getClass()))
    		return false;
    	if(o == this)
    		return true;
    	Modifiers<?> other = (Modifiers<?>) o;
    	return other.modifierMap.equals(this.modifierMap);
    }

    public static <T extends Modifier> Modifiers<T> copy(Modifiers<T> modifiers) {
    	if(modifiers == null)
    		return null;
        Modifiers<T> newModifiers = new Modifiers<T>();
        newModifiers.modifierMap = new HashMap<>(modifiers.modifierMap);
        return newModifiers;
    }
}
