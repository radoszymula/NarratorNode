package util;

import game.logic.Role;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.roles.Ability;
import models.FactionRole;
import models.Modifiers;

public class ModifierUtil {

    public static void copyModifierMap(Role toRole, Role fromRole) {
        toRole.modifiers = Modifiers.copy(fromRole.modifiers);
    }

    public static void resolveOverrides(FactionRole factionRole) {
    	Role role = factionRole.role;
    	resolveRoleModifierOverrides(factionRole);
    	
    	Modifiers<AbilityModifier> abilityModifiers;
    	for(Class<? extends Ability> abilityClass: factionRole.abilityModifiers.keySet()) {
    		abilityModifiers = factionRole.abilityModifiers.get(abilityClass);
    		resolveRoleModifierOverrides(role, abilityModifiers, abilityClass);
    	}
    }

	private static void resolveRoleModifierOverrides(Role role, Modifiers<AbilityModifier> abilityModifiers,
			Class<? extends Ability> abilityClass) {
		for(AbilityModifier modifier: abilityModifiers.getModifierIDs()) {
			if(Ability.IsIntModifier(modifier))
				role._abilities.getAbility(abilityClass).modifiers.add(modifier, abilityModifiers.getInt(modifier));
			else
				role._abilities.getAbility(abilityClass).modifiers.add(modifier, abilityModifiers.getBoolean(modifier));
		}
	}

	private static void resolveRoleModifierOverrides(FactionRole factionRole) {
		Role role = factionRole.role;
    	for(RoleModifier modifier: factionRole.roleModifiers.getModifierIDs()) {
    		if(Ability.IsIntModifier(modifier))
    			role.modifiers.add(modifier, factionRole.roleModifiers.getInt(modifier));
    		else
    			role.modifiers.add(modifier, factionRole.roleModifiers.getBoolean(modifier));
    	}
	}
}
