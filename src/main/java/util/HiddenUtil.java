package util;

import game.logic.Narrator;
import game.roles.Hidden;
import models.FactionRole;
import services.HiddenService;

public class HiddenUtil {
    public static Hidden createHiddenSingle(Narrator narrator, FactionRole role) {
        Hidden singleHidden = getHiddenSingle(narrator, role);
        if(singleHidden != null)
            return singleHidden;

        return HiddenService.createHidden(narrator, role.role.getName(), role);
    }

    public static Hidden getHiddenSingle(Narrator narrator, FactionRole factionRole) {
        for(Hidden rm: narrator.hiddens){
            if(rm.getSize() != 1)
                continue;
            if(rm.getFactionRoles().get(0) == factionRole)
                return rm;
        }
        return null;
    }
}
