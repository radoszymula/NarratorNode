package util;

import java.util.ArrayList;
import java.util.List;

import models.FactionRole;

public class FactionRoleUtil {
    public static List<FactionRole> getList(FactionRole... memberArray) {
        ArrayList<FactionRole> memberList = new ArrayList<>();
        for(FactionRole m: memberArray)
            memberList.add(m);

        return memberList;
    }
}
