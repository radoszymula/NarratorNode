package util;

import java.util.ArrayList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Role;
import game.roles.Ability;
import game.roles.FactionHidden;
import game.roles.Hidden;
import models.FactionRole;
import models.SetupHidden;

public class LookupUtil {
	
	public static Ability findAbility(Role role, long abilityID) {
		for(Ability ability: role._abilities)
			if(ability.id == abilityID)
				return ability;
		return null;
	}

    public static Hidden findHidden(Narrator narrator, long hiddenID) {
        for(Hidden hidden: narrator.hiddens){
            if(hidden.getID() == hiddenID)
                return hidden;
        }
        return null;
    }

    public static Role findRole(Narrator narrator, String name) {
        for(Role role: narrator.roles)
            if(role.getName().equals(name))
                return role;
        return null;
    }

    public static Role findRole(Narrator narrator, long roleID) {
        for(Role role: narrator.roles)
            if(role.getID() == roleID)
                return role;
        return null;
    }

    public static Hidden findHidden(Narrator n, String name) {
        for(Hidden hidden: n.hiddens){
            if(hidden.getName().equals(name))
                return hidden;
        }
        return null;
    }

    public static Hidden findHidden(Narrator narrator, String name, String color) {
        for(Hidden hidden: narrator.hiddens){
            if(!hidden.getColors().contains(color))
                continue;
            if(!hidden.getName().equals(name))
                continue;
            return hidden;
        }
        return null;
    }

    public static Hidden GetFactionHidden(Narrator n, String color) {
        for(Hidden hidden: n.hiddens){
            if(hidden instanceof FactionHidden && hidden.getColors().contains(color)){
                return hidden;
            }
        }
        return null;
    }

    public static List<Role> findRolesWithAbility(Narrator narrator, Class<? extends Ability> class1) {
        List<Role> rolesWithAbility = new ArrayList<>();
        for(Role role: narrator.roles)
            if(role.hasAbility(class1))
                rolesWithAbility.add(role);
        return rolesWithAbility;
    }

    public static FactionRole findFactionRole(Narrator narrator, String name, String color) {
        for(FactionRole factionRole: narrator.getFaction(color)._roleSet.values()){
            if(factionRole.role.getName().equals(name))
                return factionRole;
        }
        return null;
    }

    public static SetupHidden findSetupHidden(Narrator narrator, long setupHiddenID) {
        for(SetupHidden setupHidden: narrator.rolesList){
            if(setupHidden.id == setupHiddenID)
                return setupHidden;
        }
        return null;
    }

    public static SetupHidden findSetupHidden(Narrator narrator, String hiddenName) {
        for(SetupHidden setupHidden: narrator.rolesList){
            if(setupHidden.hidden.getName().equals(hiddenName))
                return setupHidden;
        }
        return null;
    }

    public static FactionRole findFactionRole(Narrator narrator, long factionRoleID) {
        for(Faction faction: narrator.getFactions()){
            for(FactionRole factionRole: faction._roleSet.values()){
                if(factionRole.id == factionRoleID)
                    return factionRole;
            }
        }
        return null;
    }

}
