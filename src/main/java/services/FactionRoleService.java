package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.support.Util;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.roles.Ability;
import game.roles.Hidden;
import models.FactionRole;
import models.Modifiers;
import models.schemas.AbilityModifierSchema;
import models.schemas.FactionRoleSchema;
import models.schemas.RoleModifierSchema;
import models.serviceResponse.ModifierServiceResponse;
import nnode.User;
import repositories.Connection;
import repositories.FactionRoleModifierRepo;
import repositories.FactionRoleRepo;
import util.LookupUtil;

public class FactionRoleService {

    public static FactionRole getOrCreateDeprecated(Role role, String color) {
		Faction faction = role.narrator.getFaction(color);
		if(faction._roleSet.containsKey(role))
			return faction._roleSet.get(role);
		return new FactionRole(faction, role);
	}

	public static void saveFactionRoles(Connection connection, long setupID, ArrayList<Faction> factions) {
		if(factions.isEmpty())
			return;
		Narrator narrator = factions.get(0).narrator;
		ArrayList<Long> factionRoleIDs = new ArrayList<>();
		
		try {
			// insert faction roles
			ArrayList<FactionRole> factionRoles = new ArrayList<>();
			for(Faction faction: factions)
				factionRoles.addAll(faction._roleSet.values());
			factionRoleIDs.addAll(FactionRoleRepo.create(connection, setupID, factionRoles));
			for(int i = 0; i < factionRoles.size(); i++) {
				FactionRole factionRole = factionRoles.get(i);
				for(Hidden hidden: narrator.hiddens) {
					if(hidden.factionRolesMap.containsKey(factionRole.id)) {
						hidden.factionRolesMap.remove(factionRole.id);
						hidden.factionRolesMap.put(factionRoleIDs.get(i), factionRole);
					}
				}
				factionRole.id = factionRoleIDs.get(i);
			}
			
			// save roleReceived
			for(FactionRole factionRole: factionRoles)
				if(factionRole.receivedRole != null)
					FactionRoleRepo.updateRoleReceived(connection, factionRole);
			
			// insert faction role role modifiers
			FactionRoleModifierRepo.createRoleModifiers(connection, factionRoles);
			
			// insert faction role ability modifiers
			ArrayList<Long> roleAbilityIDs = new ArrayList<>();
			ArrayList<Modifiers<AbilityModifier>> modifiersList = new ArrayList<>();
			ArrayList<Long> factionRoleIDsForModifier = new ArrayList<>();
			for(FactionRole factionRole: factionRoles)
				for(Class<? extends Ability> abilityClass: factionRole.abilityModifiers.keySet()) {
					roleAbilityIDs.add(factionRole.role.getAbility(abilityClass).id);
					factionRoleIDsForModifier.add(factionRole.id);
					modifiersList.add(factionRole.abilityModifiers.get(abilityClass));
				}
			FactionRoleModifierRepo.create(connection, factionRoleIDsForModifier, roleAbilityIDs, modifiersList);
		}catch(SQLException|IndexOutOfBoundsException e) {
			try {
				FactionRoleRepo.deleteByIDs(connection, setupID, factionRoleIDs);
			}catch(SQLException f) {
				Util.log(f, "Failed to clean up faction roles after bad inserts.");
			}
			throw new NarratorException("Could not save faction roles due to database error.");
		}
	}
	
	public static FactionRole get(long userId, long factionRoleID) {
		User user = UserService.assertInGame(userId);
		FactionRole factionRole = LookupUtil.findFactionRole(user.game.narrator, factionRoleID);
		if(factionRole == null)
			throw new NarratorException("No faction role found with that id.");
		return factionRole;
	}

	public static Set<FactionRoleSchema> getBySetupID(Connection connection, long setupID){
    	try {
    		Map<Long, FactionRoleSchema> factionRoles = FactionRoleRepo.getBySetupID(connection, setupID);
    		
    		Map<Long, Set<RoleModifierSchema>> factionRoleIDToModifiers = FactionRoleModifierRepo.getRoleModifiers(connection, setupID);
    		for(long factionRoleID: factionRoleIDToModifiers.keySet())
    			factionRoles.get(factionRoleID).roleModifiers = factionRoleIDToModifiers.get(factionRoleID);
    		
    		Map<Long, Map<Long, Set<AbilityModifierSchema>>> abilityModifiers = FactionRoleModifierRepo.getAbilityModifiers(connection, setupID);
    		for(long factionRoleID: abilityModifiers.keySet())
    			factionRoles.get(factionRoleID).abilityModifiers = abilityModifiers.get(factionRoleID);
    		
    		return new HashSet<>(factionRoles.values());
    	}catch(SQLException e) {
    		throw new NarratorException("Unable to get setup's faction roles due to database error.");
    	}
    }

    // assumes that factionRole and receivedRole are properly added into the setup
    public static void setRoleReceive(FactionRole factionRole, FactionRole receivedRole) {
        factionRole.receivedRole = receivedRole;
    }
    
    public static ModifierServiceResponse updateModifier(long userID, long factionRoleID, RoleModifier modifier, Object value) {
    	User user = UserService.assertIsHost(userID);
    	SetupService.assertNotAutoFilling(user);
    	FactionRole factionRole = get(userID, factionRoleID);
    	Object prevValue;
    	if(Ability.IsIntModifier(modifier) && value instanceof Integer) {
    		prevValue = factionRole.roleModifiers.getOrDefault(modifier, null);
    		addModifier(factionRole, modifier, (int) value);
    	}else if(Ability.IsBoolModifier(modifier) && value instanceof Boolean){
    		prevValue = factionRole.roleModifiers.getOrDefault(modifier, null);
    		addModifier(factionRole, modifier, (boolean) value);
    	}else
    		throw new NarratorException("Unknown role modifier type or value.");
    	
    	try {
    		Connection connection = user.game.connection;
    		FactionRoleModifierRepo.update(connection, factionRoleID, modifier, value);
    	}catch(SQLException e) {
    		if(prevValue == null)
    			factionRole.roleModifiers.remove(modifier);
    		else if(value instanceof Boolean)
    			addModifier(factionRole, modifier, (boolean) prevValue);
    		else if(value instanceof Integer)
    			addModifier(factionRole, modifier, (int) prevValue);
    		Util.log(e);
    		throw new NarratorException("Failed to update due to database error.");
    	}
    	return new ModifierServiceResponse(user.game._setup.id, value);
    }
    
    public static ModifierServiceResponse updateModifier(long userID, long factionRoleID, long abilityID, AbilityModifier modifier, Object value) {
    	User user = UserService.assertIsHost(userID);
    	SetupService.assertNotAutoFilling(user);
    	FactionRole factionRole = get(userID, factionRoleID);
    	Class<? extends Ability> abilityClass = RoleAbilityModifierService.findAbility(factionRole.role, abilityID).getClass();
    	
    	Object prevValue;
    	if(Ability.IsIntModifier(modifier) && value instanceof Integer) {
    		if(!factionRole.abilityModifiers.containsKey(abilityClass))
    			prevValue = null;
    		else
    			prevValue = factionRole.abilityModifiers.getOrDefault(modifier, null);
    		addModifier(factionRole, modifier, abilityClass, (int) value);
    	}else if(Ability.IsBoolModifier(modifier) && value instanceof Boolean){
    		if(!factionRole.abilityModifiers.containsKey(abilityClass))
    			prevValue = null;
    		else
    			prevValue = factionRole.abilityModifiers.getOrDefault(modifier, null);
    		addModifier(factionRole, modifier, abilityClass, (boolean) value);
    	}else
    		throw new NarratorException("Unknown role modifier type or value.");
    	
    	try {
    		Connection connection = user.game.connection;
    		FactionRoleModifierRepo.update(connection, factionRoleID, abilityID, modifier, value);
    	}catch(SQLException e) {
    		if(prevValue == null)
    			factionRole.abilityModifiers.get(abilityClass).remove(modifier);
    		else if(value instanceof Boolean)
    			addModifier(factionRole, modifier, abilityClass, (boolean) prevValue);
    		else if(value instanceof Integer)
    			addModifier(factionRole, modifier, abilityClass, (int) prevValue);
    		Util.log(e);
    		throw new NarratorException("Failed to update due to database error.");
    	}
    	return new ModifierServiceResponse(user.game._setup.id, value);
    }

    public static void addModifier(FactionRole factionRole, AbilityModifier modifier,
            Class<? extends Ability> abilityClass, boolean value) {
    	Ability ability = factionRole.role._abilities.getAbility(abilityClass);
    	RoleAbilityModifierService.validateAbilityModifierBool(modifier, ability);
    	if(!factionRole.abilityModifiers.containsKey(abilityClass))
    		factionRole.abilityModifiers.put(abilityClass, new Modifiers<>());
        factionRole.abilityModifiers.get(abilityClass).add(modifier, value);
    }

    public static void addModifier(FactionRole factionRole, AbilityModifier modifier,
            Class<? extends Ability> abilityClass, int value) {
    	Ability ability = factionRole.role._abilities.getAbility(abilityClass);
    	RoleAbilityModifierService.validateAbilityModifierInt(modifier, ability, value);
    	if(!factionRole.abilityModifiers.containsKey(abilityClass))
    		factionRole.abilityModifiers.put(abilityClass, new Modifiers<>());
        factionRole.abilityModifiers.get(abilityClass).add(modifier, value);
    }

	public static void addModifier(FactionRole factionRole, RoleModifier modifier, boolean value) {
		factionRole.roleModifiers.add(modifier, value);
	}

	public static void addModifier(FactionRole factionRole, RoleModifier modifier, int value) {
		factionRole.roleModifiers.add(modifier, value);
	}

	public static void deleteRoleReceived(long userID, long factionRoleID) {
		User user = UserService.assertIsHost(userID);
		SetupService.assertNotAutoFilling(user);
		FactionRole factionRole = LookupUtil.findFactionRole(user.game.narrator, factionRoleID);
		if(factionRole == null)
			throw new NarratorException("Could not find faction role with that id.");
		
		try {
			FactionRoleRepo.deleteSetRoleReceive(user.game.connection, factionRoleID, user.game._setup.id);
		}catch(SQLException e) {
			throw new NarratorException("Could not removed role received because of database error.");
		}
		deleteRoleReceived(factionRole);
	}
	
	public static void deleteRoleReceived(FactionRole factionRole) {
		factionRole.receivedRole = null;
	}
}
