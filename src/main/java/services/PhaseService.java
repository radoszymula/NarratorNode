package services;

import game.logic.exceptions.NarratorException;
import game.logic.support.Util;
import nnode.Game;
import nnode.NodeSwitch;

public class PhaseService {

    public static void setPhaseTimeout(Game game) {
        game.phaseHash = Util.getRandomString(12);
    }

    public static void endPhase(String joinID, String phaseHash) {
        Game game = NodeSwitch.idToInstance.get(joinID);
        if(game == null)
            throw new NarratorException("Game not found");
        if(phaseHash.length() == 0 || game.phaseHash.equals(phaseHash))
            game.narrator.endPhase();
        game.endGameIfNoParticipatingPlayersAlive();
    }

}
