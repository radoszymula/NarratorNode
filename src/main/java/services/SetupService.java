package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.exceptions.NarratorException;
import game.logic.support.rules.Rule;
import game.setups.Default;
import game.setups.Setup;
import models.SetupHidden;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.User;
import repositories.Connection;
import repositories.FactionEnemyRepo;
import repositories.ReplayRepo;
import repositories.SetupHiddenRepo;
import repositories.SetupModifierRepo;
import repositories.SetupRepo;
import repositories.SheriffCheckableRepo;

public class SetupService {

    public static Setup getSetup(long setupID) {
        Connection connection = null;
        try{
            connection = new Connection();
            return SetupRepo.getSetup(connection, setupID, new Narrator());
        }catch(SQLException e){
            if(connection != null)
                connection.close();
            throw new NarratorException("Database error trying to fetch setup");
        }
    }

    public static List<Setup> getSetups(Long userID) {
        List<Setup> list = new LinkedList<>();
        Setup setup;
        Narrator narrator = new Narrator();
        for(String setupKey: Setup.SETUP_LIST){
            setup = Setup.GetSetup(narrator.removeAllFactionsAndRoles(), setupKey);
            list.add(setup);
        }
        if(userID == null)
            return list;

        Connection connection = null;
        try{
            connection = new Connection();
            list.addAll(SetupRepo.getActiveByOwnerID(connection, userID));
            connection.close();
        }catch(SQLException e){
            if(connection != null)
                connection.close();
            throw new NarratorException("Database error");
        }

        return list;
    }

    public static void create(Connection connection, Setup setup) {
        try{
            SetupRepo.create(connection, setup);
        }catch(SQLException e){
            throw new NarratorException("Database error creating setup.");
        }
        Narrator narrator = setup.narrator;
        long setupID = setup.id;
        long factionID, factionEnemyID, factionDetectableID;

        try{
            RoleService.insertRoles(connection, setup.id, new ArrayList<>(narrator.roles));

            FactionService.saveFactions(connection, setupID, narrator.getFactions());
            HashMap<String, Long> factionColorToID = new HashMap<>();
            for(Faction faction: narrator.getFactions())
                factionColorToID.put(faction.getColor(), faction.id);

            for(Faction faction: narrator.getFactions()){
                factionID = factionColorToID.get(faction.getColor());
                for(String enemyColor: faction.getEnemyColors()){
                    factionEnemyID = factionColorToID.get(enemyColor);
                    FactionEnemyRepo.create(connection, factionID, factionEnemyID);
                }

                for(String detectableColor: faction.getSheriffDetectables()){
                    factionDetectableID = factionColorToID.get(detectableColor);
                    SheriffCheckableRepo.create(connection, factionID, factionDetectableID);
                }
            }

            FactionRoleService.saveFactionRoles(connection, setupID, narrator.getFactions());

            HiddenService.insertHiddens(connection, setupID, new ArrayList<>(narrator.hiddens));

            for(SetupHidden setupHidden: narrator.rolesList)
                SetupHiddenRepo.create(connection, setupHidden.hidden.getID(), setupID);

            for(Rule r: narrator._rules.rules.values()){
                if(r.isInt())
                    SetupModifierRepo.create(connection, setupID, r.id.toString(), r.getIntVal());
                else
                    SetupModifierRepo.create(connection, setupID, r.id.toString(), r.getBoolVal());
            }
        }catch(SQLException e){
            try{
                SetupRepo.deleteByID(connection, setup.id);
            }catch(SQLException f){
            }
            e.printStackTrace();
            throw new NarratorException("Database error");
        }
    }

    public static Game setSetup(long userID, long setupID) {
        User user = NodeSwitch.phoneBook.get(userID);
        checkUser(user);
        return setSetup(user.game, setupID);
    }

    public static Game setSetup(String joinID, long setupID) {
        Game game = NodeSwitch.idToInstance.get(joinID);
        checkGame(game);
        return setSetup(game, setupID);
    }

    public static Game setSetup(long userID, String setupKey) {
        User user = NodeSwitch.phoneBook.get(userID);
        checkUser(user);
        return setSetup(user.game, setupKey);
    }

    public static Game setSetup(String joinID, String setupKey) {
        Game game = NodeSwitch.idToInstance.get(joinID);
        checkGame(game);
        return setSetup(game, setupKey);
    }

    public static Game setSetup(Game game, long setupID) {
        boolean isAutoFilling = game.isAutoFillingRoles();
        Setup oldSetup = game._setup;
        try{
            game._setup = SetupRepo.getSetup(game.connection, setupID, game.narrator.removeAllFactionsAndRoles());
        }catch(SQLException e){
            throw new NarratorException("Unable to fetch setup with id: " + setupID);
        }
        try{
            ReplayRepo.setSetupID(game.connection, game.getID(), setupID);
        }catch(SQLException e1){
            game._setup = oldSetup;
            throw new NarratorException("Unable to update setup due to database error");
        }
        if(isAutoFilling)
            try{
                SetupRepo.deleteByID(game.connection, oldSetup.id);
            }catch(SQLException e){
                throw new NarratorException("Setup failed due to db error");
            }
        return game;
    }

    public static Game setSetup(Game game, String setupKey) {
        setupKey = setupKey.toLowerCase().replaceAll(" ", "");
        if(isPreset(game._setup) && game._setup.key.equalsIgnoreCase(setupKey))
            return game;

        game.narrator.clearListeners();
        Setup previousSetup = game._setup;
        Setup newSetup = Setup.GetSetup(game.narrator, setupKey);

        Narrator narrator = game.narrator;
        narrator.addListener(game);

        if(newSetup == null)
            throw new NarratorException("Couldn't find the setup you were looking for");

        game._setup = newSetup;
        if(previousSetup.isPreset())
            newSetup.id = previousSetup.id;
        else
            try{
                newSetup.id = SetupRepo.create(game.connection, game._setup).id;
            }catch(SQLException e){
                GameService.delete(game);
                throw new NarratorException("Failed to change setup");
            }
        if(isPreset(newSetup))
            game.autoFillRoles();
        else
            refreshSetup(game, previousSetup, newSetup);

        game.pushToSlackUsers("Setup has been changed to " + newSetup.getName() + ".");

        return game;
    }

    public static void refreshSetup(Game game, Setup previousSetup, Setup currentSetup) {
        try{
            if(!isPreset(currentSetup)){
                long setupID = getOrCreateSetupByUserID(game.connection, game);
                currentSetup.id = setupID;
                SetupRepo.deleteByID(game.connection, previousSetup.id);
            }else
                create(game.connection, currentSetup);
            ReplayRepo.setSetupID(game.connection, game.getID(), currentSetup.id);
        }catch(SQLException e){
            throw new NarratorException("Couldn't change setups appropriately.");
        }
    }

    public static boolean isPreset(Setup setup) {
        return !setup.getClass().equals(Setup.class) && !setup.getClass().equals(Default.class);
    }

    private static long getOrCreateSetupByUserID(Connection connection, Game game) throws SQLException {
        long userID = game.hostID;
        List<Setup> setups = SetupRepo.getActiveByOwnerID(connection, userID);
        if(!setups.isEmpty()){
            game._setup = SetupRepo.getSetup(connection, setups.get(0).id, game.narrator.removeAllFactionsAndRoles());
            return game._setup.id;
        }
        game._setup.ownerID = userID;
        return SetupRepo.create(connection, game._setup).id;
    }

    private static void checkUser(User user) {
        if(user == null)
            throw new NarratorException("No user found with that ID");
        if(!user.isInGame())
            throw new NarratorException("User is not in game");
        if(!user.isHost())
            throw new NarratorException("User is not host.");
    }

    private static void checkGame(Game game) {
        if(game == null)
            throw new NarratorException("Game with that ID not found.");
    }

    public static void assertNotAutoFilling(User user) {
        if(user.game.isAutoFillingRoles())
            throw new NarratorException("Cant manipulate preset setups.");
    }

}
