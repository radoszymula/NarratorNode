package services;

import java.sql.SQLException;
import java.util.Collection;

import game.logic.exceptions.NarratorException;
import game.logic.support.Util;
import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifier;
import json.JSONException;
import models.events.CloseLobbyEvent;
import models.schemas.GameSchema;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.User;
import repositories.ReplayRepo;

public class GameService {

    public static Collection<Game> getAllGames() {
        return NodeSwitch.idToInstance.values();
    }

    public static Game getBySetupID(long setupID) {
        for(Game game: NodeSwitch.idToInstance.values()){
            if(game._setup.id == setupID)
                return game;
        }
        return null;
    }

    public static Game getByID(long gameID) {
        for(Game game: NodeSwitch.idToInstance.values()){
            if(game.getID() == gameID)
                return game;
        }
        return null;
    }

    public static Game getByJoinID(String joinID) {
        return NodeSwitch.idToInstance.get(joinID.toUpperCase());
    }

    public static Game create(long userID, GameSchema request) {
        User user = UserService.get(userID);
        Game game;
        try{
            if(request.setupKey == null)
                game = new Game(request.setupID, userID, request.isPublic);
            else
                game = new Game(request.setupKey, userID, request.isPublic);
        }catch(SQLException e){
            System.err.println("setupID was " + request.setupID);
            System.err.println("setupKey was " + request.setupKey);
            throw new NarratorException("Failed to create game due to " + e.getMessage());
        }
        if(request.isPublic)
            NodeSwitch.instances.add(game);

        Rule r;
        for(SetupModifier modifiers: request.rules.keySet()){
            r = request.rules.get(modifiers);
            if(r.isBool())
                game.narrator.setRule(modifiers, r.getBoolVal());
            else
                game.narrator.setRule(modifiers, r.getIntVal());
        }

        PlayerService.addToGame(game, user, request.hostName);
        return game;
    }

    public static void delete(String joinID) {
        joinID = joinID.toUpperCase();
        Game game = NodeSwitch.idToInstance.get(joinID);
        if(game == null)
            return;
        delete(game);
    }

    public static void delete(Game game) {
        for(User user: game.phoneBook.values()){
            user.game = null;
            user.player = null;
        }
        game.phoneBook.clear();
        if(!game.narrator.isFinished())
            try{
                ReplayRepo.deleteByID(game.connection, game.getID());
            }catch(SQLException e){
                Util.log(e);
            }
        try{
            NodeSwitch.serverWrite(CloseLobbyEvent.request(game));
        }catch(JSONException e){
            Util.log(e, "Failed to send game end event");
        }
        NodeSwitch.instances.remove(game);
        NodeSwitch.idToInstance.remove(game.joinID);
        game.killDBConnection();
    }

    public static Game start(long userID) {
        User user = UserService.assertIsHost(userID);
        user.game.startGame();
        return user.game;
    }

}
