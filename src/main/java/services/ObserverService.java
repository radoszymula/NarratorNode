package services;

import game.logic.exceptions.NarratorException;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.User;

public class ObserverService {

    public static void addObserver(long userID, String joinID) {
        joinID = joinID.toUpperCase();
        User user = UserService.get(userID);

        Game game = NodeSwitch.idToInstance.get(joinID);
        if(game == null)
            throw new NarratorException("Game with that ID not found.");

        user.observeGame(game);
    }

}
