package services;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import game.logic.Narrator;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.support.Util;
import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifier;
import models.serviceResponse.ModifierServiceResponse;
import nnode.User;
import repositories.SetupModifierRepo;

public class SetupModifierService {

    public static void modify(Narrator n, SetupModifier modifier, int val) {
        if(!n.getRule(modifier).isInt())
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not an integer modifier.");
        n.setRule(modifier, val);
    }

    public static void modify(Narrator n, SetupModifier modifier, boolean val) {
        if(!n.getRule(modifier).isBool())
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not a boolean modifier.");
        n.setRule(modifier, val);
    }

    private static Set<SetupModifier> PRESET_MODIFIABLE_MODIFIERS = Collections
            .unmodifiableSet(new HashSet<SetupModifier>(
                    Arrays.asList(new SetupModifier[] { SetupModifier.CHAT_ROLES, SetupModifier.DAY_LENGTH,
                            SetupModifier.NIGHT_LENGTH, SetupModifier.TRIAL_LENGTH, SetupModifier.DISCUSSION_LENGTH,
                            SetupModifier.ROLE_PICKING_LENGTH, SetupModifier.HOST_VOTING, SetupModifier.VOTE_SYSTEM,
                    })));

    public static ModifierServiceResponse modify(long userID, SetupModifier id, Object value) {
        User user = UserService.assertIsHost(userID);
        if(!PRESET_MODIFIABLE_MODIFIERS.contains(id))
            SetupService.assertNotAutoFilling(user);
        Narrator narrator = user.game.narrator;

        if(narrator.isInProgress())
            throw new NarratorException("Cannot edit games that are in progress.");
        Rule rule = narrator.getRule(id);
        if(!rule.isBool() && value instanceof Boolean)
            throw new NarratorException("Modifier is not a boolean type.");
        if(!rule.isInt() && value instanceof Integer)
            throw new NarratorException("Modifier is not an integer type.");
        Object prevValue = rule.getVal();

        if(value instanceof Boolean)
            value = narrator.setRule(id, (Boolean) value);
        else
            value = narrator.setRule(id, (Integer) value);

        try{
            if(value instanceof Boolean)
                SetupModifierRepo.updateModifier(user.game.connection, id, (boolean) value, user.game._setup.id);
            else
                SetupModifierRepo.updateModifier(user.game.connection, id, (int) value, user.game._setup.id);
        }catch(SQLException e){
            if(value instanceof Boolean)
                narrator.setRule(id, (Boolean) prevValue);
            else
                narrator.setRule(id, (Integer) prevValue);
            Util.log(e);
            throw new NarratorException("Failed to update due to database error.");
        }
        return new ModifierServiceResponse(user.game._setup.id, value);
    }

}
