package services;

import java.sql.SQLException;

import game.logic.Role;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.support.Util;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifiers;
import game.roles.Ability;
import models.Modifiers;
import models.serviceResponse.ModifierServiceResponse;
import nnode.User;
import repositories.RoleAbilityModifierRepo;

public class RoleAbilityModifierService {

    public static void modify(Role role, Class<? extends Ability> abilityClass, AbilityModifier modifier, boolean val) {
        Ability ability = role._abilities.getAbility(abilityClass);
        validateAbilityModifierBool(modifier, ability);

        Modifiers<AbilityModifier> abilityModifiers = ability.modifiers;
        if(abilityModifiers == null)
            abilityModifiers = ability.modifiers = new Modifiers<>();
        abilityModifiers.add(modifier, val);
    }

    public static void modify(Role role, Class<? extends Ability> abilityClass, AbilityModifier modifier, int value) {
        Ability ability = role._abilities.getAbility(abilityClass);
        validateAbilityModifierInt(modifier, ability, value);

        Modifiers<AbilityModifier> abilityModifiers = role._abilities.getAbility(abilityClass).modifiers;
        if(abilityModifiers == null)
            abilityModifiers = ability.modifiers = new Modifiers<>();
        abilityModifiers.add(modifier, value);
    }

    public static ModifierServiceResponse modify(long userID, long roleID, long abilityID, AbilityModifier modifier,
            Object value) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);
        Role role = RoleService.get(userID, roleID);
        Ability ability = findAbility(role, abilityID);
        Object prevValue = ability.modifiers.getOrDefault(modifier, (Object) null);
        if(value instanceof Boolean)
            modify(role, ability.getClass(), modifier, (boolean) value);
        else if(value instanceof Integer)
            modify(role, ability.getClass(), modifier, (int) value);
        value = ability.modifiers.get(modifier);

        try{
            if(value instanceof Boolean)
                RoleAbilityModifierRepo.updateModifier(user.game.connection, abilityID, modifier, (boolean) value);
            else if(value instanceof Integer)
                RoleAbilityModifierRepo.updateModifier(user.game.connection, abilityID, modifier, (int) value);
        }catch(SQLException e){
            if(prevValue == null)
                ability.modifiers.remove(modifier);
            else if(value instanceof Boolean)
                modify(role, ability.getClass(), modifier, (boolean) prevValue);
            else if(value instanceof Integer)
                modify(role, ability.getClass(), modifier, (int) prevValue);
            Util.log(e);
            throw new NarratorException("Failed to update due to database error.");
        }

        return new ModifierServiceResponse(user.game._setup.id, value);
    }

    public static void validateAbilityModifierBool(AbilityModifier modifier, Ability ability)
            throws IllegalGameSettingsException {
        if(!ability.isBool(modifier))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not a boolean modifier.");

        String abilityName = ability.getClass().getSimpleName();
        if(modifier == AbilityModifier.SELF_TARGET && !ability.canEditSelfTargetableModifer())
            throw new IllegalGameSettingsException(abilityName + "'s self targetable attribute is not modifiabe.");
        if(modifier == AbilityModifier.BACK_TO_BACK && !ability.canEditBackToBackModifier())
            throw new IllegalGameSettingsException(abilityName + "'s self targetable attribute is not modifiable.");
        if(modifier == AbilityModifier.ZERO_WEIGHTED && !ability.canEditZeroWeightedModifier())
            throw new IllegalGameSettingsException(abilityName + " cannot be set to submit freely.");
    }

    public static void validateAbilityModifierInt(AbilityModifier modifier, Ability ability, int value)
            throws IllegalGameSettingsException, NarratorException {
        if(!ability.isInt(modifier))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not an integer modifier.");

        if(value < SetupModifiers.UNLIMITED || value > Constants.MAX_INT_MODIFIER)
            throw new NarratorException("Integer value out of bounds.");
        if(value == SetupModifiers.UNLIMITED && modifier == AbilityModifier.COOLDOWN)
            throw new NarratorException("Integer value out of bounds");
        if(value == 0 && modifier == AbilityModifier.CHARGES)
            throw new NarratorException("Integer value out of bounds");

        String abilityName = ability.getClass().getSimpleName();
        if(modifier == AbilityModifier.CHARGES && !ability.chargeModifiable())
            throw new IllegalGameSettingsException(abilityName + " cannot have their charges modified.");
        if(modifier == AbilityModifier.COOLDOWN && !ability.hasCooldownAbility())
            throw new IllegalGameSettingsException(abilityName + " cannot have their cooldown modified.");
        if(modifier == AbilityModifier.HIDDEN && !ability.hiddenPossible())
            throw new IllegalGameSettingsException(abilityName + " cannot be set to hidden.");
        if(modifier == AbilityModifier.SELF_TARGET && !ability.canEditSelfTargetableModifer())
            throw new IllegalGameSettingsException(abilityName + " cannot have their self targetable modifier edited.");
    }

    public static Ability findAbility(Role role, long abilityID) {
        for(Ability ability: role._abilities){
            if(ability.id == abilityID)
                return ability;
        }
        throw new NarratorException("Ability with that id not found");
    }
}
