package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.support.Util;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.roles.Ability;
import game.roles.AbilityList;
import game.roles.CultLeader;
import game.roles.Cultist;
import models.FactionRole;
import models.Modifiers;
import models.schemas.AbilityModifierSchema;
import models.schemas.RoleAbilitySchema;
import models.schemas.RoleModifierSchema;
import models.schemas.RoleSchema;
import nnode.User;
import repositories.Connection;
import repositories.RoleAbilityModifierRepo;
import repositories.RoleAbilityRepo;
import repositories.RoleModifierRepo;
import repositories.RoleRepo;

public class RoleService {

    public static Role createRole(long userID, String name, List<String> abilityNames) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);
        Narrator narrator = user.game.narrator;
        synchronized (narrator){
            AbilityList aList = Ability.CREATOR(abilityNames);

            Role role = new Role(narrator, name, aList);
            narrator.checkCreateRoleName(role);

            insertRoles(user.game.connection, user.game._setup.id, new ArrayList<>(Arrays.asList(role)));

            role = createRole(narrator, role);
            return role;
        }
    }

    @SafeVarargs
    public static Role createRole(Narrator narrator, String name, Class<? extends Ability>... abilitiyClasses) {
        List<String> abilityNames = new ArrayList<>();
        for(Class<? extends Ability> abilityClass: abilitiyClasses)
            abilityNames.add(abilityClass.getSimpleName());
        AbilityList aList = Ability.CREATOR(abilityNames);
        Role role = new Role(narrator, name, aList);
        return createRole(narrator, role);
    }

    // todo make private
    public static Role createRole(Narrator narrator, Role role) {
        narrator.checkCreateRoleName(role);
        if(role.getID() == null)
            role.setID((++narrator.roleIDCounter));
        narrator.possibleRoles = null;
        narrator.initialRoles = null;
        narrator.roles.add(role);
        return role;
    }

    public static Role createRole(Narrator narrator, RoleSchema roleSchema) {
        Role role = createRole(narrator, roleSchema.name);
        role.setID(roleSchema.id);

        if(roleSchema.modifiers != null)
            for(RoleModifierSchema roleModifier: roleSchema.modifiers){
                if(roleModifier.isInt())
                    role.addModifier(RoleModifier.valueOf(roleModifier.name.toUpperCase()), roleModifier.intValue);
                else
                    role.addModifier(RoleModifier.valueOf(roleModifier.name.toUpperCase()), roleModifier.boolValue);
            }

        if(roleSchema.abilities == null)
            return role;

        AbilityModifier modifier;
        Class<? extends Ability> abilityClass;
        for(RoleAbilitySchema ability_obj: roleSchema.abilities.values()){
            role.addAbility(ability_obj.getAbility());

            if(ability_obj.modifiers == null)
                continue;
            for(AbilityModifierSchema abilityModifier: ability_obj.modifiers){
                modifier = AbilityModifier.valueOf(abilityModifier.name.toUpperCase());
                abilityClass = Ability.getAbilityClass(ability_obj.name);
                if(abilityModifier.isInt())
                    role.addModifier(modifier, abilityClass, abilityModifier.intValue);
                else
                    role.addModifier(modifier, abilityClass, abilityModifier.boolValue);
            }
        }
        return role;
    }

    static void insertRoles(Connection connection, long setupID, ArrayList<Role> roles) {
        ArrayList<Long> roleIDs = new ArrayList<>();
        try{
            // create roles
            roleIDs.addAll(RoleRepo.create(connection, setupID, roles));
            for(int i = 0; i < roleIDs.size(); i++)
                roles.get(i).setID(roleIDs.get(i));

            // add role modifiers
            RoleModifierRepo.create(connection, roles);

            // add role abilities
            ArrayList<Ability> roleAbilities = new ArrayList<>();
            ArrayList<Long> roleAbilityRoleIDs = new ArrayList<>();
            for(Role role: roles)
                for(Ability ability: role._abilities){
                    roleAbilities.add(ability);
                    roleAbilityRoleIDs.add(role.getID());
                }
            ArrayList<Long> roleAbilityIDs = RoleAbilityRepo.create(connection, roleAbilityRoleIDs, roleAbilities);
            for(int i = 0; i < roleAbilities.size(); i++)
                roleAbilities.get(i).id = roleAbilityIDs.get(i);

            // add role ability modifiers
            ArrayList<Modifiers<AbilityModifier>> modifiersList = new ArrayList<>();
            for(Role role: roles)
                for(Ability ability: role._abilities)
                    modifiersList.add(ability.modifiers);
            RoleAbilityModifierRepo.create(connection, roleAbilityIDs, modifiersList);
        }catch(SQLException | IndexOutOfBoundsException e){
            try{
                RoleRepo.deleteByIDs(connection, setupID, roleIDs);
            }catch(SQLException f){
                Util.log(f, "Failed to cleanup roles after a bad insert.");
            }
            throw new NarratorException("Failed to save roles due to database error.");
        }
    }

    public static Role getOrCreateDeprecated(Narrator narrator, Ability ability) {
        for(Role role: narrator.roles){
            if(role._abilities.size() == 1 && role._abilities.getFirst().getClass().equals(ability.getClass()))
                return role;
        }
        String name;
        if(ability.getClass().equals(Cultist.class))
            name = narrator.getAlias(CultLeader.MINION_ALIAS);
        else
            name = ability.getClass().getSimpleName();
        return new Role(narrator, name, ability);
    }

    public static Role get(long userID, long roleID) {
        User user = UserService.assertInGame(userID);
        for(Role role: user.game.narrator.roles)
            if(role.getID() == roleID)
                return role;
        throw new NarratorException("Role with that id not found");
    }

    public static Map<Long, RoleSchema> getBySetupID(Connection connection, long setupID) {
        try{
            Map<Long, RoleSchema> roles = RoleRepo.getBySetupID(connection, setupID);

            Map<Long, Set<RoleModifierSchema>> modifiers = RoleModifierRepo.getBySetupID(connection, setupID);
            for(long roleID: modifiers.keySet())
                roles.get(roleID).modifiers = modifiers.get(roleID);

            Map<Long, Map<Long, RoleAbilitySchema>> abilities = RoleAbilityRepo.getBySetupID(connection, setupID);
            for(long roleID: abilities.keySet())
                roles.get(roleID).abilities = abilities.get(roleID);

            Map<Long, Map<Long, Set<AbilityModifierSchema>>> abilityModifiers = RoleAbilityModifierRepo
                    .getBySetupID(connection, setupID);
            Map<Long, Set<AbilityModifierSchema>> roleAbilityModifiers;
            RoleSchema role;
            for(long roleID: abilityModifiers.keySet()){
                roleAbilityModifiers = abilityModifiers.get(roleID);
                role = roles.get(roleID);
                for(long abilityID: roleAbilityModifiers.keySet())
                    role.abilities.get(abilityID).modifiers = roleAbilityModifiers.get(abilityID);
            }

            return roles;
        }catch(SQLException e){
            throw new NarratorException("Unable to get setup's roles due to database error.");
        }
    }

    public static void deleteRole(Role role) {
        List<FactionRole> factionRoles = new ArrayList<>();
        for(Faction faction: role.narrator.getFactions())
            factionRoles.add(faction._roleSet.get(role));
        for(FactionRole factionRole: factionRoles)
            if(factionRole != null)
                FactionService.deleteFactionRole(factionRole);

        role.narrator.roles.remove(role);
        role.narrator.possibleRoles = null;
        role.narrator.initialRoles = null;
    }

}
