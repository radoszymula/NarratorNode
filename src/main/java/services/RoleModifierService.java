package services;

import java.sql.SQLException;

import game.logic.Narrator;
import game.logic.Role;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.support.Util;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifiers;
import game.roles.Ability;
import game.roles.Hidden;
import models.FactionRole;
import models.Modifiers;
import models.SetupHidden;
import models.serviceResponse.ModifierServiceResponse;
import nnode.User;
import repositories.RoleModifierRepo;

public class RoleModifierService {
    public static void modify(Narrator narrator, Role role, RoleModifier modifier, boolean val) {
        if(!Ability.IsBoolModifier(modifier))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not a boolean modifier.");
        checkForUnique(narrator, role, modifier, val);
        if(role.modifiers == null)
        	role.modifiers = new Modifiers<>();
    	role.modifiers.add(modifier, val);
    }

    public static void modify(Narrator narrator, Role role, RoleModifier modifier, int val) {
        if(!Ability.IsIntModifier(modifier))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not a int modifier.");
        if(val < SetupModifiers.UNLIMITED || val > Constants.MAX_INT_MODIFIER)
        	throw new NarratorException("Invalid range input");
        if(role.modifiers == null)
        	role.modifiers = new Modifiers<>();
        role.modifiers.add(modifier, val);
    }

    private static void checkForUnique(Narrator narrator, Role m, RoleModifier modifier, boolean val) {
        if(!val)
            return;
        if(modifier != RoleModifier.UNIQUE)
            return;
        if(m.getID() == null)
            return;
        for(Hidden hiddenSpawn: narrator.rolesList.spawns.keySet()){
            for(FactionRole factionRole: narrator.rolesList.spawns.get(hiddenSpawn))
                if(factionRole.role.contains(m))
                    throw new IllegalGameSettingsException(
                            "This role cannot be unique because it's set to be spawned twice.");
        }

        boolean seenBefore = false;
        for(SetupHidden setupHidden: narrator.rolesList){
            if(!setupHidden.hidden.isHiddenSingle())
                continue;
            if(!setupHidden.hidden.contains(m))
                continue;
            if(seenBefore)
                throw new IllegalGameSettingsException(
                        "This role cannot be unique because it's already in the roles list more than once.");
            seenBefore = true;
        }

    }

    public static void modify(Narrator narrator, Hidden hidden, RoleModifier type, boolean val) {
        for(FactionRole factionRole: hidden.getFactionRoles())
            modify(narrator, factionRole.role, type, val);
    }

    public static void modify(Narrator narrator, Hidden hidden, RoleModifier type, int val) {
        for(FactionRole factionRole: hidden.getFactionRoles())
            modify(narrator, factionRole.role, type, val);
    }

    public static ModifierServiceResponse modify(long userID, long roleID, RoleModifier modifier, Object value) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);
        Role role = RoleService.get(userID, roleID);
        Object prevValue;
        if(Ability.IsIntModifier(modifier) && value instanceof Integer) {
        	prevValue = role.modifiers.getOrDefault(modifier, null);
            modify(user.game.narrator, role, modifier, (int) value);
        }else if(Ability.IsBoolModifier(modifier) && value instanceof Boolean) {
        	prevValue = role.modifiers.getOrDefault(modifier, null);
            modify(user.game.narrator, role, modifier, (boolean) value);
        }else
        	throw new NarratorException("Unknown role modifier type or value");

        try{
            if(value instanceof Boolean)
                RoleModifierRepo.updateModifier(user.game.connection, roleID, modifier, (boolean) value);
            else if(value instanceof Integer){
                RoleModifierRepo.updateModifier(user.game.connection, roleID, modifier, (int) value);
            }
        }catch(SQLException e){
        	if(prevValue == null)
        		role.modifiers.remove(modifier);
        	else if(value instanceof Boolean)
                modify(user.game.narrator, role, modifier, (boolean) prevValue);
        	else if(value instanceof Integer)
                modify(user.game.narrator, role, modifier, (int) prevValue);
            Util.log(e);
            throw new NarratorException("Failed to update due to database error.");
        }

        return new ModifierServiceResponse(user.game._setup.id, value);
    }
}
