package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.exceptions.NarratorException;
import game.logic.support.Util;
import game.roles.Hidden;
import models.FactionRole;
import models.schemas.HiddenSchema;
import nnode.Game;
import nnode.User;
import repositories.Connection;
import repositories.HiddenFactionRolesRepo;
import repositories.HiddenRepo;
import util.LookupUtil;

public class HiddenService {

    // roleIDs size will be greater than one. checked
    public static Hidden createHidden(long userID, String hiddenName, ArrayList<Long> factionRoleIDs) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);
        Game game = user.game;
        FactionRole[] factionRoles = new FactionRole[factionRoleIDs.size()];
        FactionRole role;
        long factionRoleID;
        for(int i = 0; i < factionRoleIDs.size(); i++){
            factionRoleID = factionRoleIDs.get(i);
            role = LookupUtil.findFactionRole(game.narrator, factionRoleID);
            if(role == null)
                throw new NarratorException("Role with id not found.");
            factionRoles[i] = role;
        }

        Hidden hidden = createHidden(game.narrator, hiddenName, factionRoles);
        try{
            insertHiddens(game.connection, game._setup.id, new ArrayList<>(Arrays.asList(hidden)));
        }catch(NarratorException e){
            game.narrator.hiddens.remove(hidden);
            throw e;
        }
        return hidden;
    }

    public static Hidden createHidden(Narrator narrator, String hiddenName, List<FactionRole> roleList) {
        FactionRole[] roles = new FactionRole[roleList.size()];
        for(int i = 0; i < roleList.size(); i++)
            roles[i] = roleList.get(i);
        return createHidden(narrator, hiddenName, roles);
    }

    public static Hidden createHidden(Narrator narrator, String hiddenName, FactionRole... factionRoles) {
        for(Hidden hidden: narrator.hiddens){
            if(hidden.getName().equals(hiddenName) && (factionRoles.length > 1 || hidden.factionRolesMap.size() > 1))
                throw new NarratorException("Duplicate hidden name: " + hiddenName + ".");
        }
        for(Faction faction: narrator.getFactions()){
            if(faction.getName().equals(hiddenName))
                throw new NarratorException("Hidden name is already in use by a faction.");
        }
        Hidden hidden = new Hidden(hiddenName, narrator);
        narrator.hiddens.add(hidden);
        HiddenFactionRoleService.addSpawnableRoles(narrator, hidden, factionRoles);
        checkValidHidden(hidden, narrator);
        return hidden;
    }

    public static void insertHiddens(Connection connection, long setupID, ArrayList<Hidden> hiddens) {
        ArrayList<Long> hiddenIDs = new ArrayList<>();

        try{
            // create hiddens
            hiddenIDs.addAll(HiddenRepo.create(connection, setupID, hiddens));
            for(int i = 0; i < hiddens.size(); i++)
                hiddens.get(i).setID(hiddenIDs.get(i));

            // add faction roles to hiddens
            HiddenFactionRolesRepo.create(connection, setupID, hiddens);
        }catch(SQLException | IndexOutOfBoundsException e){
            try{
                HiddenRepo.deleteByIDs(connection, setupID, hiddenIDs);
            }catch(SQLException f){
                Util.log("Failed to cleanup hiddens after a bad insert.");
            }
            throw new NarratorException("Failed to save hiddens due to database error.");
        }
        for(int i = 0; i < hiddens.size(); i++)
            hiddens.get(i).setID(hiddenIDs.get(i));
    }

    public static List<HiddenSchema> getBySetupID(Connection connection, long setupID) {
        try{
            Map<Long, HiddenSchema> hiddens = HiddenRepo.getBySetupID(connection, setupID);

            Map<Long, Set<Long>> setupfactionRoleIDs = HiddenFactionRolesRepo.getBySetupID(connection, setupID);
            for(long hiddenID: setupfactionRoleIDs.keySet())
                hiddens.get(hiddenID).factionRoleIDs = setupfactionRoleIDs.get(hiddenID);

            return new LinkedList<>(hiddens.values());
        }catch(SQLException e){
            throw new NarratorException("Unable to get setup's hiddens due to database error.");
        }
    }

    public static void delete(Narrator narrator, Hidden hidden) {
        narrator.removeAllSetupHiddens(hidden);
        narrator.hiddens.remove(hidden);
    }

    private static void checkValidHidden(Hidden newHidden, Narrator narrator) {
        narrator.roleTemplateNameCheck(newHidden);
        // for(Hidden currentHiddens: narrator.hiddens){
        // if(currentHiddens.getName().equals(newHidden.getName()) &&
        // currentHiddens.getColor().equals(newHidden.getColor())){
        // //necessary because randommember to string -> returns the name of the random
        // member
        // throw new NamingException("Can't use the same name to describe two different
        // random roles");
        // }
        // }
    }
}
