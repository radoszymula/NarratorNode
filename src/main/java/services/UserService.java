package services;

import game.logic.exceptions.NarratorException;
import nnode.NodeSwitch;
import nnode.User;

public class UserService {

    public static User get(long userID) {
        if(NodeSwitch.phoneBook.containsKey(userID))
            return NodeSwitch.phoneBook.get(userID);
        User user = new User(userID);
        NodeSwitch.phoneBook.put(userID, user);
        return user;
    }

    public static User assertInGame(long userID) {
        User user = NodeSwitch.phoneBook.get(userID);
        if(user == null)
            throw new NarratorException("User not found.");
        if(!user.isInGame())
            throw new NarratorException("User is not in a game.");
        return user;
    }

    public static User assertIsHost(long userID) {
        User user = assertInGame(userID);
        if(!user.isHost())
            throw new NarratorException("User is not the host.");
        return user;
    }
}
