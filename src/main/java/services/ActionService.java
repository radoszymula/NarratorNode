package services;

import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.Spy;
import json.JSONException;
import models.enums.GamePhase;
import models.serviceResponse.ActionResponse;
import nnode.Permission;
import nnode.StateObject;
import nnode.User;

public class ActionService {

    public static ActionResponse submitAction(long userID, String message) throws JSONException {
        User user = UserService.assertInGame(userID);
        if(user.player == null && user.game.getHost() != user)
            throw new NarratorException("User may not submit actions");

        boolean modSubmission = user.game.getHost() == user || user.game.hasPermission(user, Permission.GAME_EDITING);
        user.game.ch.command(user.player, message, "", user.game.nodeSwitchLookup, modSubmission);
        user.game.endGameIfNoParticipatingPlayersAlive();
        return getActionResponse(user);
    }

    public static ActionResponse submitAction(long userID, String command, List<String> targets, String option1,
            String option2, String option3, Integer actionIndex, boolean unvoting, boolean skipVoting)
            throws JSONException {
        User user = UserService.assertInGame(userID);
        Narrator narrator = user.game.narrator;
        PlayerList pList = new PlayerList();
        Player p = user.player;
        for(String playerName: targets)
            pList.add(narrator.getPlayerByName(playerName));

        if(option1 != null && p.is(Spy.class) && narrator.getFactionByName(option1) != null)
            option1 = narrator.getFactionByName(option1).getColor();

        boolean replacingAction = actionIndex != null;
        if(replacingAction){
            Action a = p.getActions().getActions().get(actionIndex);
            Action newAction = new Action(p, pList, Ability.ParseAbility(command), option1, option2, option3);
            p.getActions().replace(a, newAction);
        }else if(command.equalsIgnoreCase(Constants.VOTE) && user.id == user.game.hostID){
            if(!narrator.getBool(SetupModifier.HOST_VOTING)){
                if(unvoting)
                    p.unvote();
                else if(skipVoting)
                    p.voteSkip();
                else
                    p.vote(pList.getFirst());
            }else{
                try{
                    if(skipVoting){
                        for(Player x: pList){
                            if(!x.isDisenfranchised()) // dont need to check for vent since hostvoting and chat roles
                                x.voteSkip();
                            if(!narrator.isDay())
                                break;
                        }

                    }else{
                        Player votee = pList.remove(0);
                        for(Player x: pList)
                            if(!x.isDisenfranchised() && narrator.voteSystem.getVoteTarget(x) != votee)
                                x.vote(votee);
                    }
                }catch(NarratorException e){
                    e.printStackTrace();
                }
                if(narrator.isDay() && narrator._lynches == 1)
                    narrator.forceEndDay();
            }
        }else{
            if(narrator.isDay() && p.hasDayAction(command)){
                p.doDayAction(command, option1, pList);
            }else
                p.setTarget(Ability.ParseAbility(command), option1, option2, option3, pList);
        }
        user.game.endGameIfNoParticipatingPlayersAlive();
        return getActionResponse(user);
    }

    public static void deleteAction(long userID, String command, int actionIndex) {
        User user = UserService.assertInGame(userID);
        Player player = user.player;
        if(command.equalsIgnoreCase(Faction.SEND))
            player.cancelTarget(Faction.SEND_);
        else
            player.cancelAction(actionIndex);
        user.game.endGameIfNoParticipatingPlayersAlive();
    }

    private static ActionResponse getActionResponse(User user) throws JSONException {
        ActionResponse actionResponse = new ActionResponse();
        Narrator narrator = user.game.narrator;
        if(narrator.phase == GamePhase.VOTE_PHASE){
            actionResponse.isSkipping = narrator.voteSystem.getVoteTarget(user.player) == narrator.skipper;
            actionResponse.voteInfo = VoteService.getVotes(user.game);
        }
        actionResponse.actions = StateObject.getStateObjectActions(user.player);
        return actionResponse;
    }

}
