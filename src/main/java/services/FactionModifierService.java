package services;

import java.sql.SQLException;

import game.logic.Faction;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.support.Util;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.roles.Ability;
import models.serviceResponse.ModifierServiceResponse;
import nnode.User;
import repositories.FactionAbilityModifierRepo;
import repositories.FactionModifierRepo;

public class FactionModifierService {

    public static int modify(Faction faction, FactionModifier modifier, int val) {
        if(!Ability.IsIntModifier(modifier))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not an integer modifier.");
        if(FactionModifier.maxValue(modifier) < val || FactionModifier.minValue(modifier) > val)
        	throw new NarratorException("Value out of range.");
        faction._modifierMap.add(modifier, val);
        return val;
    }

    public static boolean modify(Faction faction, FactionModifier modifier, boolean value) {
    	if(!Ability.IsBoolModifier(modifier))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not a boolean modifier.");
    	faction._modifierMap.add(modifier, value);
    	return value;
    }

    public static ModifierServiceResponse modify(long userID, long factionID, FactionModifier modifier, boolean value) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);
        Faction faction = user.game.narrator.getFactionByID(factionID);
        if(faction == null)
            throw new NarratorException("Faction not found.");
        if(!Ability.IsBoolModifier(modifier))
            throw new NarratorException("Modifier is not a boolean type.");
        boolean prevVal = faction._modifierMap.getOrDefault(modifier, false);
        value = modify(faction, modifier, value);

        try{
            FactionModifierRepo.updateModifier(user.game.connection, modifier, value, factionID);
        }catch(SQLException e){
            modify(faction, modifier, prevVal);
            Util.log(e);
            throw new NarratorException("Failed to update due to database error.");
        }
        return new ModifierServiceResponse(user.game._setup.id, value);
    }

    public static ModifierServiceResponse modify(long userID, long factionID, FactionModifier modifier, int value) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);
        Faction faction = user.game.narrator.getFactionByID(factionID);
        if(faction == null)
            throw new NarratorException("Faction not found");
        if(!Ability.IsIntModifier(modifier))
            throw new NarratorException("Modifier is not an integer type.");
        int prevVal = faction._modifierMap.getOrDefault(modifier, 0);
        value = modify(faction, modifier, value);

        try{
            FactionModifierRepo.updateModifier(user.game.connection, modifier, value, factionID);
        }catch(SQLException e){
            modify(faction, modifier, prevVal);
            Util.log(e);
            throw new NarratorException("Failed to update due to database error.");
        }
        return new ModifierServiceResponse(user.game._setup.id, value);
    }

    public static ModifierServiceResponse modify(long userID, long factionID, long abilityID, AbilityModifier modifier,
            Object value) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);
        Faction faction = user.game.narrator.getFactionByID(factionID);
        Ability ability = findAbility(faction, abilityID);
        Object prevValue = ability.modifiers.get(modifier);
        if(value instanceof Boolean)
            modify(faction, ability.getClass(), modifier, (boolean) value);
        else if(value instanceof Integer)
            modify(faction, ability.getClass(), modifier, (int) value);
        value = ability.modifiers.get(modifier);

        try{
            if(value instanceof Boolean)
                FactionAbilityModifierRepo.updateModifier(user.game.connection, abilityID, modifier, (boolean) value);
            else if(value instanceof Integer)
                FactionAbilityModifierRepo.updateModifier(user.game.connection, abilityID, modifier, (int) value);
        }catch(SQLException e){
            if(value instanceof Boolean)
                modify(faction, ability.getClass(), modifier, (boolean) prevValue);
            if(value instanceof Integer)
                modify(faction, ability.getClass(), modifier, (int) prevValue);
            Util.log(e);
            throw new NarratorException("Failed to update due to database error.");
        }

        return new ModifierServiceResponse(user.game._setup.id, value);
    }

    public static void modify(Faction faction, Class<? extends Ability> abilityClass, AbilityModifier modifier,
            boolean val) {
        if(!Ability.getAbilityCopy(abilityClass).isBool(modifier))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not a boolean modifier.");
        faction.modifyAbility(modifier, abilityClass, val);
    }

    public static void modify(Faction faction, Class<? extends Ability> abilityClass, AbilityModifier modifier,
            int val) {
        if(!Ability.getAbilityCopy(abilityClass).isInt(modifier))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not a integer modifier.");
        faction.modifyAbility(modifier, abilityClass, val);
    }

    private static Ability findAbility(Faction faction, long abilityID) {
        for(Ability ability: faction._abilities){
            if(ability.id == abilityID)
                return ability;
        }
        throw new NarratorException("Ability with that id not found");
    }

}
