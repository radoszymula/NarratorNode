package services;

import game.event.EventList;
import game.event.Message;
import game.logic.support.CommandHandler;
import game.logic.support.Util;
import game.roles.Ghost;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.StateObject;
import nnode.User;

public class ChatService {

    public static JSONObject getUserChats(long userID) {
        JSONObject jo = new JSONObject();
        if(!NodeSwitch.phoneBook.containsKey(userID))
            return jo;

        User user = NodeSwitch.phoneBook.get(userID);
        if(user.isInLobby())
            return jo;

        JSONArray chatLog = new JSONArray();
        EventList eList;
        String key;
        if(user.player == null){
            key = Message.PUBLIC;
            eList = user.game.narrator.getEventManager().getEvents(key);
        }else if(user.player.isDead() && !user.player.hasAbility(Ghost.class)){
            key = Message.PRIVATE;
            eList = user.player.getEvents();
        }else{
            key = user.player.getName();
            eList = user.player.getEvents();
        }

        try{
            for(Message e: eList)
                Game.AppendMessage(chatLog, e, user.player, key);

            jo.put(StateObject.message, chatLog);
            jo.put(StateObject.chatReset, StateObject.getChatKeys(user.game.narrator, user.player));
            jo.put(StateObject.server, false);
            return jo;
        }catch(JSONException e){
            Util.log(e, "Failed to create chat object for user.");
            return null;
        }

    }

    public static void submit(long userID, String chatKey, String messageText) throws JSONException {
        ActionService.submitAction(userID, Message.CommandCreator(CommandHandler.SAY, chatKey, messageText));
    }

}
