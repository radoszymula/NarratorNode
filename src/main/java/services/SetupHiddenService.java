package services;

import java.sql.SQLException;

import game.logic.Narrator;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.UnknownFactionRoleException;
import game.logic.exceptions.UnknownHiddenException;
import game.roles.Hidden;
import models.FactionRole;
import models.SetupHidden;
import models.serviceResponse.SetupHiddenAddResponse;
import nnode.Permission;
import nnode.User;
import repositories.Connection;
import repositories.SetupHiddenRepo;
import repositories.UserPermissionRepo;
import util.LookupUtil;

public class SetupHiddenService {
    public static Hidden removeSetupHiddenByHiddenID(long setupID, Narrator narrator, long hiddenID) {
        Hidden markedSetupHidden = LookupUtil.findHidden(narrator, hiddenID);
        if(markedSetupHidden == null)
            throw new NarratorException("Could not find hidden with that id in the setup.");

        Connection connection = null;
        try{
            connection = new Connection();
            SetupHiddenRepo.delete(connection, setupID, markedSetupHidden.getID());
        }catch(SQLException e){
            if(connection != null)
                connection.close();
            throw new NarratorException("Setup Hidden failed removal due to database error.");
        }
        connection.close();
        narrator.removeSetupHidden(markedSetupHidden);
        return markedSetupHidden;
    }

    public static SetupHiddenAddResponse addSetupHidden(long userID, long hiddenID, boolean isExposed) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);

        int rolesListSize = user.game.narrator.rolesList.size();
        try{
            if(rolesListSize > 15
                    && !UserPermissionRepo.hasPermission(user.game.connection, userID, Permission.GAME_EDITING))
                throw new NarratorException("You may not add any more roles in a public game.");
        }catch(SQLException e){
            throw new NarratorException("You may not add any more roles in a public game.");
        }
        SetupHidden setupHidden = addSetupHidden(user.game._setup.id, hiddenID, isExposed, user.game.narrator,
                user.game.connection);
        return new SetupHiddenAddResponse(setupHidden, user.game._setup.id, user.game.getID());
    }

    public static SetupHidden addSetupHidden(long setupID, long hiddenID, boolean isExposed, Narrator narrator,
            Connection connection) {
        Hidden hidden = LookupUtil.findHidden(narrator, hiddenID);
        if(hidden == null)
            throw new NarratorException("No hidden found with that ID");

        long setupHiddenID = addSetupHidden(connection, setupID, hidden);
        SetupHidden setupHidden = addSetupHidden(narrator, hidden, isExposed);
        setupHidden.id = setupHiddenID;
        return setupHidden;
    }

    public static SetupHidden addSetupHidden(Narrator narrator, Hidden hidden, boolean isExposed) {
        if(!narrator.hiddens.contains(hidden))
            throw new NarratorException("Hidden is not created.");
        SetupHidden setupHidden = narrator.rolesList.add(hidden);
        setupHidden.isExposed = isExposed;
        return setupHidden;
    }

    private static long addSetupHidden(Connection connection, long setupID, Hidden hidden) {
        try{
            return SetupHiddenRepo.create(connection, hidden.getID(), setupID);
        }catch(SQLException e){
            throw new NarratorException("Setup hidden failed to be added due to database error.");
        }
    }

    public static long deleteSetupHidden(long userID, long setupHiddenID) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);
        SetupHidden setupHidden = LookupUtil.findSetupHidden(user.game.narrator, setupHiddenID);
        if(setupHidden == null)
            throw new NarratorException("No hidden found with that ID");

        try{
            SetupHiddenRepo.delete(user.game.connection, setupHiddenID, user.game._setup.id);
        }catch(SQLException e){
            throw new NarratorException("Setup hidden failed to be added due to database error.");
        }

        deleteSetupHidden(user.game.narrator, setupHidden);
        return user.game.getID();
    }

    public static void deleteSetupHidden(Narrator narrator, SetupHidden setupHidden) {
        narrator.rolesList.remove(setupHidden);
    }

    public static void CreateHostSpawn(Narrator narrator, long hiddenID, long factionRoleID) {
        FactionRole spawn = LookupUtil.findFactionRole(narrator, factionRoleID);
        Hidden hidden = LookupUtil.findHidden(narrator, hiddenID);
        if(hidden == null)
            throw new UnknownHiddenException(hiddenID);
        if(spawn == null)
            throw new UnknownFactionRoleException(factionRoleID);

        CreateHostSpawn(narrator, hidden, spawn);
    }

    public static void CreateHostSpawn(Narrator narrator, Hidden hidden, FactionRole spawn) {
        if(!hidden.contains(spawn))
            throw new IllegalGameSettingsException("Cannot spawn " + spawn.getName() + " with " + hidden.getName());
        if(spawn.role.isUnique() && narrator.rolesList.spawns.containsKey(hidden)
                && narrator.rolesList.spawns.get(hidden).contains(spawn))
            throw new IllegalGameSettingsException(
                    "Cannot spawn another role like this because " + spawn.getName() + " is unique.");
        narrator.rolesList.addSpawn(hidden, spawn);
    }

    public static void setExposed(long userID, long setupHiddenID, boolean isExposed) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);
        for(SetupHidden setupHidden: user.game.narrator.rolesList){
            if(setupHidden.id == setupHiddenID){
                try{
                    SetupHiddenRepo.setExposed(user.game.connection, setupHiddenID, isExposed);
                }catch(SQLException e){
                    throw new NarratorException("Cannot set setup hidden exposed due to database error.");
                }
                setupHidden.isExposed = isExposed;
                return;
            }
        }
        throw new NarratorException("Cannot find setup hidden with that id.");
    }

    public static void setExposed(SetupHidden setupHidden) {
        setupHidden.isExposed = true;
    }
}
