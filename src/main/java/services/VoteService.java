package services;

import game.event.DeathAnnouncement;
import game.event.Message;
import game.event.VoteAnnouncement;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.json_output.VoteSettingsJson;
import nnode.Game;
import nnode.NodeSwitch;

public class VoteService {

    public static JSONObject getVotes(String joinID) throws JSONException {
        Game game = NodeSwitch.idToInstance.get(joinID);
        if(game == null)
            throw new NarratorException("Game with that ID not found.");
        return getVotes(game);
    }

    public static JSONObject getVotes(Game game) throws JSONException {
        Narrator narrator = game.narrator;
        JSONObject voteHolder = new JSONObject();

        JSONArray voteCounts = new JSONArray(), alivePlayers;
        ;

        JSONObject dayRecap;
        for(int i = 1; i <= narrator.getDayNumber(); i++){
            dayRecap = new JSONObject();
            alivePlayers = new JSONArray();
            addVoteMovements(dayRecap, i, narrator);

            for(Player p: narrator.getAllPlayers()){
                if(p.isAlive())
                    alivePlayers.put(p.getName());
                else if(p.getDeathDay() >= i){
                    alivePlayers.put(p.getName());
                }
            }

            dayRecap.put("alivePlayers", alivePlayers);
            voteCounts.put(dayRecap);
        }

        JSONObject currentVotes = new JSONObject();
        Player voteTarget;
        for(Player p: narrator.getLivePlayers()){
            voteTarget = narrator.voteSystem.getVoteTarget(p);
            if(voteTarget == null)
                currentVotes.put(p.getName(), 0);
            else if(voteTarget == narrator.skipper)
                currentVotes.put(p.getName(), narrator.skipper.getName());
            else
                currentVotes.put(p.getName(), voteTarget.getName());
        }
        voteHolder.put("currentVotes", currentVotes);
        voteHolder.put("voteCounts", voteCounts);
        voteHolder.put("settings", VoteSettingsJson.toJson(game));
        voteHolder.put("allowedTargets", getElligibleTargets(game));

        return voteHolder;
    }

    private static JSONArray getElligibleTargets(Game game) {
        JSONArray elligibleTargets = new JSONArray();
        for(Player p: game.narrator.voteSystem.getElligibleTargets()){
            elligibleTargets.put(p.getName());
        }
        return elligibleTargets;
    }

    private static void addVoteMovements(JSONObject j, int i, Narrator narrator) throws JSONException {
        JSONArray movements = new JSONArray(), deads;

        VoteAnnouncement va;
        DeathAnnouncement da;
        JSONObject vote;
        for(Message m: narrator.getEventManager().getDayChat().getEvents()){
            if(m instanceof VoteAnnouncement && m.getDay() == i){
                va = (VoteAnnouncement) m;

                vote = new JSONObject();
                vote.put("voter", va.voter.getName());
                vote.put("voted", va.voteTarget);
                vote.put("prev", va.prev);
                if(va.voteCount != 1)
                    vote.put("power", va.voteCount);
                movements.put(vote);
            }else if(m instanceof DeathAnnouncement && m.getDay() == i){
                da = (DeathAnnouncement) m;
                if(da.dead == null || da.dead.isEmpty())
                    continue;

                deads = new JSONArray();
                for(Player d: da.dead){
                    deads.put(d.getName());
                }
                movements.put(deads);
            }
        }

        j.put("movements", movements);
    }

}
