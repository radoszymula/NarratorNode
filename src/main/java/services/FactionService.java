package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Faction;
import game.logic.MemberList;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.TeamSettingsException;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.Util;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.roles.Ability;
import game.roles.FactionHidden;
import game.roles.FactionSend;
import game.roles.Hidden;
import models.FactionRole;
import models.Modifiers;
import models.schemas.AbilityModifierSchema;
import models.schemas.FactionAbilityModifierSchema;
import models.schemas.FactionAbilitySchema;
import models.schemas.FactionModifierSchema;
import models.schemas.FactionRoleSchema;
import models.schemas.FactionSchema;
import models.schemas.RoleModifierSchema;
import nnode.User;
import repositories.Connection;
import repositories.FactionAbilityModifierRepo;
import repositories.FactionAbilityRepo;
import repositories.FactionModifierRepo;
import repositories.FactionRepo;
import repositories.FactionRoleRepo;
import util.LookupUtil;

public class FactionService {

    public static Faction createFaction(long userID, String color, String name, String description) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);

        Connection connection = user.game.connection;
        Narrator narrator = user.game.narrator;
        Faction faction = createFaction(narrator, color, name, description);
        try{
            faction.id = FactionRepo.insertFactions(connection, user.game._setup.id, Arrays.asList(faction)).get(0);
            FactionModifierRepo.create(connection, Arrays.asList(faction));
        }catch(SQLException e){
            deleteFaction(narrator, faction.id);
            throw new NarratorException("Create team failed due to database error.");
        }

        updateAutoFactionHidden(userID, faction.id, true);
        return faction;
    }

    public static Faction createFaction(Narrator narrator, String color, String name) {
        return createFaction(narrator, color, name, "");
    }

    public static Faction createFaction(Narrator narrator, String color, String name, String description) {
        checkColor(color);
        if(narrator._factions.containsKey(color))
            throw new TeamSettingsException("This team already exists");

        Faction faction = new Faction(narrator, color, name, description);
        narrator._factions.put(color, faction);
        faction.setName(name);
        faction.setDescription(description);
        faction.id = Math.abs(new Random().nextLong());
        return faction;
    }

    public static long createFactionAbility(long userID, long factionID, String abilityName) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);

        Faction faction = user.game.narrator.getFactionByID(factionID);
        if(faction == null)
            throw new NarratorException("Faction with that id not found.");

        Ability ability = Ability.CREATOR(abilityName);
        faction.addAbility(ability);

        long factionAbilityID;
        try{
            factionAbilityID = FactionAbilityRepo.create(user.game.connection,
                    new ArrayList<>(Arrays.asList(factionID)), new ArrayList<>(Arrays.asList(ability))).get(0);
        }catch(SQLException e){
            faction.removeAbility(ability.getClass());
            throw new NarratorException("Failed to create faction ability due to database exception");
        }
        ability.id = factionAbilityID;
        return factionAbilityID;
    }

    public static FactionRole createFactionRole(long userID, long factionID, long roleID) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);
        Role role = LookupUtil.findRole(user.game.narrator, roleID);
        if(role == null)
            throw new NarratorException("Role not found.");
        Faction faction = user.game.narrator.getFactionByID(factionID);
        if(faction == null)
            throw new NarratorException("Faction not found.");
        if(faction._roleSet.containsKey(role))
            return faction._roleSet.get(role);

        long factionRoleID;
        try{
            factionRoleID = FactionRoleRepo.create(user.game.connection, roleID, factionID, user.game._setup.id);
        }catch(SQLException e){
            throw new NarratorException("Failed to add role to faction due to database exception.");
        }

        FactionRole factionRole = createFactionRole(faction, role);
        factionRole.id = factionRoleID;
        return factionRole;
    }

    public static FactionRole createFactionRole(Faction faction, Role role) {
        if(!faction.narrator.roles.contains(role))
            throw new NarratorException("Role does not exist");
        FactionRole factionRole = new FactionRole(faction, role);
        factionRole.id = Util.getID();
        faction._roleSet.put(role, factionRole);
        return factionRole;
    }

    public static void createFactionRoles(Narrator narrator, Set<FactionRoleSchema> schemas) {
        Faction faction;
        Role role;
        FactionRole factionRole;
        Map<Long, FactionRole> idToFactionRoles = new HashMap<>();
        for(FactionRoleSchema schema: schemas){
            faction = narrator.getFactionByID(schema.factionID);
            role = LookupUtil.findRole(narrator, schema.roleID);
            factionRole = createFactionRole(faction, role);
            factionRole.id = schema.id;
            idToFactionRoles.put(factionRole.id, factionRole);
        }

        FactionRole receivedFactionRole;
        for(FactionRoleSchema schema: schemas){
            if(schema.receivedFactionRoleID == null)
                continue;
            factionRole = idToFactionRoles.get(schema.id);
            receivedFactionRole = idToFactionRoles.get(schema.receivedFactionRoleID);
            FactionRoleService.setRoleReceive(factionRole, receivedFactionRole);
        }

        RoleModifier modifier;
        for(FactionRoleSchema schema: schemas){
            factionRole = idToFactionRoles.get(schema.id);
            if(schema.roleModifiers == null)
                continue;
            for(RoleModifierSchema roleModifierSchema: schema.roleModifiers){
                modifier = roleModifierSchema.modifier;
                if(Ability.IsBoolModifier(modifier))
                    FactionRoleService.addModifier(factionRole, roleModifierSchema.modifier,
                            roleModifierSchema.boolValue);
                else
                    FactionRoleService.addModifier(factionRole, roleModifierSchema.modifier,
                            roleModifierSchema.intValue);
            }
        }

        Ability ability;
        for(FactionRoleSchema schema: schemas){
            factionRole = idToFactionRoles.get(schema.id);
            if(schema.abilityModifiers == null)
                continue;
            for(long abilityID: schema.abilityModifiers.keySet()){
                for(AbilityModifierSchema modifierSchema: schema.abilityModifiers.get(abilityID)){
                    ability = LookupUtil.findAbility(factionRole.role, abilityID);
                    if(Ability.IsBoolModifier(modifierSchema.modifier))
                        FactionRoleService.addModifier(factionRole, modifierSchema.modifier, ability.getClass(),
                                modifierSchema.boolValue);
                    else
                        FactionRoleService.addModifier(factionRole, modifierSchema.modifier, ability.getClass(),
                                modifierSchema.intValue);
                }
            }
        }
    }

    public static void saveFactions(Connection connection, long setupID, ArrayList<Faction> factions) {
        if(factions.isEmpty())
            return;

        ArrayList<Long> factionIDs = new ArrayList<>();
        try{

            // create factions
            factionIDs.addAll(FactionRepo.insertFactions(connection, setupID, factions));
            for(int i = 0; i < factions.size(); i++)
                factions.get(i).id = factionIDs.get(i);

            // add faction modifiers
            FactionModifierRepo.create(connection, factions);

            // add faction abilities
            ArrayList<Ability> factionAbilities = new ArrayList<>();
            ArrayList<Long> factionAbilityFactionIDs = new ArrayList<>();
            for(Faction faction: factions)
                if(faction._abilities != null)
                    for(Ability ability: faction._abilities){
                        if(!ability.getClass().equals(FactionSend.class)){
                            factionAbilities.add(ability);
                            factionAbilityFactionIDs.add(faction.id);
                        }
                    }
            ArrayList<Long> factionAbilityIDs = FactionAbilityRepo.create(connection, factionAbilityFactionIDs,
                    factionAbilities);
            for(int i = 0; i < factionAbilities.size(); i++)
                factionAbilities.get(i).id = factionAbilityIDs.get(i);

            // add faction ability modifiers
            ArrayList<Modifiers<AbilityModifier>> modifiersList = new ArrayList<>();
            for(Faction faction: factions)
                if(faction._abilities != null)
                    for(Ability ability: faction._abilities)
                        if(!ability.getClass().equals(FactionSend.class))
                            modifiersList.add(ability.modifiers);
            FactionAbilityModifierRepo.create(connection, factionAbilityIDs, modifiersList);
        }catch(SQLException | IndexOutOfBoundsException e){
            try{
                FactionRepo.deleteByIDs(connection, setupID, factionIDs);
            }catch(SQLException f){
                Util.log(f, "Failed to cleanup factions after a bad insert.");
            }
            throw new NarratorException("Failed to save factions due to database error.");
        }
    }

    public static List<FactionSchema> getBySetupID(Connection connection, long setupID) {
        try{
            Map<Long, FactionSchema> factions = FactionRepo.getBySetupID(connection, setupID);

            Map<Long, Set<FactionModifierSchema>> modifiers = FactionModifierRepo.getBySetupID(connection, setupID);
            for(long factionID: modifiers.keySet())
                factions.get(factionID).modifiers = modifiers.get(factionID);

            Map<Long, Map<Long, FactionAbilitySchema>> abilities = FactionAbilityRepo.getBySetupID(connection, setupID);
            for(long factionID: abilities.keySet())
                factions.get(factionID).abilities = abilities.get(factionID);

            Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>> abilityModifiers = FactionAbilityModifierRepo
                    .getBySetupID(connection, setupID);
            Map<Long, Set<FactionAbilityModifierSchema>> factionAbilityModifiers;
            FactionSchema faction;
            for(long factionID: abilityModifiers.keySet()){
                factionAbilityModifiers = abilityModifiers.get(factionID);
                faction = factions.get(factionID);
                for(long abilityID: factionAbilityModifiers.keySet())
                    faction.abilities.get(abilityID).modifiers = factionAbilityModifiers.get(abilityID);
            }

            return new LinkedList<>(factions.values());
        }catch(SQLException e){
            throw new NarratorException("Unable to get setup's factions due to database error.");
        }
    }

    public static void updateName(Connection connection, Faction faction, String newName) {
        nameValidation(faction.narrator, newName, faction);
        try{
            connection = new Connection();
            FactionRepo.editName(connection, faction.id, newName);
        }catch(SQLException e){
            throw new NarratorException("Failed to update name due to database exception.");
        }
        faction.setName(newName);
    }

    public static void updateDescription(Connection connection, Faction faction, String description) {
        try{
            FactionRepo.editDescription(connection, faction.id, description);
        }catch(SQLException e){
            throw new NarratorException("Failed to update name due to database exception.");
        }
        faction.setDescription(description);
    }

    public static void updateAutoFactionHidden(long userID, long factionID, boolean isAutoOn) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);

        Narrator narrator = user.game.narrator;
        Faction faction = narrator.getFactionByID(factionID);

        if(isAutoOn){
            Hidden factionHidden = new FactionHidden(faction);
            if(LookupUtil.findHidden(narrator, factionHidden.getName()) != null)
                throw new NarratorException("Can't create auto faction hidden due to existing hidden name conflict.");
        }

        try{
            FactionRepo.updateAutoFactionHidden(user.game.connection, user.game._setup.id, factionID, isAutoOn);
        }catch(SQLException e){
            throw new NarratorException("Couldn't add auto faction hidden due to database error.");
        }

        Hidden factionHidden = new FactionHidden(faction);
        if(isAutoOn){
            factionHidden.setID(faction.id * -1);
            narrator.hiddens.add(factionHidden);
        }else{
            Hidden hidden = LookupUtil.findHidden(narrator, factionHidden.getName());
            HiddenService.delete(narrator, hidden);
        }
    }

    public static long deleteFaction(long userID, long factionID) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);

        long setupID = user.game._setup.id;
        try{
            FactionRepo.deleteByIDs(user.game.connection, setupID, Arrays.asList(factionID));
        }catch(SQLException e){
            throw new NarratorException("Failed to update name due to database exception.");
        }
        deleteFaction(user.game.narrator, factionID);
        return user.game._setup.id;
    }

    public static void deleteFaction(Narrator n, long factionID) {
        Faction faction = n.getFactionByID(factionID);
        if(faction == null)
            throw new NullPointerException();

        for(Faction keep: n.getFactions()){
            if(keep == faction)
                continue;
            keep.removeSheriffDetectableTeam(faction);
            keep.addAlly(faction);
        }

        MemberList roles = new MemberList(faction._roleSet.values());
        for(FactionRole role: roles)
            deleteFactionRole(role);

        Iterator<Hidden> hiddens = n.hiddens.iterator();
        Hidden hidden;
        while (hiddens.hasNext()){
            hidden = hiddens.next();
            if(hidden instanceof FactionHidden && hidden.getColors().contains(faction.getColor()))
                hiddens.remove();
        }
        n._factions.remove(faction.getColor());
    }

    public static void deleteFactionRole(long userID, long factionRoleID) {
        User user = UserService.assertIsHost(userID);
        SetupService.assertNotAutoFilling(user);
        FactionRole factionRole = LookupUtil.findFactionRole(user.game.narrator, factionRoleID);
        if(factionRole == null)
            throw new NarratorException("Could not find faction role with that id.");

        if(factionRole.receivedRole != null)
            FactionRoleService.deleteRoleReceived(userID, factionRoleID);

        long setupID = user.game._setup.id;
        try{
            FactionRoleRepo.deleteByIDs(user.game.connection, setupID, new ArrayList<>(Arrays.asList(factionRoleID)));
        }catch(SQLException e){
            throw new NarratorException("Could not delete faction role because of database error.");
        }
        deleteFactionRole(factionRole);
    }

    public static void deleteFactionRole(FactionRole factionRole) {
        Narrator narrator = factionRole.faction.narrator;
        List<Hidden> hiddens = new ArrayList<>(narrator.hiddens);
        for(Hidden hidden: hiddens){
            if(!hidden.contains(factionRole))
                continue;
            if(hidden.isHiddenSingle())
                HiddenService.delete(narrator, hidden);
            else
                HiddenFactionRoleService.removeSpawnable(hidden, factionRole);
        }
        factionRole.faction._roleSet.remove(factionRole.role);
    }

    private static void checkColor(String color) {
        if(color == null)
            throw new TeamSettingsException("Color cannot be null");
        color = color.toUpperCase();
        if(!color.startsWith("#"))
            color = "#" + color;
        if(color.length() != 7)
            throw new TeamSettingsException("Team color must look like #A0B1C2");
        if(color.equals(Constants.A_NEUTRAL))
            throw new TeamSettingsException("Team name is reserved");
        if(!color.matches("#[0-9A-F]+"))
            throw new TeamSettingsException("Color can only contain hex characters");
    }

    public static void nameValidation(Narrator narrator, String name, Faction editedTeam) {
        if(name == null)
            throw new NamingException("Name cannot be null");
        for(String r_name: Faction.RESERVED_NAMES){
            if(r_name.equalsIgnoreCase(name))
                throw new NamingException("This name is reserved.");
        }
        for(Player p: narrator._players){
            if(p.getName().equalsIgnoreCase(name))
                throw new NamingException("Team name is taken by a player");
        }
        for(Hidden hidden: narrator.hiddens){
            if(hidden.getName().equalsIgnoreCase(name))
                throw new NamingException("Team name is taken by a role");
        }
        if(name.length() == 0)
            throw new NamingException("Team name cannot be empty");
        for(Faction faction: narrator.getFactions()){
            if(faction.getColor().equalsIgnoreCase(name))
                throw new NamingException("Team names cannot be colors");
            if(faction.getName().equalsIgnoreCase(name) && faction != editedTeam)
                throw new NamingException("That team name is taken!");
            for(FactionRole m: faction._roleSet.values()){
                if(m.role.getName().equalsIgnoreCase(name))
                    throw new NamingException("Team name is taken by a role.");
            }
        }

        try{
            narrator.nameCheck(name, true);
        }catch(NamingException e){
            if(e.t != editedTeam)
                throw e;
        }
    }
}
