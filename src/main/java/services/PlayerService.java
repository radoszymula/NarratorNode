package services;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.support.Util;
import game.roles.Ghost;
import json.JSONException;
import models.events.PlayerRemovedEvent;
import models.serviceResponse.BotAddResponse;
import models.serviceResponse.PlayerKickResponse;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.User;
import repositories.Connection;
import repositories.PlayerRepo;
import repositories.UserRepo;

public class PlayerService {
    private final static Pattern lastIntPattern = Pattern.compile("[^0-9]+([0-9]+)$");

    public static String getAcceptableName(String name, Narrator narrator) {
        name = Util.stripNonAlphaNumericCharacters(name);
        if(Util.isInt(name))
            name = "guest" + name;
        if(name.length() > Constants.MAX_DB_PLAYER_LENGTH)
            name = "guest";

        if(narrator.acceptablePlayerName(name))
            return name;

        Matcher matcher = lastIntPattern.matcher(name);
        if(matcher.find()){
            String someNumberStr = matcher.group(1);
            int lastNumberInt = Integer.parseInt(someNumberStr) + 1;
            name = name.replace(someNumberStr, Integer.toString(lastNumberInt));
        }else{
            name = name + "2";
        }
        if(!narrator.acceptablePlayerName(name))
            return getAcceptableName(name, narrator);

        return name;
    }

    public static BotAddResponse addBots(long userID, int botCount) {
        User user = UserService.assertIsHost(userID);
        if(user.game.narrator.isInProgress())
            throw new NarratorException("Cannot add bots to a started game.");
        PlayerList players = user.game.addComputers(botCount);
        return new BotAddResponse(user.game, players);
    }

    public static void deletePlayer(long kickerID, long leaverID) {
        User user = NodeSwitch.phoneBook.get(leaverID);
        if(user == null)
            throw new NarratorException("User id not found.");

        if(user.isInLobby())
            throw new NarratorException("User is not in any game.");

        Game game = user.game;
        if(game.hostID != kickerID && kickerID != leaverID)
            throw new NarratorException("Only hosts can use this command.");

        if(user.isInGame() && user.player != null && game.narrator.isInProgress()
                && (user.player.isAlive() || user.player.is(Ghost.class)))
            throw new NarratorException("You cannot leave if you are alive.");

        if(game.narrator.isStarted() && !game.narrator.isFinished() && game.connection != null){
            try{
                PlayerRepo.setExited(game.connection, game.getID(), user.id);
            }catch(SQLException e){
                Util.log(e, "Failed to set player exit");
            }
        }

        String playerName = user.player != null ? user.player.getName() : null;

        if(game.phoneBook.containsValue(game.getHost()) && !game.narrator.isStarted()){
            if(game.phoneBook.size() == 1){
                GameService.delete(game);
                return;
            }
        }else if(game.phoneBook.isEmpty()){
            GameService.delete(game);
            return;
        }else if(game.phoneBook.size() == 1 && game.phoneBook.containsValue(user)){
            GameService.delete(game);
            return;
        }

        try{
            if(!game.narrator.isStarted())
                PlayerRepo.deleteByUserAndGame(game.connection, leaverID, game.getID());
            game.removePlayer(user);
        }catch(SQLException e){
            throw new NarratorException("Failed to leave game due to database exception.");
        }

        try{
            NodeSwitch.serverWrite(PlayerRemovedEvent.request(game, user, playerName));
        }catch(JSONException e){
            Util.log(e, "Failed to send player remove event.");
        }
    }

    public static PlayerKickResponse kick(long userID, long kickedUserID) {
        User user = UserService.assertIsHost(userID);
        for(User kickedUser: user.game.phoneBook.values()){
            if(kickedUser.id != kickedUserID)
                continue;
            deletePlayer(userID, kickedUser.id);
            return new PlayerKickResponse(user.game.getHost(), user.game._setup);
        }
        throw new NarratorException("Cannot locate player with that ID");
    }

    public static Game joinAny(long userID, String playerName) {
        User user = UserService.get(userID);
        Game game;
        try{
            game = NodeSwitch.getGame(userID);
        }catch(SQLException e){
            throw new NarratorException(e.getMessage());
        }
        addToGame(game, user, playerName);
        return game;
    }

    public static Game joinSpecific(long userID, String joinID, String playerName) {
        joinID = joinID.toUpperCase();
        User user = UserService.get(userID);
        Game game = NodeSwitch.idToInstance.get(joinID);
        addToGame(game, user, playerName);
        return game;
    }

    public static void modkill(long userID) {
        User user = UserService.get(userID);
        if(user.player == null)
            throw new NarratorException("User is not participating in any game.");
        if(!user.game.narrator.isInProgress())
            throw new NarratorException("Game is not in progress.");
        if(user.player.isDead())
            throw new NarratorException("Player is already dead.");

        user.player.modkill();
        user.game.endGameIfNoParticipatingPlayersAlive();
    }

    public static void savePlayersToGame(long gameID, Narrator narrator, HashMap<Player, User> playerToUser,
            Connection connection) throws SQLException {
        Long userID, hiddenID, factionRoleID;

        for(Player p: narrator.getAllPlayers()){
            hiddenID = p.initialAssignedRole.assignedHidden.getID();
            factionRoleID = p.initialAssignedRole.assignedRole.id;
            if(playerToUser.containsKey(p) && playerToUser.get(p) != null){
                userID = playerToUser.get(p).id;
                PlayerRepo.updateRoleAssignment(connection, gameID, userID, hiddenID, factionRoleID);
            }else{
                userID = UserRepo.create(connection, false);
                p.databaseID = PlayerRepo.create(connection, gameID, userID, p.getName(), hiddenID, factionRoleID,
                        p.isComputer());
            }
        }
    }

    static void addToGame(Game game, User user, String playerName) {
        if(game == null)
            throw new NarratorException("Game with that ID not found.");
        if(game.isFull()){
            game.getHost().warn(playerName + " tried to join your game.");
            throw new NarratorException("Game is full.");
        }
        if(user.isInGame() && !user.game.narrator.isFinished())
            throw new NarratorException("User is already in a game.");
        if(game.narrator.isStarted())
            throw new NarratorException("Game is already started.");

        try{
            user.joinGame(game, playerName);
            user.player.databaseID = PlayerRepo.create(game.connection, game.getID(), user.id, user.player.getName(),
                    false);
        }catch(SQLException e){
            PlayerService.deletePlayer(user.id, user.id);
            throw new NarratorException("Failed to join game due to database error.");
        }
    }
}
