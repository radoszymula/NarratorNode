package services;

import game.logic.Faction;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.support.rules.AbilityModifier;
import game.roles.Ability;

public class FactionAbilityModifierService {

    public static void modify(Faction faction, Class<? extends Ability> abilityClass, AbilityModifier modifier,
            boolean val) {
        if(!Ability.getAbilityCopy(abilityClass).isBool(modifier))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not a boolean modifier.");
        faction.modifyAbility(modifier, abilityClass, val);
    }

    public static void modify(Faction faction, Class<? extends Ability> abilityClass, AbilityModifier modifier,
            int val) {
        if(!Ability.getAbilityCopy(abilityClass).isInt(modifier))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + modifier.toString() + " is not an integer modifier.");
        faction.modifyAbility(modifier, abilityClass, val);
    }
}
