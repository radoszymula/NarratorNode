package services;

import java.sql.SQLException;
import java.util.Set;

import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import json.JSONException;
import models.events.HostChangeEvent;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.Permission;
import nnode.User;
import repositories.PlayerRepo;
import repositories.ReplayRepo;
import repositories.UserPermissionRepo;

public class ModeratorService {
    public static void create(long userID) {
        User user = NodeSwitch.phoneBook.get(userID);
        if(user == null)
            throw new NarratorException("User id not found.");

        if(user.isInLobby())
            throw new NarratorException("User is not in any game.");

        Game game = user.game;
        if(game.narrator.isFinished())
            return;

        if(user.player == null)
            return;

        if(game.narrator.isInProgress())
            throw new NarratorException("Moderators may not be created midgame.");

        if(game.hostID != userID)
            throw new NarratorException("Only hosts can use this command.");

        try{
            PlayerRepo.deleteByUserAndGame(game.connection, userID, game.getID());
            setModerator(user);
        }catch(SQLException e){
            throw new NarratorException("Could not change host to moderator due to database problem.");
        }
    }

    public static void delete(long userID, String playerName) {
        User user = NodeSwitch.phoneBook.get(userID);
        if(user == null)
            throw new NarratorException("User id not found.");

        if(user.isInLobby())
            throw new NarratorException("User is not in any game.");

        Game game = user.game;
        if(game.narrator.isFinished())
            return;

        if(game.narrator.isStarted())
            throw new NarratorException("You may not stop moderating midgame.");

        if(user.player != null)
            return;

        if(game.isFull())
            throw new NarratorException("Cannot rejoin a full game.");

        removeModerator(user, playerName);
    }

    public static User repick(long userID, String message, Set<Long> activeUserIDs) {
        User repicker = UserService.assertInGame(userID);
        Game game = repicker.game;
        User host = game.getHost();
        try{
            if(repicker != host && UserPermissionRepo.hasPermission(game.connection, userID, Permission.GAME_EDITING)){
                message = repicker.player.getName();
                return repickHost(host, message, activeUserIDs);
            }
        }catch(SQLException e){
            // don't do anything on this SQLException
        }

        if(repicker == host)
            return repickHost(host, message, activeUserIDs);

        try{
            if(UserPermissionRepo.hasPermission(game.connection, host.id, Permission.GAME_EDITING))
                throw new NarratorException("Admins cannot be repicked.");
        }catch(SQLException e){
            throw new NarratorException(
                    "Could not determine whether the current moderator is an admin due to a database error");
        }

        if(!game.repickers.contains(repicker.player))
            game.repickers.add(repicker.player);

        if(game.repickers.size() >= game.phoneBook.size() / 2 + 1)
            return repickHost(host, "", activeUserIDs);

        StringChoice sc = new StringChoice(repicker.player);
        sc.add(repicker.player, "You");
        StringChoice sc2 = new StringChoice(host.player);
        sc2.add(host.player, "you");
        StringChoice have = new StringChoice("has");
        have.add(repicker.player, "have");

        PlayerList webUsers = game.getWebUsers();
        webUsers.remove(repicker.player);
        webUsers.warn(sc, " ", have, " voted to repick ", sc2, ".");
        return game.getHost();
    }

    private static void setModerator(User user) {
        user.game.observers.add(user);
        user.game.phoneBook.remove(user.player);
        user.game.narrator.removePlayer(user.player);
        user.player = null;
        user.game.sendGameState();
    }

    private static void removeModerator(User user, String playerName) {
        try{
            PlayerRepo.create(user.game.connection, user.game.getID(), user.id, playerName, false);
        }catch(SQLException e){
            throw new NarratorException("Could not change host to moderator due to database problem.");
        }

        user.game.observers.remove(user);
        user.game.addPlayer(user, playerName);
    }

    private static User repickHost(User host, String message, Set<Long> activeUserIDs) {
        Game game = host.game;
        PlayerList webUsers = game.getWebUsers();
        Player potential = webUsers.getPlayerByName(message);
        if(potential == null || !activeUserIDs.contains(game.phoneBook.get(potential).id)){
            potential = null;
            Player oldHost = host.player;
            PlayerList options = webUsers.copy().remove(oldHost).shuffle(new Random(), null);
            User user;
            for(Player p: options){
                user = game.phoneBook.get(p);
                if(activeUserIDs.contains(user.id)){
                    potential = p;
                    break;
                }
            }
            if(potential == null && !options.isEmpty())
                potential = new Random().getPlayer(options);
        }
        if(potential != null){
            try{
                ReplayRepo.setHostID(host.game.connection, host.id, host.game.getID());
            }catch(SQLException e){
                throw new NarratorException("Failed to change host due to database exception.");
            }
            game.changeHost(game.phoneBook.get(potential));
            try{
                NodeSwitch.serverWrite(HostChangeEvent.request(game));
            }catch(JSONException e){
                Util.log(e, "Failed to send host change event.");
            }
            game.resetSetupHiddenSpawns();
        }
        return game.getHost();
    }
}
