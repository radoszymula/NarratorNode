package services;

import java.util.List;

import game.logic.Narrator;
import game.logic.exceptions.NarratorException;
import game.roles.Hidden;
import models.FactionRole;

public class HiddenFactionRoleService {

    public static void addSpawnableRoles(Narrator narrator, Hidden hidden, List<FactionRole> roleList) {
        FactionRole[] roles = new FactionRole[roleList.size()];
        for(int i = 0; i < roleList.size(); i++)
            roles[i] = roleList.get(i);
        addSpawnableRoles(narrator, hidden, roles);
    }

    public static void addSpawnableRoles(Narrator narrator, Hidden hidden, FactionRole... factionRoles) {
        for(FactionRole factionRole: factionRoles)
            if(!factionRole.faction._roleSet.values().contains(factionRole))
                throw new NarratorException("This role is not in the setup.");

        for(FactionRole factionRole: factionRoles)
            hidden.factionRolesMap.put(factionRole.id, factionRole);
    }

    public static void removeSpawnable(Hidden hidden, FactionRole factionRole) {
        hidden.factionRolesMap.remove(factionRole.id);
    }
}
