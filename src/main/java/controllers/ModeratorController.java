package controllers;

import java.util.HashSet;
import java.util.Set;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.json_output.UserJson;
import nnode.User;
import services.ModeratorService;

public class ModeratorController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("POST")){
            JSONObject args = request.getJSONObject("body");
            if(args.has("repickTarget"))
                repickHostController(request, response);
            else
                createModeratorController(request, response);
        }else if(request.getString("method").equals("DELETE")){
            deleteModeratorController(request, response);
        }
    }

    private static void createModeratorController(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        ModeratorService.create(userID);
    }

    private static void deleteModeratorController(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        String playerName = args.getString("playerName");
        ModeratorService.delete(userID, playerName);
    }

    private static void repickHostController(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        JSONArray activeUserIDsJson = args.getJSONArray("activeUserIDs");
        String repickTarget = args.getString("repickTarget");
        long userID = args.getLong("userID");
        Set<Long> activeUserIDs = new HashSet<>();
        for(int i = 0; i < activeUserIDsJson.length(); i++)
            activeUserIDs.add(activeUserIDsJson.getLong(i));
        User host = ModeratorService.repick(userID, repickTarget, activeUserIDs);
        response.put("response", UserJson.toJson(host));
    }

}
