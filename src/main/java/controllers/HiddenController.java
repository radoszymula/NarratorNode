package controllers;

import java.util.ArrayList;

import game.roles.Hidden;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.json_output.HiddenJson;
import services.HiddenService;

public class HiddenController {
    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("POST"))
            createHidden(request, response);
    }

    private static void createHidden(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        JSONArray roleIDsArray = args.getJSONArray("factionRoleIDs");
        Long userID = args.getLong("userID");
        String hiddenName = args.getString("name");
        ArrayList<Long> roleIDs = new ArrayList<>();
        for(int i = 0; i < roleIDsArray.length(); i++)
            roleIDs.add(roleIDsArray.getLong(i));

        Hidden hidden = HiddenService.createHidden(userID, hiddenName, roleIDs);
        response.put("response", HiddenJson.toJson(hidden));
    }
}
