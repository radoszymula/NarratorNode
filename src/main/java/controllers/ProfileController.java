package controllers;

import json.JSONException;
import json.JSONObject;
import models.json_output.ProfileJson;
import nnode.User;
import services.UserService;

public class ProfileController {
    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("GET"))
            getProfile(request, response);

    }

    private static void getProfile(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");

        User user = UserService.get(userID);
        response.put("response", ProfileJson.toJson(user));
    }
}
