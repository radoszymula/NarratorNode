package controllers;

import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.json_output.FactionRoleJson;
import services.FactionRoleService;

public class FactionRoleController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("GET"))
            get(request, response);
    }

    public static void get(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long factionRoleID = args.getLong("factionRoleID");

        FactionRole factionRole = FactionRoleService.get(userID, factionRoleID);
        response.put("response", FactionRoleJson.toJson(factionRole));
    }
}
