package controllers;

import json.JSONException;
import json.JSONObject;
import services.VoteService;

public class VoteController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("GET")){
            getVotes(request, response);
        }
    }

    private static void getVotes(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        String joinID = args.getString("joinID");
        JSONObject votes = VoteService.getVotes(joinID);
        response.put("response", votes);
    }
}
