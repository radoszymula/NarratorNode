package controllers;

import game.logic.exceptions.NarratorException;
import game.logic.support.rules.FactionModifier;
import json.JSONException;
import json.JSONObject;
import models.serviceResponse.ModifierServiceResponse;
import services.FactionModifierService;

public class FactionModifierController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("PUT"))
            updateModifier(request, response);
    }

    private static void updateModifier(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        String modifierName = args.getString("name");
        FactionModifier modifier;
        try{
            modifier = FactionModifier.valueOf(modifierName);
        }catch(IllegalArgumentException e){
            throw new NarratorException("Unknown faction modifier name: " + modifierName);
        }
        long userID = args.getLong("userID");
        long factionID = args.getLong("factionID");
        Object value = args.get("value");

        ModifierServiceResponse serviceResponse;
        if(value instanceof Boolean)
            serviceResponse = FactionModifierService.modify(userID, factionID, modifier, (boolean) value);
        else
            serviceResponse = FactionModifierService.modify(userID, factionID, modifier, (int) value);

        JSONObject modifierJSON = new JSONObject();
        modifierJSON.put("name", modifierName);
        modifierJSON.put("value", serviceResponse.value);
        JSONObject updateResponse = new JSONObject();
        updateResponse.put("setupID", serviceResponse.setupID);
        updateResponse.put("modifier", modifierJSON);
        response.put("response", updateResponse);
    }

}
