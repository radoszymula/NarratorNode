package controllers;

import game.logic.exceptions.NarratorException;
import game.logic.support.rules.AbilityModifier;
import json.JSONException;
import json.JSONObject;
import models.serviceResponse.ModifierServiceResponse;
import services.FactionRoleService;

public class FactionRoleAbilityModifierController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("PUT"))
            updateModifier(request, response);
    }

    private static void updateModifier(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        String modifierName = args.getString("name");
        AbilityModifier modifier;
        try{
            modifier = AbilityModifier.valueOf(modifierName);
        }catch(IllegalArgumentException e){
            throw new NarratorException("Unknown role ability modifier name: " + modifierName);
        }
        long userID = args.getLong("userID");
        long factionRoleID = args.getLong("factionRoleID");
        long abilityID = args.getLong("abilityID");
        Object value = args.get("value");

        ModifierServiceResponse serviceResponse = FactionRoleService.updateModifier(userID, factionRoleID, abilityID, modifier, value);

        JSONObject modifierJSON = new JSONObject();
        modifierJSON.put("name", modifierName);
        modifierJSON.put("value", serviceResponse.value);
        JSONObject updateResponse = new JSONObject();
        updateResponse.put("setupID", serviceResponse.setupID);
        updateResponse.put("modifier", modifierJSON);
        response.put("response", updateResponse);
    }
}
