package controllers;

import game.logic.Role;
import json.JSONException;
import json.JSONObject;
import models.json_output.RoleJson;
import services.RoleService;

public class RoleController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("GET"))
            get(request, response);
    }

    public static void get(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long roleID = args.getLong("roleID");

        Role role = RoleService.get(userID, roleID);
        response.put("response", RoleJson.toJson(role));
    }

}
