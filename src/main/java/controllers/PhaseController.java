package controllers;

import json.JSONException;
import json.JSONObject;
import services.PhaseService;

public class PhaseController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("PUT")){
            endPhaseController(request, response);
        }
    }

    private static void endPhaseController(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        String joinID = args.getString("joinID");
        String phaseHash = args.getString("phaseHash");
        PhaseService.endPhase(joinID, phaseHash);
    }
}
