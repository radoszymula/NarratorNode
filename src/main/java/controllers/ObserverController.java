package controllers;

import json.JSONException;
import json.JSONObject;
import services.ObserverService;

public class ObserverController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("POST")){
            createObserverController(request, response);
        }

    }

    private static void createObserverController(JSONObject request, JSONObject response) throws JSONException {
        JSONObject body = request.getJSONObject("body");

        long userID = body.getLong("userID");
        String joinID = body.getString("joinID");

        ObserverService.addObserver(userID, joinID);
    }

}
