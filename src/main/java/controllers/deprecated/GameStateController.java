package controllers.deprecated;

import json.JSONException;
import json.JSONObject;
import models.json_output.SetupJson;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.StateObject;
import nnode.User;
import services.UserService;

public class GameStateController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("GET"))
            bulkUpdateRules(request, response);
    }

    private static void bulkUpdateRules(JSONObject request, JSONObject response) throws JSONException {
        JSONObject body = request.getJSONObject("body");
        long userID = body.getLong(StateObject.userID);
        if(!NodeSwitch.phoneBook.containsKey(userID) || !NodeSwitch.phoneBook.get(userID).isInGame())
            return;
        User user = UserService.get(userID);
        StateObject gameState = Game.getGameState(user.game, user);
        JSONObject gameStateJSON = gameState.construct(user, user.game);
        gameStateJSON.put("setup", SetupJson.toJson(user.game._setup));
        response.put("response", gameStateJSON);
    }
}
