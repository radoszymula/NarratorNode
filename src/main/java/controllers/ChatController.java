package controllers;

import java.util.Arrays;
import java.util.List;

import json.JSONException;
import json.JSONObject;
import services.ChatService;

public class ChatController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("GET")){
            getChatController(request.getJSONObject("body"), response);
        }
    }

    private static void getChatController(JSONObject args, JSONObject response) throws JSONException {
        long userID = args.getLong("userID");
        JSONObject chatsObject = ChatService.getUserChats(userID);
        if(chatsObject == null){
            List<String> errors = Arrays.asList("Failed to retrieve chats.");
            response.put("errors", errors);
        }else
            response.put("response", chatsObject);
    }

}
