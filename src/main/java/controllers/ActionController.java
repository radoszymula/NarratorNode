package controllers;

import java.util.ArrayList;
import java.util.List;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.serviceResponse.ActionResponse;
import services.ActionService;

public class ActionController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("PUT"))
            submitActionController(request, response);
        else if(request.getString("method").equals("DELETE"))
            deleteActionController(request);
    }

    private static void submitActionController(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        ActionResponse actionResponse;
        if(args.has("message"))
            actionResponse = ActionService.submitAction(userID, args.getString("message"));
        else{
            String command = args.getString("command");
            String option1 = args.has("option") ? args.getString("option") : null;
            String option2 = args.has("option2") ? args.getString("option2") : null;
            String option3 = args.has("option3") ? args.getString("option3") : null;
            Integer replacingAction = args.has("oldAction") ? args.getInt("oldAction") : null;
            boolean unvoting = args.has("unvote") && args.getBoolean("unvote");
            boolean skipVoting = args.has("skip day") && args.getBoolean("skip day");
            JSONArray jTargets = args.getJSONArray("targets");
            List<String> targets = new ArrayList<>();
            for(int i = 0; i < jTargets.length(); i++)
                targets.add(jTargets.getString(i));
            actionResponse = ActionService.submitAction(userID, command, targets, option1, option2, option3,
                    replacingAction, unvoting, skipVoting);
        }
        JSONObject actionResponseJson = new JSONObject();
        actionResponseJson.put("actions", actionResponse.actions);
        actionResponseJson.put("isSkipping", actionResponse.isSkipping);
        actionResponseJson.put("voteInfo", actionResponse.voteInfo);
        response.put("response", actionResponseJson);
    }

    private static void deleteActionController(JSONObject request) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        int actionIndex = args.getInt("actionIndex");
        String command = args.getString("command");
        ActionService.deleteAction(userID, command, actionIndex);
    }
}
