package controllers;

import game.logic.exceptions.NarratorException;
import game.logic.support.rules.SetupModifier;
import json.JSONException;
import json.JSONObject;
import models.serviceResponse.ModifierServiceResponse;
import services.SetupModifierService;

public class SetupModifierController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("PUT")){
            editSetupModifier(request, response);
        }
    }

    private static void editSetupModifier(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        Object value = args.get("value");
        String modifierName = args.getString("id");

        SetupModifier setupModifierID;
        try{
            setupModifierID = SetupModifier.valueOf(modifierName);
        }catch(IllegalArgumentException e){
            throw new NarratorException("Unknown faction modifier name: " + modifierName);
        }

        ModifierServiceResponse serviceResponse = SetupModifierService.modify(userID, setupModifierID, value);

        JSONObject modifierJSON = new JSONObject();
        modifierJSON.put("name", modifierName);
        modifierJSON.put("value", serviceResponse.value);
        JSONObject updateResponse = new JSONObject();
        updateResponse.put("setupID", serviceResponse.setupID);
        updateResponse.put("modifier", modifierJSON);
        response.put("response", updateResponse);
    }
}
