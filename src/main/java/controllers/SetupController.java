package controllers;

import java.sql.SQLException;
import java.util.List;

import game.logic.Narrator;
import game.logic.exceptions.NarratorException;
import game.setups.Default;
import game.setups.Setup;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.json_output.GameJson;
import models.json_output.SetupJson;
import models.json_output.SetupOverview;
import nnode.Game;
import repositories.Connection;
import services.SetupService;

public class SetupController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("GET")){
            getSetups(request, response);
        }else if(request.getString("method").equals("PUT")){
            setSetup(request, response);
        }else if(request.getString("method").equals("POST")){
            createSetup(request, response);
        }
    }

    private static void createSetup(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        JSONObject setupRequest = args.getJSONObject("setup");
        String setupName = setupRequest.getString("name");

        Setup setup = Setup.GetSetup(new Narrator(), Default.KEY);
        setup.name = setupName;
        setup.ownerID = userID;

        Connection connection = null;
        try{
            connection = new Connection();
            SetupService.create(connection, setup);
        }catch(SQLException | NarratorException e){
            if(connection != null)
                connection.close();
            throw new NarratorException("Unable to save setup to database.");
        }
        response.put("response", SetupJson.toJson(setup));
    }

    private static void getSetups(JSONObject request, JSONObject response) throws JSONException {
        JSONArray setupsResponse = new JSONArray();
        JSONObject setup;
        response.put("response", setupsResponse);
        Long userID = null;
        if(request.has("body")){
            JSONObject args = request.getJSONObject("body");
            if(args.has("setupID")){
                getSetup(args.getLong("setupID"), response);
                return;
            }
            if(!args.isNull("userID"))
                userID = args.getLong("userID");
        }
        List<Setup> userSetups = SetupService.getSetups(userID);
        for(Setup userSetup: userSetups){
            setup = SetupOverview.toJson(userSetup);
            setupsResponse.put(setup);
        }
    }

    private static void getSetup(long setupID, JSONObject response) throws JSONException {
        Setup setup = SetupService.getSetup(setupID);
        if(setup == null)
            throw new NarratorException("Setup not found with that id: " + setupID);
        response.put("response", SetupJson.toJson(setup));
    }

    private static void setSetup(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        Game game;
        if(args.has("setupID")){
            long setupID = args.getLong("setupID");
            if(args.has("joinID")){
                String joinID = args.getString("joinID");
                game = SetupService.setSetup(joinID, setupID);
            }else{
                long userID = args.getLong("userID");
                game = SetupService.setSetup(userID, setupID);
            }
        }else{
            String setupKey = args.getString("setupKey");
            if(args.has("joinID")){
                String joinID = args.getString("joinID");
                game = SetupService.setSetup(joinID, setupKey);
            }else{
                long userID = args.getLong("userID");
                game = SetupService.setSetup(userID, setupKey);
            }
        }
        response.put("response", GameJson.toJson(game));
    }

}
