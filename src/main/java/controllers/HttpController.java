package controllers;

import controllers.deprecated.GameStateController;
import game.logic.exceptions.NarratorException;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import nnode.CondorcetController;
import nnode.NodeSwitch;
import nnode.StateObject;

public class HttpController {

    public static JSONObject handleHTTPRequest(JSONObject request) throws JSONException {
        String url = request.getString("url");
        String requestID = request.getString(StateObject.requestID);
        JSONObject response = new JSONObject();
        response.put(StateObject.requestID, requestID);
        JSONArray errors = new JSONArray();
        try{
            switch (url){
            case "actions":
                ActionController.parse(request, response);
                break;
            case "chats":
                ChatController.parse(request, response);
                break;
            case "condorcet":
                CondorcetController.parse(request, response);
                break;
            case "factions":
                FactionController.parse(request, response);
                break;
            case "factionModifiers":
                FactionModifierController.parse(request, response);
                break;
            case "factionRoles":
                FactionRoleController.parse(request, response);
                break;
            case "factionRoleAbilityModifiers":
                FactionRoleAbilityModifierController.parse(request, response);
                break;
            case "factionRoleModifiers":
                FactionRoleModifierController.parse(request, response);
                break;
            case "games":
                GameController.parse(request, response);
                break;
            case "hiddens":
                HiddenController.parse(request, response);
                break;
            case "moderators":
                ModeratorController.parse(request, response);
                break;
            case "observers":
                ObserverController.parse(request, response);
                break;
            case "phases":
                PhaseController.parse(request, response);
                break;
            case "players":
                PlayerController.parse(request, response);
                break;
            case "profiles":
                ProfileController.parse(request, response);
                break;
            case "roles":
                RoleController.parse(request, response);
                break;
            case "setupModifiers":
                SetupModifierController.parse(request, response);
                break;
            case "setupHiddens":
                SetupHiddenController.parse(request, response);
                break;
            case "setups":
                SetupController.parse(request, response);
                break;
            case "votes":
                VoteController.parse(request, response);
                break;

            case "getGameState":
                GameStateController.parse(request, response);
                break;

            default:
                throw new Exception("URL not matched.");
            }
        }catch(NarratorException e){
            errors.put(e.getLocalizedMessage());
            response.put("response", new JSONObject());
        }catch(Throwable e){
            e.printStackTrace();
            errors.put(e.getLocalizedMessage());
            response.put("originalRequest", request);
            response.put("url", url);
            response.put("criticalError", true);
        }
        response.put("errors", errors);
        return HttpController.httpResponse(response);
    }

    private static JSONObject httpResponse(JSONObject jo) {
        try{
            jo.put(StateObject.httpResponse, true);
        }catch(JSONException e){
            e.printStackTrace();
        }
        NodeSwitch.nodePush(jo);
        return jo;
    }

}
