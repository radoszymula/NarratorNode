package controllers;

import json.JSONException;
import json.JSONObject;
import models.json_output.SetupHiddenJson;
import models.serviceResponse.SetupHiddenAddResponse;
import services.SetupHiddenService;

public class SetupHiddenController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("POST"))
            createSetupHidden(request, response);
        else if(request.getString("method").equals("PUT"))
            editSetupHidden(request, response);
        else if(request.getString("method").equals("DELETE"))
            deleteHidden(request, response);
    }

    private static void createSetupHidden(JSONObject request, JSONObject response) throws JSONException {
        JSONObject body = request.getJSONObject("body");
        long userID = body.getLong("userID");
        long hiddenID = body.getLong("hiddenID");
        SetupHiddenAddResponse setupHiddenAddResponse = SetupHiddenService.addSetupHidden(userID, hiddenID, false);
        JSONObject setupHiddenAddJsonResponse = SetupHiddenJson.toJson(setupHiddenAddResponse.setupID,
                setupHiddenAddResponse.setupHidden);
        setupHiddenAddJsonResponse.put("gameID", setupHiddenAddResponse.gameID);
        response.put("response", setupHiddenAddJsonResponse);
    }

    private static void editSetupHidden(JSONObject request, JSONObject response) throws JSONException {
        JSONObject body = request.getJSONObject("body");
        long userID = body.getLong("userID");
        long setupHiddenID = body.getLong("setupHiddenID");
        boolean isExposed = body.getBoolean("isExposed");
        SetupHiddenService.setExposed(userID, setupHiddenID, isExposed);
    }

    private static void deleteHidden(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long setupHiddenID = args.getLong("setupHiddenID");

        long gameID = SetupHiddenService.deleteSetupHidden(userID, setupHiddenID);
        JSONObject deleteResponse = new JSONObject();
        deleteResponse.put("gameID", gameID);
        response.put("response", deleteResponse);
    }

}
