package controllers;

import json.JSONException;
import json.JSONObject;
import services.FactionService;

public class FactionController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("DELETE"))
            deleteFaction(request, response);
    }

    private static void deleteFaction(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long factionID = args.getLong("factionID");

        long setupID = FactionService.deleteFaction(userID, factionID);
        JSONObject deleteResponse = new JSONObject();
        response.put("setupID", setupID);
        response.put("response", deleteResponse);
    }

}
