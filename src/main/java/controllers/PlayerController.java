package controllers;

import json.JSONException;
import json.JSONObject;
import models.json_output.GameJson;
import models.json_output.PlayerJson;
import models.json_output.SetupJson;
import models.json_output.UserJson;
import models.serviceResponse.BotAddResponse;
import models.serviceResponse.PlayerKickResponse;
import nnode.Game;
import services.PlayerService;

public class PlayerController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("POST")){
            createController(request, response);
        }else if(request.getString("method").equals("PUT")){
            modKillController(request, response);
        }else if(request.getString("method").equals("DELETE")){
            deletePlayerController(request, response);
        }
    }

    private static void addBotsController(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        int botCount = args.getInt("botCount");

        BotAddResponse botAddResponse = PlayerService.addBots(userID, botCount);
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("setup", SetupJson.toJson(botAddResponse.setup));
        jsonResponse.put("players", PlayerJson.toJson(botAddResponse.players, botAddResponse.game));
        response.put("response", jsonResponse);
    }

    private static void createController(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        if(args.has("botCount")){
            addBotsController(request, response);
            return;
        }

        long userID = args.getLong("userID");
        String playerName = args.getString("playerName");

        Game game;
        if(args.has("joinID"))
            game = PlayerService.joinSpecific(userID, args.getString("joinID"), playerName);
        else
            game = PlayerService.joinAny(userID, playerName);
        response.put("response", GameJson.toJson(game));
    }

    private static void deletePlayerController(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long kickerID = args.getLong("kickerID");
        if(args.has("playerID")){
            long kickedPlayerID = args.getLong("playerID");
            PlayerKickResponse kickResponse = PlayerService.kick(kickerID, kickedPlayerID);
            JSONObject leaveResponse = new JSONObject();
            leaveResponse.put("host", UserJson.toJson(kickResponse.host));
            leaveResponse.put("setup", SetupJson.toJson(kickResponse.setup));
            response.put("response", leaveResponse);
        }else{
            long leaverID = args.getLong("leaverID");
            PlayerService.deletePlayer(kickerID, leaverID);
        }
    }

    private static void modKillController(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");

        long userID = args.getLong("userID");
        PlayerService.modkill(userID);
    }

}
