package controllers;

import java.util.Collection;
import java.util.Iterator;

import game.logic.support.Util;
import game.logic.support.rules.RuleBool;
import game.logic.support.rules.RuleInt;
import game.logic.support.rules.SetupModifier;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.json_output.GameJson;
import models.schemas.GameSchema;
import nnode.Game;
import services.GameService;

public class GameController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        if(request.getString("method").equals("GET")){
            if(args.has("joinID"))
                getGameByJoinID(request, response);
            else if(args.has("setupID"))
                getGameBySetupID(request, response);
            else if(args.has("gameID"))
                getGameByID(request, response);
            else
                getAllGames(response);
        }else if(request.getString("method").equals("POST")){
            if(args.has("start"))
                startGame(request, response);
            else
                createGame(request, response);
        }else if(request.getString("method").equals("DELETE"))
            deleteGame(request);
    }

    private static void createGame(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        GameSchema gameSchema = new GameSchema();

        JSONObject rulesJson = args.getJSONObject("rules");
        Iterator<?> iterator = rulesJson.keys();
        String modifierStr;
        Object value;
        SetupModifier modifier;
        while (iterator.hasNext()){
            modifierStr = iterator.next().toString();
            value = rulesJson.get(modifierStr);
            modifier = Util.getEnumFromString(SetupModifier.class, modifierStr);

            if(SetupModifier.IsIntModifier(modifier) && value instanceof Integer)
                gameSchema.rules.put(modifier, new RuleInt(modifier, (Integer) value));
            else if(SetupModifier.IsBoolModifier(modifier) && value instanceof Boolean)
                gameSchema.rules.put(modifier, new RuleBool(modifier, (Boolean) value));
        }

        long userID = args.getLong("userID");
        if(args.has("setupKey"))
            gameSchema.setupKey = args.getString("setupKey");
        else
            gameSchema.setupID = args.getLong("setupID");
        gameSchema.isPublic = args.getBoolean("isPublic");
        gameSchema.hostName = args.getString("hostName");

        Game game = GameService.create(userID, gameSchema);
        response.put("response", GameJson.toJson(game));
    }

    private static void deleteGame(JSONObject request) throws JSONException {
        JSONObject args = request.getJSONObject("body");

        String joinID = args.getString("joinID");
        GameService.delete(joinID);
    }

    private static void getAllGames(JSONObject response) throws JSONException {
        Collection<Game> instances = GameService.getAllGames();
        JSONArray responseArray = new JSONArray();
        JSONObject instanceJSON;
        for(Game instance: instances){
            if(instance.getHost() == null){
                System.err.println(instance.hostID);
            }else{
                instanceJSON = GameJson.toJson(instance);
                responseArray.put(instanceJSON);
            }
        }
        response.put("response", responseArray);
    }

    private static void getGameBySetupID(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long setupID = args.getLong("setupID");

        Game game = GameService.getBySetupID(setupID);
        JSONObject instanceJSON;
        if(game == null)
            instanceJSON = new JSONObject();
        else
            instanceJSON = GameJson.toJson(game);

        response.put("response", instanceJSON);
    }

    private static void getGameByID(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long gameID = args.getLong("gameID");

        Game game = GameService.getByID(gameID);
        JSONObject instanceJSON;
        if(game == null)
            instanceJSON = new JSONObject();
        else
            instanceJSON = GameJson.toJson(game);

        response.put("response", instanceJSON);
    }

    private static void getGameByJoinID(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        String joinID = args.getString("joinID");

        Game game = GameService.getByJoinID(joinID);
        JSONObject instanceJSON;
        if(game == null)
            instanceJSON = new JSONObject();
        else
            instanceJSON = GameJson.toJson(game);

        response.put("response", instanceJSON);
    }

    private static void startGame(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        Game game = GameService.start(userID);
        response.put("response", GameJson.toJson(game));
    }
}
