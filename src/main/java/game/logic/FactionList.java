package game.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import game.roles.Ability;

public class FactionList implements FactionGroup, Iterable<Faction> {

    private ArrayList<Faction> teams;

    public FactionList(Faction... input_teams) {
        this.teams = new ArrayList<>();
        for(Faction t: input_teams){
            teams.add(t);
        }
    }

    public void setPriority(int i) {
        for(Faction t: teams){
            t.setPriority(i);
        }
    }

    public HashMap<Faction, Player> getAbilityControllers(int ability) {
        HashMap<Faction, Player> abilityControllers = new HashMap<>();
        for(Faction t: teams){
            if(t.size() < 2)
                continue;
            abilityControllers.put(t, t.getCurrentController(ability));
        }
        return abilityControllers;
    }

    public void add(Faction t) {
        teams.add(t);
    }

    public void add(FactionList teams) {
        for(Faction t: this)
            teams.add(t);
    }

    public boolean isEmpty() {
        return teams.isEmpty();
    }

    @Override
    public Iterator<Faction> iterator() {
        return teams.iterator();
    }

    public String getName() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < teams.size(); i++){
            if(i != 0)
                sb.append(", ");
            sb.append(teams.get(i).getName());
        }
        return sb.toString();
    }

    public int size() {
        return teams.size();
    }

    public Faction get(int i) {
        return teams.get(i);
    }

    public boolean hasAbility(int mainAbility) {
        for(Faction t: teams){
            if(t.hasAbility(mainAbility))
                return true;
        }
        return false;
    }

    public Faction getFactionWithAbility(int mainAbility) {
        Ability a;
        for(Faction t: teams){
            a = t.getAbility(mainAbility);
            if(a != null)
                return t;
        }
        return null;
    }

    public boolean contains(Faction team) {
        return teams.contains(team);
    }

    public PlayerList getFactionmates() {
        PlayerList pl = new PlayerList();
        for(Faction t: teams){
            pl.add(t.getMembers());
        }
        return pl;
    }

    public boolean hasTeamAbilities() {
        for(Faction t: teams){
            if(t.hasSharedAbilities())
                return true;
        }
        return false;
    }

    public ArrayList<Ability> getAbilities() {
        ArrayList<Ability> ret = new ArrayList<>();
        for(Faction t: teams){
            if(!t.hasSharedAbilities())
                continue;
            for(Ability a: t.getAbilities())
                ret.add(a);
        }
        return ret;
    }

    public FactionList filterKnowsTeam() {
        FactionList tl = new FactionList();
        for(Faction t: this){
            if(t.knowsTeam())
                tl.add(t);
        }
        return tl;
    }

}
