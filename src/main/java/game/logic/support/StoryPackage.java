package game.logic.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.exceptions.StoryReaderError;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.Hidden;
import models.Command;
import models.FactionRole;
import models.schemas.FactionAbilityModifierSchema;
import models.schemas.FactionAbilitySchema;
import models.schemas.FactionModifierSchema;
import models.schemas.FactionSchema;
import models.schemas.GameOverviewSchema;
import models.schemas.HiddenSchema;
import models.schemas.PlayerSchema;
import models.schemas.RoleSchema;
import models.schemas.SetupHiddenSchema;
import services.FactionService;
import services.HiddenService;
import services.RoleService;
import services.SetupHiddenService;
import util.LookupUtil;

public class StoryPackage { // Rename to replay

    public Narrator narrator = new Narrator();
    public String instanceID;
    public boolean isPrivate;
    public long hostID;
    public long replayID;
    public HashMap<Player, Long> phoneBook;
    public HashMap<String, RolePackage> assignedRoles;
    public ArrayList<Command> commands;

    public StoryPackage(long replayID) {
        this.replayID = replayID;
        phoneBook = new HashMap<>();
        commands = new ArrayList<>();
        assignedRoles = new HashMap<>();
    }

    private void addPlayer(Player p, Long userID) {
        if(userID != null)
            phoneBook.put(p, userID);
        else
            p.setComputer();
    }

    public static void getSetupInfo(StoryReader storyReader, Narrator narrator) {
        HashMap<SetupModifier, Rule> rules = storyReader.getRules();
        if(rules == null || rules.isEmpty())
            return;

        Rule rule;
        for(SetupModifier key: rules.keySet()){
            rule = rules.get(key);
            if(rule.isInt())
                narrator.setRule(key, rule.getIntVal());
            else
                narrator.setRule(key, rule.getBoolVal());
        }
        List<FactionSchema> teamInfo = storyReader.getTeamInfo();

        // create factions
        Faction team;
        for(FactionSchema faction: teamInfo){
            team = FactionService.createFaction(narrator, faction.color, faction.name, faction.description);
            team.id = faction.id;

            if(faction.modifiers != null)
                for(FactionModifierSchema fRule: faction.modifiers)
                    team.setRule(fRule.createObject());

            if(faction.abilities == null)
                continue;
            for(FactionAbilitySchema ability: faction.abilities.values()){
                team.addAbility(Ability.CREATOR(ability.name));

                if(ability.modifiers == null)
                    continue;
                for(FactionAbilityModifierSchema modifier: ability.modifiers){
                    if(modifier.isInt()){
                        team.modifyAbility(AbilityModifier.valueOf(modifier.name.toUpperCase()),
                                Ability.getAbilityClass(ability.name), modifier.intValue);
                    }else{
                        team.modifyAbility(AbilityModifier.valueOf(modifier.name.toUpperCase()),
                                Ability.getAbilityClass(ability.name), modifier.boolValue);
                    }
                }
            }
        }

        storyReader.setEnemies(narrator);
        storyReader.setSheriffDetectables(narrator);

        Map<Long, RoleSchema> idToRole = storyReader.getRoles();
        for(RoleSchema role: idToRole.values())
            RoleService.createRole(narrator, role);

        FactionService.createFactionRoles(narrator, storyReader.getFactionRoles());

        HashMap<Long, Hidden> idToHidden = new HashMap<>();
        List<HiddenSchema> hiddens = storyReader.getHiddens();

        FactionRole factionRole;
        Hidden hidden;
        List<FactionRole> roles = new ArrayList<>();
        for(HiddenSchema hiddenObj: hiddens){
            roles.clear();
            for(Long factionRoleID: hiddenObj.factionRoleIDs){
                factionRole = LookupUtil.findFactionRole(narrator, factionRoleID);
                roles.add(factionRole);
            }
            hidden = HiddenService.createHidden(narrator, hiddenObj.name, roles);
            idToHidden.put(hiddenObj.id, hidden);
            hidden.setID(hiddenObj.id);
        }

        ArrayList<SetupHiddenSchema> setupRoles = storyReader.getSetupHiddens();
        for(SetupHiddenSchema setupHidden: setupRoles){
            SetupHiddenService.addSetupHidden(narrator, idToHidden.get(setupHidden.hiddenID),
                    setupHidden.isExposed).id = setupHidden.id;
        }
    }

    public static StoryPackage deserialize(StoryReader sr, long replayID) {
        StoryPackage sp = new StoryPackage(replayID);
        Narrator narrator = sp.narrator;

        getSetupInfo(sr, narrator);

        GameOverviewSchema gameOverviewSchema = sr.getGameOverview();
        narrator.setSeed(gameOverviewSchema.seed);
        sp.instanceID = gameOverviewSchema.instanceID;
        sp.isPrivate = gameOverviewSchema.isPrivate;
        sp.hostID = gameOverviewSchema.hostID;

        Player new_player;
        RolePackage assignedRole;
        for(PlayerSchema p: sr.getPlayers()){
            new_player = narrator.addPlayer(p.name);
            new_player.databaseID = p.id;
            sp.addPlayer(new_player, p.userID);

            if(p.isExited)
                new_player.setComputer();
            assignedRole = sp.getAssignedRole(p);
            sp.assignedRoles.put(p.name, assignedRole);
        }

        sp.commands = sr.getCommands();

        return sp;
    }

    private RolePackage getAssignedRole(PlayerSchema pp) {
        for(Hidden hidden: narrator.hiddens){
            if(hidden.getID() != pp.hiddenID)
                continue;
            for(FactionRole factionRole: hidden.getFactionRoles()){
                if(factionRole.id == pp.factionRoleID)
                    return new RolePackage(hidden, factionRole);
            }
        }
        throw new StoryReaderError("Couldn't find assigned role");
    }

    public static CustomRoleAssigner GetRoleAssigner(final StoryPackage sp) {
        return new CustomRoleAssigner() {

            @Override
            public void assign() {
                sp.narrator.getRandom().reset();
                for(Player p: sp.narrator._players.sortByID()){
                    sp.assignedRoles.get(p.getID()).assign(p);
                }
            }

        };
    }

    public Throwable runCommands() {
        if(!narrator.isStarted()){
            narrator.altRoleAssignment = GetRoleAssigner(this);
            narrator.startGame();
        }
        Map<Long, Player> map = narrator._players.getDatabaseMap();
        CommandHandler ch = new CommandHandler(narrator);
        Command command;
        for(int i = 0; i < commands.size(); i++){
            command = commands.get(i);
            try{
                ch.parseCommand(map.get(command.playerID), command.text);
            }catch(Error | Exception e){
                Util.log("Game id : " + this.replayID);
                e.printStackTrace();
                return e;
            }
        }
        return null;
    }

    public boolean onlyComputers() {
        for(Player p: phoneBook.keySet()){
            if(!p.isComputer())
                return false;
        }
        return true;
    }

}
