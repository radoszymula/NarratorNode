package game.logic.support.action;

import java.util.ArrayList;

import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.event.SelectionMessage;
import game.logic.Faction;
import game.logic.FactionList;
import game.logic.MemberList;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.ActionException;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.UnknownRoleException;
import game.logic.exceptions.UnknownTeamException;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.StringChoice;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.AbilityList;
import game.roles.BreadAbility;
import game.roles.Burn;
import game.roles.CultLeader;
import game.roles.DayResolveRole;
import game.roles.Disguiser;
import game.roles.Driver;
import game.roles.ElectroManiac;
import game.roles.FactionSend;
import game.roles.Framer;
import game.roles.Ghost;
import game.roles.GraveDigger;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Joker;
import game.roles.Operator;
import game.roles.ProxyKill;
import game.roles.Spy;
import game.roles.Tailor;
import game.roles.Thief;
import game.roles.Veteran;
import game.roles.Visit;
import game.roles.Witch;
import game.roles.support.Bread;
import game.roles.support.ElectrocutionException;
import game.roles.support.Vest;
import models.enums.GamePhase;

public class Action {

    public Player owner;
    public PlayerList _targets;
    private String option, option2, option3;
    public int ability;
    public boolean isSubmittedByOwner = true;

    private boolean isProxied;

    public Action(Player owner, int ability) {
        this(owner, new PlayerList(), ability, (String) null, null);
    }

    public Action(Player owner, int ability, Player... target) {
        this(owner, new PlayerList(target), ability, (String) null, null);
    }

    public Action(Player owner, Player target, int ability, String option) {
        this(owner, new PlayerList(target), ability, option, null);
    }

    public Action(Player owner, int ability, String opt1, String opt2, Player... p1) {
        this(owner, Player.list(p1), ability, opt1, opt2);
    }

    public Action(Player owner, PlayerList target, int ability, String option, String option2) {
        this(owner, target, ability, option, option2, null);
    }

    public Action(Player owner, PlayerList target, int ability, Option o, Option o2) {
        this(owner, target, ability, o, o2, null);
    }

    public Action(Player owner, PlayerList target, int ability, Option o, Option o2, Option o3) {
        this(owner, target, ability, o == null ? null : o.getValue(), o2 == null ? null : o2.getValue(),
                o3 == null ? null : o3.getValue());
    }

    public Action(Player owner, PlayerList target, int ability, String option, String option2, String option3) {
        if(owner == null)
            throw new NullPointerException();
        this.owner = owner;
        this._targets = target.copy();
        this.ability = ability;
        setOption(option);
        setOption2(option2);
        setOption3(option3);

        isProxied = false;
    }

    public Action setProxied() {
        this.isProxied = true;
        return this;
    }

    public Action setNotSubmittedByOwner() {
        this.isSubmittedByOwner = false;
        return this;
    }

    public ArrayList<Object> getInsteadPhrase() {
        return getAbility().getInsteadPhrase(this);
    }

    public Action cancel(int actionPosition, boolean setCommand) {
        if(actionPosition == -1)
            throw new IllegalActionException("Action index out of bounds");
        SelectionMessage e = new SelectionMessage(owner, false, true);
        if(owner.narrator.nightPhase != GamePhase.NIGHT_ACTION_PROCESSING && setCommand)
            owner.narrator.getEventManager().addCommand(owner, Constants.CANCEL, (actionPosition + 1) + "");

        StringChoice sc = new StringChoice(owner);
        sc.add(owner, "You");

        e.add(sc, " decided not to ");
        ArrayList<Action> toCancel = new ArrayList<>();
        toCancel.add(this);
        e.add(getAbility().getActionDescription(toCancel));
        e.add(".");
        e.dontShowPrivate();
        e.pushOut();

        return this;
    }

    public boolean isSameAbility(int ability) {
        return this.ability == ability;
    }

    public boolean isSameAbility(Action new_action) {
        return new_action.ability == this.ability;

    }

    public boolean isTargeting(PlayerList targets, int ability) {
        if(ability != this.ability)
            return false;
        if(this._targets.size() != targets.size())
            return false;

        if(is(Driver.COMMAND))
            return this._targets.equals(targets);

        for(int i = 0; i < targets.size(); i++){
            if(!targets.get(i).getName().equals(this._targets.get(i).getName()))
                return false;
        }
        return true;
    }

    public boolean has(PlayerList target, int ability) {
        if(ability != this.ability)
            return false;
        PlayerList x1 = target;
        PlayerList x2 = this._targets;
        if(is(Driver.COMMAND)){
            x1 = x1.copy().sortByName();
            x2 = x2.copy().sortByName();
        }
        return x1.equals(x2);
    }

    public boolean visitedHome() {
        if(getTarget() == owner)
            return true;
        return false;
    }

    private boolean isInvalid = false;

    public void setInvalid() {
        if(ability != Ability.MURDER)// aka proxy killing
            isInvalid = true;
    }

    ArrayList<String> actualTargets;

    public void saveIntendedTargets() {
        if(_targets == null)
            return;
        actualTargets = new ArrayList<>();
        for(Player p: _targets)
            actualTargets.add(p.getName());
    }

    private PlayerList actuallyTargeted;

    public PlayerList getActualTargets() {
        return actuallyTargeted;
    }

    public void doNightAction() {
        if(isInvalid)
            return;
        actuallyTargeted = _targets.copy();
        boolean tNull = (_targets == null);

        boolean isDriverAction = is(Driver.COMMAND, Operator.COMMAND, ProxyKill.COMMAND);
        if(!tNull){
            actuallyTargeted = _targets.copy();
            _targets.clear();
            for(Player p: actuallyTargeted){
                if(p.disguisedPrevName != null)
                    p = owner.narrator.getPlayerByName(p.disguisedPrevName);
                if(isDriverAction)
                    _targets.add(p);
                else if(p.getHouseTarget() != null)
                    _targets.add(p.getHouseTarget());
            }
        }

        Ability r = getAbility();

        if(r == null || r.getRealCharges() == 0 || Action.isIllegalRetarget(this))
            Ability.NoNightActionVisit(this);
        else
            r.doNightAction(this);

        if(!tNull)
            _targets = actuallyTargeted;
        actuallyTargeted = null;
    }

    public void completeNightAction() {
        if(owner.getLives() >= 0 || (owner.getRealAutoVestCount() > 0 && owner.isAlive())){
            attemptCompletion();
        }else if(!owner.hasInjuriesFromPreKillingPhase() && owner.narrator.nightPhase == GamePhase.KILLING){
            attemptCompletion();
        }else if(owner.isDead() && !owner.isExhumed()){
            attemptCompletion();
        }else if(is(Disguiser.MAIN_ABILITY) && owner.getLives() > 0){
            attemptCompletion();
        }else{
            owner.narrator.addActionStack(this);
        }

    }

    public ArrayList<Action> getSameActions(ActionList actions) {
        ArrayList<Action> list = new ArrayList<Action>();
        for(Action a: actions){
            if(a.isSameAbility(this))
                list.add(a);
        }

        return list;
    }

    public boolean isDoubleTargeter() {
        return false;
    }

    public boolean isTargeting(Player target2) {
        return _targets.contains(target2);
    }

    private PlayerList witchFuck = new PlayerList();

    public Action witchTouch(Player witch) {
        if(owner.narrator.getBool(SetupModifier.WITCH_FEEDBACK))
            new Feedback(owner, Witch.WITCH_FEEDBACK);
        witchFuck.add(witch);
        return this;
    }

    public boolean wasChanged(Player witch) {
        return witch.in(witchFuck);
    }

    public boolean isArsonBurn() {
        return ability == Burn.MAIN_ABILITY;
    }

    public boolean isEmpty() {
        return _targets.isEmpty();
    }

    public boolean visited(Player target) {
        if(target == null || this._targets == null)
            return false;
        if(owner.is(Driver.class)){
            return target.in(_targets);
        }
        if(_targets.getFirst() == null)
            return false;
        return _targets.getFirst().getAddress() == target;
    }

    public void cleanup() {
        if(is(Vest.COMMAND))
            _targets.clear();
        if(is(Veteran.COMMAND))
            _targets.clear();
    }

    @Override
    public String toString() {
        return "\nowner: " + owner + "\ntarget: " + _targets + "\nability: " + getCommand();
    }

    public PlayerList getTargets() {
        if(_targets == null)
            return new PlayerList();
        return _targets.copy();
    }

    public ArrayList<String> getIntendedTargets() {
        if(_targets == null && actualTargets == null)
            return new ArrayList<>();
        if(_targets == null)
            return actualTargets;
        return _targets.getNamesToStringList();
    }

    // join used for jailor
    public void addTarget(Player p) {
        if(!owner.is(Jailor.class) || ability != Jailor.EXECUTE_)
            throw new IllegalActionException();
        _targets.add(p);
    }

    private boolean isCompleted = false;

    public boolean isCompleted() {
        return isCompleted;
    }

    public void markCompleted() {
        this.isCompleted = true;
    }

    public Player getTarget() {
        if(_targets == null || _targets.isEmpty())
            return null;
        return _targets.getFirst();
    }

    public String getIntendedTarget() {
        if(actualTargets != null && actualTargets.isEmpty())
            return null;
        else if(actualTargets != null)
            return actualTargets.get(0);
        else if(_targets == null || _targets.isEmpty())
            return null;
        return _targets.getFirst().getName();
    }

    // used by witch night action
    public void setTarget(Player p) {
        if(is(Witch.MAIN_ABILITY)){
            _targets.set(1, p);
        }else if(is(GraveDigger.MAIN_ABILITY)){
            _targets.set(1, p);
        }else if(is(ProxyKill.MAIN_ABILITY)){
            if(_targets.size() == 2)
                _targets.add(p);
            else
                _targets.set(2, p);
        }else if(is(Driver.MAIN_ABILITY)){
            _targets.set(0, p);
        }else if(is(Joker.MAIN_ABILITY) && _targets.size() > 1 && getOption() == null){
            _targets.set(1, p);
        }else if(is(Jailor.MAIN_ABILITY)){
            Jailor card = (Jailor) getAbility();
            _targets = card.jailedTargets.copy();
        }else if(_targets.isEmpty()){
            _targets.add(p);
        }else{
            _targets.set(0, p);
        }
    }

    public void setTargets(PlayerList pl) {
        _targets.clear();
        _targets.add(pl);
    }

    public Action setOption(String input) {
        this.option = input;
        if(owner.isDead())
            return this;
        if(is(Spy.COMMAND, Framer.COMMAND)){
            Faction t = owner.narrator.getFaction(input);
            if(t != null)
                return this;

            t = owner.narrator.getFactionByName(input);
            if(t != null)
                return this;

            throw new UnknownTeamException("Unknown team: " + input);
        }
        return this;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
        if(owner.isDead())
            return;
        if(is(Tailor.COMMAND)){
            if(this.option2 == null)
                throw new UnknownRoleException("Need to submit a role with this option");
            MemberList mList = owner.narrator.getPossibleMembers();

            AbilityList baseName = mList.translate(option2);
            if(baseName != null){
                ArrayList<Faction> teams = mList.getFactions(baseName, owner.narrator);

                for(Faction t: teams){
                    if(t.getColor().equals(option)){
                        return;
                    }
                }
            }
            throw new UnknownRoleException(option2 + " is not a role");
        }
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption() {
        return option;
    }

    public String getOption2() {
        return option2;
    }

    public String getOption3() {
        return option3;
    }

    // the method narrator god file calls
    public void attemptCompletion() {
        if(this.isCompleted)
            return;
        ActionList actionList = owner.getActions();
        int ability = this.ability;
        int oldAbility = this.ability;
        boolean successWillTriggerBreadUsage = getAbility() != null
                && getAbility().successfulUsageTriggersBreadUsage(this);

        if(getAbility() != null && getAbility().getRealCharges() == 0){
            getAbility().useFakeCharge();
            ability = Ability.VISIT_ABILITY;
        }else if(getAbility() == null)
            ability = Ability.VISIT_ABILITY;

        if(actionList.mainAction == this || actionList.freeActions.containsKey(ability)){
            try{
                this.ability = ability;
                doNightAction();
            }catch(ElectrocutionException e){
                electrocutionHandling(this, e);
            }
            this.ability = oldAbility;

            if(isCompleted() && successWillTriggerBreadUsage)
                owner.getAbility(BreadAbility.class).useCharge();

            useChargeIfApplicable(ability);

            return;
        }
        if(owner.isDead() || isProxied){
            try{
                doNightAction();
            }catch(ElectrocutionException e){
                electrocutionHandling(this, e);
            }
            useChargeIfApplicable(ability);
            return;
        }

        BreadAbility ba = owner.getAbility(BreadAbility.class);
        int actionIndex = actionList.extraActions.indexOf(this);
        if(actionIndex == -1)
            throw new Error();
        Bread b = ba.get(actionIndex);
        if(b.isReal()){
            try{
                doNightAction();
            }catch(ElectrocutionException e){
                electrocutionHandling(this, e);
            }
            if(isCompleted())
                b.markUsed();
            useChargeIfApplicable(ability);
        }else{
            isCompleted = true;
            b.markUsed();
            useChargeIfApplicable(ability);
        }
    }

    private void useChargeIfApplicable(int ability) {
        if(ability == this.ability && ability != Bread.MAIN_ABILITY && ability != CultLeader.MAIN_ABILITY
                && ability != Jailor.MAIN_ABILITY && ability != Thief.MAIN_ABILITY && ability != Janitor.MAIN_ABILITY
                && getAbility().getPerceivedCharges() != 0 && isCompleted){
            getAbility().useCharge();
            getAbility().triggerCooldown();
        }
    }

    private static void electrocutionHandling(Action a, ElectrocutionException e) {
        for(Player p: e.charged){
            if(!p.is(ElectroManiac.class))
                ElectroManiac.ElectroKill(p);
        }
        Message m = new Happening(a.owner.narrator).add(e.triggerer, " caused an electrical explosion which killed ");
        PlayerList otherExploders = e.charged.copy().remove(e.triggerer);
        if(otherExploders.isEmpty()){
            m.add("only ", e.triggerer);
        }else{
            m.add(otherExploders);
        }
        m.add(".");

        PlayerList electros = e.charged.filter(ElectroManiac.class);
        PlayerList nonActors = e.charged.copy().remove(electros);
        a.owner.narrator.doNightAction(nonActors, Disguiser.COMMAND);
    }

    // doesn't take care of setting command
    // return type is {new action, old action};
    public static Action[] pushCommand(SelectionMessage e, Action action, ActionList actions) {
        Player p = action.owner;
        StringChoice you = new StringChoice(p);
        Action newAction;
        Action poppedOff = null;

        try{
            newAction = actions.addAction(action);
        }catch(ActionException ae){
            newAction = ae.newAction;
            poppedOff = ae.oldAction;
        }
        Action[] ret = {
                newAction, poppedOff
        };

        if(p.isDead() && !p.is(Ghost.class))
            return ret;
        if(poppedOff != null){
            if(!poppedOff.isSameAbility(newAction)){
                e.add("Instead of ");
                e.add(poppedOff.getInsteadPhrase());
                e.add(", ");
                you.add(p, "you");
            }else{
                you.add(p, "You");
            }
        }else{
            you.add(p, "You");
        }
        e.add(you);
        if(!newAction.is(FactionSend.MAIN_ABILITY))
            e.add(" will ");
        e.add(newAction.getAbility().getActionDescription(newAction.getSameActions(actions)));
        e.add(".");
        return ret;
    }

    public void dayHappening() {
        ((DayResolveRole) owner.getAbility(ability)).dayHappening(this);

    }

    public boolean isDayAction() {
        if(owner.getDayCommands().contains(getCommand()))
            return true;
        return false;
    }

    public boolean isNightAction() {
        if(getAbility().isNightAbility(owner))
            return true;
        return false;
    }

    public boolean isTeamTargeting() {
        Faction t = owner.getFaction();
        if(!t.knowsTeam())
            return false;
        if(_targets == null)
            return false;
        String ownerColor = t.getColor();
        for(Player p: _targets){
            if(p.getFaction().getColor().equals(ownerColor))
                return true;
        }
        return false;
    }

    public Ability getAbility() {
        if(ability == Ability.VISIT_ABILITY)
            return new Visit(owner);
        if(owner.tempAbilities != null)
            for(Ability a: owner.tempAbilities){
                if(ability == a.getAbilityNumber())
                    return a;
            }

        return owner.getAbility(ability);
    }

    public boolean isTeamAbility() {
        for(Ability a: owner.role._abilities){
            if(ability == a.getAbilityNumber())
                return false;
        }
        for(Faction t: owner.getFactions()){
            if(t.hasAbility(ability))
                return true;
        }
        return false;
    }

    // TODO remove, I should use the int here only
    public boolean is(String s) {
        return Ability.ParseAbility(s) == ability;
    }

    public boolean is(AbilityList abilities) {
        for(Ability a: abilities){
            if(a.getAbilityNumber() == ability)
                return true;
        }
        return false;
    }

    public boolean is(String... s) {
        for(String command: s)
            if(is(command))
                return true;
        return false;
    }

    // is one of these abilities
    public boolean is(int... abilities) {
        for(int ability: abilities)
            if(this.ability == ability)
                return true;
        return false;
    }

    public String getCommand() {
        if(getAbility() == null)
            return "null"; // TODO
        return getAbility().getCommand();
    }

    public FactionList getFactions() {
        FactionList factionList = new FactionList();
        for(Faction faction: owner.getFactions()){
            if(faction.hasSharedAbilities() && is(faction.getAbilities())){
                factionList.add(faction);
            }
        }
        return factionList;
    }

    public void pushCommand(SelectionMessage event) {
        ArrayList<String> command = getAbility().getCommandParts(this);
        event.recordCommand(owner, command);
    }

    public static boolean isIllegalRetarget(Action action) {
        Player player = action.owner;
        if(player.narrator.isFirstPhase())
            return false;
        Ability ability = action.getAbility();
        if(ability.modifiers.getOrDefault(AbilityModifier.BACK_TO_BACK, true))
            return false;
        ActionList actionList;
        if(ability.isDayAbility())
            actionList = player.getPrevDayTarget();
        else
            actionList = player.getPrevNightTarget();
        for(Action prevAction: actionList.getActions(action.ability)){
            if(ability.isRetarget(action, prevAction))
                return true;
        }
        return false;
    }
}
