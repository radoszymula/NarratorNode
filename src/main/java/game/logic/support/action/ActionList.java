package game.logic.support.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import game.event.SelectionMessage;
import game.logic.Faction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.ActionException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.listeners.NarratorListener;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.roles.Ability;
import game.roles.BreadAbility;
import game.roles.Driver;
import game.roles.ElectroManiac;
import game.roles.FactionSend;
import game.roles.Ghost;
import game.roles.GraveDigger;
import game.roles.Witch;

public class ActionList implements Iterable<Action> {

    private Player owner;
    private Action sendAction;
    public Action mainAction;
    public HashMap<Integer, Action> freeActions;
    public ArrayList<Action> extraActions;

    public ActionList(Player owner) {
        this.owner = owner;
        freeActions = new HashMap<>();
        extraActions = new ArrayList<>();
    }

    // rename to getActionList
    public ArrayList<Action> getActions() {
        ArrayList<Action> actions = new ArrayList<>();
        if(mainAction != null)
            actions.add(mainAction);
        actions.addAll(extraActions);
        actions.addAll(freeActions.values());
        if(sendAction != null)
            actions.add(sendAction);
        return actions;
    }

    public boolean isTargeting(Player target, int ability) {
        return isTargeting(Player.list(target), ability);
    }

    public boolean isTargeting(PlayerList targets, int ability) {
        for(Action a: getActions()){
            if(a.isTargeting(targets, ability)){
                return true;
            }
        }
        return false;
    }

    public boolean canAddAnotherAction(Action a) {
        if(a.getAbility() == null)
            return false;
        if(!a.getAbility().canAddAnotherAction(a))
            return false;
        if(a.is(FactionSend.MAIN_ABILITY))
            return true;
        if(a.getAbility().canSubmitFree()){
            if(this.freeActions.containsKey(a.ability))
                return BreadAbility.getUseableBread(owner) > extraActions.size();
            return true;
        }
        if(mainAction == null)
            return true;
        return BreadAbility.getUseableBread(owner) > extraActions.size();
    }

    public boolean canAddAnotherAction(int ability, Player p) {
        if(owner.getAbility(ability) == null)
            return false;
        return canAddAnotherAction(owner.getAbility(ability).getExampleWitchAction(owner, p));
    }

    public boolean canAddAnotherAction(String s) {
        return canAddAnotherAction(Ability.ParseAbility(s));
    }

    public boolean canAddAnotherAction(int ability) {
        Action exampleAction = owner.getAbility(ability).getExampleAction(owner);
        if(exampleAction == null)
            return false;
        return canAddAnotherAction(exampleAction);
    }

    public ArrayList<Action> getActions(int ability) {
        ArrayList<Action> actions = new ArrayList<>();
        for(Action a: getActions()){
            if(a.is(ability))
                actions.add(a);
        }
        return actions;
    }

    public boolean isUsingAbility(int ability) {
        for(Action a: getActions()){
            if(a.is(ability))
                return true;
        }
        return false;
    }

    private boolean isSend(Action a) {
        if(a.ability != Faction.SEND_)
            return false;
        if(sendAction == null)
            sendAction = a;
        else{
            Action originalSend = this.sendAction;
            originalSend.setTargets(PlayerList.GetUnique(a.getTargets().add(originalSend.getTargets())));
        }
        // reorder targets here
        return true;
    }

    public Action addAction(Action newAction) {
        if(isSend(newAction))
            return sendAction;

        int maxActionTypeCount = newAction.getAbility().maxNumberOfSubmissions();

        Action oldAction = null;

        if(maxActionTypeCount == 1 && this.getActions(newAction.ability).size() != 0){
            if(mainAction != null && mainAction.is(newAction.ability)){
                oldAction = mainAction;
                mainAction = newAction;
                return newAction;
            }else if(newAction.getAbility().canSubmitFree() && freeActions.containsKey(newAction.ability)){
                oldAction = freeActions.get(newAction.ability);
                freeActions.put(newAction.ability, newAction);
                return newAction;
            }else{
                throw new Error();
            }
        }else if(newAction.getAbility().canSubmitWithoutBread(newAction)){
            if(newAction.getAbility().canSubmitFree() && !freeActions.containsKey(newAction.ability)){
                freeActions.put(newAction.ability, newAction);
                return newAction;
            }else if(mainAction == null
                    && (!newAction.getAbility().canSubmitFree() || !freeActions.containsKey(newAction.ability))){
                return mainAction = newAction;
            }
        }

        if(newAction.is(ElectroManiac.MAIN_ABILITY) && newAction._targets.size() == 2){
            for(Action eAction: getActions(ElectroManiac.MAIN_ABILITY)){
                replace(eAction, newAction);
                throw new ActionException(eAction, newAction);
            }
        }

        if(BreadAbility.getUseableBread(owner) > extraActions.size()){
            extraActions.add(newAction);
            return newAction;
        }

        // have to push something off

        if(BreadAbility.getUseableBread(owner) == extraActions.size() && !extraActions.isEmpty()){
            oldAction = extraActions.remove(extraActions.size() - 1);
            extraActions.add(newAction);
        }else if(newAction.getAbility().canSubmitFree()){
            oldAction = addFreeAction(newAction);
        }else{
            oldAction = mainAction;
            mainAction = newAction;
        }

        // if i can't submit without bread, and i don't have bread, i shouldn't get to
        // this point ever. its an unacceptable action
        if(owner.isAlive() || owner.is(Ghost.class))
            throw new ActionException(oldAction, newAction);

        return newAction;
    }

    /*
     * what is this used for?
     *
     * doctors and bgs to know who they're targeting witch, to know who the previous
     * targets were
     *
     */
    public PlayerList getTargets(String command) {
        return getTargets(Ability.ParseAbility(command));
    }

    public PlayerList getTargets(int ability) {
        PlayerList list = new PlayerList();
        for(Action a: getActions()){
            if(ability == a.ability && a.isDoubleTargeter() && (owner.is(Witch.class) || owner.is(Driver.class))){

                if(owner.is(Driver.class) || owner.is(Witch.class)){
                    list.add(a._targets);
                }else if(owner.is(GraveDigger.class)){
                    ;
                }
            }else if(a.isSameAbility(ability)){
                list.add(a.getTargets());
            }
        }
        return list;
    }

    public Action remove(PlayerList target, int ability, boolean setCommand) {
        Action toRemove = null;
        ArrayList<Action> actions = getActions();
        for(Action a: actions){
            if(a.has(target, ability)){
                toRemove = a;
                break;
            }
        }
        if(toRemove != null){
            toRemove.cancel(actions.indexOf(toRemove), setCommand);
            remove(toRemove);
        }
        return toRemove;
    }

    public Action cancelActionID(int actionPosition, boolean setCommand) {
        ArrayList<Action> actions = getActions();
        if(actionPosition >= actions.size() || actionPosition < 0)
            throw new PlayerTargetingException("Action not found");

        Action toCancel = actions.get(actionPosition);
        remove(toCancel);
        toCancel.cancel(actionPosition, setCommand);
        if(owner.narrator.isDay())
            owner.narrator.actionStack.remove(toCancel);
        toCancel.getAbility().onCancel(toCancel);
        return toCancel;
    }

    public Action getFirst() {
        if(mainAction != null)
            return mainAction;
        if(!freeActions.isEmpty())
            return freeActions.values().iterator().next();
        if(sendAction != null)
            return sendAction;
        return null;
    }

    public boolean isEmpty() {
        return mainAction == null && sendAction == null && extraActions.isEmpty() && freeActions.isEmpty();
    }

    public boolean visitedHome() {
        return visited(owner);
    }

    public boolean visited(Player target) {
        for(Action a: getActions()){
            if(a.visited(target))
                return true;
        }
        return false;
    }

    public void cleanup() {
        for(Action a: getActions()){
            a.cleanup();
        }
    }

    public boolean isVesting() {
        for(Action a: getActions()){
            if(a.is(Ability.VEST_ABILITY))
                return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o.getClass() != getClass())
            return false;
        ActionList al = (ActionList) o;

        if(!al.owner.getName().equals(owner.getName()))
            return false;
        if(Util.notEqual(mainAction, al.mainAction))
            return false;
        if(Util.notEqual(sendAction, al.sendAction))
            return false;
        if(Util.notEqual(freeActions, al.freeActions))
            return false;
        if(Util.notEqual(extraActions, al.extraActions))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return getActions().toString();
    }

    /*
     * public void sortActions() { Collections.sort(actions, new
     * Comparator<Action>() { public int compare(Action a1, Action a2){
     * if(a1.ability == Ability.NIGHT_SEND || a2.ability == Ability.NIGHT_SEND){
     * if(a1.ability == Ability.NIGHT_SEND){ return 1; } if(a2.ability ==
     * Ability.NIGHT_SEND) return -1; }
     *
     * if(a1.ability == Ability.BREAD_ABILITY || a2.ability ==
     * Ability.BREAD_ABILITY){ if(a1.ability == Ability.BREAD_ABILITY){ return 1; }
     * if(a2.ability == Ability.BREAD_ABILITY) return -1; }
     *
     * return 0; } }); }
     */

    public int size() {
        return getActions().size();
    }

    public void remove(Action a) {
        if(sendAction == a){
            sendAction = null;
            return;
        }
        if(extraActions.contains(a)){
            extraActions.remove(a);
            return;
        }
        if(freeActions.containsKey(a.ability)){
            freeActions.remove(a.ability);
            Action toMove = null;
            for(Action extra: extraActions){
                if(extra.ability == a.ability){
                    toMove = extra;
                    break;
                }
            }
            if(toMove != null){
                extraActions.remove(toMove);
                freeActions.put(toMove.ability, toMove);
            }
        }else{
            mainAction = null;
            Action toMove = null;
            for(Action extra: extraActions){
                if(!extra.getAbility().canSubmitFree()){
                    toMove = extra;
                    break;
                }
            }
            mainAction = toMove;
            if(mainAction != null)
                extraActions.remove(mainAction);
        }
    }

    private Action addFreeAction(Action a) {
        return freeActions.put(a.ability, a);
    }

    public void replace(Action oldAction, Action newAction) {// String command, PlayerList pList, String option, String
                                                             // option2, String option3) {
        owner.targetingPreconditionChecks(newAction);
        SelectionMessage descrip = new SelectionMessage(owner, true, true);
        descrip.add("Instead of ");
        descrip.add(oldAction.getInsteadPhrase());
        descrip.add(", ");

        StringChoice you = new StringChoice(owner);
        you.add(owner, "you");

        ArrayList<Action> aList = new ArrayList<>();
        aList.add(newAction);

        descrip.add(" will ");
        descrip.add(owner.getAbility(newAction.ability).getActionDescription(aList));
        descrip.add(".");

        newAction.pushCommand(descrip);

        if(oldAction.ability != newAction.ability){
            remove(oldAction);
            addAction(newAction);
            if(owner.narrator.isDay()){
                owner.narrator.actionStack.add(newAction);
                owner.narrator.actionStack.remove(oldAction);
            }
        }else{
            if(oldAction == mainAction){
                mainAction = newAction;
            }else if(freeActions.containsValue(oldAction)){
                addFreeAction(newAction);
            }else if(!isSend(newAction)){
                int index = extraActions.indexOf(oldAction);
                extraActions.add(index, newAction);
                extraActions.remove(oldAction);
            }
            owner.narrator.actionStack.add(newAction);
            owner.narrator.actionStack.remove(oldAction);
        }

        if(newAction.isTeamAbility() && (sendAction == null || !sendAction.isTargeting(owner))){
            addAction(new Action(owner, FactionSend.MAIN_ABILITY, owner));
        }

        descrip.pushOut();

        List<NarratorListener> listeners = owner.narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onTargetSelection(owner, descrip);
    }

    public void clear() {
        mainAction = sendAction = null;
        freeActions.clear();
        extraActions.clear();
    }

    @Override
    public Iterator<Action> iterator() {
        return getActions().iterator();
    }
}
