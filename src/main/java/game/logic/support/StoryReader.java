package game.logic.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Narrator;
import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifier;
import models.Command;
import models.schemas.FactionRoleSchema;
import models.schemas.FactionSchema;
import models.schemas.GameOverviewSchema;
import models.schemas.HiddenSchema;
import models.schemas.PlayerSchema;
import models.schemas.RoleSchema;
import models.schemas.SetupHiddenSchema;

public interface StoryReader {

    GameOverviewSchema getGameOverview();

    HashMap<SetupModifier, Rule> getRules();

    List<FactionSchema> getTeamInfo();

    void setEnemies(Narrator n);

    void setSheriffDetectables(Narrator n);

    Map<Long, RoleSchema> getRoles();

    Set<FactionRoleSchema> getFactionRoles();

    List<HiddenSchema> getHiddens();

    ArrayList<SetupHiddenSchema> getSetupHiddens();

    ArrayList<PlayerSchema> getPlayers();

    ArrayList<Command> getCommands();

    long getSetupOwnerID();

}
