package game.logic.support;

import java.util.ArrayList;

import game.ai.Controller;
import game.event.OGIMessage;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PhaseException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownPlayerException;
import game.logic.exceptions.UnknownRoleException;
import game.logic.exceptions.VotingException;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.TextController.TextInput;
import game.roles.Ability;
import game.roles.Burn;
import game.roles.Commuter;
import game.roles.Hidden;
import game.roles.Marshall;
import game.roles.Mayor;
import game.roles.PuppetVote;
import game.roles.Ventriloquist;
import game.roles.Veteran;
import models.FactionRole;
import models.SetupHidden;
import util.LookupUtil;

public class CommandHandler implements TextInput {

    protected Narrator narrator;

    public CommandHandler(Narrator n) {
        this.narrator = n;
    }

    public void parseCommand(Player player, String command) {
        parseCommand(player, command, true);
    }

    public void parseCommand(Player owner, String command, boolean modMode) {
        if(command.length() == 0)
            return;

        command = command.replace("\n", "");
        String name = owner == null ? "Skipper" : owner.getName();
        command(owner, command, name, null, modMode);
    }

    public static final String VOTE = Constants.VOTE;
    public static final String UNVOTE = Constants.UNVOTE;
    public static final String SKIP_VOTE = Constants.SKIP_VOTE;
    public static final String SAY = Constants.SAY;
    public static final String END_NIGHT = Constants.END_NIGHT;
    public static final String MODKILL = Constants.MODKILL;
    public static final String PREFER = "prefer";
    public static final String SET_NAME = "setname";
    public static final String END_PHASE = "end_phase";
    public static final String END_PHASED_SPACED = "end phase";
    public static final String MOD_CHANGE_ROLE = "change_role";
    public static final String[] MOD_COMMANDS = {
            MOD_CHANGE_ROLE, MODKILL, Constants.MOD_ADDBREAD
    };

    // name is used for double command
    public static final String END_NIGHT_EXCEPTION = "You can only end night during the night";
    public static final String ALIAS = "alias";

    public void command(Player owner, String message, String name) {
        command(owner, message, name, null, true);
    }

    public void command(Player owner, String message, String name, boolean modMode) {
        command(owner, message, name, null, modMode);
    }

    public void command(Player owner, String message, String name, AlternateLookup aLookup, boolean modMode) {
        if(!narrator.isInProgress() && narrator.isStarted())
            return;
        switch (message.toLowerCase()){
        case END_PHASE:
        case END_PHASED_SPACED:
            narrator.endPhase();
            return;
        case MODKILL:
            if(owner == null)
                throw new NarratorException("Must submit a target.");
            owner.modkill();
            return;
        case END_NIGHT:
            if(narrator.isDay())
                throw new PhaseException(END_NIGHT_EXCEPTION);
            if(owner.endedNight()){
                owner.cancelEndNight();
            }else{
                owner.endNight();
            }
            return;
        case SKIP_VOTE:
            if(dayAttempt()){
                owner.voteSkip();
                return;
            }
            break;
        case UNVOTE:
            if(dayAttempt())
                owner.unvote();
            break;
        case Marshall.ORDER_LOWERCASE:
            if(dayAttempt()){
                if(owner.hasDayAction(Marshall.COMMAND)){
                    owner.doDayAction(Marshall.MAIN_ABILITY);
                    return;
                }
                throw new IllegalActionException("Only mayors can do this.");
            }
            throw new IllegalActionException("Can't do this during the night.");
        case Mayor.REVEAL_LOWERCASE:
            if(dayAttempt()){
                if(owner.hasDayAction(Mayor.COMMAND)){
                    owner.doDayAction(Mayor.MAIN_ABILITY);
                    return;
                }
                throw new IllegalActionException("Only mayors can do this.");
            }
            throw new IllegalActionException("Can't do this during the night.");
        case Burn.BURN_LOWERCASE:
            if(owner.hasAbility(Burn.class)){
                if(narrator.isNight()){
                    Burn.NightBurn(owner);
                    return;

                }else if(owner.hasDayAction(Burn.COMMAND)){
                    owner.doDayAction(Burn.MAIN_ABILITY);
                    return;
                }else{
                    if(narrator.getBool(SetupModifier.ARSON_DAY_IGNITES))
                        throw new IllegalActionException("You can't do this again.");
                    throw new IllegalActionException("You can't do this during the day.");
                }
            }
            throw new IllegalActionException("Only arsonists can do this.");
        case Constants.VEST_COMMAND_LOWER:
            if(owner.getAcceptableTargets(Ability.VEST_ABILITY) != null)
                throw new IllegalActionException("You have no vests to use");

            owner.setTarget(Ability.VEST_ABILITY, null, null);
            break;

        case Veteran.COMMAND_LOWERCASE:
            if(owner.hasAbility(Veteran.class)){
                if(narrator.isNight()){
                    Veteran.alert(owner);
                    return;
                }
                throw new IllegalActionException("You can't do this during the day.");
            }
            throw new IllegalActionException("Only veterans can do this.");
        case Commuter.COMMAND_LOWERCASE:
            if(owner.is(Commuter.class)){
                if(narrator.isNight()){
                    Commuter.commute(owner);
                    return;
                }
                throw new IllegalActionException("You can't do this during the day.");
            }
            throw new IllegalActionException("Only commuters can do this.");
        case Constants.ACCEPT:
        case Constants.DECLINE:
            if(!narrator.isDay())
                throw new PhaseException("You cannot accept or decline invitations during the night.");
            if(owner.hasDayAction(Ability.INVITATION_ABILITY))
                owner.doDayAction(message);
            else
                throw new IllegalActionException(
                        "You don't have any recruitment iniviations to accept or decline right now.");
            break;

        case Constants.CANCEL:
            if(!narrator.isNight())
                throw new PhaseException("You can't cancel night actions during the day.");
            try{
                owner.clearTargets();
                new OGIMessage(owner, "You have canceled all your inputted actions.");

                return;
            }catch(NumberFormatException e){
                new OGIMessage(owner, "canceling requires a number");
            }

        default:
            tryDoubleCommand(owner, message, name, aLookup, modMode);
        }
    }

    public static Integer TEAM_PREFER = 0;
    public static Integer ROLE_PREFER = 1;

    public static void prefer(Player p, String preference) {
        Narrator n = p.narrator;
        Faction t = n.getFactionByName(preference.toLowerCase());
        if(t != null){
            p.teamPrefer(t.getColor());
            return;
        }
        Hidden hidden;
        for(SetupHidden setupHidden: n.getRolesList()){
            hidden = setupHidden.hidden;
            if(hidden.getName().equalsIgnoreCase(preference)){
                preferHidden(hidden, p);
                return;
            }
            for(FactionRole role: hidden.getFactionRoles()){
                if(role.getName().equalsIgnoreCase(preference)){
                    preferRole(role, p);
                    return;
                }
            }
        }
        throw new UnknownRoleException(preference);
    }

    private static void preferRole(FactionRole role, Player p) {
        p.rolePrefer(role.getName());
        p.teamPrefer(role.getColor());
    }

    private static void preferHidden(Hidden hidden, Player player) {
        for(FactionRole factionRole: hidden.factionRolesMap.values())
            preferRole(factionRole, player);
    }

    public void setName(Player owner, String newName) {
        if(!narrator.acceptablePlayerName(owner, newName)){
            new OGIMessage(owner, "You may not use that name");
            return;
        }
        owner.setName(newName);
        new OGIMessage(owner, "Your name is now " + newName + ".");
    }

    public void tryDoubleCommand(Player owner, String message, String name) {
        tryDoubleCommand(owner, message, name, null, true);
    }

    public void tryDoubleCommand(Controller owner, String message, String name) {
        tryDoubleCommand(owner.getPlayer(narrator), message, name, null, true);
    }

    // name is used in case owner is null
    public void tryDoubleCommand(Player owner, String message, String name, AlternateLookup aLookup, boolean modMode) {
        ArrayList<String> block = new ArrayList<>();
        for(String s: message.split(" "))
            block.add(s);

        if(block.size() < 2)
            throw new PlayerTargetingException("Must have a target");
        String command = block.remove(0);

        if(command.equalsIgnoreCase(Constants.CANCEL)){
            try{
                int num = Integer.parseInt(block.get(0));
                if(narrator.isDay())
                    owner.cancelAction(num - 1);
                else
                    owner.cancelAction(num - 1);
            }catch(NumberFormatException e){
                throw new PlayerTargetingException(
                        "If you're not canceling all actions, you must input the index of the action you wish to cancel.");
            }
            return;
        }

        if(command.equalsIgnoreCase(SAY)){
            message = message.substring(SAY.length() + 1);
            if(owner == null){
                if(narrator.isStarted())
                    return;
                owner = new Player(name + "[Pregame]", narrator);
            }

            message = message.substring(block.get(0).length() + 1);
            owner.say(message, block.get(0));
            return;
        }
        if(command.equalsIgnoreCase(PREFER)){
            if(narrator.isStarted())
                throw new PhaseException("You can only prefer before a game starts.");
            prefer(owner, message.substring(command.length() + 1));
            return;
        }
        if(command.equalsIgnoreCase(SET_NAME)){
            if(narrator.isStarted())
                throw new PhaseException("You can only change your name before game starts");
            if(message.length() <= SET_NAME.length())
                throw new PlayerTargetingException("You cant have an empty name");
            message = message.substring(SET_NAME.length() + 1);
            message = message.replaceAll(" ", "");
            setName(owner, message);
            return;
        }
        if(command.equalsIgnoreCase(Constants.MOD_ADDBREAD) && modMode){
            message = message.substring(command.length() + 1);
            boolean negative = message.startsWith("-");
            if(negative)
                message = message.substring(1);
            String number = "";
            while (Character.isDigit(message.charAt(0))){
                number += message.substring(0, 1);
                message = message.substring(1);
            }
            if(number.length() == 0)
                return;
            message = message.substring(1);
            Player target = narrator.getPlayerByName(message, aLookup);
            if(target == null)
                return;
            int breadToAdd = Integer.parseInt(number);
            if(negative)
                breadToAdd *= -1;

            target.addModBread(breadToAdd);

            return;
        }
        if(command.equals(MOD_CHANGE_ROLE) && modMode){
            owner = narrator.getPlayerByName(block.get(0), aLookup);
            String color = block.get(1);
            String roleName = block.get(2);

            owner.setRole(LookupUtil.findFactionRole(narrator, roleName, color));

            return;
        }
        if(command.equals(MODKILL) && modMode){
            owner = narrator.getPlayerByName(block.get(0), aLookup);
            owner.modkill();
            return;
        }

        int untarget = message.indexOf(block.get(0));
        if(message.substring(untarget).equals(Constants.UNTARGET)){
            if(narrator.isDay()){
                throw new IllegalActionException("You can't untarget during the day.");
            }
            int ability = Ability.ParseAbility(command);
            if(ability == Ability.INVALID_ABILITY || !owner.hasAbility(ability))
                throw new IllegalActionException();
            Player toUntarget = owner.getTargets(ability).getFirst();
            owner.cancelTarget(toUntarget, ability);
            return;
        }
        if(command.equalsIgnoreCase(Constants.CANCEL)){
            if(narrator.isDay())
                throw new IllegalActionException("You can't cancel actions during the day.");
            message = message.substring(untarget);
            throw new NullPointerException();
        }
        if(command.equalsIgnoreCase(ALIAS)){
            if(block.size() < 2){
                throw new IllegalActionException("Alias set isn't complete");
            }
            narrator.setAlias(block.get(0), block.get(1));
            return;
        }
        if(command.equalsIgnoreCase(Constants.LAST_WILL)){
            owner.setLastWill(message = message.substring(SAY.length() + 1));
            return;
        }
        if(command.equalsIgnoreCase(Constants.LAST_WILL)){
            owner.setLastWill(message = message.substring(SAY.length() + 1));
            return;
        }

        if(command.equalsIgnoreCase(Constants.TEAM_LAST_WILL)){
            owner.setTeamLastWill(message = message.substring(SAY.length() + 1));
            return;
        }

        if(narrator.isDay()){
            if(block.isEmpty())
                throw new UnknownPlayerException("Nobody selected!");
            Player target = narrator.getPlayerByName(block.get(0), aLookup);
            if(target == null)
                throw new UnknownPlayerException("Nobody selected!");
            if(target.equals(narrator.skipper))
                throw new PlayerTargetingException("You can't perform actions on Skipper!");

            int ability = Ability.ParseAbility(command);
            if(ability != Ability.INVALID_ABILITY && owner.hasAbility(ability)){
                block.add(0, command);
                Action a = owner.getAbility(ability).parseCommand(owner, block);
                owner.doDayAction(a.ability, a.getOption(), a.getTargets());
                return;
            }
            if(command.equalsIgnoreCase(Ventriloquist.VENT_SKIP_VOTE)){
                target.voteSkip(owner);
                return;
            }else if(command.equalsIgnoreCase(Ventriloquist.VENT_UNVOTE)){
                target._unvote(owner, true);
                return;
            }else if(command.equalsIgnoreCase(Ventriloquist.VENT_VOTE)){
                if(block.size() == 2){
                    Player ventTarget = narrator.getPlayerByName(block.get(1), aLookup);
                    if(ventTarget == null)
                        throw new VotingException("Ventriloquist puppets need someone to vote for");
                    owner.doDayAction(PuppetVote.MAIN_ABILITY, null, Player.list(target, ventTarget));
                    return;
                }
                throw new VotingException("Couldn't find a puppet and a vote target to use your ability on");
            }else if(!command.equalsIgnoreCase(VOTE)){
                throw new IllegalActionException();
            }else{
                owner.vote(target);
                return;
            }
        }
        int ability = Ability.ParseAbility(command);
        if(ability == Ability.INVALID_ABILITY || !owner.hasAbility(ability))
            throw new IllegalActionException("Unknown ability " + command);

        block.add(0, command);
        Action a = owner.getAbility(ability).parseCommand(owner, block);
        owner.setTarget(a);
    }

    public static String parseTeam(String name, Narrator n) {
        Faction t = n.getFaction(name); // checking if color
        if(t != null)
            return name;
        t = n.getFactionByName(name);
        if(t != null)
            return t.getColor();
        return Constants.A_INVALID;
    }

    public boolean dayAttempt() {
        if(!narrator.isDay())
            throw new PhaseException("Can't do this during the night");

        return true;
    }

    @Override
    public void text(String playerID, String message, boolean sync) {
        command(narrator.getPlayerByID(playerID), message, playerID, null, true);
    }

}
