package game.logic.support;

import game.event.EventDecoder;
import game.logic.Faction;

public class HTString {
    private String data;
    private String color;

    public HTString(Faction t) {
        this.data = t.getName();
        this.color = t.getColor();
    }

    public HTString(String data, String color) {
        this.data = data;
        this.color = color;
    }

    public HTString(String roleName, Faction team) {
        this(team.getName() + " " + roleName, team.getColor());
    }

    public String access(EventDecoder html) {
        if(html != null){
            return html.coloredText(data, color);
        }
        return data;
    }

    public String getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o.getClass() != HTString.class)
            return false;
        HTString ht = (HTString) o;
        if(ht.color.equals(color))
            if(ht.data.equals(data))
                return true;
        return false;
    }

    @Override
    public int hashCode() {
        return (color + data).hashCode();
    }
}
