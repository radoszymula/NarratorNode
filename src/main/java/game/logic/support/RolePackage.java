package game.logic.support;

import game.logic.Player;
import game.roles.Hidden;
import models.FactionRole;

//rename to PlayerRole
public class RolePackage {

    public Hidden assignedHidden;
    public FactionRole assignedRole;
    public Player player;

    // public RoleTemplate generatedFrom;

    public RolePackage(Hidden assignedHidden, FactionRole assignedRole) {
        this.assignedRole = assignedRole;
        this.assignedHidden = assignedHidden;
    }

    public void assign(Player player) {
        this.player = player;
        player.narrator.getFaction(assignedRole.getColor()).addMember(player);
        player.narrator.generatedRoles.remove(this);
        player.setRole(this);
    }

    @Override
    public String toString() {
        if(assignedHidden.getSize() == 1)
            return assignedRole.getName();
        return assignedHidden.getName() + " " + assignedRole.getName();
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(o.getClass() != this.getClass())
            return false;
        RolePackage other = (RolePackage) o;
        return this.assignedHidden == other.assignedHidden && this.player == other.player
                && this.assignedRole == other.assignedRole;
    }

    @Override
    public int hashCode() {
        if(player == null)
            return (this.assignedHidden.hashCode() + "&" + this.assignedRole.hashCode()).hashCode();
        return (this.assignedHidden.hashCode() + "&" + this.assignedRole.hashCode() + "&" + this.player.hashCode())
                .hashCode();
    }
}
