package game.logic.support;

import game.logic.Narrator;
import game.logic.Player;

public class Skipper extends Player {

    public Skipper(Narrator n) {
        super("Skip Day", n);
        this._color = Constants.A_SKIP;
    }

}
