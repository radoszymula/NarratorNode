package game.logic.support.rules;

import game.logic.Narrator;
import game.logic.support.Constants;

public enum FactionModifier implements Modifier {
    HAS_NIGHT_CHAT("Can't talk to teammates at night", "Can talk to teammates at night", "Can talk to allies at night"),
    IS_RECRUITABLE("Team members cannot be recruited", "Team members can be taken by a Cult Leader",
            "Can be recruited"),
    KNOWS_ALLIES("Doesn't know the identity of teammates", "Knows the identity of teammates", "Knows who allies are"),
    LAST_WILL("No team last will of any sort", "Can write a message that will be sent to team members upon death",
            "Can send a last will upon death"),
    LIVE_TO_WIN("Only one person from this faction needs to be alive for all faction members to win",
            "Members of this faction must be alive at the end of the game to win", "Must be alive to win"),
    PUNCH_SUCCESS("", "", "Punch success rate"), WIN_PRIORITY("", "", "");

    private final String string1, string2, string3;

    FactionModifier(String string1, String string2, String string3) {
        this.string1 = string1;
        this.string2 = string2;
        this.string3 = string3;
    }

    public static int maxValue(FactionModifier fRule) {
        switch (fRule){
        case PUNCH_SUCCESS:
            return 100;
        default:
            return Constants.MAX_INT_MODIFIER;

        }
    }

    public static int minValue(FactionModifier fRule) {
        switch (fRule){
        case PUNCH_SUCCESS:
            return 0;
        default:
            return SetupModifiers.UNLIMITED;
        }
    }

    @Override
    public String getLabel(Narrator narrator) {
        return string3;
    }

    @Override
    public String getActiveFormat() {
        return string2;
    }

    @Override
    public String getInactiveFormat() {
        return string1;
    }

    @Override
    public String getUnlimitedFormat() {
        return string2;
    }

    @Override
    public String getLimitedFormat() {
        return string1;
    }
}
