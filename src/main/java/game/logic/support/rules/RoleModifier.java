package game.logic.support.rules;

import game.logic.Narrator;
import game.logic.support.Util;
import game.roles.support.Vest;

public enum RoleModifier implements Modifier {
    AUTO_VEST("Starts off with # auto-#" + Vest.ALIAS + "#", "Unlimited auto-#" + Vest.ALIAS + "#"),
    UNIQUE("Is not unique", "Is unique"), UNBLOCKABLE("Is blockable", "Is not blockable"),
    UNDETECTABLE("Is detectable by investigative abilities", "Is undetectable by investigative abilities"),
    UNCONVERTABLE("Is convertable", "Is not convertable"),
    HIDDEN_FLIP("Death flip will be public", "Death flip will be hidden");

    private final String string1, string2;

    RoleModifier(String string1, String string2) {
        this.string1 = string1;
        this.string2 = string2;
    }

    @Override
    public String getActiveFormat() {
        return string2;
    }

    @Override
    public String getInactiveFormat() {
        return string1;
    }

    @Override
    public String getUnlimitedFormat() {
        return string2;
    }

    @Override
    public String getLimitedFormat() {
        return string1;
    }

    @Override
    public String getLabel(Narrator narrator) {
        String format = string2;
        String keyFormat;
        String value;
        for(String key: narrator.getAliases()){
            keyFormat = "#" + key + "#";
            value = narrator.getAlias(key);
            if(format.indexOf(keyFormat) == 0)
                value = Util.TitleCase(value);
            format = format.replaceAll(keyFormat, value);
        }
        return format;
    }

}
