package game.logic.support.rules;

import java.util.HashMap;
import java.util.Map;

public class ModifierLabels {
    public static final Map<Modifier, String> modifierLabels;
    static{
        modifierLabels = new HashMap<>();
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }

    public static String getEnabledLabel(Modifier modifierID) {
        return modifierLabels.get(modifierID);
    }

}
