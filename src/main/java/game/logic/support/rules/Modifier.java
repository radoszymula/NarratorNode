package game.logic.support.rules;

import game.logic.Narrator;

public interface Modifier {

    String getActiveFormat();

    String getInactiveFormat();

    String getUnlimitedFormat();

    String getLimitedFormat();

    String getLabel(Narrator narrator);
}
