package game.logic.support.rules;

import java.util.Arrays;

import game.logic.Narrator;
import game.logic.support.Util;
import game.roles.Doctor;
import game.roles.support.Bread;
import game.roles.support.Gun;

public enum SetupModifier implements Modifier {

    CHAT_ROLES("Chat roles will not be spawned", "Chat roles will be spawned", ""),

    DAY_LENGTH("", "", ""), NIGHT_LENGTH("", "", ""), TRIAL_LENGTH("", "", ""), DISCUSSION_LENGTH("", "", ""),
    ROLE_PICKING_LENGTH("", "", ""),

    HOST_VOTING("", "", ""), OMNISCIENT_DEAD("", "", ""), SELF_VOTE("You may not self vote.", "You may self vote.", ""),
    SKIP_VOTE("You may not skip vote.", "You may skip vote.", ""),
    SECRET_VOTES("Votes will be hidden.", "Votes will be announced.", ""), VOTE_SYSTEM("", "", ""),

    AMNESIAC_KEEPS_CHARGES("Will only get a single charge from the role, if the role is a limited use role",
            "Will keep remaining charges, if role is a limited use role.", "Amnesiac keeps charges"),
    AMNESIAC_MUST_REMEMBER("Amnesiac can win without changing role", "Amnesiac must remember to win",
            "Amnesiac must remember role to win"),
    ARCH_BURN("Burn deaths do not spread in architect chats",
            "Burned targets will also kill everyone else in architect chat", "Fires spread in architect chats"),
    ARCH_QUARANTINE("Chat participants may talk in other chat rooms",
            "Chat participants are removed from all other night chats", "Architect removes from other night chats"),
    ARSON_DAY_IGNITES("Limited to %s day ignite(s)", "Can day ignite every day", "Day Ignite Count"),
    BLOCK_FEEDBACK("Targets won't know they were slept with", "Targets will know they were slept with",
            "Targets know they were slept with"),
    BOMB_PIERCE("Doesn't pierce death immunity", "Pierces death immunity", "Pierces death immunity"),
    BOUNTY_INCUBATION("People with a bounty on them can go %s day(s) until consequences hit", "", "Days of bounty"),
    BOUNTY_FAIL_REWARD("Gets %s retaliatory kill(s) if bounty fails", "", "Kill charges after bounty failure"),
    BREAD_PASSING("#" + Bread.ALIAS + "# cannot be passed.", "#" + Bread.ALIAS + "# can be passed.",
            "#" + Bread.ALIAS + "# can be passed"),
    CORONER_EXHUMES("Coroners won't stop others from robbing bodies", "Coroner's will stop others from robbing bodies",
            "Coroner exhumes body"),
    CHARGE_VARIABILITY("", "", ""),
    CHECK_DIFFERENTIATION("Only sees teams as bad or not suspicious",
            "Can pick out the difference between one team and another",
            "Can pick out the difference between different factions"),
    CONVERT_REFUSABLE("Refusing a recruitment invitation is not a choice", "Power roles can refuse recruitment.",
            "Power Roles can refuse conversion"),
    CORONER_LEARNS_ROLES("Coroners won't learn what roles visited the corpse while living.",
            "Coroners will learn what roles visited the corpse while living.", "Coroner learns roles visited"),
    CULT_POWER_ROLE_CD("After recruiting a power role, must wait %s nights before next conversion",
            "No wait time between power role recruits", "Power Role conversion delay"),
    CULT_PROMOTION("Cannot promote minions", "May promote minions to another ability",
            "Cult can promote powerless roles"),
    CIT_RATIO("The percentage of citizens to non citizens is about %s%.", "", "Ratio of citizens to non-citizens"),
    CULT_KEEPS_ROLES("Recruited members change role upon successful recruitment",
            "Recruited members keep their original role upon recruitment", "Converted keep roles"),
    DAY_START("Game starts on nighttime", "Game starts at daytime", ""),
    DIFFERENTIATED_FACTION_KILLS("You will not know which faction made this kill.",
            "You will know which faction made this kill.", ""),
    DOUSE_FEEDBACK("Doused targets are unaware they were doued", "Doused targets are aware they were doused",
            "Doused targets informed"),
    ENFORCER_LEARNS_NAMES("Won't learn attempted recruiter's name", "Will learn attempted recruiter's name",
            "Will learn attempted recruiter's name"),
    EXEC_TOWN("Target can be of any faction", "Target is always of the majority faction",
            "Target always majority faction"),
    EXECUTIONER_TO_JESTER("Suicides if target(s) is night killed", "Turns into jester upon target night death",
            "Turns into jester upon loss"),
    EXECUTIONER_WIN_IMMUNE("Invulnerable after winning", "Not invulnerable after winning",
            "Invulnerable after winning"),
    FOLLOW_GETS_ALL("Can only see one target", "Can see all targets visited by the followed person",
            "Can see all targets of the followed person"),
    FOLLOW_PIERCES_IMMUNITY("Can't follow detection immune targets", "Ignored detection immunity",
            "Pierces detectability immunity"),
    GD_REANIMATE("May not target the same corpse multiple times", "May target the same corpse multiple times",
            "May target the same corpse multiple times"),
    GS_DAY_GUNS("#" + Gun.ALIAS + "#s are used during the night", "#" + Gun.ALIAS + "#s are shot during the day",
            "#" + Gun.ALIAS + "# given are used during the day"),
    GUARD_REDIRECTS_CONVERT("Conversion is unaffected by bodyguard",
            "Conversion will redirect to bodyguard if bodyguard is guarding target",
            "Conversion redirected if being guarded"),
    GUARD_REDIRECTS_DOUSE("Douse is unaffected by bodyguard",
            "Douse will redirect to bodyguard if bodyguard is guarding target", "Douse redirected if being guarded"),
    GUARD_REDIRECTS_POISON("Poison is unaffected by bodyguard",
            "Poison will redirect to bodyguard if bodyguard is guarding target", "Poison redirected if being guarded"),
    HEAL_BLOCKS_POISON(Doctor.ROLE_DETAIL_POISON_CANNOT_STOP, Doctor.ROLE_DETAIL_POISON_CAN_STOP,
            "Doctors heal poisonings"),
    HEAL_FEEDBACK("Target doesn't know if they were saved", "Target knows if they were saved, but not by who",
            "Target knows if successfully healed"),
    HEAL_SUCCESS_FEEDBACK("Will not know if target was successfully saved",
            "Will know if target was successfully saved", "Knows if successful"),
    JANITOR_GETS_ROLES("Doesn't receive target's role", "Receive targets role", "Receives target's role"),
    JESTER_CAN_ANNOY("Cannot annoy people at night", "May annoy people at night", "May annoy people at night"),
    JESTER_KILLS("Upon day elimination, %s% will suicide", "", "% of guilty voters die"),
    LAST_WILL("Last wills are not allowed", "Last wills are allowed", ""),
    MARSHALL_EXECUTIONS("Adds %s more public executions to the day",
            "Nighttime phase is skipped until game is over, or faction has a majority", "Marshall Execution Count"),
    MARSHALL_QUICK_REVEAL("Executed people are revealed immediately.",
            "Executed people are revealed at the end of the day.", "Marshall Executions Revealed Immediately"),
    MASON_NON_CIT_RECRUIT("Can only recruit citizens", "May recruit citizens without powers",
            "Can recruit powerless non-citizens"),
    MASON_PROMOTION("Masons do not get promoted",
            "A random mason will replace Mason Leader upon original Mason Leader death", "Masons get promoted"),
    MAYOR_VOTE_POWER("After revealing, vote power will increase by %s", "", "Vote Power increase"),
    MILLER_SUITED("Will not look evil on death", "Will look evil on death", "Will look evil on death"),
    MM_SPREE_DELAY("After killing more than one person, must wait %s day(s) to use ability again",
            "No wait time between ability uses", "Spree Delay"),
    PROXY_UPGRADE("Faction won't get kill ability on this role card's death",
            "Faction will get kill ability on this role card's death", "On death, team receives faction kill"),
    PUNCH_ALLOWED("Day punches are disabled", "Day punches are enabled", ""),
    SHERIFF_PREPEEK("Has no pregame check", "Knows a team member at game start", "Knows team member at game start"),
    SELF_BREAD_USAGE("#" + Bread.ALIAS + "# cannot immediately be used by maker",
            "#" + Bread.ALIAS + "# can be immediately used by maker.", "#" + Bread.ALIAS + "# useable by maker."),
    SNITCH_PIERCES_SUIT("Does not pierce tailoring", "Pierces tailoring", "Pierces tailoring"),
    SPY_TARGETS_ALLIES("Cannot target allied factions", "Can target allied factions", "Can spy on allies"),
    SPY_TARGETS_ENEMIES("Cannot target enemy factions", "Can target enemy factions", "Can spy on enemies"),
    TAILOR_FEEDBACK("Suit receivers aren't informed of suit", "Suit receivers are informed of suit",
            "Suit receivers get notification"),
    WITCH_FEEDBACK("Controlled targets dont know they were targeted",
            "Controlled targets will be informed that they were manipulated", "Leaves Feedback");

    private static SetupModifier[] INT_MODIFIERS = {
            CIT_RATIO, VOTE_SYSTEM, CHARGE_VARIABILITY, DAY_LENGTH, NIGHT_LENGTH, DISCUSSION_LENGTH,
            ROLE_PICKING_LENGTH, TRIAL_LENGTH, MAYOR_VOTE_POWER, MARSHALL_EXECUTIONS, JESTER_KILLS, ARSON_DAY_IGNITES,
            MM_SPREE_DELAY, BOUNTY_INCUBATION, BOUNTY_FAIL_REWARD, CULT_POWER_ROLE_CD,
    };

    private final String string1, string2, string3;

    SetupModifier(String string1, String string2, String string3) {
        this.string1 = string1;
        this.string2 = string2;
        this.string3 = string3;
    }

    SetupModifier(String string1, String string2, String string3, int max, int min) {
        this.string1 = string1;
        this.string2 = string2;
        this.string3 = string3;
    }

    @Override
    public String getLabel(Narrator narrator) {
        String format = string3;
        String keyFormat;
        String value;
        for(String key: narrator.getAliases()){
            keyFormat = "#" + key + "#";
            value = narrator.getAlias(key);
            if(format.indexOf(keyFormat) == 0)
                value = Util.TitleCase(value);
            format = format.replaceAll(keyFormat, value);
        }
        return format;
    }

    @Override
    public String getActiveFormat() {
        return string2;
    }

    @Override
    public String getInactiveFormat() {
        return string1;
    }

    @Override
    public String getUnlimitedFormat() {
        return string2;
    }

    @Override
    public String getLimitedFormat() {
        return string1;
    }

    public static String getDescription(Narrator n, Modifier id, boolean value) {
        return SetupModifier.BoolDescription(n, value ? id.getActiveFormat() : id.getInactiveFormat());
    }

    public static String getDescription(Narrator n, Modifier id, int value) {
        return SetupModifier.IntDescription(n,
                value == SetupModifiers.UNLIMITED ? id.getUnlimitedFormat() : id.getLimitedFormat(), value);
    }

    public static String BoolDescription(Narrator n, String format) {
        String keyFormat;
        String value;
        for(String key: n.getAliases()){
            keyFormat = "#" + key + "#";
            value = n.getAlias(key);
            if(format.indexOf(keyFormat) == 0)
                value = Util.TitleCase(value);
            format = format.replaceAll(keyFormat, value);
        }
        return format;
    }

    public static String IntDescription(Narrator n, String format, int val) {
        StringBuilder sb = new StringBuilder();
        sb.append(val);
        return format.replace("%s", sb.toString());
    }

    public static String getLabel(Narrator n, SetupModifier id) {
        return id.getLabel(n);
    }

    public static boolean IsIntModifier(SetupModifier modifier) {
        return Arrays.asList(INT_MODIFIERS).contains(modifier);
    }

    public static boolean IsBoolModifier(SetupModifier modifier) {
        return !IsIntModifier(modifier);
    }
}
