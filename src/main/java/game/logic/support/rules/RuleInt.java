package game.logic.support.rules;

import java.util.HashMap;

import game.logic.exceptions.UnsupportedMethodException;
import game.logic.support.Constants;

public class RuleInt extends Rule {

    private int val;
    private Integer max, min;
    public String limitedFormat;
    public String unlimitedFormat;

    public RuleInt(Modifier id, int i) {
        super(id);
        this.val = i;
        // limitedFormat = id[2];
        // unlimitedFormat = id[3];
        max = Constants.MAX_INT_MODIFIER;// MEDIUM INT VALUE SQL
        min = -1;
    }

    public RuleInt(HashMap<AbilityModifier, Rule> modifierMap, Modifier rID) {
        this(rID, modifierMap != null && modifierMap.containsKey(rID) ? modifierMap.get(rID).getIntVal() : 0);
    }

    public static RuleInt creator(HashMap<RoleModifier, Rule> modifierMap, Modifier rID) {
        return new RuleInt(rID,
                modifierMap != null && modifierMap.containsKey(rID) ? modifierMap.get(rID).getIntVal() : 0);
    }

    @Override
    public boolean isInt() {
        return true;
    }

    public int getValue() {
        return val;
    }

    public void setValue(int i) {
        if(min != null && i < min){
            if(val < min)
                val = min;
            return;
        }
        if(max != null && i > max){
            if(val > max)
                val = max;
            return;
        }
        val = i;
    }

    @Override
    public void setInt(int i) {
        this.setValue(i);
    }

    @Override
    public void setBool(boolean b) {
        throw new UnsupportedMethodException();
    }

    public RuleInt setLowerLimit(int i) {
        min = i;
        return this;
    }

    public void setUpperLimit(int i) {
        max = i;
    }

    public void removeLimits() {
        min = null;
        max = null;

    }

    public void setIntVal(int intVal) {
        setValue(intVal);
    }

    @Override
    public int getIntVal() {
        return val;
    }

    @Override
    public boolean getBoolVal() {
        throw new UnsupportedMethodException();
    }

    @Override
    public Object getVal() {
        return val;
    }

    @Override
    public boolean isBool() {
        return false;
    }
}
