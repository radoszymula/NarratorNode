package game.logic.support.rules;

import java.util.Arrays;
import java.util.Collection;

import game.logic.Narrator;
import game.logic.support.Util;
import game.roles.support.Gun;
import game.roles.support.Vest;

public enum AbilityModifier implements Modifier {
    BACK_TO_BACK("May not target the same person/people with this ability",
            "May target the same person/people with this ability"),
    CHARGES("Limited to % charges", "No limits to how many times this ability may be used"),
    COOLDOWN("Must wait % days before using the ability again", "No limits to how often this ability may be used."),
    HIDDEN("This ability will be known to the player.", "This ability will not be known to the player."),
    SELF_TARGET("May not self target", "May self target"),
    ZERO_WEIGHTED("This action may not be submitted for free", "This action may be submitted for free"),

    AS_FAKE_VESTS("#" + Vest.ALIAS + "#s passed out cannot be fake", "#" + Vest.ALIAS + "#s passed out can be fake"),
    GS_FAULTY_GUNS("#" + Gun.ALIAS + "#s passed out are always safe to use",
            "#" + Gun.ALIAS + "# passed out can be faulty"),
    JAILOR_CLEAN_ROLES("Limited to % cleanings", "May clean every execution"),
    SECRET_KILL("Day killer name will be announced.", "Day killer name will not be announced."),
    SILENCE_ANNOUNCED("Silenced players will not be announced.", "Silenced players will be announced."),
    DISFRANCHISED_ANNOUNCED("Disfranchised players will not be announced.", "Disfranchised players will be announced.");

    private final String string1, string2;

    public static Collection<AbilityModifier> GENERIC_MODIFIERS = Arrays.asList(BACK_TO_BACK, CHARGES, COOLDOWN, HIDDEN,
            SELF_TARGET, ZERO_WEIGHTED);

    AbilityModifier(String string1, String string2) {
        this.string1 = string1;
        this.string2 = string2;
    }

    @Override
    public String getActiveFormat() {
        return string2;
    }

    @Override
    public String getInactiveFormat() {
        return string1;
    }

    @Override
    public String getUnlimitedFormat() {
        return string2;
    }

    @Override
    public String getLimitedFormat() {
        return string1;
    }

    @Override
    public String getLabel(Narrator narrator) {
        String format = string2;
        String keyFormat;
        String value;
        for(String key: narrator.getAliases()){
            keyFormat = "#" + key + "#";
            value = narrator.getAlias(key);
            if(format.indexOf(keyFormat) == 0)
                value = Util.TitleCase(value);
            format = format.replaceAll(keyFormat, value);
        }
        return format;
    }
}
