package game.logic.support.rules;

import java.util.HashMap;

import game.logic.exceptions.UnsupportedMethodException;

public class RuleBool extends Rule {

    private boolean val;

    public RuleBool(Modifier modifierID, boolean b) {
        super(modifierID);
        this.val = b;
    }

    public RuleBool(HashMap<AbilityModifier, Rule> modifierMap, Modifier modifierID) {
        this(modifierID, modifierMap != null && modifierMap.containsKey(modifierID));
    }

    public static RuleBool creator(HashMap<RoleModifier, Rule> modifierMap, Modifier modifierID) {
        return new RuleBool(modifierID, modifierMap != null && modifierMap.containsKey(modifierID));
    }

    @Override
    public boolean isInt() {
        return false;
    }

    public boolean getValue() {
        return val;
    }

    public void setValue(boolean value) {
        this.val = value;
    }

    @Override
    public int getIntVal() {
        throw new UnsupportedMethodException();
    }

    @Override
    public void setInt(int i) {
        throw new UnsupportedMethodException();
    }

    @Override
    public void setBool(boolean b) {
        this.val = b;
    }

    public void setBoolVal(boolean boolVal) {
        setValue(boolVal);
    }

    @Override
    public boolean getBoolVal() {
        return val;
    }

    @Override
    public Object getVal() {
        return val;
    }

    @Override
    public boolean isBool() {
        return true;
    }
}
