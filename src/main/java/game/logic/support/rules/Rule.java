package game.logic.support.rules;

public abstract class Rule {
    public Modifier id; // change to ModifierName

    public Rule(Modifier id) {
        this.id = id;
    }

    public abstract boolean isInt();

    public abstract boolean isBool();

    public abstract int getIntVal();

    public abstract boolean getBoolVal();

    public abstract void setInt(int int1);

    public abstract void setBool(boolean boolean1);

    public abstract Object getVal();

    @Override
    public String toString() {
        return id + " " + getVal();
    }

}
