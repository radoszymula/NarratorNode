package game.logic.support.rules;

import java.util.Arrays;
import java.util.HashMap;

import game.logic.Narrator;
import models.enums.VoteSystemTypes;

public class SetupModifiers {

    public static final int UNLIMITED = -1;

    public static final int DEFAULT_PLAYER_LIMIT = 15;

    public static final Modifier[] GENERAL_RULES = {
            SetupModifier.DAY_START,

            SetupModifier.ROLE_PICKING_LENGTH, SetupModifier.DAY_LENGTH, SetupModifier.NIGHT_LENGTH,
            SetupModifier.DISCUSSION_LENGTH, SetupModifier.TRIAL_LENGTH,

            SetupModifier.LAST_WILL, SetupModifier.CHAT_ROLES, SetupModifier.SKIP_VOTE, SetupModifier.HOST_VOTING,
            SetupModifier.SELF_VOTE, SetupModifier.SECRET_VOTES, SetupModifier.VOTE_SYSTEM,
            SetupModifier.CHARGE_VARIABILITY, SetupModifier.PUNCH_ALLOWED, SetupModifier.OMNISCIENT_DEAD
    };

    // when adding role rules, remember to add them to the role card too. they're
    // attached to the role, not to the 'member' class

    public HashMap<SetupModifier, Rule> rules;

    public SetupModifiers() {
        rules = new HashMap<>();
        setInt(SetupModifier.CIT_RATIO, 40);
        setBool(SetupModifier.DAY_START, Narrator.DAY_START);
        setBool(SetupModifier.HOST_VOTING, false);
        setBool(SetupModifier.SELF_VOTE, false);
        setBool(SetupModifier.SKIP_VOTE, true);
        setBool(SetupModifier.SECRET_VOTES, false);
        setInt(SetupModifier.VOTE_SYSTEM, VoteSystemTypes.PLURALITY);
        setBool(SetupModifier.LAST_WILL, true);
        setBool(SetupModifier.CHAT_ROLES, true);
        setBool(SetupModifier.PUNCH_ALLOWED, false);
        setBool(SetupModifier.OMNISCIENT_DEAD, true);
        setInt(SetupModifier.CHARGE_VARIABILITY, 0).setLowerLimit(0);
        setBool(SetupModifier.DIFFERENTIATED_FACTION_KILLS, false);

        setInt(SetupModifier.DAY_LENGTH, 240).setLowerLimit(60);
        setInt(SetupModifier.NIGHT_LENGTH, 180).setLowerLimit(0);
        setInt(SetupModifier.DISCUSSION_LENGTH, 0).setLowerLimit(0);
        setInt(SetupModifier.ROLE_PICKING_LENGTH, 30).setLowerLimit(30);
        setInt(SetupModifier.TRIAL_LENGTH, 0).setLowerLimit(0);

        setBool(SetupModifier.HEAL_FEEDBACK, true);
        setBool(SetupModifier.HEAL_SUCCESS_FEEDBACK, true);
        setBool(SetupModifier.HEAL_BLOCKS_POISON, false);

        setBool(SetupModifier.GUARD_REDIRECTS_DOUSE, false);
        setBool(SetupModifier.GUARD_REDIRECTS_CONVERT, false);
        setBool(SetupModifier.GUARD_REDIRECTS_POISON, false);

        setBool(SetupModifier.MILLER_SUITED, false);
        setBool(SetupModifier.GS_DAY_GUNS, false);
        setBool(SetupModifier.SPY_TARGETS_ENEMIES, true);
        setBool(SetupModifier.SPY_TARGETS_ALLIES, false);
        setBool(SetupModifier.ARCH_QUARANTINE, false);
        setBool(SetupModifier.ARCH_BURN, false);
        setBool(SetupModifier.CHECK_DIFFERENTIATION, true);
        setBool(SetupModifier.SHERIFF_PREPEEK, false);
        setBool(SetupModifier.FOLLOW_GETS_ALL, false);
        setBool(SetupModifier.FOLLOW_PIERCES_IMMUNITY, false);
        setBool(SetupModifier.CORONER_EXHUMES, true);
        setBool(SetupModifier.CORONER_LEARNS_ROLES, false);
        setBool(SetupModifier.BOMB_PIERCE, false);
        setInt(SetupModifier.MAYOR_VOTE_POWER, 2);
        setInt(SetupModifier.MARSHALL_EXECUTIONS, 2).setLowerLimit(1);
        setBool(SetupModifier.MARSHALL_QUICK_REVEAL, false);
        setBool(SetupModifier.BREAD_PASSING, true);
        setBool(SetupModifier.SELF_BREAD_USAGE, false);
        setBool(SetupModifier.MASON_PROMOTION, true);
        setBool(SetupModifier.MASON_NON_CIT_RECRUIT, true);
        setBool(SetupModifier.ENFORCER_LEARNS_NAMES, true);
        setBool(SetupModifier.BLOCK_FEEDBACK, true);

        setBool(SetupModifier.AMNESIAC_KEEPS_CHARGES, true);
        setBool(SetupModifier.AMNESIAC_MUST_REMEMBER, false);
        setInt(SetupModifier.JESTER_KILLS, 1).setLowerLimit(0).setUpperLimit(101);
        setBool(SetupModifier.JESTER_CAN_ANNOY, true);
        setBool(SetupModifier.EXECUTIONER_WIN_IMMUNE, false);
        setBool(SetupModifier.EXEC_TOWN, false);
        setBool(SetupModifier.EXECUTIONER_TO_JESTER, true);

        setBool(SetupModifier.WITCH_FEEDBACK, false);
        setInt(SetupModifier.ARSON_DAY_IGNITES, 1);
        setBool(SetupModifier.DOUSE_FEEDBACK, true);
        setInt(SetupModifier.MM_SPREE_DELAY, 0);

        setInt(SetupModifier.BOUNTY_INCUBATION, 2).setLowerLimit(1);
        setInt(SetupModifier.BOUNTY_FAIL_REWARD, 2);

        setBool(SetupModifier.CULT_PROMOTION, false);
        setBool(SetupModifier.CULT_KEEPS_ROLES, true);
        setInt(SetupModifier.CULT_POWER_ROLE_CD, 0);
        setBool(SetupModifier.CONVERT_REFUSABLE, false);

        setBool(SetupModifier.JANITOR_GETS_ROLES, false);
        setBool(SetupModifier.TAILOR_FEEDBACK, true);
        setBool(SetupModifier.SNITCH_PIERCES_SUIT, false);
        setBool(SetupModifier.GD_REANIMATE, true);

        setBool(SetupModifier.PROXY_UPGRADE, true);
    }

    public SetupModifiers(SetupModifiers r) {
        this();
        for(Modifier key: r.rules.keySet()){
            Rule old_rule = r.rules.get(key);
            if(old_rule.getClass() == RuleBool.class){
                RuleBool old_bool_rule = (RuleBool) old_rule;
                RuleBool new_bool_rule = (RuleBool) rules.get(key);
                new_bool_rule.setValue(old_bool_rule.getValue());
            }else{
                RuleInt old_int_rule = (RuleInt) old_rule;
                RuleInt new_int_rule = (RuleInt) rules.get(key);
                new_int_rule.setValue(old_int_rule.getValue());
            }
        }

    }

    public RuleInt setInt(SetupModifier modifier, int i) {
        if(rules.containsKey(modifier))
            rules.get(modifier).setInt(i);
        else
            rules.put(modifier, new RuleInt(modifier, i));
        return (RuleInt) rules.get(modifier);
    }

    public boolean setBool(SetupModifier modifier, boolean b) {
        RuleBool rb = new RuleBool(modifier, b);
        rules.put(modifier, rb);
        return b;
    }

    public int getInt(Modifier key) {
        return ((RuleInt) rules.get(key)).getValue();
    }

    public boolean getBool(Modifier key) {
        return ((RuleBool) rules.get(key)).getValue();
    }

    public Rule getRule(Modifier id) {
        return rules.get(id);
    }

    public static boolean IsRoleRule(Modifier r) {
        return !Arrays.asList(GENERAL_RULES).contains(r);
    }

}
