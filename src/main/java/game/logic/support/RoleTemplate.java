package game.logic.support;

import java.util.Comparator;

import game.logic.Player;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.Modifier;
import game.logic.support.rules.RoleModifier;
import game.roles.Ability;

public abstract class RoleTemplate {

    private Long id = null;

    public abstract String getName();

    public abstract boolean isBoolModifier(Modifier modifier);

    public abstract boolean isIntModifier(Modifier modifier);

    public abstract boolean isRandom();

    public abstract boolean contains(String simpleName);

    public abstract boolean contains(Class<? extends Ability> c);

    public abstract void prefer(Player p);

    // public abstract RoleTemplate copy();

    public static Comparator<RoleTemplate> NameComparator = new Comparator<RoleTemplate>() {
        @Override
        public int compare(RoleTemplate o1, RoleTemplate o2) {
            return o1.getName().compareTo(o2.getName());
        }

    };

    public void setID(long id) {
        this.id = id;
    }

    public Long getID() {
        return id;
    }

    public static boolean GetDefaultValue(Modifier modifier, Class<? extends Ability> abilityClass) {
        if(modifier != AbilityModifier.SELF_TARGET)
            return false;
        Ability a = Ability.CREATOR(abilityClass.getSimpleName());
        if(!a.canEditSelfTargetableModifer())
            throw new IllegalGameSettingsException();
        return a.getDefaultSelfTargetValue();
    }

    public abstract RoleTemplate addModifier(RoleModifier modifier, boolean val); // can i remove these

    public abstract RoleTemplate addModifier(RoleModifier modifier, int val);

    // public abstract RoleTemplate copy(boolean deepCopy);

    public abstract RoleTemplate addAbility(Class<? extends Ability> a);

    public abstract void removeAbility(Class<? extends Ability> a);
}
