package game.logic.support;

public interface ActionTaker {

    public void doNightAction();

    public int getSubmissionTime();

}
