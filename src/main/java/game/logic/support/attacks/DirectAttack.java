package game.logic.support.attacks;

import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.rules.SetupModifier;
import game.logic.support.saves.BGHeal;
import game.logic.support.saves.Heal;
import game.roles.Veteran;

public class DirectAttack extends Attack {

    Player attacker, injured, cause;
    String[] type;

    public DirectAttack(Player attacker, Player injured, String[] type) {
        super(attacker);
        this.type = type;
        this.injured = injured;
        this.attacker = attacker;
        this.cause = attacker;
    }

    public boolean isImmune() {
        if(attacker == injured)
            return false;
        if(!injured.isInvulnerable()){
            if(injured.isJailed())
                return type != Constants.JAIL_KILL_FLAG && type != Constants.ARSON_KILL_FLAG;
            return false;
        }
        if(type == Constants.VETERAN_KILL_FLAG)
            return false;
        if(type == Constants.JAIL_KILL_FLAG)
            return false;
        if(type == Constants.BODYGUARD_KILL_FLAG && !injured.is(Veteran.class))
            return false;
        if(injured.isVesting() && type == Constants.ARSON_KILL_FLAG)
            return false;
        return true;
    }

    public Player getAttacker() {
        return attacker;
    }

    public String[] getType() {
        return type;
    }

    public boolean isHealable(Heal type) {
        if(getType() == Constants.POISON_KILL_FLAG)
            return injured.narrator.getBool(SetupModifier.HEAL_BLOCKS_POISON) && !(type instanceof BGHeal);
        return this.type != Constants.JAIL_KILL_FLAG;
    }

    public void counterAttack(Heal type) {
        super.counterAttack(type);
        if(type instanceof BGHeal){
            type.getHealer().kill(new BodyguardAttack(this, type.getHealer()));
            if(!attacker.is(Veteran.class) || this.type != Constants.VETERAN_KILL_FLAG)
                attacker.kill(new BodyguardAttack(this, type.getHealer()));
        }

    }

    public PlayerList getInjured() {
        return Player.list(injured);
    }

    public Player getCause() {
        return cause;
    }

    public void setCause(Player cause) {
        this.cause = cause;
    }
}
