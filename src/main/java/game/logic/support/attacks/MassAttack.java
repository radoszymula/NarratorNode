package game.logic.support.attacks;

import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.saves.BGHeal;
import game.logic.support.saves.Heal;

public class MassAttack extends Attack {

    public Player attacker, injured_single;
    public PlayerList co_injured;

    public MassAttack(Player attacker, PlayerList co_injured, Player injured_single) {
        super(attacker);
        this.attacker = attacker;
        this.injured_single = injured_single;
        this.co_injured = co_injured;
    }

    public boolean isImmune() {
        return injured_single.isInvulnerable();
    }

    public String toString() {
        return attacker + " spree\n" + co_injured;
    }

    public String[] getType() {
        return Constants.MASS_MURDERER_FLAG;
    }

    public boolean isHealable(Heal type) {
        return true;
    }

    public void counterAttack(Heal type) {
        super.counterAttack(type);
        if(type instanceof BGHeal){
            type.getHealer().kill(new BodyguardAttack(this, type.getHealer()));
            attacker.kill(new BodyguardAttack(this, type.getHealer()));
            if(((BGHeal) type).attacksDone)
                return;
            ((BGHeal) type).attacksDone = true;
            for(Player injured: co_injured){
                if(injured != injured_single){
                    injured.heal(type);
                }
            }
        }
    }

    public PlayerList getInjured() {
        return co_injured;
    }

    public Player getAttacker() {
        return attacker;
    }

    public Player getCause() {
        return attacker;
    }
}
