package game.logic.support.attacks;

import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.saves.BGHeal;
import game.logic.support.saves.Heal;

public class BodyguardAttack extends Attack {

    Attack dAttack;
    Player bodyguard;

    public BodyguardAttack(Attack savedAttack, Player bodyguard) {
        super(bodyguard);
        this.dAttack = savedAttack;
        this.bodyguard = bodyguard;
    }

    public Player getCause() {
        return bodyguard;
    }

    public String[] getType() {
        return Constants.BODYGUARD_KILL_FLAG;
    }

    public boolean isImmune() {
        // if(dAttack.getAttacker().is(Veteran.class))
        // return false;
        return false;
    }

    public String toString() {
        return "BodyguardAttack: [" + dAttack.getAttacker() + " - " + bodyguard + "]";
    }

    public boolean isHealable(Heal type) {
        return true;
    }

    public void counterAttack(Heal type) {
        super.counterAttack(type);
        if(type instanceof BGHeal){
            type.getHealer().kill(new BodyguardAttack(this, type.getHealer()));
        }

    }

    public PlayerList getInjured() {
        return Player.list(dAttack.getAttacker());
    }

    public Player getAttacker() {
        return bodyguard;
    }
}
