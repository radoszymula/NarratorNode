package game.logic.support;

import game.logic.Narrator;

public interface Alignment {

    boolean opposes(Alignment a2, Narrator n);

    String[] getFactionColors();

}
