package game.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

import game.logic.support.Random;
import game.logic.support.RolePackage;
import game.roles.Ability;
import game.roles.Hidden;
import models.FactionRole;
import models.SetupHidden;

public class RolesList implements Iterable<SetupHidden> {

    public ArrayList<SetupHidden> _setupHiddenList;
    private HashMap<String, Boolean> roleNameMap;
    public HashMap<Hidden, ArrayList<FactionRole>> spawns;
    public HashMap<Hidden, ArrayList<FactionRole>> usedSpawns;

    public RolesList() {
        _setupHiddenList = new ArrayList<>();
        roleNameMap = new HashMap<>();
        spawns = new HashMap<>();
        usedSpawns = new HashMap<>();
    }

    public RolesList(Collection<RolePackage> values) {
        this();
        for(RolePackage rt: values)
            add(rt.assignedHidden);
    }

    public boolean hasTeam(FactionList teams) {
        for(SetupHidden setupHidden: _setupHiddenList){
            for(Faction faction: teams){
                if(setupHidden.hidden.hasColor(faction.getColor()))
                    return true;
            }
        }
        return false;
    }

    public synchronized SetupHidden add(Hidden hidden) {
        SetupHidden setupHidden = new SetupHidden(hidden);
        _setupHiddenList.add(setupHidden);
        this.sort();
        return setupHidden;
    }

    public int size() {
        return _setupHiddenList.size();
    }

    public void shuffle(Random random) {
        random.shuffle(_setupHiddenList, "roles list");
    }

    public RolesList sort() {
        Collections.sort(_setupHiddenList, SetupHidden.RandomComparator());
        return this;
    }

    public RolesList assignSort() {
        Collections.sort(_setupHiddenList, new Comparator<SetupHidden>() {

            @Override
            public int compare(SetupHidden setupHidden1, SetupHidden setupHidden2) {
                Hidden hidden1 = setupHidden1.hidden;
                Hidden hidden2 = setupHidden2.hidden;

                int unique1 = hidden1.getUniqueCount();
                int unique2 = hidden2.getUniqueCount();

                if(unique1 == unique2)
                    return hidden1.getSize() - hidden2.getSize();
                return unique1 - unique2;
            }
        });
        return this;
    }

    public SetupHidden get(int i) {
        return _setupHiddenList.get(i);
    }

    public boolean remove(Hidden hidden) {
        Iterator<SetupHidden> iterator = _setupHiddenList.iterator();
        while (iterator.hasNext()){
            if(iterator.next().hidden == hidden){
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public void remove(SetupHidden setupHidden) {
        this._setupHiddenList.remove(setupHidden);
    }

    public void removeAll(Hidden hidden) {
        Iterator<SetupHidden> iterator = _setupHiddenList.iterator();
        while (iterator.hasNext()){
            if(iterator.next().hidden == hidden)
                iterator.remove();
        }
    }

    public boolean contains(Class<? extends Ability> c) {
        for(SetupHidden setupHidden: this){
            if(setupHidden.hidden.contains(c))
                return true;
        }
        return false;
    }

    public boolean contains(Class<? extends Ability> abilityClass, String color) {
        for(SetupHidden setupHidden: this){
            for(FactionRole factionRole: setupHidden.hidden.factionRolesMap.values()){
                if(factionRole.faction.getColor().equals(color) && factionRole.role.contains(abilityClass))
                    return true;
            }
        }
        return false;
    }

    public boolean contains(Hidden hidden) {
        for(SetupHidden setupHidden: this){
            if(hidden == setupHidden.hidden)
                return true;
        }
        return false;
    }

    public boolean contains(FactionRole factionRole) {
        for(SetupHidden setupHidden: this){
            if(setupHidden.hidden.contains(factionRole))
                return true;
        }
        return false;
    }

    public boolean contains(Role role) {
        for(SetupHidden setupHidden: this){
            if(setupHidden.hidden.contains(role))
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(SetupHidden rt: _setupHiddenList){
            sb.append(rt.hidden.toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public RolesList copy() {
        RolesList rl = new RolesList();
        for(SetupHidden setupHiddenInput: _setupHiddenList){
            SetupHidden setupHiddenOutput = rl.add(setupHiddenInput.hidden);
            setupHiddenOutput.id = setupHiddenInput.id;
            setupHiddenOutput.isExposed = setupHiddenInput.isExposed;
        }
        return rl;
    }

    public int count(Role a) {
        int count = 0;
        for(SetupHidden setupHidden: this)
            if(setupHidden.hidden.contains(a))
                count++;
        return count;
    }

    public int count(Class<? extends Ability> a) {
        int count = 0;
        for(SetupHidden setupHidden: this){
            if(setupHidden.hidden.contains(a))
                count++;
        }
        return count;
    }

    public boolean contains(String simpleName) {
        Boolean contains = roleNameMap.get(simpleName);
        if(contains != null)
            return contains;
        for(SetupHidden setupHidden: _setupHiddenList){
            if(setupHidden.hidden.contains(simpleName)){
                roleNameMap.put(simpleName, true);
                return true;
            }
        }
        roleNameMap.put(simpleName, false);
        return false;
    }

    public MemberList getMembers() {
        MemberList members = new MemberList();
        for(SetupHidden setupHidden: this){
            for(FactionRole m: setupHidden.hidden.getFactionRoles())
                members.add(m);
        }
        return members;
    }

    public void removeHead() {
        _setupHiddenList.remove(0);
    }

    @Override
    public int hashCode() {
        return copy().sort()._setupHiddenList.hashCode();
    }

    public void addSpawn(Hidden hidden, FactionRole spawn) {
        if(!spawns.containsKey(hidden))
            spawns.put(hidden, new ArrayList<>());
        spawns.get(hidden).add(spawn);
    }

    public void resetSpawns() {
        this.usedSpawns.clear();
    }

    public void clearSetSpawns() {
        this.spawns.clear();
    }

    @Override
    public Iterator<SetupHidden> iterator() {
        return this._setupHiddenList.iterator();
    }
}
