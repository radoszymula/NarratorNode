package game.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.ai.Controller;
import game.ai.ControllerList;
import game.event.ArchitectChat;
import game.event.ChatMessage;
import game.event.EventList;
import game.event.EventLog;
import game.event.FactionChat;
import game.event.Feedback;
import game.event.JailChat;
import game.event.Message;
import game.event.OGIMessage;
import game.event.RoleCreatedChat;
import game.event.SelectionMessage;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.PhaseException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnsupportedMethodException;
import game.logic.exceptions.VotingException;
import game.logic.listeners.NarratorListener;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.Option;
import game.logic.support.RolePackage;
import game.logic.support.Shuffler;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.logic.support.attacks.Attack;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.logic.support.saves.Heal;
import game.roles.Ability;
import game.roles.AbilityList;
import game.roles.Amnesiac;
import game.roles.Bodyguard;
import game.roles.Bomb;
import game.roles.BreadAbility;
import game.roles.Bulletproof;
import game.roles.Burn;
import game.roles.CultLeader;
import game.roles.CultPendingInvitation;
import game.roles.Cultist;
import game.roles.Disguiser;
import game.roles.Doctor;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.ElectroManiac;
import game.roles.FactionSend;
import game.roles.Ghost;
import game.roles.Goon;
import game.roles.GunAbility;
import game.roles.Gunsmith;
import game.roles.Interceptor;
import game.roles.ItemAbility;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Jester;
import game.roles.Joker;
import game.roles.Marshall;
import game.roles.MasonLeader;
import game.roles.MassMurderer;
import game.roles.Miller;
import game.roles.Poisoner;
import game.roles.Punch;
import game.roles.PuppetVote;
import game.roles.SerialKiller;
import game.roles.Spy;
import game.roles.Stripper;
import game.roles.Survivor;
import game.roles.Ventriloquist;
import game.roles.VestAbility;
import game.roles.Veteran;
import game.roles.Vigilante;
import game.roles.support.Bounty;
import game.roles.support.BountyList;
import game.roles.support.Bread;
import game.roles.support.CultInvitation;
import game.roles.support.ElectrocutionException;
import game.roles.support.Gun;
import game.roles.support.Suit;
import game.roles.support.Vest;
import models.FactionRole;
import models.Modifiers;
import models.enums.GamePhase;
import models.enums.PlayerStatus;
import util.ModifierUtil;

public class Player implements Controller {

    public Narrator narrator;
    private HashMap<PlayerStatus, Object> statusii;
    public long databaseID;

    // public because command handler will use this to make 'fake players' that
    // aren't added to the narrator object
    public Player(String name, Narrator narrator) {
        this.name = name;
        this.databaseID = Math.abs(name.hashCode());
        this.id = name;
        this.icon = narrator.getIcon();
        this.narrator = narrator;
        actions = new ActionList(this);
        rPrefers = new ArrayList<>();
        _tPrefers = new ArrayList<>();
        jailChats = new HashMap<>();
        archChats = new HashMap<>();
        lastWillTrustees = new HashMap<>();
        chats = new ArrayList<>();

        visits = new PlayerList();
        visitors = new PlayerList();
        roleVisitors = new ArrayList<>();

        puppets = new PlayerList();
        masters = new PlayerList();

        secondaryCauses = new PlayerList();
        statusii = new HashMap<>();
    }

    public void sendMessage(Message message) {
        List<NarratorListener> listeners = narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onMessageReceive(this, message);
    }

    public void warn(Message message) {
        List<NarratorListener> listeners = narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onWarningReceive(this, message);
    }

    @Override
    public final boolean is(Class<? extends Ability> inputClass) {
        if(role == null)
            return false;
        return role.is(inputClass);
    }

    public boolean willBe(Class<?>... inputClass) {
        if(pendingRole == null)
            return false;
        for(Class<?> i: inputClass)
            for(Ability a: pendingRole.role._abilities)
                if(a.getClass() == i)
                    return true;
        return false;
    }

    public String _color;

    public Faction getFaction() {
        return narrator.getFaction(_color);
    }

    public boolean isOnTeam(String color) {
        for(Faction t: getFactions()){
            if(t.getColor().equals(color))
                return true;
        }
        return false;
    }

    public RolePackage initialAssignedRole;
    private FactionRole initialFactionRole;
    private FactionRole initialPlayableFactionRole;
    public FactionRole factionRole;

    public String getInitialColor() {
        return initialAssignedRole.assignedRole.getColor();
    }

    public AbilityList getFirstPhaseAbilities() {
        return initialPlayableFactionRole.role.getBaseAbility();
    }

    public AbilityList getInitialAbilities() {
        return initialFactionRole.role.getBaseAbility();
    }

    public void addAbility(Ability a0) {
        a0.initialize(this);
        ArrayList<Ability> list = new ArrayList<>();
        for(Ability a: role._abilities){
            list.add(a);
        }
        list.add(a0);
        role._abilities = new AbilityList(list);
    }

    public void replaceAbility(Ability oldR, Ability newR) {
        newR.initialize(this);
        this.role._abilities = this.role._abilities.replace(oldR, newR);
    }

    public Role role;

    public void setRole(RolePackage rt) {
        initialAssignedRole = rt;
        initialFactionRole = rt.assignedRole.copy();

        this.setRole(initialFactionRole);
    }

    public void setRole(FactionRole factionRole) {
        if(!narrator.firstPhaseStarted())
            this.initialPlayableFactionRole = factionRole;

        this.factionRole = factionRole;
        this.role = factionRole.role;

        // role and factionrole modifier smash goes here
        ModifierUtil.resolveOverrides(this.factionRole);

        Ability.initialize(this.role, this);
        this._color = factionRole.getColor();
    }

    public AbilityList getAbilities() {
        return role._abilities;
    }

    public ArrayList<Ability> tempAbilities;

    public void addTempAbility(Ability a) {
        if(a == null)
            throw new NullPointerException();
        if(tempAbilities == null)
            tempAbilities = new ArrayList<>();
        tempAbilities.add(a);
    }

    /*
     * public Ability getRole(){ return currentRole; }
     */
    public String getDescription() {
        return getDescription(name, role);
    }

    public String getDescription(String nameToUse, Role prevRole) {
        boolean isInProgress = narrator.isInProgress();
        if(isAlive() && isInProgress || !narrator.isStarted())
            return nameToUse;
        if(Marshall.isCurrentMarshallLynch(this))
            return nameToUse;
        String roleName;
        if(isInProgress && this.getDeathType().isCleaned())
            roleName = Constants.JANITOR_DESCRIP;
        else if(hasSuits())
            roleName = suits.get(0).roleName;
        else
            roleName = prevRole.name;
        return nameToUse + "(" + roleName + ")";
    }

    public String getGraveyardRoleName() {
        boolean isInProgress = narrator.isInProgress();
        if(!narrator.isStarted())
            throw new UnsupportedMethodException();
        if(!narrator.isInProgress())
            return role.name;

        if(this.getDeathType().isCleaned() && isInProgress)
            return null;
        else if(hasSuits())
            return suits.get(0).roleName;
        else if(is(Miller.class) && narrator.getBool(SetupModifier.MILLER_SUITED))
            return Miller.getRoleFlip(this).getName();
        else
            return role.getName();
    }

    public String getGraveyardTeamName() {
        boolean isInProgress = narrator.isInProgress();
        if(!narrator.isStarted())
            throw new UnsupportedMethodException();
        if(!narrator.isInProgress())
            return getFaction().getName();

        if(this.getDeathType().isCleaned() && isInProgress)
            return null;
        else if(hasSuits())
            return narrator.getFaction(suits.get(0).color).getName();
        else if(is(Miller.class) && narrator.getBool(SetupModifier.MILLER_SUITED))
            return narrator.getFaction(Miller.MillerColor(narrator)).getName();
        else
            return narrator.getFaction(this._color).getName();
    }

    public String getGraveyardColor() {
        boolean isInProgress = narrator.isInProgress();
        if(!narrator.isStarted())
            throw new UnsupportedMethodException();
        if(!narrator.isInProgress())
            return getFaction().getColor();

        if(this.getDeathType().isCleaned() && isInProgress)
            return Constants.A_SKIP;
        else if(hasSuits() && isInProgress)
            return suits.get(0).color;
        else if(is(Miller.class) && narrator.getBool(SetupModifier.MILLER_SUITED))
            return Miller.MillerColor(narrator);
        else
            return this._color;
    }

    @Override
    public String getRoleName() {
        return role.name;
    }

    @Override
    public String getColor() {
        return _color;
    }

    public PlayerList getAcceptableTargets(String command) {
        return getAcceptableTargets(Ability.ParseAbility(command));
    }

    // returns null if its a toggle ability
    public PlayerList getAcceptableTargets(int command) {
        if(Ability.is(command, Constants.RECRUITMENT))
            return null;

        Ability a = getAbility(command);
        if(a == null)
            return new PlayerList();
        return getAbility(command).getAcceptableTargets(this);
    }

    public Action action(Player target, String command) {
        return new Action(this, Ability.ParseAbility(command), target);
    }

    public Action action(Player target, int ability) {
        return new Action(this, ability, target);
    }

    public Action action(String command) {
        return new Action(this, new PlayerList(), Ability.ParseAbility(command), (String) null, null);
    }

    public Action action(Player target) {
        return action(target, role._abilities.get(0).getCommand());
    }

    public boolean isAcceptableTarget(Action a) {
        if(hasAbility(a.ability))
            return getAbility(a.ability).isAcceptableTarget(a);
        return false;
    }

    public int nightVotePower(PlayerList teamMembers) {
        int nvp = 0;
        for(Ability a: role._abilities)
            nvp = Math.max(nvp, a.nightVotePower(this, teamMembers));
        return nvp;
    }

    public static String getName(Player p, Player target) {
        if(p.equals(target))
            return "You";
        return target.getName();
    }

    @Override
    public ArrayList<String> getCommands() {
        ArrayList<String> commands = new ArrayList<>();
        for(Ability a: getAvailableActions()){
            if(a.getCommand() != null)
                commands.add(a.getCommand());
        }
        return commands;
    }

    private boolean availableAbility(Ability a) {
        if(a.getCommand() == null)
            return false;
        if(a.getPerceivedCharges() == 0)
            return false;
        if(a.isOnCooldown())
            return false;
        if(isDead() && !a.is(Ghost.MAIN_ABILITY))
            return false;

        // handling phase restrictions
        boolean isNightAbility = a.isNightAbility(this);
        boolean isDayAbility = a.canUseDuringDay(this);
        if(isNightAbility && isDayAbility)
            return true;
        else if(isNightAbility && narrator.isNight())
            return true;
        else if(isDayAbility && narrator.isDay())
            return true;
        return false;
    }

    public ArrayList<Ability> getAvailableActions() {
        ArrayList<Ability> list = new ArrayList<>();

        if(isJailed())
            return list;
        for(Ability a: role._abilities)
            if(availableAbility(a))
                list.add(a);

        FactionList t = getFactions();
        boolean alreadyHas;
        for(Faction team: t){
            if(!team.hasSharedAbilities())
                continue;
            for(Ability s: team.getAbilities()){
                if(!availableAbility(s))
                    continue;
                if(s.is(FactionSend.MAIN_ABILITY) && t.size() == 1)
                    continue;
                alreadyHas = false;
                for(Ability acceptableAbilities: list)
                    if(acceptableAbilities.getAbilityNumber() == s.getAbilityNumber()){
                        alreadyHas = true;
                        break;
                    }
                // team abilities must belong to the player to be available, or this is
                // 'potentially' available.
                if(narrator.nightPhase != GamePhase.NIGHT_ACTION_SUBMISSION
                        && team.getCurrentController(s.getAbilityNumber()) != this
                        && team.getCurrentController(s.getAbilityNumber()) != null)
                    continue;
                if(!alreadyHas)
                    list.add(s);
            }
        }
        return list;
    }

    public boolean hasVigiGuns() {
        if(!hasAbility(GunAbility.class))
            return false;
        return getAbility(GunAbility.class).hasVigiGuns();
    }

    public HashMap<String, ArrayList<Option>> getOptions() {
        HashMap<String, ArrayList<Option>> map = new HashMap<>();
        for(Ability a: role._abilities)
            map.put(a.getCommand(), a.getOptions(this));
        return map;
    }

    public ArrayList<Option> getOptions(String command) {
        return getAbility(command).getOptions(this);
    }

    public HashMap<Option, ArrayList<Option>> getOptions2(int ability) {
        HashMap<Option, ArrayList<Option>> map = new HashMap<>();
        Ability a = getAbility(ability);
        for(Option s: a.getOptions(this)){
            map.put(s, a.getOptions2(this, s.getValue()));
        }

        return map;
    }

    public FactionList getFactions() {
        FactionList list = new FactionList();
        for(Faction t: narrator.getFactions()){
            if(t.hasMember(this))
                list.add(t);
        }
        if(isDead() && list.isEmpty()){
            list.add(narrator.getFaction(_color));
        }
        return list;
    }

    private HashMap<Integer, ActionList> prevNightTargets = new HashMap<>();
    private HashMap<Integer, ActionList> prevDayTargets = new HashMap<>();

    public ActionList getPrevDayTarget() {
        return prevDayTargets.get(narrator.getDayNumber() - 1);
    }

    public ActionList getPrevNightTarget() {
        return prevNightTargets.get(narrator.getDayNumber() - 1);
    }

    public ActionList getPrevNightTarget(int day) {
        return prevNightTargets.get(day);
    }

    public void saveNightTargets() {
        actions.cleanup();
        prevNightTargets.put(narrator.getDayNumber(), actions);
    }

    public void saveDayTargets() {
        prevDayTargets.put(narrator.getDayNumber(), actions);
    }

    private int submissionTime;

    public void targetingPreconditionChecks(Action a) {
        if(isDead() && (!is(Ghost.class) || !a.is(Ghost.COMMAND)))
            throw new PlayerTargetingException("Cannot set targets if dead");
        if(!narrator.isStarted())
            throw new PhaseException("Can't submit night actions if game isn't started");
        if(!narrator.canDoNightAction() && a.isNightAction() && !a.isDayAction())
            throw new PhaseException("Night actions are not allowed right now!");
        if(a.getTargets() == null || a.getTargets().contains(getSkipper()))
            throw new PlayerTargetingException("Cannot target nobody");
        if(narrator.isDay() && !a.isDayAction())
            throw new PlayerTargetingException("Cannot set targets during the day");
        if(narrator.endedNight(this))
            throw new PlayerTargetingException(
                    getName() + " has already ended the night. Cancel end night to change abilities.");

        if(isJailed()){
            throw new PlayerTargetingException("Cannot submit actions while in jail");
        }

        Ability ability = a.getAbility();
        if(ability == null){
            throw new PlayerTargetingException("You do not have this ability");
        }

        ability.checkAcceptableTarget(a);
    }

    public boolean setTarget(Action a) {
        return setTarget(CHECK, a.ability, a.getOption(), a.getOption2(), a.getOption3(), a.getTargets());
    }

    private static final boolean CHECK = true;

    public boolean setTarget(int ability, String option, String option2, String opt3, Player... players) {
        return setTarget(CHECK, ability, option, option2, opt3, Player.list(players));
    }

    public boolean setTarget(int ability, String option, String option2, Player... players) {
        return setTarget(CHECK, ability, option, option2, null, Player.list(players));
    }

    public boolean setTarget(int ability, String option, String option2, PlayerList players) {
        return setTarget(CHECK, ability, option, option2, null, players);
    }

    public boolean setTarget(int ability, String option, String option2, String option3, PlayerList players) {
        return setTarget(CHECK, ability, option, option2, option3, players);
    }

    // change option into string array
    private boolean setTarget(boolean checkAction, int ability, String option, String option2, String option3,
            PlayerList targets) {
        // untargets previous person without notificaition, because a previous command
        // was registered, and it needs to be canceled

        Action a = new Action(this, targets, ability, option, option2, option3);

        if(checkAction)
            targetingPreconditionChecks(a);

        SelectionMessage sm = addAction(a);// takes care of popping off abilities as needed

        List<NarratorListener> listeners = narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onTargetSelection(this, sm);
        return true;
    }

    private SelectionMessage addAction(Action action) {

        HashMap<Faction, Player> before;
        if(action.isTeamAbility())
            before = action.getFactions().getAbilityControllers(action.ability);
        else
            before = null;

        SelectionMessage e = new SelectionMessage(this, true);
        Action newAction = Action.pushCommand(e, action, actions)[0];// 0 IS THE NEW ACTION

        if(!nightIsEnding())
            newAction.pushCommand(e);

        e.pushOut();
        teamWarnChange(action, newAction, e, before);
        warnAbilityFail(newAction);

        return e;
    }

    private void teamWarnChange(Action action, Action newAction, SelectionMessage e, HashMap<Faction, Player> before) {
        HashMap<Faction, Player> after;
        if(!action.isTeamAbility())
            return;
        if(!narrator.getPossibleMembers().hasMembersThatAffectSending(narrator))
            return;
        // if(getActions().isTargeting(this, Team.SEND_))
        // return;

        FactionList changedDesignations = new FactionList();
        for(Faction t: before.keySet()){
            changedDesignations.add(t);
        }
        if(changedDesignations.isEmpty())
            return;

        e = new SelectionMessage(this, true);

        Action sendAction = action(this, Faction.SEND);
        if(!newAction.is(FactionSend.MAIN_ABILITY))
            actions.addAction(sendAction);
        e.pushOut();

        after = action.getFactions().getAbilityControllers(action.ability);
        for(Faction t: changedDesignations)
            Faction.warnAbilityOwnerChange(this, this, before.get(t), after.get(t), t);
    }

    private void warnAbilityFail(Action newAction) {
        if(!this.in(newAction._targets))
            return;

        Ability a = newAction.getAbility();
        if(!a.canEditSelfTargetableModifer())
            return;

        String warningMessage = "Your attempt to use the " + a.getClass().getSimpleName()
                + " ability on yourself will fail unless somoene manipulates your action.";
        this.warn(new OGIMessage().add(warningMessage));
    }

    private boolean nightIsEnding() {
        int liveAndGhost = narrator.getLiveSize();
        liveAndGhost += narrator._players.filter(Ghost.class).getDeadPlayers().size();
        return narrator.isNight() && narrator.getEndedNightPeople().size() == liveAndGhost;
    }

    public boolean alliesWith(Player p) {
        return getFaction().getMembers().contains(p);
    }

    public PlayerList getTargets(int ability) {
        return actions.getTargets(ability);
    }

    public Player getHouseTarget() {
        return house;
    }

    public void cancelTarget(int ability) {
        PlayerList target = actions.getTargets(ability);
        if(target != null)
            cancelTarget(target, ability, true);
    }

    public void cancelTarget(Player t, int a) {
        cancelTarget(Player.list(t), a, true);
    }

    public void cancelTarget(PlayerList target, int ability) {
        cancelTarget(target, ability, true);
    }

    public void cancelTarget(PlayerList target, int ability, boolean setCommand) {
        Action removedAction = actions.remove(target, ability, setCommand);
        if(removedAction != null){
            String command = removedAction.getCommand();
            List<NarratorListener> listeners = narrator.getListeners();
            for(int i = 0; i < listeners.size(); i++)
                listeners.get(i).onTargetRemove(this, command, target);
        }

    }

    // this is public facing. do not use internally
    @Override
    public void clearTargets() {

        HashMap<Integer, HashMap<Faction, Player>> action = new HashMap<>();
        FactionList teams = getFactions();
        for(Ability a: teams.getAbilities())
            action.put(a.getAbilityNumber(), teams.getAbilityControllers(a.getAbilityNumber()));

        actions.clear();

        Player controller;
        for(Ability a: teams.getAbilities()){
            for(Faction t: action.get(a.getAbilityNumber()).keySet()){
                controller = t.getCurrentController(a.getAbilityNumber());
                Faction.warnAbilityOwnerChange(this, null, action.get(a.getAbilityNumber()).get(t), controller,
                        getFaction());
            }
        }
    }

    private Player house = this;

    public void setHouse(Player p) {
        house = p;
        // if(!busDriven){
        new Feedback(this, Driver.FEEDBACK).setPicture("driver").addExtraInfo(
                "Anyone who attempted to target you probably ended up targeting someone else, but also vice versa.");
        ;
        statusii.put(PlayerStatus.BUS_DRIVEN, true);
        // }
    }

    public void setHide(Player p) {
        house = p;
    }

    public void setAddress(Player p) {
        house = p;
    }

    public Player getAddress() {
        return house;
    }

    public boolean busDriven() {
        return statusii.containsKey(PlayerStatus.BUS_DRIVEN);
    }

    public boolean isInTown() {
        return house != null;
    }

    public boolean hasSuccessfulBounty() {
        return statusii.containsKey(PlayerStatus.SUCCESSFUL_BOUNTY);
    }

    public Player getSuccessfulBounties() {
        return (Player) statusii.get(PlayerStatus.SUCCESSFUL_BOUNTY);
    }

    public boolean isBountied() {
        return statusii.containsKey(PlayerStatus.JOKER_PLAYER_STATUS_KEY) && !getBounties().isEmpty();
    }

    public Bounty setBounty(Player joker) {
        BountyList bounties;
        if(isBountied()){
            bounties = getBounties();
        }else{
            bounties = new BountyList(this);
            statusii.put(PlayerStatus.JOKER_PLAYER_STATUS_KEY, bounties);
        }
        return bounties.add(joker, narrator.getDayNumber());
    }

    public BountyList getBounties() {
        return (BountyList) statusii.get(PlayerStatus.JOKER_PLAYER_STATUS_KEY);
    }

    private boolean completedNightAction = false;

    private ActionList actions;

    public ActionList getActions() {
        return actions;
    }

    public Action getAction(int ability) {
        for(Action a: actions){
            if(a.is(ability))
                return a;
        }
        return null;
    }

    public boolean isSubmitting(int... possibleActions) {
        for(Action a: this.actions){
            if(a.is(possibleActions))
                return true;
        }
        return false;
    }

    public Ability getFirstAbility() {
        return role._abilities.get(0);
    }

    // this is necessary because some abilities don't have a command. the passives
    @SuppressWarnings("unchecked")
    public <T> T getAbility(Class<T> ability) {
        for(Ability abl: role._abilities){
            if(abl.getClass().equals(ability))
                return (T) abl;
        }
        for(Ability abl: getFactions().getAbilities()){
            if(abl.getClass().equals(ability))
                return (T) abl;
        }
        return null;
    }

    public Ability getAbility(String ability) {
        return getAbility(Ability.ParseAbility(ability));
    }

    public Ability getAbility(int ability) {
        if(ability == Ability.INVALID_ABILITY)
            return null;

        for(Ability a: role._abilities){
            if(a.getAbilityNumber() == ability)
                return a;
        }
        for(Faction t: getFactions()){
            if(!t.hasSharedAbilities())
                continue;
            for(Ability a: t.getAbilities()){
                if(a.getAbilityNumber() == ability)
                    return a;
            }
        }
        return null;
    }

    public boolean hasAbility(int ability) {
        return getAbility(ability) != null;
    }

    public boolean hasAbility(Class<? extends Ability> ability) {
        return getAbility(ability) != null;
    }

    public boolean hasAbility(String command) {
        return getAbility(command) != null;
    }

    public void setSubmissionTime(int time) {
        submissionTime = time;
    }

    protected boolean didNightAction() {
        return completedNightAction;
    }

    protected void setNightActionComplete() {
        completedNightAction = true;
    }

    public Player getSkipper() {
        return narrator.skipper;
    }

    public boolean stopsParity() {
        for(Ability a: role._abilities){
            if(a.stopsParity(this))
                return true;
        }
        return false;
    }

    // player name
    private String name = "";
    private String id = "";
    private String icon;

    @Override
    public String getName() {
        if(role != null && role._abilities != null && name.length() == 0){
            String t = getRoleName() + "-";
            if(getFaction() != null)
                t += getFaction().getName();
            return t;
        }
        return name;
    }

    public String getIcon() {
        return icon;
    }

    @Override
    public Player setName(String name) {
        if(!narrator.acceptablePlayerName(this, name)){
            throw new NamingException("Cannot use this name");
        }
        this.name = name;
        if(!narrator.isStarted()){
            this.id = name;
            this.databaseID = Math.abs(name.hashCode());
        }
        return this;
    }

    // getStartingName
    public String getID() {
        return id;
    }

    public void ventCheck(Player controller) {
        if(!masters.isEmpty()){
            if(controller == null)
                throw new VotingException("You cannot vote today!");
            if(masters.getFirst() != controller){
                throw new VotingException("You cannot control this person's vote.");
            }
        }else if(masters.isEmpty() && controller != null){
            throw new VotingException("You cannot control this person's vote.");
        }
    }

    // deprecated

    public int getPunchRating() {
        return getFaction().getPunchSuccess();
    }

    public void vote(Player target) {
        _vote(target, null, true);
    }

    public void _vote(Player target, Player controller, boolean recordCommand) {
        ventCheck(controller);
        synchronized (narrator){

            if(target == null)
                throw new NullPointerException(this + " voting for null object");
            if(target == narrator.skipper)
                throw new VotingException("You cannot vote for the skip day directly");
            else if(narrator.voteSystem.getVoteTarget(this) != target){
                String command;
                if(!recordCommand)
                    command = null;
                else if(controller == null)
                    command = Message.CommandCreator(CommandHandler.VOTE, target.getName());
                else{
                    command = Message.CommandCreator(Ventriloquist.VENT_VOTE, getName(), target.getName());
                }
                narrator.voteSystem.vote(this, controller == null ? this : controller, target, command);

            }else
                throw new VotingException("You are already voting for this person.");
        }
    }

    public void voteSkip() {
        voteSkip(null);
    }

    public void voteSkip(Player controller) {
        ventCheck(controller);
        synchronized (narrator){
            String command;
            if(controller == null)
                command = Message.CommandCreator(CommandHandler.SKIP_VOTE);
            else
                command = Message.CommandCreator(Ventriloquist.VENT_SKIP_VOTE, getName());
            narrator.voteSystem.skipVote(this, controller == null ? this : controller, command);
            if(narrator.isDay())
                narrator.parityCheck();
        }
    }

    @Override
    public Controller unvote() {
        _unvote(null, true);
        return this;
    }

    public void _unvote(Player controller, boolean recordCommand) {
        ventCheck(controller);
        synchronized (narrator){
            String command;
            if(!recordCommand)
                command = null;
            else if(controller == null)
                command = Message.CommandCreator(CommandHandler.UNVOTE);
            else
                command = Message.CommandCreator(Ventriloquist.VENT_UNVOTE, getName());

            narrator.voteSystem.unvote(this, controller == null ? this : controller, command);
        }
    }

    public static final String END_NIGHT_TEXT = "You have moved to end the night";
    public static final String CANCEL_END_NIGHT_TEXT = "You have canceled your motion to end the night";

    @Override
    public void endNight() {
        endNight(false);
    }

    protected void endNight(boolean forced) {
        if(this == narrator.skipper)
            return;
        if(!narrator.canDoNightAction())
            throw new PlayerTargetingException(getDescription() + " cannot end night.  It is daytime!");

        if(isDead() && !is(Ghost.class))
            throw new PlayerTargetingException(getDescription() + " cannot end night. This person is dead");

        if(endedNight())
            throw new IllegalActionException();

        synchronized (narrator){
            SelectionMessage e = new SelectionMessage(this, false);
            narrator.getEventManager().addCommand(this, CommandHandler.END_NIGHT);
            if(!forced)
                e.add(END_NIGHT_TEXT + ".");
            e.dontShowPrivate();
            e.pushOut();

            // has to be after
            narrator.endNight(this, forced);
        }
    }

    @Override
    public void cancelEndNight() {
        synchronized (narrator){
            SelectionMessage e = new SelectionMessage(this, false);
            narrator.getEventManager().addCommand(this, CommandHandler.END_NIGHT);
            e.setPrivate();
            e.dontShowPrivate();
            e.add(CANCEL_END_NIGHT_TEXT + ".");

            e.pushOut();

            narrator.cancelEndNight(this);
        }
    }

    public boolean endedNight() {
        return narrator.endedNight(this);
    }

    public void modkill() {
        synchronized (narrator){
            narrator.modkill(new PlayerList(this));
        }
    }

    public PlayerList positiveVotePower = new PlayerList(this);
    public PlayerList owedVotePower = new PlayerList();

    public int getVotePower() {
        return positiveVotePower.size();
    }

    public void increaseVotePower(int newVotePower) {
        for(int i = 0; i < newVotePower; i++)
            positiveVotePower.add(this);
    }

    public void transferVotePower(Player receiver) {
        if(this.positiveVotePower.isEmpty()){
            this.owedVotePower.add(receiver);
            return;
        }
        Player transfer = this.positiveVotePower.removeLast();
        while (!receiver.owedVotePower.isEmpty()){
            receiver = receiver.owedVotePower.removeFirst();
        }
        receiver.positiveVotePower.add(transfer);
    }

    public void stealVotePower(Player killedTarget) {
        if(killedTarget == this)
            return;
        Player p;
        for(int i = 0; i < positiveVotePower.size();){
            p = positiveVotePower.get(i);
            if(p == this)
                i++;
            transferVotePower(p);
        }
        positiveVotePower.clear();
        for(Player positiveVote: killedTarget.positiveVotePower){
            positiveVotePower.add(positiveVote);
        }
        killedTarget.positiveVotePower.clear();
        for(Player player: narrator.getLivePlayers()){
            while (player.positiveVotePower.contains(killedTarget)){
                player.positiveVotePower.remove(killedTarget);
                player.positiveVotePower.add(this);
            }
        }
    }

    /*
     * role effects
     */
    private boolean isInvulnerable = false, blockable = true, isVesting = false;

    public boolean isInvulnerable() {
        return isInvulnerable;
    }

    public void setInvulnerable(boolean b) {
        isInvulnerable = b;
    }

    public void setVesting(boolean v) {
        isVesting = v;
    }

    public boolean isVesting() {
        return isVesting;
    }

    public boolean isBlockable() {
        return blockable;
    }

    public void setBlockable(boolean b) {
        blockable = b;
    }

    private boolean jesterVote = false;

    public void votedForJester(boolean b) {
        jesterVote = b;
    }

    public boolean getVotedForJester() {
        return jesterVote;
    }

    private PlayerList cleaned;

    public void setCleaned(Player p) {
        if(cleaned == null)
            cleaned = new PlayerList();
        cleaned.add(p);
    }

    public PlayerList getCleaners() {
        return cleaned;
    }

    public String disguisedPrevName = null;

    public void setDisfranchised() {
        statusii.put(PlayerStatus.DISFRANCHISED, true);
    }

    public void setSilenced() {
        statusii.put(PlayerStatus.SILENCED, true);
    }

    public boolean isSilenced() {
        return statusii.containsKey(PlayerStatus.SILENCED);
    }

    public boolean isDisenfranchised() {
        return statusii.containsKey(PlayerStatus.DISFRANCHISED);
    }

    public void setBlocked(Player owner) {
        if(narrator.getBool(SetupModifier.BLOCK_FEEDBACK))
            Stripper.FeedbackGenerator(new Feedback(this));

        statusii.put(PlayerStatus.BLOCKED, true);
    }

    public boolean isBlocked() {
        return statusii.containsKey(PlayerStatus.BLOCKED);
    }

    private FactionRole frameStatus;

    public void setFramed(FactionRole m) {
        this.frameStatus = m;
    }

    public FactionRole getFrameStatus() {
        return frameStatus;
    }

    public int getLives() {
        if(deathType != null)
            return -1;
        for(Attack injury: injuries){
            if(!injury.isCountered() && !injury.isImmune() && injury.getType() != Constants.POISON_KILL_FLAG)
                return -1;
        }
        return 1;
    }

    // used for executioner
    public void clearHeals() {
        healList.clear();
    }

    private ArrayList<Heal> healList = new ArrayList<>();

    public void heal(Heal heal) {

        boolean successfulHeal = false;
        for(Attack a: injuries){
            if(a.isHealable(heal) && !a.isCountered()){
                a.counterAttack(heal);
                successfulHeal = true;
                break;
            }
        }

        if(!successfulHeal){
            healList.add(heal);
        }else if(getLives() >= 0){
            for(Action a: narrator.actionStack){
                if(a.owner == this && !a.isCompleted() && getLives() >= 0)
                    a.attemptCompletion();
            }
        }
    }

    /*
     * the attackTypeList keeps track of who attacked the person each night the
     * attackedBy list keeps track of who attacked this person the attack list keeps
     * track of who this person attacked
     */
    private ArrayList<Attack> injuries = new ArrayList<>();
    private ArrayList<Attack> assaults = new ArrayList<>();

    private boolean shouldGetImmuneFeedback(String[] type) {
        if(isVesting())
            return true;
        if(is(Veteran.class) && prevInvulnerability != null)
            return true;

        if(is(Bulletproof.class))
            return !getAbility(Bulletproof.class).isHiddenPassive();

        return false;
    }

    public void kill(Attack attack) {
        injuries.add(attack);
        if(!healList.isEmpty()){
            Heal counter = null;
            for(Heal h: healList){
                if(attack.isHealable(h)){
                    counter = h;
                    break;
                }
            }
            if(counter != null){
                healList.remove(counter);
                attack.counterAttack(counter);
                return;
            }
        }
    }

    protected Player isCharged;

    public boolean isCharged() {
        return isCharged != null;
    }

    public void setCharged(Player charger) {
        isCharged = charger;
    }

    public Player getCharger() {
        return isCharged;
    }

    private Player poisoned;

    public void setPoisoned(Player poisoner) {
        if(poisoner == this || !isInvulnerable())
            poisoned = poisoner;

        Poisoner.FeedbackGenerator(new Feedback(this), poisoner != this && isInvulnerable());
    }

    // used by bg and mm
    public ArrayList<Attack> getInjuries() {
        return injuries;
    }

    public ArrayList<Attack> getAssaults() {
        return assaults;
    }

    private DeathType deathType;

    public DeathType getDeathType() {
        return deathType;
    }

    private void setDead(String[] flag, Player cause) {
        setDead(cause);
        deathType.addDeath(flag);
    }

    public void setLynchDeath(PlayerList lynchers) {
        setDead(Constants.LYNCH_FLAG, lynchers.getLast());
        deathType.setLynchers(lynchers);

        Janitor.SendOutFeedback(this);
    }

    public void dayKill(Attack a, boolean day_ending) {
        setDead(a.getCause());
        deathType.addDeath(a.getType());

        Janitor.SendOutFeedback(this);

        if(!day_ending){
            narrator.voteSystem.removeFromVotes(Player.list(this));
            narrator.checkVote();
            narrator.checkProgress();
        }
    }

    public void modKillHelper() {
        setDead(Constants.MODKILL_FLAG, null);
    }

    private boolean exhumed;

    public void setExhumed() {
        exhumed = true;
    }

    public boolean isExhumed() {
        return exhumed;
    }

    private HashMap<Integer, ArrayList<JailChat>> jailChats;
    private HashMap<Integer, ArrayList<ArchitectChat>> archChats;

    public void setJailed(JailChat jc) {
        ArrayList<JailChat> jChats = jailChats.get(narrator.getDayNumber());
        if(jChats == null){
            jChats = new ArrayList<>();
            jailChats.put(narrator.getDayNumber(), jChats);
        }
        jChats.add(jc);
    }

    public boolean inArchChat() {
        return wasInArchChat(narrator.getDayNumber());
    }

    public boolean wasInArchChat(int dayNumber) {
        if(!narrator.getBool(SetupModifier.ARCH_QUARANTINE))
            return false;
        ArrayList<ArchitectChat> aChats = archChats.get(dayNumber);
        if(aChats == null)
            return false;
        if(aChats.size() > 1)
            return false;
        return !aChats.get(0).getCreator().contains(this);
    }

    public boolean isJailed() {
        return wasJailed(narrator.getDayNumber());
    }

    public boolean wasJailed(int dayNumber) {
        ArrayList<JailChat> jChats = jailChats.get(dayNumber);
        if(jChats == null)
            return false;
        if(jChats.size() > 1)
            return false;
        return jChats.get(0).getCaptive() == this;
    }

    public boolean wasQuarantined(int dayNumber) {
        return wasJailed(dayNumber) || wasInArchChat(dayNumber) && narrator.getBool(SetupModifier.ARCH_QUARANTINE);
    }

    public boolean isQuarintined() {
        return isJailed() || (inArchChat() && narrator.getBool(SetupModifier.ARCH_QUARANTINE));
    }

    public void addArchChat(ArchitectChat ac) {
        ArrayList<ArchitectChat> aChats = archChats.get(narrator.getDayNumber());
        if(aChats == null){
            aChats = new ArrayList<>();
            archChats.put(narrator.getDayNumber(), aChats);
        }
        if(!aChats.contains(ac))
            aChats.add(ac);
    }

    public ArrayList<RoleCreatedChat> getRoleCreatedChats(int day) {
        ArrayList<RoleCreatedChat> list = new ArrayList<>();

        ArrayList<JailChat> jChats = jailChats.get(day);
        if(jChats != null)
            for(JailChat jChat: jChats){
                list.add(jChat);
            }
        ArrayList<ArchitectChat> aChats = archChats.get(day);
        if(aChats != null)
            for(ArchitectChat aChat: aChats){
                list.add(aChat);
            }
        return list;
    }

    protected void onGameStart() {
        if(narrator.getBool(SetupModifier.PUNCH_ALLOWED))
            addAbility(new Punch());
        for(Ability a: role._abilities)
            a.onGameStart(this);
    }

    protected void onDayStart() {
        if(prevInvulnerability != null){
            setInvulnerable(prevInvulnerability);
            prevInvulnerability = null;
        }

        tempAbilities = null;

        isUsingAutoVest = false;
        setVesting(false);

        cultAttempt = null;
        for(Ability ability: role._abilities)
            ability.onDayStart(this);

        jesterVote = false;
        enforcer = null;
        feedback.clear();
        completedNightAction = false;
        this.owedVotePower.clear();
        submissionTime = Narrator.UNSUBMITTED;
        house = this;
        frameStatus = null;
        injuries.clear();
        assaults.clear();
        healList.clear();
        actions = new ActionList(this);
        disguisedPrevName = null;

        addChat(narrator.getEventManager().dayChat);
    }

    protected void onDayEnd() {
        if(ci != null)
            ci.pushOut();
        for(Ability a: role._abilities){
            if(isAlive() || a.onDayEndTriggersWhileDead())
                a.onDayEnd(this);
        }
        saveDayTargets();
    }

    protected void onNightEnd() {
        ArrayList<Ability> teamAbilities = new ArrayList<>();
        for(Faction t: getFactions()){
            if(!t.hasSharedAbilities())
                continue;
            for(Ability a: t.getAbilities()){
                if(t.getFactionAbilityController(a.getAbilityNumber()) == this){
                    teamAbilities.add(a);
                }
            }
        }

        for(Ability a: getFactions().getAbilities()){
            if(teamAbilities.contains(a))
                continue;
            if(a instanceof FactionSend)
                continue;
            cancelTarget(a.getAbilityNumber());
        }

        saveNightTargets();
        statusii.remove(PlayerStatus.DISFRANCHISED);
        statusii.remove(PlayerStatus.SILENCED);
        statusii.remove(PlayerStatus.BUS_DRIVEN);
        statusii.remove(PlayerStatus.BLOCKED);
        if(isAlive())
            for(Ability a: role._abilities)
                a.onPlayerNightEnd(this);
        // i can try to move more of the narrator code into here, where it came from
    }

    protected void onNightStart() {
        // called on night start
        ci = null;
        visitors.clear();
        visits.clear();
        if(isAlive()){
            if(cleaned == null)
                cleaned = new PlayerList();
            cleaned.clear();
        }

        Ability ability;
        for(int i = 0; i < role._abilities.size(); ++i){
            ability = role._abilities.get(i);
            ability.onNightStart(this);
        }
        actions = new ActionList(this);// has to be second because jailor needs this actions

        masters.clear();
        puppets.clear();

        if(isAlive())
            secondaryCauses.clear();

        addChat(narrator.getEventManager().voidChat);

        if(getBounties() != null)
            getBounties().cleanse();

        if(isBountied() && getBounties().isEmpty())
            statusii.remove(PlayerStatus.JOKER_PLAYER_STATUS_KEY);

        if(!narrator.isFirstNight() && hasAbility(FactionSend.MAIN_ABILITY) && !isSilenced() && isAlive()){
            int dayNumber = narrator.getDayNumber() - 1;
            boolean sendSelf = false;
            while (dayNumber != -1){
                if(prevNightTargets.get(dayNumber) == null){
                    dayNumber--;
                    continue;
                }
                ArrayList<Action> sendActions = prevNightTargets.get(dayNumber).getActions(FactionSend.MAIN_ABILITY);
                if(sendActions.isEmpty()){
                    dayNumber--;
                    continue;
                }
                PlayerList oldSends = sendActions.get(0).getTargets();
                oldSends = oldSends.getLivePlayers();
                if(oldSends.isEmpty()){
                    dayNumber--;
                    continue;
                }
                if(oldSends.size() == 1 && oldSends.getFirst() == this){
                    sendSelf = true;
                    dayNumber--;
                    continue;
                }
                actions.addAction(new Action(this, oldSends, FactionSend.MAIN_ABILITY, (String) null, null, null));
                return;
            }
            if(getFactions().getFactionmates().size() == 1)
                return;
            if(sendSelf)
                actions.addAction(
                        new Action(this, Player.list(this), FactionSend.MAIN_ABILITY, (String) null, null, null));
        }
    }

    public ArrayList<Feedback> feedback = new ArrayList<>();

    public void addNightFeedback(Feedback s) {
        feedback.add(s);
    }

    private void setFakeCharges() {
        if(isSquelched())
            return;
        boolean hasBlockedFeedback = false;
        for(Feedback f: feedback){
            if(f.access(this).equals(Stripper.FEEDBACK)){
                hasBlockedFeedback = true;
                break;
            }
        }
        if(!hasBlockedFeedback)
            return;

        for(Action a: actions){
            if(a.is(VestAbility.COMMAND) && a.isCompleted()){
                if(!a.owner.hasAbility(Survivor.class)
                        || a.owner.getAbility(Survivor.class).getRealCharges() != SetupModifiers.UNLIMITED)
                    addVest(Vest.FakeVest());
            }else if(a.getAbility() == null){
                continue;
            }else if(!a.isCompleted() || a.getAbility().getRealCharges() == SetupModifiers.UNLIMITED)
                continue;
            else if(a.isCompleted() && a.getAbility().isFakeBlockable())
                a.getAbility().addFakeCharge();
        }

        if(hasAbility(BreadAbility.class)){
            getAbility(BreadAbility.class).setMarkedBreadToFake();
        }
    }

    public void sendFeedback() {
        // still alive
        boolean jailKilled = false;
        boolean onlyPoisoned = true; /// or no other attacks tbh
        for(Attack a: injuries){
            if(a.getType() == Constants.JAIL_KILL_FLAG)
                jailKilled = true;
            if(a.getType() != Constants.POISON_KILL_FLAG)
                onlyPoisoned = false;
        }
        boolean alive = (getLives() >= Narrator.NORMAL_HEALTH) || onlyPoisoned || (isUsingAutoVest() && !jailKilled);

        setFakeCharges();

        Message f;

        if(alive){
            // you were attacked and healed
            for(Attack a: injuries){
                if(a.isCountered() && !isVesting() && !isJailed())
                    determineHealFeedback(a.getHeal().getType());
                else if(shouldGetImmuneFeedback(a.getType())){
                    new Feedback(this, Constants.NIGHT_IMMUNE_TARGET_FEEDBACK);
                }
            }
        }else{
            // you weren't
            String[] type;
            String killFeedback;
            for(Attack a: injuries){
                if(a.isCountered() || a.isImmune() || a.getType() == Constants.POISON_KILL_FLAG)
                    continue;
                type = a.getType();
                killFeedback = determineKillFeedback(type, narrator, a.getCause());
                f = new Feedback(this, killFeedback).setPicture("tombstone");
                if(a.getCause() != null && narrator.getBool(SetupModifier.OMNISCIENT_DEAD))
                    f.addExtraInfo(a.getCause().getName() + " caused your death.");
                deathType.addDeath(type);
            }
        }

        // dead
        Shuffler.shuffle(feedback, narrator.getRandom());
        Collections.sort(feedback);
        EventLog eLog = narrator.getEventManager().getDayChat();// hasn't quite changed yet
        // look at janitor pushing roles method if you change above

        boolean isSquelched = isSquelched();

        for(Feedback e: feedback){
            if(!isSquelched || !e.hideableFeedback){
                eLog.add(e);
                sendMessage(e);
            }
        }
        for(Ability a: role._abilities){
            a.onNightFeedback(feedback);
        }
    }

    public static String determineKillFeedback(String[] role, Narrator n, Player killer) {
        if(role[0].startsWith(Constants.FACTION_KILL_FLAG[0])){
            if(!n.getBool(SetupModifier.DIFFERENTIATED_FACTION_KILLS))
                return Goon.ANONYMOUS_DEATH_FEEDBACK;
            return Goon.DEATH_FEEDBACK + role[0].replaceAll(Constants.FACTION_KILL_FLAG[0], "") + ".";
        }

        if(role == Constants.SK_KILL_FLAG)
            return SerialKiller.DEATH_FEEDBACK;
        else if(role == Joker.BOUNTY_KILL_FLAG)
            return Joker.DEATH_FEEDBACK;

        else if(role == Constants.JESTER_KILL_FLAG)
            return Jester.DEATH_FEEDBACK;

        else if(role == Constants.VIGILANTE_KILL_FLAG)
            return Vigilante.DEATH_FEEDBACK;
        else if(role == Constants.GUN_KILL_FLAG)
            return Gunsmith.GetDeathFeedback(n);
        else if(role == Constants.GUN_SELF_KILL_FLAG)
            return Gunsmith.GetFaultyDeathFeedback(n);

        else if(role == Constants.VETERAN_KILL_FLAG)
            return Veteran.DEATH_FEEDBACK;
        else if(role == Constants.JAIL_KILL_FLAG)
            return Jailor.DEATH_FEEDBACK;

        else if(role == Constants.BODYGUARD_KILL_FLAG)
            return Bodyguard.DEATH_TARGET_FEEDBACK;

        else if(role == Constants.MASS_MURDERER_FLAG){
            if(killer.is(Interceptor.class))
                return Interceptor.DEATH_FEEDBACK;
            return MassMurderer.DEATH_FEEDBACK;
        }else if(role == Constants.MASONLEADER_KILL_FLAG)
            return MasonLeader.DEAD_FEEDBACK;
        else if(role == Constants.ARSON_KILL_FLAG)
            return Burn.DEATH_FEEDBACK;
        else if(role == Constants.ELECTRO_KILL_FLAG)
            return ElectroManiac.DEATH_FEEDBACK;
        else if(role == Constants.HIDDEN_KILL_FLAG)
            return Disguiser.DEATH_FEEDBACK;
        else if(role == Constants.BOMB_KILL_FLAG)
            return Bomb.DEATH_FEEDBACK;
        throw new IllegalArgumentException("The person cannot kill! " + role[0]);

    }

    private Message determineHealFeedback(String[] role) {
        if(role == Constants.DOCTOR_HEAL_FLAG){
            if(narrator.getBool(SetupModifier.HEAL_FEEDBACK))
                return new Feedback(this, Doctor.TARGET_FEEDBACK).setPicture("doctor");
        }else if(role == Constants.BODYGUARD_KILL_FLAG)
            return new Feedback(this, Bodyguard.TARGET_FEEDBACK).setPicture("bodyguard");
        else
            throw new IllegalArgumentException(role[0] + ": cannot heal!");
        return null;
    }

    private PlayerList visits;

    protected void setVisits(PlayerList visits) {
        this.visits = visits;
    }

    protected void setVisitors(PlayerList visitors) {
        this.visitors = visitors;
    }

    public static final boolean FAKE_VISIT = true;
    private PlayerList visitors;

    public void visit(boolean fakeVisit, Player... targets) {
        PlayerList charged = new PlayerList();
        for(Player target: targets){
            charged.add(Interceptor.handleInterceptor(this, target));
            if(!in(target.visitors))
                target.visitors.add(this);
            if(!target.in(visits)){
                if(fakeVisit)
                    visits.add(0, target);
                else
                    visits.add(target);
            }

            if(isCharged() && !fakeVisit && target != this && !charged.contains(target)){
                if(target.isCharged() || target.is(ElectroManiac.class))
                    charged.add(target);
            }
            if(!fakeVisit && narrator.getBool(SetupModifier.CORONER_LEARNS_ROLES)){
                HTString roleHT = new HTString(this.getRoleName(), this.getColor());
                if(!target.roleVisitors.contains(roleHT))
                    target.roleVisitors.add(roleHT);
            }
        }
        if(!charged.isEmpty())
            charged.add(this);

        ElectrocutionException.throwException(this, charged);
    }

    public void visit(Player... targets) {
        visit(false, targets); // not fake visit
    }

    public void visit(PlayerList targets) {
        for(Player p: targets)
            visit(false, p); // not fake visit
    }

    public PlayerList getVisitors(String why) {
        PlayerList list = PlayerList.clone(visitors);
        list.shuffle(narrator.getRandom(), why);
        return list;
    }

    public PlayerList getVisits(String why) {
        PlayerList list = PlayerList.clone(visits);
        list.shuffle(narrator.getRandom(), why);
        return list;
    }

    public void doDayAction(int ability) {
        doDayAction(ability, null);
    }

    public void doDayAction(String option) {
        doDayAction(Ability.INVITATION_ABILITY, option);
    }

    public void doDayAction(Player p) {
        doDayAction(role._abilities.get(0).getAbilityNumber(), null, p);
    }

    public void doDayAction(int ability, Player target, String option) {
        doDayAction(ability, option, target);
    }

    public void doDayAction(int ability, String option, Player... targetArray) {
        PlayerList targets = Player.list(targetArray);
        doDayAction(ability, option, targets);
    }

    public void doDayAction(String command, String option, PlayerList targets) {
        doDayAction(Ability.ParseAbility(command), option, targets);
    }

    public void doDayAction(int ability, String option, PlayerList targets) {
        if(narrator.skipper.in(targets))
            throw new NullPointerException("Dont use internal use object");
        if(!narrator.isDay())
            throw new PhaseException("Day actions can only be submitted during the day");
        if(!hasAbility(ability) && ability != Ability.INVITATION_ABILITY && ability != PuppetVote.MAIN_ABILITY)
            throw new PlayerTargetingException("You do not have this ability!");

        if(!hasAbility(ability) && ability == PuppetVote.MAIN_ABILITY && targets.size() == 1
                && targets.getFirst().isSilenced())
            new PuppetVote().doDayAction(this, targets);

        Action a = new Action(this, targets, ability, option, null);

        if(hasPuppets() && a.is(PuppetVote.MAIN_ABILITY)){
            if(hasDayAction(PuppetVote.COMMAND)){
                if(Constants.UNVOTE.equalsIgnoreCase(option) && targets.size() == 1)
                    targets.add(targets.getFirst());
                getAbility(Ventriloquist.VENT_VOTE).doDayAction(this, targets);
                return;
            }
            Ventriloquist.VentException();
        }
        if(isPuppeted() && !Ability.is(ability, PuppetVote.MAIN_ABILITY)){
            throw new IllegalActionException("You cannot perform day actions while being controlled.");
        }
        if(a.is(Constants.RECRUITMENT)){
            if(ci == null)
                throw new PlayerTargetingException("Nothing to respond to");
            if(option == null)
                throw new PlayerTargetingException("Invitations need a response");
            if(option.equalsIgnoreCase(Constants.ACCEPT))
                ci.confirm();
            else if(option.equalsIgnoreCase(Constants.DECLINE)){
                ci.decline();
            }else
                throw new IllegalActionException("Unknown response to invitation");
            return;
        }

        Ability abilityAction = a.getAbility();

        if(abilityAction == null)
            throw new IllegalActionException(this + " has no day action!");
        if(!abilityAction.canUseDuringDay(this))
            throw new PhaseException("Can't do this right now");
        abilityAction.checkAcceptableTarget(a);
        abilityAction.doDayAction(this, targets);
    }

    private void cancelAction(Action canceled) {
        narrator.actionStack.remove(canceled);
        List<NarratorListener> listeners = narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onTargetRemove(this, canceled.getCommand(), canceled.getTargets());
    }

    @Override
    public void cancelAction(int actionNumber) {
        Action a = actions.cancelActionID(actionNumber, true);

        HashMap<Faction, Player> before_set = getFactions().getAbilityControllers(a.ability);

        if(narrator.getPossibleMembers().hasMembersThatAffectSending(narrator) && a.isTeamAbility()
                && actions.getTargets(a.ability).size() < 2){
            PlayerList sendTargets = actions.getTargets(Faction.SEND_);
            if(sendTargets.size() == 1 && this.in(sendTargets))
                actions.cancelActionID(actions.size() - 1, false);
            else if(this.in(sendTargets)){
                Action sendAction = getAction(FactionSend.MAIN_ABILITY);
                sendAction.setTargets(sendAction.getTargets().remove(this));
            }
        }

        cancelAction(a);

        Player before, after;
        for(Faction t: before_set.keySet()){
            before = before_set.get(t);
            after = t.determineNightActionController(a.ability);
            Faction.warnAbilityOwnerChange(this, null, before, after, getFaction());
        }
    }

    public void doNightAction(int ability) {
        for(Action a: getActions())
            if(a.ability == ability)
                a.doNightAction();
    }

    public ArrayList<Ability> getDayAbilities() {
        ArrayList<Ability> dayActions = new ArrayList<>();
        for(Ability a: role._abilities){
            if(a.canUseDuringDay(this) && a.getPerceivedCharges() != 0 && !a.isOnCooldown())
                dayActions.add(a);
            else if(a.canUseDuringDay(this) && a.is(Jailor.MAIN_ABILITY))
                dayActions.add(a);
        }

        if(ci != null)
            dayActions.add(new CultPendingInvitation());

        return dayActions;
    }

    public ArrayList<String> getDayCommands() {
        ArrayList<String> dCommands = new ArrayList<>();
        for(Ability a: getDayAbilities()){
            dCommands.add(a.getCommand());
        }
        return dCommands;
    }

    public boolean hasDayAction(String... commands) {
        for(String s: commands){
            if(hasDayAction(s))
                return true;
        }
        return false;
    }

    public boolean hasDayAction(String command) {
        return hasDayAction(Ability.ParseAbility(command));
    }

    // unsure if this should be a 'do I have this ability in general' or 'can I use
    // this ability'
    // going with can i use this ability
    public boolean hasDayAction(int ability) {
        if(Ability.is(ability, PuppetVote.MAIN_ABILITY) && hasAbility(ability))
            return getAbility(PuppetVote.MAIN_ABILITY).canUseDuringDay(this);
        if(isPuppeted())
            return false;
        if(hasAbility(ability))
            return getAbility(ability).canUseDuringDay(this);
        else if(ability == Ability.INVITATION_ABILITY)
            return ci != null;
        return false;

    }

    public boolean isAlive() {
        return deathType == null;
    }

    public boolean isDead() {
        return !isAlive();
    }

    public boolean isEliminated() {
        return isDead() && !hasAbility(Ghost.class);
    }

    public int getDeathDay() {
        return deathType.getDeathDay();
    }

    private boolean winner = false;

    public boolean isWinner() {
        return winner;
    }

    protected void determineWin() {
        Faction t = getFaction();

        if(t.getEnemyColors().isEmpty()){
            winner = true;
            Boolean winValue;
            boolean defaultToAlive = true;
            for(Ability a: role._abilities){
                winValue = a.checkWinEnemyless(this);
                if(winValue == null)
                    continue;
                defaultToAlive = false;
                winner = winner && winValue;
            }
            if(defaultToAlive)
                winner = isAlive();
            return;
        }

        if(!isAlive() && t.getAliveToWin()){
            winner = false;
            return;
        }

        if(t.isAlive()){
            for(String teamKey: t.getEnemyColors()){
                Faction enemyTeam = narrator.getFaction(teamKey);

                if(enemyTeam == t && t.size() == 1){

                }else if(enemyTeam.isAlive() && enemyTeam.winsOver(t)){
                    return;
                }
            }
            winner = true;
            return;
        }
    }

    public static final Comparator<Player> SubmissionTime = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            return p1.getSubmissionTime() - p2.getSubmissionTime();
        }

    };

    public static final Comparator<Player> NameSort = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            return p1.getName().toLowerCase().compareTo(p2.getName().toLowerCase());
        }
    };

    public static final Comparator<Player> WinDetermineSort = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            if(p1.is(Ghost.class) && !p2.is(Ghost.class))
                return 1;
            if(!p1.is(Ghost.class) && p2.is(Ghost.class))
                return -1;
            if(p1.is(Ghost.class) && p2.is(Ghost.class) && p1.isDead() && p2.isDead()){
                DeathType d1 = p1.getDeathType();
                DeathType d2 = p2.getDeathType();

                if(d1.getCause() == p2)
                    return 1;
                if(d2.getCause() == p1)
                    return -1;
            }
            return 0;
        }
    };

    public static final Comparator<Player> TeamSort = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            if(p1.getColor().equals(p2.getColor())){
                return p1.getName().compareTo(p2.getName());
            }
            return p1.getFaction().getName().compareTo(p2.getFaction().getName());
        }

    };

    public static final Comparator<Player> IDSort = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            return p1.getID().compareTo(p2.getID());
        }

    };

    public static final Comparator<Player> Deaths = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            if(p1.isAlive() && p2.isAlive())
                return p1.getName().compareTo(p2.getName());
            if(p1.isAlive())
                return -1;
            if(p2.isAlive())
                return 1;
            if(p1.getDeathDay() == p2.getDeathDay()){
                if(p1.getDeathType().isLynch() != p2.getDeathType().isLynch()){
                    if(p1.getDeathType().isLynch())
                        return -1;
                    if(p2.getDeathType().isLynch())
                        return 1;
                }
                return p1.getName().compareTo(p2.getName());
            }
            return p1.getDeathDay() - p2.getDeathDay();
        }

    };

    public int getSubmissionTime() {
        return submissionTime;
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(o.getClass() != getClass())
            return false;
        Player p = (Player) o;

        if(notEqual(_color, p._color))
            return false;
        if(notEqual(frameStatus, p.frameStatus))
            return false;
        if(cleaned != p.cleaned)
            return false;
        // if(notEqual(comm, p.comm))
        // return false;
        if(detectable != p.detectable)
            return false;
        if(doused != p.doused)
            return false;
        if(Util.notEqual(deathType, p.deathType))
            return false;
        // if(notEqual(feedback, p.feedback))
        // return false;
        if(notEqual(healList, p.healList))
            return false;
        if(jesterVote != p.jesterVote)
            return false;
        // if(isComputer() != p.isComputer)
        // return false;
        if(notEqual(name, p.name))
            return false;
        if(actions == null){
            if(p.actions != null)
                return false;
        }else{
            return actions.equals(p.actions);

        }

        if(notEqual(pendingRole, p.pendingRole))
            return false;
        if(notEqual(role._abilities, p.role._abilities))
            return false;
        if(winner != p.winner)
            return false;

        return true;
    }

    private static boolean notEqual(Object o, Object p) {
        return Util.notEqual(o, p);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return id + "(" + getRoleName() + ")";
    }

    public static Player[] array(Player... target) {
        Player[] array = new Player[target.length];
        for(int i = 0; i < target.length; i++)
            array[i] = target[i];
        return array;
    }

    public static PlayerList list(Player... target) {
        PlayerList list = new PlayerList();
        for(Player p: target){
            list.add(p);
        }
        return list;
    }

    public static void print(ArrayList<PlayerList> perms) {
        for(PlayerList list: perms){
            for(Player p: list)
                System.out.print(p.getDescription() + " ");
            System.out.println();
        }
    }

    public static String cleanup(String message) {
        int lastComma = message.lastIndexOf(",");
        if(lastComma == -1)
            return message;
        return message.substring(0, lastComma);
    }

    public boolean isAtHome() {
        if(actions.isEmpty() || actions.getFirst().is(Ability.VEST_ABILITY))
            return true;
        return actions.visitedHome();
    }

    public void sendTeamTextPrompt() {
        getFaction().sendNightTextPrompt(this);
    }

    public void setTeam(Faction team) {
        _color = team.getColor();
    }

    public static String DescriptionList(PlayerList members) {
        String s = "";
        for(Player p: members){
            s += p.getDescription() + ", ";
        }
        return cleanup(s);
    }

    private FactionRole pendingRole;

    public void changeRole(FactionRole factionRole) {
        if(narrator.isNight())
            pendingRole = factionRole;
        else{
            for(Ability a: role._abilities){
                a.onAbilityLoss(this);
            }
            this.factionRole = factionRole;
            this.role = factionRole.role;
            for(Ability a: role._abilities)
                a.initialize(this);

            Feedback e = new Feedback(this, "You are now a ");
            HTString ht = new HTString(getRoleName(), getColor());
            e.add(ht);
            e.add(".");

            sendMessage(e);
        }
    }

    public void resolveRoleChange() {
        if(pendingRole != null){
            for(Ability a: role._abilities){
                a.onAbilityLoss(this);
            }
            if(is(Amnesiac.class)){
                // happening message is already taken care of
                // works because i'm going to keep the modifier map the same in all
                Modifiers<RoleModifier> modifierMap = Modifiers.copy(pendingRole.roleModifiers);

                AbilityList newAbilities = Amnesiac.AbilitySmash(this, role._abilities, pendingRole.role._abilities);
                Role newRole = new Role(narrator, pendingRole.getName(), newAbilities);
                Role oldRole = pendingRole.role;
                this.role = newRole;
                this.role.modifiers = modifierMap;
                Ability.initialize(newRole, this);

                Ability newA, oldA;
                for(int na = 0, oa = 0; na < newRole._abilities.size() && oa < oldRole._abilities.size();){
                    oldA = oldRole._abilities.get(oa);
                    newA = newRole._abilities.get(na);
                    if(oldA instanceof ItemAbility){
                        oa++;
                        continue;
                    }
                    if(newA instanceof ItemAbility){
                        na++;
                        continue;
                    }
                    if(narrator.getBool(SetupModifier.AMNESIAC_KEEPS_CHARGES)){
                        int initialCharges = oldA.getRealCharges();
                        if(initialCharges == 0)
                            initialCharges++;
                        newA.setInitialCharges(initialCharges, this);
                    }else if(oldA.unlimitedCharges(this))
                        newA.setInitialCharges(SetupModifiers.UNLIMITED, this);
                    else{
                        newA.setInitialCharges(1, this);
                    }
                    na++;
                    oa++;
                }
                return;
            }
            Message e = new Feedback(this, "You are now a ");
            HTString ht = new HTString(pendingRole.getName(), getColor());
            e.add(ht);
            e.add(".");
            ArrayList<Ability> pendingAbilities = new ArrayList<>();
            for(Ability a: pendingRole.role._abilities){
                if(!(a instanceof ItemAbility))
                    pendingAbilities.add(a);

            }
            AbilityList new_abilities = pendingRole.role._abilities;
            for(Ability a: new_abilities)
                a.initialize(this);

            for(Ability a: role._abilities){
                if(a instanceof ItemAbility)
                    new_abilities.add((ItemAbility<?>) a);
            }
            this.factionRole = pendingRole;
            this.role = pendingRole.role;
        }
        pendingRole = null;
    }

    public static PlayerList emptyList() {
        return Player.list();
    }

    public static Player[] emptyArray() {
        return new Player[0];
    }

    public DeathType setDead(Player cause) {
        deathType = new DeathType(this, narrator.isDay(), narrator.getDayNumber());
        deathType.setCause(cause);
        if(!is(Ghost.class)){
            narrator.getEventManager().getDeadChat().addAccess(this);
            addChat(narrator.getEventManager().deadChat);
        }
        Faction t = getFaction();
        t.removeMember(this);
        if(t.hasLastWill() && _secretLastWill != null && !_secretLastWill.isEmpty() && t.isAlive()){
            String message = getName() + " left you a message:\n" + _secretLastWill;
            Feedback fb;
            for(Player p: t.getMembers()){
                fb = new Feedback(p, message);
                fb.hideableFeedback = false;
                if(narrator.isDay()){
                    p.sendMessage(fb);
                }
            }
        }
        if(narrator.isDay()){
            onDeath();
            narrator._players.sortByName();
        }
        // ventriloquist depends on currentRole 'onDeathing' first
        for(Player master: masters){
            master.puppets.remove(this);
        }
        for(Player puppet: puppets){
            puppet.masters.remove(this);
        }
        puppets.clear();
        actions.clear();
        return deathType;

    }

    protected void onDeath() {
        if(statusii.containsKey(PlayerStatus.JOKER_PLAYER_STATUS_KEY) && is(Ghost.class)){
            BountyList bl = getBounties();
            int day = Integer.MAX_VALUE;

            PlayerList choices = new PlayerList();
            Bounty b;
            for(int i = 0; i < bl.list.size(); i++){
                b = bl.list.get(i);
                if(b.rewarder.isAlive() && !b.failed() && day >= b.start_day){
                    if(day > b.start_day)
                        choices.clear();
                    else
                        day = b.start_day;
                    choices.add(b.rewarder);
                }
            }
            if(!choices.isEmpty())
                statusii.put(PlayerStatus.SUCCESSFUL_BOUNTY, narrator.getRandom().getPlayer(choices));
        }
        for(Ability a: role._abilities)
            a.onDeath(this);
        if(narrator.isDay())
            narrator.masonLeaderPromotion();
        for(Player positiveVote: this.positiveVotePower){
            if(positiveVote == this)
                continue;
            positiveVote.positiveVotePower.add(positiveVote);
        }

    }

    // used for mason leader recruiting
    public boolean isPowerRole() {
        for(Ability a: role._abilities)
            if(a.isPowerRole()) // change to isPowerAbility
                return true;
        return false;
    }

    public boolean hasActivePassive() {
        for(Ability a: role._abilities)
            if(a.isActivePassive(this))
                return true;
        return false;
    }

    public PlayerList cultAttempt = null;

    private boolean doused = false;

    public void setDoused(boolean b) {
        doused = b;
        if(narrator.getBool(SetupModifier.DOUSE_FEEDBACK)){
            Burn.FeedbackGenerator(new Feedback(this));
        }
    }

    public boolean isDoused() {
        return doused;
    }

    private boolean culted = false;

    public void setCulted() {
        culted = true;
    }

    public boolean isCulted() {
        return culted || is(CultLeader.class) || is(Cultist.class);
    }

    private Player enforcer = null;

    public boolean isEnforced() {
        return enforcer != null;
    }

    public void setEnforced(Player enforcer) {
        this.enforcer = enforcer;
    }

    public Player getEnforcer() {
        return enforcer;
    }

    private boolean detectable = true;

    public boolean isDetectable() {
        return detectable;
    }

    public void setDetectable(boolean b) {
        detectable = b;
    }

    private boolean isComputer = false;

    public boolean isComputer() {
        return isComputer;
    }

    public Player setComputer() {
        isComputer = true;
        return this;
    }

    @Override
    public ChatMessage say(String message, String key) {
        return _say(message, key);
    }

    public ChatMessage _say(String message, String key) {
        synchronized (narrator){
            return narrator.say(this, message, key);
        }
    }

    public EventList getEvents() {
        if(isEliminated() && (narrator.getBool(SetupModifier.OMNISCIENT_DEAD) || !narrator.isInProgress()))
            return narrator.getEventManager().getEvents(getID(), Message.PRIVATE);
        return narrator.getEventManager().getEvents(getID());
    }

    public boolean in(Collection<Player> pl) {
        if(pl == null)
            return false;
        return pl.contains(this);
    }

    public boolean isPoisoned() {
        return poisoned != null;
    }

    public Player getPoisoner() {
        return poisoned;
    }

    ArrayList<String> rPrefers;
    public ArrayList<Faction> _tPrefers;

    public void rolePrefer(Role baseRole) {
        rolePrefer(baseRole.getName());
    }

    @Override
    public void rolePrefer(String prefer) {
        if(!rPrefers.contains(prefer))
            rPrefers.add(prefer);
    }

    public void teamPrefer(String prefer) {
        for(Faction t: narrator.getFactions()){
            if(_tPrefers.contains(t))
                continue;
            if(t.getColor().equalsIgnoreCase(prefer) || t.getName().equalsIgnoreCase(prefer))
                _tPrefers.add(t);
        }
    }

    public boolean hasPreferences() {
        return !rPrefers.isEmpty() || !_tPrefers.isEmpty();
    }

    @Override
    public void clearPreferences() {
        rPrefers.clear();
        _tPrefers.clear();
    }

    protected List<RolePackage> cleanPrefers(Map<String, Set<RolePackage>> generatedRoles) {
        if(!hasPreferences())
            return new ArrayList<RolePackage>();
        String key;
        Faction t;
        for(int i = 0; i < rPrefers.size(); i++){
            key = rPrefers.get(i);
            if(!generatedRoles.containsKey(key)){
                rPrefers.remove(key);
                i--;
            }
        }
        for(int i = 0; i < _tPrefers.size(); i++){
            t = _tPrefers.get(i);
            if(!generatedRoles.containsKey(t.getColor())){
                _tPrefers.remove(t);
                i--;
            }
        }
        // at this point, i'm only preferring possible roles and teams

        final Map<RolePackage, Integer> chosenPrefs = new HashMap<>();
        for(String role: rPrefers){
            for(RolePackage rp: generatedRoles.get(role)){
                if(chosenPrefs.containsKey(rp)){
                    int prevVal = chosenPrefs.remove(rp);
                    chosenPrefs.put(rp, prevVal + 1);
                }else
                    chosenPrefs.put(rp, 1);
            }
        }
        for(Faction team: _tPrefers){
            for(RolePackage tp: generatedRoles.get(team.getColor())){
                if(chosenPrefs.containsKey(tp)){
                    int prevVal = chosenPrefs.remove(tp);
                    chosenPrefs.put(tp, prevVal + 1);
                }else
                    chosenPrefs.put(tp, 1);
            }
        }

        List<RolePackage> list = new ArrayList<>();
        for(RolePackage rolePackage: narrator.generatedRoles)
            if(chosenPrefs.containsKey(rolePackage))
                list.add(rolePackage);
        list.sort(new Comparator<RolePackage>() {
            @Override
            public int compare(RolePackage o1, RolePackage o2) {
                return chosenPrefs.get(o2) - chosenPrefs.get(o1);
            }
        });
        return list;
    }

    public void addModBread(int breadToAdd) {
        for(int i = 0; i < breadToAdd; i++){
            addBread(new Bread().initialize());
        }

        SelectionMessage e = new SelectionMessage(this, false);
        StringChoice you = new StringChoice(this);
        you.add(this, "You");
        e.add(you, " ");

        StringChoice scTarget = new StringChoice("has");
        scTarget.add(this, "have");

        e.add(scTarget, " received something : " + breadToAdd + " bread.");
        narrator.getEventManager().addCommand(Constants.MOD_ADDBREAD, "" + breadToAdd, this.getName());
        e.pushOut();

    }

    public boolean isSquelched() {
        for(Message m: feedback){
            if(m.toString().equals(DrugDealer.WIPE))
                return true;
        }
        return false;
    }

    public ArrayList<String> getRoleSpecs() {
        ArrayList<String> specs = new ArrayList<>();
        for(Ability a: role._abilities)
            specs.addAll(a.getRoleSpecs(this));

        if(!isDetectable())
            specs.add("You cannot be detected by investigative roles.");
        if(!isBlockable())
            specs.add("You cannot be blocked at night.");
        if(!isRecruitable() && narrator.getPossibleMembers().abilityExists(narrator, CultLeader.class))
            specs.add("You cannot be converted.");

        if(narrator.getBool(SetupModifier.PUNCH_ALLOWED))
            specs.add("Your chance of punching someone out is " + getPunchRating() + "%.");

        if(getPerceivedAutoVestCount() > 0)
            specs.add("You have " + getPerceivedAutoVestCount() + " auto " + narrator.getAlias(Vest.ALIAS)
                    + "(s) left for use.");

        return specs;
    }

    public CultInvitation ci;

    public void addCultInvitation(Player cultLeader) {
        ci = new CultInvitation(cultLeader, this);

    }

    public boolean hasInjuriesFromPreKillingPhase() {
        for(Attack a: injuries){
            if(a.isCountered())
                continue;
            if(a.isImmune())
                continue;
            if(a.sustainedBeforeAttackPhase)
                return true;
        }
        return false;
    }

    public ArrayList<Suit> suits;

    public void removeSuits(Player tailor) {
        if(suits == null || isDead())
            return;
        for(int i = 0; i < suits.size(); i++){
            if(suits.get(i).createdBy(tailor)){
                suits.remove(i);
                i--;
            }

        }
    }

    public void addSuite(Suit s) {
        if(suits == null)
            suits = new ArrayList<>();
        suits.add(0, s);
    }

    public boolean hasSuits() {
        return suits != null && !suits.isEmpty();
    }

    private ArrayList<EventLog> chats;

    public ArrayList<EventLog> getChats() {
        if(!narrator.isInProgress() && narrator.isStarted())
            return narrator.getEventManager().getAllChats();
        if(isEliminated())
            return narrator.getEventManager().getAllChats();

        ArrayList<EventLog> ret = new ArrayList<>();
        ret.addAll(chats);

        for(ArrayList<JailChat> jChats: this.jailChats.values()){
            for(JailChat jChat: jChats)
                ret.add(jChat);
        }

        for(ArrayList<ArchitectChat> aChats: this.archChats.values()){
            for(ArchitectChat aChat: aChats)
                ret.add(aChat);
        }

        return ret;
    }

    public void addChat(EventLog el) {
        if(el == null)
            throw new NullPointerException();
        if(!chats.contains(el))
            chats.add(el);
    }

    public void removeChat(EventLog el) {
        chats.remove(el);
    }

    @Override
    public ArrayList<String> getChatKeys() {
        ArrayList<String> chatKeys = new ArrayList<>();
        String key;
        for(EventLog el: getChats()){
            key = el.getKey(this);
            if(el instanceof FactionChat && el.getEvents().isEmpty()
                    && el.getMembers().filterUnquarantined().size() < 2)
                continue;
            if(!chatKeys.contains(key) && el.isActive())
                chatKeys.add(key);
        }
        return chatKeys;
    }

    public HashMap<Player, Boolean> lastWillTrustees;
    public String _lastWill;
    public String _secretLastWill;

    @Override
    public void setLastWill(String lastWill) {
        if(!narrator.isInProgress()){
            if(!narrator.isStarted())
                throw new PhaseException("Last wills cannot be set before game starts.");
        }
        if(!narrator.getBool(SetupModifier.LAST_WILL)){
            throw new IllegalActionException();
        }
        if(isDead())
            throw new IllegalActionException();
        this._lastWill = lastWill;
    }

    @Override
    public void setTeamLastWill(String lastWill) {
        if(!narrator.isInProgress() && !narrator.isStarted())
            throw new PhaseException("Cannot set last wills before game starts.");

        if(!getFaction().hasLastWill())
            throw new IllegalActionException();

        if(isDead())
            throw new IllegalActionException();
        this._secretLastWill = lastWill;
    }

    // null requester for public last wills
    public String getLastWill(Player requester) {
        if(!narrator.getBool(SetupModifier.LAST_WILL))
            return null;
        if(!this.getDeathType().isCleaned())
            return _lastWill;
        if(lastWillTrustees.containsKey(requester) && lastWillTrustees.get(requester))
            return _lastWill;
        return null;
    }

    public void switchNames(Player target) {
        String temp = target.name;
        target.name = name;
        name = temp;

        temp = target.icon;
        target.icon = icon;
        icon = temp;
    }

    public void reset(boolean resetPrefs) {
        _lastWill = null;
        _secretLastWill = null;

        pendingRole = null;
        onNightStart();
        onDayStart();
        onDayEnd();
        onNightEnd();
        role = null;
        winner = false;
        exhumed = false;
        suits = null;
        if(resetPrefs)
            clearPreferences();
        prevNightTargets.clear();
        roleVisitors.clear();
        jailChats.clear();

        fakeAutoVestCount = 0;
        hiddenAutoVestCount = 0;
        realAutoVestCount = 0;
        isUsingAutoVest = false;

        isCharged = null;
        poisoned = null;
        doused = false;
        culted = false;
        statusii.clear();

        this.positiveVotePower.clear();
        this.positiveVotePower.add(this);
        this.owedVotePower.clear();
        isInvulnerable = false;
        detectable = true;
        blockable = true;

        deathType = null;

        if(cleaned != null)
            cleaned.clear();
        disguisedPrevName = null;
        actions.clear();

        if(this.initialAssignedRole != null){
            rolePrefer(this.initialFactionRole.role.name);
            teamPrefer(getInitialColor());
        }

        chats.clear();

        this.name = id;
    }

    private PlayerList masters;
    private PlayerList puppets;

    public void addVentController(Player vent) {
        if(vent == this){
            Ability.NoNightActionVisit(vent, this);
            return;
        }
        Ability.happening(vent, " possessed ", this);
        if(masters.isEmpty())
            new Feedback(this, Ventriloquist.FEEDBACK).setPicture("blackmailer")
                    .addExtraInfo("You are now allowed to talk or vote today.");
        this.masters.add(vent);
        vent.puppets.add(this);
    }

    public boolean hasPuppets() {
        for(Player puppet: puppets){
            if(puppet.masters.getFirst() == this && (!puppet.isDisenfranchised() || !puppet.isSilenced()))
                return true;
        }
        return false;
    }

    public boolean isPuppeted() {
        return !masters.isEmpty();
    }

    public PlayerList getPuppets() {
        PlayerList puppets = new PlayerList();
        for(Player puppet: this.puppets){
            if(puppet.masters.getFirst() == this)
                puppets.add(puppet);
        }
        return puppets;
    }

    public Player getPuppeteer() {
        if(masters.isEmpty())
            return null;
        return masters.getFirst();
    }

    public boolean isRecruitable() {
        for(Ability a: role._abilities){
            if(!a.isRecruitable())
                return false;
        }
        if(role.modifiers == null)
            return true;
        return !role.modifiers.getOrDefault(RoleModifier.UNCONVERTABLE, false);
    }

    // used for ghost only, really.
    private PlayerList secondaryCauses;

    public void secondaryCause(Player cause) {
        secondaryCauses.add(cause);
    }

    public Player getCause() {
        return secondaryCauses.getLast();
    }

    /*
     * Vest handling
     */

    public int getPerceivedVestCount() {
        if(!hasAbility(VestAbility.MAIN_ABILITY))
            return 0;
        return getAbility(VestAbility.class).getPerceivedCharges();
    }

    public void addVest(Vest v) {
        if(role._abilities.contains(VestAbility.class))
            getAbility(VestAbility.class).addConsumable(v);
        else
            role._abilities.add(new VestAbility(this, v));
    }

    public void addGun(Gun gun) {
        if(role._abilities.contains(GunAbility.class))
            getAbility(GunAbility.class).addConsumable(gun);
        else
            role._abilities.add(new GunAbility(this, gun));
    }

    public int getRealGunCount() {
        return GunAbility.getRealGunCount(this);
    }

    public Boolean prevInvulnerability = null;

    private int fakeAutoVestCount;
    private int realAutoVestCount;
    private int hiddenAutoVestCount = 0;
    private int startingAutoVestCount = 0;
    private boolean isUsingAutoVest = false;

    public void setAutoVestCount(int autoVs) {
        realAutoVestCount = autoVs;
        startingAutoVestCount = autoVs;
    }

    public int getStartingAutoVestCount() {
        return startingAutoVestCount;
    }

    public int getPerceivedAutoVestCount() {
        return realAutoVestCount + fakeAutoVestCount;
    }

    public int getRealAutoVestCount() {
        return realAutoVestCount + hiddenAutoVestCount;
    }

    public void silentUseAutoVest() {
        if(realAutoVestCount > 0){
            realAutoVestCount--;
            fakeAutoVestCount++;
        }
        isUsingAutoVest = true;
    }

    public void useAutoVest() {
        if(realAutoVestCount > 0){
            realAutoVestCount--;
        }else if(hiddenAutoVestCount > 0){
            hiddenAutoVestCount--;
        }else{
            return;
        }

        isUsingAutoVest = true;
    }

    public boolean isUsingAutoVest() {
        return isUsingAutoVest;
    }

    public void addBread(Bread b) {
        if(role._abilities.contains(BreadAbility.class))
            getAbility(BreadAbility.class).addConsumable(b);
        else
            role._abilities.add(new BreadAbility(this, b));
    }

    private ArrayList<HTString> roleVisitors = new ArrayList<>();

    public ArrayList<HTString> getRoleVisits() {
        return roleVisitors;
    }

    public boolean hasUniqueRole() {
        return role.modifiers.getOrDefault(RoleModifier.UNIQUE, false);
    }

    public void resolveFakeBreadActions() {
        for(Ability a: role._abilities)
            a.resolveFakeBreadedActions();
    }

    @Override
    public void log(String string) {
    }

    @Override
    public void setNightTarget() {
        if(is(Burn.class))
            Burn.NightBurn(this);
        else
            this.setTarget(Ability.ParseAbility(this.getAbilities().get(0).getCommand()), null, null, Player.list());
    }

    @Override
    public void spy(String team) {
        setTarget(Spy.MAIN_ABILITY, CommandHandler.parseTeam(team, narrator), null);
    }

    @Override
    public void vest() {
        this.setTarget(Vest.MAIN_ABILITY, null, null, new PlayerList());
    }

    @Override
    public Player skipVote() {
        this.voteSkip();
        return this;
    }

    @Override
    public Player ventVote(Controller puppet_c, Controller target_c) {
        Player puppet = puppet_c.getPlayer(narrator);
        Player target = target_c.getPlayer(narrator);
        this.doDayAction(PuppetVote.MAIN_ABILITY, null, puppet, target);
        return null;
    }

    @Override
    public void ventUnvote(Controller puppet_c) {
        Player puppet = puppet_c.getPlayer(narrator);
        this.doDayAction(PuppetVote.MAIN_ABILITY, Constants.UNVOTE, puppet, puppet);
    }

    @Override
    public void ventSkipVote(Controller puppet_c) {
        Player puppet = puppet_c.getPlayer(narrator);
        this.doDayAction(PuppetVote.MAIN_ABILITY, Constants.SKIP_VOTE, Player.list(puppet));

    }

    @Override
    public void doDayAction(String ability, String option, Controller... targets) {
        this.doDayAction(Ability.ParseAbility(ability), option, ControllerList.ToPlayerList(narrator, targets));
    }

    @Override
    public boolean cleanup() {
        return false;
    }

    @Override
    public boolean hasError() {
        return false;
    }

    @Override
    public void setNightTarget(String action, Controller... targets) {
        this.setNightTarget(action, null, null, null, targets);
    }

    @Override
    public void setNightTarget(String action, String option, Controller... targets) {
        this.setNightTarget(action, option, null, null, targets);
    }

    @Override
    public void setNightTarget(String action, String option, String opt2, String opt3, Controller... targets) {
        this.setTarget(Ability.ParseAbility(action), option, opt2, opt3,
                ControllerList.ToPlayerList(narrator, targets));
    }

    @Override
    public void setNightTarget(String action, String option, String opt2, Controller... targets) {
        this.setNightTarget(action, option, opt2, null, targets);
    }

    @Override
    public boolean isTargeting(String command, Controller... target_c) {
        PlayerList targets = ControllerList.list(target_c).toPlayerList(narrator);
        int ability = Ability.ParseAbility(command);

        return getActions().isTargeting(targets, ability);
    }

    @Override
    public Controller vote(Controller target) {
        this.vote(target.getPlayer(narrator));
        return this;
    }

    @Override
    public Player getPlayer(Narrator narrator) {
        return narrator.getPlayerByID(this.id);
    }

}
