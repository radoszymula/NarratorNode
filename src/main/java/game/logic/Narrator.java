package game.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.ai.RoleGroup;
import game.ai.RoleGroupList;
import game.event.Announcement;
import game.event.ArchitectChat;
import game.event.ChatMessage;
import game.event.DeadChat;
import game.event.DeathAnnouncement;
import game.event.EventList;
import game.event.EventLog;
import game.event.EventManager;
import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.event.OGIMessage;
import game.event.SelectionMessage;
import game.event.VoidChat;
import game.logic.exceptions.ChatException;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PhaseException;
import game.logic.listeners.CommandListener;
import game.logic.listeners.NarratorListener;
import game.logic.support.AlternateLookup;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.CustomRoleAssigner;
import game.logic.support.DeathDescriber;
import game.logic.support.HTString;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.RolePackage;
import game.logic.support.RoleTemplate;
import game.logic.support.Skipper;
import game.logic.support.StoryPackage;
import game.logic.support.Util;
import game.logic.support.action.Action;
import game.logic.support.attacks.Attack;
import game.logic.support.attacks.DirectAttack;
import game.logic.support.attacks.IndirectAttack;
import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.logic.templates.NativeStoryPackage;
import game.roles.Ability;
import game.roles.AbilityList;
import game.roles.Agent;
import game.roles.Amnesiac;
import game.roles.Architect;
import game.roles.Armorsmith;
import game.roles.ArmsDetector;
import game.roles.Blackmailer;
import game.roles.Blacksmith;
import game.roles.Bodyguard;
import game.roles.BreadAbility;
import game.roles.Burn;
import game.roles.Clubber;
import game.roles.Commuter;
import game.roles.Coroner;
import game.roles.Coward;
import game.roles.CultLeader;
import game.roles.Cultist;
import game.roles.Detective;
import game.roles.Disfranchise;
import game.roles.Disguiser;
import game.roles.Doctor;
import game.roles.Douse;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.Elector;
import game.roles.ElectroManiac;
import game.roles.Enforcer;
import game.roles.Executioner;
import game.roles.FactionKill;
import game.roles.FactionSend;
import game.roles.Framer;
import game.roles.Ghost;
import game.roles.GraveDigger;
import game.roles.GunAbility;
import game.roles.Gunsmith;
import game.roles.Hidden;
import game.roles.Infiltrator;
import game.roles.Interceptor;
import game.roles.Investigator;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Jester;
import game.roles.Joker;
import game.roles.Lookout;
import game.roles.Mason;
import game.roles.MasonLeader;
import game.roles.MassMurderer;
import game.roles.Operator;
import game.roles.ParityCheck;
import game.roles.Poisoner;
import game.roles.ProxyKill;
import game.roles.Scout;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.Silence;
import game.roles.Snitch;
import game.roles.Spy;
import game.roles.Stripper;
import game.roles.Tailor;
import game.roles.TeamTakedown;
import game.roles.Thief;
import game.roles.Undouse;
import game.roles.Ventriloquist;
import game.roles.VestAbility;
import game.roles.Veteran;
import game.roles.Witch;
import game.roles.support.Bread;
import game.roles.support.Gun;
import game.roles.support.Vest;
import models.Command;
import models.FactionRole;
import models.SetupHidden;
import models.enums.GamePhase;
import models.enums.VoteSystemTypes;
import models.logic.vote_systems.DiminishingPoolVoteSystem;
import models.logic.vote_systems.PluralityVoteSystem;
import models.logic.vote_systems.VoteSystem;
import nnode.Config;
import services.FactionService;
import services.RoleAssignmentService;
import services.RoleService;

public class Narrator {
    public static final int NORMAL_HEALTH = 0;
    public static final Player NARRATOR = null;

    /**
     * Default Constructor
     */
    public Narrator() {
        resetNarrator();
    }

    // this isn't accounting for started games
    public Narrator copy() {
        StoryPackage sp = NativeStoryPackage.deserialize(this);
        if(isStarted)
            sp.runCommands();
        return sp.narrator;
    }

    public Player skipper;
    public VoteSystem voteSystem;
    public HashSet<RolePackage> unpickedRoles;

    private void resetNarrator() {
        _factions.remove(Constants.A_SKIP);
        FactionService.createFaction(this, Constants.A_SKIP, "Skip Team");
        events = new EventManager(this);

        this.remIcons = new ArrayList<>();
        for(String s: ICONS)
            remIcons.add(s);
        Collections.shuffle(remIcons);

        skipper = new Skipper(this);
        random = new Random();
        setSeed(0);
        pendingAnnouncements = new ArrayList<>();

        cListeners = new ArrayList<>();

        aliasMap = new HashMap<>();
        aliasMap.put(CultLeader.MINION_ALIAS, Cultist.class.getSimpleName());
        aliasMap.put(Bread.ALIAS, Bread.class.getSimpleName().toLowerCase());
        aliasMap.put(Gun.ALIAS, Gun.class.getSimpleName().toLowerCase());
        aliasMap.put(Vest.ALIAS, Vest.class.getSimpleName().toLowerCase());
        aliasMap.put(VOTE_KILL_ALIAS, "hang");
        this.possibleRoles = null;
        this.initialRoles = null;

        this.rolesList.resetSpawns();
        this.unpickedRoles = null;

        // purposefully not redoing the role assigner
    }

    public static String VOTE_KILL_ALIAS = "vkill_alias";

    private Random random;
    public Random idRandomer = new Random();

    /**
     * Gets the random variable used for Narrator. This variable is used everywhere
     * else a random call is made, so if you ever wanted to reproduce a game with
     * the same outcomes, all you'd have to do is set the seed for this random
     * object. Do not call it for other applications.
     *
     * @return the Narrator's random object.
     */
    public Random getRandom() {
        return random;
    }

    /**
     * Sets the seed for the Narrator's internal random object.
     *
     * @param seed the value of the seed
     */
    public void setSeed(long seed) {
        this.seed = seed;
        random = new Random();
        random.setSeed(seed);
    }

    private long seed;

    /**
     * Returns the seed for the random object set by the user.
     *
     * @return the seed
     */
    public long getSeed() {
        return seed;
    }

    public SetupModifiers _rules = new SetupModifiers();

    public boolean getBool(SetupModifier r_id) {
        return _rules.getBool(r_id);
    }

    public int getInt(SetupModifier r_id) {
        return _rules.getInt(r_id);
    }

    public boolean setRule(SetupModifier r_id, boolean val) { // change to setValue
        return _rules.setBool(r_id, val);
    }

    public int setRule(SetupModifier r_id, int val) {
        return _rules.setInt(r_id, val).getIntVal();
    }

    public Rule getRule(SetupModifier r_id) {
        return _rules.getRule(r_id);
    }

    public PlayerList _players = new PlayerList();

    /**
     * @return the number of players in the game
     */
    public int getPlayerCount() {
        return _players.size();
    }

    /**
     * The appropriate way to generate players for a Narrator instance
     *
     * @param userID the name of the player
     * @param comm   the communicator type of the player
     * @return the Player
     */
    private static final String[] INVALID_NAMES = {
            Armorsmith.FAKE, Blacksmith.NOGUN, Gunsmith.FAULTY, Gunsmith.REAL, Joker.NO_BOUNTY, Constants.DAY_CHAT,
            Constants.DEAD_CHAT, "null", Constants.LAST_WILL, Constants.TEAM_LAST_WILL
    };

    public void nameCheck(String name, boolean allowSpaces) {
        if(name == null)
            throw new NamingException("Name cannot be null");
        if(name.length() > Constants.MAX_DB_PLAYER_LENGTH)
            throw new NamingException("Name too long");
        if(name.contains(" ") && !allowSpaces)
            throw new NamingException("No spaces in names");
        if(Util.hasNonAlphaNumericString(name))
            throw new NamingException("Names must be alphanumeric");
        if(name.equals(""))
            throw new NamingException("Cannot have empty names");
        if(Util.isInt(name))
            throw new NamingException("Name cannot be a number!");
        for(String invalid: INVALID_NAMES){
            if(invalid.equalsIgnoreCase(name))
                throw new NamingException("Reserved name");
        }
        for(String command: Ability.COMMAND_RESERVED_NAMES)
            if(command.equalsIgnoreCase(name))
                throw new NamingException("Reserved name");

        if(name.contains("\n"))
            throw new NamingException("No new lines in names");

        if(name.contains(ArchitectChat.KEY_SEPERATOR))
            throw new NamingException("Cannot use " + ArchitectChat.KEY_SEPERATOR + " in name.");

        for(String invalid: DrugDealer.DRUGS)
            if(invalid.equalsIgnoreCase(name))
                throw new NamingException("Reserved name");
        if(name.startsWith("#") && name.length() == 7)
            throw new NamingException("Cannot use a color value as a name");
        for(Faction t: getFactions()){
            if(name.equalsIgnoreCase(t.getName()))
                throw new NamingException("Cannot name yourself a name that's already being used by a team name", t);
            if(name.equalsIgnoreCase(t.getColor()))
                throw new NamingException("Cannot use a color value as a name");
        }
        for(SetupHidden setupHidden: this.getRolesList()){
            if(setupHidden.hidden.getName().equalsIgnoreCase(name))
                throw new NamingException("Name in use by role", setupHidden.hidden);
        }
        Player existing = getPlayerByID(name);
        if(existing != null)
            throw new NamingException("Player name taken", existing);
    }

    public boolean acceptablePlayerName(String name) {
        return acceptablePlayerName(null, name);
    }

    public boolean acceptableTeamName(String name) {
        try{
            nameCheck(name, true); // no spaces in name
            return true;
        }catch(NamingException e){
            return false;
        }
    }

    public boolean acceptablePlayerName(Player player, String name) {
        try{
            nameCheck(name, false); // no spaces in name
            return true;
        }catch(NamingException e){
            if(e.playerHolder == player && player != null)
                return true;
            return false;
        }

    }

    /**
     * The appropriate way to generate players for a Narrator instance
     *
     * @param name the name of the player
     * @return the Player
     */
    public Player addPlayer(String name) {
        if(isStarted)
            throw new PhaseException("Cannot add players if game has already started");

        synchronized (getPlayerByNameLock){
            nameCheck(name, false); // no spaces in name
            Player p = new Player(name, this);
            _players.add(p);
            return p;
        }
    }

    /**
     * Removes the player from the Narrator's list of internal players
     *
     * @param p the Player to be removed
     */
    public void removePlayer(Player p) {
        _players.remove(p);
    }

    /**
     * Nukes all the players in the game
     *
     */
    public void removeAllPlayers() {
        _players.clear();
    }

    public Narrator removeAllFactionsAndRoles() {
        for(Role role: new LinkedList<>(roles))
            RoleService.deleteRole(role);
        this._factions = new HashMap<>();
        resetNarrator();
        return this;
    }

    /**
     * Removes a player by name
     *
     * @param name the name to look up
     */
    public void removePlayer(String name) {
        if(isStarted)
            throw new IllegalActionException();
        Player rem = getPlayerByID(name);
        if(rem != null)
            _players.remove(rem);
    }

    public final Object getPlayerByNameLock = new Object();

    /**
     * Retrieves the Player using their name
     *
     * @param name the name to look up the person by
     * @return the Player found
     */
    public Player getPlayerByName(String name) {
        return getPlayerByName(name, null);
    }

    public Player getPlayerByName(String name, AlternateLookup aLookup) {
        synchronized (getPlayerByNameLock){
            try{
                int position = Integer.parseInt(name) - 1;
                if(position >= 0 && position < _players.size()){
                    return _players.get(position);
                }
            }catch(NumberFormatException e){
            }
            for(Player p: _players){
                if(p.getName().equalsIgnoreCase(name)){
                    return p;
                }
            }
            if(aLookup != null)
                return aLookup.lookup(name);
        }
        return null;
    }

    public Player getPlayerByID(String name) {
        synchronized (getPlayerByNameLock){
            for(Player p: _players){
                if(p.getID().equalsIgnoreCase(name)){
                    return p;
                }
            }
        }
        return null;
    }

    /**
     * Returns a list of all the players in the game. The player references are in
     * it, but the returned collection object is a copy. You are free to add or
     * remove players from it as you wish, as it won't affect it.
     *
     * @return the collection of players
     */
    public PlayerList getAllPlayers() {
        synchronized (_players){
            return _players.copy();
        }
    }

    /**
     * Returns a list of all the dead players in the game.
     *
     * @return the collection of dead players
     */
    public PlayerList getDeadPlayers() {
        return _players.getDeadPlayers();
    }

    /**
     * Returns a list of all the live players in the game.
     *
     * @return the collection of live players
     */
    public PlayerList getLivePlayers() {
        return _players.getLivePlayers();
    }

    public HashMap<String, Faction> _factions = new HashMap<>();

    /**
     * Returns all the teams in the game
     *
     * @return a list of all the Teams in the game
     */
    public ArrayList<Faction> getFactions() {
        ArrayList<Faction> team = new ArrayList<Faction>();
        for(Faction faction: _factions.values()){
            if(!Constants.A_SKIP.equals(faction.getColor()))
                team.add(faction);
        }
        return team;
    }

    public Integer roleIDCounter = 0;

    public void roleTemplateNameCheck(RoleTemplate rt) {
        try{
            nameCheck(rt.getName(), true); // allow spaces
        }catch(NamingException e){
            if(e.rt == null)
                throw e;
            if(e.rt instanceof Hidden && rt instanceof Hidden){
                Hidden hidden1 = (Hidden) e.rt;
                Hidden hidden2 = (Hidden) rt;
                if(hidden1.getColors().equals(hidden2.getColors()))
                    throw e;
                return;
            }else if(rt.getClass() != e.rt.getClass())
                return;
            // both naming conflicts are roles
            AbilityList a1 = ((Role) rt).getBaseAbility();
            AbilityList a2 = ((Role) e.rt).getBaseAbility();
            if(!Ability.isEqualContent(a1, a2))
                throw e;
        }
    }

    public void checkCreateRoleName(Role newRole) {
        roleTemplateNameCheck(newRole);
        try{
            nameCheck(newRole.getName(), true);
        }catch(NamingException e){
            if(e.rt == null || e.rt instanceof Role)
                throw e;
        }
        for(Role oldRole: this.roles){
            if(oldRole.getName().equals(newRole.getName()))
                throw new NamingException("Name is in use by another role.");
        }
    }

    public void deleteRole(Role role) {
        RoleService.deleteRole(role);
    }

    public void deleteHidden(Hidden deletedHidden) {
        Iterator<Hidden> iterator = this.hiddens.iterator();
        Hidden hidden;
        while (iterator.hasNext()){
            hidden = iterator.next();
            if(hidden == deletedHidden)
                iterator.remove();
        }
    }

    /**
     * The appropriate way to instantiate a Team for the Narrator
     *
     * @param teamName the color ID of the Team
     * @return the created Team object
     */

    /**
     * Removes the role, whether its random or not, from the Narrator
     *
     * @param hidden the Role to be removed
     */
    public void removeSetupHidden(Hidden hidden) {
        this.rolesList.remove(hidden);
    }

    public void removeAllSetupHiddens(Hidden hidden) {
        this.rolesList.removeAll(hidden);
    }

    public MemberList possibleRoles, initialRoles;

    public MemberList getPossibleMembers() {
        if(possibleRoles != null)
            return possibleRoles; // rename to roles
        MemberList mList = initialMembers().copy();

        FactionRole role;
        ArrayList<Faction> factions = mList.getFactions(CultLeader.class, this);
        if(mList.hasRole(CultLeader.class) && getBool(SetupModifier.CULT_KEEPS_ROLES)){ // TODO
            HashMap<String, FactionRole> recruitables = new HashMap<>();
            for(Faction recruitableFaction: getFactions()){
                if(recruitableFaction.canRecruitFrom())
                    for(FactionRole q: mList.getFactionRoles(recruitableFaction.getColor()))
                        recruitables.put(q.role.getName(), q);

            }
            for(Faction cultTeam: factions){
                for(FactionRole recruitable: recruitables.values()){
                    role = new FactionRole(cultTeam, recruitable.role);
                    mList.add(role);
                }
            }
        }else if(mList.hasRole(CultLeader.class) && getBool(SetupModifier.CULT_PROMOTION)){
            for(Faction cultTeam: factions){
                for(Option option: new CultLeader().getOptions(skipper)){
                    role = new FactionRole(cultTeam, new Role(this, option.value, Ability.CREATOR(option.value)));
                    mList.add(role);
                }
            }
        }
        possibleRoles = mList;
        return mList;
    }

    public MemberList initialMembers() {
        if(initialRoles != null)
            return initialRoles;
        MemberList mList = new MemberList();
        for(Faction faction: getFactions()){
            for(FactionRole m: faction.getAllMembers())
                mList.add(m);
        }
        initialRoles = mList;
        return mList;
    }

    /**
     * Returns a neatly sorted list of all roles
     *
     * @return list of all Roles
     */
    public RolesList rolesList = new RolesList();

    public RolesList getRolesList() {
        return rolesList.copy();
    }

    /**
     * returns the associated Team by color id. you can't get the skip team from
     * this method
     *
     * @param color the color of the team
     * @return the Team object
     */
    public Faction getFaction(String color) {
        if(color == null || color.equals(Constants.A_RANDOM))
            return null;
        return _factions.get(color);
    }

    public Faction getFactionByID(long factionID) {
        for(Faction faction: _factions.values())
            if(faction.id == factionID)
                return faction;
        return null;
    }

    public Faction getFactionByName(String teamToFind) {
        if(teamToFind == null)
            return null;
        teamToFind = teamToFind.replace(" ", "");
        String teamName;
        for(Faction t: _factions.values()){
            teamName = t.getName();
            teamName = teamName.replace(" ", "");
            if(teamName.equalsIgnoreCase(teamToFind))
                return t;
        }
        return null;
    }

    /**
     * Removes the team from the game
     *
     * @param t the team to be removed
     */
    public void removeTeam(Faction t) {
        if(t == null)
            throw new NullPointerException();

        for(SetupHidden setupHidden: getRolesList()){
            for(FactionRole factionRole: setupHidden.hidden.getFactionRoles())
                if(factionRole.faction.getColor().equals(t.getColor()))
                    setupHidden.hidden.removeFactionRole(factionRole);
            if(setupHidden.hidden.factionRolesMap.isEmpty())
                removeAllSetupHiddens(setupHidden.hidden);
        }
        _factions.remove(t.getColor());
    }

    public void removeTeam(String t) {
        removeTeam(getFaction(t));
    }

    // cross faction randoms
    public List<Hidden> hiddens = new ArrayList<>();
    public List<Role> roles = new ArrayList<>();

    /**
     * Returns a list of players with the given role. Primarily used for gathering
     * people with the same role to do night actions with
     *
     * @param roleName the name of the role
     * @return the list of players found
     */
    private PlayerList getRoles(Class<? extends Ability> roleName) {
        PlayerList listToGet = new PlayerList();
        for(Player p: _players){
            if(p.is(roleName)){
                listToGet.add(p);
            }
        }
        return listToGet;
    }

    /**
     * Determines if a the setup is okay to run as is. If not, it'll throw an error
     * describing what is wrong.
     *
     */
    public void runSetupChecks() {
        sizeCheck();
        playerNumberCheck();
        opponentCheck();
        sheriffChecks();
        invalidRulesCheck();
        uniqueChecks();
        if(!_rules.getBool(SetupModifier.CHAT_ROLES))
            chatRoleChecks();
        randomChecks();
    }

    /**
     * Checks for multiple inputed Mayors, GodFathers and the like, or other unique
     * roles
     *
     */
    private void uniqueChecks() {
        List<String> uniques = new ArrayList<>();

        FactionRole factionRole;
        for(SetupHidden setupHidden: this.rolesList){
            if(!setupHidden.hidden.isHiddenSingle())
                continue;
            factionRole = setupHidden.hidden.getFactionRoles().iterator().next();
            if(!factionRole.role.isUnique())
                continue;
            String key = factionRole.role.getName() + factionRole.faction.getColor();
            if(uniques.contains(key))
                throw new IllegalRoleCombinationException("Because " + factionRole.role.getName()
                        + " is unique, you cannot explicitly have 2 of them in the roles list.");
            uniques.add(key);
        }

        RolesList rolesListCopy = getRolesList();
        if(rolesListCopy.contains(MasonLeader.class))
            return;
        if(!rolesListCopy.contains(Mason.class))
            return;
        if(getBool(SetupModifier.MASON_PROMOTION))
            return;
        // doesn't handle if mason and mason leader are on different teams

        int masonCount = 0;
        for(SetupHidden setupHidden: this.rolesList){
            if(setupHidden.hidden.contains(Mason.class))
                masonCount++;
        }

        if(masonCount == 1)
            throw new IllegalGameSettingsException("Mason will spawn by themselves");

    }

    private void chatRoleChecks() {
        Hidden hidden;
        rmLabel: for(SetupHidden setupHidden: rolesList){
            hidden = setupHidden.hidden;
            if(hidden.getSize() == 0)
                throw new IllegalRoleCombinationException(hidden.getName() + " cannot spawn any roles!");
            for(FactionRole factionRole: hidden.getFactionRoles()){
                if(!factionRole.role.isChatRole()){
                    hidden.removeChatRoles();
                    continue rmLabel;
                }
            }
            throw new IllegalGameSettingsException(
                    hidden.getName() + " can only spawn chat roles, and chat roles are currently disabled.");
        }
    }

    /**
     * Checks for invalid random roles that don't spawn more than 2 choices
     *
     */
    private void randomChecks() {
        for(Hidden r: hiddens){
            int size = r.getSize();
            if(size == 0)
                throw new IllegalRoleCombinationException(r.getName() + " cannot spawn any roles!");
        }
    }

    /**
     * Checks for a minimum of 3 players
     *
     */
    private void sizeCheck() {
        if(_players.size() < 3){
            String message = "You need at THE VERY least 3 players to start a mafia game.";
            throw new IllegalGameSettingsException(message);
        }
    }

    /**
     * Checks if there are two opposing teams in the game
     *
     */
    private void opponentCheck() {
        RoleGroupList roleGroup = new RoleGroupList();
        for(SetupHidden hidden: getRolesList()){
            if(!hidden.hidden.contains(Thief.class))
                roleGroup.add(new RoleGroup().add(hidden.hidden));
        }

        int oppositionCount = (roleGroup.size() - getPlayerCount()) + 1;
        for(int i = 0; i < oppositionCount; i++){
            hasOpposingSpawns(roleGroup);
        }
    }

    public static void hasOpposingSpawns(RoleGroupList roleGroup) {
        for(RoleGroup roleGroup1: roleGroup){
            for(RoleGroup roleGroup2: roleGroup){
                if(roleGroup1 == roleGroup2)
                    continue;
                if(roleGroup1.opposes(roleGroup2)){
                    roleGroup.remove(roleGroup1);
                    roleGroup2.add(roleGroup1);
                    return;
                }
            }
        }
        throw new IllegalGameSettingsException("Lacking Valid Opponents");
    }

    /**
     * Makes sure that if a sheriff is spawned, it has something to detect
     *
     */
    private void sheriffChecks() {
        ArrayList<Faction> teams_ = new ArrayList<>();
        for(Faction t: this.getFactions())
            teams_.add(t);

        teamFound: for(Faction sheriffTeam: _factions.values()){
            if(!sheriffTeam.has(Sheriff.class))
                continue;
            if(!rolesList.contains(Sheriff.class, sheriffTeam.getColor()))
                continue;

            for(SetupHidden setupHidden: this.rolesList){
                // if the random role spawns a detectable team
                if(sheriffTeam.sheriffDetectsAll(setupHidden.hidden.getFactionColors()))
                    continue teamFound;
            }

            throw new IllegalGameSettingsException("Team " + sheriffTeam.getName() + " is unable to detect.");

        }

        rTeamFound: for(Faction faction: teams_){
            Role role = new Role(this, "Sheriff", new Sheriff());
            Faction sheriffTeam = faction;
            for(Hidden hidden: hiddens){
                if(!hidden.spawns(role))
                    continue;

                rEnemyChecking: for(int i = 0; i < _factions.size(); i++){
                    Faction detectTeam = teams_.get(i);
                    if(detectTeam.getAllMembers().isEmpty())
                        continue rEnemyChecking;

                    if(sheriffTeam.sheriffDetects(detectTeam.getColor()))
                        continue rTeamFound;

                }

                for(SetupHidden setupHidden: this.rolesList){
                    // if the random role spawns a detectable team
                    if(sheriffTeam.sheriffDetectsAll(setupHidden.hidden.getFactionColors()))
                        continue rTeamFound;
                }

                throw new IllegalGameSettingsException(
                        "Team " + sheriffTeam.getName() + " is unable to detect sheriffs");

            }
        }

    }

    private void invalidRulesCheck() {
        if(getBool(SetupModifier.CHAT_ROLES) && getBool(SetupModifier.HOST_VOTING))
            throw new IllegalGameSettingsException("Cannot have chat roles and host controlled voting in a game.");

        if(getBool(SetupModifier.CULT_KEEPS_ROLES) && getBool(SetupModifier.CULT_PROMOTION))
            throw new IllegalGameSettingsException(
                    "Invalid cult settings.  Recruits cannot keep roles and also be 'powered up'");

        if(getBool(SetupModifier.DAY_START) == NIGHT_START && 0 == getInt(SetupModifier.NIGHT_LENGTH))
            throw new IllegalGameSettingsException("Can't have night start with a nightless setup");

        boolean nightLess = getInt(SetupModifier.NIGHT_LENGTH) == 0;
        for(Faction t: getFactions()){
            if(t.hasSharedAbilities() && !t.knowsTeam()){
                throw new IllegalGameSettingsException(t.getName() + " can kill but doesn't know their teammates");
            }
            if(t.hasNightChat() && !t.knowsTeam())
                throw new IllegalGameSettingsException(
                        t.getName() + " has a night chat but doesn't know their teammates");

            if(t.hasEnemies() && (t.has(Ghost.class) || t.has(Jester.class) || t.has(Executioner.class))){
                throw new IllegalGameSettingsException(t.getName()
                        + " cannot have enemies and have ghosts, jesters, or executioners as possible roles");
            }
        }

        Faction faction;
        ArrayList<String> seenColors = new ArrayList<>();
        for(FactionRole factionRole: getRolesList().getMembers()){
            if(factionRole.role.contains(CultLeader.class) && !seenColors.contains(factionRole.faction.getColor())){
                if(!getFaction(factionRole.faction.getColor()).knowsTeam())
                    throw new IllegalGameSettingsException(factionRole.role.getName() + " of team "
                            + getFaction(factionRole.faction.getColor()).getName() + " must know teammates");
                seenColors.add(factionRole.faction.getColor());
            }
            if(!factionRole.role.canStartWithEnemies() && getFaction(factionRole.faction.getColor()).hasEnemies())
                throw new IllegalGameSettingsException(getFaction(factionRole.faction.getColor()).getName()
                        + " cannot have enemies and have ghosts, jesters, or executioners as possible abilities");

            factionRole.role.checkVisibleAbilities();

            if(!nightLess)
                continue;

            factionRole.role.checkIsNightless();
            faction = factionRole.faction;
            if(faction.hasSharedAbilities()){
                for(Ability a: faction.getAbilities())
                    a.checkIsNightless(this);
                if(faction.hasNightChat())
                    throw new IllegalGameSettingsException("No nightchats allowed in nightless: " + faction.getName());
            }

        }
    }

    private void playerNumberCheck() {
        int playerCount = _players.size();
        int roleListSize = getRolesList().size();
        if(roleListSize == playerCount)
            return;

        String message;
        if(roleListSize > playerCount){
            if(getRolesList().contains(Thief.class)){
                if(roleListSize - playerCount > 1)
                    return;
                message = "Thief add has no effect.";
            }else
                message = "Not enough players. Remove " + (roleListSize - playerCount) + " roles or wait for more.";
        }else
            message = "Too many players. Add more roles or remove " + (playerCount - roleListSize) + " player(s).";
        throw new IllegalGameSettingsException(message);
    }

    public Narrator restartGame(boolean startGame, boolean resetPrefs) {
        if(!this.isStarted)
            throw new NarratorException("No reason to restart game if game isn't started.");
        isStarted = false;
        dayStartingUp = false;
        win = null;
        for(Faction t: getFactions()){
            t.reset();
        }

        for(Player p: _players){
            p.reset(resetPrefs);
        }
        listeners.clear();
        cListeners.clear();
        if(deathDescriber != null)
            deathDescriber.cleanup();
        deathDescriber = null;
        long seed = getSeed();
        HashMap<String, String> oldAlias = this.aliasMap;
        resetNarrator();
        this.aliasMap = oldAlias;
        setSeed(seed);
        if(startGame)
            startGame();
        return this;
    }

    private boolean isStarted = false;
    private boolean firstPhaseStarted = false;

    public String willStart() {
        if(isStarted)
            return "Cannot start game again";
        try{
            runSetupChecks();
            return null;
        }catch(NarratorException e){
            return e.getMessage().toString();
        }
    }

    public CustomRoleAssigner altRoleAssignment;
    public ArrayList<Happening> prehappenings;

    public void startGame() {
        if(isStarted)
            throw new NarratorException("Cannot start game again.");

        random.reset();
        runSetupChecks();

        for(Faction t: _factions.values())
            t.beforeGameStart();

        RolesList list = getRolesList();
        list.assignSort();

        _players.sortByID(); // TOOD really don't need this

        if(Config.getBoolean("random_role_assignment"))
            _players = _players.shuffle(new Random(), "initial shuffling for role assignments");

        for(Player p: _players){
            events.dayChat.addAccess(p);
            events.voidChat.addAccess(p);
        }

        this.prehappenings = new ArrayList<>();
        this.generatedRoles = new ArrayList<>();
        if(altRoleAssignment == null)
            assignRoles(list);
        else{
            altRoleAssignment.assign();
            altRoleAssignment = null;
        }

        saveUnassignedRolePackages();

        this.generatedRoles = null;
        this.remainingRoles = null;

        cleanupTeams();
        possibleRoles = null;
        initialRoles = null;
        actionStack = new ArrayList<Action>();

        isStarted = true;

        this.random.reset();
        masonLeaderPromotion();
        firstPhaseStarted = true;

        _players.sortByName();

        boolean dayStart = _rules.getBool(SetupModifier.DAY_START);
        boolean hasPotentialThief = this.getRolesList().contains(Thief.class);
        if(dayStart == DAY_START)
            dayNumber = 1;
        else
            dayNumber = 0;

        initializeVoteSystem();

        if(dayStart == NIGHT_START){
            phase = GamePhase.NIGHT_ACTION_SUBMISSION;
            events.initializeTeamChats();

            for(Happening h: prehappenings)
                getEventManager().voidChat.add(h);
        }else{
            for(Happening h: prehappenings)
                getEventManager().getDayChat().add(h);
        }
        prehappenings = null;

        saveAliases();

        // I might, in the future, have to do a onGameStarting and onGameStarted
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onGameStart();
        if(hasPotentialThief)
            startRolePickingPhase();
        else
            startPlayerPhases();
    }

    private void saveUnassignedRolePackages() {
        this.unpickedRoles = new HashSet<>();
        this.unpickedRoles.addAll(this.generatedRoles);
    }

    private void initializeVoteSystem() {
        switch (getInt(SetupModifier.VOTE_SYSTEM)){
        case VoteSystemTypes.PLURALITY:
            voteSystem = new PluralityVoteSystem(this);
            break;

        case VoteSystemTypes.DIMINISHING_POOL:
            voteSystem = new DiminishingPoolVoteSystem(this);
        }
    }

    private void cleanupTeams() {
        HashSet<String> teamColors = new HashSet<String>();
        teamColors.add(Constants.A_SKIP);
        for(SetupHidden setupHidden: rolesList){
            for(FactionRole factionRole: setupHidden.hidden.getFactionRoles()){
                teamColors.add(factionRole.faction.getColor());
            }
        }
        Iterator<Hidden> hiddenIterator = this.hiddens.iterator();
        Hidden hidden;
        while (hiddenIterator.hasNext()){
            hidden = hiddenIterator.next();
            if(!rolesList.contains(hidden))
                hiddenIterator.remove();
        }

        Iterator<Role> roleIterator = this.roles.iterator();
        Role role;
        while (roleIterator.hasNext()){
            role = roleIterator.next();
            if(!rolesList.contains(role))
                roleIterator.remove();
        }

        HashMap<String, Faction> oldTeams = _factions;
        _factions = new HashMap<>();

        Faction faction, emptyTeam;
        FactionRole m;
        Iterator<FactionRole> memberIterator;
        for(String validColor: teamColors){
            faction = oldTeams.get(validColor);
            _factions.put(validColor, faction);

            for(String removedColor: oldTeams.keySet()){
                if(!teamColors.contains(removedColor)){// it's actually a removed team color
                    emptyTeam = oldTeams.get(removedColor);
                    faction.addAlly(emptyTeam);
                    faction.removeSheriffDetectableTeam(removedColor);
                }

            }
            memberIterator = faction._roleSet.values().iterator();
            while (memberIterator.hasNext()){
                m = memberIterator.next();
                if(!isElementOf(rolesList, m))
                    memberIterator.remove();
            }
            if(!faction.has(Sheriff.class))
                faction.clearSheriffDetectables();
        }
    }

    private static boolean isElementOf(RolesList rolesList, FactionRole role) {
        for(SetupHidden setupHidden: rolesList)
            if(setupHidden.hidden.contains(role))
                return true;
        return false;
    }

    /**
     * Assigns roles to players
     *
     * @param factionRolesMap a shuffled list of roles Passed in because if debug
     *                        mode is on, it's sorted instead of shuffled
     */
    public ArrayList<RolePackage> generatedRoles;
    public RolesList remainingRoles;

    private void assignRoles(RolesList list) {
        remainingRoles = list;

        for(int i = 0; i < list.size();){
            list.get(0).hidden.generateRole(this);
            list.removeHead();
        }

        Mason.ForceRespawn(this, generatedRoles);
        Map<String, Set<RolePackage>> preferenceMap = RoleAssignmentService.getRoleLookupMap(this.generatedRoles);

        PlayerList roleDesirers;
        List<RolePackage> preferredRoles;
        Map<Player, List<RolePackage>> playerToRoles = new LinkedHashMap<>();
        HashMap<RolePackage, PlayerList> roleToPlayer = new LinkedHashMap<>();
        for(Player player: _players){
            preferredRoles = player.cleanPrefers(preferenceMap);
            playerToRoles.put(player, preferredRoles);

            for(RolePackage rp: preferredRoles){
                roleDesirers = roleToPlayer.get(rp);
                if(roleDesirers == null){
                    roleDesirers = new PlayerList(player);
                    roleToPlayer.put(rp, roleDesirers);
                }else if(!player.in(roleDesirers))
                    roleDesirers.add(player);
            }
        }
        preferenceMap = null;

        Map<Player, RolePackage> assignedRoles = RoleAssignmentService.giveRolesToPlayers(this, playerToRoles,
                roleToPlayer);

        // safe to reorder the names
        random.reset();
        for(Player p: _players.sortByID())
            assignedRoles.get(p).assign(p);
    }

    private void saveAliases() {
        for(String key: aliasMap.keySet()){
            events.addCommand(skipper, CommandHandler.ALIAS, key, aliasMap.get(key));
        }
    }

    private int dayNumber = 0;

    /**
     * Returns the current day
     *
     * @return the day number
     */
    public int getDayNumber() {
        return dayNumber;
    }

    public static final boolean NIGHT_START = false;
    public static final boolean DAY_START = true;
    public GamePhase phase = GamePhase.UNSTARTED;

    /**
     * Returns whether the day is going on or not
     *
     * @return whether its day and the game's going, or not
     */
    public boolean isDay() {
        if(phase == GamePhase.ROLE_PICKING_PHASE)
            return false;
        return phase != GamePhase.NIGHT_ACTION_SUBMISSION && isInProgress();
    }

    /**
     * Returns whether the night is going on or not
     *
     * @return whether its night and the game's going, or not
     */
    public boolean isNight() {
        return phase == GamePhase.NIGHT_ACTION_SUBMISSION && isInProgress();
    }

    public boolean isFirstNight() {
        if(!isInProgress() || phase == GamePhase.VOTE_PHASE)
            return false;
        else if(_rules.getBool(SetupModifier.DAY_START))
            return dayNumber == 1;
        return dayNumber == 0;
    }

    public boolean isFirstDay() {
        if(!isInProgress() || phase == GamePhase.NIGHT_ACTION_SUBMISSION)
            return false;
        return dayNumber == 1;
    }

    public boolean isFirstPhase() {
        if(!isInProgress())
            return false;
        else if(dayNumber == 0)
            return true;
        else if(getBool(SetupModifier.DAY_START) && dayNumber == 1)
            return true;
        else
            return false;
    }

    /**
     * Helper method that starts the night, clears the votes, resets player
     * statuses, and notifies listeners that the game has started
     *
     * @param lynchedList the players who were lynched earlier in the day
     * @param poisoned
     */
    private void startNight(PlayerList lynchedList, PlayerList poisoned, EventList dayHappenings) {
        if(this.isFirstDay())
            this.events.initializeTeamChats();

        phase = GamePhase.NIGHT_ACTION_SUBMISSION;
        nightPhase = GamePhase.NIGHT_ACTION_SUBMISSION;
        if(!isInProgress()){
            dayNumber++;
            return;
        }

        nightList.clear();

        // have to create the architect chats so architect knows how many chat rooms
        // they created
        this.events.restoreFactionChats();
        this.events.initializeNightLogs();

        for(Player p: _players){
            p.onNightStart();
        }

        for(Faction t: getFactions()){
            t.onNightStart();
        }

        this.events.deactivateDayChats();
        if(getInt(SetupModifier.NIGHT_LENGTH) == 0)
            endNight();
        else
            for(int i = 0; i < listeners.size(); i++)
                listeners.get(i).onNightPhaseStart(lynchedList, poisoned, dayHappenings);
    }

    private void startRolePickingPhase() {
        phase = GamePhase.ROLE_PICKING_PHASE;
        List<NarratorListener> listeners = getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onRolepickingPhaseStart();
    }

    private void resoveRolePickingPhase() {
        Thief.fillInMissingActions(this);
        doNightAction(Thief.MAIN_ABILITY);

        SelectionMessage e = new SelectionMessage(skipper, false);
        events.addCommand(skipper, CommandHandler.END_PHASE);
        e.dontShowPrivate();
    }

    private void startPlayerPhases() {
        // assigns targets to possible executioners
        random.reset();
        for(Faction t: _factions.values()){
            t.onGameStart();
        }
        for(Player p: _players.copy().sortByID()){
            p.onGameStart();
            if(getBool(SetupModifier.DAY_START))
                p.addChat(events.dayChat);
            else
                p.addChat(events.voidChat);
        }
        if(getBool(SetupModifier.DAY_START) == NIGHT_START)
            startNight(null, null, null);
        else
            startDay(new PlayerList());
    }

    public void endPhase() {
        if(phase == GamePhase.ROLE_PICKING_PHASE){
            resoveRolePickingPhase();
            startPlayerPhases();
        }else if(isNight())
            forceEndNight();
        else{
            voteSystem.endPhase();

            SelectionMessage e = new SelectionMessage(skipper, false);
            events.addCommand(skipper, CommandHandler.END_PHASE);
            e.dontShowPrivate();
        }
    }

    public void forceEndDay() {
        if(!isDay())
            return;
        SelectionMessage e = new SelectionMessage(skipper, false);
        events.addCommand(skipper, CommandHandler.END_PHASE);
        e.dontShowPrivate();
        voteSystem.endPhase();
    }

    // people who have completed their night actions
    private PlayerList nightList = new PlayerList();
    private int key = 0;

    /**
     * Signals to the Narrator that the Player has completed their actions. Once
     * every live player has submitted their action, the night will auto end
     *
     * @param p the player who wants to end the night
     */
    protected void endNight(Player p, boolean forced) {

        if(!nightList.contains(p))
            nightList.add(p);

        p.setSubmissionTime(key++);

        if(everyoneCompletedNightActions())
            endNight();
        else{
            for(int i = 0; i < listeners.size(); i++)
                listeners.get(i).onEndNight(p, forced);
        }
    }

    /**
     * Returns the people who have signaled that they want the night to end
     *
     * @return a list of people that have ended the night
     */
    public PlayerList getEndedNightPeople() {
        return nightList.copy();
    }

    /**
     * Allows a player to change his mind and unlock their night actions
     *
     * @param p the player who no longer wants to wait for night to end right away
     */
    protected void cancelEndNight(Player p) {
        nightList.remove(p);
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onCancelEndNight(p);
    }

    /**
     * Determines if the Player is ready to start the day. Shouldn't be accessed
     * directly. Get this information through the player
     *
     * @param p the requester
     * @return
     */
    protected boolean endedNight(Player p) {
        return nightList.contains(p);
    }

    /**
     * Determines whether night actions are permitted to be done right now
     *
     * @return if game is in progress and it's nighttime
     */
    protected boolean canDoNightAction() { // rename to submit
        if(!isStarted)
            return false;

        boolean gameStillGoing = isInProgress();
        return gameStillGoing && phase == GamePhase.NIGHT_ACTION_SUBMISSION;
    }

    /**
     * Determines whether everyone's completed their night actions. This throws
     * exceptions if it's not nighttime or if something internally has gone wrong.
     *
     * @return True if everyone has completed their night actions
     */
    public boolean everyoneCompletedNightActions() {
        if(phase != GamePhase.NIGHT_ACTION_SUBMISSION)
            throw new IllegalStateException("It is not night time");

        if(_players.size() < nightList.size())
            throw new IllegalStateException("Something is wrong with the input of night actions");

        int needToEndNight = 0;
        for(Player p: _players){
            if(p.isAlive() || p.is(Ghost.class))
                needToEndNight++;
        }
        return needToEndNight == nightList.size();
    }

    public PlayerList getNightWaitingOn() {
        PlayerList pl = new PlayerList();
        for(Player p: _players){
            if(p.isAlive() || p.is(Ghost.class)){
                if(!p.endedNight())
                    pl.add(p);
            }
        }
        return pl;
    }

    private EventManager events;

    /**
     * Creates a String representation of everything that's happened in the game for
     * easy vieweing and debugging
     *
     * @return String version of what has happened in the game
     */
    public String getHappenings() {
        return events.getEvents(Message.PRIVATE).access(Message.PRIVATE);
    }

    /**
     * Returns the EventManager object. This is in charge of keeping events ordered
     * and in the right chat topic.
     *
     * @return the Event Manager object
     */
    public EventManager getEventManager() {
        return events;
    }

    private ArrayList<CommandListener> cListeners;

    /**
     * The Command Listeners are notified for when things like voting, ending night,
     * day burning, etc. happen. Its used for recording games.
     *
     * @return the current objects that are listening for command inputs to the game
     */
    public ArrayList<CommandListener> getCommandListeners() {
        return cListeners;
    }

    /**
     * This returns the complete list of string commands needed to reproduce the
     * game to this point.
     *
     * @return
     */
    public ArrayList<Command> getCommands() {
        return events.getCommands();
    }

    /**
     * Helper method that resolves all night actions, sets feedback, and kills
     * people.
     *
     */

    public boolean pre_visiting_phase = true;
    public boolean pre_recruits = true;
    public boolean pre_bodyguard = true;
    public boolean healsComplete = false;
    public boolean proxyWitchVisiting = false;

    private void endNight() {
        nightPhase = GamePhase.NIGHT_ACTION_PROCESSING;
        actionStack.clear();
        pre_visiting_phase = true;
        pre_recruits = true;
        pre_bodyguard = true;
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onNightEnding();

        for(Faction t: _factions.values()){
            if(!t.hasSharedAbilities())
                continue;

            for(Ability a: t.getAbilities()){
                if(a instanceof FactionSend)
                    continue;
                t.determineNightActionController(a.getAbilityNumber());
            }
        }

        FactionList whereItCameFrom;
        boolean noOneElseHas;
        // removes extra targets
        for(Player p: _players){
            p.onNightEnd();
            for(Action a: p.getActions()){
                a.saveIntendedTargets();
                if(!a.isTeamAbility())
                    continue;
                whereItCameFrom = a.getFactions();
                noOneElseHas = true;
                ;
                for(Faction t: whereItCameFrom){
                    if(t.getFactionAbilityController(a.ability) == p){
                        noOneElseHas = false;
                        break;
                    }
                }
                if(noOneElseHas){
                    a.setInvalid();
                }
                a.saveIntendedTargets();
            }

        }

        nightPhase = GamePhase.PRE_KILLING;

        PlayerList newDeadList = new PlayerList();
        healsComplete = false;

        doNightAction(Commuter.MAIN_ABILITY);
        doNightAction(new PlayerList(), false, GraveDigger.MAIN_ABILITY);// no exclusions, reverse it
        doNightAction(Commuter.MAIN_ABILITY);
        doNightAction(ProxyKill.MAIN_ABILITY);
        doNightAction(Witch.MAIN_ABILITY);
        doNightAction(Commuter.MAIN_ABILITY);
        proxyWitchVisiting = true;
        doNightAction(ProxyKill.MAIN_ABILITY);// for the visitation
        proxyWitchVisiting = false;

        doNightAction(Snitch.MAIN_ABILITY);
        doNightAction(Stripper.MAIN_ABILITY);
        doNightAction(Jailor.MAIN_ABILITY);
        doNightAction(Operator.MAIN_ABILITY);
        doNightAction(Coroner.MAIN_ABILITY);
        doNightAction(Driver.MAIN_ABILITY);
        doNightAction(Coward.MAIN_ABILITY);
        doNightAction(VestAbility.MAIN_ABILITY); // getVestUsers

        doNightAction(Undouse.MAIN_ABILITY);
        doNightAction(Bodyguard.MAIN_ABILITY);
        pre_bodyguard = false;
        doNightAction(Douse.MAIN_ABILITY);

        kills();
        heals();

        doNightAction(Disguiser.MAIN_ABILITY);
        doNightAction(Commuter.MAIN_ABILITY);

        doNightAction(Janitor.MAIN_ABILITY);
        doNightAction(Silence.MAIN_ABILITY, Disfranchise.MAIN_ABILITY, Blackmailer.MAIN_ABILITY,
                Ventriloquist.MAIN_ABILITY, Ghost.MAIN_ABILITY, Elector.MAIN_ABILITY);
        doNightAction(Framer.MAIN_ABILITY);

        doNightAction(Enforcer.MAIN_ABILITY);
        doNightAction(MasonLeader.MAIN_ABILITY);
        doNightAction(CultLeader.MAIN_ABILITY);
        pre_recruits = false;
        doNightAction(CultLeader.MAIN_ABILITY);

        doNightAction(DrugDealer.MAIN_ABILITY);
        doNightAction(Gunsmith.MAIN_ABILITY, Blacksmith.MAIN_ABILITY, Armorsmith.MAIN_ABILITY,
                BreadAbility.MAIN_ABILITY);
        doNightAction(Amnesiac.MAIN_ABILITY);
        doNightAction(Sheriff.MAIN_ABILITY, ParityCheck.MAIN_ABILITY, Investigator.MAIN_ABILITY, Scout.MAIN_ABILITY,
                ArmsDetector.MAIN_ABILITY);
        doNightAction(Poisoner.MAIN_ABILITY);
        doNightAction(Tailor.MAIN_ABILITY);

        doNightAction(Jester.MAIN_ABILITY);
        doNightAction(Ability.VISIT_ABILITY);
        stalkings();

        Bread.removeExpiredBread(_players);

        TeamTakedown.handleNightSuicides(this);

        boolean onlyPoisoned, attacked;
        for(Player p: _players){
            if(p.isDead())
                continue;
            onlyPoisoned = true;
            attacked = false;
            for(Attack a: p.getInjuries()){
                if(a.isCountered())
                    continue;
                if(a.isImmune()){
                    if(a.getType() == Constants.POISON_KILL_FLAG)
                        p.setPoisoned(a.getAttacker());
                    continue;
                }
                if(p.getRealAutoVestCount() > 0 && a.getType() != Constants.JAIL_KILL_FLAG){
                    onlyPoisoned = false;
                    if(p.isSquelched())
                        p.silentUseAutoVest();
                    else{
                        p.useAutoVest();
                        new Feedback(p, Constants.AUTO_VEST_USAGE);
                    }
                }else if(a.getType() == Constants.POISON_KILL_FLAG){
                    p.setPoisoned(a.getAttacker());
                    continue;
                }else{
                    onlyPoisoned = false;
                    p.setDead(a.getCause());
                }
                attacked = true;
                break;
            }
            if(!attacked || p.isUsingAutoVest() || onlyPoisoned)
                continue;
            if(Janitor.isBeingCleaned(p)){
                p.getDeathType().setCleaned();
                Janitor.SendOutFeedback(p);
            }
            newDeadList.add(p);
        }

        _players.sortByName();

        // executioner changes
        for(Player p: getRoles(Executioner.class)){
            Executioner exec = p.getAbility(Executioner.class);
            for(Player newDead: getDeadList(dayNumber)){
                if(newDead.getDeathType().isLynch())
                    continue;
                if(!newDead.equals(exec.getTarget()))
                    continue;
                if(exec.getRealCharges() != 0 && !_players.getLivePlayers().remove(p).isEmpty()){
                    Executioner.assign(p);
                    new Happening(this).add(p, " got ", exec.getTarget(), " as a new target.");
                }else if(getBool(SetupModifier.EXECUTIONER_TO_JESTER)){
                    new Happening(this).add(p, " suicided over their failure to get ", exec.getTarget(),
                            " to be democratically executed.");
                    p.replaceAbility(p.getAbility(Executioner.class), new Jester());
                }else{
                    p.clearHeals();
                    new Happening(this).add(p,
                            " suicided over their failures to democratically execute their targets.");
                    p.kill(new IndirectAttack(Constants.JESTER_KILL_FLAG, p, null));
                    p.setDead(p);
                }
            }
        }

        _players.sortByName();

        // changing of roles
        for(Player p: _players)
            p.resolveRoleChange();

        masonLeaderPromotion();

        for(Player p: _players)
            p.sendFeedback();

        // ending
        nightList.clear();

        dayNumber++;
        Announcement e = null;
        for(Player dead: newDeadList){
            e = new DeathAnnouncement(dead, Announcement.NIGHT_DEATH);
            e.setPicture("tombstone");

            if(deathDescriber == null || !deathDescriber.hasDeath(dead)){
                for(Object o: DeathType.getInfoParts(dead))
                    e.add(o);

            }else{
                deathDescriber.populateDeath(dead, e);
            }

            dead.onDeath();
            announcement(e);
        }
        if(e == null){
            PlayerList noDead = new PlayerList();
            e = new DeathAnnouncement(noDead, this, Announcement.NIGHT_DEATH);
            e.add("Everyone appears accounted for.");
            announcement(e);
        }

        pushPendingAnnouncements();

        checkProgress();
        healsComplete = false;
        actionStack.clear();
        if(isInProgress()){
            for(Faction t: getFactions()){
                t.onNightEnd();
            }
            startDay(newDeadList);
        }
        newDeadList = null;
    }

    private ArrayList<Announcement> pendingAnnouncements;

    public void announcement(Announcement systemMessage) {
        if(isNight() || dayStartingUp){
            pendingAnnouncements.add(systemMessage);
            return;
        }
        if(!systemMessage.isAnnounceable())
            return;
        systemMessage.finalize();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onAnnouncement(systemMessage);
    }

    private void pushPendingAnnouncements() {
        random.shuffle(pendingAnnouncements, "Pending announcements");
        for(Announcement m: pendingAnnouncements){
            if(!m.isAnnounceable())
                continue;
            m.finalize();
            for(int i = 0; i < listeners.size(); i++)
                listeners.get(i).onAnnouncement(m);
        }
        pendingAnnouncements.clear();
    }

    void masonLeaderPromotion() {
        if(!getBool(SetupModifier.MASON_PROMOTION))
            return;

        ArrayList<Faction> masonFactions = getLivePlayers().filter(Mason.class).getFactions();
        Player newLeader;
        PlayerList members;
        for(Faction t: masonFactions){
            members = t.getMembers();
            if(!members.filter(MasonLeader.class).isEmpty())
                continue;

            members.remove(members.filter(Infiltrator.class));
            members = members.filter(Mason.class);
            if(members.isEmpty())
                continue;

            newLeader = members.getRandom(random);
            newLeader.addAbility(new MasonLeader());
            Role masonLeaderCard = getMasonLeader(t.getColor());
            newLeader.setRole(new FactionRole(t, masonLeaderCard));
        }
    }

    private Role getMasonLeader(String color) {
        for(Player deadPlayer: getDeadPlayers()){
            if(deadPlayer.is(MasonLeader.class))
                return deadPlayer.factionRole.role;
        }

        return RoleService.getOrCreateDeprecated(this, new MasonLeader());
    }

    protected static int UNSUBMITTED = Integer.MAX_VALUE;
    /**
     * Helper method for resolving night kills, which all happen at the same time
     *
     */

    public static final int[] KILL_ABILITIES = new int[] {
            Veteran.MAIN_ABILITY, SerialKiller.MAIN_ABILITY, Disguiser.MAIN_ABILITY, MassMurderer.MAIN_ABILITY,
            Joker.MAIN_ABILITY, Interceptor.MAIN_ABILITY, ElectroManiac.MAIN_ABILITY, Clubber.MAIN_ABILITY,
            Burn.MAIN_ABILITY, GunAbility.MAIN_ABILITY, FactionKill.MAIN_ABILITY
    };
    public GamePhase nightPhase = GamePhase.NIGHT_ACTION_SUBMISSION;

    private void kills() {
        nightPhase = GamePhase.KILLING;
        doNightAction(new PlayerList(), true, KILL_ABILITIES); // don't exclude anyone, and reverse the actions

        guards();

        // apparently I wrote in that bodyguards will also not be affected by being shot
        // during ' the killing phase '
        nightPhase = GamePhase.POST_KILLING;

        Jester.handleSuicides(this);

    }

    /**
     * Helper method for Bodyguards protecting each other
     *
     */
    private void guards() {
        PlayerList bgs = getRoles(Bodyguard.class);
        bgs.sortBySubmissionTime();

        boolean noOneCompleted = false;
        while (!noOneCompleted){
            noOneCompleted = true;
            for(Player bg: bgs){
                for(Action a: bg.getActions())
                    if(a.ability == Bodyguard.MAIN_ABILITY)
                        a.attemptCompletion();
                if(bg.didNightAction()){
                    bgs.remove(bg);
                    noOneCompleted = false;
                    break;
                }
            }
        }
    }

    /**
     * Helper method for doctors healing each other
     *
     */
    private void heals() {

        PlayerList healers = getRoles(Doctor.class).sortBySubmissionTime();
        // should garuntee that all the healers have targets

        PlayerList injured = new PlayerList();

        while (!healers.isEmpty()){
            Player healer = healers.getFirst();

            // if the healer isn't alive
            if(healer.getLives() < NORMAL_HEALTH){
                injured.add(healer);
                healers.remove(healer);
            }

            else{
                // healer does the heal
                doNightAction(getAllPlayers().remove(healer), false, Doctor.MAIN_ABILITY);
                healers.remove(healer);

                PlayerList healerTargets = healer.getActions().getTargets(Doctor.COMMAND);

                // injured only contains doctors
                for(Player healerTarget: healerTargets){
                    if(healerTarget.in(injured)){
                        injured.remove(healerTarget);
                        healers.add(healerTarget);
                    }
                }
            }
        }

        healsComplete = true;

    }

    // used by disguiser call, where charged disguisers shouldn't act
    public void doNightAction(PlayerList excluded, String... cs) {
        int[] actions = new int[cs.length];
        for(int i = 0; i < cs.length; i++){
            actions[i] = Ability.ParseAbility(cs[i]);
        }
        doNightAction(excluded, false, actions);
    }

    /**
     * Helper method for doing night actions concurrently for different roles.
     *
     * @param roles   the role names that are to do their night actions next
     * @param members that can't act (typically because this is a disguiser call,
     *                and disguisers can't act)
     */
    private void doNightAction(int... ability) {
        doNightAction(new PlayerList(), false, ability);
    }

    private void doNightAction(PlayerList excluded, final boolean reversed, int... ability) {
        ArrayList<Action> list = new ArrayList<>();
        for(Player p: _players){
            for(Action a: p.getActions()){
                if(a.is(ability) && !p.in(excluded)){
                    list.add(a);
                }
            }
        }

        Collections.sort(list, new Comparator<Action>() {
            @Override
            public int compare(Action a1, Action a2) {
                if(a1.owner == a2.owner){
                    if(reversed)
                        return -1;
                    return 1;
                }
                return a1.owner.getSubmissionTime() - a2.owner.getSubmissionTime();
            }
        });

        if(reversed)
            Collections.reverse(list);

        for(Action a: list){
            a.completeNightAction();
        }
    }

    /**
     * Helper method for agents, lookouts, and detectives doing their night action
     *
     */
    private void stalkings() {
        for(int i = 0; i < 2; i++){
            doNightAction(Lookout.MAIN_ABILITY, Detective.MAIN_ABILITY, Spy.MAIN_ABILITY, Agent.MAIN_ABILITY);
            pre_visiting_phase = false;
        }

    }

    /**
     * Helper method for looking for people that have been attacked and not been
     * saved and setting them dead. No one is there to hear your screams.
     *
     * @return the new Dead people
     */
    DeathDescriber deathDescriber = null;

    public void setDeathDescriber(DeathDescriber dd) {
        deathDescriber = dd;
    }

    /*
     * private PlayerList setDead(){ PlayerList newDead = new PlayerList();
     *
     * }
     */

    /**
     * Returns a list of players who all died on the given day
     *
     * @param dayNumber the day number to search by
     * @return the playerrs who died on that day
     */
    public PlayerList getDeadList(int dayNumber) {
        PlayerList deadList = new PlayerList();
        for(Player p: getDeadPlayers()){
            if(p.getDeathDay() == dayNumber)
                deadList.add(p);
        }
        return deadList;
    }

    /**
     * Returns the number of dead people
     *
     * @return number of dead people
     */
    public int getDeadSize() {
        return getDeadPlayers().size();
    }

    /**
     * Returns the number of living people
     *
     * @return number of living people
     */
    public int getLiveSize() {
        return getLivePlayers().size();
    }

    /**
     * Helper method for resetting the votes and notifiying listeners that the day
     * has started
     *
     * @param newDead
     */

    private boolean dayStartingUp = false;

    private void startDay(PlayerList newDead) {
        if(getInt(SetupModifier.DISCUSSION_LENGTH) == 0)
            phase = GamePhase.VOTE_PHASE;
        else
            phase = GamePhase.DISCUSSION_PHASE;

        if(!isInProgress())
            return;

        // for joker bounties
        dayStartingUp = true;

        voteSystem.reset();
        _lynches = 1;

        for(Faction t: _factions.values()){
            t.onDayStart();
        }

        for(Player p: _players){
            p.onDayStart();
        }
        // for joker bounties
        dayStartingUp = false;

        this.events.deactivateNightChats();
        this.events.grantArchitectAccess();
        this.events.restoreFactionChats();
        this.events.dayChat.setActive();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onDayPhaseStart(newDead);

        parityCheck();
        pushPendingAnnouncements();

    }

    public Faction parityReached() {
        for(Player p: getLivePlayers()){
            if(voteSystem.getVoteTarget(p) != skipper && p.stopsParity())
                return null;
        }

        int count, otherCount, votePower;
        for(Faction t: getFactions()){
            if(!t.hasAbility(FactionKill.MAIN_ABILITY))
                continue;
            count = 0;
            otherCount = 0;
            for(Player p: getLivePlayers()){
                if(p.isPuppeted()){
                    if(p.getPuppeteer().getFaction() == t){
                        count += p.getVotePower();
                    }else{
                        otherCount += p.getVotePower();
                    }
                }else{
                    if(p.isSilenced())
                        votePower = 1;
                    else
                        votePower = p.getVotePower();
                    if(p.getFaction() == t)
                        count += votePower;
                    else
                        otherCount += votePower;
                }
            }

            if(count == otherCount)
                return t;
        }
        return null;
    }

    public void forceEndNight() {
        PlayerList waitingFor = new PlayerList();
        for(Player waiting: _players){
            if((waiting.isAlive() || waiting.is(Ghost.class)) && !waiting.endedNight())
                waitingFor.add(waiting);
        }
        for(Player waiting: waitingFor){
            if(phase == GamePhase.NIGHT_ACTION_SUBMISSION && isInProgress())
                waiting.endNight(true); // forced end night
        }
    }

    /**
     * Checks if the voter and target are valid and if not, throws an exception
     *
     * @param voter  the Player voting
     * @param target the Player being voted
     */

    /**
     * Meat Vote method. Shouldn't be called directly.
     *
     * @param voter  the person doing the voting
     * @param target the person that is being voted
     */

    /**
     * Unvoting helper method that removes the target from the voter's vote and the
     * voter from the voter's of people who are voting the target
     *
     * @param voter the player who wants to unvote
     * @return the voter's previous vote
     */

    /**
     * Method that indicates the player wants to skip the vote. Should not be called
     * directly
     *
     * @param p       the player who wants to skip the vote
     * @param command
     */

    /**
     * Helper method to see if minimum vote to lynch has been reached. If so, the
     * night end of day auto starts
     *
     */
    public void checkVote() {
        voteSystem.checkVote();
    }

    public Object commandLock = new Object();

    /**
     * Adds a phrase 'said' by the player. Should be called from Player, not
     * accessed directly.
     *
     * @param p       the player talking
     * @param message what the player said
     * @param key     where this message is going
     */
    protected ChatMessage say(Player p, String message, String key) {
        if(p.isSilenced() || p.isPuppeted()){
            if(p.isDead() && key.equals(Constants.DEAD_CHAT)){

            }else{
                Player ventKey = getPlayerByName(key);
                if(ventKey == null || !p.getPuppets().contains(ventKey)){
                    if(p.isSilenced())
                        throw new ChatException("Blackmailed players cannot talk.");
                    throw new ChatException("Puppetted players cannot talk.");
                }
            }
        }

        if((p.isDead() && !p.is(Ghost.class)) && isInProgress() && !key.equals(Constants.DEAD_CHAT))
            throw new IllegalActionException("Dead players cannot talk.");
        if(isNight()){
            EventLog el = events.getEventLog(key);
            if(key.equals(VoidChat.KEY)){

            }else if(teamChatMessage(p, key)){

            }else if(p.isJailed() && key.startsWith(p.getName())){
                return ChatMessage.JailCaptiveMessage(p, message, key);
            }else if(jailorToCaptiveMessage(p, key)){
                return ChatMessage.JailCaptorMessage(p, message, key);
            }else if(el != null && el instanceof ArchitectChat && el.isActive() && el.getMembers().contains(p)){

            }else if(p.isDead() && key.equalsIgnoreCase(DeadChat.KEY)){

            }else
                throw new ChatException("Player cannot talk in this chat. [" + key + "]");
        }

        return new ChatMessage(p, message, key);
    }

    private boolean teamChatMessage(Player p, String key) {
        Faction t = getFaction(key);
        if(t == null)
            return false;
        if(Mason.IsMasonType(p))
            return true;
        if(Disguiser.hasDisguised(p) && Mason.IsMasonType(Disguiser.getDisguised(p)))
            return true;
        if(!t.hasNightChat())
            return false;
        return t.getMembers().contains(p);
    }

    private boolean jailorToCaptiveMessage(Player jailor, String key) {
        if(!jailor.is(Jailor.class))
            return false;
        Jailor jailorCard = jailor.getAbility(Jailor.class);
        for(Player p: jailorCard.jailedTargets){
            if(key.equals(Jailor.KeyCreator(p, dayNumber)))
                return true;
        }
        return false;
    }

    public void addDayLynches(int lynches) {
        this._lynches += lynches;
    }

    public PlayerList getTodaysLynchedTargets() {
        PlayerList todaysLynched = new PlayerList();
        for(Player p: _players){
            if(p.isDead() && p.getDeathDay() == dayNumber && p.getDeathType().isLynch())
                todaysLynched.add(p);
        }
        if(todaysLynched.isEmpty() && getLiveSize() > 2)
            todaysLynched.add(skipper);
        return todaysLynched;
    }

    public static final boolean DAY_ENDING = true;
    public static final boolean DAY_NOT_ENDING = false;

    /**
     * Helper method that auto starts the night. Announces who got lynched.
     *
     * @param lynchedTargets the people who were lynched
     */
    public int _lynches = 0; // rename to remainingVoteRounds

    public void endDay(Faction parity) {
        masonLeaderPromotion();
        if(phase == GamePhase.NIGHT_ACTION_SUBMISSION)
            throw new IllegalStateException("It was already nighttime");

        PlayerList lynchedTargets = getTodaysLynchedTargets();

        EventList el = new EventList();

        DeathAnnouncement e;
        PlayerList poisoned = new PlayerList();
        for(Player p: _players){
            // happens before the setting dead of players because of vent onDayEnd
            p.onDayEnd();
        }

        for(Player p: _players.getLivePlayers()){
            if(!p.isPoisoned())
                continue;
            if(!p.in(lynchedTargets)){
                e = new DeathAnnouncement(p);
                e.setDayEndDeath();
                e.add(p, Poisoner.BROADCAST_MESSAGE);
                e.setPicture("poisoner");
                el.add(e);
                poisoned.add(p);
                p.dayKill(new DirectAttack(p.getPoisoner(), p, Constants.POISON_KILL_FLAG), DAY_ENDING);
            }
            p.sendMessage(new Feedback(p, Poisoner.DEATH_FEEDBACK));

        }

        for(Message m: voteSystem.publicizeVoteEnding(parity))
            el.add(m);

        isTransitioningToNight = true; // so far only used for marshall quick reveals
        for(Message x: el)
            announcement((Announcement) x);

        isTransitioningToNight = false;

        // cultCheck();
        checkProgress();
        if(isInProgress()){
            // jail targets for now
            resolveDayTargets();
            startNight(lynchedTargets.remove(skipper), poisoned, el);
        }

    }

    public boolean isTransitioningToNight = false;

    private void resolveDayTargets() {
        // remove fake bread first
        for(Player p: _players)
            p.resolveFakeBreadActions();

        for(Action a: actionStack){
            a.dayHappening();
        }

        Collections.sort(actionStack, EndDayResolveSorter);

        PlayerList immuneJailors = new PlayerList();
        Action a;
        Ability dayCard;
        for(int i = 0; i < actionStack.size(); i++){
            a = actionStack.get(i);

            dayCard = a.owner.getAbility(a.ability);
            if(a.owner.isDead() || a.getTargets().hasDead()){
                a.owner.getActions().remove(a);// .forceCancelDayAction(a);
                actionStack.remove(a);
                i--;
                continue;
            }else if(a.getTarget().in(immuneJailors)){
                a.owner.getActions().remove(a);// .forceCancelDayAction(a);
                actionStack.remove(a);
                i--;
                continue;
            }else if(a.owner.isJailed()){
                a.owner.getActions().remove(a);// .forceCancelDayAction(a);
                actionStack.remove(a);
                i--;
                continue;
            }else if(a.getTarget().isJailed()){
                a.owner.getActions().remove(a);// .forceCancelDayAction(a);
                actionStack.remove(a);
                i--;
                continue;
            }

            boolean broken = false;
            for(Player p: a.getTargets()){
                if(p.inArchChat()){
                    broken = true;
                    break;
                }
            }
            if(broken){
                a.owner.getActions().remove(a);// .forceCancelDayAction(a);
                actionStack.remove(a);
                i--;
                continue;
            }

            if(!a.owner.in(immuneJailors) && a.owner.is(Jailor.class))
                immuneJailors.add(a.owner);
            dayCard.resolveDayAction(a);
        }
    }

    public static final Comparator<Action> EndDayResolveSorter = new Comparator<Action>() {
        @Override
        public int compare(Action a1, Action a2) {
            if(a1.owner.is(Architect.class) && !a2.owner.is(Architect.class))
                return 1;
            if(a2.owner.is(Architect.class) && !a1.owner.is(Architect.class))
                return -1;
            return 0;
        }
    };

    private List<NarratorListener> listeners = new ArrayList<>();

    /**
     * Adds objects as listeners to the narrator. For example, when a vote happens
     *
     * @param nL the listener that wants to respond to narrator happenings
     */
    public Narrator addListener(NarratorListener nL) {
        if(listeners.contains(nL))
            throw new NarratorException("Cannot have the same listener listening twice.");
        listeners.add(nL);
        return this;
    }

    /**
     * Adds object as a command listener. Whenever someone inputs a command to the
     * game, this fires.
     *
     * @param cL the listener that wants to respond to narrator inputs
     */
    public void addListener(CommandListener cL) {
        synchronized (commandLock){
            cListeners.add(cL);
        }
    }

    /**
     * Removes the object that was listening to the narrator
     *
     * @param nL the object to be removed
     */
    public void removeListener(NarratorListener nL) {
        listeners.remove(nL);
    }

    public void clearListeners() {
        listeners.clear();
    }

    /**
     * Removes the object that was listening to narrator inputs
     *
     * @param cL the object to be removed
     */
    public void removeListener(CommandListener cL) {
        synchronized (commandLock){
            cListeners.remove(cL);
        }
    }

    /**
     * Returns all the objects that are listening to this Narrator
     *
     * @return the list of listening narrator objects
     */
    public List<NarratorListener> getListeners() {
        return new LinkedList<>(listeners);
    }

    /**
     * Determines whether the game has ended or not. Returns false if the game
     * hasn't even started
     *
     * @return whether the game is still going or not
     */
    public boolean isInProgress() {
        synchronized (progSync){
            if(!isStarted)
                return false;

            return win == null;
        }
    }

    private Object progSync = new Object();

    /**
     * Does the checks on whether the game is in progress or not. These checks
     * include if its day/night and there are no opposing teams, or if there are not
     * live players anymore. Once the game is over, winString is set, so
     * isInProgress knows the game is not in progres anymore
     *
     */
    public synchronized void checkProgress() {
        synchronized (progSync){

            for(Faction t: _factions.values()){
                if(!t.isAlive())
                    continue;

                for(String enemyKey: t.getEnemyColors()){
                    Faction enemyTeam = _factions.get(enemyKey);
                    if(enemyTeam == t && t.size() == 1)
                        continue;
                    if(enemyTeam.isAlive()){
                        if(phase != GamePhase.NIGHT_ACTION_SUBMISSION || getLivePlayers().size() >= 3)
                            return;
                    }
                }
            }
            if(isDay()){
                events.initializeNightLogs();
            }
            events.getNightLog(VoidChat.KEY).setInactive();
            events.getDeadChat().setInactive();

            events.getDayChat().setActive();
            determineWinners();

            phase = GamePhase.FINISHED;

            for(Player p: getLivePlayers())
                p.onDayStart();

            for(Player p: _players)
                p.addChat(events.dayChat);

            for(int i = 0; i < listeners.size(); i++)
                listeners.get(i).onGameEnd();

        }// end block for synchronized
    }

    /**
     * Helper method to determine the winners of the game.
     *
     */
    private void determineWinners() {
        PlayerList winningPlayers = new PlayerList();

        for(Player p: _players.copy().winDetermingSort()){
            p.determineWin();
            p.getActions().clear();
            if(p.isWinner()){
                winningPlayers.add(p);
            }
        }

        win = winMessage(winningPlayers, this);
    }

    private Message win;

    /**
     * This method returns null if the game isn't over yet. If it is, it'll return
     * the message of who won, or "Nobody won! If no one won."
     *
     * @return the Event message listing who won the game.
     */
    public Message getWinMessage() {
        if(win == null)
            checkProgress();
        return win;
    }

    /**
     * Event String helper method that puts together who the winners are in the game
     *
     * @param winners the winners of the game
     * @return the Event message
     */
    public static Message winMessage(PlayerList winners, Narrator n) {

        n.win = new OGIMessage(n);

        if(winners.size() == 0){
            Message e = n.win = new Announcement(n);
            e.add(Constants.NO_WINNER);
            return e;
        }
        winners.sortByTeam();
        Message e = n.win = new Announcement(n);
        for(int i = 0; i < winners.size() - 1; i++){
            e.add(winners.get(i));
            e.add(", ");
        }

        if(winners.size() != 1)
            e.add("and ");

        e.add(winners.get(winners.size() - 1));
        if(winners.size() > 1){
            e.add(" have ");
        }else
            e.add(" has ");
        e.add("won!");

        return e;
    }

    public void changeRole(Player p, FactionRole m) {
        Faction t = getFaction(p.getColor());
        t.removeMember(p);

        p.setRole(m);
        t = getFaction(m.getColor());
        t.addMember(p);
        ;

        /*
         * command player name color role name roles
         */
        String[] commandParts = new String[4];
        commandParts[0] = CommandHandler.MOD_CHANGE_ROLE;
        commandParts[1] = p.getName();
        commandParts[2] = m.getColor();
        commandParts[3] = m.role.getName();

        SelectionMessage e = new SelectionMessage(skipper, false);

        events.addCommand(skipper, commandParts);
        e.add(p.getID(), " has changed into a ", new HTString(m.role.getName(), t), ".");
    }

    /**
     * Kills a player, out of game style
     *
     * @param players
     */
    public void modkill(PlayerList players) {
        // dayTime modkill
        DeathAnnouncement da = new DeathAnnouncement(players, this, phase != GamePhase.NIGHT_ACTION_SUBMISSION);
        String[] command = new String[players.size() + 1];
        command[0] = CommandHandler.MODKILL;
        for(int i = 0; i < players.size(); i++)
            command[i + 1] = players.get(i).getName();
        getEventManager().addCommand(command);
        da.add(players, " was modkilled.");
        da.setPicture("electromaniac");

        if(phase != GamePhase.NIGHT_ACTION_SUBMISSION){
            voteSystem.removeFromVotes(players);
            for(Player player: players)
                player.modKillHelper();
            checkProgress();
            if(this.isInProgress())
                checkVote();
        }else{
            for(Player player: players)
                player.modKillHelper();
            if(everyoneCompletedNightActions())
                endNight();
        }
        announcement(da);

        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onModKill(players);
    }

    /**
     * Returns whether the game started or not
     *
     * @return whether the game is going
     */
    public boolean isStarted() {
        return isStarted;
    }

    public boolean isFinished() {
        return this.phase == GamePhase.FINISHED;
    }

    public boolean firstPhaseStarted() {
        return firstPhaseStarted;
    }

    public ArrayList<Action> actionStack;

    public void addActionStack(Action a) {
        if(!actionStack.contains(a))
            actionStack.add(a);
    }

    public void removeActionStack(Action a) {
        actionStack.remove(a);
    }

    @Override
    public String toString() {
        if(phase == GamePhase.VOTE_PHASE)
            return "Day " + dayNumber;
        return "Night " + dayNumber;
    }

    private HashMap<String, String> aliasMap;

    public String getAlias(String key) {
        return aliasMap.get(key);
    }

    public void setAlias(String key, String alias) {
        aliasMap.put(key, alias);
    }

    public Set<String> getAliases() {
        return aliasMap.keySet();
    }

    public void parityCheck() {
        Faction parity = parityReached();
        if(parity != null){
            endDay(parity);
        }
    }

    ArrayList<String> remIcons;

    public String getIcon() {
        if(remIcons.isEmpty()){
            for(String s: ICONS)
                remIcons.add(s);
            Collections.shuffle(remIcons);
        }
        return remIcons.remove(0);
    }

    public static final String[] ICONS = new String[] {
            "fas fa-ambulance", "fab fa-android", "fas fa-balance-scale", "fas fa-band-aid", "fab fa-bitcoin",
            "fas fa-bug", "fas fa-bus", "fas fa-chess-knight", "fas fa-beer", "fas fa-glass-martini", "fas fa-crow",
            "fas fa-eye", "fas fa-fighter-jet", "fas fa-fire", "fas fa-gamepad", "fab fa-gitkraken", "fab fa-grunt",
            "fas fa-motorcycle", "fas fa-paw", "fas fa-pills", "fas fa-piggy-bank", "fas fa-poo", "fab fa-qq",
            "fab fa-react", "fas fa-robot", "fas fa-rocket", "fas fa-snowflake", "fas fa-chess-rook",
            "fas fa-chess-pawn", "fas fa-binoculars"
    };
}
