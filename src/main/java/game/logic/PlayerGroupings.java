package game.logic;

import java.util.ArrayList;

public class PlayerGroupings {

    private ArrayList<PlayerList> groupings = new ArrayList<>();

    public void add(PlayerList pl) {
        groupings.add(pl);
    }

    public int indexOf(Player pi) {
        for(int i = 0; i < groupings.size(); i++){
            if(pi.in(groupings.get(i)))
                return i;
        }
        return -1;
    }

}
