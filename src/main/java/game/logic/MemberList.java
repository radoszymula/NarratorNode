package game.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import game.logic.exceptions.UnknownRoleException;
import game.roles.Ability;
import game.roles.AbilityList;
import game.roles.support.Suit;
import models.FactionRole;
import util.LookupUtil;

public class MemberList implements Iterable<FactionRole> {

    ArrayList<FactionRole> mList;
    HashMap<String, ArrayList<FactionRole>> otherColors;
    HashMap<Class<? extends Ability>, ArrayList<FactionRole>> otherColors_class;
    HashMap<String, ArrayList<FactionRole>> teamComp;
    HashMap<String, AbilityList> roleBaseDict;

    public MemberList() {
        mList = new ArrayList<>();
        otherColors = new HashMap<>();
        otherColors_class = new HashMap<>();
        teamComp = otherColors;
        roleBaseDict = new HashMap<>();
    }

    public MemberList(Collection<FactionRole> roleList) {
        this();
        for(FactionRole m: roleList)
            this.add(m);
    }

    @Override
    public Iterator<FactionRole> iterator() {
        return mList.iterator();
    }

    public static final Comparator<? super FactionRole> NameComparator = new Comparator<FactionRole>() {
        @Override
        public int compare(FactionRole role1, FactionRole role2) {
            return role1.role.getName().compareTo(role2.role.getName());
        }

    };

    public void add(FactionRole m) {
        if(mList.contains(m)){
            return;
        }
        mList.add(m);
        mList.sort(NameComparator);
        roleBaseDict.put(m.role.getName(), m.role.getBaseAbility());
        String key = getKey(m.role.getBaseAbility());
        String color = m.getColor();
        if(!otherColors.containsKey(key))
            otherColors.put(key, new ArrayList<>());
        otherColors.get(key).add(m);

        for(Ability a: m.role.getBaseAbility()){
            if(!otherColors_class.containsKey(a.getClass()))
                otherColors_class.put(a.getClass(), new ArrayList<>());
            otherColors_class.get(a.getClass()).add(m);
        }

        if(!teamComp.containsKey(color))
            teamComp.put(color, new ArrayList<>());
        teamComp.get(color).add(m);
    }

    public AbilityList translate(String roleName) {
        return roleBaseDict.get(roleName);
    }

    public boolean hasRole(String simpleName) {
        return otherColors.containsKey(simpleName);
    }

    public boolean hasRole(Class<? extends Ability> role) {

        return otherColors_class.containsKey(role);
    }

    public ArrayList<Faction> getFactions(Class<? extends Ability> role, Narrator narrator) {
        ArrayList<Faction> teams = new ArrayList<>();
        ArrayList<FactionRole> otherMembers = otherColors_class.get(role);
        if(otherMembers == null)
            otherMembers = new ArrayList<>();

        for(FactionRole m: otherMembers){
            teams.add(narrator.getFaction(m.getColor()));
        }

        return teams;
    }

    public ArrayList<Faction> getFactions(AbilityList a, Narrator narrator) {
        ArrayList<Faction> teams = new ArrayList<>();
        ArrayList<FactionRole> otherMembers = otherColors.get(getKey(a));
        if(otherMembers == null)
            otherMembers = new ArrayList<>();

        for(FactionRole m: otherMembers)
            teams.add(narrator.getFaction(m.getColor()));

        return teams;
    }

    public ArrayList<FactionRole> getOtherColors(Ability a) {
        return getOtherColors(new AbilityList(a));
    }

    public static String getKey(AbilityList al) {
        return al.getDatabaseName();
    }

    public ArrayList<FactionRole> getOtherColors(AbilityList role) {
        ArrayList<FactionRole> ret = otherColors.get(getKey(role));
        if(ret == null)
            ret = new ArrayList<>();
        return ret;
    }

    public List<FactionRole> getFactionRoles(String color) {
        ArrayList<FactionRole> ret = teamComp.get(color);
        if(ret == null)
            ret = new ArrayList<>();
        return ret;
    }

    public boolean contains(AbilityList a) {
        return otherColors.containsKey(getKey(a));
    }

    public boolean contains(Class<? extends Ability> abilityClass) {
        for(FactionRole factionRole: this){
            if(factionRole.role.contains(abilityClass))
                return true;
        }
        return false;
    }

    public FactionRole get(Suit suit) {
        for(FactionRole m: getFactionRoles(suit.color)){
            if(m.role.getName().equals(suit.roleName))
                return m;
        }
        throw new UnknownRoleException(suit.toString());
    }

    public MemberList copy() {
        MemberList mList = new MemberList();
        for(FactionRole m: this){
            mList.add(m);
        }
        return mList;
    }

    public String getName(Ability a, String colorOfMember) {
        return getName(new AbilityList(a), colorOfMember);
    }

    public String getName(AbilityList role, String colorOfMember) {
        ArrayList<FactionRole> alternateRoles = getOtherColors(role);
        for(FactionRole m: alternateRoles){
            if(m.getColor().equals(colorOfMember))
                return m.role.getName();
        }
        return role.getClass().getSimpleName();
    }

    public boolean abilityExists(Narrator narrator, Class<? extends Ability> ability) {
        return !LookupUtil.findRolesWithAbility(narrator, ability).isEmpty();
    }

    public ArrayList<FactionRole> getMemberWithAbilities(Class<? extends Ability> class1) {
        ArrayList<FactionRole> hasAbilities = new ArrayList<>();
        for(FactionRole m: mList){
            if(m.role.contains(class1))
                hasAbilities.add(m);
        }
        return hasAbilities;
    }

    public boolean hasMembersThatAffectSending(Narrator narrator) {
        for(Role m: narrator.roles){
            if(m.hasMembersThatAffectSending())
                return true;
        }
        return false;
    }

    public boolean allowsForFriendlyFire(Narrator narrator) {
        for(Role m: narrator.roles)
            if(m.allowsForFriendlyFire())
                return true;
        return false;
    }

}
