package game.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.event.Message;
import game.event.OGIMessage;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.TeamSettingsException;
import game.logic.exceptions.UnknownTeamException;
import game.logic.support.Alignment;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.action.Action;
import game.logic.support.condorcet.Condorcet;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.AbilityList;
import game.roles.CultLeader;
import game.roles.Disguiser;
import game.roles.FactionKill;
import game.roles.FactionSend;
import game.roles.Goon;
import models.FactionRole;
import models.Modifiers;
import services.FactionModifierService;
import services.FactionService;

public class Faction implements Alignment, FactionGroup {

    public static final int FACTION_INT_MAX = 127;

    private String name = " ";
    private String factionColor;
    private String description;
    private Set<String> enemyColors = new HashSet<>();
    private Set<String> sheriffDetectables = new HashSet<>();

    public Map<Role, FactionRole> _roleSet = new HashMap<>();

    public AbilityList _abilities;
    public Modifiers<FactionModifier> _modifierMap;

    public Narrator narrator;

    public int _startingSize = 0; // this is used for telling ghosts what team
    public long id;

    private AbilityList attainedAbilities;

    PlayerList infiltrators;

    public Faction(Narrator narrator, String color, String name, String description) {
        this.narrator = narrator;
        this.factionColor = color;
        this.name = name;
        this.description = description;
        _modifierMap = new Modifiers<>();
        this.setRule(FactionModifier.HAS_NIGHT_CHAT, false);
        this.setRule(FactionModifier.IS_RECRUITABLE, true);
        this.setRule(FactionModifier.KNOWS_ALLIES, false);
        this.setRule(FactionModifier.LAST_WILL, false);
        this.setRule(FactionModifier.LIVE_TO_WIN, false);
        this.setRule(FactionModifier.PUNCH_SUCCESS, 0);
        this.setRule(FactionModifier.WIN_PRIORITY, 0);
    }

    public String getName() {
        return name;
    }

    public static final String[] RESERVED_NAMES = {
            "Random", "Randoms", "Neutral", "Neutrals"
    };

    public Faction setName(String name) {
        if(narrator.isStarted())
            throw new TeamSettingsException("Cannot change name after game has started");
        FactionService.nameValidation(narrator, name, this);
        this.name = name;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        if(description == null)
            return "";
        return description;
    }

    public ArrayList<String> getRules() {
        if(narrator.isStarted())
            return getRules(this);
        ArrayList<String> list = new ArrayList<>();

        for(FactionModifier f_rule: FactionModifier.values())
            list.add(this.getColor() + f_rule);
        return list;
    }

    public static ArrayList<String> getRules(Faction faction) {
        Narrator narrator = faction.narrator;
        ArrayList<String> teamRules = new ArrayList<>();

        if(faction.knowsTeam()){
            teamRules.add(FactionModifier.KNOWS_ALLIES.getActiveFormat());

            if(faction.hasNightChat())
                teamRules.add(FactionModifier.HAS_NIGHT_CHAT.getActiveFormat());
            else
                teamRules.add(FactionModifier.HAS_NIGHT_CHAT.getActiveFormat());
        }else
            teamRules.add(FactionModifier.KNOWS_ALLIES.getInactiveFormat());

        if(faction.hasLastWill())
            teamRules.add(FactionModifier.LAST_WILL.getActiveFormat());

        if(faction.getAliveToWin())
            teamRules.add(FactionModifier.LIVE_TO_WIN.getActiveFormat());
        else
            teamRules.add(FactionModifier.LIVE_TO_WIN.getInactiveFormat());

        if(narrator.getBool(SetupModifier.PUNCH_ALLOWED)){
            int success = faction.getPunchSuccess();
            teamRules.add(faction.getName() + " members have a " + success + "% chance to punch out their target.");
        }

        if(narrator.getPossibleMembers().abilityExists(narrator, CultLeader.class)){
            if(faction.canRecruitFrom())
                teamRules.add(FactionModifier.IS_RECRUITABLE.getActiveFormat());
            else
                teamRules.add(FactionModifier.IS_RECRUITABLE.getInactiveFormat());
        }

        return teamRules;
    }

    public boolean winsOver(Faction t) {
        if(this.getPriority() == t.getPriority())
            return isEnemy(this) == t.isEnemy(t);

        // if my priority
        return this.getPriority() > t.getPriority();
    }

    @Override
    public void setPriority(int i) {
        this._modifierMap.add(FactionModifier.WIN_PRIORITY, i);
    }

    public int getPriority() {
        return this._modifierMap.getInt(FactionModifier.WIN_PRIORITY);
    }

    public String getColor() {
        return factionColor;
    }

    public void beforeGameStart() {

    }

    public void onGameStart() {
        _startingSize = members.size();
        if(_abilities != null){
            if(narrator.getPossibleMembers().hasMembersThatAffectSending(narrator))
                _abilities = new AbilityList(_abilities, new FactionSend());
            for(Ability a: _abilities)
                a.initialize(this);
        }
    }

    public Set<FactionRole> getAllMembers() {
        return new HashSet<>(_roleSet.values());
    }

    public boolean has(Class<? extends Ability> abilityClass) {
        for(Role role: _roleSet.keySet()){
            if(role.hasAbility(abilityClass))
                return true;
        }
        return false;
    }

    public boolean has(String roleName) { // actualName
        for(FactionRole m: _roleSet.values())
            if(m.role.getName().equals(roleName))
                return true;
        return false;
    }

    public void setKillEnabled() {
        addAbility(new FactionKill());
    }

    public void setKillDisabled() {
        _abilities.remove(FactionKill.class);
    }

    public void setKill(boolean b) {
        // if team can kill, they need to know their teammates
        if(b)
            setKillEnabled();
        else
            setKillDisabled();
    }

    public boolean hasSharedAbilities() {
        return _abilities != null || attainedAbilities != null;
    }

    public boolean hasAbility(int num) {
        return getAbility(num) != null;
    }

    public Ability getAbility(int num) {
        if(_abilities != null)
            for(Ability a: _abilities)
                if(a.getAbilityNumber() == num)
                    return a;
        if(attainedAbilities != null)
            for(Ability a: attainedAbilities)
                if(a.getAbilityNumber() == num)
                    return a;
        return null;
    }

    public void modifyAbility(AbilityModifier modifier, Class<? extends Ability> abilityClass, int val) {
        if(modifier == null)
            throw new IllegalGameSettingsException();
        Modifiers<AbilityModifier> modifiers = this._abilities.getAbility(abilityClass).modifiers;
        modifiers.add(modifier, val);
    }

    public void modifyAbility(AbilityModifier modifier, Class<? extends Ability> abilityClass, boolean val) {
    	Ability ability = this._abilities.getAbility(abilityClass);
    	if(ability == null)
    		throw new NarratorException("This faction doesn't have that ability");
        Modifiers<AbilityModifier> modifiers = this._abilities.getAbility(abilityClass).modifiers;
        modifiers.add(modifier, val);
    }

    public void addAttainedAbility(Ability a) {
        if(attainedAbilities == null && _abilities == null)
            attainedAbilities = new AbilityList(a, new FactionSend());
        else if(attainedAbilities == null)
            attainedAbilities = new AbilityList(a);
        else
            attainedAbilities = new AbilityList(attainedAbilities, a);
        if(sender == null)
            sender = new HashMap<>();
    }

    public void addAbility(Ability a) {
        if(narrator.isStarted())
            throw new IllegalActionException("Cannot modify team events after game has started");

        if(a instanceof FactionSend)
            throw new IllegalGameSettingsException("Cannot add this ability.");

        if(!a.supportedFactionAbility()){
            throw new IllegalActionException("This ability cannot be shared");
        }

        // if team has shared ability, they need to know their teammates
        this.setKnowsTeam(true);
        if(_abilities == null){
            _abilities = new AbilityList(a);
            sender = new HashMap<>();
        }else
            _abilities = new AbilityList(_abilities, a);
    }

    public void removeAbility(Class<? extends Ability> c) {
        if(_abilities == null)
            return;
        if(c.equals(FactionSend.class))
            throw new IllegalActionException();
        if(_abilities.contains(c)){
            if(_abilities.size() == 1){
                _abilities = null;
                sender = null;
            }else
                _abilities = _abilities.remove(c);
        }
    }

    public String getNightPrompt(Player p) {
        StringBuilder sb = new StringBuilder();
        for(Ability a: _abilities)
            sb.append(a.getNightText(p));
        return sb.toString();
    }

    public boolean getAliveToWin() {
        return this._modifierMap.getBoolean(FactionModifier.LIVE_TO_WIN);
    }

    public void setMustBeAliveToWin(boolean b) {
        this._modifierMap.add(FactionModifier.LIVE_TO_WIN, b);
    }

    protected void addEnemy(Faction enemyTeam) {
        if(!enemyColors.contains(enemyTeam.factionColor))
            enemyColors.add(enemyTeam.factionColor);

        if(!enemyTeam.enemyColors.contains(factionColor))
            enemyTeam.enemyColors.add(factionColor);
    }

    public void addEnemy(String enemyColor) {
        addEnemy(narrator.getFaction(enemyColor));
    }

    public void addAlly(Faction allyTeam) {
        if(enemyColors.contains(allyTeam.factionColor))
            enemyColors.remove(allyTeam.factionColor);

        if(allyTeam.enemyColors.contains(factionColor))
            allyTeam.enemyColors.remove(factionColor);
    }

    public Set<String> getEnemyColors() {
        return enemyColors;
    }

    public Set<Faction> getEnemyFactions() {
        Set<Faction> enemyFactions = new HashSet<>();
        for(String enemyFactionColor: enemyColors)
            enemyFactions.add(narrator.getFaction(enemyFactionColor));
        return enemyFactions;
    }

    public boolean hasEnemies() {
        return !enemyColors.isEmpty();
    }

    public boolean isEnemy(String team) {
        return enemyColors.contains(team);
    }

    public boolean isEnemy(Faction t) {
        return isEnemy(t.getColor());
    }

    public boolean sheriffDetects(String enemyTeam) {
        return sheriffDetectables.contains(enemyTeam);
    }

    public void addSheriffDetectableTeam(Faction... teams) {
        for(Faction team: teams)
            addSheriffDetectableTeam(team.getColor());
    }

    public void addSheriffDetectableTeam(String... colors) {
        for(String color: colors){
            if(narrator.getFaction(color) == null)
                throw new UnknownTeamException(color);
            sheriffDetectables.add(color);
        }
    }

    public void removeSheriffDetectableTeam(String color) {
        sheriffDetectables.remove(color);
    }

    public void removeSheriffDetectableTeam(Faction t) {
        removeSheriffDetectableTeam(t.getColor());
    }

    public boolean sheriffDetectsAll(String[] teams) {
        for(String team: teams)
            if(!sheriffDetectables.contains(team))
                return false;
        return true;
    }

    public Set<String> getSheriffDetectables() {
        return sheriffDetectables;
    }

    private PlayerList members = new PlayerList();

    public void addMember(Player player) {
        members.add(player);
    }

    public void removeMember(Player p) {
        members.remove(p);
    }

    public PlayerList getMembers() {
        return members.copy();
    }

    public int size() {
        return members.size();
    }

    public boolean isAlive() {
        if(members.isEmpty())
            return false;
        for(Player p: members){
            if(p.isAlive() && p.getFaction() == this)
                return true;
        }
        return false;
    }

    public void reset() {
        members.clear();
        sender = null;
        if(this._abilities != null){
            ArrayList<Ability> pastAbilities = new ArrayList<>();
            for(Ability a: _abilities){
                if(!(a instanceof FactionSend))
                    pastAbilities.add(a.newCopy());
            }
            this._abilities = null;
            for(Ability a: pastAbilities)
                this.addAbility(a.newCopy());
        }
        if(infiltrators != null)
            infiltrators.clear();
        this.attainedAbilities = null;
    }

    // players that put in their night action kill

    public static final int SEND_ = Ability.NIGHT_SEND;
    public static final Faction NOT_SUSPICIOUS = null;

    public static final String SEND = "Send";

    private HashMap<Integer, Player> sender;

    public Player determineNightActionController(int ability) {
        Player newSender = null;
        if(!members.getLivePlayers().isEmpty() && hasAbility(ability)){
            newSender = getCurrentController(ability, true); // night IS ending
            if(newSender != null){
                sender.put(ability, newSender);
                newSender.addTempAbility(this.getAbility(ability));
            }else
                sender.remove(ability);
        }
        return newSender;
    }

    public Player getFactionAbilityController(int abilityNumber) {
        return sender.get(abilityNumber);
    }

    public void overrideSenderViaWitch(Player victim, int abilityNumber) {
        if(!sender.containsKey(abilityNumber)){
            sender.put(abilityNumber, victim);
            victim.addTempAbility(getAbility(abilityNumber));
        }

    }

    public Player getCurrentController(int ability) {
        return getCurrentController(ability, false);// night isn't ending
    }

    protected Player getCurrentController(int ability, boolean nightEnding) {
        if(_abilities == null && this.attainedAbilities == null)
            return null;

        if(members.size() == 1)
            return members.get(0);
        PlayerList members = this.members.filterSubmittingAction(ability);
        if(members.isEmpty())
            return null;
        if(members.size() == 1)
            return members.get(0);

        List<List<Set<Player>>> sendRankings = FactionSend.CondorcetOrdering(this.members);
        List<Set<Player>> order = Condorcet.getOrderings(members.toSet(), sendRankings);
        if(order.isEmpty())
            return null;
        Set<Player> topChoices = order.get(0);
        if(topChoices.size() == 1)
            return topChoices.iterator().next();
        Set<Player> goons = getGoons(topChoices);
        if(goons.size() == 1)
            return goons.iterator().next();
        if(nightEnding){
            if(!goons.isEmpty())
                topChoices = goons;
            topChoices = maxActionSubmission(topChoices);
            return new PlayerList(topChoices).sortBySubmissionTime().getFirst();
        }

        return null;
    }

    private Set<Player> getGoons(Set<Player> list) {
        Set<Player> pl = new HashSet<Player>();
        for(Player p: list){
            if(p.is(Goon.class)){
                pl.add(p);
                continue;
            }
            if(!Disguiser.hasDisguised(p)){
                continue;
            }
            if(Disguiser.getDisguised(p).hasAbility(Goon.class))
                pl.add(p);
        }
        return pl;
    }

    private Set<Player> maxActionSubmission(Set<Player> pl) {
        int maxCount = 0;
        Set<Player> ret = new HashSet<Player>();
        int count;
        for(Player p: pl){
            count = 0;
            for(Action a: p.getActions()){
                if(a.is(getAbilities()))
                    count++;
            }
            if(maxCount > count)
                continue;
            if(maxCount < count){
                ret.clear();
                maxCount = count;
            }
            ret.add(p);
        }
        return ret;
    }

    @Override
    public boolean opposes(Alignment a2, Narrator n) {
        for(String teamNumb: a2.getFactionColors()){
            if(!(enemyColors.contains(teamNumb)))
                return false;
        }
        return true;

    }

    @Override
    public String[] getFactionColors() {
        return new String[] {
                factionColor
        };
    }

    public boolean isPlayer() {
        return false;
    }
    

    public AbilityList getAbilities() {
        if(_abilities == null && attainedAbilities == null)
            return null;
        else if(_abilities == null)
            return attainedAbilities;
        else if(attainedAbilities == null)
            return _abilities;
        return new AbilityList(_abilities, attainedAbilities);
    }

    @Override
    public String toString() {
        return name;
    }

    public void sendNightTextPrompt(Player player) {
        String message = getNightPrompt(player);
        if(message != null)
            new OGIMessage(player, message);
    }

    public boolean hasMember(Player p) {
        return members.contains(p);
    }

    public boolean isSolo() {
        return members.size() == 1;
    }

    public void setEnemies(Faction... enemyTeams) {
        for(Faction enemy: enemyTeams)
            addEnemy(enemy);
    }

    public void setAllies(Faction... enemyTeams) {
        for(Faction enemy: enemyTeams)
            addAlly(enemy);
    }

    public PlayerList getTargets() {
        PlayerList ret = new PlayerList();
        for(Player p: members.copy().sortByID()){
            if(p.getFaction() == this){
                for(Player target: p.getVisits("team visits")){
                    if(!target.in(ret))
                        ret.add(target);
                }
            }
        }

        return ret;
    }

    public String[] getKillFlag() {
        if(narrator.getBool(SetupModifier.DIFFERENTIATED_FACTION_KILLS)){
            return new String[] {
                    Constants.FACTION_KILL_FLAG[0] + getName(), Constants.FACTION_KILL_FLAG[1] + getName()
            };
        }
        String name = "faction";
        return new String[] {
                Constants.FACTION_KILL_FLAG[0] + name, Constants.FACTION_KILL_FLAG[1] + name
        };
    }

    public void clearSheriffDetectables() {
        sheriffDetectables.clear();
    }

    public static boolean isIntRule(FactionModifier f) {
        return f == FactionModifier.WIN_PRIORITY || f == FactionModifier.PUNCH_SUCCESS;
    }

    public int setRule(FactionModifier fRule, int val) {
    	return FactionModifierService.modify(this, fRule, val);
    }

    public boolean setRule(FactionModifier fRule, boolean val) {
    	return FactionModifierService.modify(this, fRule, val);
    }

    public void setRule(Rule r) {
        if(r.isBool())
            setRule((FactionModifier) r.id, r.getBoolVal());
        else
            setRule((FactionModifier) r.id, r.getIntVal());
    }

    public boolean canRecruitFrom() {
        return this._modifierMap.getOrDefault(FactionModifier.IS_RECRUITABLE, true);
    }

    public Faction setCanRecruitFrom(boolean b) {
    	FactionModifierService.modify(this, FactionModifier.IS_RECRUITABLE, b);
        return this;
    }

    public void setKnowsTeam(boolean b) {
    	FactionModifierService.modify(this, FactionModifier.KNOWS_ALLIES, b);
    }

    public boolean knowsTeam() {
    	return this._modifierMap.getOrDefault(FactionModifier.KNOWS_ALLIES, false);
    }

    public boolean hasNightChat() {
    	return this._modifierMap.getOrDefault(FactionModifier.HAS_NIGHT_CHAT, false);
    }

    public void setNightChat(boolean b) {
    	FactionModifierService.modify(this, FactionModifier.HAS_NIGHT_CHAT, b);
    }

    public int getPunchSuccess() {
    	return this._modifierMap.getOrDefault(FactionModifier.PUNCH_SUCCESS, 0);
    }

    public boolean hasLastWill() {
    	return this._modifierMap.getOrDefault(FactionModifier.LAST_WILL, false);
    }

    public void infiltrate(Player owner) {
        if(infiltrators == null)
            infiltrators = new PlayerList();
        infiltrators.add(owner);
    }

    public void onDayStart() {
        if(isAlive() && infiltrators != null)
            for(Player infil: infiltrators){
                addMember(infil);
            }
        infiltrators = null;
        if(sender == null)
            return;
        for(int ability: sender.keySet())
            getAbility(ability).onDayStart(sender.get(ability));

        sender.clear();
    }

    public void onNightStart() {
        AbilityList abilities = getAbilities();
        if(abilities != null)
            for(Ability a: abilities)
                a.onNightStart(null);
    }

    public void onNightEnd() {
        if(_abilities != null)
            for(Ability a: _abilities)
                a.onPlayerNightEnd(null); // maybe review who's using this.
    }

    public void matchProperties(Faction t) {
        for(FactionModifier fRuleID: FactionModifier.values()){
            if(Ability.IsIntModifier(fRuleID)){
                setRule(fRuleID, t._modifierMap.getInt(fRuleID));
            }else{
                setRule(fRuleID, t._modifierMap.getBoolean(fRuleID));
            }
        }
        this.name = t.name;

        for(String enemyTeam: t.enemyColors){
            enemyColors.add(enemyTeam);
        }

        for(String sheriffCheckable: t.sheriffDetectables){
            sheriffDetectables.add(sheriffCheckable);
        }

        if(t._abilities != null){
            for(Ability a: t._abilities){
                if(a instanceof FactionSend)
                    continue;
                addAbility(a.newCopy());
            }
        }
        _modifierMap = Modifiers.copy(t._modifierMap);
    }

    // sender is never null
    public static void warnAbilityOwnerChange(Player sender, Player intendedTarget, Player before, Player after,
            Faction t) {
        if(!sender.narrator.getPossibleMembers().hasMembersThatAffectSending(sender.narrator))
            return;

        Message e = new OGIMessage();
        if(before == null){
            if(after == null)
                return;
            e.add(after, " can perform the faction kill.");
            t.getMembers().remove(sender).warn(e);
        }
        // the send is useless
        else if(before == intendedTarget)
            return;

        // the send isn't changing who is being sent out
        else if(before == after && intendedTarget != null){
            sender.warn(e.add("You voted to send ", intendedTarget, " for the kill, but ", before,
                    " is still in control of the factional kill."));
        }

        else{// after the send, no one is being sent out. tell everyone this.
            if(after == null){
                e.add("There is no longer a consensus on who should be in control of the factional kill.");
                t.getMembers().remove(after).warn(e);
            }else{
                e.add("Instead of ", before, ", ", after, " is now in control of the kill.");
                t.getMembers().warn(e);
            }
        }

    }

    public static Faction GetFaction(ArrayList<String> commands, Narrator narrator) {
        Faction faction = narrator.getFaction(commands.get(0));

        if(faction != null){
            commands.remove(0);
            return faction;
        }

        ArrayList<String> toAddBack = new ArrayList<>();

        StringBuilder sb = new StringBuilder(commands.remove(0));
        toAddBack.add(sb.toString());

        faction = narrator.getFactionByName(sb.toString());
        String s;
        while (!commands.isEmpty() && faction == null){
            sb.append(" ");
            s = commands.remove(0);
            toAddBack.add(s);
            sb.append(s);
            faction = narrator.getFactionByName(sb.toString());
        }

        if(faction == null){
            for(int i = toAddBack.size() - 1; i >= 0; i--){
                commands.add(0, toAddBack.remove(i));
            }
        }

        return faction;

    }

    public static Option Option(Faction faction) {
        return new Option(faction.getColor(), faction.getName()).setColor(faction.getColor());
    }

    public static boolean CanFriendlyFire(Narrator narrator) {
        return narrator.getPossibleMembers().allowsForFriendlyFire(narrator);
    }

}
