package game.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.support.RoleTemplate;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.Modifier;
import game.logic.support.rules.RoleModifier;
import game.roles.Ability;
import game.roles.AbilityList;
import models.Modifiers;
import services.RoleAbilityModifierService;
import services.RoleModifierService;

//role creator
public class Role extends RoleTemplate implements Comparable<Role> {

	public String name;
    public AbilityList _abilities;
    public Modifiers<RoleModifier> modifiers;
    public Narrator narrator;
    // every property doesn't directly apply to the role itself except for charges
    // for charges, we'll use the charges name, and the index to indicate which one
    // should have the charges modifier

    public Role(Narrator narrator, String name, Ability... roles) {
        this.narrator = narrator;
        this._abilities = new AbilityList(roles);
        this.modifiers = new Modifiers<>();
        setName(name);
    }

    public Role(Narrator narrator, String name, AbilityList aList) {
        this.narrator = narrator;
        this._abilities = aList;
        setName(name);
        this.modifiers = new Modifiers<>();
    }

    private void setName(String name) {
    	if(name == null)
    		throw new NullPointerException("This shouldn't ever happen.");
    	this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public ArrayList<String> getDescription() {
        ArrayList<String> list = new ArrayList<>();
        for(Ability a: _abilities){
            for(String s: a.getRoleInfo(narrator))
                list.add(s);
        }
        return list;
    }

    @Override
    public String toString() {
        return name;
    }

    public AbilityList getBaseAbility() {
        return _abilities;
    }

    public static AbilityList rolesCopyHelper(AbilityList rs) {
        return rs.copy();
    }

    @Override
    public boolean contains(Class<? extends Ability> c) {
        for(Ability a: _abilities)
            if(a.getClass().equals(c))
                return true;
        return false;
    }

    public boolean contains(Role m) {
        return Ability.isEqualContent(_abilities, m._abilities);
    }

    @Override
    public int compareTo(Role arg0) {
        return name.compareTo(arg0.name);
    }

    public Set<Modifier> getRuleIDs() {
        Set<Modifier> rules = new HashSet<>();
        for(Ability role: _abilities){
        	rules.addAll(role.getAbilityModifiers());
        	rules.addAll(role.getSetupModifiers());
        }
        return rules;
    }

    public Role deepCopy() {
        Ability[] abilities = rolesCopyHelper(_abilities).toArray();
        Role role = new Role(this.narrator, getName(), abilities);
        role.modifiers = Modifiers.copy(modifiers);
        return role;
    }

    // simple name
    @Override
    public boolean contains(String simpleName) {
        return _abilities.getClass().getSimpleName().equals(simpleName);
    }

    @Override
    public void prefer(Player p) {
        p.rolePrefer(getName());
    }

    public boolean isUnique() {
        for(Ability a: _abilities){
            if(a.isUnique())
                return true;
        }
        return modifiers.getOrDefault(RoleModifier.UNIQUE, false);
    }

    public void addCooldownModifier(Class<? extends Ability> class1, int dayCD) {
        addModifier(AbilityModifier.COOLDOWN, class1, dayCD);
    }

    public Role addChargeModifier(int charges) {
        if(_abilities.size() != 1)
            throw new IllegalActionException();
        addModifier(AbilityModifier.CHARGES, _abilities.get(0).getClass(), charges);
        return this;
    }

    public void hidePassive() {
        if(_abilities.size() != 1)
            throw new IllegalActionException();
        addModifier(AbilityModifier.HIDDEN, _abilities.get(0).getClass(), true);
    }

    @Override
    public boolean isBoolModifier(Modifier modifier) {
        for(Ability a: _abilities){
            if(a.isBool(modifier))
                return true;
        }
        return false;
    }

    @Override
    public boolean isIntModifier(Modifier modifier) {
        for(Ability a: _abilities){
            if(a.isInt(modifier))
                return true;
        }
        return false;
    }

    @Override
    public RoleTemplate addModifier(RoleModifier modifier, boolean b) {
    	RoleModifierService.modify(narrator, this, modifier, b);
        return this;
    }

    @Override
    public RoleTemplate addModifier(RoleModifier modifier, int val) {
        RoleModifierService.modify(narrator, this, modifier, val);
        return this;
    }

    public RoleTemplate addModifier(AbilityModifier modifier, Class<? extends Ability> abilityClass, boolean b) {
    	RoleAbilityModifierService.modify(this, abilityClass, modifier, b);
        return this;
    }

    public void addModifier(AbilityModifier modifier, Class<? extends Ability> abilityClass, int val) {
        RoleAbilityModifierService.modify(this, abilityClass, modifier, val);
    }

    public final boolean is(Class<? extends Ability> c) {
        for(Ability r: _abilities){
            if(r.getClass().equals(c))
                return true;
        }
        return false;
    }

    public final boolean is(Ability checkRole) {
        for(Ability r: _abilities){
            if(r.getClass().equals(checkRole.getClass()))
                return true;
        }
        return false;
    }

    // just one role needs to be a chat role for this to return true
    public boolean isChatRole() {
        for(Ability r: _abilities){
            if(r.isChatRole())
                return true;
        }
        return false;
    }

    // every role needs to be a can start with enemies for this to return true
    public boolean canStartWithEnemies() {
        for(Ability r: _abilities){
            if(!r.canStartWithEnemies())
                return false;
        }
        return true;
    }

    public AbilityList getAbilityCopy() {
        return _abilities.copy();
    }

    public boolean hasAbilities(Role toCheck) {
        return Ability.isEqualContent(_abilities, toCheck._abilities);
    }

    public boolean hasMembersThatAffectSending() {
        for(Ability a: _abilities){
            if(a.affectsSending())
                return true;
        }
        return false;
    }

    public boolean allowsForFriendlyFire() {
        for(Ability a: _abilities){
            if(a.allowsForFriendlyFire())
                return true;
        }
        return false;
    }
    
    public List<String> getPublicDescription(String color){
    	List<String> ruleTexts = new LinkedList<>();
        for(Ability a: _abilities)
            ruleTexts.addAll(a.getPublicDescription(narrator, name, color, this._abilities));
        ruleTexts.addAll(Ability.GetModifierRuleTexts(modifiers));
        return ruleTexts;

    }

    @Override
    public Role addAbility(Class<? extends Ability> abilityClass) {
        Ability ability = Ability.CREATOR(abilityClass.getSimpleName());
        return addAbility(ability);
    }

    public Role addAbility(Ability a) {
        _abilities = new AbilityList(_abilities, a);
        return this;
    }

    @Override
    public void removeAbility(Class<? extends Ability> a) {
        ArrayList<Ability> list = new ArrayList<>();
        for(Ability b: _abilities){
            if(b.getClass() == a)
                continue;
            list.add(b);
        }
        _abilities = new AbilityList(list);
    }

    public void checkIsNightless() {
        for(Ability ability: _abilities)
            ability.checkIsNightless(this.narrator);
    }

    public void checkVisibleAbilities() {
        int size = 0;
        for(Ability ability: _abilities)
        	if(ability.modifiers != null && ability.modifiers.getOrDefault(AbilityModifier.HIDDEN, false))
        		size++;
        if(size >= _abilities.size())
            throw new IllegalGameSettingsException("No visible abilities for " + this.getName());
    }

    public Ability getAbility(Class<? extends Ability> abilityClass) {
        for(Ability a: this._abilities){
            if(a.getClass().equals(abilityClass))
                return a;
        }
        return null;
    }

    public Ability getAbility() {
        return this._abilities.get(0);
    }

    public boolean hasAbility(Class<? extends Ability> abilityClass) {
        return getAbility(abilityClass) != null;
    }

    public int getAbilityCount() {
        return this._abilities.size();
    }

    @Override
    public boolean isRandom() {
        return false;
    }
}
