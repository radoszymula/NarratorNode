package game.logic.exceptions;

@SuppressWarnings("serial")
public class NotEqualsException extends Error {

    public NotEqualsException(String string) {
        super(string);
    }

    public NotEqualsException() {
    }

}
