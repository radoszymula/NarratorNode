package game.logic.exceptions;

@SuppressWarnings("serial")
public class IllegalActionException extends NarratorException {

    public IllegalActionException(String string) {
        super(string);
    }

    public IllegalActionException() {
        super("");
    }

}
