package game.logic.exceptions;

import game.logic.Player;
import game.logic.Faction;
import game.logic.support.RoleTemplate;

@SuppressWarnings("serial")
public class NamingException extends NarratorException {

    public Player playerHolder;
    public RoleTemplate rt;
    public Faction t;

    public NamingException(String name, Player p) {
        super(name);
        this.playerHolder = p;
    }

    public NamingException(String name, RoleTemplate p) {
        super(name);
        this.rt = p;
    }

    public NamingException(String string) {
        super(string);
    }

    public NamingException(String name, Faction t2) {
        super(name);
        this.t = t2;
    }
}
