package game.logic.exceptions;

@SuppressWarnings("serial")
public class UnknownHiddenException extends NarratorException {
    public Long hidden_id;

    public UnknownHiddenException(long hiddenID) {
        super();
        this.hidden_id = hiddenID;
    }
}
