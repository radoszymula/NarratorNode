package game.logic.exceptions;

@SuppressWarnings("serial")
public class PhaseException extends NarratorException {

    public PhaseException() {
        this("");
    }

    public PhaseException(String role) {
        super(role);
    }

}
