package game.logic.exceptions;

@SuppressWarnings("serial")
public class StoryReaderError extends Error {

    public StoryReaderError(String message) {
        super(message);
    }

}
