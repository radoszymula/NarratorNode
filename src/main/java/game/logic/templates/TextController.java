package game.logic.templates;

import java.util.ArrayList;

import game.ai.Controller;
import game.event.ChatMessage;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.exceptions.UnsupportedMethodException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.Util;
import game.roles.Ability;
import game.roles.Spy;
import game.roles.Ventriloquist;

public class TextController implements Controller {

    protected TextInput texter;
    public String playerID;

    public TextController(TextInput ti, String player_id) {
        this.texter = ti;
        this.playerID = player_id;
    }

    public TextController(TextInput ti) {
        this.texter = ti;
    }

    public TextController() {
        // must set a text input right away
    }

    @Override
    public void log(String string) {

    }

    public static final boolean SYNC = true;
    public static final boolean ASYNC = false;

    @Override
    public void endNight() {
        texter.text(playerID, Constants.END_NIGHT, SYNC);
    }

    @Override
    public void cancelEndNight() {
        endNight();
    }

    @Override
    public void setNightTarget() {
        throw new UnsupportedMethodException();
        /*
         * if(a.is(Veteran.class)) texter.text(playerID, Veteran.COMMAND, ASYNC); else
         * if(a.is(Burn.class)) texter.text(a, Burn.COMMAND, ASYNC);
         */
    }

    @Override
    public void cancelAction(int actionIndex) {
        // I'm pretty sure the actionIndex coming in is 0-based. CommandHandler reads it
        // as 1-based
        texter.text(playerID, Constants.CANCEL + " " + Integer.toString(actionIndex + 1), ASYNC);
    }

    public void clearTargets(Player p) {

    }

    @Override
    public void spy(String team) {
        texter.text(playerID, Spy.COMMAND + " " + team, ASYNC);
    }

    @Override
    public void vest() {
        texter.text(playerID, Constants.VEST_COMMAND, ASYNC);
    }

    @Override
    public Controller vote(Controller target) {
        texter.text(playerID, Constants.VOTE + " " + target.getName(), SYNC);
        return target;
    }

    @Override
    public Controller skipVote() {
        texter.text(playerID, Constants.SKIP_VOTE, SYNC);
        return this;
    }

    @Override
    public ChatMessage say(String text, String key) {
    	String messageInput = Constants.SAY + " " + key + " " + text;
        texter.text(playerID, messageInput, ASYNC);
        return null;
    }

    @Override
    public void doDayAction(String ability, String option, Controller... targets) {
        this.setNightTarget(ability, option, null, null, targets);
    }

    @Override
    public Controller unvote() {
        texter.text(playerID, Constants.UNVOTE, SYNC);
        return this;
    }

    @Override
    public Controller ventVote(Controller puppet, Controller target) {
        texter.text(playerID, Ventriloquist.VENT_VOTE + " " + puppet.getName() + " " + target.getName(), SYNC);
        return this;
    }

    @Override
    public void ventUnvote(Controller puppet) {
        texter.text(playerID, Ventriloquist.VENT_UNVOTE + " " + puppet.getName(), SYNC);
    }

    @Override
    public void ventSkipVote(Controller puppet) {
        texter.text(playerID, Ventriloquist.VENT_SKIP_VOTE + " " + puppet, SYNC);
    }

    public interface TextInput {
        void text(String player_id, String message, boolean sync);
    }

    @Override
    public boolean cleanup() {
        return true;
    }

    @Override
    public void clearPreferences() {
        throw new UnsupportedMethodException();
    }

    @Override
    public Controller setName(String string) {
        throw new UnsupportedMethodException();
        // return this;
    }

    @Override
    public void setNightTarget(String action, Controller... b) {
        this.setNightTarget(action, null, null, null, b);
    }

    @Override
    public void setNightTarget(String action, String option, Controller... targets) {
        this.setNightTarget(action, option, null, null, targets);
    }

    @Override
    public void setNightTarget(String action, String option, String opt2, Controller... b) {
        this.setNightTarget(action, option, opt2, null, b);
    }

    @Override
    public boolean hasError() {
        return false;
    }

    @Override
    public void setNightTarget(String action, String option, String opt2, String opt3, Controller... targets) {
        ArrayList<String> commandParts = new ArrayList<>();
        commandParts.add(action);
        for(Controller c: targets)
            commandParts.add(c.getName());

        if(option != null){
            commandParts.add(option);

            if(opt2 != null){
                commandParts.add(opt2);

                if(opt3 != null)
                    commandParts.add(opt3);
            }
        }
        texter.text(playerID, action + " " + Util.SpacedString(commandParts), SYNC);
    }

    @Override
    public void setLastWill(String string) {
        texter.text(playerID, Constants.LAST_WILL + " " + string, ASYNC);
    }

    @Override
    public void setTeamLastWill(String string) {
        texter.text(playerID, Constants.TEAM_LAST_WILL + " " + string, ASYNC);
    }

    @Override
    public void rolePrefer(String s) {
        texter.text(playerID, CommandHandler.PREFER + " " + s, ASYNC);
    }

    @Override
    public void clearTargets() {
        throw new UnsupportedMethodException();
    }

    @Override
    public String getName() {
        throw new UnsupportedMethodException();
    }

    @Override
    public boolean isTargeting(String ability, Controller... targets) {
        throw new UnsupportedMethodException();
    }

    @Override
    public Player getPlayer(Narrator narrator) {
        throw new UnsupportedMethodException();
    }

    @Override
    public boolean is(Class<? extends Ability> classes) {
        throw new UnsupportedMethodException();
    }

    @Override
    public String getColor() {
        throw new UnsupportedMethodException();
    }

    @Override
    public ArrayList<String> getChatKeys() {
        throw new UnsupportedMethodException();
    }

    @Override
    public ArrayList<String> getCommands() {
        throw new UnsupportedMethodException();
    }

    @Override
    public String getRoleName() {
        throw new UnsupportedMethodException();
    }

}
