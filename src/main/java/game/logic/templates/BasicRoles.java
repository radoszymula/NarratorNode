package game.logic.templates;

import java.util.ArrayList;
import java.util.Collections;

import game.logic.Faction;
import game.logic.Narrator;
import game.roles.Ability;
import game.roles.AbilityList;
import game.roles.Agent;
import game.roles.Amnesiac;
import game.roles.Architect;
import game.roles.Armorsmith;
import game.roles.ArmsDetector;
import game.roles.Assassin;
import game.roles.Baker;
import game.roles.Blackmailer;
import game.roles.Blacksmith;
import game.roles.Bodyguard;
import game.roles.Bomb;
import game.roles.Bulletproof;
import game.roles.Burn;
import game.roles.Citizen;
import game.roles.Clubber;
import game.roles.Commuter;
import game.roles.Coroner;
import game.roles.Coward;
import game.roles.CultLeader;
import game.roles.Cultist;
import game.roles.Detective;
import game.roles.Disfranchise;
import game.roles.Disguiser;
import game.roles.Doctor;
import game.roles.Douse;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.Elector;
import game.roles.ElectroManiac;
import game.roles.Enforcer;
import game.roles.Executioner;
import game.roles.Framer;
import game.roles.Ghost;
import game.roles.Godfather;
import game.roles.Goon;
import game.roles.GraveDigger;
import game.roles.Gunsmith;
import game.roles.Interceptor;
import game.roles.Investigator;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Jester;
import game.roles.Joker;
import game.roles.Lookout;
import game.roles.Marshall;
import game.roles.Mason;
import game.roles.MasonLeader;
import game.roles.MassMurderer;
import game.roles.Mayor;
import game.roles.Miller;
import game.roles.Operator;
import game.roles.ParityCheck;
import game.roles.Poisoner;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.Silence;
import game.roles.Sleepwalker;
import game.roles.Snitch;
import game.roles.Spy;
import game.roles.Stripper;
import game.roles.Survivor;
import game.roles.Tailor;
import game.roles.Thief;
import game.roles.Undouse;
import game.roles.Ventriloquist;
import game.roles.Veteran;
import game.roles.Vigilante;
import game.roles.Witch;
import game.setups.Setup;
import models.FactionRole;

public class BasicRoles {

    public static Narrator narrator;
    public static final String BUS_DRIVER = "Bus Driver";
    public static final String BULLETPROOF = "BulletProof";
    public static final String CHAUFFEUR = "Chauffeur";
    public static final String KIDNAPPER = "Kidnapper";
    public static final String ESCORT = "Escort";
    public static final String CONSORT = "Consort";
    public static final String ARSONIST = "Arsonist";

    public static Setup setup;

    public static FactionRole getMember(String teamColor, Ability... requiredAbilities) {
        Faction faction = narrator.getFaction(teamColor);
        if(faction == null)
            throw new NullPointerException();
        ArrayList<String> requiredAbilityList = getStringList(requiredAbilities), roleAbilityList;
        for(FactionRole factionRole: faction.getAllMembers()){
            roleAbilityList = getStringList(factionRole.role._abilities.toArray());
            if(requiredAbilityList.equals(roleAbilityList))
                return factionRole;
        }
        throw new NullPointerException();
    }

    private static ArrayList<String> getStringList(Ability... abilityArray) {
        ArrayList<String> stringList = new ArrayList<>();
        for(Ability ability: abilityArray){
            stringList.add(ability.getClass().getSimpleName());
        }
        Collections.sort(stringList);
        return stringList;
    }

    public static FactionRole Citizen() {
        return getMember(Setup.TOWN_C, new Citizen());
    }

    public static FactionRole Bulletproof() {
        return getMember(Setup.TOWN_C, new Bulletproof());
    }

    public static FactionRole Miller() {
        return getMember(Setup.TOWN_C, new Miller());
    }

    public static FactionRole Sleepwalker() {
        return getMember(Setup.TOWN_C, new Citizen(), new Sleepwalker());
    }

    public static FactionRole Bomb() {
        return getMember(Setup.TOWN_C, new Bomb());
    }

    public static FactionRole Sheriff() {
        return getMember(Setup.TOWN_C, new Sheriff());
    }

    public static FactionRole ArmsDetector() {
        return getMember(Setup.TOWN_C, new ArmsDetector());
    }

    public static FactionRole ParityCop() {
        return getMember(Setup.TOWN_C, new ParityCheck());
    }

    public static FactionRole Detective() {
        return getMember(Setup.TOWN_C, new Detective());
    }

    public static FactionRole Lookout() {
        return getMember(Setup.TOWN_C, new Lookout());
    }

    public static FactionRole Spy() {
        return getMember(Setup.TOWN_C, new Spy());
    }

    public static FactionRole Coroner() {
        return getMember(Setup.TOWN_C, new Coroner());
    }

    public static FactionRole Doctor() {
        return getMember(Setup.TOWN_C, new Doctor());
    }

    public static FactionRole Bodyguard() {
        return getMember(Setup.TOWN_C, new Bodyguard());
    }

    public static FactionRole Armorsmith() {
        return getMember(Setup.TOWN_C, new Armorsmith());
    }

    public static FactionRole Blacksmith() {
        return getMember(Setup.TOWN_C, new Blacksmith());
    }

    public static FactionRole EvilBlacksmith() {
        return getMember(Setup.OUTCAST_C, new Blacksmith());
    }

    public static FactionRole Escort() {
        return getMember(Setup.TOWN_C, new Stripper());
    }

    public static FactionRole Stripper(String color) {
        return getMember(color, new Stripper());
    }

    public static FactionRole BusDriver() {
        return getMember(Setup.TOWN_C, new Driver());
    }

    public static FactionRole Operator() {
        return getMember(Setup.TOWN_C, new Operator());
    }

    public static FactionRole Gunsmith() {
        return getMember(Setup.TOWN_C, new Gunsmith());
    }

    public static FactionRole Vigilante() {
        return getMember(Setup.TOWN_C, new Vigilante());
    }

    public static FactionRole Jailor() {
        return getMember(Setup.TOWN_C, new Jailor());
    }

    public static FactionRole Architect() {
        return getMember(Setup.TOWN_C, new Architect());
    }

    public static FactionRole Veteran() {
        return getMember(Setup.TOWN_C, new Veteran());
    }

    public static FactionRole Baker() {
        return getMember(Setup.TOWN_C, new Baker());
    }

    public static FactionRole Snitch() {
        return getMember(Setup.TOWN_C, new Snitch());
    }

    public static FactionRole Mayor() {
        return getMember(Setup.TOWN_C, new Mayor());
    }

    public static FactionRole Marshall() {
        return getMember(Setup.TOWN_C, new Marshall());
    }

    public static FactionRole Mason() {
        return getMember(Setup.TOWN_C, new Mason());
    }

    public static FactionRole MasonLeader() {
        return getMember(Setup.TOWN_C, new MasonLeader());
    }

    public static FactionRole Clubber() {
        return getMember(Setup.TOWN_C, new Clubber());
    }

    public static FactionRole Enforcer() {
        return getMember(Setup.TOWN_C, new Enforcer());
    }

    public static FactionRole Commuter() {
        return getMember(Setup.TOWN_C, new Commuter());
    }

    public static FactionRole Godfather() {
        return Godfather(Setup.MAFIA_C);
    }

    public static FactionRole Godfather(String color) {
        return getMember(color, new Godfather(), new Bulletproof());
    }

    public static FactionRole Goon() {
        return Goon(Setup.MAFIA_C);
    }

    public static FactionRole Goon(String color) {
        return getMember(color, new Goon());
    }

    public static FactionRole Consort() {
        return getMember(Setup.MAFIA_C, new Stripper());
    }

    public static FactionRole Blackmailer() {
        return getMember(Setup.MAFIA_C, new Blackmailer());
    }

    public static FactionRole Silencer() {
        return getMember(Setup.MAFIA_C, new Silence());
    }

    public static FactionRole Disfranchiser() {
        return getMember(Setup.MAFIA_C, new Disfranchise());
    }

    public static FactionRole Janitor() {
        return getMember(Setup.MAFIA_C, new Janitor());
    }

    public static FactionRole Framer() {
        return getMember(Setup.MAFIA_C, new Framer());
    }

    public static FactionRole Disguiser() {
        return getMember(Setup.MAFIA_C, new Disguiser());
    }

    public static FactionRole DrugDealer() {
        return getMember(Setup.MAFIA_C, new DrugDealer());
    }

    public static FactionRole Tailor() {
        return getMember(Setup.MAFIA_C, new Tailor());
    }

    public static FactionRole Agent() {
        return getMember(Setup.MAFIA_C, new Agent());
    }

    public static FactionRole Investigator() {
        return getMember(Setup.MAFIA_C, new Investigator());
    }

    public static FactionRole Chauffeur() {
        return getMember(Setup.MAFIA_C, new Driver());
    }

    public static FactionRole Kidnapper() {
        return getMember(Setup.MAFIA_C, new Jailor());
    }

    public static FactionRole Coward() {
        return getMember(Setup.MAFIA_C, new Coward());
    }

    public static FactionRole Assassin() {
        return getMember(Setup.MAFIA_C, new Assassin());
    }

    public static FactionRole Chauffeur2() {
        return getMember(Setup.YAKUZA_C, new Driver());
    }

    public static FactionRole Jester() {
        return getMember(Setup.BENIGN_C, new Jester());
    }

    public static FactionRole Ghost() {
        return getMember(Setup.BENIGN_C, new Ghost());
    }

    public static FactionRole Executioner() {
        return getMember(Setup.BENIGN_C, new Executioner());
    }

    public static FactionRole Survivor() {
        return getMember(Setup.BENIGN_C, new Survivor());
    }

    public static FactionRole Amnesiac() {
        return getMember(Setup.BENIGN_C, new Amnesiac());
    }

    public static FactionRole CultLeader() {
        return getMember(Setup.CULT_C, new CultLeader());
    }

    public static FactionRole Cultist() {
        return getMember(Setup.CULT_C, new Cultist());
    }

    public static FactionRole Witch() {
        return getMember(Setup.OUTCAST_C, new Witch());
    }

    public static FactionRole GraveDigger() {
        return getMember(Setup.OUTCAST_C, new GraveDigger());
    }

    public static FactionRole Interceptor() {
        return getMember(Setup.OUTCAST_C, new Interceptor());
    }

    public static FactionRole Elector() {
        return getMember(Setup.OUTCAST_C, new Elector());
    }

    public static FactionRole Ventriloquist() {
        return getMember(Setup.OUTCAST_C, new Ventriloquist());
    }

    public static FactionRole SerialKiller() {
        return getMember(Setup.THREAT_C, new SerialKiller(), new Bulletproof());
    }

    public static FactionRole Arsonist() {
        return getMember(Setup.THREAT_C, ArsonistPack().toArray());
    }

    public static AbilityList ArsonistPack() {
        return ArsonistPack(true);
    }

    public static AbilityList ArsonistPack(boolean invul) {
        if(invul)
            return new AbilityList(new Burn(), new Douse(), new Undouse(), new Bulletproof());
        return new AbilityList(new Burn(), new Douse(), new Undouse());
    }

    public static FactionRole Poisoner() {
        return getMember(Setup.THREAT_C, new Poisoner(), new Bulletproof());
    }

    public static FactionRole ElectroManiac() {
        return getMember(Setup.THREAT_C, new ElectroManiac(), new Bulletproof());
    }

    public static FactionRole MassMurderer() {
        return getMember(Setup.THREAT_C, new MassMurderer(), new Bulletproof());
    }

    public static FactionRole Joker() {
        return getMember(Setup.THREAT_C, new Joker(), new Bulletproof());
    }

    public static FactionRole Thief() {
        return getMember(Setup.BENIGN_C, new Thief());
    }

}
