package game.logic.templates;

import game.ai.Controller;
import game.logic.Narrator;

public interface ControllerGenerator {

    Controller getActionController(String name);

    NativeSetupController getSetupController();

    Narrator getNarrator();

    void reset();

    boolean cleanup();
}
