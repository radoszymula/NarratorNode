package game.logic.templates;

import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.Hidden;
import game.setups.Setup;
import services.HiddenService;
import services.RoleService;
import services.SetupHiddenService;

public class NativeSetupController {

    Narrator narrator;

    public NativeSetupController(Narrator narrator) {
        this.narrator = narrator;
    }

    public void createRole(Role role) {
        RoleService.createRole(narrator, role);
    }

    public void deleteRole(Role role) {
        narrator.deleteRole(role);
    }

    public void createHidden(Hidden hidden) {
        HiddenService.createHidden(narrator, hidden.getName(), hidden.getFactionRoles());
    }

    public void deleteHidden(Hidden hidden) {
        narrator.deleteHidden(hidden);
    }

    public void addHidden(Hidden hidden) {
        SetupHiddenService.addSetupHidden(narrator, hidden, false);
    }

    public void setAllies(String t1, String t2) {
        narrator.getFaction(t1).setAllies(narrator.getFaction(t2));
    }

    public void setEnemies(String t1, String t2) {
        narrator.getFaction(t1).setEnemies(narrator.getFaction(t2));
    }

    public void addSheriffDetectable(String t1, String t2) {
        narrator.getFaction(t1).addSheriffDetectableTeam(t2);
    }

    public void removeSheriffDetectable(String t1, String t2) {
        narrator.getFaction(t1).removeSheriffDetectableTeam(t2);
    }

    public void changeRule(SetupModifier ruleArr, int i) {
        narrator.setRule(ruleArr, i);
    }

    public void changeRule(SetupModifier ruleArr, boolean b) {
        narrator.setRule(ruleArr, b);
    }

    public void setTeamRule(String team, FactionModifier rule, int val) {
        narrator.getFaction(team).setRule(rule, val);
    }

    public void setTeamRule(String team, FactionModifier rule, boolean val) {
        narrator.getFaction(team).setRule(rule, val);
    }

    public void startGame() {
        narrator.startGame();
    }

    public void setSetup(String setupName) {
        Setup.GetSetup(narrator, setupName).applyRolesList();
    }

    public void addRoleAbility(Role role, Class<? extends Ability> abilityClass) {
        try{
            role.addAbility(abilityClass.newInstance());
        }catch(InstantiationException | IllegalAccessException e){
            e.printStackTrace();
        }
    }

    public void removeRoleAbility(Role role, Class<? extends Ability> abilityClass) {
        role.removeAbility(abilityClass);
    }

    public void modifyRole(Role m, RoleModifier modifier, int val) {
        m.addModifier(modifier, val);
    }

    public void modifyRole(Role m, RoleModifier modifier, boolean val) {
        m.addModifier(modifier, val);
    }

    public void modifyRoleAbility(Role m, AbilityModifier modifierID, Class<? extends Ability> abilityClass,
            boolean val) {
        m.addModifier(modifierID, abilityClass, val);
    }

    public void modifyRoleAbility(Role m, AbilityModifier modifierID, Class<? extends Ability> abilityClass, int val) {
        m.addModifier(modifierID, abilityClass, val);
    }

    public void restartGame() {
        narrator.restartGame(false, false);
    }

    public void addTeamAbility(String color, String simpleName) {
        narrator.getFaction(color).addAbility(Ability.CREATOR(simpleName));
    }

    public void removeTeamAbility(String color, String simpleName) {
        narrator.getFaction(color).removeAbility(Ability.CREATOR(simpleName).getClass());
    }

    public void modifyTeamAbility(String color, AbilityModifier modifier, Class<? extends Ability> abilityClass,
            int i) {
        narrator.getFaction(color).modifyAbility(modifier, abilityClass, i);
    }

    public void modifyTeamAbility(String color, AbilityModifier modifier, Class<? extends Ability> abilityClass,
            boolean i) {
        narrator.getFaction(color).modifyAbility(modifier, abilityClass, i);
    }

}
