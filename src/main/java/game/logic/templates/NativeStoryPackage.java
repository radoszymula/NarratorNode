package game.logic.templates;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.Random;
import game.logic.support.StoryPackage;
import game.logic.support.StoryReader;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.FactionSend;
import game.roles.Hidden;
import models.Command;
import models.FactionRole;
import models.Modifiers;
import models.SetupHidden;
import models.schemas.AbilityModifierSchema;
import models.schemas.FactionAbilityModifierSchema;
import models.schemas.FactionAbilitySchema;
import models.schemas.FactionModifierSchema;
import models.schemas.FactionRoleSchema;
import models.schemas.FactionSchema;
import models.schemas.GameOverviewSchema;
import models.schemas.HiddenSchema;
import models.schemas.PlayerSchema;
import models.schemas.RoleModifierSchema;
import models.schemas.RoleSchema;
import models.schemas.SetupHiddenSchema;

public class NativeStoryPackage implements StoryReader {

    Narrator input;

    public NativeStoryPackage(Narrator n) {
        input = n;
    }

    @Override
    public HashMap<SetupModifier, Rule> getRules() {
        HashMap<SetupModifier, Rule> ret = new HashMap<>();
        for(Rule r: input._rules.rules.values()){
            ret.put((SetupModifier) r.id, r);
        }
        return ret;
    }

    @Override
    public GameOverviewSchema getGameOverview() {
        GameOverviewSchema gameOverview = new GameOverviewSchema();
        gameOverview.hostID = -1;
        gameOverview.isPrivate = false;
        gameOverview.seed = input.getSeed();
        return gameOverview;
    }

    @Override
    public ArrayList<FactionSchema> getTeamInfo() {
        ArrayList<FactionSchema> teams = new ArrayList<>();
        FactionSchema newFaction;
        FactionAbilitySchema newAbility;
        FactionAbilityModifierSchema abilityModifier;
        FactionModifierSchema modifier;
        for(Faction oldFaction: input.getFactions()){
            newFaction = new FactionSchema();
            newFaction.color = oldFaction.getColor();
            newFaction.description = oldFaction.getDescription();
            newFaction.name = oldFaction.getName();
            newFaction.id = oldFaction.id;

            newFaction.abilities = new HashMap<>();
            if(oldFaction._abilities != null){
                for(Ability oldAbility: oldFaction._abilities){
                    if(oldAbility instanceof FactionSend)
                        continue;
                    newAbility = new FactionAbilitySchema();
                    newAbility.name = oldAbility.getName();
                    newAbility.modifiers = new HashSet<>();
                    newFaction.abilities.put(oldAbility.id, newAbility);
                    for(AbilityModifier oldAbilityModifier: oldAbility.modifiers.getModifierIDs()){
                        abilityModifier = new FactionAbilityModifierSchema();
                        abilityModifier.name = oldAbilityModifier.toString();
                        if(Ability.IsIntModifier(oldAbilityModifier))
                            abilityModifier.intValue = oldAbility.modifiers.getInt(oldAbilityModifier);
                        else
                            abilityModifier.boolValue = oldAbility.modifiers.getBoolean(oldAbilityModifier);
                        newAbility.modifiers.add(abilityModifier);
                    }
                }
            }

            newFaction.modifiers = new HashSet<>();
            if(oldFaction._modifierMap != null){
                for(FactionModifier factionModifier: oldFaction._modifierMap.getModifierIDs()){
                    modifier = new FactionModifierSchema();
                    modifier.name = factionModifier.toString();
                    if(Ability.IsIntModifier(factionModifier))
                        modifier.intValue = oldFaction._modifierMap.getInt(factionModifier);
                    else
                        modifier.boolValue = oldFaction._modifierMap.getBoolean(factionModifier);
                    newFaction.modifiers.add(modifier);
                }
            }
            teams.add(newFaction);
        }
        return teams;
    }

    @Override
    public void setEnemies(Narrator n) {
        for(Faction t: input.getFactions())
            for(String s: t.getEnemyColors())
                n.getFaction(s).setEnemies(t);
    }

    @Override
    public void setSheriffDetectables(Narrator n) {
        for(Faction t: input.getFactions())
            for(String s: t.getSheriffDetectables())
                n.getFaction(t.getColor()).addSheriffDetectableTeam(s);

    }

    @Override
    public Map<Long, RoleSchema> getRoles() {
        HashMap<Long, RoleSchema> roleMap = new HashMap<>();
        for(Role role: input.roles)
            roleMap.put(role.getID(), new RoleSchema(role));
        return roleMap;
    }

    @Override
    public List<HiddenSchema> getHiddens() {
        List<HiddenSchema> hiddens = new ArrayList<>();
        Random random = new Random();

        HiddenSchema hidden;
        for(Hidden inputHidden: input.hiddens){
            hidden = new HiddenSchema();
            if(inputHidden.getID() == null)
                inputHidden.setID(random.nextLong());
            hidden.id = inputHidden.getID();
            hidden.name = inputHidden.getName();
            hidden.factionRoleIDs = new HashSet<>();

            for(FactionRole role: inputHidden.getFactionRoles())
                hidden.factionRoleIDs.add(role.id);

            hiddens.add(hidden);
        }

        return hiddens;
    }

    @Override
    public ArrayList<PlayerSchema> getPlayers() {
        ArrayList<PlayerSchema> players = new ArrayList<>();

        PlayerSchema pp;
        for(Player p: input.getAllPlayers()){
            pp = new PlayerSchema();
            pp.isExited = !input.isInProgress() && input.isStarted();
            pp.name = p.getName();
            pp.factionRoleID = p.initialAssignedRole.assignedRole.id;
            pp.hiddenID = p.initialAssignedRole.assignedHidden.getID();
            players.add(pp);
        }

        return players;
    }

    @Override
    public ArrayList<Command> getCommands() {
        ArrayList<Command> commands = new ArrayList<>();
        for(Command s: input.getCommands())
            commands.add(new Command(s.playerID, s.text));
        return commands;
    }

    public static StoryPackage deserialize(Narrator narrator) {
        return StoryPackage.deserialize(new NativeStoryPackage(narrator), -1);
    }

    @Override
    public ArrayList<SetupHiddenSchema> getSetupHiddens() {
        ArrayList<SetupHiddenSchema> setupHiddenIDs = new ArrayList<>();

        SetupHiddenSchema setupHiddenSchema;
        for(SetupHidden setupHidden: input.getRolesList()){
            setupHiddenSchema = new SetupHiddenSchema();
            setupHiddenSchema.hiddenID = setupHidden.hidden.getID();
            setupHiddenSchema.isExposed = setupHidden.isExposed;
            setupHiddenIDs.add(setupHiddenSchema);
        }

        return setupHiddenIDs;
    }

    @Override
    public long getSetupOwnerID() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Set<FactionRoleSchema> getFactionRoles() {
        Set<FactionRoleSchema> factionRoles = new HashSet<>();
        FactionRoleSchema factionRoleSchema;
        for(Faction faction: input.getFactions()){
            for(FactionRole factionRole: faction._roleSet.values()){
                factionRoleSchema = new FactionRoleSchema();
                factionRoleSchema.id = factionRole.id;
                factionRoleSchema.factionID = faction.id;
                factionRoleSchema.roleID = factionRole.role.getID();
                factionRoleSchema.roleModifiers = getFactionRoleRoleModifierSchema(factionRole);
                factionRoleSchema.abilityModifiers = getFactionRoleAbilityModifierSchema(factionRole);
                factionRoles.add(factionRoleSchema);
            }
        }
        return factionRoles;
    }

    private Map<Long, Set<AbilityModifierSchema>> getFactionRoleAbilityModifierSchema(FactionRole factionRole) {
        Map<Long, Set<AbilityModifierSchema>> abilityModifiers = new HashMap<>();
        Modifiers<AbilityModifier> modifiers;
        Object value;
        long abilityID;
        Set<AbilityModifierSchema> modifiersSet;
        for(Class<? extends Ability> abilitClass: factionRole.abilityModifiers.keySet()){
            modifiers = factionRole.abilityModifiers.get(abilitClass);
            abilityID = factionRole.role.getAbility(abilitClass).id;
            modifiersSet = new HashSet<>();
            for(AbilityModifier modifier: modifiers.getModifierIDs()){
                value = modifiers.get(modifier);
                modifiersSet.add(new AbilityModifierSchema(modifier, value));
            }
            abilityModifiers.put(abilityID, modifiersSet);
        }
        return abilityModifiers;
    }

    private Set<RoleModifierSchema> getFactionRoleRoleModifierSchema(FactionRole factionRole) {
        Set<RoleModifierSchema> roleModifiers = new HashSet<>();
        Object value;
        for(RoleModifier modifier: factionRole.roleModifiers.getModifierIDs()){
            value = factionRole.roleModifiers.get(modifier);
            roleModifiers.add(new RoleModifierSchema(modifier, value));
        }
        return roleModifiers;
    }

}
