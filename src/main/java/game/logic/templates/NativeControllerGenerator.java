package game.logic.templates;

import game.ai.Controller;
import game.logic.Narrator;

public class NativeControllerGenerator implements ControllerGenerator {

    Narrator n;

    public NativeControllerGenerator() {
        n = new Narrator();
    }

    @Override
    public Controller getActionController(String name) {
        return n.addPlayer(name).setComputer();
    }

    @Override
    public NativeSetupController getSetupController() {
        return new NativeSetupController(n);
    }

    @Override
    public Narrator getNarrator() {
        return n;
    }

    @Override
    public void reset() {
        n = new Narrator();
    }

    @Override
    public boolean cleanup() {
        return false;
    }

}
