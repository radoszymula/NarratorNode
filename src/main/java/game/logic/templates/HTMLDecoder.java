package game.logic.templates;

import game.event.EventDecoder;
import game.event.Message;
import game.event.Message.PlayerWrapper;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.roles.Marshall;
import game.roles.Miller;

public class HTMLDecoder implements EventDecoder {

    public static final String INITIAL = Message.INITIAL;

    @Override
    public String decode(PlayerWrapper p, int day, String key) {
        Narrator n = p.player.narrator;
        String color, role;
        if(key.equals(Message.PRIVATE) || (n.isStarted() && !n.isInProgress())){
            color = p.getColor();
            role = p.player.getID() + "(" + p.player.getRoleName() + ")";
        }else if(!n.isStarted()){
            color = INITIAL;
            role = p.getName();
        }else if(p.name == null && (p.player.isAlive() || p.player.getDeathType().isCleaned()
                || Marshall.isCurrentMarshallLynch(p.player)) && n.isInProgress()){
            color = INITIAL;
            role = p.player.getDescription(p.getName(), p.role);
        }else if(p.name == null && p.player.hasSuits() && n.isInProgress()){
            color = p.player.suits.get(0).color;
            role = p.player.getDescription(p.getName(), p.role);
        }else if(p.player.is(Miller.class) && p.player.narrator.getBool(SetupModifier.MILLER_SUITED)){
            color = Miller.MillerColor(p.player.narrator);
            role = p.player.getGraveyardRoleName();
        }else{
            if(p.name != null){// handles jailor
                role = p.name;
                color = INITIAL;
            }else{
                role = p.player.getDescription(p.getName(), p.role);
                color = p.getColor();
            }
        }
        return coloredText(role, color);
    }

    @Override
    public String coloredText(String s, String color) {

        return "<font color = " + color + ">" + s + "</font>";
    }

    public static String ToHex(int color) {
        String hexColor = Integer.toHexString(color);// apparently there are 2 ffs
        while (hexColor.length() < 6){
            hexColor = "0" + hexColor;
        }
        while (hexColor.length() > 6){
            hexColor = hexColor.substring(1);
        }
        return hexColor;
    }

    public String toHex(int i) {
        String s = Integer.toHexString(i);
        if(s.length() == 1)
            return "0" + s;
        return s;
    }

    @Override
    public String feedbackDecode(String access) {
        return "<span class='feedback'>" + access + "</span>";
    }

    @Override
    public String bold(String ret) {
        return "<b>" + ret + "</b>";
    }

    @Override
    public String headerDecode(String toRet) {
        return "<div class='headerLabel'><u>" + toRet + "</u></div>";
    }

}
