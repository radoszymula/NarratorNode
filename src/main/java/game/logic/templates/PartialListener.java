package game.logic.templates;

import game.event.DeathAnnouncement;
import game.event.EventList;
import game.event.Message;
import game.event.SelectionMessage;
import game.event.VoteAnnouncement;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.listeners.NarratorListener;
import game.logic.support.action.Action;

public class PartialListener implements NarratorListener {

    @Override
    public void onAssassination(Player assassin, Player target, DeathAnnouncement e) {

    }

    @Override
    public void onDayBurn(Player arson, PlayerList burned, DeathAnnouncement e) {

    }

    @Override
    public void onAnnouncement(Message nl) {

    }

    @Override
    public void onCancelEndNight(Player p) {

    }

    @Override
    public void onDayActionSubmit(Player submitter, Action a) {

    }

    @Override
    public void onDayPhaseStart(PlayerList newDead) {

    }

    @Override
    public void onElectroExplosion(PlayerList deadPeople, DeathAnnouncement explosion) {

    }

    @Override
    public void onEndNight(Player p, boolean forced) {

    }

    @Override
    public void onGameStart() {

    }

    @Override
    public void onGameEnd() {

    }

    @Override
    public void onMessageReceive(Player receiver, Message e) {

    }

    @Override
    public void onModKill(PlayerList bad) {

    }

    @Override
    public void onNightEnding() {

    }

    @Override
    public void onNightPhaseStart(PlayerList lynched, PlayerList poisoned, EventList events) {
    }

    @Override
    public void onRolepickingPhaseStart() {
    }

    @Override
    public void onRoleReveal(Player mayor, Message e) {

    }

    @Override
    public void onTargetRemove(Player owner, String command, PlayerList prev) {

    }

    @Override
    public void onTargetSelection(Player owner, SelectionMessage selectionMessage) {

    }

    @Override
    public void onTrialStart(Player trailedPlayer) {
    }

    @Override
    public void onVote(Player voter, Player target, VoteAnnouncement e) {

    }

    @Override
    public void onVoteCancel(Player voter, Player prev, VoteAnnouncement e) {

    }

    @Override
    public void onVotePhaseReset(int resetsRemaining) {
    }

    @Override
    public void onVotePhaseStart() {

    }

    @Override
    public void onWarningReceive(Player player, Message m) {

    }

}
