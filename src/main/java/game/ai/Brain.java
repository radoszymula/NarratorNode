package game.ai;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import game.event.Message;
import game.logic.DeathType;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PhaseException;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.templates.HTMLDecoder;
import game.roles.FactionKill;
import game.roles.Ghost;
import models.FactionRole;
import models.SetupHidden;
import models.enums.GamePhase;

public class Brain {

    public HashMap<Player, Computer> computers = new HashMap<>();

    private ArrayList<Claim> claims = new ArrayList<>();
    HashMap<Faction, ArrayList<PlayerList>> suspList = new HashMap<>();
    private double ballsyMaf = 0.99;

    public Random random;
    public PlayerList slaves;

    public Narrator narrator;

    public Brain(Narrator n, Random r) {
        this(n, n.getAllPlayers(), r);
        setNarrator(n);
    }

    public Brain(Narrator narrator, PlayerList slaves, Random random) {
        this.narrator = narrator;
        mafKiller = new HashMap<>();
        mafKillTarget = new HashMap<>();
        this.random = random;

        this.slaves = slaves;
        for(Player p: slaves)
            computers.put(p, new Computer(p, this));
    }

    public Brain(HashMap<Player, Controller> controls, Random rand) {
        this.narrator = slaves.get(0).narrator;
        mafKiller = new HashMap<>();
        mafKillTarget = new HashMap<>();
        this.random = rand;

        this.slaves = new PlayerList();
        for(Player p: controls.keySet()){
            slaves.add(p);
            computers.put(p, new Computer(p, this, controls.get(p)));
        }
    }

    public Brain setNarrator(Narrator n) {
        BrainListener bl = new BrainListener(this);
        if(n.isStarted())
            bl.onFirstPhaseStart();
        n.addListener(bl);
        return this;
    }

    public int size() {
        return computers.size();
    }

    public ControllerList dayAction() {
        if(slaves.isEmpty() || !slaves.get(0).narrator.isDay())
            return new ControllerList();
        talkings();
        return randomLynch();
    }

    private ControllerList randomLynch() {
        Player choice = random.getPlayer(slaves.getLivePlayers());
        return Brain.Vote(this, choice, "No one's died in a while, so I'll random vote...");
    }

    private void talkings() {
        for(Player s: slaves.getLivePlayers()){
            computers.get(s).talkings();
        }
    }

    // returns people that voted
//    private ControllerList voting(HashMap<Computer, PlayerList[]> accusedMap) throws ClaimInterrupt{
//    	ControllerList voters = new ControllerList();
//    	if(preconditionVotingChecks(voters))
//    		return voters;
//    	
//    	Narrator n = slaves.get(0).narrator;
//    	PlayerList livePlayers = n.getLivePlayers();
//    	
//    	int minVoteToLynch = getMinLynchVote();
//    	
//    	boolean dub = false;
//    	for(Player p: livePlayers){
//    		int voteCount = p.getVoteCount();
//    		if(voteCount == 0){
//    			continue;
//    		}
//    		
//    		if(voteCount < minVoteToLynch){//if i find a new low non0 
//    			minVoteToLynch = voteCount;
//    			dub = false;
//    		}else if(voteCount == minVoteToLynch){
//    			dub = true;
//    		}
//    	}
//    	PlayerList choices;
//    	PlayerList needToChange;
//    	
//    	choices = n.getLivePlayers();
//		needToChange = choices.intersect(slaves);
//    	
//		int voteCount;
//		PlayerList votersOfP;
//    	if(minVoteToLynch != getMinLynchVote() && !claimsIncreased()){
//    		choices = new PlayerList();
//    		needToChange = new PlayerList();
//    	
//	    	for(Player p: slaves){
//	    		if(p.isDead())
//	    			continue;
//	    		voteCount = p.getVoteCount();
//	    		votersOfP = p.getVoters();
//	    		if(voteCount > minVoteToLynch){
//	    			choices.add(p);
//	    		}else if (voteCount == minVoteToLynch){
//	    			if(dub && minVoteToLynch > 1){
//		    			choices.add(p);
//	    			}
//	    			for(Player toChange: votersOfP)
//	    				if(slaves.contains(toChange))
//	    					needToChange.add(toChange);
//	    		}else{
//	    			for(Player toChange: votersOfP)
//	    				if(slaves.contains(toChange))
//	    					needToChange.add(toChange);
//	    		}
//	    	}
//    	}
//    	
//    	Computer c = null;
//    	PlayerList playerChoices, shifty, safe;
//    	PlayerList[] processedPeople;
//    	for(Player p: n._players){
//    		c = computers.get(p);
//    		if(c == null)
//    			continue;
//    		processedPeople = accusedMap.get(c);
//    		if(processedPeople == null)
//    			continue;
//    		playerChoices = choices.copy();
//    		shifty = processedPeople[0];
//    		safe = processedPeople[1];
//    		playerChoices.remove(safe);
//    		playerChoices.softIntersect(shifty);
//    		if(playerChoices.isEmpty())
//    			playerChoices = n.getLivePlayers();
//    		if(c.vote(playerChoices) != null){
//    			voters.add(p);
//    			if(c.slave.getVoteTarget() == null)
//    				break;
//    		}
//    		if(!n.isDay())
//    			break;
//    	}
//    	
//    	return voters;
//    }

    public void nightAction() {
        reset();
        for(Player c: slaves.sortByID()){
            if(c.isDead() && !c.is(Ghost.class))
                continue;
            Computer comp = computers.get(c);
            if(comp != null && c.narrator.isNight())
                comp.doNightAction();
        }
        reset();
    }

    private HashMap<String, Player> mafKiller;
    private HashMap<String, Player> mafKillTarget;

    public boolean talkingEnabled = true;

    public void reset() {
        mafKiller.clear();
        mafKillTarget.clear();
        suspList.clear();
    }

    public Player getMafSender(Player slave) {
        Player killer = null;
        for(Faction x: slave.getFactions()){
            killer = mafKiller.get(x.getColor());
            if(x.hasMember(slave) && killer != null){
                return killer;
            }
        }

        Faction theTeam = slave.getFactions().getFactionWithAbility(FactionKill.MAIN_ABILITY);
        if(theTeam == null)
            return null;

        for(Player poss: theTeam.getMembers().getLivePlayers()){
            if(poss.getAbilities().getNightAbilities(poss).getNonpassive().filterHasCharge().isEmpty()){
                killer = poss;
                mafKiller.put(theTeam.getColor(), killer);
                return killer;
            }
        }
        killer = random.getPlayer(theTeam.getMembers().getLivePlayers());
        mafKiller.put(theTeam.getColor(), killer);
        return killer;
    }

    public Player getMafKillTarget(Player slave) {
        Player killTarget = mafKillTarget.get(slave.getColor());
        if(killTarget != null)
            return killTarget;

        PlayerList aliveTeammates = slave.getFaction().getMembers().getLivePlayers();
        PlayerList alive = slave.narrator.getAllPlayers().getLivePlayers();

        PlayerList choices = alive.compliment(aliveTeammates);

        PlayerList outedChoices = getHighProfilePeople(slave).getLivePlayers();
        if(outedChoices.isEmpty() || choices.intersect(outedChoices).isEmpty()
                || choices.compliment(outedChoices).isEmpty())
            killTarget = random.getPlayer(choices);
        if(ballsyMaf >= random.nextDouble() && killTarget == null){
            killTarget = random.getPlayer(choices.intersect(outedChoices));
        }else if(killTarget == null){
            killTarget = random.getPlayer(choices.compliment(outedChoices));
        }
        if(!slave.getFaction().hasAbility(FactionKill.MAIN_ABILITY))
            killTarget = null;
        else if(slave.getFaction().getAbility(FactionKill.MAIN_ABILITY).isOnCooldown())
            killTarget = null;

        // killTarget = random.getPlayer(alive.compliment(aliveTeammates));
        mafKillTarget.put(slave.getColor(), killTarget);
        return killTarget;
    }

    public PlayerList getHighProfilePeople(Player slave) {
        PlayerList profiles = new PlayerList();
        if(mayor != null)
            profiles.add(mayor);
        PlayerList teammates = slave.getFaction().getMembers();
        for(Claim c: claims){
            if(!c.getAccused().intersect(teammates).isEmpty()){
                if(!teammates.contains(c.getClaimer()))
                    profiles.add(c.getClaimer());
            }

        }
        return profiles;

    }

    public static void EndGame(Narrator n, long seed) {
        EndGame(n, seed, null, -1);
    }

    public static boolean EndGame(Narrator n, long seed, Integer endGameDay, int maximumRunningTime) {
        Random r = new Random();
        r.setSeed(seed);
        r.reset();
        Brain b = new Brain(n, r);
        b.talkingEnabled = false;
        return b.endGame(endGameDay, maximumRunningTime);
    }

    public void endGame() {
        endGame(null, -1);
    }

    public boolean endGame(Integer endDay, int timeToEnd) {
        if(slaves.isEmpty())
            return false;
        Narrator n = slaves.get(0).narrator;
        if(!n.isStarted())
            n.startGame();// predictable role assignment
        long startTime = System.currentTimeMillis();
        while (n.isInProgress() && (endDay == null || n.getDayNumber() < endDay)
                && (timeToEnd < 0 || timeToEnd >= System.currentTimeMillis() - startTime)){
            if(n.phase == GamePhase.ROLE_PICKING_PHASE)
                n.endPhase();
            if(n.isDay()){
                if(n.phase == GamePhase.DISCUSSION_PHASE)
                    n.endPhase();
                dayAction();
            }else
                nightAction();
        }
        return n.isInProgress();
    }

    public void endDay() {
        if(slaves.isEmpty())
            return;
        Narrator n = slaves.get(0).narrator;
        while (n.isDay())
            dayAction();
    }

    public Claim claim(Claim c) {
        if(!c.getClaimer().isSilenced()){
            claims.add(c);
            return c;
        }
        return null;
    }

    public ArrayList<PlayerList> getSuspiciousPeople(Player bot) {

        Faction botFaction = bot.getFaction();
        ArrayList<PlayerList> ret = suspList.get(botFaction);
        if(ret != null)
            return ret;
        ret = new ArrayList<>();

        Narrator narrator = bot.narrator;

        HashMap<Player, Integer> scores = new HashMap<Player, Integer>();
        for(Player p: narrator.getLivePlayers()){
            scores.put(p, 0);
        }

        for(Player dead: narrator.getDeadPlayers()){
            DeathType dt = dead.getDeathType();
            if(!dt.isLynch() || dt.isCleaned())
                continue;
            PlayerList lynch = dt.getLynchers().getLivePlayers();
            Faction deadTeam = dead.getFaction();

            if(botFaction.isEnemy(deadTeam)){// if person voted for my enemy, i think more highly of them
                for(Player lyncher: lynch){
                    increment(scores, lyncher);
                }
                for(Player notLyncher: narrator.getLivePlayers().remove(lynch)){
                    decrement(scores, notLyncher);
                }
            }else{
                for(Player lyncher: lynch){
                    decrement(scores, lyncher);
                }
                for(Player notLyncher: narrator.getLivePlayers().remove(lynch)){
                    increment(scores, notLyncher);
                }
            }
        }
        // this gives me a list of all the different types of ratings that exist
        ArrayList<Integer> ratings = new ArrayList<>();
        for(Integer rating: scores.values()){
            if(!ratings.contains(rating))
                ratings.add(rating);
        }

        if(ratings.size() == 1){
            ret.add(narrator.getLivePlayers());
        }else{

            // put them
            Collections.sort(ratings);
            Collections.reverse(ratings);

            // fill the return
            for(int i = 0; i < ratings.size(); i++){
                ret.add(new PlayerList());
            }

            for(Player p: narrator.getLivePlayers()){
                Integer rating = scores.get(p);
                int index = ratings.indexOf(rating);
                ret.get(index).add(p);
            }
            suspList.put(botFaction, ret);
        }
        return ret;
    }

    private void increment(HashMap<Player, Integer> map, Player p) {
        map.put(p, map.get(p) + 1);
    }

    private void decrement(HashMap<Player, Integer> map, Player p) {
        map.put(p, map.get(p) - 1);
    }

    public boolean gridlock() {
        int greatDeathDay = 0;
        Narrator n = slaves.get(0).narrator;
        for(Player p: n.getDeadPlayers()){
            greatDeathDay = Math.max(greatDeathDay, p.getDeathDay());
        }
        if(n.getDayNumber() - greatDeathDay > 10){
            return true;
        }
        return false;
    }

    Player mayor = null; // TODO DELETE there shouldn't be a singular mayor

    private ArrayList<String> safeColors;

    public ArrayList<String> getSafeColors(Narrator n) {
        if(safeColors == null)
            resetSafeColors(n);
        if(safeColors.size() == 0)
            resetSafeColors(n);
        return safeColors;
    }

    public Player getRevealedMayor() {
        return mayor;
    }

    public RoleClaim getRoleClaim(Player slave) {
        RoleClaim rc;
        for(Claim c: claims){
            if(c instanceof RoleClaim){
                rc = (RoleClaim) c;
                if(rc.getClaimer() == slave)
                    return rc;
            }

        }
        return null;
    }

    void resetSafeColors(Narrator n) {
        safeColors = new ArrayList<String>();

        HashMap<String, Integer> memberSizes = new HashMap<>();
        for(Faction t: n.getFactions()){
            memberSizes.put(t.getColor(), 0);
        }

        for(RoleGroup rt: publicRoles){
            for(String color: rt.getColors())
                memberSizes.put(color, memberSizes.get(color) + 1);
        }

        ArrayList<Faction> teams = n.getFactions();

        Faction t1, t2;
        int rating;
        for(int i = 0; i < teams.size(); i++){
            t1 = teams.get(i);
            if(memberSizes.get(t1.getColor()) == 0)
                continue;
            rating = 0;
            for(int j = 0; j < teams.size(); j++){
                t2 = teams.get(j);

                if(t1.isEnemy(t2)){
                    rating -= memberSizes.get(t2.getColor());
                }else{
                    rating += memberSizes.get(t2.getColor());
                }
            }

            if(rating >= 0)
                safeColors.add(t1.getColor());
        }
    }

    public static void saveEvents(Narrator n) {
        BrowserEvents(n, "text.html");
    }

    public static void BrowserEvents(Narrator n, String to) {
        String events = "<style> body{background-color: black; color: white}></style>";
        events += n.getEventManager().getEvents(Message.PRIVATE).access(Message.PRIVATE, new HTMLDecoder());
        events = events.replaceAll("\n", "<br>");
        PrintWriter writer = null;
        try{
            writer = new PrintWriter(to, "UTF-8");
            writer.println(events);
            writer.close();
        }catch(FileNotFoundException | UnsupportedEncodingException f){
            f.printStackTrace();
        }

    }

    public RoleGroupList publicRoles;

    void setPublicRoles(Narrator narrator) {
        publicRoles = new RoleGroupList();
        for(SetupHidden setupHidden: narrator.rolesList)
            publicRoles.add(new RoleGroup().add(setupHidden.hidden));
    }

    public void crossOff(PlayerList pList) {
        if(pList != null)
            for(Player player: pList)
                publicRoles.crossOff(player);
        for(Computer computer: computers.values()){
            if(computer.player.isAlive())
                computer.setRolesList();
        }
    }

    int lastDeath = -1;

    public int count(FactionRole a) {
        int count = 0;
        for(RoleGroup roleGroup: publicRoles){
            if(roleGroup.contains(a))
                count++;
        }
        return count;
    }

    public void prettyPrintRoles() {

    }

//	public boolean isAtHammer(Player slave, Player choice) {
//		int minLynchVote = getMinLynchVote();
//		int votes = choice.getVoteCount();
//		if(choice.getVoters().contains(slave)){
//			return false;
//		}
//		return votes + slave.getVotePower() >= minLynchVote;
//	}
    public ArrayList<Claim> getClaims() {
        ArrayList<Claim> claims = new ArrayList<>();
        for(Claim c: this.claims){
            claims.add(c);
        }
        return claims;
    }

    public static ControllerList Vote(Brain brain, Player target, String message) {
        ControllerList voters = new ControllerList();
        Computer c;
        int day = target.narrator.getDayNumber();
        for(Player p: brain.slaves.getLivePlayers()){
            if(target == p)
                continue;
            if(p.narrator.isNight())
                break;
            if(day != target.narrator.getDayNumber())
                break;
            if(target.isDead())
                break;
            c = brain.computers.get(p);
            if(c == null)
                continue;
            Player voteTarget = c.player.narrator.voteSystem.getVoteTarget(c.player);
            if(c.player.isDisenfranchised()){
                if(!c.player.getSkipper().equals(voteTarget))
                    voters.add(c.controller.skipVote());
                continue;
            }
            if(c.player.isPuppeted() && brain.computers.get(c.player.getPuppeteer()) != null){
                Computer ventro = brain.computers.get(c.player.getPuppeteer());
                if(message != null && brain.talkingEnabled)
                    ventro.controller.say(message, c.player.getName());
                if(voteTarget != target){
                    ventro.controller.ventVote(c.player, target);
                }
            }else{
                if(message != null && brain.talkingEnabled && !c.player.isSilenced())
                    c.controller.say(message, Constants.DAY_CHAT);
                if(voteTarget != target)
                    voters.add(c.controller.vote(target));
            }

        }
        return voters;
    }

    public static void Unvote(Brain brain) {
        Computer c;
        for(Player p: brain.slaves.getLivePlayers()){
            c = brain.computers.get(p);
            if(!p.narrator.isDay() || c == null)
                continue;
            Player voteTarget = c.player.narrator.voteSystem.getVoteTarget(c.player);
            if(c.player.isPuppeted() && brain.computers.get(c.player.getPuppeteer()) != null){
                Computer ventro = brain.computers.get(c.player.getPuppeteer());
                if(voteTarget != null){
                    ventro.controller.ventUnvote(c.player);
                }
            }else{
                if(voteTarget != null)
                    c.controller.unvote();
            }

        }
    }

    public static void SkipDay(Brain b, Narrator narrator) {
        if(narrator.isDay()){
            for(Player slave: b.slaves){
                if(!slave.narrator.isDay())
                    return;// voters;
                if(slave.isDead() && slave.is(Ghost.class) && slave.hasPuppets()){
                    b.computers.get(slave).skipVoteAssist();
                    continue;
                }
                if(slave.isDead())
                    continue;
                b.computers.get(slave).skipVoteAssist();

            }
        }
        if(narrator.isDay()){
            throw new PhaseException();
        }
    }

}
