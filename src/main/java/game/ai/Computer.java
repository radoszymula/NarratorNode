package game.ai;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import game.ai.roles.AIStripper;
import game.ai.roles.AIWitch;
import game.event.DayChat;
import game.event.Feedback;
import game.logic.Faction;
import game.logic.FactionList;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.Amnesiac;
import game.roles.BreadAbility;
import game.roles.Burn;
import game.roles.Detective;
import game.roles.Douse;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.Executioner;
import game.roles.FactionKill;
import game.roles.Framer;
import game.roles.GunAbility;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Jester;
import game.roles.Lookout;
import game.roles.Mayor;
import game.roles.ProxyKill;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.Spy;
import game.roles.Stripper;
import game.roles.Tailor;
import game.roles.Undouse;
import game.roles.VestAbility;
import game.roles.Veteran;
import game.roles.support.Gun;
import models.FactionRole;

public class Computer {

    public static final String NAME = "Slave";
    public static final int NUM_PLAYERS = 10;

    public Player player;
    public Brain brain;
    public Controller controller;
    protected Narrator narrator;

    public List<Claim> badClaims;
    public List<Feedback> feedbackMessages;

    public Computer(Player slave, Brain brain, Controller controller) {
        this.player = slave;
        this.brain = brain;
        this.controller = controller;
        this.narrator = slave.narrator;
        memory = new Memory();
        badClaims = new ArrayList<>();
        feedbackMessages = new LinkedList<>();
    }

    public Computer(Player slave, Brain brain) {
        this(slave, brain, slave);
    }

    public static final String STRIPPER = Stripper.class.getSimpleName();
    public static final String WITCH = Stripper.class.getSimpleName();

    public boolean doRoleNightAction(String command) {
        command = toTitleCase(command);
        switch (command){
        case "Stripper":
            AIStripper.doRoleNightAction(this);
            return true;
        case "Witch":
            AIWitch.doRoleNightAction(this);
            return true;
        default:
            return false;
        }
    }

    private static String toTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for(char c: input.toCharArray()){
            if(Character.isSpaceChar(c)){
                nextTitleCase = true;
            }else if(nextTitleCase){
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();
    }

    public boolean useVest() {
        return false;
    }

    public boolean useGun() {
        return false;
    }

    public boolean mafKilling() {
        if(brain.getMafSender(player) != player)
            return false;
        PlayerList pl = player.narrator.getAllPlayers();
        pl.remove(player);
        if(pl.isEmpty())
            return false;
        for(Player player: player.getFaction().getMembers()){
            if(pl.size() == 1)
                break;
            pl.remove(player);
        }
        mafKillAbility(FactionKill.COMMAND, pl);
        return true;
    }

    public PlayerList getFactionmates() {
        Faction faction = player.getFaction();
        if(!faction.knowsTeam())
            return new PlayerList();
        return faction.getMembers();

    }

    public void doNightAction() {
        submitNightTargets();
        controller.endNight();
    }

    private void submitNightTargets() {
        if(player.endedNight())
            controller.cancelEndNight();

        if(player.isJailed())
            return;

        if(useGun()){
            return;
        }
        if(useVest()){
            return;
        }
        if(mafKilling()){
            return;
        }

        for(String command: player.getCommands())
            if(doRoleNightAction(command))
                return;

        // if(slave.getTeam().knowsTeam() && !slave.isBlackmailed())
        // controller.say(slave, "hi", slave.getTeam().getName());
        PlayerList selections = player.narrator.getLivePlayers();

        ArrayList<String> commands = player.getCommands();

        // Collections.reverse(commands);

        for(String command: commands){
            if(player.getAbility(command) == null){
                player.getAbility(command);
            }
            if(!player.getAbility(command).isNightAbility(player))
                continue;

            if(player.getAbility(command).getPerceivedCharges() == 0)
                continue;

            if(mafSendAbility(command))
                continue;

            if(!player.getActions().canAddAnotherAction(player.getAbility(command).getAbilityNumber()))
                break;

            // if(slave.getAbility(ability).getPerceivedCharges() != 0){
            if(arsonAbility(command, selections))
                continue;

            if(janitorAbility(command))
                continue;
            if(proxyKillingAbility(command))
                continue;
            if(driverAbility(command))
                continue;
            if(tailorAbility(command, narrator))
                continue;

            if(skAbility(command))
                continue;

            // }
            if(VestAbility.COMMAND.equalsIgnoreCase(command)){
                controller.vest();
                continue;
            }

            if(vigiAbility(command))
                continue;

            if(mafKillAbility(command, selections))
                continue;

            Player choice;

            if(player.is(Veteran.class) && Veteran.COMMAND.equalsIgnoreCase(command)){
                if(player.getAbility(Veteran.COMMAND).getPerceivedCharges() != 0)
                    controller.setNightTarget(Veteran.COMMAND, player);
                return;
            }else if(player.is(Spy.class) && Spy.COMMAND.equalsIgnoreCase(command)){
                choice = player;
            }else if(player.is(Amnesiac.class) && Amnesiac.COMMAND.equalsIgnoreCase(command))
                choice = getTargetChoice(command, selections.getDeadPlayers());
            else
                choice = getTargetChoice(command, selections);

            if(choice == null)
                continue;

            if(framerMainAbility(command, choice))
                continue;

            if(drugDealerMainAbility(command, choice))
                continue;

            if(spyMainAbility(command))
                continue;

            try{
                controller.setNightTarget(command, choice);
            }catch(PlayerTargetingException e){
                e.printStackTrace();
            }
        }
        arsonAction = false;
    }

    private boolean arsonAction = false;

    private boolean arsonAbility(String ability, PlayerList choices) {
        if(!player.is(Burn.class) || player.is(Undouse.class) || player.is(Douse.class))
            return false;

        boolean arsonAbility = (!Burn.COMMAND.equalsIgnoreCase(ability) && !Douse.COMMAND.equalsIgnoreCase(ability)
                && !Undouse.COMMAND.equalsIgnoreCase(ability));
        arsonAbility = !arsonAbility;

        // if arson already performed an arson action and this is an arson ability
        if(arsonAction)
            return arsonAbility;

        // if arson didn't perform their action yet and this isn't an arson ability
        if(!arsonAbility)
            return false;

        arsonAction = true;

        int choice = brain.random.nextInt(10);
        if(choice == 0 && !narrator.isFirstNight())
            controller.setNightTarget(Undouse.COMMAND, player);
        else if(choice <= 3 && player.isAcceptableTarget(new Action(player, Ability.BURN, player))
                && !narrator.isFirstNight()){
            controller.setNightTarget(Burn.COMMAND);
            // lastBurned = narrator.getDayNumber();
        }else{
            PlayerList prevDouses = new PlayerList();
            for(int i = 0; i < player.narrator.getDayNumber(); i++){
                ActionList prevs = player.getPrevNightTarget(i);
                if(prevs == null)
                    continue;
                PlayerList prevD = prevs.getTargets(Douse.COMMAND);
                if(prevD != null)
                    prevDouses.add(prevD);
            }
            choices = choices.copy().remove(player);
            choices.remove(prevDouses);
            if(choices.isEmpty()){
                controller.setNightTarget();
                // lastBurned = narrator.getDayNumber();
            }else
                controller.setNightTarget(Douse.COMMAND, brain.random.getPlayer(choices));
        }
        return true;
    }
    // private int lastBurned = 0;

    private boolean framerMainAbility(String command, Player choice) {
        if(player.is(Framer.class) && Framer.COMMAND.equalsIgnoreCase(command)){
            int teamChoiceSize = player.narrator.getFactions().size();
            int random = brain.random.nextInt(teamChoiceSize);
            String teamName = choice.narrator.getFactions().get(random).getName();
            controller.setNightTarget(Framer.COMMAND, teamName, choice);
            return true;
        }
        return false;
    }

    private boolean drugDealerMainAbility(String command, Player choice) {
        if(player.is(DrugDealer.class) && DrugDealer.COMMAND.equalsIgnoreCase(command)){
            controller.setNightTarget(DrugDealer.COMMAND, DrugDealer.WIPE, choice);
            return true;
        }
        return false;
    }

    private boolean spyMainAbility(String command) {
        if(player.is(Spy.class) && Spy.COMMAND.equalsIgnoreCase(command)){
            for(Option t: player.getOptions().get(command)){
                controller.spy(t.getValue());
                return true;
            }
            return true;
        }
        return false;
    }

    private boolean vigiAbility(String ability) {
        if(Ability.is(GunAbility.MAIN_ABILITY, ability)){
            PlayerList choices = player.narrator.getLivePlayers().remove(player);
            choices.remove(brain.getRevealedMayor());
            if(player.narrator.isNight() && player.getFaction().knowsTeam())
                choices.remove(player.getFaction().getMembers());

            PlayerList temp;
            ArrayList<PlayerList> suspicious = brain.getSuspiciousPeople(player);// returned to you by the least
                                                                                 // suspicious people to most
            for(PlayerList susp: suspicious){
                if(choices.size() <= 1)
                    break;
                temp = choices.copy();
                temp.remove(susp);
                if(!temp.isEmpty())
                    choices = temp;
            }
            if(!choices.isEmpty())
                controller.setNightTarget(Gun.COMMAND, brain.random.getPlayer(choices));
            return true;
        }
        return false;
    }

    private boolean skAbility(String command) {
        if(player.is(SerialKiller.class) && SerialKiller.COMMAND.equalsIgnoreCase(command)){
            PlayerList choices = player.narrator.getLivePlayers().remove(player);
            if(player.getFaction().knowsTeam())
                choices.remove(player.getFaction().getMembers());
            controller.setNightTarget(SerialKiller.COMMAND, brain.random.getPlayer(choices));
            return true;
        }
        return false;
    }

    private boolean driverAbility(String command) {
        if(player.is(Driver.class) && Ability.is(Driver.COMMAND, command)){
            PlayerList choices = player.narrator.getLivePlayers();
            Player p1 = brain.random.getPlayer(choices);
            Player p2 = brain.random.getPlayer(choices.remove(p1));
            if(p1 != null && p2 != null)
                controller.setNightTarget(Driver.COMMAND, p1, p2);
            return true;
        }
        return false;
    }

    private boolean proxyKillingAbility(String command) {
        if(player.is(ProxyKill.class) && Ability.is(ProxyKill.COMMAND, command)){
            PlayerList choices = player.narrator.getLivePlayers();
            Player p1 = player;
            Player p2 = brain.random.getPlayer(choices.remove(player.getFaction().getMembers()));
            if(p1 != null && p2 != null)
                controller.setNightTarget(ProxyKill.COMMAND, p1, p2);
            return true;
        }
        return false;
    }

    private boolean tailorAbility(String command, Narrator narrator) {
        if(player.is(Tailor.class) && Tailor.COMMAND.equalsIgnoreCase(command)){
            PlayerList choices = player.narrator.getLivePlayers();
            if(!player.getAbility(Tailor.class).canSelfTarget(narrator))
                choices.remove(player);
            Player p1 = brain.random.getPlayer(choices);
            if(p1 != null)
                controller.setNightTarget(Tailor.COMMAND, player.getInitialColor(), player.role.name, p1);
            return true;
        }
        return false;
    }

    private boolean janitorAbility(String command) {
        if(Ability.is(command, Janitor.MAIN_ABILITY) && player.hasAbility(command)){
            if(brain.getMafSender(player) != player){
                Player killTarget = brain.getMafKillTarget(player);
                if(killTarget == null)
                    return true;
                if(killTarget.isDead()){
                    brain.reset();
                    killTarget = brain.getMafKillTarget(player);
                    if(killTarget == null || killTarget.isDead())
                        return true;
                }
                controller.setNightTarget(Janitor.COMMAND, killTarget);
            }
            return true;
        }
        return false;
    }

    private boolean mafSendAbility(String command) {
        if(Faction.SEND.equalsIgnoreCase(command)){
            Player sender = brain.getMafSender(player);
            if(sender == null || sender.isDead())
                brain.reset();
            sender = brain.getMafSender(player);
            if(sender == player || !player.isSilenced() && sender != null){ // if not blackmailed, they can choose
                                                                            // whoever they want. if they are, they'll
                                                                            // shut up if its not themselves
                controller.setNightTarget(Faction.SEND, sender);

            }
            return true;
        }

        return false;
    }

    private boolean mafKillAbility(String command, PlayerList selections) {
        if(FactionKill.COMMAND.equalsIgnoreCase(command) && player.hasAbility(command)){
            selections = selections.copy();
            selections.remove(player.getFaction().getMembers());
            if(brain.getMafSender(player) != player)
                return false;

            Player killTarget = brain.getMafKillTarget(player);
            if(killTarget == null)
                return true;

            if(killTarget.isDead()){
                brain.reset();
                killTarget = brain.getMafKillTarget(player);
            }

            // stops situation where disguiser is part of the team, and mafia don't know who
            // to kill.
            int dayNumber = player.narrator.getDayNumber();
            ActionList prevTargets = player.getPrevNightTarget(dayNumber - 1);
            ActionList prevprevTargets = player.getPrevNightTarget(dayNumber - 2);
            if(prevTargets != null && prevprevTargets != null){
                if(prevTargets.isTargeting(killTarget, FactionKill.MAIN_ABILITY)
                        && prevprevTargets.isTargeting(killTarget, FactionKill.MAIN_ABILITY))
                    killTarget = player.narrator.getLivePlayers().remove(player).getRandom(brain.random);
            }

            controller.setNightTarget(FactionKill.COMMAND, killTarget);
            if(!player.isSilenced() && brain.talkingEnabled)
                controller.say("I will kill " + killTarget.getName(), player.getFaction().getColor());

        }
        return true;

    }

    private Player getTargetChoice(String command, PlayerList possible) {
        Ability a = player.getAbility(command);
        if(a == null || a.getPerceivedCharges() == 0)
            return null;
        Player selection = brain.random.getPlayer(possible);
        if(selection == null)
            return null;

        Option option;
        if(player.getOptions(command).isEmpty())
            option = null;
        else
            option = player.getOptions().get(command).get(0);

        if(player.isAcceptableTarget(new Action(player, Player.list(selection),
                player.getAbility(command).getAbilityNumber(), option, null, null))){
            selection = player.narrator.getPlayerByName(selection.getName());
            return selection;
        }
        return getTargetChoice(command, possible.remove(selection));
    }

    private boolean noPrevNight() {
        Narrator n = player.narrator;
        if(n.getDayNumber() == 1 && n.getBool(SetupModifier.DAY_START))
            return true;
        return false;
    }

    void talkings() {
        if(player.is(Mayor.class) && !player.isPuppeted() && !player.isSilenced()){
            for(String comm: player.getDayCommands())
                controller.doDayAction(comm, null);
            brain.mayor = player;
        }
        if(player.is(Jailor.class) && !player.isPuppeted()){
            int jailedPeople = 0;
            for(Player targets: brain.slaves.getLivePlayers().shuffle(brain.random, null)){
                if(jailedPeople >= BreadAbility.getUseableBread(player) + 1){
                    break;
                }
                if(targets == this.player)
                    continue;
                controller.doDayAction(Jailor.JAIL, null, targets);
                jailedPeople++;
            }
        }
        if(player.isSilenced() || player.isPuppeted())
            return;
        if(noPrevNight())
            return;
        if(player.is(Detective.class))
            detectiveTalk();
        else if(player.is(Lookout.class))
            lookoutTalk();
        else if(player.is(Sheriff.class)){
            sheriffTalk();
        }else if(player.is(Executioner.class)){
            exeuctionerTalk();
        }else if(player.is(Jester.class)){
            jesterTalk();
        }

    }

    private void fakeSheriffClaim(Player target) {
        Faction sheriffDetectableTeam = null;
        Set<String> detected;
        Iterator<String> iterator;
        int random;
        for(Faction t: narrator.getPossibleMembers().getFactions(Sheriff.class, narrator)){
            detected = t.getSheriffDetectables();
            if(detected.isEmpty())
                continue;
            random = brain.random.nextInt(detected.size());
            iterator = detected.iterator();
            while (random != 0){
                iterator.next();
                random--;
            }
            sheriffDetectableTeam = narrator.getFaction(iterator.next());
            sheriffClaim(new PlayerList(target), sheriffDetectableTeam);
            return;
        }
    }

    private void exeuctionerTalk() {
        Executioner ex = player.getAbility(Executioner.class);
        Player target = ex.getTarget();
        if(target.isDead())
            return;
        fakeSheriffClaim(target);
    }

    private void jesterTalk() {
        Player randAccused = brain.random.getPlayer(player.narrator.getLivePlayers());
        fakeSheriffClaim(randAccused);
    }

    private void sheriffClaim(PlayerList targets, Faction team) {
        sheriffClaim(targets, new FactionList(team));
    }

    private void sheriffClaim(PlayerList targets, FactionList team) {
        if(player.isSilenced() || player.isPuppeted())
            return;

        String say = "I'm " + player.getRoleName() + ". ";
        say = say + targets.getStringName() + " is ";
        if(team == null)
            say += "not suspicious.";
        else
            say += "part of the " + team.getName() + ".";
        if(brain.talkingEnabled)
            controller.say(say, DayChat.KEY);

        SheriffClaim sc = new SheriffClaim(player, targets, team, brain);
        brain.claim(sc);
    }

    private List<Feedback> getFeedback() {
        return this.feedbackMessages;
    }

    private void detectiveTalk() {
        int lastNight = player.narrator.getDayNumber() - 1;
        for(Feedback e: getFeedback()){
            if(e.access(player).startsWith(Detective.FEEDBACK)){
                Player target = e.getPlayers().get(0);
                if(target.isAlive())
                    continue;
                if(target.getDeathType().isLynch())
                    continue;
                ArrayList<String[]> attacks = target.getDeathType().getList();
                if(isEvilAttack(attacks)){
                    PlayerList followed = player.getPrevNightTarget(lastNight).getTargets(Detective.COMMAND);
                    String say = "I'm Detective. " + followed.getStringName() + " may have killed " + target.getName();
                    if(brain.talkingEnabled)
                        controller.say(say, DayChat.KEY);
                    ArrayList<Faction> teams = new ArrayList<>();
                    Narrator n = player.narrator;
                    for(String[] attack: attacks){
                        if(attack == Constants.FACTION_KILL_FLAG){
                            for(Faction t: n.getFactions()){
                                if(t.hasAbility(FactionKill.MAIN_ABILITY))
                                    teams.add(t);
                            }
                        }else if(attack == Constants.SK_KILL_FLAG){
                            for(Faction skTeam: getSKTeams()){
                                teams.add(skTeam);
                            }
                        }
                    }
                    brain.claim(new DetectiveClaim(player, followed, teams, brain));
                }
            }
        }
    }

    private boolean isEvilAttack(ArrayList<String[]> attacks) {
        for(Faction t: player.narrator.getFactions()){
            if(t.hasAbility(FactionKill.MAIN_ABILITY) && attacks.contains(Constants.FACTION_KILL_FLAG)){
                return true;
            }
        }
        if(attacks.contains(Constants.SK_KILL_FLAG))
            return true;
        return false;
    }

    private ArrayList<Faction> getSKTeams() {
        return narrator.getPossibleMembers().getFactions(SerialKiller.class, narrator);
    }

    private void lookoutTalk() {
        int lastNight = player.narrator.getDayNumber() - 1;
        for(Feedback e: getFeedback()){
            if(e.access(player).startsWith(Lookout.FEEDBACK)){
                PlayerList watchedTargets = player.getPrevNightTarget(lastNight).getTargets(Lookout.COMMAND);

                if(!watchedTargets.hasDead())
                    continue;
                // if (watched.getDeathType().isLynch())
                // continue;
                ArrayList<String[]> attacks = new ArrayList<>();
                for(Player watched: watchedTargets)
                    if(watched.isDead())
                        attacks.addAll(watched.getDeathType().getList());
                if(isEvilAttack(attacks)){

                    PlayerList possibleAttackers = e.getPlayers();

                    for(Player dead: watchedTargets){
                        if(dead.isAlive())
                            continue;
                        String say = "I'm Lookout. " + possibleAttackers.getStringName() + " may have killed "
                                + dead.getName();
                        if(brain.talkingEnabled)
                            controller.say(say, DayChat.KEY);

                        ArrayList<Faction> teams = new ArrayList<>();
                        Narrator n = player.narrator;
                        for(String[] attack: attacks){
                            if(attack == Constants.FACTION_KILL_FLAG){
                                for(Faction t: n.getFactions()){
                                    if(t.hasAbility(FactionKill.MAIN_ABILITY))
                                        teams.add(t);
                                }
                            }else if(attack == Constants.SK_KILL_FLAG){
                                for(Faction skTeam: getSKTeams()){
                                    teams.add(skTeam);
                                }
                            }
                        }
                        if(!teams.isEmpty())
                            brain.claim(new LookoutClaim(player, possibleAttackers, teams, brain));
                    }
                }
            }
        }
    }

    private void sheriffTalk() {
        int lastNight = player.narrator.getDayNumber() - 1;

        PlayerList targets = player.getPrevNightTarget(lastNight).getTargets(Sheriff.COMMAND).getLivePlayers();
        if(targets.isEmpty())
            return;

        for(Feedback e: getFeedback()){
            String s = e.access(player).replace("\n", "");
            if(s.startsWith("Your target")){
                boolean friendly = s.startsWith(Sheriff.NOT_SUSPICIOUS);
                FactionList tl;
                if(friendly)
                    tl = null;
                else if(e.getHTStrings().isEmpty()){
                    tl = new FactionList();
                    for(String teamColor: player.getFaction().getEnemyColors())
                        tl.add(player.narrator.getFaction(teamColor));
                }else{
                    tl = new FactionList(player.narrator.getFaction(e.getHTStrings().get(0).getColor()));
                }
                sheriffClaim(targets, tl);
            }

        }
    }

//	public Controller vote(PlayerList choices) throws ClaimInterrupt{
//    	if (slave.isDead() && !slave.is(Ghost.class))
//    		return null;
//
//        if(slave.isDead() && slave.is(Ghost.class) && !slave.hasPuppets())
//        	return null;
//        	
//		Narrator n = slave.narrator;
//    	if (n.isNight() || !n.isInProgress())
//            return null;
//
//        //arson killed himself
//        if (slave.isDead() && !slave.is(Ghost.class))
//            return null;
//        
//        if(slave.isDead() && slave.hasPuppets()){
//        	choices.remove(slave.getPuppets().getFirst());
//        }
//
//		if(slave.isPuppeted())
//			return null;
//		if(slave.isBlackmailed()){
//			if(slave.getVoteTarget() == slave.getSkipper())
//				return null;
//			return skipVoteAssist();
//		}
//		choices = choices.copy();
//		choices.remove(slave);
//		
//		if(choices.isEmpty()){
//			if(slave.getVoteTarget() == slave.getSkipper())
//				return null;
//			if(brain.talkingEnabled)
//				controller.say("I don't have anyone to vote for, so I'm skipping the vote", Constants.DAY_CHAT);
//			skipVoteAssist();
//			Player puppet;
//			for(int i = 0; i < slave.getPuppets().size() && slave.narrator.isDay(); i++){
//				puppet = slave.getPuppets().get(i);
//				if(puppet.getVoteTarget() != slave.getSkipper())
//					controller.ventSkipVote(slave.getPuppets().get(i));
//			}
//			return null;
//		}
//		
//		//if(choices.size() < 3)
//			//controller.say(slave, "These are my choices" + choices , Constants.PUBLIC);
//		
//		
//		if(!brain.gridlock()){
//			if (slave.getTeam().knowsTeam()){
//				choices.softRemove(slave.getTeam().getMembers());
//			}
//		
//			ArrayList<PlayerList> suspicious = brain.getSuspiciousPeople(slave);//returned to you by the least suspicious people to most
//			if(suspicious.get(0).size() == n.getLiveSize() && slave.getVoters().size() >= 2){
//				choices.softIntersect(slave.getVoters());
//			}else{
//				for(PlayerList susp: suspicious){
//					if(choices.size() <= 1)
//						break;
//					choices.softRemove(susp);
//				}
//			}
//		}
//		HashMap<Player, Integer> voteCounts = new HashMap<>();
//		int voteCount;
//		for(Player p: choices){
//			voteCount = p.getVoteCount();
//			if(slave.getVoteTarget() == p)
//				voteCount *= voteCount;
//			if(voteCount > 0)
//				voteCounts.put(p, voteCount);
//		}
//		Player choice = null;
//		final boolean startOwnTrain = brain.random.nextInt(voteCounts.size()) == 0;
//		if(startOwnTrain){
//			choice = brain.random.getPlayer(choices);
//		}else{
//			int totalVoteCount = 0;
//			for(Integer i: voteCounts.values())
//				totalVoteCount += i;
//			
//			int threshold = brain.random.nextInt(totalVoteCount) + (choices.size());
//			
//			for(Player p: voteCounts.keySet()){
//				threshold -= voteCounts.get(p);
//				if(threshold <= 0){
//					choice = p;
//					break;
//				}
//			}
//		}
//		if(choice == null)
//			choice = brain.random.getPlayer(choices);
//		
//		
//		if(slave.getVoteTarget() == choice)
//			return null;
//		Computer choiceComputer = brain.computers.get(choice);
//		if(!brain.isAtHammer(slave, choice) || choiceComputer == null){
//			return voteHelper(choice);
//		}
//		
//		//make them role claim now
//		if(brain.getRoleClaim(choice) != null){
//			return voteHelper(choice);
//		}
//		
//		RoleClaim c = (RoleClaim) choiceComputer.roleClaim();//computer claimed
//		if(c == null)//can't make humans role claim
//			return voteHelper(choice);
//		
//		throw new ClaimInterrupt();
//	}

//	private Controller voteHelper(Player choice){
//		if(slave.is(Ghost.class) && slave.isDead() && slave.hasPuppets()){
//			if(choice.equals(slave.getPuppets().getFirst().getVoteTarget()))
//				return null;
//			return controller.ventVote(slave.getPuppets().getFirst(), choice);
//		}
//		return controller.vote(choice);
//	}

    Player skipVoteAssist() {
        Player voteTarget = player.narrator.voteSystem.getVoteTarget(player);
        if(player.isAlive() && voteTarget != player.getSkipper() && !player.isPuppeted())
            controller.skipVote();

        Player puppet;
        for(int i = 0; i < player.getPuppets().size() && player.narrator.isDay(); i++){
            puppet = player.getPuppets().get(i);
            voteTarget = player.narrator.voteSystem.getVoteTarget(puppet);
            if(voteTarget != puppet.getSkipper() && puppet.getPuppeteer() == player)
                brain.computers.get(player).controller.ventSkipVote(puppet);
        }
        return null;
    }

    public Controller vote(Controller choice) {
        Controller ret = controller.vote(choice);
        for(int i = 0; i < player.getPuppets().size() && player.narrator.isDay(); i++){
            if(!choice.getPlayer(player.narrator).isDisenfranchised() && player.getPuppets().get(i) != choice)
                controller.ventVote(player.getPuppets().get(i), choice);
        }

        return ret;
    }

    public Claim roleClaim() {
        if(player.is(Jester.class) && player.getFaction().getEnemyColors().isEmpty())
            return null;
        Claim c = brain.getRoleClaim(player);
        if(c != null)
            return c;

        if(player.isPuppeted())
            return null;

        ArrayList<String> safeColors = brain.getSafeColors(player.narrator);

        RoleClaim roleClaim;
        if(safeColors.isEmpty() || safeColors.contains(player.getFaction().getColor())){ // this means your color is a
                                                                                         // safe one to claim
            roleClaim = new RoleClaim(player, player.factionRole, brain);
        }else{
            int i = brain.random.nextInt(safeColors.size());
            String safeColor = safeColors.get(i);
            FactionRole safeRole = getSafeRole(player.narrator, safeColor);
            roleClaim = new RoleClaim(player, safeRole, brain);
        }
        c = brain.claim(roleClaim);
        if(c != null){
            String roleName = roleClaim.role.getName();
            String teamName = roleClaim.faction.getName();
            if(brain.talkingEnabled)
                controller.say("Don't " + player.narrator.getAlias(Narrator.VOTE_KILL_ALIAS) + " me. My role is "
                        + roleName + " and I'm part of the " + teamName + ".", Constants.DAY_CHAT);
        }
        return c;
    }

    private FactionRole getSafeRole(Narrator narrator, String safeColor) {
        return narrator.getFaction(safeColor)._roleSet.values().iterator().next();
    }

    public static String toLetter(int num) {
        String result = "";
        while (num > 0){
            num--; // 1 => a, not 0 => a
            int remainder = num % 26;
            char digit = (char) (remainder + 65);
            result = digit + result;
            num = (num - remainder) / 26;
        }

        return result;
    }

    public RoleGroupList rolesList;
    public Memory memory;

    public class Memory {

        public PlayerList rPlayers;

        public Memory() {
            rPlayers = new PlayerList();
        }

        public void add(Player p) {
            rPlayers.add(p);
        }

        public void add(PlayerList list) {
            rPlayers.add(list);

        }
    }

    public void setRolesList() {
        rolesList = brain.publicRoles.copy();
        rolesList.crossOff(player.factionRole);
    }

    @Override
    public String toString() {
        return player.toString();
    }

    public PlayerList[] resolveBadClaims() {
        badClaims = null;
        ArrayList<Claim> claims = brain.getClaims();
        if(rolesList == null)
            setRolesList();
        RoleGroupList rolesList = this.rolesList.copy();

        PlayerList accused = new PlayerList(), safe = new PlayerList();
        boolean hasBadClaims = false;
        for(Claim c: claims){
            if(!c.valid(rolesList, this)){
                hasBadClaims = true;
                break;
            }
            accused.add(c.getPlayersToActOn(this));
            safe.add(c.getSafePlayers(this));
        }

        if(!hasBadClaims){
            return new PlayerList[] {
                    accused, safe
            };
        }

        accused = new PlayerList();
        safe = new PlayerList();

        Claim claimInQuestion;
        boolean claimIsOkay;
        for(int i = 0; i < claims.size(); i++){
            claimInQuestion = claims.remove(i);
            claimIsOkay = false;
            rolesList = this.rolesList.copy();

            for(Claim c: claims){
                if(!c.valid(rolesList, this)){
                    claimIsOkay = true; // this is because we still have a conflicting claim, and this one isn't being
                                        // considered.
                    safe.add(c.getSafePlayers(this));
                    break;
                }
            }

            if(claimIsOkay){
                i--;
            }else{
                claims.add(i, claimInQuestion);
                accused.add(claimInQuestion.getClaimer());
            }
        }

        badClaims = claims;
        return new PlayerList[] {
                accused, safe
        };
    }

    public void talkAboutBadClaims() {
        if(badClaims == null || badClaims.isEmpty())
            return;
        if(player.isSilenced() || !brain.talkingEnabled)
            return;

        for(Claim bClaim: badClaims){
            bClaim.talkAboutBadClaim(this);
        }
    }

}
