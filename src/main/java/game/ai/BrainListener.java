package game.ai;

import game.event.DeathAnnouncement;
import game.event.EventList;
import game.event.Feedback;
import game.event.Message;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.templates.PartialListener;
import game.roles.Thief;

public class BrainListener extends PartialListener {

    private Brain brain;
    private Narrator narrator;

    public BrainListener(Brain brain) {
        this.brain = brain;
        this.narrator = brain.narrator;
    }

    // not an Override function
    public void onFirstPhaseStart() {
        brain.setPublicRoles(narrator);
        for(Computer c: brain.computers.values()){
            if(c.player.isAlive())
                c.setRolesList();
        }
    }

    @Override
    public void onAssassination(Player assassin, Player target, DeathAnnouncement e) {
        if(target == null)
            return;
        brain.crossOff(e.dead);
        brain.resetSafeColors(narrator);
        brain.lastDeath = narrator.getDayNumber();
    }

    @Override
    public void onDayBurn(Player arson, PlayerList burned, DeathAnnouncement e) {
        brain.crossOff(burned);
        brain.resetSafeColors(narrator);
        if(burned != null && !burned.isEmpty())
            brain.lastDeath = narrator.getDayNumber();
    }

    @Override
    public void onDayPhaseStart(PlayerList newDead) {
        if(narrator.isFirstPhase() && !narrator.rolesList.contains(Thief.class))
            onFirstPhaseStart();

        brain.crossOff(newDead);
        brain.resetSafeColors(narrator);
        if(newDead != null && !newDead.isEmpty())
            brain.lastDeath = narrator.getDayNumber();
    }

    @Override
    public void onElectroExplosion(PlayerList deadPeople, DeathAnnouncement explosion) {
        brain.crossOff(deadPeople);
        brain.resetSafeColors(narrator);
        brain.lastDeath = narrator.getDayNumber();
    }

    @Override
    public void onGameStart() {
        for(Player player: narrator.getAllPlayers()){
            if(!brain.computers.containsKey(player)){
                brain.slaves.add(player);
                brain.computers.put(player, new Computer(player, brain));
            }
        }
    }

    @Override
    public void onMessageReceive(Player player, Message message) {
        if(brain.computers.containsKey(player) && message instanceof Feedback)
            brain.computers.get(player).feedbackMessages.add((Feedback) message);
    }

    @Override
    public void onModKill(PlayerList bad) {
        brain.crossOff(bad);
        brain.resetSafeColors(narrator);
    }

    @Override
    public void onNightPhaseStart(PlayerList lynched, PlayerList poisoned, EventList e) {
        if(narrator.isFirstPhase() && !narrator.rolesList.contains(Thief.class))
            onFirstPhaseStart();

        brain.crossOff(lynched);
        brain.resetSafeColors(narrator);
        if(lynched != null && !lynched.isEmpty() && !lynched.contains(narrator.skipper))
            brain.lastDeath = narrator.getDayNumber();
    }

    @Override
    public void onRolepickingPhaseStart() {
        onFirstPhaseStart();
    }
}
