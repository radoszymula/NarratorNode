package game.ai;

import java.util.ArrayList;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.roles.Detective;

public class DetectiveClaim extends Claim {

    private ArrayList<Faction> teams;
    private PlayerList accused;
    private int claimDay;

    public DetectiveClaim(Player prosecutor, PlayerList accused, ArrayList<Faction> teams, Brain b) {
        super(prosecutor, b);
        this.teams = teams;
        this.accused = accused;
        claimDay = prosecutor.narrator.getDayNumber() - 1;// because this happened during the night
    }

    public PlayerList getAccused() {
        return accused.copy();
    }

    public boolean believable(Computer c) {
        if(!accused.hasLiving())
            return false;

        if(c.player == getClaimer())
            return true;

        Narrator n = c.player.narrator;
        // shouldn't be believed if the day of the feedback < death of possible visitor
        // day.
        // for example, if detect claimed day 5 that someone visited dead killed by sk,
        // but sk is in the graveyard, well this shouldn't be a claim.

        PlayerList deadPlayers;
        for(int i = claimDay; i < n.getDayNumber(); i++){
            deadPlayers = n.getDeadList(i);
            for(Player p: deadPlayers){
                for(Faction t: teams){
                    if(p.getColor().equals(t.getColor()))
                        return false;
                }
            }
        }

        return true;
    }

    public boolean outlandish(Computer c) {
        PlayerList deadAccused = getAccused().getDeadPlayers();
        if(deadAccused.isEmpty())
            return false;

        for(Player acc: getAccused().getDeadPlayers()){
            if(acc.getDeathType().isCleaned())
                return false;
            if(teams.contains(acc.getFaction()))
                return false;
        }
        // i did not find a dead person whos team was a team that the prosecutor claimed
        return true;
    }

    @Override
    public boolean valid(RoleGroupList rolesList, Computer c) {
        if(c.player.equals(getClaimer()))
            return true;

        if(!rolesList.contains(Detective.class))
            return false;
        return true;
    }

    public void talkAboutBadClaim(Computer c) {
        if(c.player.isSilenced() || c.player.isPuppeted())
            return;
        StringBuilder sb = new StringBuilder();
        sb.append("I don't believe " + getClaimer().getName() + "'s claim that " + accused.getStringName()
                + " is part of the ");
        if(teams.size() == 1)
            sb.append(teams.get(0).getName());
        else{
            Faction lastTeam = teams.get(teams.size() - 1);
            for(Faction t: teams){
                if(t == lastTeam){
                    sb.deleteCharAt(sb.length() - 1);
                    sb.append(" or ");
                }
                sb.append(t.getName());
                sb.append(", ");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.deleteCharAt(sb.length() - 1);
            sb.append('.');
        }
        c.controller.say(sb.toString(), Constants.DAY_CHAT);

    }

    public PlayerList getPlayersToActOn(Computer computer) {
        if(outlandish(computer))
            return new PlayerList(getClaimer());
        return getAccused();
    }

    public PlayerList getSafePlayers(Computer computer) {
        // if(!believable(computer))
        return new PlayerList();

    }
}
