package game.ai;

import game.logic.Faction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.FactionList;
import game.logic.support.Constants;
import game.roles.Sheriff;

public class SheriffClaim extends Claim {

    private PlayerList accused;
    private FactionList teams;

    public SheriffClaim(Player prosecutor, PlayerList targets, FactionList t, Brain b) {
        super(prosecutor, b);
        this.accused = targets;
        teams = new FactionList();
        if(t != null)
            teams.add(t);
        else{
            Faction sheriffTeam = prosecutor.getFaction();
            for(Faction friend: prosecutor.narrator.getFactions()){
                if(!sheriffTeam.isEnemy(friend))// if sheriff team is not an enemy of this team
                    teams.add(friend);
            }
        }
    }

    public PlayerList getAccused() {
        return accused.copy();
    }

    public boolean believable(Computer computer) {
        if(accused.getLivePlayers().isEmpty())// no one left in the claim to accuse
            return false;

        RoleGroupList livingRoles = computer.rolesList;
        return livingRoles.hasTeam(teams);
    }

    public boolean outlandish(Computer c) {
        if(!accused.getLivePlayers().isEmpty())// if there are some people still alive
            return false;
        for(Faction t: teams)
            for(Player p: accused)
                if(p.getFaction().getColor().equals(t.getColor()))
                    return false;
        return true;
    }

    public boolean valid(RoleGroupList rolesList, Computer c) {
        if(c.player.equals(getClaimer()))
            return true;

        if(!rolesList.contains(Sheriff.class))
            return false;
        return true;
    }

    public void talkAboutBadClaim(Computer c) {
        if(c.player.isPuppeted() || c.player.isSilenced())
            return;
        StringBuilder sb = new StringBuilder();
        sb.append("I don't believe " + getClaimer().getName() + "'s claim that " + accused.getStringName()
                + " is part of the ");
        if(teams.size() == 1)
            sb.append(teams.get(0).getName());
        else{
            Faction lastTeam = teams.get(teams.size() - 1);
            for(Faction t: teams){
                if(t == lastTeam){
                    sb.deleteCharAt(sb.length() - 1);
                    sb.append(" or ");
                }
                sb.append(t.getName());
                sb.append(", ");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.deleteCharAt(sb.length() - 1);
            sb.append('.');
        }
        c.controller.say(sb.toString(), Constants.DAY_CHAT);

    }

    public PlayerList getPlayersToActOn(Computer computer) {
        if(outlandish(computer))
            return new PlayerList(getClaimer());
        int allies = 0;
        for(Faction t: teams){
            if(!t.isEnemy(computer.player.getFaction()))
                allies++;
        }
        if(allies >= teams.size())
            return new PlayerList();
        return getAccused();
    }

    public PlayerList getSafePlayers(Computer computer) {
        // if(!believable(computer))
        return new PlayerList();

    }
}
