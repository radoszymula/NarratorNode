package game.ai.roles;

import java.util.ArrayList;

import game.ai.Computer;
import game.ai.Controller;
import game.logic.DeathType;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.roles.Witch;

public class AIWitch {

    public static void doRoleNightAction(Computer c) {
        Player slave = c.player;
        Controller controller = c.controller;

        PlayerList possibleVictims = c.brain.slaves.getLivePlayers().remove(c.getFactionmates()).remove(c.player);
        PlayerList possibleTargets = possibleVictims.copy();
        if(possibleVictims.isEmpty() || possibleTargets.isEmpty())
            return;

        ActionList prevActions = slave.getPrevNightTarget();
        if(prevActions != null){
            PlayerList shooters = prevActions.getTargets(Witch.MAIN_ABILITY).getLivePlayers();
            PlayerList prevTargets = new PlayerList();
            for(Action a: slave.getPrevNightTarget().getActions()){
                if(a.isDoubleTargeter())
                    prevTargets.add(a.getTargets().get(1));
            }

            DeathType dt;
            ArrayList<String[]> deathTypes;
            for(Player p: prevTargets.copy()){
                if(p.isAlive()){
                    prevTargets.remove(p);
                    continue;
                }
                dt = p.getDeathType();
                if(dt.isLynch()){
                    prevTargets.remove(p);
                    continue;
                }

                deathTypes = dt.getList();
                deathTypes.remove(Constants.GUN_KILL_FLAG);
                deathTypes.remove(Constants.MODKILL_FLAG);
                deathTypes.remove(Constants.VETERAN_KILL_FLAG);
                if(deathTypes.isEmpty()){
                    prevTargets.remove(p);
                    continue;
                }
            }

            // if previous victim is dead, or their target is alive
            if(!shooters.isEmpty() && !prevTargets.isEmpty()){
                controller.say("I found a shooter last night", slave.getName());
                possibleVictims.softIntersect(shooters);
            }else{
                possibleVictims.softRemove(shooters);
                c.memory.add(shooters);
                possibleVictims.softRemove(c.memory.rPlayers);
            }
        }

        possibleVictims.shuffle(c.brain.random, null);
        possibleTargets.shuffle(c.brain.random, null);

        for(Player possibleVictim: possibleVictims){
            for(Player possibleTarget: possibleTargets){
                try{
                    controller.setNightTarget(Witch.COMMAND, slave, possibleVictim, possibleTarget);
                    return;
                }catch(PlayerTargetingException e){
                }
            }
        }

    }
}
