package game.ai;

import java.util.ArrayList;

import game.event.ChatMessage;
import game.logic.Narrator;
import game.logic.Player;
import game.roles.Ability;

public interface Controller {

    public abstract void log(String string);

    public abstract void endNight();

    public abstract void cancelEndNight();

    /*
     * used for:
     * 
     * alerting burning
     */
    public abstract void setNightTarget();

    /*
     * used for:
     * 
     * abilities with no option.
     */
    public abstract void setNightTarget(String action, Controller... targets);

    public abstract void setNightTarget(String action, String option, Controller... targets);

    public abstract void setNightTarget(String action, String option, String opt2, Controller... targets);

    public abstract void setNightTarget(String action, String option, String opt2, String opt3, Controller... targets);

    public abstract void spy(String team);

    public abstract void vest();

    public abstract void clearTargets();

    // 0 indexed
    public abstract void cancelAction(int actionIndex);

    public abstract Controller vote(Controller target);

    public abstract Controller skipVote();

    public abstract Controller unvote();

    public abstract Controller ventVote(Controller puppet, Controller target);

    public abstract void ventUnvote(Controller puppet);

    public abstract void ventSkipVote(Controller puppet);

    public abstract ChatMessage say(String string, String key);

    public abstract void doDayAction(String ability, String option, Controller... targets);

    public abstract boolean cleanup();

    public abstract void rolePrefer(String s);

    public abstract void clearPreferences();

    public abstract boolean hasError();

    public boolean isTargeting(String ability, Controller... targets);

    public abstract Controller setName(String string);

    public abstract String getName();

    public abstract String getColor();

    public abstract String getRoleName();

    public abstract ArrayList<String> getChatKeys(); // I want to make sure the tests for this are gone //TODO remove

    public abstract ArrayList<String> getCommands();

    public abstract Player getPlayer(Narrator narrator);

    public abstract void setLastWill(String string);

    public abstract void setTeamLastWill(String string);

	public abstract boolean is(Class<? extends Ability> class1);

}
