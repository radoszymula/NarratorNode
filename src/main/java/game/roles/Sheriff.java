package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.HTString;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Sheriff extends Ability {

    public static final int MAIN_ABILITY = Ability.CHECK;
    public static final String COMMAND = "Check";
    public static final String NIGHT_ACTION_DESCRIPTION = "Investigate someone to determine if they are suspicious or not.";

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    public static final String NOT_SUSPICIOUS = "Your target is not suspicious.";

    @Override
    public void doNightAction(Action action) {
        Player owner = action.owner, target = action.getTarget();
        if(target == null)
            return;

        Faction sheriffFaction = owner.narrator.getFaction(owner.getInitialColor());

        Faction targetFaction = target.getFaction();

        FactionRole status = target.getFrameStatus();
        if(status != null)
            targetFaction = owner.narrator.getFaction(status.getColor());

        if(!target.isDetectable())
            targetFaction = null;
        else if(!sheriffFaction.sheriffDetects(targetFaction.getColor()))
            targetFaction = null;
        else if(!owner.narrator.getBool(SetupModifier.CHECK_DIFFERENTIATION) && !sheriffFaction.isEnemy(targetFaction))
            targetFaction = null;

        new Feedback(owner).add(generateFeedback(targetFaction, owner.narrator)).setPicture("lookout")
                .addExtraInfo("As a reminder, you attempted to check " + action.getIntendedTarget() + ".");

        happening(owner, " checked ", target);
        action.markCompleted();
        owner.visit(target);
    }

    public static ArrayList<Object> generateFeedback(Faction faction, Narrator narrator) {
        ArrayList<Object> list = new ArrayList<Object>();
        if(faction == null){
            list.add(NOT_SUSPICIOUS);
            return list;
        }

        if(narrator.getBool(SetupModifier.CHECK_DIFFERENTIATION)){
            if(faction.knowsTeam())// this is just for text
                list.add("Your target is a member of the ");
            else
                list.add("Your target is a ");
            list.add(new HTString(faction.getName(), faction.getColor()));
        }else{
            list.add("Your target is suspicious");
        }

        list.add(".");
        return list;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.CHECK_DIFFERENTIATION);
        rules.add(SetupModifier.SHERIFF_PREPEEK);

        return rules;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player player) {
        ArrayList<String> ret = new ArrayList<>();

        boolean knowsDifference = player.narrator.getBool(SetupModifier.CHECK_DIFFERENTIATION);

        if(player.getFaction().getSheriffDetectables().size() == 1){

        }else if(knowsDifference)
            ret.add("You will be able to tell the difference between targets.");
        else
            ret.add("You can only tell if a target is suspicious or not.");
        ret.add(getDetectText(player.narrator, player.getInitialColor(), player));

        if(player.narrator.getBool(SetupModifier.SHERIFF_PREPEEK))
            ret.add("You will know the name of one of your allies at game start.");
        else
            ret.add("You do not get a pregame check.");

        return ret;
    }

    private static String getDetectText(Narrator narrator, String nullableColor, Player player) {
        boolean knowsDifference = narrator.getBool(SetupModifier.CHECK_DIFFERENTIATION);
        Faction sheriffFaction = narrator.getFaction(nullableColor);
        ArrayList<Faction> detectableFactions = new ArrayList<>();

        Faction faction;
        for(String detectableFactionColors: sheriffFaction.getSheriffDetectables()){
            if(knowsDifference || sheriffFaction.isEnemy(detectableFactionColors)){
                faction = narrator.getFaction(detectableFactionColors);
                detectableFactions.add(faction);
            }
        }

        if(detectableFactions.isEmpty()){
            return ("Unfortunately, the host created a ridiculous save that doesn't allow you to do anything.  Please report this individual.");
        }
        StringBuffer sb = new StringBuffer();
        if(player == null){
            ArrayList<FactionRole> sMembers = narrator.getPossibleMembers().getMemberWithAbilities(Sheriff.class);
            if(sMembers.isEmpty())
                sb.append("Sheriffs");
            else
                sb.append(sMembers.get(0).getName());
        }else{
            sb.append("You");
        }

        if(detectableFactions.size() == 1){
            sb.append(" can detect who " + detectableFactions.get(0).getName() + " are.");
        }else{
            sb.append(" can detect the following team(s): ");
            for(Faction detectable: detectableFactions){
                sb.append(detectable.getName());
                sb.append(", ");
            }
            sb.delete(sb.length() - 2, sb.length());
        }
        return sb.toString();
    }

    @Override
    public ArrayList<String> getPublicDescription(Narrator narrator, String color, String actionOriginatorName, AbilityList role) {
        ArrayList<String> ruleTextList = super.getPublicDescription(narrator, actionOriginatorName, color, role);
        if(color != null)
        	ruleTextList.add(getDetectText(narrator, color, null));
        return ruleTextList;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public void onGameStart(Player sheriff) {
        super.onGameStart(sheriff);
        if(!sheriff.narrator.getBool(SetupModifier.SHERIFF_PREPEEK))
            return;
        Player peek = sheriff.getFaction().getMembers().remove(sheriff).getRandom(sheriff.narrator.getRandom());
        if(peek == null)
            return;

        Message e = new Feedback(sheriff, Feedback.PREGAME_FEEDBACK);
        e.add(peek, " is on your team.");
        e.setVisibility(sheriff);
        e.dontShowPrivate();
        sheriff.sendMessage(e);

        e = new Happening(sheriff.narrator);

        StringChoice sc = new StringChoice(sheriff);
        sc.add(sheriff, "Your");
        e.add(sc);

        sc = new StringChoice("'s");
        sc.add(sheriff, "");

        e.add(sc, " pregame clear is ", peek, ".");
        e.setPrivate();
    }

    @Override
    public void checkIsNightless(Narrator n) {
        if(!n.getBool(SetupModifier.SHERIFF_PREPEEK))
            super.checkIsNightless(n);
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Sheriff", Sheriff.class);
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }
}
