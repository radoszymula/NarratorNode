package game.roles;

import java.util.ArrayList;

import game.event.Feedback;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.roles.support.ElectrocutionException;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Interceptor extends Ability {

    public static final int MAIN_ABILITY = Ability.INTERCEPT;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String ROLE_NAME = "Mass Murderer";

    private static final String SELECTION_PROMPT = "murder whoever visits ";

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        PlayerList targets = getActionTargets(actions);

        Player owner = actions.get(0).owner;

        list.add(SELECTION_PROMPT);
        if(targets.size() == 1){
            Player target = targets.getFirst();

            StringChoice sc = new StringChoice(target);
            sc.add(target, "your");

            list.add(sc);
            sc = new StringChoice("'s");
            sc.add(target, "");

            list.add(sc);
            list.add(" house");
        }else{
            for(Player target: targets){
                StringChoice sc = new StringChoice(target);
                sc.add(owner, "your");

                list.add(sc);
                sc = new StringChoice("'s");
                sc.add(owner, "");

                list.add(sc);
                list.add(" house");
                list.add(" and ");
            }
            list.remove(list.size() - 1);
        }

        return list;
    }

    public ArrayList<Object> getInsteadText(Action a) {
        Player target = a.getTarget();
        ArrayList<Object> list = new ArrayList<Object>();
        list.add("Instead of camping out at ");

        StringChoice sc = new StringChoice(target);
        sc.add(target, "your");
        list.add(sc);
        list.add(new StringChoice("'s ").add(target, ""));
        list.add(" house, ");
        return list;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Kill the first person who visits your target.";

    public static final String COMMAND = "Intercept";
    public static final String DEATH_FEEDBACK = "You were killed by an interceptor.";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        if(target == null)
            return;

        // if no one visited, put a marker on the player
        // if someone visited, kill that person
        // send feedback to all people
        // put a marker on the person

        // set intercept listener for future visits
        // send feedback to all people that missed the interceptor
        // kill first one, if visitors is empty

        happening(a.owner, " will kill the first person who visits ", target);
        a.markCompleted();
        a.owner.visit(target);
        return;
    }

    @Override
    public ArrayList<String> getPublicDescription(Narrator narrator, String actionOriginatorName, String color, AbilityList abilities) {
        ArrayList<String> list = new ArrayList<>();

        list.add("Will only kill the first person to visit the target.");
        list.add("Won't kill non-movers like citizens.");
        list.add("Identity is revealed to all visitors of " + actionOriginatorName + " target");

        list.addAll(super.getPublicDescription(narrator, actionOriginatorName, color, abilities));

        return list;
    }

    public static ArrayList<Object> FeedbackGenerator(Player interceptor) {
        ArrayList<Object> parts = new ArrayList<>();
        parts.add(
                "As you approached your target, you discovered that he was being guarded by an interceptor. The interceptor is ");
        parts.add(interceptor);
        parts.add(".");
        return parts;
    }

    private int killCount;

    public boolean canKill(Player interceptor) {
        return killCount < interceptor.getActions().getActions(MAIN_ABILITY).size();
    }

    public PlayerList intercept(Player interceptor, Player target) {
        try{
            killCount++;
            Kill(interceptor, target, Constants.MASS_MURDERER_FLAG);
        }catch(ElectrocutionException e){
            return e.charged;
        }
        return new PlayerList();
    }

    public static void HandleFeedback(Player owner, Player interceptor) {
        ArrayList<Object> parts = FeedbackGenerator(interceptor);
        Message m = new Feedback(owner).add(parts).setPicture("massmurderer");

        int allied = 0;
        int enemied = 0;
        for(Faction t: owner.narrator.getFactions()){
            if(owner.getFaction().isEnemy(t))
                enemied++;
            else
                allied++;
        }

        if(allied != 0 && enemied == 0)
            m.addExtraInfo("This person is definitely your enemy");
        else if(allied == 0 && enemied != 0)
            m.addExtraInfo("This person is definitely your ally.");

    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        killCount = 0;
    }

    public static PlayerList handleInterceptor(Player visitor, Player greeter) {
        // if there's an interceptor among the visitors, i need to know about it
        // see if the interceptor will attack me

        // if i'm an interceptor, i need to tell everyone else that i'm an interceptor
        // if there's someone there, kill the first visitor.
        PlayerList electrocuted = new PlayerList();
        Interceptor ic;
        for(Player interceptor: greeter.getVisitors("interceptor").filter(Interceptor.class)){
            if(interceptor == visitor)
                continue;
            if(!usingInterceptorAbility(interceptor))
                continue;

            Interceptor.HandleFeedback(visitor, interceptor);

            ic = (Interceptor) interceptor.getAbility(COMMAND);
            if(ic.canKill(interceptor))
                electrocuted.add(ic.intercept(interceptor, visitor));
        }
        PlayerList otherVisitors = greeter.getVisitors("interceptor").remove(visitor);
        if(!visitor.is(Interceptor.class) || otherVisitors.isEmpty())
            return electrocuted;

        for(Player other: greeter.getVisitors("interceptor")){
            Interceptor.HandleFeedback(other, visitor);
        }

        ic = (Interceptor) visitor.getAbility(COMMAND);

        if(ic.canKill(visitor))
            electrocuted.add(ic.intercept(visitor, greeter.getVisitors("interceptor").getFirst()));

        return electrocuted;
    }

    private static boolean usingInterceptorAbility(Player interceptor) {
        for(Action a: interceptor.getActions()){
            if(a.is(INTERCEPT)){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Interceptor", Interceptor.class);
        return FactionService.createFactionRole(faction, role);
    }
}
