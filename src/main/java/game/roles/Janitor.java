package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.action.Action;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import models.Modifiers;
import services.FactionService;
import services.RoleService;

public class Janitor extends Ability {

    public static final String COMMAND = "Clean";

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Hide the role of a person from being revealed to everyone.";
    public static final int MAIN_ABILITY = Ability.CLEAN;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    public static final int CLEAN_ = MAIN_ABILITY;

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.JANITOR_GETS_ROLES);
        return rules;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        target.setCleaned(a.owner);
        happening(owner, " cleaned ", target);
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();
        if(p.narrator.getBool(SetupModifier.JANITOR_GETS_ROLES))
            ret.add("You will uncover the roles of your successful cleans.");

        return ret;
    }

    public static void Reveal(Player jan, Player player) {
        if(!jan.is(Janitor.class))
            return;

        Investigator.Feedback(jan, player, null, true);
    }

    public static void SendOutFeedback(Player p) {
        if(p.getCleaners() == null)
            return;
        for(Player jan: p.getCleaners()){
            if(p.narrator.getBool(SetupModifier.JANITOR_GETS_ROLES))
                Janitor.Reveal(jan, p);
            if(jan.is(Janitor.class))
                jan.getAbility(Janitor.class).useCharge().triggerCooldown();
        }
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static boolean isBeingCleaned(Player p) {
        boolean janitorCleaned = p.getCleaners() != null && !p.getCleaners().isEmpty();
        Modifiers<RoleModifier> modifiers = p.factionRole.role.modifiers;
        boolean attributeCleaned = modifiers.getOrDefault(RoleModifier.HIDDEN_FLIP, false);
        return janitorCleaned || attributeCleaned;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Janitor", Janitor.class);
    }

}
