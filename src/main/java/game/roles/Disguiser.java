package game.roles;

import game.event.NightChat;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.attacks.Attack;
import game.logic.support.rules.AbilityModifier;
import models.FactionRole;
import models.enums.GamePhase;
import services.FactionService;
import services.RoleService;

public class Disguiser extends Ability {

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final int MAIN_ABILITY = Ability.DISGUISE;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Kill someone and take their identity.";

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        disguisedTonight = false;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);

        if(getPerceivedCharges() == 0)
            Exception("You cannot disguise anymore");

    }

    public static final String DEATH_FEEDBACK = "You were killed by a Disguiser!";
    public static final String COMMAND = "Disguise";

    private static boolean willDie(Player p) {
        if(p.getLives() > 1)
            return false;
        if(p.getRealAutoVestCount() > 0){
            for(Attack a: p.getInjuries()){
                if(a.isCountered())
                    continue;
                if(a.getType() == Constants.JESTER_KILL_FLAG)
                    return true;
            }
        }
        return true;
    }

    public Player killedTarget;
    public Player disguised;
    boolean disguisedTonight;

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(owner.narrator.nightPhase == GamePhase.PRE_KILLING || disguisedTonight)
            return;
        if(killedTarget != null && killedTarget.disguisedPrevName == null && willDie(killedTarget)
                && owner.narrator.nightPhase == GamePhase.POST_KILLING){
            disguisedTonight = true;
            owner.switchNames(killedTarget);
            killedTarget.setCleaned(owner.getSkipper());
            killedTarget.disguisedPrevName = owner.getName();
            happening(owner, " disguised as ", killedTarget);

            Faction currentTeam = owner.getFaction();
            if(disguised != null){
                for(Faction t: owner.getFactions()){
                    if(currentTeam != t){
                        t.removeMember(owner);
                        NightChat nl = owner.narrator.getEventManager().getNightLog(t.getColor());
                        if(nl != null){
                            owner.removeChat(nl);
                        }
                    }
                }
            }

            if(!killedTarget.getID().equals(a.owner.getID())){
                for(Faction t: killedTarget.getFactions()){
                    if(t != currentTeam && t.knowsTeam())
                        t.infiltrate(a.owner);
                }
                if(Mason.IsMasonType(killedTarget))
                    killedTarget.getFaction().infiltrate(a.owner);
            }

            disguised = killedTarget;
            owner.stealVotePower(killedTarget);
            killedTarget = null;
            a.markCompleted();
            return;
        }else if(target == null){
            return;
        }else if(killedTarget == null){
            killedTarget = target;
            Kill(owner, target, Constants.HIDDEN_KILL_FLAG);// visiting taken care of already in kill
            return;
        }
        return;
    }

    @Override
    public boolean isChatRole() {
        return true;
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    protected String getChargeText() {

        int charges = getPerceivedCharges();
        if(super.charges == null){
            return ("You can disguise as many times as you want.");
        }else if(charges == 0){
            return ("You cannot disguise anymore.");
        }else if(charges == 1){
            if(modifiers.hasKey(AbilityModifier.CHARGES)
                    && modifiers.getOrDefault(AbilityModifier.CHARGES, 1) == 1)
                return ("You may only disguise once");

            return "You can disguise one more time.";
        }else{
            return ("You can give disguise " + charges + " more times.");
        }
    }

    @Override
    public int nightVotePower(Player p, PlayerList pl) {
        if(disguised == null){
            return super.nightVotePower(p, pl);
        }

        int vp = 0;
        for(Ability a: disguised.getAbilities()){
            vp = Math.max(vp, a.nightVotePower(disguised, pl.copy()));
        }
        return Math.max(super.nightVotePower(p, pl), vp);
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    public static boolean hasDisguised(Player p) {
        if(p.isDead())
            return false;
        if(p.getAbility(Disguiser.class) == null)
            return false;
        return p.getAbility(Disguiser.class).disguised != null;
    }

    public static Player getDisguised(Player p) {
        if(p.isDead())
            return null;
        if(p.getAbility(Disguiser.class) == null)
            return null;
        return p.getAbility(Disguiser.class).disguised;
    }

    @Override
    public boolean allowsForFriendlyFire() {
        return true;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = template(faction.narrator);
        return FactionService.createFactionRole(faction, role);
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Disguiser", Disguiser.class);
    }
}
