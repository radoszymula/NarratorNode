package game.roles;

import java.util.ArrayList;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.roles.support.Gun;
import game.roles.support.Vest;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Vigilante extends Passive {

    public Vigilante() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    private static Gun VigiGun() {
        return new Gun(null).setFlag(Constants.VIGILANTE_KILL_FLAG);
    }

    public boolean unlimitedGuns = false;

    public static final String NIGHT_ACTION_DESCRIPTION = "Takes justice into own hands and kills people at night.";

    @Override
    public String getNightText(Player p) {
        Narrator n = p.narrator;
        if(getMyVigiGuns() == 0)
            return "You have no more " + n.getAlias(Vest.ALIAS) + " to make";
        String message = "Type " + NQuote(Constants.GUN_COMMAND) + " to kill this person.";
        if(getMyVigiGuns() > 0)
            message += "You have " + getMyVigiGuns() + " " + n.getAlias(Vest.ALIAS) + " left to use.";
        return message;
    }

    @Override
    public Ability initialize(Player p) {
        if(!p.hasAbility(GunAbility.class))
            p.addAbility(new GunAbility(p));
        return super.initialize(p);
    }

    @Override
    public void mainAbilityCheck(Action a) {
        noAcceptableTargets();
    }

    public static final String DEATH_FEEDBACK = "You were killed by a Vigilante!";

    public int getMyVigiGuns() {
        return getPerceivedCharges();
    }

    /*
     * @Override public int getRealCharges(){ if(unlimitedGuns) return
     * Rules.UNLIMITED; int bullets = 0; for(Gun g: p.getGuns()){ if(g.isReal() &&
     * g.getFlag().equals(Constants.VIGILANTE_KILL_FLAG)) bullets++; } return
     * bullets; }
     */

    /*
     * @Override public int getPerceivedCharges(){ if(unlimitedGuns) return
     * Rules.UNLIMITED; int myGunCount = 0; for(Gun g: p.getGuns()){
     * if(isVigiGun(g)) myGunCount++; } return myGunCount; }
     */

    @Override
    public boolean unlimitedCharges(Player p) {
        return getPerceivedCharges() == SetupModifiers.UNLIMITED;
    }

    public static boolean UnlimitedShots(Player p) {
        if(!p.is(Vigilante.class))
            return false;
        return p.getAbility(Vigilante.class).unlimitedGuns;
    }

    @Override
    public void setInitialCharges(int charge, Player player) {
        if(player.narrator.getInt(SetupModifier.CHARGE_VARIABILITY) != 0 && charge != SetupModifiers.UNLIMITED)
            charge = AddNoise(charge, player.narrator);

        if(!player.hasAbility(GunAbility.class))
            player.addAbility(new GunAbility(player));

        GunAbility ga = player.getAbility(GunAbility.class);
        ga.removeVigiGuns();
        for(int i = 0; i < charge; i++){
            player.addGun(VigiGun());
        }

        unlimitedGuns = charge == SetupModifiers.UNLIMITED;

        if(!ga.hasItems() && unlimitedGuns)
            player.addGun(VigiGun());
    }

    @Override
    public int getRealCharges() {
        if(unlimitedGuns)
            return SetupModifiers.UNLIMITED;
        return p.getAbility(GunAbility.class).getRealCharges();
    }

    @Override
    public boolean isPowerRole() {
        return Vigilante.GetVigiGunCount(p, null) != 0;
    }

    /*
     * public boolean hasNightGuns(Player p){ for(Gun g: p.getGuns()){
     * if(isVigiGun(g)) return true; } return p.hasNightGuns(); }
     */

    public static boolean isVigiGun(Gun g) {
        return g.getFlag() == Constants.VIGILANTE_KILL_FLAG;
    }

    @Override
    public boolean chargeModifiable() {
        return true;
    }

    @Override
    public boolean isActivePassive(Player p) {
        if(unlimitedGuns)
            return true;
        return getPerceivedCharges() != 0;
    }

    @Override
    public void onAbilityLoss(Player p) {
        p.getAbility(GunAbility.class).removeVigiGuns();
    }

    @Override
    public void onDayStart(Player p) {
        super.onPlayerNightEnd(p);
        if(Vigilante.GetRealVigiGunCount(p) == 0 && unlimitedGuns){
            p.addGun(VigiGun());
        }
    }

    @Override
    public String getRuleChargeText(Narrator n) {
        int val = this.modifiers.getInt(AbilityModifier.CHARGES);
        StringBuilder sb = new StringBuilder("Starts off with ");
        if(n.getInt(SetupModifier.CHARGE_VARIABILITY) != 0)
            sb.append("~");
        sb.append(val);
        sb.append(" ");
        sb.append(n.getAlias(Gun.ALIAS));
        if(val != 1)
            sb.append("s");
        return sb.toString();
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> specs = new ArrayList<>();
        GunAbility va = p.getAbility(GunAbility.class);
        if(!va.hasItems())
            specs.add("You've used up all your " + p.narrator.getAlias(Gun.ALIAS) + "s.");
        else if(unlimitedGuns)
            specs.add("You can use your " + p.narrator.getAlias(Gun.ALIAS) + " as many times as you want.");

        return specs;
    }

    public static int GetRealVigiGunCount(Player vigi) {
        return GetVigiGunCount(vigi, true);
    }

    public static int GetVigiGunCount(Player vigi, Boolean real) {
        int vigiGuns = 0;
        for(Gun g: vigi.getAbility(GunAbility.class)){
            if(g.getFlag() != Constants.VIGILANTE_KILL_FLAG)
                continue;
            if(real == null){
                vigiGuns++;
            }else if(real && g.isReal())
                vigiGuns++;
        }
        return vigiGuns;
    }

    @Override
    public int getPerceivedCharges() {
        if(unlimitedGuns)
            return SetupModifiers.UNLIMITED;

        return GetVigiGunCount(p, null);
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public boolean canEditBackToBackModifier() {
        return false;
    }

    @Override
    public boolean canEditZeroWeightedModifier() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Vigilante", Vigilante.class);
        return FactionService.createFactionRole(faction, role);
    }
}
