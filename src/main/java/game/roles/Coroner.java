package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.HTString;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Coroner extends Ability {

    public static final String ROLE_NAME = "Coroner";
    public static final String COMMAND = "Autopsy";

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Target a dead player to learn their true identity, and who they visited each night.";

    public static final int MAIN_ABILITY = Ability.AUTOPSY;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        aliveCheck(a);
    }

    // public static final
    public static final String LIVE_FEEDBACK = "You tried to perform an autopsy on a living person.";

    @Override
    public void doNightAction(Action a) {
        Player coro = a.owner;
        Player target = a.getTarget();
        if(target == null)
            return;

        if(target.isAlive()){
            new Feedback(coro, LIVE_FEEDBACK).setPicture("amnesiac");
            NoNightActionVisit(a);
            return;
        }

        HTString team = new HTString(target.getRoleName(), target.getFaction());
        Message fb = new Feedback(coro, "Your target was a ").add(team).add(".").setPicture("lookout");

        int i = 0;
        if(coro.narrator.getBool(SetupModifier.DAY_START))
            i = 1;

        PlayerList prevTargets;
        ActionList al;
        for(int day = i; day <= coro.narrator.getDayNumber() - 1; day++){
            al = target.getPrevNightTarget(day);
            prevTargets = new PlayerList();
            for(Action pAction: al.getActions()){
                if(pAction.ability == Faction.SEND_)
                    continue;
                prevTargets.add(pAction.getTargets());
            }
            if(!prevTargets.isEmpty()){
                fb.add(" On Night " + day + ", your target visited ", prevTargets, ".");
            }
        }

        if(target.getDeathType().isCleaned()){
            ArrayList<String[]> attacks = target.getDeathType().attacks;
            String deathString;
            if(attacks.size() == 1)
                deathString = attacks.get(0)[1].toLowerCase() + ".";
            else{
                StringBuilder sb = new StringBuilder();
                for(int j = 0; j < attacks.size(); j++){
                    if(j == attacks.size() - 1)
                        sb.append(" and ");
                    sb.append(attacks.get(j)[1].toLowerCase());
                    sb.append(", ");
                }
                sb.deleteCharAt(sb.length() - 1);
                sb.deleteCharAt(sb.length() - 1);
                sb.append(".");
                deathString = sb.toString();
            }
            fb.add(" Your target was actually " + deathString);
        }

        if(target.narrator.getBool(SetupModifier.CORONER_LEARNS_ROLES)){
            fb.add(" Your target was visited by the following roles: ");
            for(int j = 0; j < target.getRoleVisits().size(); j++){
                if(j != 0)
                    fb.add(", ");
                fb.add(target.getRoleVisits().get(j));
            }
            fb.add(".");
        }

        if(target.getDeathType().isCleaned())
            target.lastWillTrustees.put(coro, true);

        if(target.narrator.getBool(SetupModifier.CORONER_EXHUMES))
            target.setExhumed();

        happening(coro, " performed an autopsy on ", target);
        a.markCompleted();
        coro.visit(target);
    }

    @Override
    public String getNightText(Player p) {
        return "Type " + NQuote(COMMAND.toLowerCase()) + " to perform an autopsy on this person.";
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.CORONER_EXHUMES);
        rules.add(SetupModifier.CORONER_LEARNS_ROLES);
        return rules;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.CORONER_LEARNS_ROLES))
            ret.add("You will learn what roles visited your targets.");
        else
            ret.add("You will not learn what roles visited your targets.");

        if(p.narrator.getBool(SetupModifier.CORONER_LEARNS_ROLES)
                && p.narrator.getPossibleMembers().abilityExists(p.narrator, GraveDigger.class))
            ret.add("Autopsies stop bodies from being reanimated.");
        else
            ret.add("Autopsies do not stop bodies from being reanimated.");

        return ret;
    }

    @Override
    public String getUsage(Player p, Random r) {
        if(p.narrator.getDeadPlayers().isEmpty())
            return "Can't use this until people die";
        return super.getUsage(p, r);
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Coroner", Coroner.class);
        return FactionService.createFactionRole(faction, role);
    }
}
