package game.roles;

import java.util.ArrayList;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.attacks.IndirectAttack;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class ElectroManiac extends Ability {

    public static final int MAIN_ABILITY = Ability.CHARGE;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String ROLE_NAME = "Electro Maniac";

    @Override
    public Ability initialize(Player p) {
        fakeCharge = false;
        usedDoubleAction = null;
        return super.initialize(p);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Polarize people, so that they will die when visiting each other.";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    public static final String COMMAND = "Charge";

    public static final String DEATH_FEEDBACK = "You were killed by an Electro Maniac!";
    public static final String CHARGED_FEEDBACK = "You were charged last night!";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner;
        PlayerList targets = a.getTargets();
        if(targets == null || targets.isEmpty())
            return;
        for(Player target: targets){
            if(target.isCharged() && !target.is(ElectroManiac.class))
                Kill(owner, target, Constants.ELECTRO_KILL_FLAG);// visiting taken care of already in kill
            else{
                Feedback fb = new Feedback(target);
                FeedbackGenerator(fb);
                target.setCharged(owner);
                owner.visit(target);
                happening(owner, " charged ", target);
            }
            if(fakeCharge)
                break;
        }
        if(targets.size() == 2)
            usedDoubleAction = owner.narrator.getDayNumber();

        a.markCompleted();
    }

    public static void FeedbackGenerator(Feedback fb) {
        fb.add(CHARGED_FEEDBACK).setPicture("electromaniac");
        fb.addExtraInfo("If you visit or get visited by someone else that's charged, you'll be shocked to death!");
    }

    public Integer usedDoubleAction;
    private boolean fakeCharge;
    private boolean wasBlocked = false;

    @Override
    public void targetSizeCheck(Action a) {
        if(usedDoubleAction != null && !fakeCharge){
            super.targetSizeCheck(a);
            return;
        }
        if(a.getTargets().size() == 1)
            return;
        if(a.getTargets().size() != 2){
            Exception("You may select one or two targets only.");
        }
    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        int dayNumber = p.narrator.getDayNumber() - 1;
        if(usedDoubleAction == null || dayNumber != usedDoubleAction)
            return;
        if(!wasBlocked)
            return;
        wasBlocked = false;
        usedDoubleAction = null;
        fakeCharge = true;
    }

    @Override
    public void onNightFeedback(ArrayList<Feedback> feedback) {
        super.onNightFeedback(feedback);
        for(Feedback f: feedback){
            if(f.access(p).equals(Stripper.FEEDBACK)){
                wasBlocked = true;
                break;
            }
        }
    }

    public static void ElectroKill(Player p) {
        if(p.isAlive())
            p.kill(new IndirectAttack(Constants.ELECTRO_KILL_FLAG, p, p.getCharger()));
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public String getUsage(Player p, Random r) {
        if(usedDoubleAction == null)
            return super.getUsage(p, r);
        PlayerList live = p.narrator.getLivePlayers().remove(p);
        live.shuffle(r, null);
        return getCommand() + " *" + live.getFirst().getName() + " " + live.getLast().getName() + "*";
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    @Override
    public boolean canAddAnotherAction(Action a) {
        if(a.getTargets().size() == 1)
            return super.canAddAnotherAction(a);
        for(Action current: a.owner.getActions().getActions(MAIN_ABILITY))
            if(current._targets.size() == 2)
                return false;
        return super.canAddAnotherAction(a);
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Electromaniac", ElectroManiac.class, Bulletproof.class);
        return FactionService.createFactionRole(faction, role);
    }

}
