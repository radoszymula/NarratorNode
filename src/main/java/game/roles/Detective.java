package game.roles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Detective extends Ability {

    public static final String COMMAND = "Follow";

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final int MAIN_ABILITY = Ability.FOLLOW;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Find out who someone visits at night.";

    // private boolean visited = false;
    public static final String FEEDBACK = "Your target visited: ";
    public static final String NO_VISIT = "Your target didn't visit anyone.";
    public static final String NO_ONE_ELSE = "Your target didn't visit anyone else.";
    private HashMap<Player, PlayerList> seenTargets;

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        if(owner.narrator.pre_visiting_phase){
            owner.visit(target);
            return;
        }

        if(seenTargets == null)
            seenTargets = new HashMap<>();
        PlayerList formerTargets = seenTargets.get(target);
        if(formerTargets == null){
            formerTargets = new PlayerList();
            seenTargets.put(target, formerTargets);
        }
        Message feedback = follow(owner, target, a.getIntendedTarget(), formerTargets);
        if(feedback != null)
            formerTargets.add(feedback.getPlayers());
        happening(owner, " followed ", target);
        a.markCompleted();

    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        if(seenTargets != null)
            seenTargets.clear();
    }

    public static boolean seesAll(Player owner) {
        return owner.narrator.getBool(SetupModifier.FOLLOW_GETS_ALL) || owner.is(Agent.class);
    }

    public static Message follow(Player owner, Player target, String ogTarget, PlayerList seenTargets) {
        boolean seesAll = seesAll(owner);
        PlayerList visits;
        if(target.isDetectable()
                || (owner.is(Detective.class) && owner.narrator.getBool(SetupModifier.FOLLOW_PIERCES_IMMUNITY)))
            visits = target.getVisits("Detective");
        else
            visits = new PlayerList();
        for(int i = 0; i < visits.size(); i++){
            if(!seesAll && visits.get(i).in(seenTargets)){
                visits.remove(i);
                i--;
            }
        }
        if(seesAll)
            visits.sortByName();
        ArrayList<Object> parts = FeedbackGenerator(owner, visits, seenTargets);
        Feedback f = new Feedback(owner);
        f.add(parts);
        if(!parts.isEmpty() && parts.get(0).toString().equalsIgnoreCase(NO_ONE_ELSE))
            f.setSorted();
        f.setPicture("detective");
        f.addExtraInfo("As a reminder, you attempted to follow " + ogTarget + ".");
        return f;
    }

    public static ArrayList<Object> FeedbackGenerator(Player owner, PlayerList visits, PlayerList seenTargets) {
        ArrayList<Object> parts = new ArrayList<>();
        if(visits.isEmpty()){
            if(seenTargets == null || seenTargets.isEmpty())
                parts.add(NO_VISIT);
            else
                parts.add(NO_ONE_ELSE);
        }else{
            parts.add(FEEDBACK);
            for(Player p: visits){
                parts.add(p);
                if(!seesAll(owner))
                    break;
                if(visits.getLast() != p)
                    parts.add(", ");
            }
            parts.add(".");
        }
        return parts;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.FOLLOW_GETS_ALL)){
            ret.add("You will see all targets visited by the person you follow.");
        }else{
            ret.add("You will only see one target visited by the person you follow.");
        }
        if(p.narrator.getBool(SetupModifier.FOLLOW_PIERCES_IMMUNITY)){
            ret.add("You will be able to track detection immune targets");
        }else{
            ret.add("You cannot track detection immune targets");
        }

        return ret;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.FOLLOW_GETS_ALL);
        rules.add(SetupModifier.FOLLOW_PIERCES_IMMUNITY);
        return rules;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Detective", Detective.class);
        return FactionService.createFactionRole(faction, role);
    }
}
