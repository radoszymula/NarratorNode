package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.event.Happening;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.UnknownTeamException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Spy extends Ability {

    public static final int MAIN_ABILITY = Ability.SPY;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String ROLE_NAME = "Spy";

    public static final String NIGHT_ACTION_DESCRIPTION = "Find out all targets of a team that you select.";

    // private boolean visited = false;
    public static final String NO_VISIT = "The faction didn't visit anyone.";
    public static final String FEEDBACK = "Your spying revealed these people as targets: ";

    public static final String COMMAND = "SpyAbility";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner;
        String teamColor = a.getOption();
        if(teamColor == null){
            if(owner.narrator.pre_visiting_phase && a.getTarget() != null)
                owner.visit(a.getTarget());
            return;
        }
        if(owner.narrator.pre_visiting_phase){
            if(a.getTarget() != null)
                owner.visit(a.getTarget());
        }else{
            Faction t = owner.narrator.getFaction(teamColor);
            spy(owner, t);
            Happening e = new Happening(owner.narrator);
            e.add(owner, " spied on ", t.getName());
            a.markCompleted();
        }

    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.SPY_TARGETS_ALLIES);
        rules.add(SetupModifier.SPY_TARGETS_ENEMIES);
        return rules;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        StringBuilder teamsToSpyOn = new StringBuilder();
        teamsToSpyOn.append("You can spy on the following teams: ");
        for(Option team: getOptions(p)){
            teamsToSpyOn.append(team.getName());
            teamsToSpyOn.append(", ");
        }

        teamsToSpyOn.delete(teamsToSpyOn.length() - 2, teamsToSpyOn.length());
        teamsToSpyOn.append('.');

        ret.add(teamsToSpyOn.toString());

        return ret;
    }

    @Override
    protected String getChargeText() {
        int charges = getPerceivedCharges();
        if(charges == 1)
            return ("You may spy one more time.");
        else if(charges == 0)
            return ("You may not spy anymore.");
        else if(charges != SetupModifiers.UNLIMITED)
            return ("You may spy " + charges + " more times.");
        else
            return ("You may spy as many times as you wish");
    }

    public static void spy(Player owner, Faction target) {
        Feedback f = new Feedback(owner);
        ArrayList<Object> parts = FeedbackGenerator(target.getTargets());
        f.add(parts);
        f.setPicture("lookout");
        f.addExtraInfo("As a reminder, you tracked the movements of the " + target.getName() + ".");
    }

    public static ArrayList<Object> FeedbackGenerator(PlayerList visitors) {
        visitors.sortByName();
        ArrayList<Object> parts = new ArrayList<>();
        if(visitors.isEmpty())
            parts.add(NO_VISIT);
        else{
            parts.add(FEEDBACK);
            for(Player p: visitors){
                parts.add(p);
                if(visitors.getLast() != p)
                    parts.add(", ");
            }
        }
        return parts;
    }

    @Override
    public String getNightText(Player p) {
        Narrator n = p.narrator;
        StringBuilder teams = new StringBuilder();
        for(Option t: getOptions(p)){
            teams.append(t.getName());
            teams.append(", ");
        }
        return "Type " + SQuote("spy teamName") + " to spy on that faction.\n" + "These are the list of teams: "
                + teams.substring(0, teams.length() - 2) + ".\n" + "For example 'spy "
                + n.getFactions().get(0).getName() + "'.";
    }

    @Override
    public ArrayList<Option> getOptions(Player p) {
        ArrayList<Option> teamsToSpyOn = new ArrayList<>();
        boolean canSpyOnEnemies = p.narrator.getBool(SetupModifier.SPY_TARGETS_ENEMIES);
        boolean canSpyOnAllies = p.narrator.getBool(SetupModifier.SPY_TARGETS_ALLIES);
        for(Faction t: p.narrator.getFactions()){
            if(t == p.getFaction())
                continue;
            if(!canSpyOnEnemies && t.isEnemy(p.getFaction())){
                continue;
            }
            if(!canSpyOnAllies && !t.isEnemy(p.getFaction()))
                continue;
            teamsToSpyOn.add(Faction.Option(t));
        }
        return teamsToSpyOn;
    }

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        if(getOptions(pi).isEmpty())
            return new PlayerList();
        return null;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();
        Action a = actionList.get(0);
        Player owner = a.owner;

        for(Action act: actionList){
            Faction parse = owner.narrator.getFaction(act.getOption());
            if(parse != null){
                list.add("spy on the " + parse.getName());
            }
            list.add(" and ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();

        list.add("spying on the ");
        list.add(a.owner.narrator.getFaction(a.getOption()).getName());

        return list;
    }

    @Override
    public ArrayList<Object> getActionDescription(Action a) {
        ArrayList<Object> list = new ArrayList<>();

        list.add("spy on the ");
        list.add(a.owner.narrator.getFaction(a.getOption()));

        return list;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        if(getPerceivedCharges() == 0)
            Exception("You cannot spy anymore");
        String option = a.getOption();
        Faction t = a.owner.narrator.getFaction(option);
        if(t == null)
            throw new UnknownTeamException(option);
        // Role.Exception("You will need to select a team to spy upon.");;
        if(!t.knowsTeam() && t == a.owner.getFaction())
            Ability.Exception("Can't spy on teams that don't know their allies.");
        if(!a.owner.narrator.getBool(SetupModifier.SPY_TARGETS_ENEMIES) && t.isEnemy(a.owner.getColor()))
            Ability.Exception("Can't spy on enemy teams.");
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(!a.getTargets().isEmpty())
            Exception("You can't target players with this ability");
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        if(a.ability != MAIN_ABILITY)
            return super.getCommandParts(a);
        ArrayList<String> parsedCommands = new ArrayList<>();
        parsedCommands.add(COMMAND);
        parsedCommands.add(a.getOption());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, ArrayList<String> commands) {
        commands.remove(0);
        StringBuilder teamName = new StringBuilder();
        for(String s: commands)
            teamName.append(s);

        Narrator n = p.narrator;

        String teamColor = CommandHandler.parseTeam(teamName.toString(), n);
        if(Constants.A_INVALID.equals(teamColor))
            throw new UnknownTeamException(teamName.toString());

        return new Action(p, Player.list(), MAIN_ABILITY, teamColor, null);
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public Action getExampleAction(Player owner) {
        ArrayList<Option> list = getOptions(owner);
        if(list.isEmpty())
            return null;
        Option o = list.get(0);
        return new Action(owner, new PlayerList(), getAbilityNumber(), o, null);
    }

    @Override
    public String getUsage(Player p, Random r) {
        ArrayList<Option> options = getOptions(p);
        int index = r.nextInt(options.size());
        String ret = getCommand() + " *" + options.get(index) + "*";
        if(options.size() != 1){
            ret += "\nBelow are the teams that you can spy on: ";
            for(int i = 0; i < options.size(); i++){
                ret += options.get(i);
                if(i != options.size() - 1){
                    ret += ", ";
                }
            }
        }
        return ret;
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Spy", Spy.class);
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    @Override
    public boolean isRetarget(Action action, Action action2) {
        return action.getOption().equals(action2.getOption());
    }
}
