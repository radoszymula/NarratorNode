package game.roles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import game.logic.Narrator;
import game.logic.Player;
import game.roles.support.Consumable;

public abstract class ItemAbility<T extends Consumable> extends Ability implements Iterable<T> {

    private ArrayList<T> itemList;

    public ItemAbility(Player p, T initialItem) {
        this(p);
        itemList.add(initialItem);
    }

    public abstract String getAliasKey();

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    public ItemAbility(Player p) {
        this();
        this.p = p;
        this.narrator = p.narrator;
    }

    public ItemAbility() {
        itemList = new ArrayList<>();
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return "use a " + n.getAlias(getAliasKey()) + " to survive the night";
    }

    @Override
    public void addFakeCharge() {
        T fake = getFakeConsumable();
        itemList.add(fake);
    }

    protected abstract T getFakeConsumable();

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        for(int i = 0; i < size(); i++){
            if(get(i).isUsed()){
                remove(i);
                i--;
            }
        }
        Collections.sort(itemList);
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return new String[] {};
    }

    @Override
    public int getPerceivedCharges() {
        return size();
    }

    @Override
    public boolean isPowerRole() {
        return false;
    }

    @Override
    public Ability useCharge() {
        itemList.remove(0);
        return this;
    }

    public void addConsumable(T item) {
        itemList.add(item);
    }

    public void add(int i, T b) {
        itemList.add(i, b);
    }

    public T get(int i) {
        return itemList.get(i);
    }

    public int size() {
        return itemList.size();
    }

    public T remove(int i) {
        return itemList.remove(i);
    }

    @Override
    public int getFakeCharges() {
        int i = 0;
        for(T item: this){
            if(item.isFake())
                i++;
        }
        return i;
    }

    @Override
    public int getRealCharges() {
        int i = 0;
        for(T item: this){
            if(item.isReal())
                i++;
        }
        return i;
    }

    public boolean hasItems() {
        return !itemList.isEmpty();
    }

    @Override
    public Iterator<T> iterator() {
        return itemList.iterator();
    }

    @Override
    public void useFakeCharge() {
        for(int i = 0; i < size(); i++){
            if(get(i).isFake()){
                remove(i);
                return;
            }
        }
    }

    @Override
    public int maxNumberOfSubmissions() {
        return size();
    }

    @Override
    public boolean canSubmitFree() {
        return false;
    }

}
