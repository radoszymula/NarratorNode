package game.roles;

import java.util.ArrayList;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.roles.support.Vest;

public class VestAbility extends ItemAbility<Vest> {

    public VestAbility() {
    }

    public VestAbility(Player p) {
        super(p);
    }

    public VestAbility(Player p, Vest initialItem) {
        super(p, initialItem);
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        Player owner = actions.get(0).owner;

        ArrayList<Object> list = new ArrayList<>();
        list.add("use ");

        StringChoice scVest = new StringChoice("their");
        scVest.add(owner, "your");
        list.add(scVest);
        list.add(" " + owner.narrator.getAlias(Vest.ALIAS));

        return list;
    }

    public static final String COMMAND = Constants.VEST_COMMAND;
    public static final int MAIN_ABILITY = VEST_ABILITY;
    public static final String ALIAS = "vest_alias";

    @Override
    public String getAbilityDescription(Narrator n) {
        return "use a " + n.getAlias(ALIAS) + " to survive the night";
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return Ability.VEST_ABILITY;
    }

    @Override
    public void doNightAction(Action action) {
        Player owner = action.owner;
        if(!hasItems())
            return;
        Vest v = get(0);
        // owner.consumedItems.add(v);
        owner.prevInvulnerability = owner.isInvulnerable();
        if(v.isReal()){
            owner.setInvulnerable(true);
            owner.setVesting(true);
        }
        action.markCompleted();
        if(action.getTarget() != null)
            owner.visit(action.getTarget());
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("putting on a " + a.owner.narrator.getAlias(ALIAS) + ", ");
        return list;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return new String[] {};
    }

    @Override
    public void mainAbilityCheck(Action a) {
        if(size() == 0)
            Exception("You don't have any " + a.owner.narrator.getAlias(ALIAS) + "!");
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(!a.getTargets().isEmpty())
            Exception("You ma noy select anyone else with this ability.");
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        if(size() != 0)
            return null;
        return new PlayerList();
    }

    @Override
    public Action getExampleAction(Player owner) {
        return new Action(owner, MAIN_ABILITY);
    }

    public void removeSurvivorVests() {
        for(int i = 0; i < size(); i++){
            if(!get(i).armorSmithVest){
                remove(i);
                i--;
            }
        }
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        if(!hasItems())
            return super.getSpecificRoleSpecs(p);

        ArrayList<String> specs = new ArrayList<>();
        if(size() == 1)
            specs.add("You have a " + p.narrator.getAlias(Vest.ALIAS) + " left to protect you from death.");
        else if(size() > 0)
            specs.add(
                    "You have " + size() + " " + p.narrator.getAlias(Vest.ALIAS) + " left to protect you from death.");

        return specs;
    }

    public static int GetSurvivorVests(Player p) {
        if(!p.hasAbility(VestAbility.class))
            return 0;

        VestAbility va = p.getAbility(VestAbility.class);
        int survivorCount = 0;
        for(Vest v: va)
            if(!v.armorSmithVest)
                survivorCount++;
        return survivorCount;
    }

    @Override
    protected Vest getFakeConsumable() {
        return Vest.FakeVest();
    }

    @Override
    public int maxNumberOfSubmissions() {
        if(hasItems())
            return 1;
        return 0;
    }

    @Override
    public String getAliasKey() {
        return ALIAS;
    }
}
