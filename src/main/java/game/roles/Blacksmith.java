package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.Modifier;
import game.logic.support.rules.SetupModifier;
import game.roles.support.Gun;
import game.roles.support.Vest;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Blacksmith extends Ability {

    public static final String COMMAND = "Craft";
    public static final String FAULTY = Gunsmith.FAULTY;
    public static final String REAL = Gunsmith.REAL;
    public static final String FAKE = Armorsmith.FAKE;
    public static final String NOGUN = "no";
    public static final String NOARMOR = NOGUN;
    public static final int MAIN_ABILITY = Ability.CRAFT;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return GetNightActionDescription(n);
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(GetNightActionDescription(n));
    }

    public static String GetNightActionDescription(Narrator n) {
        return "Hand out " + n.getAlias(Gun.ALIAS) + " or " + n.getAlias(Vest.ALIAS) + " so others can use them.";
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() == 0)
            Exception("You must select at least one target.");
        if(a.getTargets().size() > 2)
            Exception("You can't select that many targets");
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
        noTeamMateTargeting(a);

        Narrator n = a.owner.narrator;
        PlayerList targets = a.getTargets();
        String gunOpt = a.getOption();
        String vestOpt = a.getOption2();

        if(gunOpt == null || vestOpt == null)
            Exception("Must specify if you're using real " + n.getAlias(Gun.ALIAS) + "s or " + n.getAlias(Vest.ALIAS)
                    + "s with options.");

        if(faultyGuns()){
            if(!REAL.equalsIgnoreCase(gunOpt) && !FAULTY.equalsIgnoreCase(gunOpt) && !NOGUN.equalsIgnoreCase(gunOpt))
                Exception("Can't tell whether " + n.getAlias(Gun.ALIAS) + " should be fake or real.");
        }else if(FAULTY.equalsIgnoreCase(gunOpt))
            Exception("Can't pass out faulty " + n.getAlias(Gun.ALIAS) + "s.");

        if(fakeVests()){
            if(!REAL.equalsIgnoreCase(vestOpt) && !FAKE.equalsIgnoreCase(vestOpt) && !NOGUN.equalsIgnoreCase(vestOpt))
                Exception("Can't tell whether " + n.getAlias(Vest.ALIAS) + " should be fake or real.");
        }else if(FAKE.equalsIgnoreCase(vestOpt))
            Exception("Can't pass out fake " + n.getAlias(Vest.ALIAS) + "s.");

        if(gunOpt == null && vestOpt == null && targets.size() == 1){
            Exception("Unable to determine what single item should be passed out.");
        }

        if(targets.size() == 2){
            if(targets.getLast() == targets.getFirst())
                Exception("Cannot give a " + n.getAlias(Gun.ALIAS) + " and " + n.getAlias(Vest.ALIAS)
                        + " to the same person");
            if(NOGUN.equalsIgnoreCase(gunOpt))
                Exception("Cannot give two " + n.getAlias(Vest.ALIAS) + "s to a person");
        }

        if(a.getTargets().size() == 1){
            if(!NOGUN.equalsIgnoreCase(a.getOption())){
                a.setOption2(NOARMOR);
            }else if(a.getOption() == null){
                a.setOption(NOGUN);
            }
        }

    }

    @Override
    public ArrayList<Option> getOptions(Player p) {
        String g_alias = p.narrator.getAlias(Gun.ALIAS);
        ArrayList<Option> options = new ArrayList<>();
        options.add(new Option(REAL, REAL + " " + g_alias));
        if(faultyGuns())
            options.add(new Option(FAULTY, FAULTY + " " + g_alias));
        options.add(new Option(NOGUN, NOGUN + " " + g_alias));
        return options;
    }

    @Override
    public ArrayList<Option> getOptions2(Player p, String option1) {
        String v_alias = p.narrator.getAlias(Vest.ALIAS);

        ArrayList<Option> options = new ArrayList<>();
        options.add(new Option(REAL, REAL + " " + v_alias));
        if(fakeVests())
            options.add(new Option(FAKE, FAKE + " " + v_alias));
        options.add(new Option(NOGUN, NOGUN + " " + v_alias));
        return options;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        if(actions.isEmpty())
            return list;
        Narrator n = actions.get(0).owner.narrator;

        PlayerList targets;
        Player target;
        String gunOpt, vestOpt;
        for(Action a: actions){
            targets = a.getTargets();
            gunOpt = a.getOption();
            vestOpt = a.getOption2();
            for(int i = 0; i < targets.size(); i++){
                target = targets.get(i);
                if(i == 0 && !NOGUN.equalsIgnoreCase(gunOpt)){
                    if(FAULTY.equalsIgnoreCase(gunOpt))
                        list.add("give a faulty " + n.getAlias(Gun.ALIAS) + " to ");
                    else
                        list.add("give a " + n.getAlias(Gun.ALIAS) + " to ");
                }else{
                    if(FAKE.equals(vestOpt)){
                        list.add("give a fake " + n.getAlias(Vest.ALIAS) + " to ");
                    }else{
                        list.add("give a " + n.getAlias(Vest.ALIAS) + " to ");
                    }
                }

                list.add(StringChoice.YouYourselfSingle(a.owner, target));
                if(i == 0 && targets.size() > 1){
                    list.add(" and ");
                }
            }
        }

        return list;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, gTarget = a.getTargets().getFirst(), vTarget = a.getTargets().getLast();

        if(vTarget == gTarget)
            vTarget = null;

        if(vTarget == null && gTarget == null)
            return;

        String gOpt = a.getOption(), vOpt = a.getOption2();

        if(NOGUN.equalsIgnoreCase(gOpt) || (gOpt == null && FAKE.equalsIgnoreCase(vOpt))){
            vTarget = gTarget;
            gTarget = null;
        }

        Armorsmith.ArmorsmithAction(owner, vTarget, vOpt);
        Gunsmith.GunsmithAction(owner, gTarget, gOpt);

        a.markCompleted();
        if(vTarget != null && gTarget != null)
            owner.visit(vTarget, gTarget);
        else if(vTarget != null)
            owner.visit(vTarget);
        else
            owner.visit(gTarget);
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.getFaction().knowsTeam() && p.getFaction().getMembers().size() > 1){
            ret.add("Your " + p.narrator.getAlias(Gun.ALIAS) + " and " + p.narrator.getAlias(Vest.ALIAS)
                    + " cannot go to teammates.");
        }
        return ret;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.GS_DAY_GUNS);
        return rules;
    }

    @Override
    public List<AbilityModifier> getAbilityModifiers() {
        List<AbilityModifier> rules = new LinkedList<>();
        rules.add(AbilityModifier.GS_FAULTY_GUNS);
        rules.add(AbilityModifier.AS_FAKE_VESTS);
        rules.addAll(super.getAbilityModifiers());
        return rules;
    }

    @Override
    public Action parseCommand(Player p, ArrayList<String> commands) {
        Action a;
        Narrator n = p.narrator;

        String last = commands.get(commands.size() - 1);
        if(n.getPlayerByName(last) != null)
            return super.parseCommand(p, commands);

        String last2 = commands.get(commands.size() - 2);
        if(n.getPlayerByName(last2) != null){
            a = super.parseCommand(p, commands);
            if(last.equalsIgnoreCase(NOGUN) || last.equalsIgnoreCase(FAULTY))
                a.setOption(last);
            else
                a.setOption2(last);
            return a;
        }

        commands.remove(commands.size() - 1);
        if(commands.isEmpty())
            throw new PlayerTargetingException("Couldn't find the playerName in question.");
        commands.remove(commands.size() - 1);
        if(commands.isEmpty())
            throw new PlayerTargetingException("Couldn't find the playerName in question.");
        a = super.parseCommand(p, commands);
        a.setOption(last2);
        a.setOption2(last);
        return a;
    }

    @Override
    public String getUsage(Player p, Random r) {
        PlayerList live = p.narrator.getLivePlayers();
        live.shuffle(r, null);
        String ret = getCommand() + " *" + live.getFirst().getName() + " " + live.getLast().getName() + "* " + REAL
                + " " + REAL + " (For both a gun and a vest)";
        ret += "\n" + getCommand() + " *" + live.getFirst().getName() + "* " + NOGUN + " " + REAL
                + " (To only give armor)";
        ret += "\n" + getCommand() + " *" + live.getFirst().getName() + "* " + REAL + " " + NOARMOR
                + " (To only give a gun)";
        if(faultyGuns())
            ret += "\n" + getCommand() + " *" + live.getFirst().getName() + " " + FAULTY + " " + NOARMOR;
        if(fakeVests())
            ret += "\n" + getCommand() + " *" + live.getFirst().getName() + " " + NOGUN + " " + FAKE;
        return ret;
    }

    @Override
    public ArrayList<String> getCommandParts(Action action) {
        ArrayList<String> parts = super.getCommandParts(action);
        if(action.getOption() != null)
            parts.add(action.getOption());
        if(action.getOption2() != null)
            parts.add(action.getOption2());

        return parts;
    }

    public boolean faultyGuns() {
        return FaultyGuns(this);
    }

    public boolean fakeVests() {
        return FakeVests(this);
    }

    public static boolean FaultyGuns(Ability r) {
        return Gunsmith.FaultyGuns(r);
    }

    public static boolean FakeVests(Ability r) {
        return Armorsmith.FakeVests(r);
    }

    @Override
    public boolean isBool(Modifier modifier) {
        if(AbilityModifier.GS_FAULTY_GUNS.equals(modifier))
            return true;
        if(AbilityModifier.AS_FAKE_VESTS.equals(modifier))
            return true;
        return super.isBool(modifier);
    }

    @Override
    public String getNightText(Player p) {
        String ret = "Incomplete";
        // TODO
        return ret;
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        return p.narrator.getLivePlayers().sortByName().remove(p);
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Blacksmith", Blacksmith.class);
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static FactionRole templateEvil(Faction faction) {
        FactionRole factionRole = template(faction);
        factionRole.role.addModifier(AbilityModifier.AS_FAKE_VESTS, Blacksmith.class, true);
        factionRole.role.addModifier(AbilityModifier.GS_FAULTY_GUNS, Blacksmith.class, true);
        return factionRole;
    }
}
