package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.IllegalActionException;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.Modifier;
import game.logic.support.rules.SetupModifier;
import game.roles.support.Gun;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Gunsmith extends Ability {

    public static final String COMMAND = "Gun";
    public static final String FAULTY = "faulty";
    public static final String REAL = "real";

    public static final String[] GS_FAULTY_GUNS = new String[] {
            "gs_faulty_guns", "#" + Gun.ALIAS + "#s can be optionally faulty",
    };

    public static final int MAIN_ABILITY = Ability.GUN;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return "Hand out " + n.getAlias(Gun.ALIAS) + " so others can shoot them.";
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray("Hand out " + n.getAlias(Gun.ALIAS) + " so others can shoot them.");
    }

    public static final String GetDeathFeedback(Narrator n) {
        return "You were killed by a " + n.getAlias(Gun.ALIAS) + " wielder!";
    }

    public static final String GetFaultyDeathFeedback(Narrator n) {
        return "You accidentally killed yourself because your " + n.getAlias(Gun.ALIAS) + " was sabotged!";
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
        noTeamMateTargeting(a);
        if(!FaultyGuns(a.owner) && a.getOption() != null && !a.getOption().equalsIgnoreCase(REAL))
            Exception(Util.TitleCase(a.owner.narrator.getAlias(Gun.ALIAS))
                    + " can only be real. Please omit any action options.");
        if(FaultyGuns(a.owner) && a.getOption() != null
                && (!a.getOption().equalsIgnoreCase(REAL) && !a.getOption().equalsIgnoreCase(FAULTY)))
            Exception("Unable to tell whether " + a.owner.narrator.getAlias(Gun.ALIAS) + " should real or faulty");

        if(FaultyGuns(a.owner) && a.getOption() == null)
            a.setOption(REAL);
    }

    @Override
    public ArrayList<Option> getOptions(Player p) {
        if(!FaultyGuns(p))
            return super.getOptions(p);
        String g_opt = p.narrator.getAlias(Gun.ALIAS);
        ArrayList<Option> options = new ArrayList<>();
        options.add(new Option(REAL, REAL + " " + g_opt));
        options.add(new Option(FAULTY, FAULTY + " " + g_opt));
        return options;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        PlayerList targets = getActionTargets(actions);

        if(actions.get(0).getOption() != null && actions.get(0).getOption().equalsIgnoreCase(FAULTY)){
            if(targets.size() == 1)
                list.add("give a faulty gun to ");
            else
                list.add("give faulty guns to ");
        }else{
            if(targets.size() == 1)
                list.add("give a gun to ");
            else
                list.add("give guns to ");
        }
        list.addAll(StringChoice.YouYourself(actions.get(0).owner, targets));

        return list;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target != null)
            a.markCompleted();
        GunsmithAction(owner, target, a.getOption());
    }

    public static String GetGunReceiveMessage(Narrator n) {
        String ret = "You received a ";
        if(n.getBool(SetupModifier.GS_DAY_GUNS))
            ret += "day-use ";
        else
            ret += "night-use ";
        ret += n.getAlias(Gun.ALIAS) + "!";

        return ret;
    }

    public static void GunsmithAction(Player gs, Player target, String gOption) {
        if(target == null)
            return;
        if(gs.isAlive() && gs.getFaction().knowsTeam() && gs.getFaction() == target.getFaction()){
            NoNightActionVisit(gs, target);
            return;
        }

        Narrator n = gs.narrator;

        if(!target.isSquelched()){
            Gun g = new Gun(gs);
            boolean faulty = FaultyGuns(gs) && FAULTY.equalsIgnoreCase(gOption);
            if(faulty)
                g.setFaulty();
            target.addGun(g);
            FeedbackGenerator(new Feedback(target));
            if(faulty)
                happening(gs, " gave a sabotoged " + n.getAlias(Gun.ALIAS) + " to ", target);
            else
                happening(gs, " gave a " + n.getAlias(Gun.ALIAS) + " to ", target);
        }else{
            happening(gs, " tried to give a " + n.getAlias(Gun.ALIAS) + " to ", target);
        }
        gs.visit(target); // visited in the actual night action call, not the static one
    }

    private static void FeedbackGenerator(Feedback feedback) {
        Narrator narrator = feedback.n;
        feedback.add(GetGunReceiveMessage(feedback.player.narrator)).setPicture("mafioso");
        for(FactionRole factionRole: narrator.getPossibleMembers()){
            if(hasFaultyGuns(factionRole)){
                feedback.addExtraInfo("There is a chance that your gun might kill you on use.");
                break;
            }
        }

        if(narrator.getPossibleMembers().contains(new AbilityList(new DrugDealer())))
            feedback.addExtraInfo("There is a chance that your gun won't do anything");
    }

    private static boolean hasFaultyGuns(FactionRole factionRole) {
        // TODO
        // account for possibility of modifier on the faction role, but not the role
        for(Ability a: factionRole.role._abilities){
            if(a.modifiers != null && a.modifiers.hasKey(AbilityModifier.GS_FAULTY_GUNS))
                return a.modifiers.getBoolean(AbilityModifier.GS_FAULTY_GUNS);
        }
        return false;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.getFaction().knowsTeam() && p.getFaction().getMembers().size() > 1){
            ret.add("Your " + p.narrator.getAlias(Gun.ALIAS) + "s cannot go to teammates.");
        }
        return ret;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.GS_DAY_GUNS);
        return rules;
    }

    @Override
    public List<AbilityModifier> getAbilityModifiers() {
        List<AbilityModifier> rules = new LinkedList<>();
        rules.add(AbilityModifier.GS_FAULTY_GUNS);
        rules.addAll(super.getAbilityModifiers());
        return rules;
    }

    @Override
    public boolean isBool(Modifier modifier) {
        if(AbilityModifier.GS_FAULTY_GUNS == modifier)
            return true;
        return super.isBool(modifier);
    }

    @Override
    public Action parseCommand(Player p, ArrayList<String> commands) {
        if(!FaultyGuns(p))
            return super.parseCommand(p, commands);

        if(!commands.get(commands.size() - 1).equalsIgnoreCase(FAULTY))
            return super.parseCommand(p, commands);

        commands.remove(commands.size() - 1);
        return super.parseCommand(p, commands).setOption(FAULTY);
    }

    @Override
    public String getUsage(Player p, Random r) {
        if(FaultyGuns(p))
            return super.getUsage(p, r) + " " + REAL + "\n" + super.getUsage(p, r) + " " + FAULTY;
        return super.getUsage(p, r);
    }

    @Override
    public ArrayList<String> getCommandParts(Action action) {
        ArrayList<String> parts = super.getCommandParts(action);
        if(faultyGuns() && FAULTY.equalsIgnoreCase(action.getOption())){
            parts.add(FAULTY);
        }
        return parts;
    }

    private boolean faultyGuns() {
        return FaultyGuns(this);
    }

    public static boolean FaultyGuns(Player p) {
        if(p.hasAbility(Gunsmith.class))
            return FaultyGuns(p.getAbility(Gunsmith.class));
        if(p.hasAbility(Blacksmith.class))
            return FaultyGuns(p.getAbility(Blacksmith.class));
        else if(p.hasAbility(GraveDigger.class)){
            for(Player q: p.narrator._players){
                if(q.hasAbility(Gunsmith.class) || q.hasAbility(Blacksmith.class)){
                    if(FaultyGuns(q))
                        return true;
                }
            }
            return false;
        }
        throw new IllegalActionException(
                "Cannot test for faulty " + p.narrator.getAlias(Gun.ALIAS) + " on this player");
    }

    public static boolean FaultyGuns(Ability r) {
        return r.modifiers != null && r.modifiers.getOrDefault(AbilityModifier.GS_FAULTY_GUNS, false);
    }

    @Override
    public String getNightText(Player p) {
        String ret = "Type " + NQuote(COMMAND) + " to give a " + p.narrator.getAlias(Gun.ALIAS)
                + " to this this person.";
        if(FaultyGuns(p))
            ret += " To give a " + p.narrator.getAlias(Gun.ALIAS) + " that will kill the user, type "
                    + SQuote(COMMAND + " playerName FAULTY") + ".";
        return ret;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Gunsmith", Gunsmith.class);
        return FactionService.createFactionRole(faction, role);
    }
}
