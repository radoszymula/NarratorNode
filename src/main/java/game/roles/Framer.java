package game.roles;

import java.util.ArrayList;
import java.util.List;

import game.event.Happening;
import game.logic.Faction;
import game.logic.MemberList;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownPlayerException;
import game.logic.exceptions.UnknownTeamException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Framer extends Ability {

    public static final int MAIN_ABILITY = Ability.FRAME;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getNightText(Player p) {
        Narrator n = p.narrator;
        StringBuilder teams = new StringBuilder();
        for(Option color: getOptions(p)){
            teams.append(color.getName());
            teams.append(", ");
        }
        return "Type " + SQuote("frame player_name faction") + " to frame a target.\n" + "These are the list of teams: "
                + teams.substring(0, teams.length() - 2) + ".\n" + "For example 'frame Sue "
                + n.getFactions().get(0).getName() + "'.";
    }

    @Override
    public ArrayList<Option> getOptions(Player p) {
        Narrator n = p.narrator;
        ArrayList<Option> availableTeams = new ArrayList<>();
        for(Faction t: n.getFactions()){
            availableTeams.add(new Option(t));
        }
        return availableTeams;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Frame other people for murder, changing most investigation results on them.";
    public static final String COMMAND = "Frame";

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();
        Action a = actionList.get(0);
        Player owner = a.owner;

        for(Action act: actionList){
            Player target = act.getTarget();
            list.add(COMMAND.toLowerCase());
            list.add(" ");
            list.add(StringChoice.YouYourselfSingle(owner, target));
            Faction parse = owner.narrator.getFaction(act.getOption());
            if(parse == null)
                parse = owner.narrator.getFactionByName(act.getOption());
            if(parse != null){
                list.add(" as " + parse.getName());
            }
            list.add(" and ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    public static final int FRAME_ = MAIN_ABILITY;

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        Narrator n = owner.narrator;
        if(target == null)
            return;
        if(a.getOption() == null){
            NoNightActionVisit(a);
            return;
        }
        Player sender;
        for(Faction t: owner.getFactions()){
            if(!t.hasSharedAbilities()){
                continue;
            }
            for(Ability tAbility: t.getAbilities()){
                sender = t.getFactionAbilityController(tAbility.getAbilityNumber());
                if(sender == null)
                    continue;

                for(Action senderAction: sender.getActions().getActions()){
                    if(senderAction.is(tAbility.getAbilityNumber()) && senderAction.isCompleted()){
                        // won't trigger the electro here.
                        target.visit(Player.FAKE_VISIT, senderAction.getTarget());
                    }
                }
            }

        }
        MemberList mList = n.getPossibleMembers();
        List<FactionRole> tMembers = mList.getFactionRoles(a.getOption());
        if(tMembers.isEmpty())
            return;

        int elem = n.getRandom().nextInt(tMembers.size());

        FactionRole m = tMembers.get(elem);
        target.setFramed(m);

        new Happening(n).add(owner, " framed ", target, " as ", new HTString(m.getName(), n.getFaction(a.getOption())));
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        if(a.ability != MAIN_ABILITY)
            return super.getCommandParts(a);
        ArrayList<String> parsedCommands = new ArrayList<>();
        parsedCommands.add(COMMAND);
        parsedCommands.add(a.getTarget().getName());
        parsedCommands.add(a.getOption());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, ArrayList<String> commands) {

        String overflow, toAdd;
        while (commands.size() > 3){
            overflow = commands.remove(3);
            toAdd = commands.remove(2);
            commands.add(2, toAdd + overflow);
        }

        if(commands.size() != 3){
            throw new PlayerTargetingException("You need a player target and a team name to frame.");
        }
        commands.remove(0);
        Narrator n = p.narrator;

        String playerName = commands.remove(0);
        Player pi = n.getPlayerByName(playerName);
        if(pi == null)
            throw new UnknownPlayerException(playerName);
        String frame = commands.get(0);
        String teamColor = CommandHandler.parseTeam(frame, n);
        if(Constants.A_INVALID.equals(teamColor))
            throw new UnknownTeamException(frame);

        return new Action(p, pi, MAIN_ABILITY, frame);
    }

    @Override
    public ArrayList<String> getPublicDescription(Narrator narrator, String actionOriginatorName, String color, AbilityList abilityList) {
        ArrayList<String> list = super.getPublicDescription(narrator, actionOriginatorName, color, abilityList);
        list.add("May choose to frame as any alignment");

        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("framing ");
        return list;
    }

    @Override
    public String getUsage(Player p, Random r) {
        ArrayList<Option> options = getOptions(p);
        int index = r.nextInt(options.size());
        String ret = super.getUsage(p, r) + " " + options.get(index);
        if(options.size() != 1){
            ret += "\nAll frameable teams are are: ";
            for(int i = 0; i < options.size(); i++){
                ret += options.get(i);
                if(i != options.size() - 1){
                    ret += ", ";
                }
            }
        }
        return ret;
    }

    @Override
    public Action getExampleAction(Player owner) {
        return new Action(owner, MAIN_ABILITY, owner.getColor(), (String) null, owner);
    }

    @Override
    public Action getExampleWitchAction(Player owner, Player target) {
        return new Action(owner, target, Framer.MAIN_ABILITY, owner.getColor());
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Framer", Framer.class);
    }
}
