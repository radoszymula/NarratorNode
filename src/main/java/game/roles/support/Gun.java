package game.roles.support;

import java.util.List;

import game.event.DeathAnnouncement;
import game.event.Feedback;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.attacks.IndirectAttack;
import game.logic.support.rules.SetupModifiers;
import game.roles.Ability;
import game.roles.Burn;
import game.roles.ElectroManiac;
import game.roles.GunAbility;
import game.roles.Vigilante;

public class Gun extends Consumable {

    public static final String IMAGINARY_GUN = "You raise your gun to eye level, take a deep breathe to aim, and notice you actually have no gun in your hand!";
    public static final String COMMAND = GunAbility.COMMAND;
    public static final int MAIN_ABILITY = GunAbility.MAIN_ABILITY;
    public static final String ALIAS = "gun_alias";

    private String[] flag = Constants.GUN_KILL_FLAG;

    Player smith;

    public Gun(Player smith) {
        this.smith = smith;
    }

    public String getAbilityDescription(Narrator n) {
        return "use your " + n.getAlias(ALIAS) + " to kill";
    }

    @Override
    public String toString() {
        if(isReal()){
            if(flag == Constants.VIGILANTE_KILL_FLAG)
                return "VigiGun";
            return "Gun";
        }
        return "FakeGun";
    }

    @Override
    public int compareTo(Consumable q) {
        if(!(q instanceof Gun))
            return 0;

        Gun o = (Gun) q;

        if(flag == o.flag){
            if(isReal() == o.isReal())
                return 0;
            else if(o.isReal())
                return 1;
            else
                return -1;
        }
        if(flag == Constants.VIGILANTE_KILL_FLAG)
            return 1;
        return -1;

    }

    public String[] getFlag() {
        return flag;
    }

    public Gun setFlag(String[] flag) {
        this.flag = flag;
        return this;
    }

    public boolean shoot(Action a) {
        if(a.getTarget() == null)
            return false;
        Player target = a.getTarget(), owner = a.owner;
        Narrator narrator = a.owner.narrator;
        if(narrator.isNight()){
            if(faulty){
                if(a.owner.isInvulnerable()){
                    Ability.NoNightActionVisit(
                            new Action(a.owner, Player.list(a.owner), Ability.VISIT_ABILITY, (String) null, null));
                }else
                    Ability.Kill(a.owner, a.owner, Constants.GUN_SELF_KILL_FLAG, this.smith);
            }else if(isReal()){
                if(a.owner.is(Vigilante.class)){
                    GunAbility ga = a.owner.getAbility(GunAbility.class);
                    if(isReal() && ga.size() == 1 && getFlag() == Constants.VIGILANTE_KILL_FLAG)
                        if(owner.getAbility(Vigilante.class).getPerceivedCharges() == SetupModifiers.UNLIMITED)
                            a.owner.addGun(this);
                }
                Ability.Kill(a.owner, target, flag);// visiting taken care of already in kill
            }else
                Ability.NoNightActionVisit(a);
        }else{
            if(isFake()){
                Feedback f = new Feedback(a.owner, IMAGINARY_GUN);
                a.owner.sendMessage(f);

                Ability.happening(owner, " used their fake gun ", target);
            }else{
                PlayerList deadPeople = new PlayerList();
                DeathAnnouncement e = new DeathAnnouncement(deadPeople, narrator, !DeathAnnouncement.NIGHT_DEATH);
                e.add("BANG! ");
                if(!faulty){
                    if(target.isInvulnerable())
                        e.add("An attack rang through the courtyard, but no one appears to know where it came from.");
                    else
                        e.add(" ", target, " crumpled to the floor, blood gushing from their stomach.");
                }else{
                    if(a.owner.isInvulnerable())
                        e.add(" The shot rang through the courtyard, but no one appears to know where it came from.");
                    else
                        e.add(" ", a.owner, " crumpled to the floor, blood gushing from their stomach.");
                }
                a.owner.narrator.getEventManager().addCommand(a.owner, Constants.GUN_COMMAND, target.getName());

                narrator.announcement(e);

                if(!faulty){
                    if(!target.isInvulnerable()){
                        target.setDead(a.owner);
                        target.getDeathType().addDeath(flag);
                        deadPeople.add(target);
                    }
                }else{
                    if(!a.owner.isInvulnerable()){
                        a.owner.setDead(this.smith);
                        a.owner.getDeathType().addDeath(flag);
                        deadPeople.add(a.owner);
                    }
                }

                DeathAnnouncement explosion = null;

                if(!faulty){
                    if((target.is(ElectroManiac.class) || target.isCharged())
                            && (owner.isCharged() || owner.is(ElectroManiac.class))){
                        if(!owner.isInvulnerable() && !owner.is(ElectroManiac.class)){
                            owner.dayKill(new IndirectAttack(Constants.ELECTRO_KILL_FLAG, owner, owner.getCharger()),
                                    true); // dont want the checks to run = true
                            deadPeople.add(owner);
                        }
                        if(!target.isInvulnerable() && !target.is(ElectroManiac.class))
                            target.getDeathType().addDeath(Constants.ELECTRO_KILL_FLAG);
                        explosion = Burn.ExplosionAnnouncement(narrator, deadPeople, "electromaniac");
                        narrator.announcement(explosion);
                    }
                }

                narrator.voteSystem.removeFromVotes(deadPeople);
                narrator.checkProgress();
                if(!narrator.isInProgress())
                    return true;
                narrator.checkVote();

                if((!faulty && target.isInvulnerable()) || (faulty && a.owner.isInvulnerable()))
                    target = null;
                if(narrator.isInProgress()){
                    List<NarratorListener> listeners = narrator.getListeners();
                    for(int i = 0; i < listeners.size(); i++){
                        listeners.get(i).onAssassination(a.owner, target, e);
                        if(explosion != null)
                            listeners.get(i).onElectroExplosion(deadPeople, explosion);
                    }

                    narrator.parityCheck();
                }
            }
        }

        return true;
    }

    private boolean faulty = false;

    public void setFaulty() {
        faulty = true;

    }

    public boolean isFaulty() {
        return faulty;
    }
}
