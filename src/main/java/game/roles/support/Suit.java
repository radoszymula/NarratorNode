package game.roles.support;

import game.logic.Player;
import game.logic.support.action.Action;
import game.roles.AbilityList;

public class Suit {

    public String roleName;
    public String color;
    public Player tailor;

    public Suit(Action action) {
        roleName = action.getOption2();
        color = action.getOption();
        this.tailor = action.owner;
    }

    public boolean createdBy(Player tailor) {
        return tailor == this.tailor;
    }

    public AbilityList getBaseRole() {
        return tailor.narrator.getPossibleMembers().translate(roleName);
    }

}
