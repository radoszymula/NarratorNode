package game.roles.support;

import game.event.Feedback;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.Baker;
import game.roles.BreadAbility;

public class Bread extends Consumable {

    public static final String COMMAND = BreadAbility.COMMAND;
    public static final int MAIN_ABILITY = BreadAbility.MAIN_ABILITY;
    public static final String ALIAS = "bread_alias";

    private boolean initialized = false;
    private boolean expiring = false;

    @Override
    public String toString() {
        if(isReal())
            return "Bread";
        return "FakeBread";
    }

    @Override
    public int compareTo(Consumable q) {
        if(!(q instanceof Bread))
            return 0;
        Bread o = (Bread) q;
        if(o.isReal() == isReal())
            return 0;
        if(o.isReal())
            return 1;
        return -1;
    }

    public static Bread FakeBread() {
        Bread b = new Bread();
        b.setReal(false);
        b.initialize();
        return b;
    }

    public Bread initialize() {
        initialized = true;
        return this;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public static void PassBread(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        if(owner.getFaction().knowsTeam() && owner.getFaction() == target.getFaction()){
            Ability.NoNightActionVisit(a);
            return;
        }

        BreadAbility ba = a.owner.getAbility(BreadAbility.class);

        if(!ba.hasItems()){
            Ability.NoNightActionVisit(a);
            return;
        }

        if(!a.owner.narrator.getBool(SetupModifier.BREAD_PASSING) && !a.owner.is(Baker.class)){
            Ability.NoNightActionVisit(a);
            return;
        }

        Bread b = ba.remove(0);
        ba.addConsumable(Bread.ExpiringBread());

        if(b.isFake() || target.isSquelched()){
            Ability.happening(owner, " tried to give " + GetBreadName(a) + " to ", target);
        }else{
            target.addBread(new Bread().initialize());
            Ability.happening(owner, " gave " + GetBreadName(a) + " to ", target);
            FeedbackDecorator(new Feedback(target), a);
        }

        a.markCompleted();
        owner.visit(target);
    }

    public boolean isExpired() {
        return expiring;
    }

    private static Bread ExpiringBread() {
        Bread b = new Bread();
        b.expiring = true;
        return b;
    }

    private static String GetBreadName(Action a) {
        return a.owner.narrator.getAlias(ALIAS);
    }

    public static String GetFeedback(Narrator n) {
        return Baker.BreadReceiveMessage(n);
    }

    public String getAbilityDescription(Narrator n) {
        return "pass one of your " + n.getAlias(ALIAS) + " to someone else for their use";
    }

    // TODO move this breadability
    public static void removeExpiredBread(PlayerList _players) {
        BreadAbility ba;
        for(Player p: _players){
            if(!p.hasAbility(BreadAbility.class))
                continue;
            ba = p.getAbility(BreadAbility.class);
            for(int i = 0; i < ba.size(); i++){
                if(ba.get(i).isExpired()){
                    ba.remove(i);
                    i--;
                }
            }
        }

    }

    public static void FeedbackDecorator(Feedback fb, Action a) {
        fb.add(Baker.BreadReceiveMessage(a.owner.narrator));
        fb.setPicture("baker");

        fb.addExtraInfo("You may use this on subsequent nights(or days) to do more than one of your actions.");
        fb.addExtraInfo("For example, a mafia member could carry out 2 kills instead of just 1.");
    }

}
