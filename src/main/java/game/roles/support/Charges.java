package game.roles.support;

public class Charges {

    private int charges;
    private int fakeCharges;

    public Charges(int charge) {
        this.charges = charge;
        fakeCharges = 0;
    }

    public int getPerceivedCharges() {
        return charges + fakeCharges;
    }

    public int getRealCharges() {
        return charges;
    }

    public void useCharge() {
        charges--;
        if(charges < 0){
            charges = 0;
            fakeCharges--;
            if(fakeCharges < 0)
                fakeCharges = 0;
        }
    }

    public void addFakeCharge() {
        fakeCharges++;
    }

    public int getFakeCharges() {
        return fakeCharges;
    }

    public void useFakeCharge() {
        fakeCharges--;
        if(fakeCharges < 0)
            fakeCharges = 0;
    }

}
