package game.roles.support;

import java.util.List;

import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.event.SelectionMessage;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.exceptions.UnsupportedMethodException;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.CultLeader;

public class CultInvitation extends Ability {

    public static final int MAIN_ABILITY = Ability.INVITATION_ABILITY;
    private Player cultLeader, recruit;
    public boolean accepted = false;

    public CultInvitation(Player cultLeader, Player recruit) {
        this.cultLeader = cultLeader;
        this.recruit = recruit;
        new Feedback(recruit, "You have been invited to join the " + cultLeader.getFaction().getName());
    }

    public CultInvitation() {
        // this should only be used in Ability.parse
    }

    public void confirm() {
        accepted = true;

        SelectionMessage e = new SelectionMessage(recruit, false);
        e.recordCommand(recruit, Constants.ACCEPT);
        e.add("You've accepted the invitation to join the cult.");
        e.dontShowPrivate();
        e.pushOut();

        List<NarratorListener> listeners = recruit.narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onTargetSelection(recruit, e);
    }

    public void decline() {
        accepted = false;

        SelectionMessage e = new SelectionMessage(recruit, false);
        e.recordCommand(recruit, Constants.DECLINE);
        e.add("You've declined the invitation to join the cult.");
        e.dontShowPrivate();
        e.pushOut();

        List<NarratorListener> listeners = recruit.narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onTargetSelection(recruit, e);
    }

    public void pushOut() {
        Message q = new Happening(cultLeader.narrator); // day has already ended, but i want it on the previous day
        if(!accepted){
            q.add(recruit, " rejected ", cultLeader, "'s request to join the " + cultLeader.getFaction().getName());
            return;
        }
        q.add(recruit, " accepted ", cultLeader, "'s request to join the " + cultLeader.getFaction().getName());

        CultLeader cl = (CultLeader) cultLeader.getAbility(CultLeader.MAIN_ABILITY);
        Narrator n = cultLeader.narrator;
        if(n.getBool(SetupModifier.CULT_KEEPS_ROLES) && recruit.isPowerRole())
            cl.setPowerCD(n.getInt(SetupModifier.CULT_POWER_ROLE_CD) + 1);

        // reseting cooldown since its a successful cooldown
        cl.triggerCooldown();
        cl.useCharge();

        recruit.setTeam(cultLeader.getFaction());

        HTString ht = new HTString(cultLeader.getFaction().getName(), cultLeader.getColor());

        SelectionMessage e = new SelectionMessage(cultLeader, false);
        // e.setCommand(recruit, Constants.DECLINE);
        e.add("Your recruitment was succesful.");
        e.dontShowPrivate();
        e.pushOut();

        e = new SelectionMessage(recruit, false);
        // e.setCommand(recruit, Constants.DECLINE);
        e.add(cultLeader, " converted you to the ", ht, ". Your teammates are : ", cultLeader.getFaction().getMembers(),
                ".");
        e.dontShowPrivate();
        e.pushOut();

        cultLeader.getFaction().addMember(recruit);

    }

    public static final String COMMAND = "invitation";

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return INVITATION_ABILITY;
    }

    @Override
    public void doNightAction(Action a) {
        throw new UnsupportedMethodException();
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        // TODO Auto-generated method stub
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        // TODO Auto-generated method stub
        return null;
    }
}
