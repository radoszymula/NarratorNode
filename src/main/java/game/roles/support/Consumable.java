package game.roles.support;

public abstract class Consumable implements Comparable<Consumable> {

    private boolean isUsed = false;
    private boolean isReal = true;

    public Consumable setReal(boolean real) {
        isReal = real;
        return this;
    }

    public boolean isReal() {
        return isReal;
    }

    public boolean isFake() {
        return !isReal;
    }

    public void markUsed() {
        isUsed = true;
    }

    public void markUnused() {
        isUsed = false;
    }

    public boolean isUsed() {
        return isUsed;
    }
}
