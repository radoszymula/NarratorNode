package game.roles.support;

import game.logic.Narrator;
import game.roles.VestAbility;

public class Vest extends Consumable {

    public static final String ALIAS = "vest_alias";
    public static final String COMMAND = VestAbility.COMMAND;
    public static final int MAIN_ABILITY = VestAbility.MAIN_ABILITY;

    public Vest() {
        armorSmithVest = false;
    }

    public String getAbilityDescription(Narrator n) {
        return "use a " + n.getAlias(ALIAS) + " to survive the night";
    }

    public boolean armorSmithVest;

    @Override
    public String toString() {
        if(isReal())
            return "Vest";
        return "FakeVest";
    }

    @Override
    public int compareTo(Consumable q) {
        if(!(q instanceof Vest))
            return 0;
        Vest o = (Vest) q;
        if(o.isReal() == isReal())
            return 0;
        if(o.isReal())
            return 1;
        return -1;
    }

    public static Vest FakeVest() {
        Vest v = new Vest();
        v.setReal(false);
        return v;
    }
}
