package game.roles.support;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.rules.SetupModifier;
import game.roles.Joker;

public class Bounty {

    public int start_day;
    public Player rewarder, target;

    public Bounty(Player bountyPlacer, Player bountyHolder, int day) {
        if(bountyPlacer == null || bountyHolder == null)
            throw new NullPointerException();
        this.rewarder = bountyPlacer;
        this.target = bountyHolder;
        this.start_day = day;
    }

    public boolean failed() {
        int incubation = rewarder.narrator.getInt(SetupModifier.BOUNTY_INCUBATION);
        return target.isAlive() && rewarder.narrator.getDayNumber() >= this.start_day + incubation;
    }

    public void dayAnnounce() {
        Narrator n = rewarder.narrator;
        n.announcement(Joker.DayStartAnnouncementCreator(target,
                start_day + n.getInt(SetupModifier.BOUNTY_INCUBATION) - n.getDayNumber() + 1));
    }

    public void onFail() {
        Narrator n = rewarder.narrator;
        n.announcement(Joker.DayEndAnnouncementCreator(target));
    }

}
