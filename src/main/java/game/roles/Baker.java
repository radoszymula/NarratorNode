package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.support.Bread;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Baker extends Passive {

    public static final String COMMAND = "Bread";

    public Baker() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(GetNightActionDescription(n));
    }

    @Override
    public boolean isPowerRole() {
        return true;
    }

    public static String GetNightActionDescription(Narrator n) {
        return "Give " + n.getAlias(Bread.ALIAS)
                + " to people at night so they can perform additional actions other nights.";
    }

    public static String BreadReceiveMessage(Narrator n) {
        return "You received " + n.getAlias(Bread.ALIAS) + "!";
    }

    @Override
    public ArrayList<Object> getActionDescription(Action a) {
        ArrayList<Object> list = new ArrayList<>();

        list.add(" give " + GetBreadName(p) + " to ");
        list.add(StringChoice.YouYourself(a.owner, a.getTargets()));

        return list;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.getFaction().knowsTeam() && p.getFaction().getMembers().size() > 1){
            ret.add(Util.TitleCase(GetBreadName(p)) + " that you make cannot go to teammates.");
        }

        if(p.narrator.getBool(SetupModifier.SELF_BREAD_USAGE)){
            ret.add("You may use or hold onto the " + GetBreadName(p) + " that you make");
        }

        return ret;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.BREAD_PASSING);
        rules.add(SetupModifier.SELF_BREAD_USAGE);
        return rules;
    }

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        if(!isOnCooldown() && getPerceivedCharges() != 0){
            Bread b = new Bread();
            if(getRealCharges() == 0)
                b.setReal(false);
            p.addBread(b);
        }
    }

    @Override
    public String getCooldownText(Player p) {
        int cd = getAbilityCooldown();
        if(cd == 0)
            return null;
        if(cd == 1)
            return "You will receive a passable " + GetBreadName(p) + " every other day";
        return "You will receive a passable " + GetBreadName(p) + " that may be used every " + cd + " days";
    }

    private String GetBreadName(Player p) {
        return p.narrator.getAlias(Bread.ALIAS);
    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);

        BreadAbility ba = p.getAbility(BreadAbility.class);
        if(ba == null)
            return;
        Bread b;
        for(int i = 0; i < ba.size(); i++){
            b = ba.get(i);
            if(!b.isInitialized()){
                if(p.narrator.getBool(SetupModifier.SELF_BREAD_USAGE)){
                    b.initialize();
                    triggerCooldown();
                    useCharge();
                }else{
                    ba.remove(i);
                }
                return;
            }
        }

        if(getRemainingCooldown() == 0){
            triggerCooldown();
            useCharge();
        }
    }

    @Override
    public boolean hasCooldownAbility() {
        return true;
    }

    @Override
    public boolean chargeModifiable() {
        return true;
    }

    @Override
    public String getChargeDescription(Narrator n) {
        return Ability.CHARGE_DESCRIPTION + " for " + n.getAlias(Bread.ALIAS).toLowerCase() + " making";
    }

    @Override
    public boolean isActivePassive(Player p) {
        return this.modifiers.getOrDefault(AbilityModifier.CHARGES, 0) != 0;
    }

    @Override
    public void onAbilityLoss(Player p) {
        BreadAbility ba = p.getAbility(BreadAbility.class);
        Bread b;
        for(int i = 0; i < ba.size(); i++){
            b = ba.get(i);
            if(!b.isInitialized())
                ba.remove(i);
        }
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Baker", Baker.class);
    }
}
