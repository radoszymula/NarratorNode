package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.exceptions.UnknownPlayerException;
import game.logic.support.HTString;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionRoleService;
import services.RoleService;

public class CultLeader extends Ability {

    public static final String ROLE_NAME = "Cult Leader";

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final int MAIN_ABILITY = Ability.CONVERT;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.CULT_KEEPS_ROLES);
        rules.add(SetupModifier.CULT_POWER_ROLE_CD);
        rules.add(SetupModifier.CONVERT_REFUSABLE);
        rules.add(SetupModifier.CULT_PROMOTION);
        rules.add(SetupModifier.GUARD_REDIRECTS_DOUSE);
        return rules;
    }

    private int powerCD;

    private int getPowerCD() {
        return powerCD;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Recruit others into your cult, converting them to your side.";

    public void setPowerCD(int i) {
        powerCD = i;
    }

    @Override
    public String getNightText(Player p) {
        return "You have the ability to convert somoene to your team.";
    }

    public static final String COMMAND = "Convert";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);

        if(getPowerCD() > 0)
            Exception("You must wait " + getPowerCD() + " more days to convert again.");

        noTeamMateTargeting(a);
        if(!a.owner.narrator.getBool(SetupModifier.CULT_PROMOTION) && a.getOption() != null){
            Exception("No promotions are allowed");
        }else if(a.getOption() != null){
            for(Option o: getOptions(p)){
                if(o.getValue().equalsIgnoreCase(a.getOption()))
                    return;
            }
            Exception("Unknown promotion ability");
        }
    }

    private void invitation(Player owner, Player target) {
        Message q = new Happening(target.narrator);
        q.add(owner, " invited ", target, " to join the " + owner.getFaction().getName());

        target.addCultInvitation(owner);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        Narrator narrator = owner.narrator;

        if(target == null)
            return;

        boolean actionReady = getPowerCD() <= 0;

        if(target.narrator.getBool(SetupModifier.GUARD_REDIRECTS_CONVERT))
            target = Bodyguard.redirect(target);

        if(narrator.pre_recruits){
            if(actionReady){
                if(target.cultAttempt == null)
                    target.cultAttempt = Player.list(owner);
                else if(target.is(Infiltrator.class))
                    target.cultAttempt.add(owner);
                else
                    target.cultAttempt = Player.list(owner.getSkipper());
            }
            return;
        }

        if(!actionReady){
            NoNightActionVisit(a);
            return;
        }

        if(target.cultAttempt.contains(owner) && (target.getFaction().canRecruitFrom() || target.is(Infiltrator.class))
                && (!Mason.IsMasonType(target) || target.is(Infiltrator.class)) && !target.is(CultLeader.class)
                && target.isRecruitable() && !target.willBe(Mason.class) && !target.isEnforced() && !target.isJailed()){
            if(narrator.getBool(SetupModifier.CONVERT_REFUSABLE) && (target.isPowerRole() && !target.is(Infiltrator.class))){
                invitation(owner, target);
                a.markCompleted();
                return;
            }

            if(narrator.getBool(SetupModifier.CULT_KEEPS_ROLES) && target.isPowerRole())
                setPowerCD(narrator.getInt(SetupModifier.CULT_POWER_ROLE_CD) + 1);

            // reseting cooldown since its a successful cooldown

            if(!target.is(Infiltrator.class))
                target.setCulted();

            String simpleName = target.getAbilities().get(0).getClass().getSimpleName();
            String recruit = null;
            if(!narrator.getBool(SetupModifier.CULT_KEEPS_ROLES) && !target.is(Infiltrator.class)){
                if(narrator.getBool(SetupModifier.CULT_PROMOTION) && !target.isPowerRole() && !target.hasActivePassive()
                        && a.getOption() != null && !a.getOption().equalsIgnoreCase(narrator.getAlias(MINION_ALIAS))){
                    recruit = a.getOption();
                    Role role = RoleService.getOrCreateDeprecated(narrator, Ability.CREATOR(a.getOption()));
                    FactionRole factionRole = FactionRoleService.getOrCreateDeprecated(role, owner.getColor());
                    target.changeRole(factionRole);
                }else if(narrator.getBool(SetupModifier.CULT_PROMOTION) && target.getAbilities().size() == 1
                        && getOptions(owner).contains(new Option(simpleName))
                        && (a.getOption() == null || a.getOption().equalsIgnoreCase(narrator.getAlias(MINION_ALIAS))
                                || a.getOption().equalsIgnoreCase(Cultist.class.getSimpleName()))){
                    // do nothing
                }else{
                    Role role = RoleService.getOrCreateDeprecated(narrator, new Cultist());
                    FactionRole factionRole = FactionRoleService.getOrCreateDeprecated(role, owner.getColor());
                    target.changeRole(factionRole);
                }
            }else if(target.is(Citizen.class)){
                Role role = RoleService.getOrCreateDeprecated(narrator, new Cultist());
                FactionRole factionRole = FactionRoleService.getOrCreateDeprecated(role, owner.getColor());
                target.changeRole(factionRole);
            }

            if(!target.is(Infiltrator.class))
                target.setTeam(owner.getFaction());
            owner.getFaction().addMember(target);
            triggerCooldown();
            useCharge();

            if(owner.getFaction().hasNightChat())
                target.addChat(narrator.getEventManager().getNightLog(owner.getColor()));

            HTString ht = new HTString(owner.getFaction().getName(), owner.getColor());

            Feedback recruitResult = new Feedback(owner);
            recruitResult.hideableFeedback = false;
            recruitResult.add("Your recruitment was succesful. ");
            recruitResult.add(target.getName() + " is now part of your following.");
            recruitResult.setPicture("mayor");

            Feedback e = new Feedback(target);
            e.hideableFeedback = false;
            e.add(owner, " converted you to the ", ht, ". Your teammates are : ", owner.getFaction().getMembers(), ".");
            e.setPicture("cultist");

            Message q = new Happening(narrator);
            q.add(owner, " recruited ", target, " to the " + owner.getFaction().getName());
            if(recruit != null)
                q.add(" as a ", recruit);
            q.add(".");
            owner.visit(target);
            a.markCompleted();
        }else{
            Feedback recruitResult = new Feedback(owner);
            recruitResult.hideableFeedback = false;
            recruitResult.add("You failed to recruit your target");
            happening(owner, " failed to recruit ", target);

            if(target.isEnforced() && !target.isJailed()){
                Enforcer.addFeedback(target.getEnforcer(), owner);
            }

            a.markCompleted();
            owner.visit(target);
        }
    }

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        if(getPowerCD() > 0)
            setPowerCD(getPowerCD() - 1);
    }

    @Override
    public String getCooldownText(Player p) {
        int cd = Math.max(this.getRemainingCooldown(), getPowerCD());
        if(cd > 0)
            return "You must wait " + cd + " more days to convert again.";
        return null;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.CULT_KEEPS_ROLES)){
            ret.add("If you convert power roles, they will keep their role and ability.");
        }else{
            ret.add("Anyone you sucessfully convert will become a " + p.narrator.getAlias(MINION_ALIAS) + ".");
        }

        if(p.narrator.getBool(SetupModifier.CONVERT_REFUSABLE)){
            ret.add("Power roles can refuse your request.");
        }else{
            ret.add("No one can refuse your invitation.");
        }

        if(p.narrator.getBool(SetupModifier.CULT_PROMOTION) && p.hasAbility(CultLeader.class)){
            ArrayList<Option> remRoles = p.getAbility(CultLeader.class).getOptions(p);
            if(remRoles.isEmpty()){
                ret.add("You've exhausted your available recruits");
            }else{
                StringBuilder sb = new StringBuilder(
                        "You may try to convert recruits into one of the following roles: ");
                for(int i = 0; i < remRoles.size(); i++){
                    if(i != 0)
                        sb.append(", ");
                    sb.append(remRoles.get(i));
                }
                sb.append('.');
                ret.add(sb.toString());
            }

        }else if(!p.narrator.getBool(SetupModifier.CULT_PROMOTION)){
            ret.add("You may not promote anyone in this game");
        }

        if(p.narrator.getBool(SetupModifier.GUARD_REDIRECTS_CONVERT)){
            ret.add("If your target is guarded, you will recruit that person instead.");
        }

        return ret;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        ArrayList<String> parsedCommands = new ArrayList<>();
        parsedCommands.add(COMMAND);
        parsedCommands.add(a.getTarget().getName());
        if(a.owner.narrator.getBool(SetupModifier.CULT_PROMOTION) && a.getOption() != null)
            parsedCommands.add(a.getOption());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, ArrayList<String> commands) {
        commands.remove(0);
        Narrator n = p.narrator;

        String playerName = commands.remove(0);
        Player pi = n.getPlayerByName(playerName);
        if(pi == null)
            throw new UnknownPlayerException(playerName);
        String opt;
        if(!commands.isEmpty() && n.getBool(SetupModifier.CULT_PROMOTION))
            opt = commands.get(0);
        else
            opt = null;

        return new Action(p, pi, MAIN_ABILITY, opt);
    }

    @Override
    public String getUsage(Player p, Random r) {
        ArrayList<Option> options = getOptions(p);
        int index = r.nextInt(options.size());
        if(p.narrator.getBool(SetupModifier.CULT_PROMOTION)){
            String ret = super.getUsage(p, r) + " " + options.get(index);
            if(options.size() != 1){
                ret += "\nAll promotions are: ";
                for(int i = 0; i < options.size(); i++){
                    ret += options.get(0);
                    if(i != options.size() - 1){
                        ret += ", ";
                    }
                }
            }
            return ret;
        }
        return super.getUsage(p, r);
    }

    private static final String[] PROMOTES = {
            Sheriff.class.getSimpleName(), Coroner.class.getSimpleName(), Detective.class.getSimpleName(),
            Investigator.class.getSimpleName(), Lookout.class.getSimpleName()
    };

    public static final String MINION_ALIAS = "minion_alias";

    @Override
    public ArrayList<Option> getOptions(Player p) {
        if(!p.narrator.getBool(SetupModifier.CULT_PROMOTION))
            return super.getOptions(p);

        ArrayList<Option> promotes = new ArrayList<>();
        for(String name: PROMOTES){
            promotes.add(new Option(name));
        }

        Ability a;
        if(p.getFaction() != null)
            for(Player q: p.getFaction().getMembers()){
                for(String command: PROMOTES){
                    a = Ability.CREATOR(command);
                    if(q.hasAbility(a.getAbilityNumber())){
                        promotes.remove(new Option(command));
                    }
                }
            }

        Option o;
        if(!promotes.isEmpty()){
            o = new Option(Cultist.class.getSimpleName(), p.narrator.getAlias(MINION_ALIAS));
            promotes.add(o);
        }

        return promotes;
    }

    @Override
    public boolean allowsForFriendlyFire() {
        return true;
    }
}
