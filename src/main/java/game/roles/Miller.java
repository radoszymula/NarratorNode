package game.roles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import game.logic.Faction;
import game.logic.MemberList;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.RolesList;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import models.SetupHidden;
import services.FactionService;
import services.RoleService;

public class Miller extends Passive {

    public Miller() {
    }

    private FactionRole flip;

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        Narrator n = p.narrator;
        String millerColor = MillerColor(n);
        if(millerColor == null)
            return;

        Faction faction = n.getFaction(millerColor);
        p.setFramed(new FactionRole(faction, new Role(p.narrator, "Miller", new Miller())));
    }

    @Override
    public ArrayList<String> getPublicDescription(Narrator narrator, String actionOriginatorName, String nullableColor,
            AbilityList abilities) {
        ArrayList<String> ruleTextList = super.getPublicDescription(narrator, actionOriginatorName, nullableColor,
                abilities);
        String color = Miller.MillerColor(narrator);
        if(color != null && narrator.getPossibleMembers().abilityExists(narrator, Sheriff.class)){
            ruleTextList.add("Will turn up as " + narrator.getFaction(color) + " to sheriffs");
        }
        return ruleTextList;
    }

    public static String MillerColor(final Narrator narrator) {
        ArrayList<Faction> teams = narrator.getFactions();

        MemberList mList = narrator.initialMembers();
        for(FactionRole m: mList.getMemberWithAbilities(Miller.class)){
            teams.remove(narrator.getFaction(m.getColor()));
        }
        if(teams.isEmpty())
            return null;

        RolesList rolesList = narrator.getRolesList();
        Collections.sort(teams, new Comparator<Faction>() {
            private int teamSize(Faction t) {
                int i = 0;
                for(SetupHidden setupHidden: rolesList){
                    if(setupHidden.hidden.getColors().size() == 1)
                        if(setupHidden.hidden.getColors().contains(t.getColor()))
                            i++;
                }
                for(Player player: narrator.getDeadPlayers()){
                    if(player.getDeathType().isCleaned())
                        continue;
                    else if(player.hasSuits()){
                        if(player.suits.get(0).color.equals(t.getColor()))
                            i--;
                    }else if(player.getColor().equals(t.getColor()))
                        i--;
                }
                return i;
            }

            @Override
            public int compare(Faction o1, Faction o2) {
                if(o1.hasSharedAbilities() && o2.hasSharedAbilities())
                    return teamSize(o2) - teamSize(o1);
                if(o2.hasSharedAbilities())
                    return -1;
                if(o1.hasSharedAbilities())
                    return 1;
                int size1 = teamSize(o1);
                int size2 = teamSize(o2);
                if(size1 == size2)
                    return o1.getName().compareTo(o2.getName());
                return size1 - size2;
            }

        });
        return teams.get(0).getColor();
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Looks evil to investigative actions";

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player player) {
        ArrayList<String> ret = new ArrayList<>();

        String color = MillerColor(player.narrator);
        Faction t = player.narrator.getFaction(color);
        if(player.narrator.getBool(SetupModifier.MILLER_SUITED))
            ret.add("On your death, you will look like a " + t.getName() + " member.");
        else
            ret.add("On your death, you'll look normal.");

        if(!player.narrator.getPossibleMembers().abilityExists(player.narrator, Sheriff.class))
            return ret;

        if(color != null){
            ret.add("You will turn up as " + player.narrator.getFaction(color) + " to sheriffs.");
            return ret;
        }

        return ret;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.MILLER_SUITED);
        return rules;
    }

    @Override
    public void onDeath(Player p) {
        super.onDeath(p);
        if(!p.narrator.getBool(SetupModifier.MILLER_SUITED))
            return;
        String color = MillerColor(p.narrator);
        List<FactionRole> members = p.narrator.getPossibleMembers().getFactionRoles(color);
        int index = p.narrator.getRandom().nextInt(members.size());
        flip = members.get(index);
    }

    public static FactionRole getRoleFlip(Player p) {
        Miller m = p.getAbility(Miller.class);
        return m.flip;
    }

    @Override
    public void checkIsNightless(Narrator n) {
    }

    @Override
    public boolean hiddenPossible() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Miller", Miller.class);
        return FactionService.createFactionRole(faction, role);
    }

}
