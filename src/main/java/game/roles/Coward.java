package game.roles;

import java.util.ArrayList;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifiers;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Coward extends Ability {

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final int MAIN_ABILITY = Ability.HIDE;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    public static final String ROLE_NAME = "Coward";

    public static final String NIGHT_ACTION_DESCRIPTION = "Hide behind someone, redirecting all actions to the target";

    public static final String COMMAND = "Hide";

    @Override
    public void mainAbilityCheck(Action a) {
        if(getPerceivedCharges() == 0)
            Exception("You cannot hide anymore.");
        deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        target.secondaryCause(owner);

        owner.setHide(target);
        happening(owner, " hid behind ", target);
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    protected String getChargeText() {

        int charges = getPerceivedCharges();
        if(charges == SetupModifiers.UNLIMITED){
            return ("You can hide as many times as you want.");
        }else if(charges == 0){
            return ("You can't hide anymore.");
        }else if(charges == 1){
            return ("You can hide " + charges + " more time.");
        }else{
            return ("You can hide " + charges + " more times.");
        }
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        PlayerList targets = getActionTargets(actions);

        Player owner = actions.get(0).owner;
        list.add("hide behind ");
        list.addAll(StringChoice.YouYourself(owner, targets));

        return list;
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Coward", Coward.class);
    }
}
