package game.roles;

import java.util.ArrayList;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Faction;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;

public class Undouse extends Ability {

    public static final int MAIN_ABILITY = Ability.UNDOUSE;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String COMMAND = "Undouse";

    public String getNightText(ArrayList<Faction> t) {
        return "  Type " + NQuote(COMMAND) + " to undouse.";
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Remove dangerous flammable liquid from your target.";

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        Action a = actions.get(0);
        PlayerList target = getActionTargets(actions);
        Player owner = a.owner;
        ArrayList<Object> list = new ArrayList<>();

        list.add(COMMAND.toLowerCase() + " ");
        list.addAll(StringChoice.YouYourself(owner, target));

        return list;
    }

    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();

        target.setDoused(false);
        happening(owner, " undoused ", target);
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<Object>();
        list.add("undousing ");
        list.addAll(StringChoice.YouYourself(a.owner, a.getTargets()));
        return list;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public boolean isDatabaseAbility() {
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }
}
