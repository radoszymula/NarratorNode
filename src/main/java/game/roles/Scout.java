package game.roles;

import game.event.Feedback;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;

public class Scout extends Ability {

    public static final int MAIN_ABILITY = Ability.SCOUT;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Investigate someone, determing if they're recruitable or not.";

    public void mainAbilityCheck(Action a) {
        deadCheck(a);
        noTeamMateTargeting(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        Feedback(owner, target);

        happening(owner, " investigated ", target);
        a.markCompleted();
        owner.visit(target);
    }

    public static Feedback Feedback(Player scout, Player target) {
        String feedback;
        if(target.getFrameStatus() != null){
            feedback = NOT_RECRUITABLE;
        }else if(target.is(Infiltrator.class) || target.is(Citizen.class)){
            feedback = RECRUITABLE;
        }else if(!scout.narrator.getBool(SetupModifier.MASON_NON_CIT_RECRUIT)){
            feedback = NOT_RECRUITABLE;
        }else if(target.isPowerRole()){
            feedback = NOT_RECRUITABLE;
        }else{
            feedback = RECRUITABLE;
        }

        Feedback f = new Feedback(scout);
        f.add(feedback);
        f.setPicture("lookout");

        return f;
    }

    public static final String COMMAND = "Peek";
    public static final String NOT_RECRUITABLE = "Your target isn't recruitable.";
    public static final String RECRUITABLE = "Your target can be recruited.";
}
