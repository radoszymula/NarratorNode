package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.DeathAnnouncement;
import game.event.Feedback;
import game.event.Happening;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.attacks.DirectAttack;
import game.logic.support.attacks.IndirectAttack;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Burn extends Ability {

    // TODO add can't burn right now if burned previous day/night phase

    public static final int MAIN_ABILITY = Ability.BURN;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    private boolean burnedDuringDay;
    private boolean burnedLastNight = false;
    private int dayIgnites;

    @Override
    public Ability initialize(Player p) {
        Narrator n = p.narrator;
        dayIgnites = n.getInt(SetupModifier.ARSON_DAY_IGNITES);
        return super.initialize(p);
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.ARSON_DAY_IGNITES);
        rules.add(SetupModifier.ARCH_BURN);
        return rules;
    }

    public static final String BURN_LOWERCASE = "burn";
    public static final String COMMAND = "Burn";

    public static final String NIGHT_ACTION_DESCRIPTION = "Burn everyone that is doused in gasoline.";
    public static final String DEATH_FEEDBACK = "You were burned to death by an Arsonist";

    // if this changes again, remember to change the 'super', so the roles are
    // parsed in the right order
    public static final int BURN_ = MAIN_ABILITY; // has to be main ability because, i will only limit roles from doing
                                                  // their main

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        list.add("ignite those doused");

        return list;
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(!a.getTargets().isEmpty())
            Exception("You can't target anyone with that ability");
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);

        if(burnedDuringDay())
            Exception("You can't burn again if you burned during the day");
    }

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        if(burnedDuringDay())
            return new PlayerList();
        if(p.isPuppeted())
            return new PlayerList();
        return null;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        int ability = a.ability;

        if(ability == BURN_){
            burn(owner);
            if(target != null)
                owner.visit(target);
            setBurnedLastNight();
        }
        a.markCompleted();
    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        if(burnedDuringDay())
            burnedDuringDay = false; // set burned during day false
    }

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        if(getBurnedLastNight())
            this.burnedLastNight = false;
    }

    private boolean getBurnedLastNight() {
        return burnedLastNight;
    }

    private void setBurnedLastNight() {
        burnedLastNight = true;
    }

    private void setBurnedDuringDay() {
        burnedDuringDay = true;
    }

    boolean burnedDuringDay() {
        return burnedDuringDay;
    }

    private boolean hasDayIgnite() {
        return this.dayIgnites != 0;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return hasDayIgnite() && !getBurnedLastNight();
    }

    public static final String NO_DAY_DEATHS = "There was an minor explosion but no one appeared hurt.";
    public static final String DOUSED_FEEDBACK = "You were doused with gasoline last night!";

    public static DeathAnnouncement ExplosionAnnouncement(Narrator n, PlayerList burned, String type) {
        DeathAnnouncement e = new DeathAnnouncement(burned, n, DeathAnnouncement.DAY_DEATH);
        e.setPicture(type);

        if(burned.isEmpty())
            e.add(NO_DAY_DEATHS);
        else if(burned.size() == 1)
            e.add("There was an explosion and ", burned.get(0), " was found twitching on the floor. Dead.");
        else if(burned.size() == 2)
            e.add("There was a huge explosion, and ", burned.get(0), " and ", burned.get(1), " were found dead.");
        else
            e.add("There was a huge explosion, and ", burned, " were found dead.");

        return e;
    }

    @Override
    public void doDayAction(Player owner, PlayerList unused) {
        Narrator n = owner.narrator;

        PlayerList burned = new PlayerList();
        for(Player p: n.getLivePlayers()){
            if(p.isDoused()){
                if(!new IndirectAttack(Constants.ARSON_KILL_FLAG, p, owner).isImmune())
                    ;
                burned.add(p);
            }

        }
        DeathAnnouncement e = ExplosionAnnouncement(n, burned, "survivor");

        p.narrator.getEventManager().addCommand(owner, COMMAND);
        // it might be possible that arsons wont be able to douse the next night because
        // they just burned
        dayIgnites--;
        setBurnedDuringDay();

        n.announcement(e);

        burned = burn(owner);
        for(Player p: burned){
            p.dayKill(new DirectAttack(owner, p, Constants.ARSON_KILL_FLAG), Narrator.DAY_NOT_ENDING);
        }

        if(n.isDay()){
            List<NarratorListener> listeners = owner.narrator.getListeners();
            for(int i = 0; i < listeners.size(); i++)
                listeners.get(i).onDayBurn(owner, burned, e);
            n.parityCheck();
        }
    }

    private PlayerList burn(Player owner) {
        Narrator n = owner.narrator;
        PlayerList dousedTargets = new PlayerList();
        for(Player p: n.getLivePlayers()){
            if(!p.isInTown())
                continue;
            if(p.isDoused() || Architect.RoomFire(p))
                dousedTargets.add(p);
        }
        for(Player p: dousedTargets){
            // if your target isn't an arson, or if they're you, or if they weren't about to
            // burn
            if(!p.is(Burn.class) || p == owner || !p.getActions().isTargeting(new PlayerList(), BURN_))
                p.setDoused(false);
            Kill(owner, p, Constants.ARSON_KILL_FLAG);
        }
        Happening e = new Happening(n);
        e.add(owner, " ignited, attacking ");
        if(dousedTargets.isEmpty())
            e.add("no one.");
        else
            e.add(dousedTargets, ".");

        return dousedTargets;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<Object>();

        list.add("burning everyone");

        return list;
    }

    @Override
    public String getDayCommand() {
        if(!burnedDuringDay())
            return COMMAND;
        return null;
    }

    @Override
    public ArrayList<String> getCommandParts(Action action) {
        ArrayList<String> commands = new ArrayList<>();
        commands.add(COMMAND);
        return commands;
    }

    public static final String ARCHITECT_ROOM_SPREAD_TEXT = "Your burned targets will also kill undoused people in architect rooms.";
    public static final String ARCHITECT_ROOM_NO_SPREAD_TEXT = "The fires in your burned target's architect rooms will not spread.";

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getInt(SetupModifier.ARSON_DAY_IGNITES) == 0){
            ret.add("You do not have any daytime ignites.");
        }else if(hasDayIgnite()){
            if(!getBurnedLastNight()){
                ret.add("If you choose, you may ignite during the day.");
            }else{
                ret.add("You burned last night, making you unable to burn today.");
            }
        }else{
            ret.add("You cannot cause anymore daytime ignitions.");
        }

        if(p.narrator.getPossibleMembers().hasRole(Architect.class)){
            if(p.narrator.getBool(SetupModifier.ARCH_BURN))
                ret.add(ARCHITECT_ROOM_SPREAD_TEXT);
            else
                ret.add(ARCHITECT_ROOM_NO_SPREAD_TEXT);
        }

        return ret;
    }

    public static void NightBurn(Player arson) {
        arson.setTarget(BURN_, null, (String) null);
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public boolean isDayAbility() {
        return p.narrator.getInt(SetupModifier.ARSON_DAY_IGNITES) != 0 || p.narrator.isNight();
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    public boolean stopsParity(Player p) {
        if(p.isPuppeted())
            return false;
        return hasDayIgnite() && !(p.narrator.isFirstDay() && p.narrator.isFirstPhase()) && !burnedDuringDay();
    }

    public static void FeedbackGenerator(Feedback feedback) {
        feedback.add(Douse.DOUSED_FEEDBACK).setPicture("arsonist")
                .addExtraInfo("You are now at great risk of being burned alive!");
    }

    @Override
    public Action getExampleAction(Player owner) {
        if(owner.narrator.isNight())
            return new Action(owner, MAIN_ABILITY);
        return null;
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    @Override
    public boolean canEditBackToBackModifier() {
        return false;
    }

    @Override
    public boolean canEditZeroWeightedModifier() {
        return false;
    }

    @Override
    public boolean canSelfTarget(Narrator narrator) {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Arsonist", Douse.class, Burn.class, Undouse.class,
                Bulletproof.class);
        return FactionService.createFactionRole(faction, role);
    }

}
