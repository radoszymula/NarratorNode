package game.roles;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.Random;
import game.logic.support.action.Action;

public class Passive extends Ability {

    public Passive() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return null;
    }

    @Override
    public String getCommand() {
        return null;
    }

    @Override
    public int getAbilityNumber() {
        return INVALID_ABILITY;
    }

    private static final String NIGHT_ACTION_DESCRIPTION = "A voice of reason during the day.";

    @Override
    public boolean hasCooldownAbility() {
        return false;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        noAcceptableTargets();
    }

    @Override
    public void doNightAction(Action a) {
        NoNightActionVisit(a);
    }

    @Override
    public boolean isPowerRole() {
        return false;
    }

    @Override
    public boolean chargeModifiable() {
        return false;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public String getUsage(Player p, Random r) {
        return null;
    }

    @Override
    public boolean canSubmitFree() {
        return false;
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    @Override
    public boolean canEditBackToBackModifier() {
        return false;
    }

    @Override
    public boolean canEditZeroWeightedModifier() {
        return false;
    }

    @Override
    public boolean hasChargeAbility() {
        return false;
    }
}
