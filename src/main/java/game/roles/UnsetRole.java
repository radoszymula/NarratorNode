package game.roles;

import java.util.ArrayList;

import game.logic.Faction;
import game.logic.support.action.Action;

public class UnsetRole extends Passive {

    public static final String ROLE_NAME = "UnsetRole";

    public String getNightText(ArrayList<Faction> t) {
        return "";
    }

    public String[] getRoleInfo() {
        return new String[] { ""
        };
    }

    @Override
    public void mainAbilityCheck(Action a) {
    }

    public ArrayList<String> getClasses() {
        return toStringList();
    }
}
