package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Announcement;
import game.event.Happening;
import game.event.SnitchAnnouncement;
import game.logic.Faction;
import game.logic.MemberList;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Snitch extends Ability {

    public static final String COMMAND = "Plan";

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public boolean canEditBackToBackModifier() {
        return false;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public Ability initialize(Player p) {
        toReveal = new PlayerList();
        return super.initialize(p);
    }

    public static final int MAIN_ABILITY = Ability.SNITCH;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Upon death, reveal your last target to the town!";

    public static final int SNITCH_ = MAIN_ABILITY;

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    PlayerList toReveal;

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        if(first){
            first = false;
            toReveal.clear();
        }
        toReveal.add(target);
        new Happening(owner.narrator).add(owner, " will reveal ", target, " upon death.");
        a.markCompleted();
        owner.visit(target);
        return;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<Object>();

        PlayerList targets = getActionTargets(actions);

        list.add("make contingency plans on ");
        list.addAll(StringChoice.YouYourself(actions.get(0).owner, targets));

        return list;
    }

    private boolean first = true;

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        first = true;
    }

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        Player last = toReveal.getLast();
        toReveal.clear();
        if(last != null)
            toReveal.add(last);
    }

    public static FactionRole getCitizenTeam(Narrator n) {
        MemberList mList = n.getPossibleMembers();
        ArrayList<FactionRole> teams = mList.getMemberWithAbilities(Citizen.class);
        if(teams.isEmpty())
            return null;
        return teams.get(0);
    }

    @Override
    public void onDeath(Player snitch) {
        if(toReveal.isEmpty())
            return;

        Narrator n = snitch.narrator;
        Announcement a;
        String r;
        Faction t;
        FactionRole m;
        for(Player revealed: toReveal){
            if(revealed.isDead() && !revealed.getDeathType().isCleaned())
                continue;
            if(revealed.hasSuits() && !snitch.narrator.getBool(SetupModifier.SNITCH_PIERCES_SUIT)){
                r = revealed.suits.get(0).roleName;
                t = n.getFaction(revealed.suits.get(0).color);
            }else if(revealed.isDetectable()){
                r = revealed.getRoleName();
                t = revealed.getFaction();
            }else{
                m = getCitizenTeam(n);
                if(m == null){
                    t = revealed.getFaction();
                    r = revealed.getName();
                }else{
                    r = m.role.getName();
                    t = n.getFaction(m.faction.getColor());
                }
            }
            a = new SnitchAnnouncement(revealed, t, r);
            n.announcement(a);
        }
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        if(!p.narrator.getPossibleMembers().hasRole(Tailor.class))
            return super.getSpecificRoleSpecs(p);
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.SNITCH_PIERCES_SUIT))
            ret.add("Players that you reveal aren't affected by suits.");
        else
            ret.add("Players that you reveal might be affected by a suit.");

        return ret;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.SNITCH_PIERCES_SUIT);
        return rules;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction,
                RoleService.createRole(faction.narrator, "Snitch", Snitch.class));
    }
}
