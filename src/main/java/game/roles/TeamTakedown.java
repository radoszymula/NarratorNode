package game.roles;

import java.util.HashMap;

import game.event.Happening;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Faction;
import game.logic.support.Constants;
import game.logic.support.attacks.Attack;
import game.logic.support.attacks.IndirectAttack;

public class TeamTakedown extends Passive {

    // the reason why this is done on night phase, is so people won't know who not
    // to target, because they might be dead or not.
    // it makes sense, not to do it immediately post lynch
    public TeamTakedown() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Upon death, half of your remaining team members, rounding down, will suicide.";

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
    }

    private static void massSuicide(HashMap<Player, Faction> teams, PlayerList willDie) {
        double teamSize;
        PlayerList pl;
        Player sPlayer;
        Faction t;
        for(Player q: teams.keySet()){
            t = teams.get(q);
            teamSize = 0.0;
            pl = t.getMembers().getLivePlayers().shuffle(q.narrator.getRandom(), "mass suicide");
            pl.remove(willDie);
            for(Player p: pl){
                if(p.getFaction() == t)
                    teamSize += 0.5;
            }
            for(int i = 0; i < (int) teamSize; i++){
                sPlayer = pl.get(i);
                sPlayer.kill(new IndirectAttack(Constants.JESTER_KILL_FLAG, sPlayer, null));
                new Happening(q.narrator).add(sPlayer, " suicided over the death of ", q, ".");
            }
        }
    }

    public static void handleNightSuicides(Narrator n) {
        int day = n.getDayNumber();

        PlayerList willDie = new PlayerList();
        for(Player player: n._players){
            if(!player.isAlive()){
                if(player.getDeathDay() == day) // day kill
                    willDie.add(player);
                continue;
            }
            for(Attack attack: player.getInjuries()){
                if(attack.isCountered() || attack.isImmune())
                    continue;
                if(player.getRealAutoVestCount() > 0 && attack.getType() != Constants.JAIL_KILL_FLAG)
                    continue;
                if(attack.getType() == Constants.POISON_KILL_FLAG)
                    continue;
                willDie.add(player);
                break;
            }
        }

        HashMap<Player, Faction> suicideCausers = new HashMap<>();
        for(Player q: willDie){
            if(!suicideCausers.containsValue(q.getFaction()) && q.getAbilities().contains(TeamTakedown.class))
                suicideCausers.put(q, q.getFaction());
        }

        massSuicide(suicideCausers, willDie);
    }

    @Override
    public boolean isDatabaseAbility() {
        return false;
    }

    @Override
    public void checkIsNightless(Narrator n) {
    }

    @Override
    public boolean hiddenPossible() {
        return true;
    }
}
