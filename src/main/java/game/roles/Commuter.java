package game.roles;

import java.util.ArrayList;

import game.event.Happening;
import game.event.Message;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifiers;

public class Commuter extends Ability {

    public static final int MAIN_ABILITY = Ability.COMMUTE;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Leave town at night, making you untargetable.";

    public static final String COMMAND = "Commute";
    public static final String COMMAND_LOWERCASE = "commute";

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        if(getPerceivedCharges() != 0)
            return null;
        return new PlayerList();
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();
        list.add(" leave the town");
        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("leaving the town");
        return list;
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public void mainAbilityCheck(Action a) {

    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() != 0)
            Exception("You can't target players with this ability");
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner;
        owner.setAddress(null);

        // completedNightAction = true;

        Message e = new Happening(a.owner.narrator);
        e.add(owner, " commuted away from the game.");
        a.markCompleted();
    }

    @Override
    protected String getChargeText() {
        int charges = getPerceivedCharges();
        if(charges == SetupModifiers.UNLIMITED){
            return ("You can commute as many times as you want.");
        }else if(charges == 0){
            return ("You are unable to commute anymore.");
        }else if(charges == 1){
            return ("You can commute away from the game 1 more time.");
        }else{
            return ("You can commute away from the game " + charges + " more times.");
        }
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    public static void commute(Player commuter) {
        commuter.setTarget(MAIN_ABILITY, null, (String) null);
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public Action getExampleAction(Player owner) {
        return new Action(owner, MAIN_ABILITY);
    }

    public static boolean CommutingTargets(Action a) {
        Player commuter = a.getActualTargets().getFirst();
        if(commuter == null)
            return false;
        if(!commuter.is(Commuter.class))
            return false;
        return !commuter.getActions().getActions(Commuter.MAIN_ABILITY).isEmpty();
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    @Override
    public boolean canEditBackToBackModifier() {
        return false;
    }
}
