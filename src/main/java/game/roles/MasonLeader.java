package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionRoleService;
import services.RoleService;

public class MasonLeader extends Ability {

    public static final String ROLE_NAME = "Mason Leader";
    public static final String COMMAND = "Recruit";
    public static final String NIGHT_ACTION_DESCRIPTION = "Recruit citizens into your masons and talk to them at night.";

    public static final String[] BLUDGEON = Constants.MASONLEADER_KILL_FLAG;
    public static final String DEAD_FEEDBACK = "You were bludgeoned to death for your heretical practices.";

    public static final int MAIN_ABILITY = Ability.RECRUIT;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.MASON_PROMOTION);
        rules.add(SetupModifier.MASON_NON_CIT_RECRUIT);
        return rules;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        MasonryCheck(a);
    }

    public static void MasonryCheck(Action a) {
        deadCheck(a);
        Player target = a.getTarget();
        if(target.getFactions().contains(a.owner.getFaction()))
            if(Mason.IsMasonType(target))
                Exception("Can't target masons");
    }

    private static void UnsuccessfulFeedback(Action a) {
        Feedback f = new Feedback(a.owner, "Your recruitment was unsuccessful");
        f.setPicture("cultist");
        f.hideableFeedback = false;
        NoNightActionVisit(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        Player ml = a.owner;
        if(target == null)
            return;

        if(target.isCulted()){
            a.markCompleted();
            Kill(ml, target, BLUDGEON);
            return;
        }
        if(!target.is(Infiltrator.class)){
            if(target.isPowerRole() || target.isJailed() || target.isEnforced() || Mason.IsMasonType(target)
                    || !target.getColor().equalsIgnoreCase(ml.getColor())){
                UnsuccessfulFeedback(a);
                return;
            }

            if(!target.isPowerRole() && !target.is(Citizen.class)
                    && !ml.narrator.getBool(SetupModifier.MASON_NON_CIT_RECRUIT)){
                UnsuccessfulFeedback(a);
                return;
            }

            Role role = RoleService.getOrCreateDeprecated(narrator, new Mason());
            FactionRole factionRole = FactionRoleService.getOrCreateDeprecated(role, ml.getColor());
            target.changeRole(factionRole);
        }else{
            target.addAbility(new Mason());
            ml.getFaction().addMember(target);
        }

        Feedback recruitResult = new Feedback(ml);
        recruitResult.hideableFeedback = false;
        recruitResult.add("Your recruitment was succesful.");
        recruitResult.setPicture("cultleader");

        PlayerList masons = new PlayerList();
        for(Player p: ml.narrator.getLivePlayers()){
            if(!p.getFactions().contains(ml.getFaction()))
                continue;

            if(Mason.IsMasonType(p)){
                masons.add(p);
            }else if(p.is(Infiltrator.class)){
                masons.add(p);
            }

        }

        Feedback e = new Feedback(target);
        e.hideableFeedback = false;
        e.add(ml, " converted you to the masonry. Your other masons are : ", masons, ".");
        e.setPicture("cultist");

        happening(ml, " recruited ", target);

        a.markCompleted();
        ml.visit(target);
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();
        ret.add(SetupModifier.getDescription(p.narrator, SetupModifier.MASON_PROMOTION, p.narrator.getBool(SetupModifier.MASON_PROMOTION)));
        Mason.addMasonText(p.narrator, this, ret);
        return ret;
    }

    @Override
    public ArrayList<String> getPublicDescription(Narrator narrator, String actionOriginatorName, String color, AbilityList abilities) {
        ArrayList<String> ruleTextList = super.getPublicDescription(narrator, actionOriginatorName, color, abilities);
        Mason.addMasonText(narrator, this, ruleTextList);
        return ruleTextList;
    }
}
