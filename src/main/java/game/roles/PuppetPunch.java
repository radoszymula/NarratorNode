package game.roles;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Faction;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnsupportedMethodException;
import game.logic.support.action.Action;

public class PuppetPunch extends Ability {

    public static final String COMMAND = Ventriloquist.VENT_PUNCH;
    public static final int MAIN_ABILITY = Ability.PUPPET_PUNCH;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public void doNightAction(Action a) {
        throw new UnsupportedMethodException();
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return null;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        if(!a.owner.hasPuppets())
            Ventriloquist.VentException();
        deadCheck(a);
        Player puncher = a.getTargets().getFirst();
        if(puncher.isPuppeted() && puncher.getPuppeteer() != a.owner)
            throw new PlayerTargetingException(
                    "You can't punch people because you're not in control of your own actions.");

    }

    @Override
    public String getAbilityDescription(Narrator n) {
        for(Faction t: n.getFactions())
            if(t.has(CultLeader.class))
                return "accept recruitment invitation to the " + t.getName();
        return null;
    }

    @Override
    public PlayerList getAcceptableTargets(Player puncher) {
        return puncher.narrator.getLivePlayers().remove(puncher);
    }

    @Override
    public void doDayAction(Player ventController, PlayerList targets) {
        Punch.punch(targets.getFirst(), targets.getLast(), ventController);
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return true;
    }

    @Override
    public int getDefaultTargetSize() {
        return 2;
    }
}
