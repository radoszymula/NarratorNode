package game.roles;

import java.util.ArrayList;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.roles.support.Bread;
import game.roles.support.Gun;

public class BreadAbility extends ItemAbility<Bread> {

    public static final String COMMAND = Baker.COMMAND;
    public static final int MAIN_ABILITY = Ability.BREAD_ABILITY;
    public static final String ALIAS = "bread_alias";

    public BreadAbility() {
    }

    public BreadAbility(Player p) {
        super(p);
    }

    public BreadAbility(Player p, Bread b) {
        super(p, b);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return "use a " + n.getAlias(ALIAS) + " to survive the night";
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public String getCommand() {
        if(p == null)
            return COMMAND;
        if(p.narrator.getBool(SetupModifier.BREAD_PASSING))
            return COMMAND;
        if(p.is(Baker.class))
            return COMMAND;
        return null;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public void doNightAction(Action a) {
        Bread.PassBread(a);
    }

    @Override
    public void useFakeCharge() {
        for(Bread b: this){
            if(b.isFake()){
                b.markUsed();
                return;
            }
        }
    }

    @Override
    public void mainAbilityCheck(Action a) {
        Player owner = a.owner;
        if(a.getTargets().size() != 1)
            Exception("You must select one and only one target");
        if(!hasItems())
            Exception("You don't have any bread to pass!");
        deadCheck(a);
        if(!a.owner.narrator.getBool(SetupModifier.BREAD_PASSING) && !a.owner.is(Baker.class))
            Exception("You can't pass bread!");
        if(!a.isTeamTargeting() || !a.owner.is(Baker.class))
            return;
        ArrayList<Bread> breadCopy = new ArrayList<>();
        for(Bread b: this)
            breadCopy.add(b);
        for(Action curAction: owner.getActions()){
            if(curAction.ability != BREAD_ABILITY)
                continue;
            if(!curAction.isTeamTargeting())
                return;

            // is team targeting, so remove this bread from the option of being passed
            for(int i = 0; i < breadCopy.size(); i++){
                if(breadCopy.get(i).isInitialized()){
                    breadCopy.remove(i);
                    break;
                }
            }
        }
        for(Bread b: breadCopy)
            if(b.isInitialized())
                return;

        Exception("Cannot target known teammates with this");
    }

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        if(!hasItems() && !pi.is(Baker.class))
            return new PlayerList();
        return pi.narrator.getLivePlayers().remove(pi);
    }

    @Override
    public Action getExampleAction(Player owner) {
        return new Action(owner, MAIN_ABILITY);
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        if(!hasItems())
            return super.getSpecificRoleSpecs(p);

        ArrayList<String> specs = new ArrayList<>();
        int breadCount = size();
        if(breadCount == 1)
            specs.add("You have a piece of " + p.narrator.getAlias(Bread.ALIAS)
                    + ", allowing you to submit an extra night action");
        else if(breadCount > 0)
            specs.add("You have " + breadCount + " pieces of " + p.narrator.getAlias(Gun.ALIAS)
                    + ", allowing you to submit multiple night actions.");

        return specs;
    }

    @Override
    public boolean canAddAnotherAction(Action a) {
        int breadToGive = a.owner.getActions().getActions(Ability.BREAD_ABILITY).size();
        int availableBread = size();
        if(breadToGive >= availableBread && availableBread >= 0)
            return false;
        return super.canAddAnotherAction(a);
    }

    public static int getPassableBread(Player breadUser) {
        if(!breadUser.hasAbility(BreadAbility.class))
            return 0;

        if(!breadUser.is(Baker.class) && !breadUser.narrator.getBool(SetupModifier.BREAD_PASSING))
            return 0;

        int realBreads = 0;
        BreadAbility ba = breadUser.getAbility(BreadAbility.class);
        for(int i = 0; i < ba.size(); i++){
            if(ba.get(i).isReal())
                realBreads++;
        }
        return realBreads;
    }

    public static int getUseableBread(Player breadUser) {
        return getUseableBread(breadUser, null);
    }

    public static int getUseableBread(Player breadUser, Boolean real) {
        if(!breadUser.hasAbility(BreadAbility.class))
            return 0;

        BreadAbility ba = breadUser.getAbility(BreadAbility.class);
        if(breadUser.narrator.getBool(SetupModifier.SELF_BREAD_USAGE))
            return ba.size();
        int bCount = 0;
        for(Bread b: ba){
            if(real != null && real && b.isFake())
                continue;
            if(b.isInitialized())
                bCount++;
        }
        return bCount;
    }

    @Override
    protected Bread getFakeConsumable() {
        return Bread.FakeBread();
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        Player owner = actions.get(0).owner;
        PlayerList targets = getActionTargets(actions);

        ArrayList<Object> list = new ArrayList<>();
        list.add("pass " + owner.narrator.getAlias(ALIAS) + " to ");
        list.addAll(StringChoice.YouYourself(owner, targets));

        return list;
    }

    public void setMarkedBreadToFake() {
        for(Bread b: this){
            if(b.isUsed()){
                b.markUnused();
                b.setReal(false);
            }
        }
    }

    @Override
    public boolean isPositiveAbility() {
        return false;
    }

    @Override
    public String getAliasKey() {
        return ALIAS;
    }

    @Override
    public int maxNumberOfSubmissions() {
        return size();
    }
}
