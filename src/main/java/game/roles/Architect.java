package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.ArchitectChat;
import game.event.Happening;
import game.event.NightChat;
import game.event.RoleCreatedChat;
import game.event.SelectionMessage;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.listeners.NarratorListener;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Architect extends Ability implements DayResolveRole {

    public Architect() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final int MAIN_ABILITY = Ability.BUILD;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.ARCH_QUARANTINE);
        rules.add(SetupModifier.ARCH_BURN);
        return rules;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Construct rooms at night so others can converse securely.";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    public static final String COMMAND = "Build";

    @Override
    public void doNightAction(Action a) {
        NoNightActionVisit(a);
    }

    @Override
    public boolean isChatRole() {
        return true;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return true;
    }

    @Override
    public void doDayAction(Player arch, PlayerList targets) {
        if(targets.hasDead())
            Exception("Cannot target dead people");
        if(targets.getFirst() == targets.getLast())
            Exception("Cannot target the same people with this ability");
        if(arch.getActions().isTargeting(targets, MAIN_ABILITY))
            return;
        SelectionMessage e = new SelectionMessage(arch, false);
        Action[] newOld = Action.pushCommand(e, new Action(arch, targets, MAIN_ABILITY, (String) null, null),
                arch.getActions());
        e.recordCommand(arch, COMMAND, targets.get(0).getName(), targets.get(1).getName());
        e.dontShowPrivate();

        arch.narrator.addActionStack(newOld[0]);// new is 0
        arch.narrator.removeActionStack(newOld[1]);// old is 1

        List<NarratorListener> listeners = arch.narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onDayActionSubmit(arch, newOld[0]);
    }

    @Override
    public int getDefaultTargetSize() {
        return 2;
    }

    @Override
    public void resolveDayAction(Action a) {
        Happening e = new Happening(a.owner.narrator);
        e.add(a.owner, " set up a secret meeting for ", a.getTargets(), ".");
        a.owner.narrator.getEventManager().createArchitectChat(a);

        useCharge();
        triggerCooldown();
    }

    @Override
    public void dayHappening(Action a) {
        happening(a.owner, " attempted to create a room for ", a.getTargets().toArray());
    }

    @Override
    public void onNightStart(Player architect) {
        super.onNightStart(architect);
        int architectChats = getArchitectChatsMadeCount(architect);
        int breadToRemove = architectChats - 1;
        for(int i = 0; i < breadToRemove; i++)
            architect.getAbility(BreadAbility.class).useCharge();
    }

    private int getArchitectChatsMadeCount(Player p) {
        int count = 0;
        // i might need to filter on day number here
        ArchitectChat ac;
        for(NightChat nc: p.narrator.getEventManager().tempChats){
            if(nc instanceof ArchitectChat){
                ac = (ArchitectChat) nc;
                if(ac.getCreator().contains(p) && ac.getDay() == p.narrator.getDayNumber()){
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public String getDayCommand() {
        return COMMAND;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        if(actions.isEmpty())
            return list;
        Narrator n = actions.get(0).owner.narrator;
        if(n.isNight())
            return super.getActionDescription(actions);

        PlayerList targets = getActionTargets(actions);

        list.add(COMMAND.toLowerCase() + " a room for ");
        list.addAll(StringChoice.YouYourself(actions.get(0).owner, targets));

        return list;
    }

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        PlayerList pl = pi.narrator.getLivePlayers();
        if(!super.canSelfTarget(pi.narrator))
            pl.remove(pi);
        return pl;
    }

    @Override
    public void checkPerformDuringDay(Player owner) {
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        if(a.ability != MAIN_ABILITY)
            return super.getInsteadPhrase(a);
        ArrayList<Object> list = new ArrayList<>();

        list.add("building a room for ");
        list.addAll(StringChoice.YouYourself(a.owner, a.getTargets()));

        return list;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public String getUsage(Player p, Random r) {
        PlayerList live = p.narrator.getLivePlayers();
        live.shuffle(r, null);
        return getCommand() + " *" + live.getFirst().getName() + " " + live.getLast().getName() + "*";
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.ARCH_QUARANTINE)){
            ret.add("Participants in the rooms you build will be in no other temporary chats.");
        }else{
            ret.add("Participants in the rooms you build can also be in other temporary chats.");
        }

        if(p.narrator.getPossibleMembers().hasRole(Burn.class)){
            if(p.narrator.getBool(SetupModifier.ARCH_BURN)){
                ret.add("Participants that get burned in chats will burn other chat participants.");
            }else{
                ret.add("Participants that get burned in chats won't affect other chat participants.");
            }
        }

        return ret;
    }

    public static boolean RoomFire(Player p) {
        if(!p.narrator.getBool(SetupModifier.ARCH_BURN))
            return false;
        for(RoleCreatedChat rc: p.getRoleCreatedChats(p.narrator.getDayNumber())){
            if(!(rc instanceof ArchitectChat))
                continue;
            for(Player q: rc.getMembers()){
                if(q.isDoused())
                    return true;
            }
        }
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Architect", Architect.class);
    }
}
