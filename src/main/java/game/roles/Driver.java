package game.roles;

import java.util.ArrayList;

import game.event.Happening;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Driver extends Ability {

    public static final String NIGHT_ACTION_DESCRIPTION = "Pick up any two people to drive around.  Any action that affects one will instead affect the other.";

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String COMMAND = "Swap";
    public static final int MAIN_ABILITY = Ability.SWAP;

    @Override
    public String getNightText(Player p) {
        return "Type " + SQuote(COMMAND + " name1 name2") + " to swap your targets";
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);

        if(a.getTargets().getFirst() == a.getTargets().getLast())
            Exception("This ability can't be used on the same person");
    }

    @Override
    public int getDefaultTargetSize() {
        return 2;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        Action a = actionList.get(0);
        Player owner = a.owner;
        ArrayList<Object> list = new ArrayList<>();

        for(Action da: actionList){
            Player t1 = da._targets.getFirst();
            Player t2 = da._targets.getLast();
            // swaps
            if(t1 == null){
                Player temp;
                temp = t1;
                t1 = t2;
                t2 = temp;
            }

            if(t1 == null){
                list.add("pick up anyone");
            }else if(t2 == null){
                list.add("pick up ");
                list.add(StringChoice.YouYourselfSingle(owner, t1));
            }else{
                list.add("switch ");
                list.add(StringChoice.YouYourselfSingle(owner, t1));
                list.add(" and ");
                list.add(StringChoice.YouYourselfSingle(owner, t2));
            }
            list.add(", ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    public static final String FEEDBACK = "You were bus driven.";

    @Override
    public void doNightAction(Action da) {
        Player bd = da.owner, t1 = da._targets.getFirst(), t2 = da._targets.getLast();
        // these are the intended targets. making sure they aren't null
        if(t1 == null)
            return;
        if(!t1.isInTown() && !t2.isInTown())
            return;
        if(!t1.isInTown()){
            NoNightActionVisit(bd, t2);
            return;
        }
        if(!t2.isInTown()){
            NoNightActionVisit(bd, t1);
            return;
        }

        if(t1 == t2){
            NoNightActionVisit(da);
            return;
        }

        Player address1 = t1.getAddress();
        Player address2 = t2.getAddress();

        t1.setHouse(address2);
        t2.setHouse(address1);// feedback here

        t1.secondaryCause(bd);
        t2.secondaryCause(bd);

        new Happening(bd.narrator).add(bd, " switched ", t1, " and ", t2, ".");

        da.markCompleted();
        bd.visit(t1, t2);
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();

        list.add("swaping ");
        list.add(StringChoice.YouYourself(a.owner, a.getTargets()));

        return list;
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        return p.narrator.getLivePlayers().sortByName();
    }

    @Override
    public boolean allowsForFriendlyFire() {
        return true;
    }

    @Override
    public String getUsage(Player p, Random r) {
        PlayerList live = p.narrator.getLivePlayers();
        live.shuffle(r, null);
        return getCommand() + " *" + live.getFirst().getName() + " " + live.getLast().getName() + "*";
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Bus Driver", Driver.class);
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }
}
