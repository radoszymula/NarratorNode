package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Bomb extends Passive {

    public Bomb() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "You will kill anyone who attacks you.";
    public static final String DEATH_FEEDBACK = "You attacked a bomb, resulting in your untimely death!";

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.BOMB_PIERCE);
        return rules;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.BOMB_PIERCE)){
            ret.add("You will kill death immune targets.");
        }else{
            ret.add("You won't kill death immune targets.");
        }

        return ret;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean isPowerRole() {
        return true;
    }

    @Override
    public void checkIsNightless(Narrator n) {
    }

    @Override
    public boolean hiddenPossible() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Bomb", Bomb.class);
        return FactionService.createFactionRole(faction, role);
    }
}
