package game.roles;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Agent extends Ability {

    public static final String NIGHT_ACTION_DESCRIPTION = "Stalk someone to find out who their target visited and who visited them.";
    public static final String COMMAND = "Stalk";
    public static final int MAIN_ABILITY = Ability.STALK;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String FEEDBACK = "These are the people who visited your target: ";

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        if(target == null)
            return;
        Player owner = a.owner;
        if(owner.narrator.pre_visiting_phase){
            owner.visit(target);
            return;
        }
        Detective.follow(owner, target, a.getIntendedTarget(), null);
        Lookout.watch(owner, target, a.getIntendedTarget());
        Ability.happening(owner, " stalked ", target);
        a.markCompleted();

    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Agent", Agent.class);
    }
}
