package game.roles;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.roles.support.Vest;
import models.FactionRole;
import services.FactionRoleService;
import services.FactionService;
import services.RoleService;

public class Sleepwalker extends Passive {

    public Sleepwalker() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public String getNightText(Narrator n) {
        return Constants.NO_NIGHT_ACTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Wander around at night visiting people.";

    @Override
    public void mainAbilityCheck(Action a) {
        noAcceptableTargets();
    }

    private static Player getRandomPlayer(Player walker) {
        Random r = walker.narrator.getRandom();
        PlayerList living = walker.narrator.getLivePlayers();
        return living.getRandom(r);
    }

    @Override
    public void onPlayerNightEnd(Player walker) {
        super.onPlayerNightEnd(walker);
        if(walker.isJailed())
            return;

        ActionList actions = walker.getActions();
        Action vestAction = walker.getAction(Vest.MAIN_ABILITY);
        if(vestAction != null){
            vestAction.setTarget(getRandomPlayer(walker));
            return;
        }

        if(actions.mainAction == null)
            actions.mainAction = new Action(walker, Ability.ParseAbility(Ability.VISIT), getRandomPlayer(walker));

        // possibility of making mainAction an array and adding it here. needs option
        // for visits someone if they have other abilities.
    }

    @Override
    public boolean chargeModifiable() {
        return false;
    }

    @Override
    public boolean hiddenPossible() {
        return true;
    }

    public static FactionRole template(Faction faction, FactionRole citizen) {
        Role sleepwalker = RoleService.createRole(faction.narrator, "Sleepwalker", Citizen.class, Sleepwalker.class);
        FactionRole factionRole = FactionService.createFactionRole(faction, sleepwalker);
        FactionRoleService.setRoleReceive(factionRole, citizen);
        return factionRole;
    }
}
