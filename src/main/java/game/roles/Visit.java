package game.roles;

import java.util.ArrayList;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.action.Action;

public class Visit extends Ability {

    public static final String COMMAND = Ability.VISIT;
    public static final int MAIN_ABILITY = VISIT_ABILITY;

    public Visit(Player p) {
        this.p = p;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return new String[] {};
    }

    public String getAbilityDescription(Narrator n) {
        return null;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        noAcceptableTargets();
    }

    @Override
    public void doNightAction(Action a) {
        NoNightActionVisit(a);
    }

    public ArrayList<String> getClasses() {
        return toStringList(new String[] {});
    }

    @Override
    public boolean isPowerRole() {
        return false;
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return VISIT_ABILITY;
    }
}
