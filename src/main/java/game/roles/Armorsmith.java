package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.IllegalActionException;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.Modifier;
import game.roles.support.Vest;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Armorsmith extends Ability {

    public static final String COMMAND = "Armor";
    public static final int MAIN_ABILITY = Ability.ARMOR;
    public static final String[] AS_FAKE_VEST = new String[] {
            "vs_fake_vest", "#" + Vest.ALIAS + "#s can be optionally fake",
    };

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return "Give " + n.getAlias(Vest.ALIAS)
                + " during the night, protecting them from attack on subsequent nights.";
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray("Give " + n.getAlias(Vest.ALIAS)
                + " during the night, protecting them from attack on subsequent nights.");
    }

    public static final String FAKE = "fake";
    public static final String REAL = Gunsmith.REAL;

    public static String GetArmorReceiveMessage(Narrator n) {
        return "You received " + n.getAlias(Vest.ALIAS) + "!";
    }

    public static boolean FakeVests(Player p) {
        if(p.hasAbility(Armorsmith.class))
            return FakeVests(p.getAbility(Armorsmith.class));
        if(p.hasAbility(Blacksmith.class))
            return FakeVests(p.getAbility(Blacksmith.class));
        if(p.hasAbility(GraveDigger.class)){
            for(Player q: p.narrator._players){
                if(q.hasAbility(Armorsmith.class) || q.hasAbility(Blacksmith.class)){
                    if(FakeVests(p))
                        return true;
                }
            }
            return false;
        }

        throw new IllegalActionException("Cannot test for fake " + p.narrator.getAlias(Vest.ALIAS) + " on this player");
    }

    public static boolean FakeVests(Ability ability) {
        return ability.modifiers != null && ability.modifiers.getOrDefault(AbilityModifier.AS_FAKE_VESTS, false);
    }

    public boolean fakeVests() {
        return FakeVests(this);
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
        Narrator n = a.owner.narrator;
        if(!fakeVests() && a.getOption() != null && !a.getOption().equalsIgnoreCase(REAL))
            Exception(Util.TitleCase(n.getAlias(Vest.ALIAS)) + " can only be real. Please omit any action options.");
        if(fakeVests() && a.getOption() != null
                && (!a.getOption().equalsIgnoreCase(REAL) && !a.getOption().equalsIgnoreCase(FAKE)))
            Exception("Unable to tell whether " + n.getAlias(Vest.ALIAS) + " should real or faulty");

        if(fakeVests() && a.getOption() == null)
            a.setOption(REAL);
    }

    @Override
    public ArrayList<Option> getOptions(Player p) {
        if(!fakeVests())
            return super.getOptions(p);
        String v_opt = p.narrator.getAlias(Vest.ALIAS);
        ArrayList<Option> options = new ArrayList<>();
        options.add(new Option(REAL, REAL + " " + v_opt));
        options.add(new Option(FAKE, FAKE + " " + v_opt));
        return options;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target != null)
            a.markCompleted();
        ArmorsmithAction(owner, target, a.getOption());
        owner.visit(target);
    }

    // visits not handled in here
    public static void ArmorsmithAction(Player as, Player target, String vOption) {
        if(target == null)
            return;
        if(as.getFaction().knowsTeam() && as.getFaction() == target.getFaction()){
            NoNightActionVisit(as, target);
            return;
        }

        Narrator n = as.narrator;
        if(!target.isSquelched()){
            Vest v = new Vest();
            v.armorSmithVest = true;
            if(FakeVests(as) && FAKE.equals(vOption)){
                v.setReal(false);
                happening(as, " gave fake " + n.getAlias(Vest.ALIAS) + " to ", target);
            }else{
                happening(as, " gave " + n.getAlias(Vest.ALIAS) + " to ", target);
            }
            target.addVest(v);
            FeedbackGenerator(new Feedback(target));
        }else{
            happening(as, " tried to give " + n.getAlias(Vest.ALIAS) + " to ", target);
        }
    }

    public static void FeedbackGenerator(Feedback feedback) {
        Narrator narrator = feedback.n;
        feedback.add(GetArmorReceiveMessage(narrator));
        feedback.addExtraInfo("You may use this at night to protect yourself from most attacks!");
        feedback.setPicture("armorsmith");
        for(Role role: narrator.roles){
            for(Ability ability: role._abilities){
                if(ability.modifiers != null && ability.modifiers.getOrDefault(AbilityModifier.AS_FAKE_VESTS, false)){
                    feedback.addExtraInfo("There is a chance that your vest might not work.");
                    return;
                }
            }
        }
        for(Faction faction: narrator.getFactions()){
            AbilityList factionAbilities = faction.getAbilities();
            if(factionAbilities == null)
                continue;
            for(Ability ability: factionAbilities){
                if(ability.modifiers != null && ability.modifiers.getOrDefault(AbilityModifier.AS_FAKE_VESTS, false)){
                    feedback.addExtraInfo("There is a chance that your vest might not work.");
                }
            }
        }

    }

    @Override
    public Action parseCommand(Player p, ArrayList<String> commands) {
        if(!fakeVests()){
            return super.parseCommand(p, commands);
        }
        if(!commands.get(commands.size() - 1).equalsIgnoreCase(FAKE)){
            return super.parseCommand(p, commands);
        }
        commands.remove(commands.size() - 1);
        return super.parseCommand(p, commands).setOption(FAKE);
    }

    @Override
    public String getUsage(Player p, Random r) {
        if(fakeVests())
            return super.getUsage(p, r) + " " + REAL + "\n" + super.getUsage(p, r) + " " + FAKE;
        return super.getUsage(p, r);
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        ArrayList<String> parts = super.getCommandParts(a);
        if(fakeVests() && FAKE.equalsIgnoreCase(a.getOption()))
            parts.add(FAKE);

        return parts;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();
        if(actions.isEmpty())
            return list;
        Player owner = actions.get(0).owner;
        Narrator n = owner.narrator;
        PlayerList targets = getActionTargets(actions);

        if(actions.get(0) != null && FAKE.equalsIgnoreCase(actions.get(0).getOption())){
            list.add("give fake " + n.getAlias(Vest.ALIAS) + " to ");
        }else{
            list.add("give " + n.getAlias(Vest.ALIAS) + " to ");
            list.addAll(StringChoice.YouYourself(owner, targets));
        }
        return list;
    }

    @Override
    public List<AbilityModifier> getAbilityModifiers() {
        List<AbilityModifier> rules = new LinkedList<>();
        rules.add(AbilityModifier.AS_FAKE_VESTS);
        rules.addAll(super.getAbilityModifiers());
        return rules;
    }

    @Override
    public boolean isBool(Modifier modifier) {
        if(modifier == AbilityModifier.AS_FAKE_VESTS)
            return true;
        return super.isBool(modifier);
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.getFaction().knowsTeam() && p.getFaction().getMembers().size() > 1){
            ret.add("Your " + p.narrator.getAlias(Vest.ALIAS) + " cannot go to teammates.");
        }
        return ret;
    }

    @Override
    public String getNightText(Player p) {
        Narrator n = p.narrator;
        String ret = "Type " + NQuote(COMMAND) + " to give " + n.getAlias(Vest.ALIAS) + " to this person.";
        if(fakeVests())
            ret += " To give " + n.getAlias(Vest.ALIAS) + " that won't protect the user, type "
                    + SQuote(COMMAND + " playerName FAKE") + ".";
        return ret;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Armorsmith", Armorsmith.class);
        return FactionService.createFactionRole(faction, role);
    }
}
