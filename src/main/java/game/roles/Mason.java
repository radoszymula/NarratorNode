package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.RolePackage;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;
import util.FactionRoleUtil;

public class Mason extends Passive {

    public static final String NIGHT_ACTION_DESCRIPTION = "You are part of the masons and may communicate with others like you at night.";

    public Mason() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.MASON_PROMOTION);
        rules.add(SetupModifier.MASON_NON_CIT_RECRUIT);
        return rules;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        noAcceptableTargets();
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player player) {
        Narrator narrator = player.narrator;
        ArrayList<String> ret = new ArrayList<>();
        ret.add(SetupModifier.getDescription(narrator, SetupModifier.MASON_PROMOTION,
                narrator.getBool(SetupModifier.MASON_PROMOTION)));
        addMasonText(player.narrator, this, ret);
        return ret;
    }

    @Override
    public ArrayList<String> getPublicDescription(Narrator narrator, String actionOriginatorName, String color,
            AbilityList abilities) {
        ArrayList<String> ruleTextList = super.getPublicDescription(narrator, actionOriginatorName, color, abilities);
        addMasonText(narrator, this, ruleTextList);
        return ruleTextList;
    }

    @Override
    public void checkIsNightless(Narrator n) {
    }

    public static void addMasonText(Narrator n, Ability r, ArrayList<String> list) {
        if(!n.getBool(SetupModifier.MASON_PROMOTION))
            list.add("This will never spawn by themselves.");
        list.add(SetupModifier.getDescription(n, SetupModifier.MASON_NON_CIT_RECRUIT,
                n.getBool(SetupModifier.MASON_NON_CIT_RECRUIT)));
    }

    // lowercase
    public static void ForceRespawn(Narrator n, List<RolePackage> generatedRoles) {
        if(n.getBool(SetupModifier.MASON_PROMOTION))
            return;
        for(Faction t: n.getFactions())
            forceTeamRespawn(t, generatedRoles);
    }

    private static void forceTeamRespawn(Faction team, List<RolePackage> generatedRoles) {
        FactionRole factionRole;
        FactionRole mason = null;
        for(RolePackage rp: generatedRoles){
            factionRole = rp.assignedRole;
            if(!factionRole.getColor().equals(team.getColor())) // wrong color
                continue;
            if(factionRole.role.contains(MasonLeader.class)) // mason leader means no respawn
                return;
            if(!factionRole.role.contains(Mason.class)) // not a mason, dont care
                continue;
            if(mason != null) // mason was previously found, this'll be the second
                return;
            mason = factionRole;
        }

        ArrayList<RolePackage> respawns = new ArrayList<>();
        for(RolePackage rp: generatedRoles){
            if(rp.assignedRole != mason)
                continue;
            respawns.add(rp);
        }
        for(RolePackage rp: respawns){
            generatedRoles.add(rp.assignedHidden.getRole(team.narrator, FactionRoleUtil.getList(mason)));
            generatedRoles.remove(rp);
        }
    }

    public static boolean IsMasonType(Player p) {
        return p.is(Mason.class) || p.is(MasonLeader.class) || p.is(Enforcer.class) || p.is(Clubber.class);
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Mason", Mason.class);
        return FactionService.createFactionRole(faction, role);
    }
}
