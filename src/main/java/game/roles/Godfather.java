package game.roles;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.action.Action;

public class Godfather extends Passive {

    public static final String NIGHT_ACTION_DESCRIPTION = "The leader of the mafia!  Can override who is sent to kill";

    public Godfather() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public void mainAbilityCheck(Action a) {
        noAcceptableTargets();
    }

    public static final String DEATH_FEEDBACK = "You were killed by the mafia!";

    // members is copied, as it's only
    @Override
    public int nightVotePower(Player pi, PlayerList members) {
        return Math.max(1, members.size() - 1);
    }

    @Override
    public boolean isUnique() {
        return true;
    }
}
