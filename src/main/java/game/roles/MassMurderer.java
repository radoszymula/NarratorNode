package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Happening;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.attacks.MassAttack;
import game.logic.support.rules.SetupModifier;
import game.roles.support.ElectrocutionException;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class MassMurderer extends Ability {

    public static final int MAIN_ABILITY = Ability.SPREE;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.MM_SPREE_DELAY);
        return rules;
    }

    public static final String ROLE_NAME = "Mass Murderer";

    private static final String SELECTION_PROMPT = "murder all players at ";

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        PlayerList targets = getActionTargets(actions);

        Player owner = actions.get(0).owner;

        list.add(SELECTION_PROMPT);
        if(targets.size() == 1){
            Player target = targets.getFirst();

            StringChoice sc = new StringChoice(target);
            sc.add(target, "your");

            list.add(sc);
            sc = new StringChoice("'s");
            sc.add(target, "");

            list.add(sc);
            list.add(" house");
        }else{
            for(Player target: targets){
                StringChoice sc = new StringChoice(target);
                sc.add(owner, "your");

                list.add(sc);
                sc = new StringChoice("'s");
                sc.add(owner, "");

                list.add(sc);
                list.add(" house");
                list.add(" and ");
            }
            list.remove(list.size() - 1);
        }

        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        Player target = a.getTarget();
        ArrayList<Object> list = new ArrayList<Object>();
        list.add("Instead of murdering everyone at ");

        StringChoice sc = new StringChoice(target);
        sc.add(target, "your");
        list.add(sc);
        list.add(new StringChoice("'s ").add(target, ""));
        list.add(" house, ");
        return list;
    }

    @Override
    public String getNightText(Player p) {
        if(getCooldown() >= 1)
            return "You must wait a night to kill people again.";
        return super.getNightText(p);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Kill everyone who performs an action at a chosen person's house.";

    public static final String COMMAND = "Spree";
    public static final String DEATH_FEEDBACK = "You were killed by a Mass Murderer";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
        if(getCooldown() >= 1)
            Exception("You have to wait another night until you can murder people again.");
    }

    private int cooldown;

    private int getCooldown() {
        return cooldown;
    }

    private void setCoolDown(int day) {
        this.cooldown = day;
    }

    private void decrementCooldown() {
        setCoolDown(getCooldown() - 1);
    }
    // private int cooldown;

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        decrementCooldown();
    }

    @Override
    public void doNightAction(Action a) {
        if(getCooldown() > 0){
            NoNightActionVisit(a);
            return;
        }

        Player target = a.getTarget();
        if(target == null)
            return;

        Player mm = a.owner;

        ArrayList<Player[]> visitsToDo = new ArrayList<>();

        PlayerList killed = new PlayerList();
        Narrator narrator = mm.narrator;
        for(Player visitor: narrator.getLivePlayers()){
            if(visitor == mm)
                continue;
            if(visitor.getActions().visited(target) && !visitor.in(killed)){
                visitsToDo.add(new Player[] {
                        visitor, target
                });
                killed.add(visitor);
            }
        }

        if(target.isAtHome() && target != mm && !killed.contains(target))
            killed.add(target);

        if(killed.size() > 1)
            setCoolDown(narrator.getInt(SetupModifier.MM_SPREE_DELAY) + 1);

        for(Player visitor: killed){
            visitor.kill(new MassAttack(mm, killed, visitor));
        }

        Object add;
        if(killed.isEmpty())
            add = "no one";
        else
            add = killed;

        new Happening(narrator).add(mm, " camped out at ", target, "'s house killing ", add, ".");

        PlayerList electrocuted = new PlayerList();
        try{
            mm.visit(target);
        }catch(ElectrocutionException e){
            electrocuted.add(e.charged);
        }
        for(Player[] p: visitsToDo){
            try{
                p[0].visit(p[1]);
            }catch(ElectrocutionException e){
                electrocuted.add(e.charged);
            }
        }
        ElectrocutionException.throwException(mm, electrocuted);
        a.markCompleted();
        return;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        int cd = getCooldown();
        if(cd > 0)
            ret.add("You must wait " + cd + " more days before you can kill people again.");

        return ret;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Mass Murderer", MassMurderer.class, Bulletproof.class);
        return FactionService.createFactionRole(faction, role);
    }
}
