package game.roles;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Blackmailer extends Ability {

    public static final String COMMAND = "Blackmail";

    public static final String NIGHT_ACTION_DESCRIPTION = "Stop people from voting and talking.";

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final int MAIN_ABILITY = Ability.BLACKMAIL;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        Silence.setSilenced(owner, target);
        Disfranchise.setDisfranchised(owner, target);
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Blackmailer", Blackmailer.class);
    }

    public static FactionRole template(Faction faction) {
        Role role = template(faction.narrator);
        return FactionService.createFactionRole(faction, role);
    }
}
