package game.roles;

import java.util.ArrayList;
import java.util.List;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.Util;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class ParityCheck extends Ability {

    public static final String COMMAND = "Compare";
    public static final int MAIN_ABILITY = Ability.COMPARE;
    public static final String NIGHT_ACTION_DESCRIPTION = "Compare player allegiances each night for differences.";

    public static final String GOTTA_WAIT_FEEDBACK = "You don't have previous alignments to compare last night's target(s) to.";
    public static final String FEEDBACK_SAME_ALIGNMENT = "Your target's alignment matches up with all of last night's targets.";
    public static final String FEEDBACK_ALL_DIFFERENT_ALIGNMENT = "Your target's alignment doesn't match up with any of last night's targets";

    private PlayerList lastChecked;
    private PlayerList checkedTonight;
    private List<String> intendedNamesCheckedTonight;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public Ability initialize(Player player) {
        this.checkedTonight = new PlayerList();
        this.lastChecked = new PlayerList();
        return super.initialize(player);
    }

    @Override
    public void doNightAction(Action action) {
        Player owner = action.owner;
        Player target = action.getTarget();
        if(target == null)
            return;

        if(lastChecked.isEmpty())
            new Feedback(owner).add(GOTTA_WAIT_FEEDBACK);
        else{
            Feedback feedback;
            int differentAlignmentCount = getDifferentAlignmentCount(target, lastChecked);
            if(differentAlignmentCount == 0)
                feedback = (Feedback) new Feedback(owner).add(FEEDBACK_SAME_ALIGNMENT);
            else if(differentAlignmentCount == lastChecked.size())
                feedback = (Feedback) new Feedback(owner).add(FEEDBACK_ALL_DIFFERENT_ALIGNMENT);
            else
                feedback = (Feedback) new Feedback(owner)
                        .add(getFeedback(lastChecked.size() - differentAlignmentCount));
            if(action.isSubmittedByOwner){
                feedback.addExtraInfo(
                        "As a reminder, you attempted to check " + action.getIntendedTarget() + " last night.");
                feedback.addExtraInfo("You also attempted to check " + Util.SpacedString(intendedNamesCheckedTonight)
                        + " two nights ago.");
            }
        }
        checkedTonight.add(target);
        intendedNamesCheckedTonight.add(action.getIntendedTarget());

        happening(owner, " parity checked ", target);
        action.markCompleted();
        owner.visit(target);
    }

    private static int getDifferentAlignmentCount(Player target, PlayerList lastChecked) {
        int count = 0;
        Faction targetFaction = target.getFaction();
        for(Player player: lastChecked){
            if(player.getFaction() != targetFaction)
                count++;
        }
        return count;
    }

    public static String getFeedback(int sameAlignmentCount) {
        return "Your target's alignment matches that of " + sameAlignmentCount
                + " other(s) from the last time you checked.";
    }

    @Override
    public void onNightStart(Player player) {
        super.onNightStart(player);
        intendedNamesCheckedTonight = new ArrayList<>();
    }

    @Override
    public void onDayStart(Player player) {
        super.onDayStart(player);
        if(checkedTonight.isEmpty())
            return;
        if(player.isSquelched())
            lastChecked.clear();
        else{
            lastChecked.add(checkedTonight);
            checkedTonight.clear();
        }
    }

    @Override
    public String[] getRoleInfo(Narrator narrator) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public void mainAbilityCheck(Action action) {
        deadCheck(action);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Parity Cop", ParityCheck.class);
    }

    public static FactionRole template(Faction faction) {
        Role role = template(faction.narrator);
        return FactionService.createFactionRole(faction, role);
    }

}
