package game.roles;

import game.logic.support.action.Action;

public interface DayResolveRole {

    void dayHappening(Action action);

}
