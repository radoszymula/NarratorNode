package game.roles;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.UnsupportedMethodException;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;

public class PuppetVote extends Ability {

    public String getAbilityDescription(Narrator n) {
        return null;
    }

    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    public static final String COMMAND = Ventriloquist.VENT_VOTE;
    public static final int MAIN_ABILITY = Ability.DAY_POSSESS;

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return new String[0];
    }

    @Override
    public String getCommand() {
        return Ventriloquist.VENT_VOTE;
    }

    @Override
    public void doDayAction(Player owner, PlayerList targets) {
        Ventriloquist.ControlDayAction(owner, targets);
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return p.hasPuppets();
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public void doNightAction(Action a) {
        throw new UnsupportedMethodException();
    }

    public static void AddAbility(Player p) {
        p.addAbility(new PuppetVote().initialize(p));
        if(p.narrator.getBool(SetupModifier.PUNCH_ALLOWED))
            p.addAbility(new PuppetPunch().initialize(p));
    }

    public static void RemoveAbility(Player p) {

    }
}
