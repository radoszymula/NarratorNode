package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Announcement;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalActionException;
import game.logic.listeners.NarratorListener;
import game.logic.support.HTString;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;

public class Marshall extends Ability {

    private boolean revealedToday = false;

    public static final int MAIN_ABILITY = Ability.ORDER;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }
    // private int voteCount;

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        if(revealedToday)
            return new PlayerList();
        return null;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.MARSHALL_EXECUTIONS);
        rules.add(SetupModifier.MARSHALL_QUICK_REVEAL);
        return rules;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Call the town to order to enable multiple public executions.";

    public static final String COMMAND = "MarshallLaw";
    public static final String ORDER_LOWERCASE = "marshalllaw";

    @Override
    public void mainAbilityCheck(Action a) {
    }

    @Override
    public void doNightAction(Action a) {
        NoNightActionVisit(a);
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return !revealedToday && !p.isSilenced() && !p.isPuppeted() && this.getRealCharges() != 0;
    }

    @Override
    public void doDayAction(Player owner, PlayerList unused) {
        if(owner.isSilenced())
            throw new IllegalActionException("Can't reveal if blackmailed");
        Narrator narrator = owner.narrator;
        Announcement e = new Announcement(narrator);
        e.setPicture("mayor");
        narrator.getEventManager().addCommand(owner, COMMAND);

        StringChoice sc = new StringChoice(owner);
        sc.add(owner, "You");
        e.add(sc, " ");

        sc = new StringChoice("has");
        sc.add(owner, "have");

        e.add(sc, " revealed as the ", new HTString(owner.getRoleName(), owner.getColor()), "!");

        revealedToday = true;
        useCharge();

        e.finalize();

        narrator.addDayLynches(narrator.getInt(SetupModifier.MARSHALL_EXECUTIONS)); // how many you're adding

        List<NarratorListener> listeners = narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onRoleReveal(owner, e);
    }

    @Override
    public String getDayCommand() {
        return COMMAND;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        int executionCount = p.narrator.getInt(SetupModifier.MARSHALL_EXECUTIONS);
        int startingExecutions = modifiers.getOrDefault(AbilityModifier.CHARGES, 0);

        if(startingExecutions == getPerceivedCharges()){
            ret.add("You may reveal yourself during the day.  In addition you will enable a special " + executionCount
                    + " public execution day.");
        }else{
            ret.add("You've already revealed yourself, but you can still enable another " + executionCount
                    + " public execution day tomorrow.");
        }

        if(p.narrator.getBool(SetupModifier.MARSHALL_QUICK_REVEAL))
            ret.add("You will immediately know what the role flips are after day executions.");
        else
            ret.add("You will know role flips after day is over.");

        return ret;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        this.revealedToday = false;
    }

    public static boolean isCurrentMarshallLynch(Player p) {
        return !p.narrator.getBool(SetupModifier.MARSHALL_QUICK_REVEAL) && p.isDead() && p.getDeathType().isLynch()
                && p.getDeathDay() == p.narrator.getDayNumber() && p.narrator.isDay()
                && !p.narrator.isTransitioningToNight;
    }

    @Override
    public Action getExampleAction(Player owner) {
        return null;
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    @Override
    public boolean canEditBackToBackModifier() {
        return false;
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(!a.getTargets().isEmpty())
            Exception("You can't target anyone with that ability");
    }
}
