package game.roles;

import java.util.ArrayList;
import java.util.Arrays;

import game.event.Feedback;
import game.event.Happening;
import game.logic.Faction;
import game.logic.MemberList;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownPlayerException;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.roles.support.Bread;
import game.roles.support.Gun;
import game.roles.support.Vest;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class DrugDealer extends Ability {

    public static final String ROLE_NAME = "Drug Dealer";
    public static boolean SAFETY = true;

    public static final int MAIN_ABILITY = Ability.DRUG;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "At night, provide false feedback, giving people false clues.";

    public static final String COMMAND = "Drug";

    public static final String WIPE = "wiped";
    public static final String BLOCKED = "blocked";
    public static final String DOUSED = "doused";
    public static final String DOCTOR = "healed";
    public static final String DRIVEN = "swapped";
    public static final String GUARDED = "guarded";
    public static final String POISONED = "poisoned";
    public static final String WITCH = "witched";
    public static final String VEST = "vest";
    public static final String GUN_RECEIVED = "gun";
    public static final String BREAD = "bread";
    public static final String CHARGED = "charged";
    public static final String SUITTED = "suited";
    public static final String ANNOYED = "annoyed";

    public static final String[] DRUGS = {
            WIPE, BLOCKED, DOUSED, DOCTOR, DRIVEN, GUARDED, POISONED, WITCH, VEST, GUN_RECEIVED, BREAD, CHARGED, SUITTED, ANNOYED
    };

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);

        String drug = a.getOption();
        if(drug == null){
            Ability.Exception("Must select a drug");
        }
        drug = drug.toLowerCase();
        if(!Arrays.asList(DRUGS).contains(drug)){
            Ability.Exception("Invalid drug");
        }

        MemberList rl = a.owner.narrator.getPossibleMembers();
        if(SAFETY && !possibleDrug(drug, rl, a.owner.narrator))
            Ability.Exception("Drug not possible!");
    }

    public static ArrayList<String> getAvailableDrugs(Player owner) {
        ArrayList<String> possibleDrugs = new ArrayList<>();
        MemberList rl = owner.narrator.getPossibleMembers();
        for(String drug: DRUGS){
            if(possibleDrug(drug, rl, owner.narrator)){
                possibleDrugs.add(drug);
            }
        }

        return possibleDrugs;
    }

    @Override
    public ArrayList<Option> getOptions(Player owner) {
        ArrayList<Option> drugs = new ArrayList<>();
        for(String s: getAvailableDrugs(owner))
            drugs.add(new Option(s));
        return drugs;
    }

    public static boolean possibleDrug(String drug, MemberList rl, Narrator n) {
        if(drug.equalsIgnoreCase(WIPE))
            return true;
        if(drug.equalsIgnoreCase(BLOCKED) && rl.abilityExists(n, Stripper.class)
                && n.getBool(SetupModifier.BLOCK_FEEDBACK))
            return true;
        if(drug.equalsIgnoreCase(DOUSED) && rl.abilityExists(n, Douse.class) && n.getBool(SetupModifier.DOUSE_FEEDBACK))
            return true;
        if(drug.equalsIgnoreCase(DOCTOR) && rl.abilityExists(n, Doctor.class) && n.getBool(SetupModifier.HEAL_FEEDBACK))
            return true;
        if(drug.equalsIgnoreCase(DRIVEN) && rl.abilityExists(n, Driver.class))
            return true;
        if(drug.equalsIgnoreCase(GUARDED) && rl.abilityExists(n, Bodyguard.class))
            return true;
        if(drug.equalsIgnoreCase(POISONED) && rl.abilityExists(n, Poisoner.class))
            return true;
        if(drug.equalsIgnoreCase(WITCH) && rl.abilityExists(n, Witch.class) && n.getBool(SetupModifier.WITCH_FEEDBACK))
            return true;
        if(drug.equalsIgnoreCase(VEST)
                && (rl.abilityExists(n, Armorsmith.class) || rl.abilityExists(n, Blacksmith.class)))
            return true;
        if(drug.equalsIgnoreCase(GUN_RECEIVED) && (rl.abilityExists(n, Gunsmith.class) || rl.abilityExists(n, Blacksmith.class)))
            return true;
        if(drug.equalsIgnoreCase(BREAD) && rl.abilityExists(n, Baker.class))
            return true;
        if(drug.equalsIgnoreCase(CHARGED) && rl.abilityExists(n, ElectroManiac.class))
            return true;
        if(drug.equalsIgnoreCase(SUITTED) && rl.abilityExists(n, Tailor.class) && n.getBool(SetupModifier.TAILOR_FEEDBACK))
            return true;
        if(drug.equalsIgnoreCase(ANNOYED) && rl.abilityExists(n, Jester.class)
                && n.getBool(SetupModifier.JESTER_CAN_ANNOY))
            return true;
        return false;
    }

    @Override
    public String getNightText(Player p) {
        Narrator n = p.narrator;
        String nText = "Type " + SQuote("drug playerName drugName ")
                + " to drug a person so they receive the associated feedback.  Available drugs are: ";
        String drug;
        boolean hasVarietyOfDrugs = false;
        MemberList mList = n.getPossibleMembers();
        for(int i = 0; i < DRUGS.length; i++){
            drug = DRUGS[i];
            if(!possibleDrug(drug, mList, n))
                continue;
            if(i != 0)
                hasVarietyOfDrugs = true;
            nText += drug;
            nText += ", ";
        }
        nText += ". " + WIPING_EXPLANATION;
        if(hasVarietyOfDrugs)
            nText += "  Otherwise, the player will receive an additional feedback.";
        return nText;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        Narrator n = owner.narrator;

        String option = a.getOption();
        if(option == null){
            NoNightActionVisit(a);
            return;
        }
        Feedback fb = new Feedback(target);

        String happeningText = "";
        if(option.equalsIgnoreCase(BLOCKED)){
            Stripper.FeedbackGenerator(fb);
            happeningText = " with a role block";
        }else if(option.equalsIgnoreCase(ANNOYED)){
            fb.add(Jester.FEEDBACK);
            happeningText = " with an annoyance";
            fb.setPicture("jester");
        }else if(option.equalsIgnoreCase(DOUSED)){
            Burn.FeedbackGenerator(fb);
            happeningText = " with a gasoline douse";
        }else if(option.equalsIgnoreCase(CHARGED)){
            ElectroManiac.FeedbackGenerator(fb);
            happeningText = " with an electrical charge";
        }else if(option.equalsIgnoreCase(SUITTED)){
            Tailor.FeedbackGenerator(fb);
            happeningText = " with a suit";
        }else if(option.equalsIgnoreCase(DOCTOR)){
            if(target.isInvulnerable())
                fb.add(Constants.NIGHT_IMMUNE_TARGET_FEEDBACK);
            else
                fb.add(Doctor.TARGET_FEEDBACK);

            happeningText = " with a doctor heal";
            fb.setPicture("doctor");
        }else if(option.equalsIgnoreCase(GUARDED)){
            fb.add(Bodyguard.TARGET_FEEDBACK);
            happeningText = " with a bodyguard save";
            fb.setPicture("bodyguard");
        }else if(option.equalsIgnoreCase(POISONED)){
            Poisoner.FeedbackGenerator(fb, target.isInvulnerable());
            if(target.isInvulnerable()){
                happeningText = " with a failed poison attempt";
            }else{
                happeningText = " with poisoned";
            }
        }else if(option.equalsIgnoreCase(VEST)){
            fb.add(Armorsmith.GetArmorReceiveMessage(n));
            happeningText = " with a " + n.getAlias(Vest.ALIAS);
            if(!target.isSquelched()){
                target.addVest(Vest.FakeVest());
            }
        }else if(option.equalsIgnoreCase(GUN_RECEIVED)){
            fb.add(Gunsmith.GetGunReceiveMessage(n));
            happeningText = " with a " + n.getAlias(Gun.ALIAS);
            if(!target.isSquelched()){
                Gun g = new Gun(null);
                g.setReal(false);
                target.addGun(g);
            }
            fb.setPicture("armorsmith");
        }else if(option.equalsIgnoreCase(BREAD)){
            happeningText = " with " + n.getAlias(Bread.ALIAS);
            Bread.FeedbackDecorator(fb, a);
            if(!a.getTarget().isSquelched()){
                a.getTarget().addBread(Bread.FakeBread());
            }

        }else if(option.equalsIgnoreCase(WIPE)){
            Happening e = new Happening(owner.narrator);
            e.add(owner, " wiped the feedback of ", target, ".");
            fb.add(WIPE);
            a.markCompleted();
            owner.visit(target);
            return;
        }

        Happening e = new Happening(owner.narrator);
        e.add(owner, " drugged ", target, happeningText, ".");
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        ArrayList<String> parsedCommands = new ArrayList<>();
        parsedCommands.add(COMMAND);
        parsedCommands.add(a.getTarget().getName());
        parsedCommands.add(a.getOption());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, ArrayList<String> commands) {
        if(commands.size() != 3){
            throw new PlayerTargetingException("You need a player target and a drug type to deal drugs.");
        }
        commands.remove(0);
        Narrator n = p.narrator;

        String playerName = commands.remove(0);
        Player pi = n.getPlayerByName(playerName);
        if(pi == null)
            throw new UnknownPlayerException(playerName + " isn't someone in the game.");
        String drug = commands.get(0);

        return new Action(p, pi, MAIN_ABILITY, drug);
    }

    private static final String WIPING_EXPLANATION = "Wiping stops the player from receiving almost all feedback.";

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();
        ret.add(WIPING_EXPLANATION);
        return ret;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public String getUsage(Player p, Random r) {
        ArrayList<Option> options = getOptions(p);
        int index = r.nextInt(options.size());
        String ret = super.getUsage(p, r) + " " + options.get(index);
        if(options.size() != 1){
            ret += "\nAvailable drugs: ";
            for(int i = 0; i < options.size(); i++){
                ret += options.get(i);
                if(i != options.size() - 1){
                    ret += ", ";
                }
            }
        }
        return ret;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Drug Dealer", DrugDealer.class);
    }
}
