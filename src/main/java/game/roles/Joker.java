package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Announcement;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.roles.support.Bounty;
import game.roles.support.BountyList;
import game.roles.support.ElectrocutionException;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Joker extends Ability {

    public static final String NIGHT_ACTION_DESCRIPTION = "A bounty hunter that kills if their bounties aren't publically executed.";

    public static final int MAIN_ABILITY = Ability.BOUNTY;
    public int bounty_kills = 0;
    public Bounty activeBounty;
    private ArrayList<Bounty> sideBounties;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public Ability initialize(Player p) {
        if(p.narrator.getPossibleMembers().hasRole(Baker.class))
            sideBounties = new ArrayList<>();

        return super.initialize(p);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        int days = n.getInt(SetupModifier.BOUNTY_INCUBATION);
        int kills = n.getInt(SetupModifier.BOUNTY_FAIL_REWARD);
        return "set a bounty on someone.  If they aren't killed within " + days + " days, you may kill " + kills
                + " people.";
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    public static boolean CanAddBountyActionTonight(Player dead) {
        Joker card = dead.getAbility(Joker.class);
        int bountySubmissions = 0;
        for(Action a: dead.getActions()){
            if(a.is(Joker.MAIN_ABILITY) & a.getOption() == null)
                bountySubmissions++;
        }
        if(card.activeBounty == null && 0 == bountySubmissions)
            return true;
        if(card.sideBounties == null)
            return false;
        int useableBread = BreadAbility.getUseableBread(dead);
        if(card.activeBounty == null)
            return bountySubmissions < useableBread;
        return bountySubmissions < useableBread;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
        if(BreadAbility.getUseableBread(a.owner) == 0 && activeBounty != null)
            Exception("You already have a bounty out");
        if(a.getOption() == null){
            PlayerList targets = a.getTargets();
            Player bounty = targets.remove(0);
            if(bounty.in(targets)){
                Exception("Can't kill someone you're trying to bounty");
            }
        }
    }

    public static final String COMMAND = "Bounty";

    public static final String DEATH_FEEDBACK = "You are a casualty of the failed bounty!";

    public static final String[] BOUNTY_KILL_FLAG = Constants.JOKER_KILL_FLAG;

    public static final String NO_BOUNTY = "kill";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner;
        if(a.getTarget() == null)
            return;
        PlayerList targets = a.getTargets();
        Player newBounty = null;

        if(a.getOption() == null && !Commuter.CommutingTargets(a)){ // this mean a bounty is to be submitted
            targets.remove(0);
            newBounty = a.getActualTargets().getFirst();
        }

        PlayerList electrod = new PlayerList();

        for(Player p: targets){
            this.bounty_kills--;
            try{
                Kill(owner, p, BOUNTY_KILL_FLAG);// visiting taken care of already in kill
            }catch(ElectrocutionException e){
                electrod.add(e.charged);
            }
        }

        Player bountySetter = null;
        if(owner.isAlive()){
            bountySetter = owner;
        }else{
            for(Player p: owner.narrator.getLivePlayers()){
                if(p.is(GraveDigger.class)){
                    for(Action gd_action: p.getActions().getActions(GraveDigger.MAIN_ABILITY)){
                        if(gd_action.getTargets().getFirst() == owner && gd_action.getOption2() == null){
                            bountySetter = p;
                            break;
                        }
                    }
                }
            }
        }

        if(newBounty != null && bountySetter != null){

            Bounty b = newBounty.setBounty(bountySetter);
            if(activeBounty == null)
                activeBounty = b;
            else
                sideBounties.add(b);
            happening(owner, " placed a bounty on ", newBounty);
            try{
                owner.visit(newBounty);
            }catch(ElectrocutionException e){
                electrod.add(e.charged);
            }
        }
        a.markCompleted();
        ElectrocutionException.throwException(owner, electrod);
    }

    private PlayerList getBounties() {
        PlayerList pl = new PlayerList();

        if(activeBounty != null){
            pl.add(activeBounty.target);
        }

        if(sideBounties != null)
            for(Bounty b: sideBounties){
                pl.add(b.target);
            }

        return pl;
    }

    @Override
    public void onDayStart(Player p) {
        if(!p.narrator.isInProgress())
            return;

        if(activeBounty != null)
            activeBounty.dayAnnounce();
        if(sideBounties != null)
            for(Bounty b: sideBounties)
                b.dayAnnounce();
    }

    @Override
    public void onDayEnd(Player p) {
        super.onDayEnd(p);

        int reward = p.narrator.getInt(SetupModifier.BOUNTY_FAIL_REWARD);

        BountyList bs;
        for(Player bounty: getBounties()){
            if(!bounty.isBountied())
                continue;
            bs = bounty.getBounties();
            this.bounty_kills += bs.awardBounties(p).size() * reward;
        }
        if(activeBounty != null && (activeBounty.target.isDead() || activeBounty.failed())){
            activeBounty.onFail();
            activeBounty = null;
        }

        Bounty b;
        if(sideBounties != null)
            for(int i = 0; sideBounties != null && i < sideBounties.size(); i++){
                b = sideBounties.get(i);
                if(b.target.isDead() || b.failed()){
                    b.onFail();
                    sideBounties.remove(i);
                    i--;
                }
            }
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.BOUNTY_INCUBATION);
        rules.add(SetupModifier.BOUNTY_FAIL_REWARD);
        return rules;
    }

    @Override
    public void targetSizeCheck(Action a) {
        int size = a.getTargets().size();
        if(size == 0)
            Exception("You must select at least one target");
        if(a.getOption() == null){
            size--;
        }
        if(size > bounty_kills)
            Exception("You can't submit more than " + bounty_kills + " kill(s)");
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();
        Player owner = actions.get(0).owner;

        PlayerList bounties = new PlayerList();
        PlayerList toBeKilled = new PlayerList();
        PlayerList tempTargs;
        for(Action a: actions){
            tempTargs = a.getTargets();
            if(a.getOption() == null){
                bounties.add(tempTargs.removeFirst());
            }
            toBeKilled.add(tempTargs);
        }

        if(!bounties.isEmpty()){
            list.add("place a bounty on ");
            list.addAll(StringChoice.YouYourself(owner, bounties));
            if(!toBeKilled.isEmpty()){
                list.add(" and ");
            }
        }

        if(!toBeKilled.isEmpty()){
            list.add("kill ");
            list.addAll(StringChoice.YouYourself(owner, toBeKilled));
        }

        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<Object>();
        list.add("Instead of ");

        PlayerList targets = a.getTargets();

        if(a.getOption() != null){
            list.add("placing a bounty on ");
            list.add(StringChoice.YouYourselfSingle(a.owner, a.getTarget()));
            targets.removeFirst();
            if(targets.isEmpty())
                return list;
            list.add(" and ");
        }

        list.add("killing ");
        list.addAll(StringChoice.YouYourself(a.owner, targets));

        return list;
    }

    @Override
    public ArrayList<String> getCommandParts(Action action) {
        ArrayList<String> commands = super.getCommandParts(action);

        if(action.getOption() != null)
            commands.add(NO_BOUNTY);

        return commands;
    }

    @Override
    public Action parseCommand(Player p, ArrayList<String> commands) {
        boolean submittingBounty = true;
        if(commands.get(commands.size() - 1).equalsIgnoreCase(NO_BOUNTY)){
            commands.remove(commands.size() - 1);
            submittingBounty = false;
        }
        Action a = super.parseCommand(p, commands);
        if(!submittingBounty)
            a.setOption(NO_BOUNTY);
        return a;
    }

    @Override
    public boolean onDayEndTriggersWhileDead() {
        return true;
    }

    @Override
    public boolean successfulUsageTriggersBreadUsage(Action a) {
        return activeBounty != null && a.getOption() == null;
    }

    public static Announcement DayStartAnnouncementCreator(Player p, int days) {
        Announcement a = new Announcement(p.narrator);
        a.add("There is a bounty on ", p, "'s head that will expire in ");
        a.add(Integer.toString(days));
        a.add(Util.plural(" day", days), ".");
        a.setPicture("executioner");
        return a;
    }

    public static Announcement DayEndAnnouncementCreator(Player p) {
        Announcement a = new Announcement(p.narrator);
        a.setPicture("executioner");
        a.add("The bounty on ", p, " has expired.  Expect deaths.");
        return a;
    }

    /*
     * @Override public int totalActionWeight(ArrayList<Action> actions) {
     * if(activeBounty == null) return super.totalActionWeight(actions); int weight
     * = 0; boolean initialBountySubmitted = false;
     * 
     * for(Action a: actions){ if(a.getOption() != null){ weight++; continue; }
     * weight++; if(!initialBountySubmitted){ weight++; initialBountySubmitted =
     * true; }
     * 
     * }
     * 
     * return weight; }
     */

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        Narrator n = p.narrator;
        ret.add("Your bounties last " + n.getInt(SetupModifier.BOUNTY_INCUBATION) + " day(s) long.");
        ret.add("Upon bounty failures, you may kill " + n.getInt(SetupModifier.BOUNTY_FAIL_REWARD) + " individual(s)");

        return ret;
    }

    @Override
    public boolean canAddAnotherAction(Action a) {
        if(!Joker.CanAddBountyActionTonight(a.owner) && a.getOption() == null)
            return false;
        return super.canAddAnotherAction(a);
    }

    @Override
    public boolean canSubmitWithoutBread(Action a) {
        if(a.getOption() != null)
            return super.canAddAnotherAction(a);
        if(this.activeBounty == null)
            return true;
        return false;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Joker", Joker.class, Bulletproof.class);
        return FactionService.createFactionRole(faction, role);
    }
}
