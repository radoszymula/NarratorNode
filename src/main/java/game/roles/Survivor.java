package game.roles;

import java.util.ArrayList;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.roles.support.Vest;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Survivor extends Passive {

    public boolean unlimitedVests = false;

    @Override
    public Ability initialize(Player p) {
        if(!p.hasAbility(VestAbility.class))
            p.addAbility(new VestAbility(p));
        return super.initialize(p);
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(GetNightActionDescription(n));
    }

    public static String GetNightActionDescription(Narrator n) {
        return "Survive until the end, sometimes with a a few " + n.getAlias(Vest.ALIAS) + "s for help.";
    }

    @Override
    public void mainAbilityCheck(Action a) {
        noAcceptableTargets();
    }

    public static Vest SurvVest() {
        Vest v = new Vest();
        v.armorSmithVest = false;
        return v;
    }

    @Override
    public void setInitialCharges(int vests, Player player) {
        if(player.narrator.getInt(SetupModifier.CHARGE_VARIABILITY) != 0 && vests != SetupModifiers.UNLIMITED)
            vests = AddNoise(vests, player.narrator);

        player.getAbility(VestAbility.class).removeSurvivorVests();

        if(player != null)
            vests = AddNoise(vests, player.narrator);
        for(int i = 0; i < vests; i++){
            player.addVest(SurvVest());
        }

        unlimitedVests = vests == SetupModifiers.UNLIMITED;

        if(vests == SetupModifiers.UNLIMITED)
            player.addVest(SurvVest());
    }

    @Override
    public boolean unlimitedCharges(Player p) {
        return unlimitedVests;
    }

    public int getSurvivorVests() {
        return getPerceivedCharges();
    }

    @Override
    public int getRealCharges() {
        if(unlimitedVests)
            return SetupModifiers.UNLIMITED;
        return p.getAbility(VestAbility.class).getRealCharges();
    }

    @Override
    public int getPerceivedCharges() {
        if(unlimitedVests)
            return SetupModifiers.UNLIMITED;

        return VestAbility.GetSurvivorVests(p);
    }

    @Override
    public void onAbilityLoss(Player p) {
        super.onAbilityLoss(p);
        p.getAbility(VestAbility.class).removeSurvivorVests();
    }

    @Override
    public void onDayStart(Player p) {
        super.onPlayerNightEnd(p);
        if(VestAbility.GetSurvivorVests(p) == 0 && unlimitedVests){
            p.addVest(SurvVest());
        }
    }

    @Override
    public boolean isPowerRole() {
        return getSurvivorVests() == 0;
    }

    @Override
    public boolean chargeModifiable() {
        return true;
    }

    @Override
    public boolean isActivePassive(Player p) {
        if(unlimitedVests)
            return true;
        return getSurvivorVests() != 0;
    }

    @Override
    public String getRuleChargeText(Narrator n) {
        int val = this.modifiers.getInt(AbilityModifier.CHARGES);
        StringBuilder sb = new StringBuilder("Starts off with ");
        if(n.getInt(SetupModifier.CHARGE_VARIABILITY) != 0)
            sb.append("~");
        sb.append(val);
        sb.append(" ");
        sb.append(n.getAlias(Vest.ALIAS));
        if(val != 1)
            sb.append("s");
        return sb.toString();
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        VestAbility va = p.getAbility(VestAbility.class);
        if(va.hasItems())
            return super.getSpecificRoleSpecs(p);

        ArrayList<String> specs = new ArrayList<>();
        specs.add("You have no more " + p.narrator.getAlias(Vest.ALIAS) + ".");
        return specs;
    }

    public static FactionRole template(Faction benigns) {
        return FactionService.createFactionRole(benigns, RoleService.createRole(benigns.narrator, "Survivor", Survivor.class));
    }
}
