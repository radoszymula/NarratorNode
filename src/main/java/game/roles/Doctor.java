package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.logic.support.saves.Heal;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Doctor extends Ability {

    public static final String COMMAND = "Heal";

    public static final String ROLE_DETAIL_POISON_CANNOT_STOP = "Healing can't stop poisoning";
    public static final String ROLE_DETAIL_POISON_CAN_STOP = "Healing can stop poisonging";

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final int MAIN_ABILITY = Ability.HEAL;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Save someone from an attack.";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.HEAL_SUCCESS_FEEDBACK);
        rules.add(SetupModifier.HEAL_FEEDBACK);
        rules.add(SetupModifier.HEAL_BLOCKS_POISON);
        return rules;
    }

    @Override
    public List<SetupModifier> getSetupModifiersForRoleDetails(Narrator narrator) {
        List<SetupModifier> modifiers = this.getSetupModifiers();
        if(!narrator.getPossibleMembers().contains(Poisoner.class))
            modifiers.remove(SetupModifier.HEAL_BLOCKS_POISON);
        return modifiers;
    }

    public static final String SUCCESFULL_HEAL = "Your target was attacked tonight!";
    public static final String TARGET_FEEDBACK = "You were healed by a doctor!";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        happening(owner, " healed ", target);
        // but the person is healed either way
        target.heal(new Heal(owner, Constants.DOCTOR_HEAL_FLAG));
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.HEAL_SUCCESS_FEEDBACK)){
            ret.add("You will know if your target was attacked.");
        }else{
            ret.add("You won't know if your target was attacked.");
        }

        if(p.narrator.getBool(SetupModifier.HEAL_FEEDBACK)){
            ret.add("Your target will know if they were attacked and healed, but not who healed them.");
        }else{
            ret.add("Your taget will not know that they were attacked and healed.");
        }

        return ret;
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Doctor", Doctor.class);
    }
}
