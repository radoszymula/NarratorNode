package game.roles;

import game.logic.Narrator;

public class Cultist extends Passive {

    public Cultist() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "A member of the ever-growing cult.";

    @Override
    public void checkIsNightless(Narrator n) {
    }
}
