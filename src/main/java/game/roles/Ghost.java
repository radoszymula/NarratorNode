package game.roles;

import java.util.ArrayList;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.HTString;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Ghost extends Ability {

    public static final int MAIN_ABILITY = Ability.POSSESS;

    @Override
    public Ability initialize(Player p) {
        super.initialize(p);
        PuppetVote.AddAbility(p);
        return this;
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public boolean isChatRole() {
        return true;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Die, and be able to possess people during the day.  Make the person who killed you lose.";

    public static final String COMMAND = "Possess";

    @Override
    public void mainAbilityCheck(Action a) {
        if(a.owner.isAlive() || total_loss)
            noAcceptableTargets();
        else
            deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        if(owner.isAlive()){
            NoNightActionVisit(a);
            return;
        }
        target.addVentController(owner);

        a.markCompleted();
        owner.visit(target);
    }

    @Override
    public Boolean checkWinEnemyless(Player p) {
        if(p.isAlive())
            return false;
        Player cause = p.getDeathType().getCause();
        if(cause == p && p.getCause() != null)
            return !p.getCause().isWinner();
        if(cause == null)
            return false;
        if(cause.isDead() && cause.is(Ghost.class) && cause.getDeathType().getCause() == p)
            return false;
        return !cause.isWinner();
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return p.hasPuppets();
    }

    @Override
    public void doDayAction(Player owner, PlayerList targets) {
        Ventriloquist.ControlDayAction(owner, targets);
    }

    @Override
    public boolean canStartWithEnemies() {
        return false;
    }

    @Override
    public boolean isRecruitable() {
        return false;
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    public boolean isActivePassive(Player p) {
        return true;
    }

    boolean total_loss = false;

    @Override
    public void onDeath(Player p) {
        Feedback f;
        Player cause = p.getDeathType().getCause();
        if(cause == null){
            total_loss = true;
            f = new Feedback(p, "You've died and lost, but don't have someone to try to make to lose.");
        }else{
            f = new Feedback(p, "You've died.  To win you must make the ");
            f.add(new HTString(cause.getFaction()), " lose.");
        }

        p.sendMessage(f);

    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> specs = new ArrayList<>();
        if(p.isAlive()){
            specs.add("You must die.  After, you will know what faction you must try and take down.");
        }else if(p.getDeathType().getCause() != null){
            specs.add("You must make the " + p.getDeathType().getCause().getFaction() + " lose.");
        }else{
            specs.add("You cannot possibly win and may not interact with the dead anymore.");
        }
        return specs;
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Ghost", Ghost.class);
        return FactionService.createFactionRole(faction, role);
    }
}
