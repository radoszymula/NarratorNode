package game.roles;

import java.util.ArrayList;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalActionException;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.roles.support.Gun;

public class GunAbility extends ItemAbility<Gun> {

    public static final String COMMAND = Constants.GUN_COMMAND;
    public static final int MAIN_ABILITY = Ability.SHOOT;
    public static final String ALIAS = "vest_alias";

    private boolean usedGunToday = false;

    public GunAbility() {
    }

    public GunAbility(Player p, Gun gun) {
        super(p, gun);
    }

    public GunAbility(Player p) {
        super(p);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return "use a " + n.getAlias(ALIAS) + " to survive the night";
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public String getCommand() {
        return Gun.COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        if(target == null)
            return;

        if(this.hasItems()){
            Gun g = get(0);
            g.shoot(a);
            a.markCompleted();
        }else
            NoNightActionVisit(a);
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("gunning down ");
        list.add(StringChoice.YouYourselfSingle(a.owner, a.getTarget()));
        return list;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return new String[] {};
    }

    @Override
    public void mainAbilityCheck(Action a) {
        Player owner = a.owner;
        Narrator n = owner.narrator;
        if(a.getTargets().size() != 1)
            Exception("You must select one and only one target");
        if(!hasItems())
            Exception("You don't have any " + n.getAlias(Gun.ALIAS) + " to use!");
        deadCheck(a);
        if(owner.narrator.getBool(SetupModifier.GS_DAY_GUNS)){
            boolean isDay = owner.narrator.isDay();
            for(Gun g: this){
                if(g.getFlag() == Constants.GUN_KILL_FLAG){
                    if(isDay)
                        return;
                }else{
                    if(!isDay)
                        return;
                }
            }
            if(isDay)
                Exception("You don't have any " + n.getAlias(Gun.ALIAS) + "s given to you by a gunsmith");
            else
                Exception("You don't have any " + n.getAlias(Gun.ALIAS) + "s you can use right now.");
        }
        return;
    }

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        if(!hasItems())
            return new PlayerList();
        return pi.narrator.getLivePlayers().remove(pi);
    }

    @Override
    public void doDayAction(Player owner, PlayerList targets) {
        Narrator n = owner.narrator;
        Gun g;
        for(int i = 0; i < size(); i++){
            g = get(i);
            if(g.getFlag() == Constants.GUN_KILL_FLAG){
                g.shoot(new Action(owner, targets, Ability.SHOOT, null, (String) null)); // TODO null role gunning
                                                                                         // should be an action,
                                                                                         // honestly
                remove(i);
                usedGunToday = true;
                n.parityCheck();
                return;
            }
        }
        throw new IllegalActionException("No guns to shoot");
    }

    @Override
    public void onDayEnd(Player p) {
        super.onDayEnd(p);
        usedGunToday = false;
    }

    public boolean hasVigiGuns() {
        for(Gun g: this){
            if(Vigilante.isVigiGun(g))
                return true;
        }
        return false;
    }

    @Override
    public boolean canAddAnotherAction(Action a) {
        if(Vigilante.UnlimitedShots(a.owner))
            return super.canAddAnotherAction(a);
        int shotsToFire = a.owner.getActions().getActions(Ability.SHOOT).size();
        int availableGuns = size();
        if(shotsToFire >= availableGuns && availableGuns >= 0)
            return false;
        return super.canAddAnotherAction(a);
    }

    @Override
    public boolean isOnCooldown() {
        if(p.narrator.isDay()){
            if(!p.narrator.getBool(SetupModifier.GS_DAY_GUNS) || usedGunToday)
                return false;
        }

        return super.isOnCooldown();
    }

    @Override
    public boolean isNightAbility(Player p) {
        if(hasVigiGuns())
            return true;
        return !p.narrator.getBool(SetupModifier.GS_DAY_GUNS);
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return !p.isSilenced() && !p.isDisenfranchised() && !p.isPuppeted()
                && p.narrator.getBool(SetupModifier.GS_DAY_GUNS) && size() - Vigilante.GetRealVigiGunCount(p) > 0;
    }

    @Override
    public boolean stopsParity(Player p) {
        if(!p.narrator.getBool(SetupModifier.GS_DAY_GUNS))
            return false;

        for(Gun g: this){
            if(g.isReal() && !Vigilante.isVigiGun(g))
                return true;
        }
        return false;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        if(!hasItems())
            return super.getSpecificRoleSpecs(p);

        ArrayList<String> specs = new ArrayList<>();
        if(size() == 1 && !Vigilante.UnlimitedShots(p))
            specs.add("You have 1 " + p.narrator.getAlias(Gun.ALIAS) + " left to use.");
        else if(size() > 1)
            specs.add("You have " + size() + " " + p.narrator.getAlias(Gun.ALIAS) + "s left to use.");

        return specs;
    }

    public void removeVigiGuns() {
        for(int i = 0; i < size(); i++){
            if(Vigilante.isVigiGun(get(i))){
                remove(i);
                i--;
            }
        }
    }

    @Override
    public int getPerceivedCharges() {
        if(Vigilante.UnlimitedShots(p))
            return SetupModifiers.UNLIMITED;
        return super.getPerceivedCharges();
    }

    public static boolean HasNightGuns(Player p) {
        if(!p.hasAbility(GunAbility.class))
            return false;
        GunAbility ga = p.getAbility(GunAbility.class);
        if(ga.getRealCharges() == 0)
            return false;

        if(ga.hasVigiGuns())
            return true;

        // has regular guns, but no vigi guns
        return p.narrator.getBool(SetupModifier.GS_DAY_GUNS);
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        Player owner = actions.get(0).owner;
        PlayerList targets = getActionTargets(actions);

        ArrayList<Object> list = new ArrayList<>();
        list.add(owner.narrator.getAlias(Gun.ALIAS) + " down ");
        list.addAll(StringChoice.YouYourself(owner, targets));

        return list;
    }

    public static int getTotalGunCount(Player p) {
        if(!p.hasAbility(GunAbility.class))
            return 0;
        GunAbility ga = p.getAbility(GunAbility.class);
        return ga.size();
    }

    public static int getRealGunCount(Player player) {
        if(!player.hasAbility(GunAbility.class))
            return 0;
        return player.getAbility(GunAbility.class).getRealCharges();
    }

    @Override
    protected Gun getFakeConsumable() {
        return (Gun) new Gun(null).setReal(false);
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    @Override
    public int maxNumberOfSubmissions() {
        if(Vigilante.UnlimitedShots(p))
            return Integer.MAX_VALUE;
        return super.maxNumberOfSubmissions();
    }

    @Override
    public void checkPerformDuringDay(Player owner) {
        Narrator n = owner.narrator;
        if(!n.getBool(SetupModifier.GS_DAY_GUNS))
            throw new IllegalActionException("Cannot shoot guns during the day.");
        if(usedGunToday)
            throw new IllegalActionException("Cannot shoot guns twice");
    }

    @Override
    public String getAliasKey() {
        return ALIAS;
    }
}
