package game.roles;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class SerialKiller extends Ability {

    public static final String ROLE_NAME = "Serial Killer";

    public static final String NIGHT_ACTION_DESCRIPTION = "A crazed psychopath trying to kill in the town.";

    public static final int MAIN_ABILITY = Ability.STAB;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return "stab someone, killing them";
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    public static final String COMMAND = "Stab";

    public static final String DEATH_FEEDBACK = "You were killed by a Serial Killer!";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        a.markCompleted();
        Kill(owner, target, Constants.SK_KILL_FLAG);// visiting taken care of already in kill
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Serial Killer", SerialKiller.class, Bulletproof.class);
        return FactionService.createFactionRole(faction, role);
    }
}
