package game.roles;

import game.logic.Narrator;

public class Infiltrator extends Passive {

    public Infiltrator() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Retain original team upon any sort of recruitment.";

    @Override
    public boolean allowsForFriendlyFire() {
        return true;
    }

    @Override
    public boolean isPowerRole() {
        return true;
    }
}
