package game.roles;

import java.util.ArrayList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Bulletproof extends Passive {

    public Bulletproof() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public void adjustRoleSpecs(List<String> specs) {
        specs.remove(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public Ability initialize(Player p) {
        p.setInvulnerable(true);
        happening(p, " is immune to night kills.");
        return super.initialize(p);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Invulnerable at night";

    @Override
    public void checkIsNightless(Narrator n) {
    }

    @Override
    public boolean hiddenPossible() {
        return true;
    }

    @Override
    public ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();
        ret.add("You are invulnerable at night.");
        return ret;
    }

    @Override
    public boolean canSelfTarget(Narrator narrator) {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Bulletproof", Bulletproof.class);
        return FactionService.createFactionRole(faction, role);
    }
}
