package game.roles;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Ventriloquist extends Ability {

    public static final int MAIN_ABILITY = Ability.PUPPET;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public Ability initialize(Player p) {
        PuppetVote.AddAbility(p);
        return super.initialize(p);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "A master puppeteer, taking control of people's words and votes during the day.";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    public static final String COMMAND = "Puppet";
    public static final String VENT_SKIP_VOTE = "vskipday";
    public static final String VENT_UNVOTE = "vunvote";
    public static final String VENT_VOTE = "vvote";
    public static final String VENT_PUNCH = "vpunch";
    public static final String FEEDBACK = "You suddenly lose the ability to vote and speak, horrified by the thought of not being able to control your actions.";

    @Override
    public void doNightAction(Action action) {
        Player owner = action.owner, target = action.getTarget();
        if(target == null)
            return;
        target.addVentController(owner);
        action.markCompleted();
        owner.visit(target);
    }

    @Override
    public boolean isChatRole() {
        return true;
    }

    // target sizes :
    // 1 means skip vote
    // 2 with a 'self vote' means unvote
    // 3 otherwise regular vote
    public static void ControlDayAction(Player owner, PlayerList targets) {
        if(!owner.hasPuppets()){
            VentException();
        }
        if(targets.isEmpty() || targets.size() > 2){
            throw new PlayerTargetingException("Need two targets to vote");
        }

        Player puppet = targets.get(0);
        if(targets.size() == 1){
            puppet.voteSkip(owner);
        }else{
            Player voteTarget = targets.get(1);
            if(voteTarget == puppet)
                puppet._unvote(owner, true);// record command
            else
                puppet._vote(voteTarget, owner, true);// record command

        }
    }

    public static void VentException() {
        throw new IllegalActionException("You cannot vote as other people.");
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Ventriloquist", Ventriloquist.class);
        return FactionService.createFactionRole(faction, role);
    }
}
