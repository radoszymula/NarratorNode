package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Announcement;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.IllegalActionException;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Mayor extends Ability {

    public Mayor() {
    }

    public static final int MAIN_ABILITY = Ability.REVEAL;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        if(getRevealed())
            return new PlayerList();
        return null;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.MAYOR_VOTE_POWER);
        return rules;
    }

    public String getNightText(Narrator n) {
        return Constants.NO_NIGHT_ACTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Reveal during the day to lead the town while also gaining extra votes.";

    public static final String COMMAND = "Reveal";
    public static final String REVEAL_LOWERCASE = "reveal";

    @Override
    public void mainAbilityCheck(Action a) {
    }

    @Override
    public void doNightAction(Action a) {
        NoNightActionVisit(a);
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return !getRevealed() && !p.isSilenced() && !p.isPuppeted();
    }

    @Override
    public void doDayAction(Player mayor, PlayerList unused) {
        if(mayor.isSilenced())
            throw new IllegalActionException("Can't reveal if blackmailed");
        Narrator narrator = mayor.narrator;
        Announcement e = new Announcement(narrator);
        e.setPicture("mayor");
        narrator.getEventManager().addCommand(mayor, COMMAND);

        StringChoice sc = new StringChoice(mayor);
        sc.add(mayor, "You");
        e.add(sc, " ");

        sc = new StringChoice("has");
        sc.add(mayor, "have");

        e.add(sc, " revealed as the ", new HTString(mayor.getRoleName(), mayor.getColor()), "!");

        setRevealed();
        e.finalize();

        mayor.increaseVotePower(mayor.narrator.getInt(SetupModifier.MAYOR_VOTE_POWER));

        narrator.checkVote();

        if(narrator.isDay()){ // if its still day
            List<NarratorListener> listeners = narrator.getListeners();
            for(int i = 0; i < listeners.size(); i++)
                listeners.get(i).onRoleReveal(mayor, e);
            narrator.parityCheck();
        }
    }

    private void setRevealed() {
        isRevealed = true;
    }

    private boolean isRevealed = false;

    private boolean getRevealed() {
        return isRevealed;
    }

    @Override
    public String getDayCommand() {
        return COMMAND;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        int voteCount = p.narrator.getInt(SetupModifier.MAYOR_VOTE_POWER);
        if(canUseDuringDay(p)){
            ret.add("You may reveal yourself during the day.  In addition your vote power will be increased by "
                    + voteCount + ".");
        }else{
            ret.add("Your vote power is " + voteCount + ".");
        }
        return ret;
    }

    @Override
    public boolean chargeModifiable() {
        return false;
    }

    @Override
    public boolean canEditBackToBackModifier() {
        return false;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public boolean stopsParity(Player p) {
        if(p.isPuppeted() || p.isSilenced())
            return false;
        return !getRevealed();
    }

    @Override
    public Action getExampleAction(Player owner) {
        return null;
    }

    @Override
    public void checkIsNightless(Narrator n) {
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(!a.getTargets().isEmpty())
            Exception("You can't target anyone with that ability");
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Mayor", Mayor.class);
        role.addModifier(RoleModifier.UNIQUE, true);
        return FactionService.createFactionRole(faction, role);
    }
}
