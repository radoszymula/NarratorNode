package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Poisoner extends Ability {

    public static final String COMMAND = "Poison";

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final int MAIN_ABILITY = Ability.POISON;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Poison someone at night. Poisoned targets die at the end of the next day.";
    public static final String POISONER_FEEDBACK = "You were poisoned!";
    public static final String POISONER_FAILED_FEEDBACK = "Someone tried to poison you last night!";
    public static final String DEATH_FEEDBACK = "You succumbed to last night's poison.";
    public static final String BROADCAST_MESSAGE = "'s body couldn't fight the poison anymore, and fell over dead.";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        if(target.narrator.getBool(SetupModifier.GUARD_REDIRECTS_POISON))
            target = Bodyguard.redirect(target);
        if(owner.getFaction().knowsTeam() && owner.getFaction() == target.getFaction())
            return;
        happening(owner, " poisoned ", target);
        a.markCompleted();
        Kill(owner, target, Constants.POISON_KILL_FLAG);
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.HEAL_BLOCKS_POISON);
        rules.add(SetupModifier.GUARD_REDIRECTS_DOUSE);
        return rules;
    }

    public static void FeedbackGenerator(Feedback fb, boolean isImmune) {
        fb.setPicture("poisoner");
        if(isImmune){
            fb.add(Poisoner.POISONER_FAILED_FEEDBACK);
            fb.addExtraInfo("Whoever poisoned you might wonder why you survived...");
        }else{
            fb.add(Poisoner.POISONER_FEEDBACK);
            fb.addExtraInfo("You won't live to see the next night.");
        }
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> hasBurn = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.GUARD_REDIRECTS_POISON))
            hasBurn.add("Players guarding your poisoned target will absorb the poison instead.");

        return hasBurn;
    }

    // fake poisoning, change ability to make this check overrideable
    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Poisoner", Poisoner.class, Bulletproof.class);
    }
}
