package game.roles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.UnsupportedMethodException;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;

public class FactionSend extends Ability {

    public static final String NIGHT_ACTION_DESCRIPTION = "vote on who controls your factional kill, if there are any ties";

    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() == 0)
            Exception("Can't decide that no one should use team abilities.");
        if(a.owner.isSilenced()){
            if(a._targets.size() != 1)
                Exception("Can't decide who should use team abilities if blackmailed.");
        }
    }

    @Override
    public void mainAbilityCheck(Action a) {
        Player owner = a.owner;
        if(owner.isSilenced() && a.getTarget() != owner)
            Exception("Cannot indicate who should control faction abilities while blackmailed.");
        deadCheck(a);
        PlayerList targets = PlayerList.GetUnique(a.getTargets());
        if(targets.size() != a.getTargets().size())
            Exception("Can't have two of the same person in your decision list.");
        boolean foundAlly;
        for(Player p: targets){
            foundAlly = false;
            for(Faction t: owner.getFactions()){
                if(t.size() == 1)
                    continue;
                if(!t.knowsTeam())
                    continue;
                if(t.getMembers().contains(p)){
                    foundAlly = true;
                    break;
                }
            }
            if(!foundAlly)
                Exception("May only allow allies to use team powers");
        }
    }

    public static final String COMMAND = Faction.SEND;
    public static final int MAIN_ABILITY = Ability.NIGHT_SEND;

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getCommand() {
        return Faction.SEND;
    }

    @Override
    public int getAbilityNumber() {
        return Faction.SEND_;
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        for(Faction t: p.getFactions()){
            if(!t.hasSharedAbilities())
                continue;
            if(t.getMembers().size() == 1)
                return new PlayerList();
            if(p.isSilenced())
                return Player.list(p);
            return t.getMembers().copy();
        }
        return new PlayerList();
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        // TODO account for multiple people
        StringChoice scTarget = new StringChoice(actions.get(0).getTarget());
        scTarget.add(actions.get(0).getTarget(), "yourself");

        list.add(" voted to allow ");
        list.add(scTarget);
        list.add(" to use the factional kill"); // if it's just single, vs not another factional ability
        return list;
    }

    public static List<List<Set<Player>>> CondorcetOrdering(PlayerList factionMembers) {
        List<List<Set<Player>>> rankings = new ArrayList<>();
        List<Set<Player>> ranking;
        Set<Player> element;
        Action action;
        for(Player player: factionMembers){
            action = player.getAction(FactionSend.MAIN_ABILITY);
            if(action == null)
                continue;
            ranking = new ArrayList<>();
            for(int i = 0; i < player.nightVotePower(factionMembers); i++)
                rankings.add(ranking);
            for(Player sendPreference: action._targets){
                element = new HashSet<>();
                element.add(sendPreference);
                ranking.add(element);
            }
        }
        return rankings;
    }

    @Override
    public void doNightAction(Action a) {
        throw new UnsupportedMethodException();
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }
}
