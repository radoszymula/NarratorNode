package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Happening;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;

public class ProxyKill extends Ability {

    public static final String NIGHT_ACTION_DESCRIPTION = "Ability to order a teammate to kill";

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() != 2)
            Exception("You must select 2 and only 2 targets for murdering");
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> list = new LinkedList<>();
        list.add(SetupModifier.PROXY_UPGRADE);
        return list;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
        if(!a.getTargets().get(0).in(a.owner.getFaction().getMembers()))
            Exception("Killer must be in team");
    }

    public static final String COMMAND = "Murder";
    public static final int MAIN_ABILITY = Ability.MURDER;

    @Override
    public void doNightAction(Action a) {
        if(a.owner.narrator.proxyWitchVisiting){
            a.markCompleted();
            if(a._targets.size() > 2){
                NoNightActionVisit(a.owner, a._targets.get(2));
                return;
            }
            return;
        }
        Player sender, target;
        if(a.getTargets().size() < 2 && a.owner.isDead()){
            sender = a.owner;
            target = a.getTargets().getFirst();
        }else{
            sender = a.getTargets().getFirst();
            target = a.getTargets().get(1);
        }
        if(target == null || sender == null || sender.isBlocked())
            return;

        sender.addTempAbility(new FactionKill().initialize(a.owner.getFaction()));

        sender.getActions().extraActions.add(new Action(sender, FactionKill.MAIN_ABILITY, target).setProxied());

        if(a.owner != sender){
            Happening e = new Happening(a.owner.narrator);
            e.add(a.owner, " sent ", sender, " to kill ", target, ".");
        }
    }

    @Override
    public void onDeath(Player p) {
        if(p.narrator.getBool(SetupModifier.PROXY_UPGRADE) && p.getFaction().knowsTeam()){
            p.getFaction().addAttainedAbility(new FactionKill().initialize(p.getFaction())); // true indicates, proxy
                                                                                             // kill override
        }
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();

        // MemberList ml = owner.narrator.getPossibleMembers();

        for(Action da: actionList){
            if(da.getTargets().getFirst() == da.owner){
                list.add("kill ");
                list.add(StringChoice.YouYourselfSingle(da.owner, da.getTargets().getLast()));
                list.add(" and ");
                continue;
            }

            list.add("send ");
            list.add(StringChoice.YouYourselfSingle(da.owner, da.getTargets().getFirst()));
            list.add(" to kill ");
            list.add(StringChoice.YouYourselfSingle(da.owner, da.getTargets().getLast()));
            list.add(" and ");

        }
        list.remove(list.size() - 1);

        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("sending ");
        list.add(StringChoice.YouYourselfSingle(a.owner, a.getTargets().getFirst()));
        list.add(" to kill ");
        list.add(StringChoice.YouYourselfSingle(a.owner, a.getTargets().getLast()));
        return list;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        PlayerList livesy = p.narrator.getLivePlayers().sortByName();

        PlayerList teamMembers = p.getFaction().getMembers().sortByName();

        PlayerList ret = new PlayerList();
        ret.add(p);

        for(Player tm: teamMembers){
            if(!tm.in(ret))
                ret.add(tm);
        }

        for(Player live: livesy){
            if(!live.in(ret))
                ret.add(live);
        }

        return ret;
    }

    @Override
    public String getUsage(Player p, Random r) {
        Player team = p.getFaction().getMembers().getRandom(r);
        PlayerList live = p.narrator.getLivePlayers();
        live.shuffle(r, null);
        return getCommand() + " *" + team.getName() + " " + live.getLast().getName() + "*";
    }

    @Override
    public boolean isDatabaseAbility() {
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }
}
