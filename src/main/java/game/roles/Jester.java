package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.attacks.IndirectAttack;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Jester extends Ability {

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final int MAIN_ABILITY = Ability.ANNOY;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return "annoy people, leaving them feedback";
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    public static final String COMMAND = "Annoy";
    public static final String FEEDBACK = "You were annoyed last night by a crazy person.";
    public static final String NIGHT_ACTION_DESCRIPTION = "Goal is to die via a day execution, through any means necessary.";
    public static final String DEATH_FEEDBACK = "You killed yourself because you voted for the jester last night!";

    @Override
    public void mainAbilityCheck(Action a) {
        if(!a.owner.narrator.getBool(SetupModifier.JESTER_CAN_ANNOY))
            noAcceptableTargets();
    }

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        if(target == null)
            return;
        Message fb = new Feedback(a.getTarget(), FEEDBACK).setPicture("jester");
        if(!a.owner.narrator.getPossibleMembers().contains(new AbilityList(new DrugDealer()))){
            fb.addExtraInfo("You know there is a confirmed Jester in the game.");
        }
        happening(a.owner, " annoyed ", target);
        a.markCompleted();
        a.owner.visit(target);
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.JESTER_KILLS);
        rules.add(SetupModifier.JESTER_CAN_ANNOY);
        return rules;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getInt(SetupModifier.JESTER_KILLS) > 0){
            ret.add("If you get publically executed, " + p.narrator.getInt(SetupModifier.JESTER_KILLS)
                    + " will commit suicide in guilt.");
        }else{
            ret.add("There are no consequences to you dying.");
        }

        return ret;
    }

    @Override
    public Boolean checkWinEnemyless(Player p) {
        if(p.isAlive())
            return false;
        return p.getDeathType().isLynch();
    }

    @Override
    public boolean canStartWithEnemies() {
        return false;
    }

    public static void handleSuicides(Narrator n) {
        if(n.getInt(SetupModifier.JESTER_KILLS) > 0){
            PlayerList jesterList = new PlayerList();
            // jester kills
            for(Player p: n._players)
                if(p.getVotedForJester())
                    jesterList.add(p);
            jesterList.sortByName();
            if(jesterList.size() > 0){
                double suicides_d = (jesterList.size() * n.getInt(SetupModifier.JESTER_KILLS)) / 100.;
                int suicides_i = (int) Math.ceil(suicides_d);
                suicides_i = Math.min(jesterList.size(), suicides_i);

                PlayerList suicides = new PlayerList();
                Player sKilled;
                while (suicides_i > 0){
                    sKilled = jesterList.getRandom(n.getRandom());
                    sKilled.votedForJester(false);
                    sKilled.kill(new IndirectAttack(Constants.JESTER_KILL_FLAG, sKilled, null));
                    jesterList.remove(sKilled);
                    suicides_i--;
                }
                if(!suicides.isEmpty())
                    new Happening(n).add(suicides, " suicided over the jester kill.");
            }
        }

    }

    @Override
    public boolean isActivePassive(Player p) {
        for(Faction t: p.getFactions()){
            if(t.hasEnemies())
                return false;
        }
        return true;
    }

    @Override
    public void checkIsNightless(Narrator n) {
        if(n.getBool(SetupModifier.JESTER_CAN_ANNOY))
            super.checkIsNightless(n);
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Jester", Jester.class);
        return FactionService.createFactionRole(faction, role);
    }
}
