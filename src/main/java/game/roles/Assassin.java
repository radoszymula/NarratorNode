package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.DeathAnnouncement;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.attacks.IndirectAttack;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.Modifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Assassin extends Ability {

    public static final String[] SECRET_KILL = new String[] {
            "assassin_secret", "Day killer name is silent.",

    };

    public Assassin() {
    }

    public static final int MAIN_ABILITY = Ability.ASSASSINATE;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    private boolean hasKnife;

    @Override
    public Ability initialize(Player p) {
        super.initialize(p);
        hasKnife = true;

        return this;
    }

    public String getNightText(Narrator n) {
        return Constants.NO_NIGHT_ACTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Day-kill someone once during the day!";

    public static final String COMMAND = "Assassinate";
    public static final String ASSASSINATE_LOWERCASE = "assassinate";

    @Override
    public void mainAbilityCheck(Action a) {
        if(a.owner.narrator.isNight())
            noAcceptableTargets();
    }

    @Override
    public void doNightAction(Action a) {
        NoNightActionVisit(a);
    }

    @Override
    public String getDayCommand() {
        if(hasKnife())
            return COMMAND;
        return null;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return hasKnife() && !p.isPuppeted();
    }

    private boolean hasKnife() {
        return hasKnife;
    }

    @Override
    public void doDayAction(Player owner, PlayerList targets) {
        if(targets.size() != 1)
            Exception("Cannot assassinate more than one person");
        Narrator n = owner.narrator;
        Player target = targets.getFirst();
        boolean convertedAlly = n.getPossibleMembers().hasRole(CultLeader.class) && !n.isFirstDay();
        boolean disguisedAlly = n.getPossibleMembers().hasRole(Disguiser.class)
                && !n.getDeadPlayers().filterHiddenDeath().isEmpty();
        if(owner.getFaction().knowsTeam() && !(convertedAlly || disguisedAlly)){
            if(target.getFaction().equals(owner.getFaction()) && !enemyHasAssassin(owner.getFaction(), n)){
                Exception("Don't assassinate allies!");
            }
        }

        DeathAnnouncement e = new DeathAnnouncement(target);
        e.setPicture("assassin");
        n.getEventManager().addCommand(owner, COMMAND, target.getName());

        StringChoice sc = new StringChoice(owner);
        sc.add(owner, "You");
        if(secretKill())
            sc.add(Message.PUBLIC, "Someone");
        e.add(sc, " assassinated ");

        StringChoice target_sc = new StringChoice(target);
        target_sc.add(target, "you");
        e.add(target_sc);
        e.add(".");

        hasKnife = false;

        PlayerList deadPeople = new PlayerList(target);
        DeathAnnouncement explosion = null;

        // was formerly daykill
        target.setDead(owner);
        target.getDeathType().addDeath(Constants.ASSASSIN_KILL_FLAG);

        n.announcement(e);

        if((target.is(ElectroManiac.class) || target.isCharged()) && owner.isCharged()){
            if(!owner.isInvulnerable()){
                owner.dayKill(new IndirectAttack(Constants.ELECTRO_KILL_FLAG, owner, owner.getCharger()),
                        Narrator.DAY_NOT_ENDING);
                deadPeople.add(owner);
            }
            if(!target.isInvulnerable() && !target.is(ElectroManiac.class))
                target.getDeathType().addDeath(Constants.ELECTRO_KILL_FLAG);
            explosion = Burn.ExplosionAnnouncement(n, deadPeople, "electromaniac");
            n.announcement(explosion);
        }

        n.voteSystem.removeFromVotes(deadPeople);
        n.checkVote();
        n.checkProgress();

        if(n.isInProgress()){
            List<NarratorListener> listeners = owner.narrator.getListeners();
            for(int i = 0; i < listeners.size(); i++)
                listeners.get(i).onAssassination(owner, target, e);
        }
    }

    private boolean secretKill() {
        return this.modifiers.getOrDefault(AbilityModifier.SECRET_KILL, false);
    }

    private boolean enemyHasAssassin(Faction team, Narrator n) {
        ArrayList<Faction> teamsWithAssassins = n.getPossibleMembers().getFactions(Assassin.class, n);
        Faction enemy;
        for(String s: team.getEnemyColors()){
            enemy = n.getFaction(s);
            if(teamsWithAssassins.contains(enemy))
                return true;
        }
        return false;
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public void checkPerformDuringDay(Player owner) {
        if(canUseDuringDay(owner) && owner.narrator.isDay())
            return;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(canUseDuringDay(p)){
            ret.add("You can assassinate during the day.");
            if(secretKill())
                ret.add("No one will know you commited the assassinatino.");
            else
                ret.add("Everyone will know that you commited the assassination.");
        }else{
            ret.add("You cannot assassinate again.");
        }

        return ret;
    }

    @Override
    public boolean isPowerRole() {
        return hasKnife();
    }

    @Override
    public boolean chargeModifiable() {
        return false;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public boolean stopsParity(Player p) {
        if(p.isPuppeted())
            return false;
        return hasKnife();
    }

    @Override
    public void checkIsNightless(Narrator n) {
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    @Override
    public boolean isBool(Modifier modifier) {
        if(AbilityModifier.SECRET_KILL == modifier)
            return true;
        return super.isBool(modifier);
    }

    @Override
    public List<AbilityModifier> getAbilityModifiers() {
        List<AbilityModifier> rules = new LinkedList<>();
        rules.add(AbilityModifier.SECRET_KILL);
        rules.addAll(super.getAbilityModifiers());
        return rules;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Assassin", Assassin.class);
    }
}
