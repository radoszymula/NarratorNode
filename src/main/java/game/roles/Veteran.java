package game.roles;

import java.util.ArrayList;

import game.event.Happening;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.attacks.DirectAttack;
import game.logic.support.rules.SetupModifiers;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Veteran extends Ability {

    public static final int MAIN_ABILITY = Ability.ALERT;

    private boolean alertCausingImmunity = false;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public String getNightText(Narrator n) {
        return "Type " + NQuote(COMMAND) + " to kill all who visit you tonight";
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Kill everyone who visits this person when on alert.";

    public static final String COMMAND = "Alert";
    public static final String COMMAND_LOWERCASE = "alert";

    public static final String DEATH_FEEDBACK = "You were killed by a Veteran!";

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        if(getPerceivedCharges() != 0)
            return null;
        return new PlayerList();
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("go on alert");
        return list;
    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        if(alertCausingImmunity)
            p.setInvulnerable(false);
        alertCausingImmunity = false;
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        if(getPerceivedCharges() == 0)
            Exception("You have no more alerts");
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() != 0)
            Exception("You can't target players with this ability");
    }

    @Override
    public void doNightAction(Action a) {
        Player veteran = a.owner, target = a.getTarget();
        Narrator narrator = veteran.narrator;
        // witched
        if(target != veteran && getPerceivedCharges() == 0){
            Ability.NoNightActionVisit(a);
        }
        if(!veteran.isInvulnerable()){
            veteran.setInvulnerable(true);
            alertCausingImmunity = true;
        }

        PlayerList people = new PlayerList();
        for(Player pQ: narrator.getLivePlayers()){
            if(pQ == veteran)
                continue;
            if(pQ.getActions().visited(veteran)){
                pQ.kill(new DirectAttack(veteran, pQ, Constants.VETERAN_KILL_FLAG));
                // pQ.visit(owner);
                people.add(pQ);
            }
        }
        Object add;
        if(people.isEmpty())
            add = "no one.";
        else
            add = people;

        Message e = new Happening(narrator);
        e.add(veteran, " went on alert, killing ", add);
        a.markCompleted();
    }

    public static boolean isImmuneVet(Player owner) {
        if(!owner.is(Veteran.class))
            return false;
        Player target = owner.getTargets(MAIN_ABILITY).getFirst();
        if(target == null)
            return false;

        Veteran v = (Veteran) owner.getAbility(MAIN_ABILITY);
        if(v.getPerceivedCharges() != 0)
            return true;
        return false;
    }

    @Override
    protected String getChargeText() {
        int charges = getPerceivedCharges();
        if(charges == SetupModifiers.UNLIMITED){
            return ("You can go on alert as many times as you want.");
        }else if(charges == 0){
            return ("You are too tired to go on alert anymore.");
        }else if(charges == 1){
            return ("You can guard your house 1 more time.");
        }else{
            return ("You can guard your house " + charges + " more times.");
        }
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    public static void alert(Player vet) {
        vet.setTarget(MAIN_ABILITY, null, (String) null);
    }

    @Override
    public void onAbilityLoss(Player p) {
        if(alertCausingImmunity){
            p.setInvulnerable(false);
            if(p.isAlive())
                p.getInjuries().clear();
        }
        alertCausingImmunity = false;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean canEditBackToBackModifier() {
        return false;
    }

    @Override
    public Action getExampleAction(Player owner) {
        return new Action(owner, MAIN_ABILITY);
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Veteran", Veteran.class);
        return FactionService.createFactionRole(faction, role);
    }
}
