package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.StringChoice;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Executioner extends Passive {

    public static final String DEATH_FEEDBACK = "You killed yourself because your target died.";

    private PlayerList targets;

    public Executioner() {
    }

    @Override
    public Ability initialize(Player p) {
        targets = new PlayerList();
        return super.initialize(p);
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.EXECUTIONER_WIN_IMMUNE);
        rules.add(SetupModifier.EXECUTIONER_TO_JESTER);
        rules.add(SetupModifier.EXEC_TOWN);
        return rules;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Sole purpose is to get a given target killed. Do it.";

    public PlayerList getTargets() {
        return targets;
    }

    public Player getTarget() {
        return targets.getLast();
    }

    public void addTarget(Player target) {
        this.targets.add(target);
    }

    public void setTarget(Player target) {
        this.targets.set(targets.size() - 1, target);
    }

    private boolean winner = false;

    @Override
    public Boolean checkWinEnemyless(Player p) {
        return winner;
    }

    public void setWon() {
        winner = true;
    }

    @Override
    public void setInitialCharges(int charge, Player p) {
        super.setInitialCharges(charge, null); // null player means no noise adding
    }

    public static Player GetTarget(Player p) {
        if(!p.hasAbility(Executioner.class))
            return null;

        Executioner r_card = p.getAbility(Executioner.class);
        return r_card.getTarget();
    }

    @Override
    public void onGameStart(Player exec) {
        super.onGameStart(exec);
        assign(exec);
    }

    public static void assign(Player exec) {
        Narrator n = exec.narrator;
        PlayerList players = new PlayerList();
        ;
        if(n.getBool(SetupModifier.EXEC_TOWN)){
            int threshold = 0;
            for(Faction t: n.getFactions()){
                if(t._startingSize > threshold){
                    players.clear();
                    for(Player p: t.getMembers()){
                        if(p.getFaction() == t && p.isAlive())
                            players.add(p);
                    }
                    threshold = t._startingSize;
                }else if(t._startingSize == threshold){
                    for(Player p: t.getMembers()){
                        if(p.getFaction() == t && p.isAlive())
                            players.add(p);
                    }
                }
            }
        }
        if(players.isEmpty())
            players = n.getLivePlayers();

        players.remove(exec).sortByID();
        Player target = n.getRandom().getPlayer(players);

        Executioner role = exec.getAbility(Executioner.class);
        role.useCharge();
        role.addTarget(target);
        Feedback e = new Feedback(exec, Feedback.PREGAME_FEEDBACK);

        StringChoice sc = new StringChoice(exec);
        sc.add(exec, "Your");
        e.add(sc);

        sc = new StringChoice("'s");
        sc.add(exec, "");

        e.add(sc, " target is ", target, ".");
        e.setVisibility(exec);
        e.setPrivate();
    }

    @Override
    public boolean isPowerRole() {
        return false;
    }

    public static final String UNKNOWN_TARGET = "You no longer know who your target is.";

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player exec) {
        ArrayList<String> roleInfo = new ArrayList<String>();
        Faction t = exec.getFaction();
        if(winner && t.getEnemyColors().isEmpty()){
            roleInfo.add("You've already won!");
            return roleInfo;
        }
        Player target = getTarget();

        if(target.getName().equals(target.getID()))
            roleInfo.add("You must try to " + target.getName() + " publically executed to win.");
        else
            roleInfo.add(UNKNOWN_TARGET);

        if(exec.narrator.getBool(SetupModifier.EXEC_TOWN)){
            t = exec.narrator.getFaction(getTarget().getInitialColor());
            roleInfo.add("Your target will always be part of the " + t.getName());
        }else
            roleInfo.add("Your target may be of any alignment!");
        return roleInfo;
    }

    @Override
    public boolean canStartWithEnemies() {
        return false;
    }

    @Override
    public boolean chargeModifiable() {
        return true;
    }

    @Override
    public boolean isActivePassive(Player p) {
        for(Faction t: p.getFactions()){
            if(t.hasEnemies())
                return false;
        }
        return true;
    }

    @Override
    public String getRuleChargeText(Narrator n) {
        int val = this.modifiers.getInt(AbilityModifier.CHARGES);
        if(val == 1)
            return "Target will not change if original target dies.";
        return "Gets " + (val - 1) + " chances to publicly execute target before loss.";
    }

    @Override
    public void checkIsNightless(Narrator n) {
    }

    public static void SetTarget(Player exec, Player p) {
        exec.getAbility(Executioner.class).setTarget(p);
    }

    @Override
    public boolean hasChargeAbility() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role exec = RoleService.createRole(faction.narrator, "Executioner", Executioner.class);
        exec.addChargeModifier(1);
        return FactionService.createFactionRole(faction, exec);
    }
}
