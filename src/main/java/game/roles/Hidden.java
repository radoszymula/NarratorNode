package game.roles;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import game.ai.RoleGroup;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.RolesList;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.support.Alignment;
import game.logic.support.Constants;
import game.logic.support.RolePackage;
import game.logic.support.RoleTemplate;
import game.logic.support.Util;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.Modifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import models.FactionRole;
import util.ModifierUtil;

public class Hidden extends RoleTemplate implements Alignment {

    private String name;
    public Narrator narrator;

    public Hidden(String name, Narrator narrator) {
        this.name = name;
        this.narrator = narrator;
    }

    @Override
    public String getName() {
        return name;
    }

    public boolean hasColor(String color) {
        for(FactionRole m: factionRolesMap.values()){
            if(m.getColor().equals(color))
                return true;
        }
        return false;
    }

    // this needs to be a linked hash map, or else the order from values() is not
    // consistent
    public HashMap<Long, FactionRole> factionRolesMap = new LinkedHashMap<>(); // rename to roles

    public boolean contains(FactionRole factionRole) {
        return factionRolesMap.containsValue(factionRole);
    }

    public boolean contains(Role role) {
        for(FactionRole factionRole: factionRolesMap.values()){
            if(factionRole.role == role)
                return true;
        }
        return false;
    }

    public RoleGroup combine(Hidden r2) {
        RoleGroup roleGroup = new RoleGroup();
        roleGroup.add(this, r2);
        return roleGroup;
    }

    public void removeRole(FactionRole m) {
        FactionRole factionRole;
        for(long factionRoleID: new HashSet<>(factionRolesMap.keySet())){
            factionRole = factionRolesMap.get(factionRoleID);
            if(factionRole == m)
                factionRolesMap.remove(factionRoleID);
        }
    }

    public void removeFactionRole(FactionRole m) {
        factionRolesMap.remove(m.id);
    }

    public boolean spawns(Role name) {
        FactionRole factionRole;
        for(long factionRoleID: factionRolesMap.keySet()){
            factionRole = factionRolesMap.get(factionRoleID);
            if(factionRole.role == name)
                return true;
        }
        return false;
    }

    public boolean hasMultiAlignment() {
        return getAlignmentCount() > 1;
    }

    // rename to size()
    public int getSize() {
        return this.getFactionRoles().size();
    }

    public boolean isHiddenSingle() {
        return factionRolesMap.size() == 1;
    }

    public String getAlignment() {
        if(hasMultiAlignment())
            throw new Error("Has multiple alignments");
        return factionRolesMap.values().iterator().next().getColor();
    }

    public RolePackage generateRole(Narrator narrator) {
        return getRole(narrator, new ArrayList<>());
    }

    private RolePackage getSpawn(Narrator narrator) {
        RolesList rolesList = narrator.rolesList;
        if(!rolesList.spawns.containsKey(this))
            return null;
        ArrayList<FactionRole> spawns = rolesList.spawns.get(this), usedSpawns;
        if(!rolesList.usedSpawns.containsKey(this)){
            usedSpawns = new ArrayList<>();
            rolesList.usedSpawns.put(this, usedSpawns);
        }else{
            usedSpawns = rolesList.usedSpawns.get(this);
        }

        if(usedSpawns.size() == spawns.size())
            return null;

        FactionRole spawnedRole = spawns.get(usedSpawns.size());
        usedSpawns.add(spawnedRole);

        return new RolePackage(this, spawnedRole);
    }

    public RolePackage getRole(Narrator n, List<FactionRole> excludedRoles) {
        if(getSize() == 0)
            throw new IllegalRoleCombinationException("No roles in this random slot");
        RolePackage spawn = getSpawn(n);
        if(spawn != null){
            n.generatedRoles.add(spawn);
            return spawn;
        }

        List<FactionRole> rList = new ArrayList<>();
        FactionRole cit = null;

        for(FactionRole factionRole: getFactionRoles()){
            if(factionRole.isUnique() && hasRole(n, factionRole))
                continue;
            if(excludedRoles.contains(factionRole))
                continue;
            if(factionRole.role.is(Citizen.class))
                cit = factionRole;
            else
                rList.add(factionRole);
        }

        if(cit != null){
            int size = rList.size() - 1;
            double ratio = n.getInt(SetupModifier.CIT_RATIO) / 100.;
            double factor = size / (1 - ratio) - size;

            for(int i = 0; i < (int) factor; i++)
                rList.add(cit);

            if(rList.size() == 0) // really just accounts for when its a hidden single citizen
                rList.add(cit);
        }

        rList.sort(new Comparator<FactionRole>() {

            @Override
            public int compare(FactionRole o1, FactionRole o2) {
                if(o1.faction == o2.faction)
                    return o1.getName().compareTo(o2.getName());
                return o1.faction.getName().compareTo(o2.faction.getName());
            }

        });

        int choice = n.getRandom().nextInt(rList.size());
        FactionRole member = rList.get(choice);

        RolePackage role = new RolePackage(this, member);
        n.generatedRoles.add(role);
        return role;
    }

    private static boolean hasRole(Narrator narrator, FactionRole factionRole) {
        for(RolePackage role: narrator.generatedRoles){
            if(role.assignedRole == factionRole)
                return true;
        }
        return false;
    }

    @Override
    public boolean opposes(Alignment a2, Narrator n) {
        for(FactionRole m: factionRolesMap.values()){
            Faction myTeam = n.getFaction(m.getColor());

            String[] enemyNumber = a2.getFactionColors();

            for(String theirTeam: enemyNumber)
                if(!myTeam.isEnemy(theirTeam))
                    return false;

        }

        return true;

    }

    @Override
    public String[] getFactionColors() {
        LinkedHashSet<String> teams = new LinkedHashSet<>();
        for(FactionRole m: factionRolesMap.values())
            teams.add(m.getColor());

        String[] returnVal = new String[teams.size()];
        Object[] array = teams.toArray();
        for(int i = 0; i < teams.size(); i++)
            returnVal[i] = (String) array[i];

        return returnVal;
    }

    public static Hidden TownRandom() {
        return BasicRoles.setup.getRandom(Constants.TOWN_RANDOM_ROLE_NAME);
    }

    public static Hidden TownInvestigative() {
        return BasicRoles.setup.getRandom(Constants.TOWN_INVESTIGATIVE_ROLE_NAME);
    }

    public static Hidden TownProtective() {
        return BasicRoles.setup.getRandom(Constants.TOWN_PROTECTIVE_ROLE_NAME);
    }

    public static Hidden TownKilling() {
        return BasicRoles.setup.getRandom(Constants.TOWN_KILLING_ROLE_NAME);
    }

    public static Hidden TownGovernment() {
        return BasicRoles.setup.getRandom(Constants.TOWN_GOVERNMENT_ROLE_NAME);
    }

    public static Hidden MafiaRandom() {
        return BasicRoles.setup.getRandom(Constants.MAFIA_RANDOM_ROLE_NAME);
    }

    public static Hidden YakuzaRandom() {
        return BasicRoles.setup.getRandom(Constants.YAKUZA_RANDOM_ROLE_NAME);
    }

    public static Hidden NeutralRandom() {
        return BasicRoles.setup.getRandom(Constants.NEUTRAL_RANDOM_ROLE_NAME);
    }

    public static Hidden NeutralEvilRandom() {
        return BasicRoles.setup.getRandom(Constants.NEUTRAL_EVIL_RANDOM_ROLE_NAME);
    }

    public static Hidden NeutralKillingRandom() {
        return BasicRoles.setup.getRandom(Constants.NEUTRAL_KILLING_RANDOM_ROLE_NAME);
    }

    public static Hidden NeutralBenignRandom() {
        return BasicRoles.setup.getRandom(Constants.NEUTRAL_BENIGN_RANDOM_ROLE_NAME);
    }

    public static String list(String... ss) {
        StringBuilder sb = new StringBuilder();
        sb.append("Spawns:\n");

        for(String s: ss){
            sb.append(s);
            sb.append("\n");
        }

        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public static Hidden AnyRandom() {
        return BasicRoles.setup.getRandom(Constants.ANY_RANDOM_ROLE_NAME);
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;

        if(o.getClass() != getClass())
            return false;

        Hidden r = (Hidden) o;
        if(Util.notEqual(name, r.name))
            return false;
        if(Util.notEqual(factionRolesMap, r.factionRolesMap))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (name.toString() + factionRolesMap.hashCode()).hashCode();
//		if(modifierMap == null)
//		else
//			return (name.toString() + list.hashCode() + modifierMap.toString()).hashCode();
    }

    @Override
    public String toString() {
        if(factionRolesMap.values().size() == 1)
            return factionRolesMap.values().iterator().next().toString();
        return name;
    }

    public int getAlignmentCount() {
        HashSet<String> colors = new HashSet<>();
        for(FactionRole m: this.factionRolesMap.values()){
            colors.add(m.getColor());
        }
        return colors.size();
    }

    public Set<String> getColors() {
        Set<String> colors = new HashSet<String>();
        for(FactionRole m: factionRolesMap.values())
            colors.add(m.getColor());
        return colors;
    }

    @Override
    public boolean contains(String roleName) {
        for(FactionRole m: factionRolesMap.values()){
            if(m.role.contains(roleName))
                return true;
        }
        return false;
    }

    @Override
    public void prefer(Player p) {
        for(FactionRole m: factionRolesMap.values()){
            m.role.prefer(p);
        }
    }

    public void removeChatRoles() {
        FactionRole factionRole;
        List<Long> toRemove = new ArrayList<>();
        for(Long factionRoleID: factionRolesMap.keySet()){
            factionRole = factionRolesMap.get(factionRoleID);
            if(factionRole.role.isChatRole())
                toRemove.add(factionRoleID);
        }
        for(Long purge: toRemove){
            factionRolesMap.remove(purge);
        }
    }

    public int getUniqueCount() {
        int uniqueCount = 0;
        FactionRole factionRoleCopy;
        for(FactionRole factionRole: factionRolesMap.values()){
            factionRoleCopy = factionRole.copy();
            ModifierUtil.resolveOverrides(factionRoleCopy);
            if(factionRoleCopy.role.isUnique())
                uniqueCount++;
        }
        return uniqueCount;
    }

    @Override
    public boolean isBoolModifier(Modifier modifier) {
        return Ability.IsBoolModifier(modifier);
    }

    @Override
    public boolean isIntModifier(Modifier modifier) {
        return Ability.IsIntModifier(modifier);
    }

    @Override
    public boolean contains(Class<? extends Ability> c) {
        for(FactionRole factionRole: this.factionRolesMap.values()){
            if(factionRole.role.contains(c))
                return true;
        }
        return false;
    }

    public Hidden addAbility(Ability a) {
        for(FactionRole factionRole: this.factionRolesMap.values()){
            factionRole.role.addAbility(a);
        }
        return this;
    }

    @Override
    public Hidden addModifier(RoleModifier modifier, boolean val) {
        for(FactionRole factionRole: this.factionRolesMap.values())
            factionRole.role.addModifier(modifier, val);
        return this;
    }

    @Override
    public RoleTemplate addModifier(RoleModifier modifier, int val) {
        for(FactionRole factionRole: this.factionRolesMap.values())
            factionRole.role.addModifier(modifier, val);
        return this;
    }

    public RoleTemplate addModifier(AbilityModifier modifier, Class<? extends Ability> abilityClass, boolean val) {
        for(FactionRole factionRole: this.factionRolesMap.values())
            factionRole.role.addModifier(modifier, abilityClass, val);

        return this;
    }

    public RoleTemplate addModifier(AbilityModifier modifier, Class<? extends Ability> abilityClass, int val) {
        for(FactionRole factionRole: this.factionRolesMap.values())
            factionRole.role.addModifier(modifier, abilityClass, val);

        return this;
    }

    @Override
    public Hidden addAbility(Class<? extends Ability> a) {
        for(FactionRole factionRole: this.factionRolesMap.values())
            factionRole.role.addAbility(a);

        return this;
    }

    @Override
    public void removeAbility(Class<? extends Ability> a) {
        for(FactionRole factionRole: this.factionRolesMap.values())
            factionRole.role.removeAbility(a);

    }

    public List<FactionRole> getFactionRoles() {
        List<FactionRole> roles = new ArrayList<>();
        for(FactionRole role: factionRolesMap.values())
            roles.add(role);
        return roles;
    }

    public FactionRole getRoleBy(long factionRoleID) {
        return factionRolesMap.get(factionRoleID);
    }

    @Override
    public boolean isRandom() {
        return true;
    }

}
