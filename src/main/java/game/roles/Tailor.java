package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.event.Happening;
import game.logic.Faction;
import game.logic.MemberList;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.HTString;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.roles.support.Suit;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Tailor extends Ability {

    public static final int MAIN_ABILITY = Ability.SUIT;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String COMMAND = "Suit";

    public static final String NIGHT_ACTION_DESCRIPTION = "Give suits to people.  Upon death, they'll look like a different role in the graveyard.";

    public static final String FEEDBACK = "You have been given a suit!";

    @Override
    public ArrayList<Option> getOptions(Player p) {
        ArrayList<Option> teams = new ArrayList<>();

        for(Faction t: p.narrator.getFactions()){
            teams.add(new Option(t));
        }

        return teams;
    }

    @Override
    public ArrayList<Option> getOptions2(Player p, String team) {
        Narrator n = p.narrator;
        MemberList ml = n.getPossibleMembers();
        String color;
        if(n.getFaction(team) != null)
            color = team;
        else
            color = n.getFactionByName(team).getColor();

        ArrayList<Option> roleNames = new ArrayList<>();
        for(FactionRole m: ml.getFactionRoles(color))
            roleNames.add(new Option(m.role.getName()).setColor(team));
        return roleNames;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
        a.setOption(a.getOption());
        a.setOption2(a.getOption2());

        if(getPerceivedCharges() == 0)
            Exception("You cannot give out suits anymore");
    }

    @Override
    public void doNightAction(Action a) {
        if(a.getTarget() == null)
            return;
        Faction t = a.owner.narrator.getFaction(a.getOption());

        Player target = a.getTarget();
        if(t == null || a.getOption() == null || a.getOption2() == null){
            NoNightActionVisit(a);
            return;
        }

        MemberList mList = target.narrator.getPossibleMembers();
        AbilityList baseName = mList.translate(a.getOption2());

        ArrayList<Faction> possibleFactions = mList.getFactions(baseName, target.narrator);
        boolean foundTeam = false;
        for(Faction pTeam: possibleFactions){
            if(pTeam.getColor().equals(a.getOption())){
                foundTeam = true;
                break;
            }
        }

        if(!foundTeam){
            Ability.NoNightActionVisit(a);
            return;
        }

        target.addSuite(new Suit(a));
        if(target.narrator.getBool(SetupModifier.TAILOR_FEEDBACK)){
            FeedbackGenerator(new Feedback(target));
        }

        Happening h = new Happening(a.owner.narrator);
        h.add(a.owner);
        h.add(" gave a ");
        h.add(new HTString(t + " " + a.getOption2(), a.getOption()));
        h.add(" suit to ");
        h.add(target);
        h.add(".");

        a.markCompleted();
        a.owner.visit(target);
    }

    @Override
    public void onDeath(Player tailor) {
        for(Player p: tailor.narrator.getLivePlayers()){
            p.removeSuits(tailor);
        }
    }

    @Override
    protected String getChargeText() {

        int charges = getPerceivedCharges();
        if(charges == SetupModifiers.UNLIMITED){
            return ("You can give out as many suits as you want.");
        }else if(charges == 0){
            return ("You can't give out any suits.");
        }else if(charges == 1){
            return ("You can give out " + charges + " more suit.");
        }else{
            return ("You can give out " + charges + " more suits.");
        }
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        if(a.ability != MAIN_ABILITY)
            return super.getCommandParts(a);
        ArrayList<String> parsedCommands = new ArrayList<>();
        PlayerList targets = a.getTargets().copy();

        parsedCommands.add(COMMAND);
        for(Player target: targets){
            parsedCommands.add(target.getName());
        }
        parsedCommands.add(a.getOption());
        parsedCommands.add(a.getOption2());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, ArrayList<String> commands) {
        commands.remove(0);

        if(commands.isEmpty())
            throw new PlayerTargetingException("Need to target someone to give a suit");

        Narrator n = p.narrator;
        Player target = n.getPlayerByName(commands.remove(0));

        if(commands.isEmpty())
            throw new PlayerTargetingException("Need to selection what team this suit is from.");

        Faction t = Faction.GetFaction(commands, n);
        // if successful, commands will be edited. if not, commands will be unchanged.

        if(t == null)
            throw new PlayerTargetingException("Need to select what team this suit is from.");

        String roleName = Ability.GetRoleName(commands, n);

        if(commands.isEmpty() && roleName == null)
            throw new PlayerTargetingException("Need to select what role this suit is");

        return new Action(p, Player.list(target), MAIN_ABILITY, t.getColor(), roleName);
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> phrase = new ArrayList<>();
        for(Action da: actionList){
            phrase.add(COMMAND.toLowerCase());
            phrase.add(" ");
            phrase.add(StringChoice.YouYourselfSingle(da.owner, da.getTarget()));

            phrase.add(" as ");
            phrase.add(new HTString(da.owner.narrator.getFaction(da.getOption()).getName() + " " + da.getOption2(),
                    da.getOption()));

            phrase.add(" and ");
        }
        phrase.remove(phrase.size() - 1);

        return phrase;
    }

    @Override
    public String getUsage(Player p, Random r) {
        ArrayList<Option> options = getOptions(p);
        int index = r.nextInt(options.size());
        ArrayList<Option> option2s = getOptions2(p, options.get(index).getValue());
        int index2 = r.nextInt(option2s.size());

        return super.getUsage(p, r) + " " + options.get(index) + " " + option2s.get(index2)
                + "\nAny team/role name combinations are valid";
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.TAILOR_FEEDBACK))
            ret.add("Your targets will know they received your suit.");
        else
            ret.add("Your targets will not know they received your suit.");

        if(p.narrator.getBool(SetupModifier.SNITCH_PIERCES_SUIT))
            ret.add("Players that get revealed won't show your suit.");
        else
            ret.add("Players that get revealed will show your suit instead.");

        return ret;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.TAILOR_FEEDBACK);
        rules.add(SetupModifier.SNITCH_PIERCES_SUIT);
        return rules;
    }

    public static void FeedbackGenerator(Feedback fb) {
        Player target = fb.player;
        fb.add(Tailor.FEEDBACK);
        fb.setPicture("mayor");
        fb.addExtraInfo("When you die, you'll probably look like something other than a " + target.getRoleName() + ".");
    }

    @Override
    public Action getExampleAction(Player owner) {
        return new Action(owner, MAIN_ABILITY, owner.getColor(), owner.getRoleName(), owner);
    }

    @Override
    public Action getExampleWitchAction(Player owner, Player target) {
        return new Action(owner, Player.list(target), Tailor.MAIN_ABILITY, owner.getColor(), owner.getRoleName());
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Tailor", Tailor.class);
    }
}
