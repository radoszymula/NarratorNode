package game.roles;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Clubber extends Ability {

    public static final String NIGHT_ACTION_DESCRIPTION = "An acolyte of the masonry, kill all cultic heretics.";
    public static final String ROLE_NAME = "Mason Clubber";
    public static final String COMMAND = "Purge";

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final int MAIN_ABILITY = Ability.PURGE;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        MasonLeader.MasonryCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        Player ml = a.owner;
        if(target == null)
            return;

        if(target.isCulted()){
            a.markCompleted();
            Kill(ml, target, MasonLeader.BLUDGEON);
        }else{
            NoNightActionVisit(a);
        }
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Clubber", Clubber.class);
        return FactionService.createFactionRole(faction, role);
    }
}
