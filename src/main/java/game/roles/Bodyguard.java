package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.logic.support.saves.BGHeal;
import models.FactionRole;
import models.enums.GamePhase;
import services.FactionService;
import services.RoleService;

public class Bodyguard extends Ability {

    private boolean isProtecting = false;

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final int MAIN_ABILITY = Ability.GUARD;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.GUARD_REDIRECTS_CONVERT);
        rules.add(SetupModifier.GUARD_REDIRECTS_POISON);
        rules.add(SetupModifier.GUARD_REDIRECTS_DOUSE);
        return rules;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Guard someone from death, dying and killing the attacker.";

    public static final String COMMAND = "Guard";
    public static final String TARGET_FEEDBACK = "You were saved by a bodyguard!";
    public static final String DEATH_TARGET_FEEDBACK = "You were killed by a bodyguard that was protecting your target";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public ArrayList<String> getPublicDescription(Narrator narrator, String actionOriginatorName, String color, AbilityList abilities) {
        ArrayList<String> ruleTextList = super.getPublicDescription(narrator, actionOriginatorName, color, abilities);

        ruleTextList.add("Target will know that they were protected, if they are attacked");

        return ruleTextList;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        if(owner.narrator.pre_bodyguard){
            owner.visit(target);
            return;
        }
        if(!getProtected()){
            happening(owner, " protected ", target);
            setProtected(false);
        }

        target.heal(new BGHeal(owner, Constants.BODYGUARD_KILL_FLAG));
        a.markCompleted();
    }

    private boolean getProtected() {
        return isProtecting;
    }

    private void setProtected(boolean b) {
        this.isProtecting = b;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        ret.add("Your target will know that they were protected if someone attacks them.");

        return ret;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    public static Player redirect(Player target) {
        PlayerList visitors = target.getVisitors("bg -> douse").filterIsTargeting(target, Bodyguard.MAIN_ABILITY);
        if(target.narrator.nightPhase == GamePhase.POST_KILLING){
            for(int i = 0; i < visitors.size(); i++){
                if(visitors.get(i).getLives() < 0){
                    visitors.remove(i);
                    i--;
                }
            }
        }
        if(!visitors.isEmpty())
            target = visitors.getFirst();

        return target;
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Bodyguard", Bodyguard.class);
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }
}
