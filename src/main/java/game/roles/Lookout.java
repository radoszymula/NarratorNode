package game.roles;

import java.util.ArrayList;

import game.event.Feedback;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Lookout extends Ability {

    public static final String COMMAND = "Watch";

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final int MAIN_ABILITY = Ability.WATCH;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Find out all who visit someone.";

    // private boolean visited = false;
    public static final String FEEDBACK = "These are the people who visited your target: ";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        if(owner.narrator.pre_visiting_phase){
            owner.visit(target);
            return;
        }
        a.markCompleted();
        watch(owner, target, a.getIntendedTarget());
        happening(owner, " watched ", target);
    }

    public static final String NO_VISIT = "No one visited your target";

    public static void watch(Player owner, Player target, String ogTarget) {
        PlayerList visitors = target.getVisitors("Lookout");// this is copied.
        for(int i = 0; i < visitors.size(); i++){
            if(!visitors.get(i).isDetectable()){
                visitors.remove(i);
                i--;
            }
        }
        visitors.remove(owner);
        visitors.sortByName();
        ArrayList<Object> parts = FeedbackGenerator(visitors);
        Message m = new Feedback(owner).add(parts).setPicture("lookout");

        m.addExtraInfo("As a reminder, you attempted to follow " + ogTarget + ".");
    }

    public static ArrayList<Object> FeedbackGenerator(PlayerList visitors) {
        ArrayList<Object> parts = new ArrayList<>();
        if(visitors.isEmpty())
            parts.add(NO_VISIT);
        else{
            parts.add(FEEDBACK);
            for(Player p: visitors){
                parts.add(p);
                if(visitors.getLast() != p)
                    parts.add(", ");
            }
            parts.add(".");
        }
        return parts;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Lookout", Lookout.class);
        return FactionService.createFactionRole(faction, role);
    }

}
