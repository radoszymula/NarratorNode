package game.roles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.event.Happening;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.PhaseException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownRoleException;
import game.logic.exceptions.UnknownTeamException;
import game.logic.exceptions.UnsupportedMethodException;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.logic.support.attacks.DirectAttack;
import game.logic.support.attacks.IndirectAttack;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.Modifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.roles.support.Bread;
import game.roles.support.Charges;
import game.roles.support.CultInvitation;
import game.roles.support.Gun;
import game.roles.support.Vest;
import models.Modifiers;
import util.LookupUtil;

public abstract class Ability implements Comparable<Ability> {

    /*
     * HOW TO ADD ANOTHER ABILITY
     * 
     * add to the great switch case ParseAbility
     * 
     * add command to narrator's command reserved names. Located in this file
     * 
     * add to GetAbilityCopy, also in this file
     * 
     * add to faction ?
     */
    public static final int BURN = 66;
    public static final int SPREE = 65;
    public static final int NIGHT_KILL = 64;
    public static final int STAB = 63;
    public static final int SHOOT = 62;
    public static final int INTERCEPT = 61;
    public static final int DISGUISE = 60;
    public static final int POISON = 59;
    public static final int JAIL = 58;
    public static final int CHARGE = 57;
    public static final int BOUNTY = 56;// Joker
    public static final int PURGE = 55;
    public static final int MURDER = 54; // proxy killing

    public static final int VEST_ABILITY = 53;

    public static final int CONVERT = 52;
    public static final int RECRUIT = 51;
    public static final int ENFORCE = 50;

    public static final int DOUSE = 49;

    public static final int GUN = 48;
    public static final int CRAFT = 47;
    public static final int ARMOR = 46;
    public static final int BREAD_ABILITY = 45;
    public static final int GUARD = 44;
    public static final int HEAL = 43;
    public static final int VOTE_STEAL = 42;

    public static final int ANNOY = 41;
    public static final int POSSESS = 40;
    public static final int DAY_POSSESS = 39;
    public static final int BLOCK = 38;
    public static final int DISFRANCHISE = 37;
    public static final int BLACKMAIL = 36;
    public static final int SUIT = 35;
    public static final int DRUG = 34;
    public static final int FRAME = 33;
    public static final int SILENCE = 32;

    public static final int CLEAN = 31;
    public static final int COMMUTE = 30;

    public static final int INVESTIGATE = 29;
    public static final int STALK = 28;
    public static final int FOLLOW = 27;
    public static final int WATCH = 26;
    public static final int CHECK = 25;
    public static final int COMPARE = 24;
    public static final int ARMS_DETECT = 23;
    public static final int SCOUT = 22;
    public static final int AUTOPSY = 21;
    public static final int SPY = 20;

    public static final int VISIT_ABILITY = 19;

    public static final int CONTROL = 18;
    public static final int SWITCH = 17; // OPERATOR
    public static final int SWAP = 16; // DRIVER
    public static final int SNITCH = 15;
    public static final int DIG = 14; // GRAVE DIGGER
    public static final int HIDE = 13;
    public static final int REMEMBER = 12; // AMNESIAC
    public static final int UNDOUSE = 11;
    public static final int ALERT = 10;

    public static final int ORDER = 9;// MARSHALL
    public static final int PUPPET_PUNCH = 8;
    public static final int PUNCH = 7;
    public static final int PUPPET = 6;
    public static final int ASSASSINATE = 5;
    public static final int BUILD = 4;
    public static final int REVEAL = 3;

    public static final int INVITATION_ABILITY = 2;
    public static final int NIGHT_SEND = 1;
    public static final int INVALID_ABILITY = -1;
    public static final int VOTE_ABILITY = -2;
    public static final int THIEF_PICK = -3;
    public static final boolean PIERCING = true;

    public static final String VISIT = "Visit";
    public static final Modifier[] MODIFIERS = new Modifier[] { // TODO DO I NEED THIS?
            AbilityModifier.SELF_TARGET, AbilityModifier.ZERO_WEIGHTED, RoleModifier.UNDETECTABLE,
            RoleModifier.UNCONVERTABLE, RoleModifier.UNBLOCKABLE, RoleModifier.HIDDEN_FLIP, AbilityModifier.CHARGES,
            AbilityModifier.COOLDOWN, RoleModifier.AUTO_VEST, RoleModifier.UNIQUE, AbilityModifier.HIDDEN
    };
    public static final Modifier[] BOOL_MODIFIERS = new Modifier[] {
            AbilityModifier.BACK_TO_BACK, AbilityModifier.DISFRANCHISED_ANNOUNCED, AbilityModifier.HIDDEN,
            AbilityModifier.SELF_TARGET, AbilityModifier.SILENCE_ANNOUNCED, AbilityModifier.SECRET_KILL,
            AbilityModifier.ZERO_WEIGHTED, AbilityModifier.AS_FAKE_VESTS, AbilityModifier.GS_FAULTY_GUNS,

            FactionModifier.HAS_NIGHT_CHAT, FactionModifier.IS_RECRUITABLE, FactionModifier.KNOWS_ALLIES,
            FactionModifier.LAST_WILL, FactionModifier.LIVE_TO_WIN,

            RoleModifier.UNDETECTABLE, RoleModifier.UNCONVERTABLE, RoleModifier.UNBLOCKABLE, RoleModifier.UNIQUE,
            RoleModifier.HIDDEN_FLIP,

            SetupModifier.HEAL_FEEDBACK, SetupModifier.HEAL_SUCCESS_FEEDBACK, SetupModifier.HEAL_BLOCKS_POISON,
            SetupModifier.GUARD_REDIRECTS_CONVERT, SetupModifier.GUARD_REDIRECTS_DOUSE,
            SetupModifier.GUARD_REDIRECTS_POISON, SetupModifier.MILLER_SUITED, SetupModifier.GS_DAY_GUNS,
            SetupModifier.SPY_TARGETS_ALLIES, SetupModifier.SPY_TARGETS_ENEMIES, SetupModifier.ARCH_BURN,
            SetupModifier.ARCH_QUARANTINE, SetupModifier.CHECK_DIFFERENTIATION, SetupModifier.SHERIFF_PREPEEK,
            SetupModifier.FOLLOW_GETS_ALL, SetupModifier.FOLLOW_PIERCES_IMMUNITY, SetupModifier.CORONER_EXHUMES,
            SetupModifier.CORONER_LEARNS_ROLES, SetupModifier.BOMB_PIERCE, SetupModifier.MARSHALL_QUICK_REVEAL,
            SetupModifier.BREAD_PASSING, SetupModifier.SELF_BREAD_USAGE, SetupModifier.MASON_PROMOTION,
            SetupModifier.MASON_NON_CIT_RECRUIT, SetupModifier.ENFORCER_LEARNS_NAMES, SetupModifier.BLOCK_FEEDBACK,
            SetupModifier.AMNESIAC_KEEPS_CHARGES, SetupModifier.AMNESIAC_MUST_REMEMBER, SetupModifier.JESTER_CAN_ANNOY,
            SetupModifier.EXECUTIONER_WIN_IMMUNE, SetupModifier.EXEC_TOWN, SetupModifier.EXECUTIONER_TO_JESTER,
            SetupModifier.WITCH_FEEDBACK, SetupModifier.DOUSE_FEEDBACK, SetupModifier.CULT_PROMOTION,
            SetupModifier.CULT_KEEPS_ROLES, SetupModifier.CONVERT_REFUSABLE, SetupModifier.DIFFERENTIATED_FACTION_KILLS,
            SetupModifier.JANITOR_GETS_ROLES, SetupModifier.TAILOR_FEEDBACK, SetupModifier.SNITCH_PIERCES_SUIT,
            SetupModifier.GD_REANIMATE, SetupModifier.PROXY_UPGRADE
    };
    public static final Modifier[] INT_MODIFIERS = new Modifier[] {
            AbilityModifier.CHARGES, AbilityModifier.COOLDOWN, AbilityModifier.JAILOR_CLEAN_ROLES,

            FactionModifier.PUNCH_SUCCESS, FactionModifier.WIN_PRIORITY,

            RoleModifier.AUTO_VEST,

            SetupModifier.BOUNTY_FAIL_REWARD, SetupModifier.BOUNTY_INCUBATION, SetupModifier.CULT_POWER_ROLE_CD,
            SetupModifier.MM_SPREE_DELAY, SetupModifier.ARSON_DAY_IGNITES, SetupModifier.JESTER_KILLS,
            SetupModifier.MARSHALL_EXECUTIONS, SetupModifier.MAYOR_VOTE_POWER, SetupModifier.CIT_RATIO
    };

    public static String GetModifierName(Modifier modifier) {
        if(modifier == AbilityModifier.ZERO_WEIGHTED)
            return ABILITY_WEIGHT_DESCRIPTION;
        if(modifier == AbilityModifier.SELF_TARGET)
            return NO_SELF_TARGET_DESCRIPTION;
        if(modifier == RoleModifier.UNDETECTABLE)
            return UNDETECTABLE_DESCRIPTION;
        if(modifier == RoleModifier.UNCONVERTABLE)
            return UNCONVERTABLE_DESCRIPTION;
        if(modifier == RoleModifier.UNBLOCKABLE)
            return UNBLOCKABLE_DESCRIPTION;
        if(modifier == RoleModifier.UNIQUE)
            return UNIQUE_DESCRIPTION;
        if(modifier == AbilityModifier.CHARGES)
            return CHARGE_DESCRIPTION;
        if(modifier == AbilityModifier.COOLDOWN)
            return COOLDOWN_DESCRIPTION;
        if(modifier == RoleModifier.AUTO_VEST)
            return AUTO_VEST_COUNT_DESCRIPTION;
        if(modifier == AbilityModifier.HIDDEN)
            return HIDDEN_ATTRIBUTE_DESCRIPTION;
        if(modifier == RoleModifier.HIDDEN_FLIP)
            return HIDDEN_FLIP_DESCRIPTION;

        return null;
    }

    public static boolean IsBoolModifier(Modifier modifier) {
        return Arrays.asList(BOOL_MODIFIERS).contains(modifier);
    }

    public static boolean IsIntModifier(Modifier modifier) {
        return Arrays.asList(INT_MODIFIERS).contains(modifier);
    }

    public static final String NO_SELF_TARGET_DESCRIPTION = "Can self target";
    public static final String UNDETECTABLE_DESCRIPTION = "Undetectable";
    public static final String UNCONVERTABLE_DESCRIPTION = "Unconvertable";
    public static final String UNBLOCKABLE_DESCRIPTION = "Unblockable";
    public static final String UNIQUE_DESCRIPTION = "Is unique";
    public static final String HIDDEN_FLIP_DESCRIPTION = "Hidden flip";

    public static final String ABILITY_WEIGHT_DESCRIPTION = "Can freely submit: ";
    public static final String CHARGE_DESCRIPTION = "Charge count";
    public static final String AUTO_VEST_COUNT_DESCRIPTION = "Auto vest count";
    public static final String HIDDEN_ATTRIBUTE_DESCRIPTION = "Unbeknownst passive : ";
    public static final String COOLDOWN_DESCRIPTION = "Usage cooldown";
    public static final String[] COMMAND_RESERVED_NAMES = {
            Agent.COMMAND, Amnesiac.COMMAND, Architect.COMMAND, Armorsmith.COMMAND, ArmsDetector.COMMAND,
            Assassin.COMMAND, Baker.COMMAND, Blackmailer.COMMAND, Blacksmith.COMMAND, Bodyguard.COMMAND, Burn.COMMAND,
            Clubber.COMMAND, Commuter.COMMAND, Coroner.COMMAND, CultLeader.COMMAND, Detective.COMMAND,
            Disfranchise.COMMAND, Disguiser.COMMAND, Doctor.COMMAND, Douse.COMMAND, Driver.COMMAND, DrugDealer.COMMAND,
            Elector.COMMAND, ElectroManiac.COMMAND, Enforcer.COMMAND, FactionKill.COMMAND, FactionSend.COMMAND,
            Framer.COMMAND, Ghost.COMMAND, GraveDigger.COMMAND, Gunsmith.COMMAND, Interceptor.COMMAND,
            Investigator.COMMAND, Interceptor.COMMAND, Investigator.COMMAND, Jailor.COMMAND, Janitor.COMMAND,
            Jester.COMMAND, Joker.COMMAND, Lookout.COMMAND, MasonLeader.COMMAND, MassMurderer.COMMAND, Mayor.COMMAND,
            Marshall.COMMAND, Operator.COMMAND, ParityCheck.COMMAND, Poisoner.COMMAND, PuppetVote.COMMAND,
            Scout.COMMAND, SerialKiller.COMMAND, Silence.COMMAND, Sheriff.COMMAND, Snitch.COMMAND, Spy.COMMAND,
            Stripper.COMMAND, Tailor.COMMAND, Thief.COMMAND, Undouse.COMMAND, Veteran.COMMAND, Visit.COMMAND,
            Witch.COMMAND,

            GunAbility.COMMAND, VestAbility.COMMAND, BreadAbility.COMMAND, Punch.COMMAND, CultInvitation.COMMAND,

            Ventriloquist.VENT_PUNCH, Ventriloquist.VENT_SKIP_VOTE, Ventriloquist.VENT_UNVOTE, Ventriloquist.VENT_VOTE
    };

    protected Narrator narrator;
    protected Player p; // TODO use this more often
    public Modifiers<AbilityModifier> modifiers = new Modifiers<>();
    private int abilityCD = 0;
    public long id;

    public Ability initialize(Faction t) {
        this.narrator = t.narrator;
        this.initialize((Player) null);
        this.initializeModifiers(t);
        return this;
    }

    public Ability initialize(Player p) {
        this.p = p;
        if(p != null){
            this.narrator = p.narrator;
            initializeModifiers(p.getFaction());
        }else{
            initializeModifiers(null);
        }
        return this;
    }

    private void initializeModifiers(Faction faction) {
        if(modifiers == null)
            setInitialCharges(SetupModifiers.UNLIMITED, p);
        else{
            int initialCharges = modifiers.getOrDefault(AbilityModifier.CHARGES, SetupModifiers.UNLIMITED);
            setInitialCharges(initialCharges, p);
        }
    }

    protected int getAbilityCooldown() {
        if(modifiers == null)
            return 0;
        return modifiers.getOrDefault(AbilityModifier.COOLDOWN, 0);
    }

    public abstract String getCommand();

    public abstract int getAbilityNumber();

    /**
     * @param owner the player controller
     */

    public abstract void doNightAction(Action action);

    public abstract String[] getRoleInfo(Narrator narrator);

    public int nightVotePower(Player p, PlayerList pl) {
        return 1;
    }

    public PlayerList getActionTargets(ArrayList<Action> actions) {
        PlayerList actionTargets = new PlayerList();
        for(Action a: actions){
            actionTargets.add(a.getTargets());
        }
        return actionTargets;
    }

    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        PlayerList targets = getActionTargets(actions);

        Player owner = actions.get(0).owner;
        list.add(getCommand().toLowerCase() + " ");
        list.addAll(StringChoice.YouYourself(owner, targets));

        return list;
    }

    public ArrayList<Object> getActionDescription(Action a) {
        ArrayList<Action> actions = new ArrayList<>();
        actions.add(a);
        return getActionDescription(actions);
    }

    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();

        list.add(a.getCommand().toLowerCase() + "ing ");
        list.add(StringChoice.YouYourselfSingle(a.owner, a.getTarget()));

        return list;
    }

    public String getNightText(Player p) {
        if(getCommand() == null){
            return Constants.NO_NIGHT_ACTION;
        }
        return "Type " + NQuote(getCommand().toLowerCase()) + " to " + getCommand().toLowerCase() + " this person.";
    }

    public ArrayList<Option> getOptions(Player p) {
        return new ArrayList<>();
    }

    public ArrayList<Option> getOptions2(Player p, String option1) {
        return new ArrayList<>();
    }

    public ArrayList<Option> getOptions3(Player p, String option1, String option2) {
        return new ArrayList<>();
    }

    /******/

    public boolean isAcceptableTarget(Action a) {
        try{
            checkAcceptableTarget(a);
            return true;
        }catch(PlayerTargetingException | UnknownTeamException | PhaseException e){
            return false;
        }
    }

    public boolean isAcceptableTarget(Player owner) {
        return isAcceptableTarget(new Action(owner, getAbilityNumber()));
    }

    public void checkAcceptableTarget(Action a) {
        Player owner = a.owner;

        if(a.owner.isDead() && a.ability != Ghost.MAIN_ABILITY)
            Exception("Dead people cannot submit this action.");

        if(isOnCooldown() && getRemainingCooldown() == 1)
            Exception("You must wait an additional night to use this ability.");
        else if(isOnCooldown())
            Exception("You must wait " + getRemainingCooldown() + "additional nights to use this ability");

        PlayerList allowedTargets;
        if(isNegativeAbility() && !Faction.CanFriendlyFire(owner.narrator))
            for(Faction t: owner.getFactions()){
                if(!t.knowsTeam())
                    continue;
                allowedTargets = owner.narrator.getLivePlayers().remove(t.getMembers());
                if(allowedTargets.contains(a.getTarget()))
                    continue;
                Faction.CanFriendlyFire(owner.narrator);
                Exception("Can't target teammates with this ability");
            }
        targetSizeCheck(a);
        mainAbilityCheck(a);
        if(Action.isIllegalRetarget(a))
            Exception("Can't target same person twice in a row");
        selfTargetableCheck(a);
        if(owner.narrator.isDay())
            checkPerformDuringDay(owner);
        if(owner.narrator.isNight() && !isNightAbility(a.owner))
            throw new PhaseException("Can't do that during the night.");

    }

    public void selfTargetableCheck(Action a) {
        if(!a.owner.in(a._targets))
            return;
        if(!canSelfTarget(a.owner.narrator))
            Exception("You may not target yourself with this action.");
    }

    public boolean canSelfTarget(Narrator narrator) {
        boolean defaultValue = this.getDefaultSelfTargetValue()
                || (narrator.getPossibleMembers().allowsForFriendlyFire(narrator) && !isPositiveAbility());
        if(modifiers == null)
            return defaultValue;
        return modifiers.getOrDefault(AbilityModifier.SELF_TARGET, defaultValue);
    }

    public String getSelfTargetMemberDescription(boolean singleAbility, boolean canSelfTarget) {
        if(singleAbility && canSelfTarget)
            return "May target themselves.";
        if(!singleAbility && canSelfTarget)
            return "May target themselves with the " + getClass().getSimpleName().toLowerCase() + " ability.";
        if(singleAbility && !canSelfTarget)
            return "May not target themselves.";
        if(!singleAbility && !canSelfTarget)
            return "May not target themselves with the " + getClass().getSimpleName().toLowerCase() + " ability.";
        return "";
    }

    public boolean canEditSelfTargetableModifer() {
        return true;
    }

    public boolean canEditZeroWeightedModifier() {
        return !this.isDayAbility();
    }

    public boolean canEditBackToBackModifier() {
        return true;
    }

    public boolean getDefaultSelfTargetValue() {
        return false;
    }

    public boolean showSelfTargetTextDefault() {
        return false;
    }

    public boolean isNegativeAbility() {
        return false;
    }

    public boolean isPositiveAbility() {
        return !isNegativeAbility();
    }

    // return t.getSelectionFeedback(owner, target, ability);
    public abstract void mainAbilityCheck(Action a);

    // public void validateAction(Action a){}

    public void targetSizeCheck(Action a) {
        int defaultTargetSize = getDefaultTargetSize();
        if(a.getTargets().size() != defaultTargetSize)
            Exception("You must select " + defaultTargetSize + " and only " + defaultTargetSize + " target");
    }

    public int getDefaultTargetSize() {
        return 1;
    }

    public void checkPerformDuringDay(Player owner) {
        if(owner.narrator.isDay() && !canUseDuringDay(owner))
            throw new PhaseException("You can't do this during the day.");
    }

    public boolean canUseDuringDay(Player p) {
        return false;
    }

    public void doDayAction(Player owner, PlayerList target) {
        throw new IllegalActionException();
    }

    public Action cancelDayAction(int actionIndex) {
        throw new IllegalActionException();
    }

    public void setInitialCharges(int charge, Player p) {
        if(p != null)
            charge = AddNoise(charge, p.narrator);

        if(charge != SetupModifiers.UNLIMITED)
            this.charges = new Charges(charge);
        else
            this.charges = null;
    }

    public static int AddNoise(int charge, Narrator n) {
        if(charge == SetupModifiers.UNLIMITED)
            return SetupModifiers.UNLIMITED;
        int variablity = n.getInt(SetupModifier.CHARGE_VARIABILITY);
        if(variablity == 0)
            return charge;
        int oldCharge = charge;
        charge = n.getRandom().nextGaussian() * variablity + charge;
        if(charge == 0 && n.getRandom().nextBoolean()){
            if(n.getRandom().nextDouble() * oldCharge < 1)
                charge = SetupModifiers.UNLIMITED;
            else
                charge = 1;
        }else if(charge < 0)
            charge = SetupModifiers.UNLIMITED;
        else
            charge++;
        return charge;

    }

    protected Charges charges;

    // marked and overriden
    public boolean unlimitedCharges(Player p) {
        return charges == null;
    }

    public Ability useCharge() {
        if(charges != null)
            charges.useCharge();
        return this;
    }

    public void useFakeCharge() {
        if(charges != null)
            charges.useFakeCharge();
    }

    public int getFakeCharges() {
        if(charges == null)
            return 0;
        return charges.getFakeCharges();
    }

    public int getPerceivedCharges() {
        if(charges == null)
            return SetupModifiers.UNLIMITED;
        return charges.getPerceivedCharges();
    }

    // marked and overriden
    public int getRealCharges() {
        if(charges == null)
            return SetupModifiers.UNLIMITED;
        return charges.getRealCharges();
    }

    public void addFakeCharge() {
        if(charges != null)
            charges.addFakeCharge();
    }

    public boolean isPowerRole() {
        return charges == null || charges.getPerceivedCharges() != 0;
    }

    public boolean chargeModifiable() {
        return true;
    }

    public boolean isChatRole() {
        return false;
    }

    public void onDeath(Player p) {
    }

    public static boolean Kill(Player owner, Player target, String[] flag) {
        return Kill(owner, target, flag, owner);
    }

    public static boolean Kill(Player owner, Player target, String[] flag, Player cause) {
        if(owner.narrator.isNight() && flag != Constants.POISON_KILL_FLAG) // otherwise I have other happenings to
                                                                           // record this
            happening(owner, " attacked ", target);
        DirectAttack da = new DirectAttack(owner, target, flag);
        if(cause != null)
            da.setCause(cause);
        target.kill(da);
        if(target.is(Bomb.class)){
            new Happening(owner.narrator).add(target, " blew up in ", target).add("'s face");
            owner.kill(new IndirectAttack(Constants.BOMB_KILL_FLAG, owner, target));
        }
        owner.visit(target);
        return true;
    }

    public static void Exception(String s) throws PlayerTargetingException {
        throw new PlayerTargetingException(s);
    }

    public static void deadCheck(Action a) {
        if(a.getTargets().hasDead())
            Exception(a.owner.getRoleName() + " can't use this ablity on dead targets. "
                    + a.getTargets().getDeadPlayers().getStringName() + " is dead.");
    }

    public void aliveCheck(Action a) {
        if(a.getTargets().hasLiving())
            Exception(a.owner.getRoleName() + " can't use this ablity on living targets. "
                    + a.getTargets().getLivePlayers().getStringName() + " is/are alive.");
    }

    public void aliveCheck(PlayerList p) {
        if(p.hasLiving())
            Exception("This can't use this ablity on living targets. ");
    }

    public void noAcceptableTargets() {
        Exception("You don't have any role abilities");
    }

    protected static void noTeamMateTargeting(Action a) {
        if(a.owner.getFaction().knowsTeam()){
            if(!a.getTargets().intersect(a.owner.getFaction().getMembers()).isEmpty())
                Exception("You can't target known allies");
        }

    }

    public static void NoNightActionVisit(Player owner, Player target) {
        if(target == null)
            return;
        happening(owner, " visited ", target);
        owner.visit(target);
    }

    public static void NoNightActionVisit(Action a) {
        Player owner = a.owner;
        Player target = a.getTarget();
        a.markCompleted();
        NoNightActionVisit(owner, target);
    }

    public Boolean checkWinEnemyless(Player p) {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(o.getClass() != getClass())
            return false;

        return true;
    }

    public Ability newCopy() {
        Ability a = CREATOR(getClass().getSimpleName());
        a.modifiers = Modifiers.copy(this.modifiers);
        a.id = this.id;
        return a;
    }

    @SuppressWarnings("unchecked")
    public static Class<? extends Ability> getAbilityClass(String abilityName) {
        abilityName = Util.TitleCase(abilityName);
        try{
            return (Class<? extends Ability>) Class.forName("game.roles." + abilityName);
        }catch(ClassNotFoundException | NoClassDefFoundError e){
            throw new UnknownRoleException(abilityName);
        }
    }

    public static Ability CREATOR(String abilityName) {
        try{
            return getAbilityClass(abilityName).newInstance();
        }catch(InstantiationException | IllegalAccessException e){
            throw new UnknownRoleException(abilityName);
        }
    }

    public static AbilityList CREATOR(List<String> c) {
        ArrayList<Ability> aList = new ArrayList<>();

        Ability ability;
        for(String abilityName: c){
            ability = CREATOR(abilityName.replaceAll(" ", ""));
            ability.id = Util.getID();
            aList.add(ability);
        }

        return new AbilityList(aList);
    }

    // rename to isAbility
    public static boolean isRole(String role) {
        try{
            Ability.CREATOR(role);
            return true;
        }catch(UnknownRoleException e){
            return false;
        }
    }

    public void resolveDayAction(Action action) {
        throw new UnsupportedMethodException();
    }

    public void onDayEnd(Player player) {
    }

    public void onDayStart(Player player) {
    }

    public void onNightStart(Player player) {
        decrementCooldown();
    }

    public void onPlayerNightEnd(Player player) {
    }

    public void onGameStart(Player player) {
    }

    private void decrementCooldown() {
        if(abilityCD > 0)
            abilityCD--;
    }

    public int getRemainingCooldown() {
        return abilityCD;
    }

    public boolean isOnCooldown() {
        return abilityCD > 0;
    }

    public String getCooldownText(Player p) {
        if(getCommand() == null)
            return null;
        int cd = getAbilityCooldown();
        if(cd == 0)
            return null;
        if(cd == 1)
            return getCommand() + " may be used every other day";
        return getCommand() + " may be used every day";
    }

    public String getBackToBackLabel() {
        String phaseLabel = this.isDayAbility() ? "days" : "nights";
        return "May target the same people on consecutive " + phaseLabel + " using \""
                + this.getClass().getSimpleName().toLowerCase() + "\".";
    }

    public void triggerCooldown() {
        abilityCD = getAbilityCooldown() + 1;
    }

    public static String NQuote(String s) {
        return SQuote(s + " playerName");
    }

    public static String SQuote(String s) {
        return "\'" + s + "\'";
    }

    public static Happening happening(Player player, String string, Player... targets) {
        Happening happening = new Happening(player.narrator);

        happening.add(player, string);
        happening.add(Player.list(targets));
        if(targets.length != 0 || !string.endsWith("."))
            happening.add(".");

        return happening;
    }

    @Override
    public int compareTo(Ability arg0) {
        return getClass().getSimpleName().compareTo(arg0.getClass().getSimpleName());
    }

    public int maxNumberOfSubmissions() {
        return Integer.MAX_VALUE;
    }

    public String getDayCommand() {
        return null;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "<C>";
    }

    public List<SetupModifier> getSetupModifiers() {
        return new LinkedList<>();
    }

    public List<AbilityModifier> getAbilityModifiers() {
        List<AbilityModifier> modifiers = new LinkedList<>();
        if(this.canEditBackToBackModifier())
            modifiers.add(AbilityModifier.BACK_TO_BACK);
        if(this.hasChargeAbility())
            modifiers.add(AbilityModifier.CHARGES);
        if(this.hasCooldownAbility())
            modifiers.add(AbilityModifier.COOLDOWN);
        if(this.hiddenPossible())
            modifiers.add(AbilityModifier.HIDDEN);
        if(this.canEditSelfTargetableModifer())
            modifiers.add(AbilityModifier.SELF_TARGET);
        if(this.canSubmitFree())
            modifiers.add(AbilityModifier.ZERO_WEIGHTED);
        return modifiers;
    }

    // move to static role class
    public static ArrayList<String> GetModifierRuleTexts(Modifiers<RoleModifier> modifierMap) {
        ArrayList<String> ruleTextList = new ArrayList<String>();
        if(modifierMap != null){
            if(modifierMap.hasKey(RoleModifier.UNDETECTABLE))
                ruleTextList.add("Immune to detection.");
            if(modifierMap.hasKey(RoleModifier.UNBLOCKABLE))
                ruleTextList.add("Immune to roleblocks.");
            if(modifierMap.hasKey(RoleModifier.UNCONVERTABLE))
                ruleTextList.add("Cannot be converted.");
            if(modifierMap.hasKey(RoleModifier.AUTO_VEST))
                ruleTextList.add("Starts off with " + modifierMap.getInt(RoleModifier.AUTO_VEST) + " auto vests.");
        }
        return ruleTextList;
    }

    public List<AbilityModifier> getAbilityModifiersForRoleDetails(Narrator narrator) {
        List<AbilityModifier> modifiers = this.getAbilityModifiers();
        modifiers.removeAll(AbilityModifier.GENERIC_MODIFIERS);
        return modifiers;
    }

    public List<SetupModifier> getSetupModifiersForRoleDetails(Narrator narrator) {
        return this.getSetupModifiers();
    }

    // the only one that is significantly modified is amnesiac. Make sure that
    // matches any changes here.
    // extras
    public ArrayList<String> getPublicDescription(Narrator narrator, String roleName, String nullableColor,
            AbilityList abilityList) {
        ArrayList<String> ruleTextList = new ArrayList<>();
        String tag, text;
        if(getCommand() != null)
            tag = getCommand().toLowerCase();
        else
            tag = getClass().getSimpleName().toLowerCase();
        for(AbilityModifier modifier: getAbilityModifiersForRoleDetails(narrator)){
            if(Ability.IsIntModifier(modifier))
                text = SetupModifier.getDescription(narrator, modifier, modifiers.getOrDefault(modifier, 1));
            else
                text = SetupModifier.getDescription(narrator, modifier, modifiers.getOrDefault(modifier, false));
            ruleTextList.add(text);
        }
        for(SetupModifier modifier: getSetupModifiersForRoleDetails(narrator)){
            if(Ability.IsIntModifier(modifier))
                ruleTextList.add(SetupModifier.getDescription(narrator, modifier, narrator.getInt(modifier)));
            else
                ruleTextList.add(SetupModifier.getDescription(narrator, modifier, narrator.getBool(modifier)));
        }

        if(modifiers.hasKey(AbilityModifier.CHARGES))
            ruleTextList.add(getRuleChargeText(narrator));

        int cooldown = modifiers.getOrDefault(AbilityModifier.COOLDOWN, SetupModifiers.UNLIMITED);
        if(SetupModifiers.UNLIMITED != cooldown){
            if(cooldown == 1)
                ruleTextList.add("Can only use " + tag + " every other night.");
            else
                ruleTextList.add("Can only use " + tag + " every " + cooldown + "nights.");
        }

        if(modifiers.getOrDefault(AbilityModifier.HIDDEN, false))
            ruleTextList.add(getHiddenMemberDescription());

        boolean canSelfTarget = modifiers.getOrDefault(AbilityModifier.SELF_TARGET, false);
        if(canSelfTarget != this.getDefaultSelfTargetValue())
            ruleTextList.add(getSelfTargetMemberDescription(abilityList.size() == 1, canSelfTarget));

        return ruleTextList;

    }

    public boolean isBool(Modifier modifier) {
        return IsBoolModifier(modifier);
    }

    public boolean isInt(Modifier modifier) {
        return IsIntModifier(modifier);
    }

    public ArrayList<String> getCommandParts(Action action) {
        PlayerList targets = action.getTargets();
        String command = action.getCommand();

        ArrayList<String> parts = new ArrayList<>();
        parts.add(command);
        for(int i = 0; i < targets.size(); i++){
            parts.add(targets.get(i).getName());
        }
        return parts;
    }

    // it is safe to edit commands
    public Action parseCommand(Player p, ArrayList<String> commands) {
        commands.remove(0);
        Narrator n = p.narrator;
        Player pi;
        try{
            int ability = this.getAbilityNumber();
            PlayerList pl = new PlayerList();
            for(int i = 0; i < commands.size(); i++){
                pi = n.getPlayerByName(commands.get(i));
                pl.add(pi);
            }
            return new Action(p, pl, ability, null, (String) null);
        }catch(NullPointerException e){
            throw new PlayerTargetingException("Couldn't find the playerName in question.");
        }
    }

    public void onAbilityLoss(Player p) {
    }

    public static ArrayList<String> toStringList(String... classNames) {
        return new ArrayList<String>(Arrays.asList(classNames));
    }

    public ArrayList<String> getRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<String>(), specifics = getSpecificRoleSpecs(p);
        if(isHiddenPassive())
            return ret;

        String[] roleInfo = getRoleInfo(p.narrator);
        if(roleInfo != null)
            for(String s: roleInfo)
                ret.add(s);

        String chargeText = getChargeText();
        if(chargeText != null)
            ret.add(chargeText);
        else if(getCommand() != null){
            int pCharges = getPerceivedCharges();
            if(pCharges != SetupModifiers.UNLIMITED){
                if(pCharges == 0)
                    ret.add("You may not " + getCommand().toLowerCase() + " anymore.");
                else if(pCharges == 1)
                    ret.add("You have one more use of " + getCommand().toLowerCase() + " left.");
                else
                    ret.add("You have " + pCharges + " more uses of " + getCommand().toLowerCase() + ".");
            }
        }

        String cdText = getCooldownText(p);
        if(cdText != null)
            ret.add(cdText);

        if(specifics != null)
            for(String s: specifics)
                ret.add(s);

        String selfTargetText = getSelfTargetText();
        if(selfTargetText != null)
            ret.add(selfTargetText);

        adjustRoleSpecs(ret);

        return ret;
    }

    // used so bulletproof doesn't specify twice that they're invulnerable
    // needs better solution
    // probably one where it's specified exactly what's the description in "you/me"
    // words
    public void adjustRoleSpecs(List<String> specs) {

    }

    public String getSelfTargetText() {
        if(getDefaultSelfTargetValue() != canSelfTarget(narrator) || showSelfTargetTextDefault()){
            String notText;
            if(canSelfTarget(narrator))
                notText = "";
            else
                notText = " not";

            if(p.getAbilities().filterKnown().size() == 1)
                return "You may" + notText + " target yourself.";
            return "You may" + notText + " target yourself with the " + getClass().getSimpleName().toLowerCase()
                    + " ability.";
        }
        return null;
    }

    protected String getChargeText() {
        return null;
    }

    // only used in this file
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        return null;
    }

    // null return indicates that it's active, but there is no 'target' persay

    public PlayerList getAcceptableTargets(Player pi) {
        if(isOnCooldown())
            return new PlayerList();
        Option option = null, option2 = null;
        if(this instanceof DrugDealer)
            option = new Option(DrugDealer.WIPE);
        else if(this instanceof Spy || this instanceof Framer){
            ArrayList<Option> options = getOptions(pi);
            if(options.isEmpty())
                return new PlayerList();
            option = getOptions(pi).get(0);
        }else if(this instanceof Tailor){
            option = Faction.Option(pi.getFaction());
            option2 = new Option(pi.getRoleName());
        }
        PlayerList acceptableTargets = new PlayerList();

        Action a;
        for(Player p: pi.narrator.getAllPlayers()){
            try{
                a = new Action(pi, Player.list(p), getAbilityNumber(), option, option2);
            }catch(UnknownTeamException e){
                continue;
            }

            if(isAcceptableTarget(a))
                acceptableTargets.add(p);
        }
        return acceptableTargets.sortByName();
    }

    public boolean isFakeBlockable() {
        return true;
    }

    public boolean isRetarget(Action action, Action action2) {
        return action.getTargets().sortByID().equals(action2.getTargets().sortByID());
    }

    public boolean canStartWithEnemies() {
        return true;
    }

    public boolean isRecruitable() {
        return true;
    }

    public static HashMap<AbilityModifier, Rule> ModifiersCopy(HashMap<AbilityModifier, Rule> modifierMap) {
        if(modifierMap == null)
            return null;
        HashMap<AbilityModifier, Rule> map = new HashMap<>();
        for(AbilityModifier key: modifierMap.keySet())
            map.put(key, modifierMap.get(key));

        return map;
    }

    public static String[] ToStringArray(String s) {
        return new String[] {
                s
        };
    }

    public String getName() {
        return getClass().getSimpleName();
    }

    public boolean weightAbilityModifiable() {
        return true;
    }

    // MOVE TO MEMBER
    public static void initialize(Role newRole, Player player) {
        for(Ability ability: newRole._abilities)
            ability.initialize(player);

        Modifiers<RoleModifier> modifiers = newRole.modifiers;
        if(modifiers.getOrDefault(RoleModifier.UNDETECTABLE, false)){
            player.setDetectable(false);
            happening(player, " is immune to detection roles.");
        }
        if(modifiers.getOrDefault(RoleModifier.UNBLOCKABLE, false)){
            player.setBlockable(false);
            happening(player, " is immune to role blocks");
        }

        int vests = modifiers.getOrDefault(RoleModifier.AUTO_VEST, 0);
        if(vests != 0){
            player.setAutoVestCount(vests);
            happening(player, " starts off with " + vests + " auto vests");
        }

    }

    public static int ParseAbility(String command) {
        if(Agent.COMMAND.equalsIgnoreCase(command))
            return Agent.MAIN_ABILITY;
        else if(Amnesiac.COMMAND.equalsIgnoreCase(command))
            return Amnesiac.MAIN_ABILITY;
        else if(Architect.COMMAND.equalsIgnoreCase(command))
            return Architect.MAIN_ABILITY;
        else if(Armorsmith.COMMAND.equalsIgnoreCase(command))
            return Armorsmith.MAIN_ABILITY;
        else if(ArmsDetector.COMMAND.equalsIgnoreCase(command))
            return ArmsDetector.MAIN_ABILITY;
        else if(Assassin.COMMAND.equalsIgnoreCase(command))
            return Assassin.MAIN_ABILITY;

        else if(Blackmailer.COMMAND.equalsIgnoreCase(command))
            return Blackmailer.MAIN_ABILITY;
        else if(Blacksmith.COMMAND.equalsIgnoreCase(command))
            return Blacksmith.MAIN_ABILITY;
        else if(Bodyguard.COMMAND.equalsIgnoreCase(command))
            return Bodyguard.MAIN_ABILITY;
        else if(Bread.COMMAND.equalsIgnoreCase(command))
            return BREAD_ABILITY;
        else if(Burn.COMMAND.equalsIgnoreCase(command))
            return Burn.MAIN_ABILITY;

        else if(Clubber.COMMAND.equalsIgnoreCase(command))
            return Clubber.MAIN_ABILITY;
        else if(Commuter.COMMAND.equalsIgnoreCase(command))
            return Commuter.MAIN_ABILITY;
        else if(Coroner.COMMAND.equalsIgnoreCase(command))
            return Coroner.MAIN_ABILITY;
        else if(Coward.COMMAND.equalsIgnoreCase(command))
            return Coward.MAIN_ABILITY;
        else if(CultLeader.COMMAND.equalsIgnoreCase(command))
            return CultLeader.MAIN_ABILITY;
        else if(CultInvitation.COMMAND.equalsIgnoreCase(command))
            return CultInvitation.MAIN_ABILITY;

        else if(Detective.COMMAND.equalsIgnoreCase(command))
            return Detective.MAIN_ABILITY;
        else if(Disfranchise.COMMAND.equals(command))
            return Disfranchise.MAIN_ABILITY;
        else if(Disguiser.COMMAND.equalsIgnoreCase(command))
            return Disguiser.MAIN_ABILITY;
        else if(Doctor.COMMAND.equalsIgnoreCase(command))
            return Doctor.MAIN_ABILITY;
        else if(Douse.COMMAND.equalsIgnoreCase(command))
            return Douse.MAIN_ABILITY;
        else if(Driver.COMMAND.equalsIgnoreCase(command))
            return Driver.MAIN_ABILITY;
        else if(DrugDealer.COMMAND.equalsIgnoreCase(command))
            return DrugDealer.MAIN_ABILITY;

        else if(Elector.COMMAND.equalsIgnoreCase(command))
            return Elector.MAIN_ABILITY;
        else if(ElectroManiac.COMMAND.equalsIgnoreCase(command))
            return ElectroManiac.MAIN_ABILITY;
        else if(Enforcer.COMMAND.equalsIgnoreCase(command))
            return Enforcer.MAIN_ABILITY;

        else if(FactionKill.COMMAND.equalsIgnoreCase(command))
            return FactionKill.MAIN_ABILITY;
        else if(FactionSend.COMMAND.equalsIgnoreCase(command))
            return FactionSend.MAIN_ABILITY;
        else if(Framer.COMMAND.equalsIgnoreCase(command))
            return Framer.MAIN_ABILITY;

        else if(Ghost.COMMAND.equalsIgnoreCase(command))
            return Ghost.MAIN_ABILITY;
        else if(GraveDigger.COMMAND.equalsIgnoreCase(command))
            return GraveDigger.MAIN_ABILITY;
        else if(Gun.COMMAND.equalsIgnoreCase(command))
            return SHOOT;
        else if(Gunsmith.COMMAND.equalsIgnoreCase(command))
            return Gunsmith.MAIN_ABILITY;

        else if(Interceptor.COMMAND.equalsIgnoreCase(command))
            return Interceptor.MAIN_ABILITY;
        else if(Investigator.COMMAND.equalsIgnoreCase(command))
            return Investigator.MAIN_ABILITY;

        else if(Jailor.COMMAND.equalsIgnoreCase(command) || Jailor.EXECUTE.equalsIgnoreCase(command))
            return Jailor.EXECUTE_;
        else if(Janitor.COMMAND.equalsIgnoreCase(command))
            return Janitor.MAIN_ABILITY;
        else if(Jester.COMMAND.equalsIgnoreCase(command))
            return Jester.MAIN_ABILITY;
        else if(Joker.COMMAND.equalsIgnoreCase(command))
            return Joker.MAIN_ABILITY;

        else if(Lookout.COMMAND.equalsIgnoreCase(command))
            return Lookout.MAIN_ABILITY;

        else if(Marshall.COMMAND.equalsIgnoreCase(command))
            return Marshall.MAIN_ABILITY;
        else if(MasonLeader.COMMAND.equalsIgnoreCase(command))
            return MasonLeader.MAIN_ABILITY;
        else if(MassMurderer.COMMAND.equalsIgnoreCase(command))
            return MassMurderer.MAIN_ABILITY;
        else if(Mayor.COMMAND.equalsIgnoreCase(command))
            return Mayor.MAIN_ABILITY;

        else if(Operator.COMMAND.equalsIgnoreCase(command))
            return Operator.MAIN_ABILITY;

        else if(ParityCheck.COMMAND.equalsIgnoreCase(command))
            return ParityCheck.MAIN_ABILITY;
        else if(Poisoner.COMMAND.equalsIgnoreCase(command))
            return Poisoner.MAIN_ABILITY;
        else if(ProxyKill.COMMAND.equalsIgnoreCase(command))
            return ProxyKill.MAIN_ABILITY;
        else if(Punch.COMMAND.equalsIgnoreCase(command))
            return Punch.MAIN_ABILITY;
        else if(PuppetPunch.COMMAND.equalsIgnoreCase(command))
            return PuppetPunch.MAIN_ABILITY;
        else if(PuppetVote.COMMAND.equalsIgnoreCase(command))
            return PuppetVote.MAIN_ABILITY;

        else if(Constants.RECRUITMENT.equalsIgnoreCase(command))
            return Ability.INVITATION_ABILITY;

        else if(Scout.COMMAND.equalsIgnoreCase(command))
            return Scout.MAIN_ABILITY;
        else if(SerialKiller.COMMAND.equalsIgnoreCase(command))
            return SerialKiller.MAIN_ABILITY;
        else if(Sheriff.COMMAND.equalsIgnoreCase(command))
            return Sheriff.MAIN_ABILITY;
        else if(Silence.COMMAND.equalsIgnoreCase(command))
            return Silence.MAIN_ABILITY;
        else if(Snitch.COMMAND.equalsIgnoreCase(command))
            return Snitch.MAIN_ABILITY;
        else if(Spy.COMMAND.equalsIgnoreCase(command))
            return Spy.MAIN_ABILITY;
        else if(Stripper.COMMAND.equalsIgnoreCase(command))
            return Stripper.MAIN_ABILITY;

        else if(Thief.COMMAND.equalsIgnoreCase(command))
            return Thief.MAIN_ABILITY;
        else if(Tailor.COMMAND.equalsIgnoreCase(command))
            return Tailor.MAIN_ABILITY;

        else if(Undouse.COMMAND.equalsIgnoreCase(command))
            return Undouse.MAIN_ABILITY;

        else if(Visit.COMMAND.equalsIgnoreCase(command))
            return Visit.MAIN_ABILITY;
        else if(Ventriloquist.COMMAND.equalsIgnoreCase(command))
            return Ventriloquist.MAIN_ABILITY;
        else if(Vest.COMMAND.equalsIgnoreCase(command))
            return VEST_ABILITY;
        else if(Veteran.COMMAND.equalsIgnoreCase(command))
            return Veteran.MAIN_ABILITY;

        else if(Witch.COMMAND.equalsIgnoreCase(command))
            return Witch.MAIN_ABILITY;
        else
            return INVALID_ABILITY;
    }

    public static Ability getAbilityCopy(Class<? extends Ability> abilityClass) {
        try{
            return abilityClass.newInstance();
        }catch(InstantiationException | IllegalAccessException e){
            throw new UnknownRoleException(abilityClass.getSimpleName());
        }
    }

    // ignore case
    public static Ability GetAbilityCopy(String command) {
        return GetAbilityCopy(ParseAbility(command));
    }

    public static Ability GetAbilityCopy(int ability) {
        switch (ability){
        case Agent.MAIN_ABILITY:
            return new Agent();
        case Amnesiac.MAIN_ABILITY:
            return new Amnesiac();
        case Architect.MAIN_ABILITY:
            return new Architect();
        case Armorsmith.MAIN_ABILITY:
            return new Armorsmith();
        case ArmsDetector.MAIN_ABILITY:
            return new ArmsDetector();
        case Assassin.MAIN_ABILITY:
            return new Assassin();

        case Blackmailer.MAIN_ABILITY:
            return new Blackmailer();
        case Blacksmith.MAIN_ABILITY:
            return new Blacksmith();
        case Bodyguard.MAIN_ABILITY:
            return new Bodyguard();
        case BreadAbility.MAIN_ABILITY:
            return new BreadAbility();
        case Burn.MAIN_ABILITY:
            return new Burn();

        case Clubber.MAIN_ABILITY:
            return new Clubber();
        case Commuter.MAIN_ABILITY:
            return new Commuter();
        case Coroner.MAIN_ABILITY:
            return new Coroner();
        case Coward.MAIN_ABILITY:
            return new Coward();
        case CultLeader.MAIN_ABILITY:
            return new CultLeader();
        case CultInvitation.MAIN_ABILITY:
            return new CultInvitation();

        case Detective.MAIN_ABILITY:
            return new Detective();
        case Disfranchise.MAIN_ABILITY:
            return new Disfranchise();
        case Disguiser.MAIN_ABILITY:
            return new Disguiser();
        case Doctor.MAIN_ABILITY:
            return new Doctor();
        case Douse.MAIN_ABILITY:
            return new Douse();
        case Driver.MAIN_ABILITY:
            return new Driver();
        case DrugDealer.MAIN_ABILITY:
            return new DrugDealer();

        case Elector.MAIN_ABILITY:
            return new Elector();
        case ElectroManiac.MAIN_ABILITY:
            return new ElectroManiac();
        case Enforcer.MAIN_ABILITY:
            return new Enforcer();

        case FactionKill.MAIN_ABILITY:
            return new FactionKill();
        case FactionSend.MAIN_ABILITY:
            return new FactionSend();
        case Framer.MAIN_ABILITY:
            return new Framer();

        case Ghost.MAIN_ABILITY:
            return new Ghost();
        case GraveDigger.MAIN_ABILITY:
            return new GraveDigger();
        case GunAbility.MAIN_ABILITY:
            return new GunAbility();
        case Gunsmith.MAIN_ABILITY:
            return new Gunsmith();

        case Interceptor.MAIN_ABILITY:
            return new Interceptor();
        case Investigator.MAIN_ABILITY:
            return new Investigator();

        case Jailor.MAIN_ABILITY:
            return new Jailor();
        case Janitor.MAIN_ABILITY:
            return new Janitor();
        case Jester.MAIN_ABILITY:
            return new Jester();
        case Joker.MAIN_ABILITY:
            return new Joker();

        case Lookout.MAIN_ABILITY:
            return new Lookout();

        case Marshall.MAIN_ABILITY:
            return new Marshall();
        case MasonLeader.MAIN_ABILITY:
            return new MasonLeader();
        case MassMurderer.MAIN_ABILITY:
            return new MassMurderer();
        case Mayor.MAIN_ABILITY:
            return new Mayor();

        case Operator.MAIN_ABILITY:
            return new Operator();

        case ParityCheck.MAIN_ABILITY:
            return new ParityCheck();
        case Poisoner.MAIN_ABILITY:
            return new Poisoner();
        case Punch.MAIN_ABILITY:
            return new Punch();
        case PuppetVote.MAIN_ABILITY:
            return new PuppetVote();

        case Scout.MAIN_ABILITY:
            return new Scout();
        case SerialKiller.MAIN_ABILITY:
            return new SerialKiller();
        case Sheriff.MAIN_ABILITY:
            return new Sheriff();
        case Silence.MAIN_ABILITY:
            return new Silence();
        case Snitch.MAIN_ABILITY:
            return new Snitch();
        case Spy.MAIN_ABILITY:
            return new Spy();
        case Stripper.MAIN_ABILITY:
            return new Stripper();

        case Thief.MAIN_ABILITY:
            return new Thief();
        case Tailor.MAIN_ABILITY:
            return new Tailor();

        case Undouse.MAIN_ABILITY:
            return new Undouse();

        case Ventriloquist.MAIN_ABILITY:
            return new Ventriloquist();
        case Veteran.MAIN_ABILITY:
            return new Veteran();
        case Visit.MAIN_ABILITY:
            return new Visit(null);

        case Witch.MAIN_ABILITY:
            return new Witch();
        }
        return null;
    }

    public boolean is(int... cs) {
        for(int c: cs){
            if(getAbilityNumber() == c)
                return true;
        }
        return false;
    }

    public static boolean is(int ability, String command) {
        return ParseAbility(command) == ability;
    }

    public static boolean is(int a1, int a2) {
        return a1 == a2;
    }

    public static boolean is(String command, int ability) {
        return ParseAbility(command) == ability;
    }

    public static boolean is(String command, String command2) {
        if(command == null)
            return false;
        if(command2 == null)
            return false;

        return command.equalsIgnoreCase(command2);
    }

    public static String GetRoleName(ArrayList<String> commands, Narrator n) {
        ArrayList<String> toAddBack = new ArrayList<>();

        StringBuilder sb = new StringBuilder();
        String roleName = null, s;
        Role m;
        while (!commands.isEmpty() && roleName == null){
            s = commands.remove(0);
            toAddBack.add(s);
            sb.append(s);

            m = LookupUtil.findRole(n, sb.toString());
            if(m == null){
                sb.append(" ");
            }else{
                roleName = m.getName();
            }
        }

        if(roleName == null){
            for(int i = toAddBack.size() - 1; i >= 0; i--){
                commands.add(0, toAddBack.remove(i));
            }
        }

        return roleName;

    }

    public static boolean isEqualContent(AbilityList abilities, AbilityList abilities2) {
        if(abilities == null && abilities2 == null)
            return true;
        if(abilities == null || abilities2 == null)
            return false;

        if(abilities.size() != abilities2.size())
            return false;
        for(int i = 0; i < abilities.size(); i++){
            if(!abilities.get(i).getClass().equals(abilities2.get(i).getClass()))
                return false;
        }
        return true;
    }

    public boolean isNightAbility(Player p) {
        return true;
    }

    public boolean isDayAbility() {
        return false;
    }

    // check to remove?
    public void onCancel(Action a) {
    }

    public boolean hasCooldownAbility() {
        return true;
    }

    public boolean hasChargeAbility() {
        return true;
    }

    public boolean isActivePassive(Player p) {
        return false;
    }

    public String getRuleChargeText(Narrator n) {
        int val = modifiers.getOrDefault(AbilityModifier.CHARGES, 1);
        String tag;
        if(getCommand() != null)
            tag = getCommand().toLowerCase();
        else
            tag = getClass().getSimpleName().toLowerCase();
        if(n.getInt(SetupModifier.CHARGE_VARIABILITY) != 0){
            if(val == 1)
                return "Limited to ~1 use of " + tag + ".";
            return "Limited to ~" + val + " uses of " + tag + ".";
        }
        if(val == 1)
            return "Limited to 1 use of " + tag + ".";
        return "Limited to " + val + " uses of " + tag + ".";
    }

    public abstract String getAbilityDescription(Narrator n);

    public boolean affectsSending() {
        return false;
    }

    public boolean allowsForFriendlyFire() {
        return false;
    }

    public static String getDefaultUsage(Ability a, Player p, Random r) {
        return a.getCommand() + " *" + p.getAcceptableTargets(a.getAbilityNumber()).getRandom(r).getName() + "*";
    }

    public String getUsage(Player p, Random r) {
        return getDefaultUsage(this, p, r);
    }

    public Action getExampleAction(Player owner) {
        PlayerList pl = owner.getAcceptableTargets(getAbilityNumber());
        if(pl.isEmpty())
            return null;
        return new Action(owner, getAbilityNumber(), pl.getFirst());
    }

    public Action getExampleWitchAction(Player owner, Player target) {
        return new Action(owner, getAbilityNumber(), target);
    }

    public boolean isDatabaseAbility() {
        return true;
    }

    public boolean stopsParity(Player player) {
        return false;
    }

    public boolean onDayEndTriggersWhileDead() {
        return false;
    }

    public boolean successfulUsageTriggersBreadUsage(Action a) {
        return false;
    }

    // cost of adding this action, given the actions that are submitted
    // also used to calculate the action's given weight, if its in the set of
    // actions already

    public boolean canAddAnotherAction(Action a) {
        return true;
    }

    public void resolveFakeBreadedActions() {
        ActionList dayActions = p.getActions();
        if(dayActions == null || dayActions.isEmpty())
            return;
        Player submitter = dayActions.getFirst().owner;
        int realBreads = 1 + BreadAbility.getUseableBread(submitter, true);

        Action overflow;
        for(int i = realBreads; i < dayActions.size(); i++){
            overflow = dayActions.getActions().get(i);
            dayActions.remove(overflow);
            overflow.owner.narrator.removeActionStack(overflow);
        }
    }

    public boolean supportedFactionAbility() {
        return true;
    }

    public boolean canSubmitFree() {
        return modifiers != null && modifiers.getOrDefault(AbilityModifier.ZERO_WEIGHTED, false);
    }

    public boolean canSubmitWithoutBread(Action a) {
        return true;
    }

    public String getChargeDescription(Narrator n) {
        return Ability.CHARGE_DESCRIPTION + " for " + getCommand();
    }

    public String getCooldownDescription(Narrator n) {
        return Ability.COOLDOWN_DESCRIPTION + " for " + getClass().getSimpleName().toLowerCase();
    }

    public String getZeroWeightDescription(Narrator n) {
        return Ability.ABILITY_WEIGHT_DESCRIPTION + getCommand();
    }

    public String getHiddenStatusRuleLabel() {
        return Ability.HIDDEN_ATTRIBUTE_DESCRIPTION + getClass().getSimpleName();
    }

    public void onNightFeedback(ArrayList<Feedback> feedback) {
    }

    public void checkIsNightless(Narrator n) {
        throw new IllegalGameSettingsException("Illegal nightless ability: " + getClass().getSimpleName());
    }

    public boolean isHiddenPassive() {
        if(!hiddenPossible())
            return false;
        if(this.modifiers == null)
            return false;
        return this.modifiers.getOrDefault(AbilityModifier.HIDDEN, false);
    }

    public boolean hiddenPossible() {
        return false;
    }

    public boolean isUnique() {
        return false;
    }

    public String getHiddenMemberDescription() {
        return "Is unaware of " + getClass().getSimpleName() + " passive";
    }

    public boolean isRolePickingPhaseAbility() {
        return false;
    }

}
