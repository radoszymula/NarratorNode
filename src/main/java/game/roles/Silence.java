package game.roles;

import game.event.Announcement;
import game.event.AnnouncementCheckCondition;
import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import models.FactionRole;
import models.Modifiers;
import services.FactionService;
import services.RoleService;

public class Silence extends Ability {

    public static final int MAIN_ABILITY = Ability.SILENCE;
    public static final String COMMAND = "Silence";
    public static final String FEEDBACK = "You were silenced.  You may not talk today.";
    public static final String NIGHT_ACTION_DESCRIPTION = "Stop players from talking.";

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        setSilenced(owner, target);

        a.markCompleted();
        owner.visit(target);
    }

    static void setSilenced(Player owner, final Player target) {
        target.setSilenced();
        Feedback f = new Feedback(target, FEEDBACK);
        f.setPicture("blackmailer");
        f.addExtraInfo("You may not talk today.  Keep quiet.");
        f.hideableFeedback = false;
        happening(owner, " silenced ", target);

        if(announcedSilencings(owner)){
            Announcement announcement = new Announcement(owner.narrator);
            announcement.add(target, " is silenced.");
            final int silencedDay = owner.narrator.getDayNumber() + 1;
            announcement.setCheckCondition(new AnnouncementCheckCondition() {
                @Override
                public boolean isAnnounceable() {
                    if(target.isAlive())
                        return true;
                    return target.getDeathDay() > silencedDay;
                }
            });
            owner.narrator.announcement(announcement);
        }
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Silencer", Silence.class);
        return FactionService.createFactionRole(faction, role);
    }

    private static boolean announcedSilencings(Player silencer) {
        Modifiers<AbilityModifier> modifierMap;
        if(silencer.hasAbility(Silence.class)){
            modifierMap = silencer.getAbility(Silence.class).modifiers;
            if(modifierMap != null && modifierMap.getOrDefault(AbilityModifier.SILENCE_ANNOUNCED, false))
                return true;
        }
        if(!silencer.hasAbility(Blackmailer.class))
            return false;
        modifierMap = silencer.getAbility(Blackmailer.class).modifiers;
        return modifierMap != null && modifierMap.getOrDefault(AbilityModifier.SILENCE_ANNOUNCED, false);
    }
}
