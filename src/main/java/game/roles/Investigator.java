package game.roles;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.Util;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Investigator extends Ability {

    public static final int MAIN_ABILITY = Ability.INVESTIGATE;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Investigate someone to determine their role";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    public static final String NOT_SUSPICIOUS = "Your target is not suspicious.";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        Feedback(owner, target, a.getIntendedTarget(), false);

        happening(owner, " investigated ", target);
        a.markCompleted();
        owner.visit(target);
    }

    public static Feedback Feedback(Player investigator, Player target, String nullIfJanitor, boolean isDead) {
        String feedbackText = "Your target is a";
        String role = null;
        if(target.getFrameStatus() != null && !isDead){
            role = target.getFrameStatus().getName();
        }else if(target.isDetectable() || isDead){
            role = target.getRoleName();
        }else{
            FactionRole m = Snitch.getCitizenTeam(investigator.narrator);
            if(m == null)
                role = target.getRoleName();
            else
                role = m.getName();
        }

        if(Util.vowelStart(role)){
            feedbackText += "n ";
        }else
            feedbackText += " ";

        feedbackText += role + ".";

        Feedback feedback = new Feedback(investigator);
        feedback.add(feedbackText);
        feedback.setPicture("detective");
        if(nullIfJanitor != null)
            feedback.addExtraInfo("As a reminder, you attempted to investigate " + nullIfJanitor + ".");
        if(investigator.narrator.isDay())
            investigator.sendMessage(feedback);
        return feedback;
    }

    public static final String COMMAND = "Investigate";

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Investigator", Investigator.class);
    }
}
