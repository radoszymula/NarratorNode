package game.roles;

import java.util.ArrayList;
import java.util.HashSet;

import game.event.Happening;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownRoleException;
import game.logic.support.HTString;
import game.logic.support.Random;
import game.logic.support.RolePackage;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Thief extends Ability {

    public static final String NIGHT_ACTION_DESCRIPTION = "Become one of the unselected roles.";
    public static final String COMMAND = "pick";

    public static final int MAIN_ABILITY = Ability.THIEF_PICK;

    public Thief() {
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public void doNightAction(Action action) {
        HashSet<RolePackage> choices = getValidRoleChoices(action.owner);
        FactionRole rolePackage = getPickedRole(action.getOption(), action.owner, choices).assignedRole;
        addThiefChangingRoleHappening(action.owner, rolePackage);
        Amnesiac.changeRoleAndTeam(action.owner, rolePackage);
        action.owner.getActions().clear();
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public void mainAbilityCheck(Action action) {
        HashSet<RolePackage> allChoices = getAllRoleChoices(action.owner);
        HashSet<RolePackage> validChoices = getValidRoleChoices(action.owner);
        RolePackage foundRole = getPickedRole(action.getOption(), action.owner, allChoices);
        RolePackage foundValidRole = getPickedRole(action.getOption(), action.owner, validChoices);

        if(foundValidRole != null)
            return;

        if(foundRole == null)
            throw new UnknownRoleException(action.getOption() + " is not a valid role or team.");

        String message = "Roles with lower priority are not allowed to be picked.";
        if(validChoices.size() == 1){
            FactionRole role = validChoices.iterator().next().assignedRole;
            Faction team = action.owner.narrator.getFaction(role.getColor());
            message += "  '" + team.getName() + "' or '" + role.getName() + "' are the only allowed choices.";
        }
        throw new UnknownRoleException(message);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String getUsage(Player player, Random random) {
        String option = Thief.getOptionValue(player);
        return COMMAND + " " + option;
    }

    @Override
    public Action getExampleAction(Player owner) {
        String option = Thief.getOptionValue(owner);
        return new Action(owner, MAIN_ABILITY, option, (String) null);
    }

    @Override
    public boolean isUnique() {
        return true;
    }

    @Override
    public boolean canEditBackToBackModifier() {
        return false;
    }

    @Override
    public boolean canEditZeroWeightedModifier() {
        return false;
    }

    @Override
    public boolean canStartWithEnemies() {
        return false;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public int getDefaultTargetSize() {
        return 0;
    }

    @Override
    public boolean isRolePickingPhaseAbility() {
        return true;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        return this.getActionDescription(actions.get(0));
    }

    @Override
    public ArrayList<Object> getActionDescription(Action action) {
        Narrator narrator = action.owner.narrator;
        HashSet<RolePackage> choices = getValidRoleChoices(action.owner);
        FactionRole pickedRole = getPickedRole(action.getOption(), action.owner, choices).assignedRole;
        Faction pickedTeam = narrator.getFaction(pickedRole.getColor());
        ArrayList<Object> list = new ArrayList<>();
        list.add(getCommand().toLowerCase() + " ");
        list.add(new HTString(pickedRole.getName(), pickedTeam));
        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action action) {
        Narrator narrator = action.owner.narrator;
        HashSet<RolePackage> choices = getValidRoleChoices(action.owner);
        FactionRole pickedRole = getPickedRole(action.getOption(), action.owner, choices).assignedRole;
        Faction pickedTeam = narrator.getFaction(pickedRole.getColor());
        ArrayList<Object> list = new ArrayList<>();
        list.add(getCommand().toLowerCase() + "ing ");
        list.add(new HTString(pickedRole.getName(), pickedTeam));
        return list;
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        if(a.ability != MAIN_ABILITY)
            return super.getCommandParts(a);
        ArrayList<String> parsedCommands = new ArrayList<>();
        parsedCommands.add(COMMAND);
        parsedCommands.add(a.getOption());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player player, ArrayList<String> commands) {
        commands.remove(0);

        if(commands.isEmpty())
            throw new PlayerTargetingException("Need to pick a role with this action");

        String pick = commands.get(0);

        Action a = new Action(player, MAIN_ABILITY, pick, (String) null);
        mainAbilityCheck(a);
        return a;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Thief", Thief.class);
        return FactionService.createFactionRole(faction, role);
    }

    public static void fillInMissingActions(Narrator narrator) {
        PlayerList thieves = narrator._players.filter(Thief.class);
        String option;
        Action action;
        for(Player thief: thieves){
            if(!thief.getActions().isEmpty())
                continue;
            option = getOptionValue(thief);
            action = new Action(thief, MAIN_ABILITY, option, null);
            narrator.getEventManager().addCommand(thief, COMMAND, option);
            thief.getActions().addAction(action);
        }
    }

    private static String getOptionValue(Player thief) {
        FactionRole forcedPickedRole = getValidRoleChoices(thief).iterator().next().assignedRole;
        if(getPickedRole(forcedPickedRole.getName(), thief, getValidRoleChoices(thief)) == null)
            return forcedPickedRole.getColor();
        return forcedPickedRole.getName();
    }

    private static RolePackage getPickedRole(String pickName, Player thief, HashSet<RolePackage> roleChoices) {
        Faction team = thief.narrator.getFaction(pickName);
        if(team == null)
            team = thief.narrator.getFactionByName(pickName);

        RolePackage found = null;
        if(team != null){
            for(RolePackage roleChoice: roleChoices){
                if(!roleChoice.assignedRole.getColor().equals(team.getColor()))
                    continue;
                if(found != null)
                    return null;
                found = roleChoice;
            }
        }else{
            for(RolePackage roleChoice: roleChoices){
                if(!roleChoice.assignedRole.getName().equalsIgnoreCase(pickName))
                    continue;
                if(found != null)
                    return null;
                found = roleChoice;
            }
        }

        return found;
    }

    private static HashSet<RolePackage> getValidRoleChoices(Player thief) {
        HashSet<RolePackage> choices = new HashSet<>();
        Faction team;
        int priority = Integer.MIN_VALUE;
        for(RolePackage choice: thief.narrator.unpickedRoles){
            team = thief.narrator.getFaction(choice.assignedRole.getColor());
            if(team.getPriority() > priority){
                priority = team.getPriority();
                choices.clear();
            }
            if(team.getPriority() == priority)
                choices.add(choice);
        }
        return choices;
    }

    private static HashSet<RolePackage> getAllRoleChoices(Player thief) {
        return thief.narrator.unpickedRoles;
    }

    private static void addThiefChangingRoleHappening(Player player, FactionRole rolePackage) {
        Faction roleTeam = player.narrator.getFaction(rolePackage.getColor());

        Happening happening = new Happening(player.narrator);
        HTString htRoleString = new HTString(rolePackage.getName(), roleTeam);
        happening.add(player, " became a ", htRoleString, ".");
    }
}
