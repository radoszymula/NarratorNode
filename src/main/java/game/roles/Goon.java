package game.roles;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Goon extends Passive {

    public Goon() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public void mainAbilityCheck(Action a) {
        noAcceptableTargets();
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "A henchman of the mafia.";

    public static final String DEATH_FEEDBACK = "You were killed by the ";
    public static final String ANONYMOUS_DEATH_FEEDBACK = "You became a victim of organized crime.";

    @Override
    public void checkIsNightless(Narrator n) {
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Goon", Goon.class);
    }
}
