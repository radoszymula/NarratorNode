package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;

public class Douse extends Ability {

    public static final int MAIN_ABILITY = Ability.DOUSE;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.DOUSE_FEEDBACK);
        rules.add(SetupModifier.GUARD_REDIRECTS_DOUSE);
        return rules;
    }

    public static final String COMMAND = "Douse";
    public static final String DOUSE = COMMAND;

    public String getNightText(ArrayList<Faction> t) {
        return "Type " + NQuote(COMMAND) + " to ignite everyone doused.";
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Douse someone in flammable gasoline.";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();

        if(target == null)
            return;

        if(target.narrator.getBool(SetupModifier.GUARD_REDIRECTS_DOUSE))
            target = Bodyguard.redirect(target);

        target.setDoused(true);
        happening(owner, " doused ", target);
        a.markCompleted();
        owner.visit(target);
    }

    public static final String DOUSED_FEEDBACK = "You were doused with gasoline last night!";

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<Object>();

        list.add("undousing ");
        list.addAll(StringChoice.YouYourself(a.owner, a.getTargets()));
        return list;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> hasBurn = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.DOUSE_FEEDBACK)){
            hasBurn.add("Targeting people will give them 'douse' feedback.");
        }else{
            hasBurn.add("Targets will be unaware that they are doused them.");
        }

        if(p.narrator.getBool(SetupModifier.GUARD_REDIRECTS_DOUSE)){
            hasBurn.add("Players guarding your doused target will absorb the douse instead of your target.");
        }

        return hasBurn;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public boolean isDatabaseAbility() {
        return false;
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }
}
