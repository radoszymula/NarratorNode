package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Stripper extends Ability {

    public static final int MAIN_ABILITY = Ability.BLOCK;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Entertain at night. Targets will not be able to complete any pending night actions.";

    public static final String COMMAND = "Block";
    public static final String BLOCK = COMMAND;

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.BLOCK_FEEDBACK);
        return rules;
    }

    public static final String FEEDBACK = "An attractive visitor occupied your night. You were unable to do anything else.";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();

        if(target == null)
            return;

        target.setBlocked(owner);
        happening(owner, " blocked ", target);

        a.markCompleted();
        if(target.is(Veteran.class) && target.getAction(Veteran.MAIN_ABILITY) != null){

        }else if(!target.isBlockable()){

        }else{
            for(Action ab: target.getActions().getActions()){
                ab.setInvalid();
            }
        }
        owner.visit(target);
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.BLOCK_FEEDBACK)){
            ret.add("Your targets will be know that they couldn't complete their action(s).");
        }else{
            ret.add("Your targets will not know that you visited them.");
        }
        return ret;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    public static void FeedbackGenerator(Feedback f) {
        f.add(FEEDBACK).setPicture("stripper").addExtraInfo("None of your actions went through last night!");
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Stripper", Stripper.class);
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }
}
