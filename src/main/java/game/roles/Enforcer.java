package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Enforcer extends Ability {

    public static final String NIGHT_ACTION_DESCRIPTION = "An acolyte of the masonry, prevent someone from being recruited.";
    public static final String ROLE_NAME = "Mason Enforcer";
    public static final String COMMAND = "Enforce";
    public static final String FEEDBACK = " tried to recruit your target.";

    public static final int MAIN_ABILITY = Ability.ENFORCE;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public void mainAbilityCheck(Action a) {
        MasonLeader.MasonryCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        Player enforcer = a.owner;
        if(target == null)
            return;

        target.setEnforced(enforcer);

        happening(enforcer, " enforced ", target);
        a.markCompleted();
        enforcer.visit(target);

        return;
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.ENFORCER_LEARNS_NAMES);
        return rules;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.ENFORCER_LEARNS_NAMES)){
            ret.add("You will learn the names of everyone that attempts to convert your target.");
        }else{
            ret.add("YOu will not know who tries to convert your target.");
        }

        return ret;
    }

    public static ArrayList<Object> FeedbackGenerator(Player cultLeader) {
        ArrayList<Object> parts = new ArrayList<>();
        parts.add(cultLeader);
        parts.add(FEEDBACK);
        return parts;
    }

    public static void addFeedback(Player enforcer, Player cultLeader) {
        if(enforcer.narrator.getBool(SetupModifier.ENFORCER_LEARNS_NAMES)){
            Feedback f = new Feedback(enforcer);
            f.setPicture("lookout");
            f.add(FeedbackGenerator(cultLeader));
        }

    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Enforcer", Enforcer.class);
        return FactionService.createFactionRole(faction, role);
    }
}
