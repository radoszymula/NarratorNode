package game.roles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import game.logic.Player;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.templates.BasicRoles;
import game.roles.support.Consumable;

public class AbilityList implements Iterable<Ability> {

    private ArrayList<Ability> list;
    public String name; // move these out

    private AbilityList() {
        list = new ArrayList<>();
    }

    public AbilityList(Ability... r) {
        this();
        for(Ability a: r)
            add(a);
        checkList();
    }

    public AbilityList(ArrayList<Ability> aList) {
        this();
        for(Ability a: aList)
            add(a);
        checkList();
    }

    public AbilityList(AbilityList list, Ability... others) {
        this();
        for(Ability a: list)
            add(a);
        for(Ability a: others)
            add(a);
    }

    public AbilityList(AbilityList abilities, AbilityList attainedAbilities) {
        this();
        for(Ability a: abilities)
            add(a);
        for(Ability a: attainedAbilities)
            add(a);

    }

    private void checkList() {
        int size = 0;
        for(Ability a: this){
            if(a instanceof ItemAbility)
                continue;
            size++;
        }

        if(contains(Amnesiac.class) && size != 1)
            throw new IllegalRoleCombinationException("Amnesiac cannot be combined with other roles");
    }

    public static String GetTitleCaseName(String s) {
        StringBuilder sb = new StringBuilder();

        String[] split = s.split("(?=\\p{Upper})");
        for(int i = 0; i < split.length; i++){
            if(i != 0)
                sb.append(" ");
            sb.append(split[i]);
        }
        return sb.toString();
    }

    private void add(Ability a) {
        if(name == null && list.isEmpty()){
            name = GetTitleCaseName(a.getClass().getSimpleName());
        }
        if(!contains(a.getClass()))
            list.add(a);
    }

    public void add(ItemAbility<? extends Consumable> a) {
        add((Ability) a);
    }

    @Override
    public Iterator<Ability> iterator() {
        return list.iterator();
    }

    public boolean contains(String command) {
        if(command == null)
            return false;
        for(Ability a: this){
            if(command.equalsIgnoreCase(a.getCommand()))
                return true;
        }
        return false;
    }

    public AbilityList copy() {
        AbilityList aList = new AbilityList();
        aList.name = this.name;

        for(Ability a: this)
            aList.add(a.newCopy());

        return aList;
    }

    public int size() {
        return list.size();
    }

    public Ability get(int i) {
        return list.get(i);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public AbilityList getNonpassive() {
        AbilityList aList = new AbilityList();
        for(Ability a: this)
            if(a.getCommand() != null)
                aList.add(a);

        return aList;
    }

    // used for witch
    public AbilityList filterHasCharge() {
        AbilityList aList = new AbilityList();
        for(Ability a: this)
            if(a.getCommand() != null && a.getRealCharges() != 0)
                aList.add(a);

        return aList;
    }

    public AbilityList filterKnown() {
        AbilityList aList = new AbilityList();
        for(Ability a: this){
            if(!a.isHiddenPassive())
                aList.add(a);
        }

        return aList;
    }

    public AbilityList getNightAbilities(Player p) {
        AbilityList aList = new AbilityList();
        for(Ability a: this)
            if(a.getCommand() != null && a.isNightAbility(p))
                aList.add(a);

        return aList;
    }

    public AbilityList getDayAbilities() {
        AbilityList aList = new AbilityList();
        for(Ability a: this)
            if(a.getCommand() != null && a.isDayAbility())
                aList.add(a);

        return aList;
    }

    public Ability getFirst() {
        if(isEmpty())
            return null;
        return get(0);
    }

    public boolean contains(Class<? extends Ability> class1) {
        for(Ability a: this)
            if(a.getClass().equals(class1))
                return true;
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < size(); i++){
            if(i != 0)
                sb.append(",\n");
            sb.append(get(i).getClass().getSimpleName());
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!o.getClass().equals(getClass()))
            return false;

        return Ability.isEqualContent(this, (AbilityList) o);
    }

    public Ability getAbility(String abilityName) {
        for(Ability a: this){
            if(a.getClass().getSimpleName().equalsIgnoreCase(abilityName))
                return a;
        }
        return null;
    }

    public Ability getAbility(Class<? extends Ability> abilityClass) {
        for(Ability a: this){
            if(a.getClass() == abilityClass)
                return a;
        }
        return null;
    }

    public AbilityList replace(Ability oldR, Ability newR) {
        AbilityList aList = new AbilityList(this.list);
        aList.name = this.name;
        aList.list.add(newR);
        for(int i = 0; i < list.size(); i++){
            if(aList.list.get(i).getClass().equals(oldR.getClass())){
                aList.list.remove(i);
                break;
            }
        }
        return aList;
    }

    public String getDatabaseName() {
        StringBuilder ret = new StringBuilder();

        for(Ability a: list){
            if(a.is(Burn.MAIN_ABILITY))
                ret.append(BasicRoles.ARSONIST);
            else if(a.isDatabaseAbility())
                ret.append(a.getClass().getSimpleName());
            else
                continue;
            break;
        }

        if(ret.length() == 0 && !list.isEmpty())
            ret.append(list.get(0).getClass().getSimpleName());

        return ret.toString();
    }

    public AbilityList remove(Class<? extends Ability> class1) {
        AbilityList aList = new AbilityList();
        for(Ability a: this)
            if(a.getClass() != class1)
                aList.add(a);

        return aList;
    }

    @Override
    public int hashCode() {
        String rep = this.name + "^";
        ArrayList<String> ability_s = new ArrayList<>();
        for(Ability a: this.list)
            ability_s.add(a.getClass().getSimpleName());

        Collections.sort(ability_s);
        rep += ability_s.hashCode();
        return rep.hashCode();
    }

    public Ability[] toArray() {
        Ability[] array = new Ability[this.list.size()];
        for(int i = 0; i < array.length; i++){
            array[i] = this.list.get(i);
        }
        return array;
    }

}
