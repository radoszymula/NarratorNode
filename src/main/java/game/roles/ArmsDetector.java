package game.roles;

import java.util.ArrayList;
import java.util.List;

import game.event.Feedback;
import game.event.Happening;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.support.action.Action;
import models.FactionRole;
import services.FactionService;
import services.RoleService;
import util.LookupUtil;

public class ArmsDetector extends Ability {

    public static final int MAIN_ABILITY = Ability.ARMS_DETECT;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Investigate someone to determine if they have access to weapons.";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }

    public static final String NO_GUNS = "Your target doesn't appear to have weapons.";
    public static final String HAS_GUNS = "Your target appears to have weapons.";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        Feedback(owner, target);

        new Happening(owner.narrator).add(owner, " checked ", target, " for weapons");
        a.markCompleted();
        owner.visit(target);
    }

    public static Feedback Feedback(Player scout, Player target) {
        String feedback;
        if(!target.isDetectable())
            feedback = NO_GUNS;
        else if(target.getFrameStatus() != null)
            feedback = HAS_GUNS;
        else if(target.is(Doctor.class) || target.is(SerialKiller.class))
            feedback = NO_GUNS;
        else if(target.getFactions().hasAbility(FactionKill.MAIN_ABILITY))
            feedback = HAS_GUNS;
        else if(target.is(Sheriff.class) || target.is(Veteran.class))
            feedback = HAS_GUNS;
        else if(GunAbility.HasNightGuns(target))
            feedback = HAS_GUNS;
        else
            feedback = NO_GUNS;

        Feedback f = new Feedback(scout);
        f.add(feedback);
        if(feedback.equals(NO_GUNS))
            f.setPicture("citizen");
        else
            f.setPicture("mafioso");

        return f;
    }

    public static Feedback Feedback(Player armsDetector, Player target, String nullIfJanitor, boolean isDead) {
        String feedback = "Your target is a";
        String role = null;
        if(target.getFrameStatus() != null && !isDead){
            role = target.getFrameStatus().getName();
        }else if(target.isDetectable() || isDead){
            role = target.getRoleName();
        }else{
            FactionRole m = Snitch.getCitizenTeam(armsDetector.narrator);
            if(m == null)
                role = target.getRoleName();
            else
                role = m.role.getName();
        }

        feedback += role + ".";

        Feedback f = new Feedback(armsDetector);
        f.add(feedback);
        f.setPicture("lookout");
        if(nullIfJanitor != null)
            f.addExtraInfo("As a reminder, you attempted to search " + nullIfJanitor + " for arms.");

        return f;
    }

    public static final String COMMAND = "Detect";

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public ArrayList<String> getPublicDescription(Narrator narrator, String actionOriginatorName, String nullableColor, AbilityList abilities) {
        ArrayList<String> list = super.getPublicDescription(narrator, actionOriginatorName, nullableColor, abilities);

        List<Role> sheriffs = LookupUtil.findRolesWithAbility(narrator, Sheriff.class);
        if(!sheriffs.isEmpty()){
            StringBuilder sb = new StringBuilder();
            int i = 0;
            for(Role s: sheriffs){
                if(i != 0)
                    sb.append(", ");
                list.add(s.getName());
                i++;
            }
            list.add(" will be detected as having guns");
        }

        return list;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Arms Detector", ArmsDetector.class);
        return FactionService.createFactionRole(faction, role);
    }
}
