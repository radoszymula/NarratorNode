package game.roles;

import java.util.ArrayList;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.support.Bread;
import game.roles.support.Gun;
import game.roles.support.Vest;
import models.FactionRole;
import models.Modifiers;
import services.FactionService;
import services.RoleService;

public class GraveDigger extends Ability {

    public static final String ROLE_NAME = "Grave Digger";

    private PlayerList prevTargeted;

    @Override
    public Ability initialize(Player p) {
        if(!p.narrator.getBool(SetupModifier.GD_REANIMATE))
            prevTargeted = new PlayerList();
        return super.initialize(p);
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getNightText(Player p) {
        return "Type " + SQuote(COMMAND + " deadName aliveName")
                + " to first person target the second.  For more complex actions, use the format "
                + SQuote(COMMAND + " deadName aliveName <action>") + " where <action> matches the format of an action.";
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Reanimate a dead person to target living people with their actions";

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();
        Action a = actionList.get(0);
        Player owner = a.owner;

        PlayerList targets;
        Ability ability;
        Action victimAction;
        for(Action da: actionList){

            Player victim = da.getTarget();
            ability = Ability.GetAbilityCopy(a.getOption());

            list.add("dig up ");
            list.add(StringChoice.YouYourselfSingle(owner, victim));
            if(da.getTargets().size() > 1){
                targets = da.getTargets().copy().remove(victim);
                if(victim.getDeathType().isCleaned()){
                    list.add(" to target ");
                    list.addAll(StringChoice.YouYourself(owner, targets));
                    continue;
                }
                if(ability == null || ability instanceof Spy){
                    list.add(" to target ");
                    list.addAll(StringChoice.YouYourself(owner, targets));
                }else{
                    victimAction = new Action(victim, targets, ability.getAbilityNumber(), a.getOption2(),
                            a.getOption3());
                    try{
                        ArrayList<Object> parts = ability.getActionDescription(victimAction);
                        list.add(" to ");
                        list.addAll(parts);
                    }catch(IndexOutOfBoundsException e){
                        list.add(" to target ");
                        list.addAll(StringChoice.YouYourself(owner, targets));
                    }
                }
            }else if(victim.is(Douse.class)){// && da.getOption() != null){
                list.add(" to burn");
            }

            list.add(" and ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    @Override
    public void selfTargetableCheck(Action a) {
        if(!a.owner.in(a._targets.subList(1)))
            return;
        if(!canSelfTarget(a.owner.narrator))
            Exception("You may not target yourself with this action.");
    }

    @Override
    public void targetSizeCheck(Action a) {
        Player corpse = a.getTarget();
        if(corpse == null)
            Exception("You need to select a target with this ability");
    }

    public static final String WITCH_FEEDBACK = "You were witched!";

    @Override
    public void mainAbilityCheck(Action a) {
        Player corpse = a._targets.getFirst();
        if(corpse.isAlive())
            Ability.Exception("Can't reanimate living people");

        PlayerList targets = a._targets.copy();
        targets.remove(corpse);

        if(targets.hasDead())
            Exception("Can't make dead people target dead people.");

        if(a.getOption() != null && !actionInGame(Ability.ParseAbility(a.getOption()), corpse.narrator))
            Exception("No one has this ability.");

        if(a.getOption() != null && isBannedAction(Ability.ParseAbility(a.getOption())))
            Exception("This ability can't be triggered by Grave Diggers.");

        if(a.owner.narrator.getBool(SetupModifier.GD_REANIMATE))
            return;
        if(prevTargeted != null && prevTargeted.contains(corpse))
            Exception("You may not reanimate this corse again.");

    }

    public static final int[] BANNED_ACTIONS = {
            Agent.MAIN_ABILITY, Amnesiac.MAIN_ABILITY, Architect.MAIN_ABILITY, ArmsDetector.MAIN_ABILITY,
            Coroner.MAIN_ABILITY, CultLeader.MAIN_ABILITY, Detective.MAIN_ABILITY, Disguiser.MAIN_ABILITY,
            FactionKill.MAIN_ABILITY, FactionSend.MAIN_ABILITY, Ghost.MAIN_ABILITY, GraveDigger.MAIN_ABILITY,
            Investigator.MAIN_ABILITY, Jailor.MAIN_ABILITY, Lookout.MAIN_ABILITY, MasonLeader.MAIN_ABILITY,
            Mayor.MAIN_ABILITY, Sheriff.MAIN_ABILITY, Snitch.MAIN_ABILITY, Spy.MAIN_ABILITY, Ventriloquist.MAIN_ABILITY
    };

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        Narrator n = p.narrator;
        if(n._players.hasDead())
            return n.getDeadPlayers().sortByName().add(n.getLivePlayers().sortByName());
        return new PlayerList();
    }

    private static boolean actionInGame(int ability, Narrator n) {
        if(ability == Visit.MAIN_ABILITY)
            return true;
        if(ability == Gun.MAIN_ABILITY && (n.getPossibleMembers().hasRole(Gunsmith.class)
                || n.getPossibleMembers().hasRole(Blacksmith.class) || n.getPossibleMembers().hasRole(Vigilante.class)))
            return true;
        if(ability == Bread.MAIN_ABILITY && n.getPossibleMembers().hasRole(Baker.class))
            return true;
        if(ability == Vest.MAIN_ABILITY
                && (n.getPossibleMembers().hasRole(Survivor.class) || n.getPossibleMembers().hasRole(Blacksmith.class)
                        || n.getPossibleMembers().hasRole(Armorsmith.class)))
            return true;
        for(FactionRole m: n.getPossibleMembers()){
            for(Ability a: m.role._abilities){
                if(a.getAbilityNumber() == ability)
                    return true;
            }
        }
        return false;
    }

    public static final String LIVE_FEEDBACK = "You tried to dig up the grave of a living person.";

    @Override
    public void doNightAction(Action action) {
        Player gd = action.owner;
        Player dead = action.getTargets().getFirst();
        if(dead.isAlive()){
            new Feedback(gd, LIVE_FEEDBACK).setPicture("amnesiac");
            NoNightActionVisit(action);
            return;
        }

        PlayerList targets = action.getTargets().remove(dead);

        String command = action.getOption();
        int ability_parse = Ability.ParseAbility(command);
        if(isBannedAction(ability_parse))
            command = null;

        if(command == null || Visit.COMMAND.equalsIgnoreCase(command)){
            for(Ability a: dead.getAbilities()){
                if(a.getRealCharges() == 0)
                    continue;
                if(a.is(Joker.MAIN_ABILITY) && action.getOption2() == null && !Joker.CanAddBountyActionTonight(dead))
                    continue;
                if(!a.is(BANNED_ACTIONS) && a.getCommand() != null && a.isNightAbility(dead)){
                    command = a.getCommand();
                    ability_parse = a.getAbilityNumber();
                    break;
                }
            }
        }

        if(dead.is(Ghost.class) && !dead.getActions().isEmpty()){
            dead.getActions().getFirst().setTarget(targets.getFirst());
        }else if(command != null){
            addCorpseAction(gd, dead, command, action.getOption2(), action.getOption3(), targets);
        }else{
            boolean foundActionToSubmit = false;
            for(Ability a: dead.getAbilities()){
                if(a.is(Ghost.MAIN_ABILITY, GraveDigger.MAIN_ABILITY, Ventriloquist.MAIN_ABILITY,
                        Disguiser.MAIN_ABILITY))
                    continue;
                if(a.isAcceptableTarget(new Action(dead, targets, Ability.ParseAbility(a.getCommand()),
                        action.getOption(), action.getOption2()))){
                    foundActionToSubmit = true;
                    addCorpseAction(gd, dead, a.getCommand(), action.getOption2(), action.getOption3(), targets);
                    break;
                }
            }
            if(!foundActionToSubmit)
                addCorpseAction(gd, dead, Ability.VISIT, null, null, targets);
        }

        happening(gd, " reanimated ", dead);

        if(prevTargeted != null)
            prevTargeted.add(dead);
        action.markCompleted();
        gd.visit(dead);
    }

    private void addCorpseAction(Player gd, Player dead, String command, String option, String option2,
            PlayerList targets) {
        int ability = Ability.ParseAbility(command);
        if(dead.getActions().mainAction == null)
            dead.getActions().mainAction = new Action(dead, targets, ability, option, option2);
        else
            dead.getActions().extraActions.add(new Action(dead, targets, ability, option, option2));
        dead.setSubmissionTime(gd.getSubmissionTime());

    }

    public static final String COMMAND = "Dig";
    public static final int MAIN_ABILITY = Ability.DIG;

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        ArrayList<String> parsedCommands = new ArrayList<>();
        PlayerList targets = a.getTargets().copy();

        parsedCommands.add(COMMAND);
        for(Player target: targets){
            parsedCommands.add(target.getName());
        }
        if(a.getOption() != null)
            parsedCommands.add(a.getOption());
        if(a.getOption2() != null)
            parsedCommands.add(a.getOption2());
        if(a.getOption3() != null)
            parsedCommands.add(a.getOption3());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, ArrayList<String> commands) {
        commands.remove(0);
        PlayerList targets = new PlayerList();

        String name;
        Player pi;
        for(int i = 0; i < commands.size();){
            name = commands.get(0);
            pi = p.narrator.getPlayerByName(name);
            if(pi == null)
                break;
            targets.add(pi);
            commands.remove(0);
        }

        if(targets.isEmpty())
            throw new PlayerTargetingException("Need to target a corpse with this ability");

        Ability a;
        if(commands.isEmpty()){
            a = new Visit(p);
        }else{
            a = Ability.GetAbilityCopy(commands.get(0));
            if(a == null)
                a = new Visit(p);
            else
                commands.remove(0);
        }

        String option = a.getCommand();
        String option2, option3;

        if(!commands.isEmpty()){
            Faction t = Faction.GetFaction(commands, p.narrator);
            if(t != null)
                option2 = t.getColor();
            else
                option2 = commands.remove(0);
        }else{
            option2 = null;
            option3 = null;
        }

        if(!commands.isEmpty()){
            String t = Ability.GetRoleName(commands, p.narrator);
            if(t != null)
                option3 = t;
            else
                option3 = commands.remove(0);
        }else{
            option3 = null;
        }

        // need to account for 3rd option.
        return new Action(p, targets, MAIN_ABILITY, option, option2, option3);
    }

    public static boolean isBannedAction(int number) {
        for(int i: GraveDigger.BANNED_ACTIONS)
            if(i == number)
                return true;
        return false;
    }

    @Override
    public ArrayList<Option> getOptions(Player owner) {
        ArrayList<Option> aList = new ArrayList<>();
        ArrayList<String> vList = new ArrayList<>();
        Role role;
        for(FactionRole factionRole: owner.narrator.getPossibleMembers()){
            role = factionRole.role;
            for(Ability a: role.getBaseAbility()){
                if(a.isNightAbility(owner) && !vList.contains(a.getCommand()) && !isBannedAction(a.getAbilityNumber())){
                    aList.add(new Option(a.getCommand(), a.getClass().getSimpleName()));
                    vList.add(a.getCommand());
                }
            }
            if(role.contains(Vigilante.class) && !vList.contains(Gun.COMMAND))
                aList.add(new Option(Gun.COMMAND));
            if(role.contains(Baker.class) && !vList.contains(Bread.COMMAND))
                aList.add(new Option(Bread.COMMAND));
        }
        aList.add(new Option(Visit.COMMAND));
        return aList;
    }

    @Override
    public ArrayList<Option> getOptions2(Player p, String option1) {
        if(option1 == null)
            return new ArrayList<>();
        if(option1.equalsIgnoreCase(Armorsmith.COMMAND)){
            Ability a = new Armorsmith();
            a.modifiers = new Modifiers<>();
            a.modifiers.add(AbilityModifier.AS_FAKE_VESTS, true);
            return a.getOptions(p);
        }else if(option1.equalsIgnoreCase(DrugDealer.COMMAND))
            return new DrugDealer().getOptions(p);
        else if(option1.equalsIgnoreCase(Blacksmith.COMMAND)){
            Ability a = new Blacksmith();
            a.modifiers = new Modifiers<>();
            a.modifiers.add(AbilityModifier.GS_FAULTY_GUNS, true);
            return a.getOptions(p);
        }else if(option1.equalsIgnoreCase(Framer.COMMAND))
            return new Framer().getOptions(p);
        else if(option1.equalsIgnoreCase(Gunsmith.COMMAND)){
            Ability a = new Gunsmith();
            a.modifiers = new Modifiers<>();
            a.modifiers.add(AbilityModifier.GS_FAULTY_GUNS, true);
            return a.getOptions(p);
        }else if(option1.equalsIgnoreCase(Tailor.COMMAND)){
            return new Tailor().getOptions(p);
        }
        return new ArrayList<>();
    }

    @Override
    public ArrayList<Option> getOptions3(Player p, String option1, String option2) {
        if(option1 == null)
            return new ArrayList<>();
        if(option1.equalsIgnoreCase(Blacksmith.COMMAND)){
            Ability a = new Blacksmith();
            a.modifiers = new Modifiers<>();
            a.modifiers.add(AbilityModifier.AS_FAKE_VESTS, true);
            return a.getOptions2(p, option2);
        }else if(option1.equalsIgnoreCase(Tailor.COMMAND)){
            return new Tailor().getOptions2(p, option2);
        }
        return new ArrayList<>();
    }

    @Override
    public String getUsage(Player p, Random r) {
        if(p.narrator.getDeadPlayers().isEmpty())
            return "Can't use this until people die";
        Player live = p.narrator.getLivePlayers().getRandom(r);
        Player dead = p.narrator.getDeadPlayers().getRandom(r);

        return getCommand() + " *" + live.getName() + " " + dead.getName() + "*";
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(!p.narrator.getBool(SetupModifier.GD_REANIMATE)){
            ret.add("You may not target the same corpse twice.");
        }

        return ret;
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Grave Digger", GraveDigger.class);
        return FactionService.createFactionRole(faction, role);
    }
}
