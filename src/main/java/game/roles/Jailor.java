package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.ai.Controller;
import game.event.Feedback;
import game.event.Message;
import game.event.SelectionMessage;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.logic.support.action.Action;
import game.logic.support.attacks.DirectAttack;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.Modifier;
import game.logic.support.rules.SetupModifiers;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Jailor extends Ability implements DayResolveRole {

    public static final String COMMAND = "Jail";

    public static final String DRAGGED_MESSAGE = "You were dragged off to jail!";

    public Jailor() {
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public int remainingCleanings;

    @Override
    public Ability initialize(Player p) {
        remainingCleanings = modifiers.getOrDefault(AbilityModifier.JAILOR_CLEAN_ROLES, 0);
        return super.initialize(p);
    }

    @Override
    public boolean isInt(Modifier modifier) {
        if(modifier == AbilityModifier.JAILOR_CLEAN_ROLES)
            return true;
        return super.isInt(modifier);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Capture, talk to, and potentially execute someone in a jail cell each night.";
    public static final String NO_EXECUTE = "Jailed Targets";

    public static final int MAIN_ABILITY = Ability.JAIL;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public List<AbilityModifier> getAbilityModifiers() {
        List<AbilityModifier> rules = new LinkedList<>();
        rules.add(AbilityModifier.JAILOR_CLEAN_ROLES);
        rules.addAll(super.getAbilityModifiers());
        return rules;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public Action getExampleWitchAction(Player owner, Player target) {
        if(target.in(this.jailedTargets))
            return new Action(owner, getAbilityNumber(), target);
        return new Action(owner, MAIN_ABILITY, this.jailedTargets.getRandom(owner.narrator.getRandom()));
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.owner.narrator.isNight()){
            if(a.getTargets().size() > jailedTargets.size())
                Exception("Cannot execute someone that isn't jailed");
            if(getPerceivedCharges() != SetupModifiers.UNLIMITED)
                if(a.getTargets().size() > getPerceivedCharges())
                    Exception("Not enough executions for that many targets");
        }

    }

    @Override
    public void mainAbilityCheck(Action a) {
        // wrote this weird. what i want to do is check that there's no prev day lynch
        // then make sure that i'm trying to execute someone i have jailed.
        // however i only do the first check if i can validate that i have someone in
        // jail, and
        /// its the right person

        deadCheck(a);

        if(getPerceivedCharges() == 0 && a.owner.narrator.isNight())
            Exception("You cannot execute anymore.");

        if(a.owner.narrator.isNight()){ // executing
            if(a.getOption() != null){
                if(!Util.isInt(a.getOption()))
                    Exception("Option must be a positive integer.");

                int suggestedCleanings = Integer.parseInt(a.getOption());
                if(suggestedCleanings < 1)
                    Exception("Option must be a positive integer.");

                if(suggestedCleanings > remainingCleanings)
                    Exception("You may not clean that many people.");
            }

            if(a.getTarget() == null){
                Exception("Must target someone with this ability.");
            }

            Narrator n = a.owner.narrator;
            if(lynchedHappened(n))
                Exception("Cannot execute after a day execution");

            for(Player executeAttempt: a.getTargets())
                if(!executeAttempt.in(jailedTargets))
                    Exception("Cannot execute unjailed people");
        }
    }

    public static boolean lynchedHappened(Narrator n) {
        PlayerList lynchedPeople = n.getDeadList(n.getDayNumber());
        for(Player p: lynchedPeople){
            if(p.getDeathType().isLynch())
                return true;
        }
        return false;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return !p.isPuppeted();
    }

    @Override
    public void doNightAction(Action a) {
        int cleanedTargets;
        if(remainingCleanings > 0 && Util.isInt(a.getOption())){
            cleanedTargets = Integer.parseInt(a.getOption());
        }else
            cleanedTargets = 0;
        PlayerList targets = a.getTargets();
        boolean killedSomeone = false;
        for(Player target: targets){
            if(!jailedTargets.contains(target)){
                if(jailedTargets.isEmpty()){
                    NoNightActionVisit(a);
                    return;
                }
            }
            if(getRealCharges() != 0){
                killedSomeone = true;
                happening(a.owner, " executed ", target);
                if(targets.indexOf(target) < cleanedTargets){
                    target.setCleaned(target.getSkipper());
                    remainingCleanings--;
                }
                target.kill(new DirectAttack(a.owner, target, Constants.JAIL_KILL_FLAG));
                a.owner.visit(target);
                a.getAbility().useCharge();
            }
        }
        if(killedSomeone)
            a.markCompleted();

    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        if(a.owner.narrator.isDay())
            return super.getInsteadPhrase(a);
        ArrayList<Object> list = new ArrayList<>();

        list.add("excuting ");
        list.add(StringChoice.YouYourself(a.owner, a.getTargets()));

        return list;
    }

    @Override
    public String getDayCommand() {
        return COMMAND;
    }

    public PlayerList jailedTargets;

    public static final String CAPTOR = "Captor";

    public static final String JAIL = COMMAND;
    public static final String EXECUTE = "Execute";
    public static final int EXECUTE_ = MAIN_ABILITY;
    public static final int JAIL_ = MAIN_ABILITY;

    public static final String DEATH_FEEDBACK = "You were executed by a Jailor!";

    @Override
    public void doDayAction(Player jailor, PlayerList targets) {
        if(targets.size() != 1){
            Exception("Cannot jail more than one person at a time");
        }
        if(jailor.in(targets))
            Exception("Cannot jail self");
        if(jailor.getActions().isTargeting(targets, JAIL_))
            return;
        SelectionMessage e = new SelectionMessage(jailor, false, true);
        Action[] newOld = Action.pushCommand(e, new Action(jailor, targets, JAIL_, (String) null, null),
                jailor.getActions());
        jailor.narrator.getEventManager().addCommand(jailor, JAIL, targets.getFirst().getName());
        e.dontShowPrivate();
        jailor.sendMessage(e);

        jailor.narrator.addActionStack(newOld[0]);// new is 0
        jailor.narrator.removeActionStack(newOld[1]);// old is 1

        List<NarratorListener> listeners = jailor.narrator.getListeners();
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).onDayActionSubmit(jailor, newOld[0]);
    }

    @Override
    protected String getChargeText() {
        int perceivedCharges = getPerceivedCharges();
        if(perceivedCharges == SetupModifiers.UNLIMITED){
            return ("You can execute as many times as you want.");
        }else if(perceivedCharges == 1){
            return ("You have " + getPerceivedCharges() + " execution left.");
        }else{
            return ("You have " + getPerceivedCharges() + " executions left.");
        }
    }

    public void onSubmit(Action action) {
        // make selection message, add it to the jail chat, have it so only the receiver
        // can view this message

    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        if(actions.isEmpty())
            return list;
        Narrator n = actions.get(0).owner.narrator;
        PlayerList targets = getActionTargets(actions);
        if(n.isNight()){
            PlayerList cleaned = new PlayerList();
            Integer i;
            for(Action a: actions){
                if(Util.isInt(a.getOption())){
                    i = Integer.parseInt(a.getOption());
                    cleaned.add(a.getTargets().subList(0, i));
                }
            }
            if(cleaned.size() == targets.size()){
                list.add("clean and execute ");
                list.add(StringChoice.YouYourself(actions.get(0).owner, targets));
            }else{
                list.add(EXECUTE.toLowerCase());
                list.add(" ");
                list.add(StringChoice.YouYourself(actions.get(0).owner, targets));

                if(!cleaned.isEmpty()){
                    list.add(" and hide the role");
                    if(cleaned.size() > 1)
                        list.add("s");
                    list.add(" of ");
                    list.add(StringChoice.YouYourself(actions.get(0).owner, cleaned));
                }
            }
        }else{
            list.add(JAIL.toLowerCase());
            list.add(" ");
            list.addAll(StringChoice.YouYourself(actions.get(0).owner, targets));
        }
        return list;
    }

    @Override
    public int maxNumberOfSubmissions() {
        if(p.narrator.isDay())
            return Integer.MAX_VALUE;
        return 1;
    }

    @Override
    public void onNightStart(Player jailor) {
        super.onNightStart(jailor);
        jailedTargets = jailor.getActions().getTargets(MAIN_ABILITY);
        int breadToRemove = jailedTargets.size() - 1;
        for(int i = 0; i < breadToRemove; i++)
            jailor.getAbility(BreadAbility.class).useCharge();
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public void resolveDayAction(Action a) {
        happening(a.owner, " jailed ", a.getTarget());
        Message m = new Feedback(a.getTarget(), DRAGGED_MESSAGE).setPicture("jailor");
        if(!a.owner.getAvailableActions().isEmpty())
            m.addExtraInfo("You will not be able to perform any of your night actions.");
        m.addExtraInfo("Check your chats!");
        a.getTarget().sendMessage(m);
        a.owner.narrator.getEventManager().createJailChat(a);
    }

    @Override
    public void dayHappening(Action action) {
        happening(action.owner, " attempted to jail ", action.getTarget());
    }

    @Override
    public boolean isChatRole() {
        return false;
    }

    @Override
    public boolean hasCooldownAbility() {
        return false;
    }

    // null indicates skip it.
    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        if(!this.modifiers.hasKey(AbilityModifier.JAILOR_CLEAN_ROLES))
            return null;
        ArrayList<String> ret = new ArrayList<>();

        if(remainingCleanings == 1 && 1 == this.modifiers.getInt(AbilityModifier.JAILOR_CLEAN_ROLES)){
            ret.add("You may clean one execution.");
        }else if(remainingCleanings == 1){
            ret.add("You may clean one more execution.");
        }else if(remainingCleanings > 1){
            ret.add("You may clean " + remainingCleanings + " more execution.");
        }

        return ret;
    }

    @Override
    public boolean stopsParity(Player p) {
        if(p.isPuppeted())
            return false;
        return getRealCharges() != 0;
    }

    public static String KeyCreator(Controller jailed, int dayNumber) {
        return jailed.getName() + Constants.JAIL_CHAT_KEY_SEPERATOR + dayNumber;
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Jailor", Jailor.class);
    }
}
