package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;

public class FactionKill extends Ability {

    public static final String NIGHT_ACTION_DESCRIPTION = "Ability to kill at night.";

    private Faction t;

    @Override
    public Ability initialize(Faction t) {
        super.initialize(t);
        this.t = t;
        return this;
    }

    public FactionKill() {

    }

    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String COMMAND = "Kill";
    public static final int MAIN_ABILITY = Ability.NIGHT_KILL;

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null || t == null)
            return;

        a.markCompleted();
        Ability.Kill(owner, target, t.getKillFlag());
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.DIFFERENTIATED_FACTION_KILLS);
        return rules;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("killing ");
        list.add(StringChoice.YouYourselfSingle(a.owner, a.getTarget()));
        return list;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return Ability.NIGHT_KILL;
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
    }
}
