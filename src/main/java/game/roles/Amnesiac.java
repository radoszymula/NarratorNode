package game.roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import game.event.Feedback;
import game.logic.Faction;
import game.logic.MemberList;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Amnesiac extends Ability {

    public static final String COMMAND = "Remember";
    public static final String NIGHT_ACTION_DESCRIPTION = "Permanently change  your role and team to one in the graveyard.";

    public static final int MAIN_ABILITY = Ability.REMEMBER;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    // any changes must be reflected in getRuleTexsts as well! (next method)
    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.AMNESIAC_KEEPS_CHARGES);
        rules.add(SetupModifier.AMNESIAC_MUST_REMEMBER);
        return rules;
    }

    @Override
    public ArrayList<String> getPublicDescription(Narrator narrator, String actionOriginatorName, String color,
            AbilityList abilities) {
        ArrayList<String> ruleTextList = new ArrayList<String>();

        ruleTextList.add(SetupModifier.getDescription(narrator, SetupModifier.AMNESIAC_KEEPS_CHARGES,
                narrator.getBool(SetupModifier.AMNESIAC_KEEPS_CHARGES)));

        if(narrator.getBool(SetupModifier.AMNESIAC_MUST_REMEMBER) && narrator.getFaction(color) != null
                && narrator.getFaction(color).getEnemyColors().isEmpty()){
            ruleTextList.add(SetupModifier.getDescription(narrator, SetupModifier.AMNESIAC_MUST_REMEMBER,
                    narrator.getBool(SetupModifier.AMNESIAC_KEEPS_CHARGES)));
        }
        return ruleTextList;
    }

    public static final String LIVE_FEEDBACK = "You tried to remember who you were, but the person got up and walked away.";
    public static final String ROLE_NAME = "Amnesiac";

    @Override
    public void doNightAction(Action action) {
        Player target = action.getTarget();
        if(target == null)
            return;
        Player owner = action.owner;
        if(target.isAlive()){
            new Feedback(owner, LIVE_FEEDBACK).setPicture("amnesiac")
                    .addExtraInfo("Someone probably changed who you intended to target")
                    .addExtraInfo("Try again tomorrow night!");
            NoNightActionVisit(action);
            return;
        }
        if(owner.isEnforced()){
            NoNightActionVisit(action);
            return;
        }
        changeRoleAndTeam(owner, target.factionRole);
        happening(owner, " has become like ", target);
        Feedback fb = new Feedback(owner);
        fb.hideableFeedback = false;
        fb.setPicture("amnesiac");

        StringChoice sc = new StringChoice(target);
        sc.add(target, "You");
        fb.add(sc, " remembered you were like ", target, ".");

        action.markCompleted();
        owner.visit(target);
    }

    public static void changeRoleAndTeam(Player owner, FactionRole factionRole) {
        if(owner.getFaction().getEnemyColors().isEmpty()){
            owner.getFaction().removeMember(owner);
            owner.setTeam(owner.narrator.getFaction(factionRole.getColor()));
            owner.getFaction().addMember(owner);
        }
        owner.changeRole(factionRole);
    }

    @Override
    public void mainAbilityCheck(Action action) {
        aliveCheck(action);
        Player target = action.getTarget();
        if(target.hasUniqueRole())
            Exception("Can't target unique roles");
        Player owner = action.owner;
        MemberList mList = owner.narrator.getPossibleMembers();
        Faction originalTeam = owner.getFaction();

        Set<String> enemyTeams = originalTeam.getEnemyColors();
        if(enemyTeams.isEmpty())
            return;
        for(Faction t: mList.getFactions(target.getAbilities(), target.narrator)){
            if(!enemyTeams.contains(t.getColor())){// is an allied team
                return;
            }
        }

        Exception("Can't target roles that are only available on enemy teams.");
    }

    public ArrayList<Object> getInsteadText(Action a) {
        ArrayList<Object> list = new ArrayList<Object>();
        list.add("Instead of remembering you were like ");
        list.add(a.getTarget());
        return list;
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.AMNESIAC_KEEPS_CHARGES)){
            ret.add("You will get the number of charges as your target had, or 1, whichever is more.");
        }else{
            ret.add("You will only get to use one charge on the role that you pick up, if the role has limited charges.");
        }

        if(p.getFaction().getEnemyColors().isEmpty() && p.narrator.getBool(SetupModifier.AMNESIAC_MUST_REMEMBER)){
            ret.add("You must adopt another role card to win.");
        }

        return ret;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        PlayerList targets = getActionTargets(actions); // should just be 1

        list.add("remember they were like ");
        list.addAll(StringChoice.YouYourself(actions.get(0).owner, targets));

        return list;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public boolean chargeModifiable() {
        return false;
    }

    @Override
    public Boolean checkWinEnemyless(Player p) {
        if(p.narrator.getBool(SetupModifier.AMNESIAC_MUST_REMEMBER))
            return false;
        return null;
    }

    public static AbilityList AbilitySmash(Player p, AbilityList oldA, AbilityList newA) {
        ArrayList<Ability> list = new ArrayList<>();
        for(Ability a: newA){
            if(a instanceof ItemAbility)
                continue;
            list.add(a.newCopy());
        }
        for(Ability a: oldA){
            if(a instanceof ItemAbility)
                list.add(a);
        }

        newA = new AbilityList(list);
        for(Ability pa: newA){
            if(pa instanceof ItemAbility)
                continue;
            pa.initialize(p);
            if(pa.getRealCharges() != SetupModifiers.UNLIMITED)
                pa.modifiers.add(AbilityModifier.CHARGES, pa.getRealCharges());
        }
        return newA;
    }

    @Override
    public boolean canEditSelfTargetableModifer() {
        return false;
    }

    @Override
    public boolean canEditBackToBackModifier() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.narrator, "Amnesiac", Amnesiac.class);
        return FactionService.createFactionRole(faction, role);
    }
}
