package game.roles;

import java.util.List;

import game.event.DeathAnnouncement;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnsupportedMethodException;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.attacks.IndirectAttack;

public class Punch extends Ability {

    public static final String COMMAND = Constants.PUNCH;
    public static final int MAIN_ABILITY = Ability.PUNCH;

    private boolean usedPunchToday = false;

    @Override
    public String getCommand() {
        return Constants.PUNCH;
    }

    @Override
    public int getAbilityNumber() {
        return Ability.PUNCH;
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public void doNightAction(Action a) {
        throw new UnsupportedMethodException();
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return null;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        if(a.owner.isSilenced())
            throw new PlayerTargetingException("You can't punch people because you're silenced.");
        deadCheck(a);
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return "Attacks someone, with a random chance of it succeeding.";
    }

    @Override
    public PlayerList getAcceptableTargets(Player puncher) {
        return puncher.narrator.getLivePlayers().remove(puncher);
    }

    @Override
    public boolean canUseDuringDay(Player player) {
        return !usedPunchToday && !player.isSilenced() && !player.isPuppeted() && player.narrator.isDay();
    }

    @Override
    public void doDayAction(Player puncher, PlayerList targets) {
        this.usedPunchToday = true;
        punch(puncher, targets.getFirst(), null);
    }

    public static void punch(Player puncher, Player target, Player ventController) {
        Narrator narrator = puncher.narrator;
        int punchRating = puncher.getPunchRating();

        boolean success;
        if(punchRating == 0){
            success = false;
        }else if(punchRating == 100){
            success = true;
        }else{
            success = (narrator.getRandom().nextDouble() * 100) < punchRating;
        }

        if(target.isInvulnerable())
            success = false;

        DeathAnnouncement e = new DeathAnnouncement(target);
        e.setPicture("armorsmith");

        if(success)
            e.add("Crack! ", puncher, "'s mean right hook knocked out ", target, ". For good...");
        else
            e.add("Swing! ", puncher, " was too slow for ", target, "'s quick moves. Punch evaded!");

        if(ventController == null)
            narrator.getEventManager().addCommand(puncher, Constants.PUNCH, target.getName());
        else
            narrator.getEventManager().addCommand(ventController, Ventriloquist.VENT_PUNCH, puncher.getName(),
                    target.getName());

        narrator.announcement(e);

        PlayerList deadPeople = new PlayerList();
        if(success){
            target.setDead(puncher);
            target.getDeathType().addDeath(Constants.PUNCH_KILL_FLAG);
            deadPeople.add(target);
        }

        DeathAnnouncement explosion = null;

        if((target.is(ElectroManiac.class) || target.isCharged())
                && (puncher.isCharged() || puncher.is(ElectroManiac.class))){
            if(!puncher.isInvulnerable() && !puncher.is(ElectroManiac.class)){
                boolean overrideChecks = true;
                puncher.dayKill(new IndirectAttack(Constants.ELECTRO_KILL_FLAG, puncher, puncher.getCharger()),
                        overrideChecks);
                deadPeople.add(puncher);
            }
            if(!target.isInvulnerable() && !target.is(ElectroManiac.class))
                target.getDeathType().addDeath(Constants.ELECTRO_KILL_FLAG);
            explosion = Burn.ExplosionAnnouncement(narrator, deadPeople, "electromaniac");
            narrator.announcement(explosion);
        }

        narrator.voteSystem.removeFromVotes(deadPeople);
        narrator.checkProgress();
        if(!narrator.isInProgress())
            return;
        narrator.checkVote();

        if(!success)
            target = null;
        if(narrator.isInProgress()){
            List<NarratorListener> listeners = narrator.getListeners();
            for(int i = 0; i < listeners.size(); i++){
                listeners.get(i).onAssassination(puncher, target, e);
                if(explosion != null)
                    listeners.get(i).onElectroExplosion(deadPeople, explosion);
            }

            if(narrator.isDay())
                narrator.parityCheck();
        }
    }

    @Override
    public String getUsage(Player p, Random r) {
        return Ability.getDefaultUsage(this, p, r);
    }

    @Override
    public boolean stopsParity(Player p) {
        return canUseDuringDay(p);
    }

    @Override
    public void onDayStart(Player p) {
        this.usedPunchToday = false;
    }

}
