package game.roles;

import game.logic.Narrator;
import game.logic.Faction;
import game.logic.exceptions.UnsupportedMethodException;
import game.logic.support.Constants;
import game.logic.support.action.Action;

public class CultPendingInvitation extends Ability {

    @Override
    public String getCommand() {
        return Constants.RECRUITMENT;
    }

    @Override
    public int getAbilityNumber() {
        return Ability.INVITATION_ABILITY;
    }

    @Override
    public boolean supportedFactionAbility() {
        return false;
    }

    @Override
    public void doNightAction(Action a) {
        throw new UnsupportedMethodException();
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return null;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        // TODO Auto-generated method stub
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        for(Faction t: n.getFactions())
            if(t.has(CultLeader.class))
                return "accept recruitment invitation to the " + t.getName();
        return null;
    }

}
