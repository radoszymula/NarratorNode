package game.roles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import game.logic.Faction;
import game.logic.Role;
import models.FactionRole;

public class FactionHidden extends Hidden {

    private Faction t;

    public FactionHidden(Faction t) {
        super(t.getName() + " Hidden", t.narrator);
        this.t = t;
    }

    @Override
    public List<FactionRole> getFactionRoles() {
        List<FactionRole> roles = new ArrayList<>();
        roles.addAll(t._roleSet.values());
        return roles;
    }

    @Override
    public boolean contains(Role m) {
        return t._roleSet.containsKey(m);
    }

    @Override
    public int getAlignmentCount() {
        return 1;
    }

    @Override
    public String getName() {
        return t.getName() + " Hidden";
    }

    @Override
    public void removeFactionRole(FactionRole m) {

    }

    @Override
    public Set<String> getColors() {
        Set<String> set = new HashSet<>();
        set.add(t.getColor());
        return set;
    }

}
