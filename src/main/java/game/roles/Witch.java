package game.roles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.event.Happening;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.logic.support.rules.SetupModifier;
import game.roles.support.Gun;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class Witch extends Ability {

    public static final int MAIN_ABILITY = Ability.CONTROL;

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public int getAbilityNumber() {
        return MAIN_ABILITY;
    }

    @Override
    public String getAbilityDescription(Narrator n) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String[] getRoleInfo(Narrator n) {
        return ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public List<SetupModifier> getSetupModifiers() {
        List<SetupModifier> rules = new LinkedList<>();
        rules.add(SetupModifier.WITCH_FEEDBACK);
        return rules;
    }

    @Override
    public String getNightText(Player p) {
        return "Type " + SQuote(COMMAND + " name1 name2") + " to make the first person target the second.";
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Manipulate someone else's action target to do your bidding.";

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();
        Action a = actionList.get(0);
        Player owner = a.owner;

        for(Action da: actionList){
            Player victim = da._targets.getFirst();
            Player newTarget = da._targets.getLast();
            // String message;

            if(victim != null && newTarget != null){
                list.add("make ");
                list.add(StringChoice.YouYourselfSingle(owner, victim));
                list.add(" target ");
                list.add(StringChoice.YouYourselfSingle(owner, newTarget));
            }else if(newTarget != null){
                list.add("make the victim target ");
                list.add(StringChoice.YouYourselfSingle(owner, newTarget));
            }else if(victim != null){
                list.add("control ");
                list.add(StringChoice.YouYourselfSingle(owner, victim));
            }else{
                list.add("not control anyone tonight");
            }
            list.add(" and ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    public static final String WITCH_FEEDBACK = "Your intended action was manipulated last night!";

    @Override
    public void mainAbilityCheck(Action a) {
        deadCheck(a);
        if(a.getTargets().getFirst() == a.owner)
            Exception("You can't control yourself.");
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() != 2)
            Exception("This ability requires a target and an target-target");
    }

    @Override
    public void doNightAction(Action witchAction) {
        Player witch = witchAction.owner;
        if(witchAction._targets.isEmpty())
            return;
        Player victim = witchAction._targets.getFirst();
        if(witchAction._targets.size() < 2){
            NoNightActionVisit(witchAction);
            return;
        }

        ActionList victimActions = victim.getActions();
        Player newTarget = witchAction._targets.getLast();
        Narrator n = witch.narrator;

        new Happening(n).add(witch, " changed ", victim, "\'s target to ", newTarget, ".");

        ArrayList<Action> actions = new ArrayList<>();
        for(Action victimAction: victimActions)
            if(!victimAction.wasChanged(witch) && !victimAction.is(Faction.SEND))
                actions.add(victimAction);

        ArrayList<Ability> abilities = victim.getAvailableActions();
        Collections.sort(abilities, new Comparator<Ability>() {
            @Override
            public int compare(Ability arg0, Ability arg1) {
                return arg1.getAbilityNumber() - arg0.getAbilityNumber();
            }
        });

        for(Ability ability: abilities){
            if(ability.is(Spy.MAIN_ABILITY))
                continue;
            if(ability.is(FactionSend.MAIN_ABILITY))
                continue;
            if(ability.is(Jailor.MAIN_ABILITY) && victim.getAction(Jailor.MAIN_ABILITY) != null){
                if(!PlayerList.SameShuffle(((Jailor) ability).jailedTargets,
                        victim.getAction(Jailor.MAIN_ABILITY).getTargets())){
                    victim.getAction(Jailor.MAIN_ABILITY).witchTouch(witch).setTarget(newTarget);
                    witchAction.markCompleted();
                    witch.visit(victim);
                    return;
                }
                break;
            }

            if(victimActions.canAddAnotherAction(ability.getAbilityNumber(), newTarget)){
                if(jokerKillAdded(witch, victim, newTarget, victimActions)){
                    witchAction.markCompleted();
                    witch.visit(victim);
                    return;
                }

                Action newAction = victim.getActions()
                        .addAction(ability.getExampleWitchAction(victim, newTarget).witchTouch(witch))
                        .setNotSubmittedByOwner();
                if(!newAction.isTeamAbility()){
                    if(ability.is(Gun.MAIN_ABILITY))
                        victim.secondaryCause(witch);
                    witchAction.markCompleted();
                    witch.visit(victim);
                    return;
                }
                for(Faction t: newAction.getFactions()){
                    if(t.getFactionAbilityController(newAction.ability) == victim){
                        witchAction.markCompleted();
                        witch.visit(victim);
                        return;
                    }
                }
                for(Faction t: newAction.getFactions()){
                    if(t.getFactionAbilityController(newAction.ability) == null){
                        t.overrideSenderViaWitch(victim, newAction.ability);
                        witchAction.markCompleted();
                        witch.visit(victim);
                        return;
                    }
                }
                Util.log(new NarratorException(), "Witch wasn't able to manipulate action.");
            }
        }

        Collections.sort(actions, new Comparator<Action>() {
            @Override
            public int compare(Action arg0, Action arg1) {
                return arg1.ability - arg0.ability;
            }
        });

        for(Action action: actions){
            if(action.is(Jailor.MAIN_ABILITY)){
                if(PlayerList.SameShuffle(((Jailor) action.getAbility()).jailedTargets, action.getTargets()))
                    continue;
            }
            if(action.is(Joker.MAIN_ABILITY) && action.getTargets().size() == 1 && action.getOption() == null){
                continue;
            }
            if(action.is(Gun.MAIN_ABILITY))
                victim.secondaryCause(witch);
            action.setTarget(newTarget);
            witchAction.markCompleted();
            witch.visit(victim);
            return;
        }

        Action visitAction = victim.action(newTarget, Ability.VISIT);
        if(victimActions.canAddAnotherAction(visitAction)){
            victimActions.addAction(visitAction.witchTouch(witch));
            witchAction.markCompleted();
            witch.visit(victim);
        }
    }

    private boolean jokerKillAdded(Player witch, Player victim, Player newTarget, ActionList victimActions) {
        if(!victim.is(Joker.class))
            return false;
        Joker card = victim.getAbility(Joker.class);

        int submittedKills = 0;
        for(Action a: victimActions){
            if(!a.is(Joker.COMMAND))
                continue;
            if(a.getOption() == null && a.getTargets().size() == 1)
                continue;
            submittedKills += a.getTargets().size();
            if(a.getOption() != null)
                submittedKills--;
        }

        if(card.bounty_kills <= submittedKills)
            return false;

        victimActions.addAction(new Action(victim, newTarget, Joker.MAIN_ABILITY, Joker.NO_BOUNTY)).witchTouch(witch);

        return true;
    }

    public static void ManipulatedFeedback(Player... p) {
        for(Player x: p)
            new Feedback(x, Witch.WITCH_FEEDBACK).setPicture("witch");
    }

    public static final String COMMAND = "Control";

    @Override
    protected ArrayList<String> getSpecificRoleSpecs(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.narrator.getBool(SetupModifier.WITCH_FEEDBACK)){
            ret.add("Targeting people will give them 'witch' feedback");
        }else{
            ret.add("Targets will be unaware that you targeted them.");
        }
        return ret;
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        return p.narrator.getLivePlayers().sortByName();
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public String getUsage(Player p, Random r) {
        PlayerList live = p.narrator.getLivePlayers().remove(p);
        live.shuffle(r, null);
        return getCommand() + " *" + live.getFirst().getName() + " " + live.getLast().getName() + "*";
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    @Override
    public String getSelfTargetText() {
        String notText;
        if(canSelfTarget(narrator))
            notText = "";
        else
            notText = " not";

        if(p.getAbilities().filterKnown().size() == 1)
            return "You may" + notText + " cause self-targets.";
        return "You may" + notText + " use your " + getClass().getSimpleName() + " ability to cause self-targets.";
    }

    @Override
    public String getSelfTargetMemberDescription(boolean singleAbility, boolean canSelfTarget) {
        if(singleAbility && canSelfTarget)
            return "May cause self-targets.";
        if(!singleAbility && canSelfTarget)
            return "May cause self-targets with the " + getClass().getSimpleName() + " ability.";
        if(singleAbility && !canSelfTarget)
            return "May not cause self-targets.";
        if(!singleAbility && !canSelfTarget){
            return "May not cause self-targets with the " + getClass().getSimpleName() + " ability.";
        }
        return "";
    }

    @Override
    public void selfTargetableCheck(Action a) {
        PlayerList targets = a._targets;
        if(targets.getFirst() != targets.getLast())
            return;
        if(!canSelfTarget(a.owner.narrator))
            Exception("You may not cause self-targets.");
    }

    public static FactionRole template(Faction faction) {
        return FactionService.createFactionRole(faction, template(faction.narrator));
    }

    public static Role template(Narrator narrator) {
        return RoleService.createRole(narrator, "Witch", Witch.class);
    }

}
