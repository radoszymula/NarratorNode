package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.roles.Agent;
import game.roles.Blackmailer;
import game.roles.Citizen;
import game.roles.Detective;
import game.roles.Doctor;
import game.roles.Goon;
import game.roles.Hidden;
import game.roles.Jester;
import game.roles.Lookout;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.Stripper;
import models.FactionRole;
import services.HiddenService;

public class VanillaE extends Setup {

    public static final String KEY = "vanillae";

    public VanillaE(Narrator narrator) {
        Faction town = Town(narrator);
        Faction maf = Mafia(narrator);
        Faction psycho = Killer(narrator);
        Faction benign = Benign(narrator);

        Prioritize(psycho, maf, town, benign);
        town.addSheriffDetectableTeam(maf);
        town.setEnemies(maf, psycho);
        maf.setEnemies(psycho);

        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.FOLLOW_GETS_ALL, false);
        narrator.setRule(SetupModifier.BLOCK_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_SUCCESS_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_FEEDBACK, false);
        narrator.setRule(SetupModifier.JESTER_KILLS, 0);
        narrator.setRule(SetupModifier.JESTER_CAN_ANNOY, false);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        FactionRole stripper = Stripper.template(town);
        FactionRole sheriff = Sheriff.template(town);
        FactionRole doctor = Doctor.template(town);
        FactionRole lookout = Lookout.template(town);
        FactionRole detective = Detective.template(town);
        FactionRole cit = Citizen.template(town);
        Hidden townPower = HiddenService.createHidden(narrator, "Hidden Town", stripper, sheriff, doctor, lookout,
                detective);

        FactionRole agent = Agent.template(maf);
        FactionRole bm = Blackmailer.template(maf);
        Hidden mafiaPower = HiddenService.createHidden(narrator, "Hidden Mafia", agent, bm);

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        FactionRole goon = Goon.template(maf);

        FactionRole sk = SerialKiller.template(psycho);

        FactionRole jester = Jester.template(benign);

        addInitialRole(cit, 3);
        addInitialRole(townPower, 2);
        addInitialRole(mafiaPower, 2);
        addInitialRole(jester);

        addSetupChange(9, cit);

        addSetupChange(10, sk, jester, cit);
        addSetupChange(11, jester);

        addSetupChange(12, goon, mafiaPower, goon);
        addSetupChange(13, goon, townPower, mafiaPower);

        addSetupChange(14, cit);
        addSetupChange(15, jester);

        addSetupChange(16, goon, cit, mafiaPower);
    }

    @Override
    public String getShortDescription() {
        return "Basic game with 3rd party roles";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "New Neutral Roles - Serial Killer and Jester", "Minimum of 8 players"
        };
    }

    @Override
    public String getName() {
        return "Intro to the 3rd Party";
    }

    @Override
    public boolean isPreset() {
        return true;
    }

}
