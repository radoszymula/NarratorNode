package game.setups;

import game.logic.Faction;
import game.logic.FactionList;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Agent;
import game.roles.Amnesiac;
import game.roles.Architect;
import game.roles.Armorsmith;
import game.roles.Assassin;
import game.roles.Baker;
import game.roles.Blackmailer;
import game.roles.Blacksmith;
import game.roles.Bodyguard;
import game.roles.Bomb;
import game.roles.Bulletproof;
import game.roles.Burn;
import game.roles.Citizen;
import game.roles.Clubber;
import game.roles.Commuter;
import game.roles.Coroner;
import game.roles.Coward;
import game.roles.CultLeader;
import game.roles.Cultist;
import game.roles.Detective;
import game.roles.Disfranchise;
import game.roles.Disguiser;
import game.roles.Doctor;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.Elector;
import game.roles.ElectroManiac;
import game.roles.Enforcer;
import game.roles.Executioner;
import game.roles.Framer;
import game.roles.Ghost;
import game.roles.Godfather;
import game.roles.Goon;
import game.roles.GraveDigger;
import game.roles.Gunsmith;
import game.roles.Hidden;
import game.roles.Interceptor;
import game.roles.Investigator;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Jester;
import game.roles.Joker;
import game.roles.Lookout;
import game.roles.Marshall;
import game.roles.Mason;
import game.roles.MasonLeader;
import game.roles.MassMurderer;
import game.roles.Mayor;
import game.roles.Miller;
import game.roles.Operator;
import game.roles.ParityCheck;
import game.roles.Poisoner;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.Silence;
import game.roles.Sleepwalker;
import game.roles.Snitch;
import game.roles.Spy;
import game.roles.Stripper;
import game.roles.Survivor;
import game.roles.Tailor;
import game.roles.Ventriloquist;
import game.roles.Veteran;
import game.roles.Vigilante;
import game.roles.Witch;
import models.FactionRole;
import services.FactionRoleService;
import services.FactionService;
import services.HiddenFactionRoleService;
import services.HiddenService;
import services.RoleAbilityModifierService;
import services.RoleModifierService;
import services.RoleService;

public class Default extends Setup {

    public static final String KEY = "custom";

    public Default(Narrator narrator) {
        Faction benigns = Benign(narrator);
        Faction town = Town(narrator);
        Faction outcasts = Outcast(narrator);
        Faction cult = Cult(narrator);
        Faction mafia = Mafia(narrator, 1);
        Faction yakuza = Mafia(narrator, 2);
        Faction threat = Killer(narrator);

        Prioritize(threat, new FactionList(mafia, yakuza), cult, outcasts, town, benigns);

        town.addSheriffDetectableTeam(mafia, yakuza, cult, threat);

        town.setEnemies(outcasts, mafia, yakuza, cult, threat);
        mafia.setEnemies(yakuza, cult, threat);
        yakuza.setEnemies(cult, threat);
        cult.setEnemies(threat);

        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.FOLLOW_GETS_ALL, false);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        Role architectRole = Architect.template(narrator);
        Role busDriverRole = Driver.template(narrator);
        Role jailorRole = Jailor.template(narrator);
        RoleAbilityModifierService.modify(jailorRole, Jailor.class, AbilityModifier.CHARGES, 3);
        Role operatorRole = Operator.template(narrator);
        Role stripperRole = Stripper.template(narrator);

        FactionRole vigi = Vigilante.template(town);
        RoleAbilityModifierService.modify(vigi.role, Vigilante.class, AbilityModifier.CHARGES, 2);
        FactionRole vet = Veteran.template(town);
        RoleAbilityModifierService.modify(vet.role, Veteran.class, AbilityModifier.CHARGES, 3);

        Role marshallRole = RoleService.createRole(narrator, "Marshall", Marshall.class);
        RoleModifierService.modify(narrator, marshallRole, RoleModifier.UNIQUE, true);
        RoleAbilityModifierService.modify(marshallRole, Marshall.class, AbilityModifier.CHARGES, 1);
        FactionRole marshall = FactionService.createFactionRole(town, marshallRole);

        Role commuterRole = RoleService.createRole(narrator, "Commuter", Commuter.class);
        RoleAbilityModifierService.modify(commuterRole, Commuter.class, AbilityModifier.CHARGES, 3);
        FactionRole commuter = FactionService.createFactionRole(town, commuterRole);

        Role masonLeaderRole = RoleService.createRole(narrator, "Mason Leader", MasonLeader.class);
        masonLeaderRole.addChargeModifier(4);
        masonLeaderRole.addModifier(RoleModifier.UNIQUE, true);
        FactionRole masonLeader = FactionService.createFactionRole(town, masonLeaderRole);

        FactionRole architectTown = FactionService.createFactionRole(town, architectRole);
        FactionRole armorsmith = Armorsmith.template(town);
        FactionRole baker = Baker.template(town);
        FactionRole citizen = Citizen.template(town);
        FactionRole driverTown = FactionService.createFactionRole(town, busDriverRole);
        FactionRole bodyguard = Bodyguard.template(town);
        FactionRole bulletproof = Bulletproof.template(town);
        FactionRole clubber = Clubber.template(town);
        FactionRole coroner = Coroner.template(town);
        FactionRole detective = Detective.template(town);
        FactionRole doctor = Doctor.template(town);
        FactionRole enforcer = Enforcer.template(town);
        FactionRole gunsmith = Gunsmith.template(town);
        FactionRole jailor = FactionService.createFactionRole(town, jailorRole);
        FactionRole lookout = Lookout.template(town);
        FactionRole mayor = Mayor.template(town);
        FactionRole mason = Mason.template(town);
        FactionRole miller = Miller.template(town);
        FactionRole operator = FactionService.createFactionRole(town, operatorRole);
        FactionRole parityCop = ParityCheck.template(town);
        FactionRole sheriff = Sheriff.template(town);
        FactionRole sleepwalker = Sleepwalker.template(town, citizen);
        FactionRole snitch = Snitch.template(town);
        FactionRole spy = Spy.template(town);
        FactionRole stripperTown = FactionService.createFactionRole(town, stripperRole);

        Hidden townRandom = HiddenService.createHidden(narrator, Constants.TOWN_RANDOM_ROLE_NAME, architectTown, baker,
                bodyguard, bulletproof, citizen, clubber, commuter, coroner, detective, doctor, driverTown, enforcer,
                lookout, miller, parityCop, sheriff, sleepwalker, snitch, spy, operator, stripperTown, armorsmith, vigi,
                vet, gunsmith, jailor, mayor, masonLeader, marshall, mason);

        HiddenService.createHidden(narrator, Constants.TOWN_KILLING_ROLE_NAME, vigi, bodyguard, jailor, vet, gunsmith);

        HiddenService.createHidden(narrator, Constants.TOWN_INVESTIGATIVE_ROLE_NAME, sheriff, lookout, coroner,
                detective, spy);

        HiddenService.createHidden(narrator, Constants.TOWN_GOVERNMENT_ROLE_NAME, mayor, marshall, masonLeader,
                architectTown, baker, snitch, mason, enforcer, clubber);

        HiddenService.createHidden(narrator, Constants.TOWN_PROTECTIVE_ROLE_NAME, doctor, driverTown, operator,
                stripperTown, armorsmith, bodyguard);

        Role gfRole = RoleService.createRole(narrator, "Godfather", Godfather.class, Bulletproof.class);
        gfRole.addModifier(RoleModifier.UNDETECTABLE, true);
        gfRole.addModifier(RoleModifier.UNIQUE, true);
        FactionRole godfather = FactionService.createFactionRole(mafia, gfRole);

        FactionRole tailor = Tailor.template(mafia);
        tailor.role.addChargeModifier(3);

        FactionRole coward = Coward.template(mafia);
        coward.role.addChargeModifier(3);

        FactionRole disguiser = Disguiser.template(mafia);
        disguiser.role.addChargeModifier(1);

        FactionRole[] mafiaRoles = new FactionRole[] {
                Assassin.template(mafia), Agent.template(mafia), Blackmailer.template(mafia),
                Disfranchise.template(mafia), DrugDealer.template(mafia), Framer.template(mafia), Goon.template(mafia),
                Investigator.template(mafia), Janitor.template(mafia), Silence.template(mafia),

                godfather, tailor, coward, disguiser,

                FactionService.createFactionRole(mafia, architectRole), FactionService.createFactionRole(mafia, busDriverRole),
                FactionService.createFactionRole(mafia, jailorRole), FactionService.createFactionRole(mafia, stripperRole)
        };

        Hidden maf_random = HiddenService.createHidden(narrator, Constants.MAFIA_RANDOM_ROLE_NAME, mafiaRoles);

        FactionRole[] yakuzaRoles = new FactionRole[mafiaRoles.length];
        for(int i = 0; i < mafiaRoles.length; i++)
            yakuzaRoles[i] = FactionService.createFactionRole(yakuza, mafiaRoles[i].role);

        Hidden yak_random = HiddenService.createHidden(narrator, Constants.YAKUZA_RANDOM_ROLE_NAME, yakuzaRoles);

        FactionRole surv = Survivor.template(benigns);
        surv.role.addChargeModifier(4);

        FactionRole exec = Executioner.template(benigns);

        Hidden neut_benign = HiddenService.createHidden(narrator, Constants.NEUTRAL_BENIGN_RANDOM_ROLE_NAME,
                Amnesiac.template(benigns), Ghost.template(benigns), Jester.template(benigns), exec, surv);

        Role blacksmithRole = RoleService.createRole(narrator, "Blacksmith", Blacksmith.class);
        FactionRole evilBlacksmith = FactionService.createFactionRole(outcasts, blacksmithRole);
        FactionRoleService.addModifier(evilBlacksmith, AbilityModifier.AS_FAKE_VESTS, Blacksmith.class, true);
        FactionRoleService.addModifier(evilBlacksmith, AbilityModifier.GS_FAULTY_GUNS, Blacksmith.class, true);

        Role cultist = RoleService.createRole(narrator, "Cultist", Cultist.class);
        Role cLeader = RoleService.createRole(narrator, "Cult Leader", CultLeader.class);
        cLeader.addModifier(RoleModifier.UNIQUE, true);
        cLeader.addCooldownModifier(CultLeader.class, 1);

        Hidden neut_evil = HiddenService.createHidden(narrator, Constants.NEUTRAL_EVIL_RANDOM_ROLE_NAME,
                Elector.template(outcasts), GraveDigger.template(outcasts), Interceptor.template(outcasts),
                Ventriloquist.template(outcasts), Witch.template(outcasts),

                evilBlacksmith, FactionService.createFactionRole(cult, cultist), FactionService.createFactionRole(cult, cLeader),
                FactionService.createFactionRole(outcasts, operatorRole));

        Hidden neut_killing = HiddenService.createHidden(narrator, Constants.NEUTRAL_KILLING_RANDOM_ROLE_NAME,
                SerialKiller.template(threat), Burn.template(threat), MassMurderer.template(threat),
                Poisoner.template(threat), ElectroManiac.template(threat), Joker.template(threat));

        Hidden neut_random = HiddenService.createHidden(narrator, Constants.NEUTRAL_RANDOM_ROLE_NAME);
        HiddenFactionRoleService.addSpawnableRoles(narrator, neut_random, neut_killing.getFactionRoles());
        HiddenFactionRoleService.addSpawnableRoles(narrator, neut_random, neut_benign.getFactionRoles());
        HiddenFactionRoleService.addSpawnableRoles(narrator, neut_random, neut_evil.getFactionRoles());

        Hidden any_rand = HiddenService.createHidden(narrator, Constants.ANY_RANDOM_ROLE_NAME);
        HiddenFactionRoleService.addSpawnableRoles(narrator, any_rand, townRandom.getFactionRoles());
        HiddenFactionRoleService.addSpawnableRoles(narrator, any_rand, maf_random.getFactionRoles());
        HiddenFactionRoleService.addSpawnableRoles(narrator, any_rand, yak_random.getFactionRoles());
        HiddenFactionRoleService.addSpawnableRoles(narrator, any_rand, neut_random.getFactionRoles());

        Bomb.template(town);
        FactionService.createFactionRole(town, blacksmithRole);

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);
    }

    @Override
    public String getShortDescription() {
        return "";
    }

    @Override
    public String[] getDescription() {
        return new String[0];
    }

    @Override
    public String getName() {
        return "Custom";
    }
}
