package game.setups;

import java.util.ArrayList;
import java.util.List;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Citizen;
import models.FactionRole;
import models.enums.VoteSystemTypes;
import services.FactionService;
import util.HiddenUtil;

public class FreeForAll extends Setup {

    public static final String KEY = "ffa";

    public FreeForAll(Narrator narrator) {
        List<Faction> factions = new ArrayList<>();
        List<FactionRole> factionRoles = new ArrayList<>();
        String color;
        Faction faction, otherFaction;
        Role role = Citizen.template(narrator);
        FactionRole factionRole;
        role.addModifier(RoleModifier.HIDDEN_FLIP, true);
        for(int i = 0; i < 20; i++){
            color = getColor(i);
            faction = FactionService.createFaction(narrator, color, "Team" + (i + 1));
            faction.setKnowsTeam(false);

            for(int j = 0; j < factions.size(); j++){
                otherFaction = factions.get(j);
                faction.setEnemies(otherFaction);
            }
            factions.add(faction);
            factionRole = FactionService.createFactionRole(faction, role);
            factionRoles.add(factionRole);
            HiddenUtil.createHiddenSingle(narrator, factionRole);
        }

        narrator.setRule(SetupModifier.SKIP_VOTE, false);
        narrator.setRule(SetupModifier.DISCUSSION_LENGTH, 999);
        narrator.setRule(SetupModifier.DAY_LENGTH, 999);
        narrator.setRule(SetupModifier.NIGHT_LENGTH, 999);
        narrator.setRule(SetupModifier.TRIAL_LENGTH, 999);
        narrator.setRule(SetupModifier.VOTE_SYSTEM, VoteSystemTypes.DIMINISHING_POOL);
        narrator.setRule(SetupModifier.DAY_START, true);
        narrator.setRule(SetupModifier.LAST_WILL, false);
        narrator.setRule(SetupModifier.SELF_VOTE, true);
        narrator.setRule(SetupModifier.SECRET_VOTES, true);

        init(narrator, KEY);

        addInitialRole(factionRoles.get(0));
        addInitialRole(factionRoles.get(1));
        addInitialRole(factionRoles.get(2));

        for(int i = 3; i < 20; i++){
            addSetupChange(i + 1, factionRoles.get(i));
        }
    }

    @Override
    public String getShortDescription() {
        return getDescription()[0];
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "Free for all setup.  Be the last man or woman standing."
        };
    }

    @Override
    public String getName() {
        return "FFA setup";
    }

    @Override
    public boolean isPreset() {
        return true;
    }

    private static String getColor(int offset) {
        long colorInt = Long.parseLong(Constants.GREEN.substring(1), 16);
        colorInt -= offset;
        String str = Long.toHexString(colorInt).toUpperCase();
        while (str.length() < 6){
            str = "0" + str;
        }
        return "#" + str;

    }
}
