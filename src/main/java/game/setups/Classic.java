package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.roles.Citizen;
import game.roles.Doctor;
import game.roles.Goon;
import game.roles.Sheriff;
import models.FactionRole;

public class Classic extends Setup {

    public static final String KEY = "classic";

    public Classic(Narrator narrator) {
        Faction town = Town(narrator);
        Faction mafia = Mafia(narrator);

        Prioritize(mafia, town);
        town.setEnemies(mafia);

        town.addSheriffDetectableTeam(mafia);

        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.HEAL_SUCCESS_FEEDBACK, true);
        narrator.setRule(SetupModifier.HEAL_FEEDBACK, true);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        FactionRole sheriff = Sheriff.template(town);
        FactionRole citizen = Citizen.template(town);
        FactionRole doctor = Doctor.template(town);

        FactionRole goon = Goon.template(mafia);

        addInitialRole(citizen, 3);
        addInitialRole(sheriff);
        addInitialRole(doctor);
        addInitialRole(goon, 2);

        addSetupChange(8, citizen);
        addSetupChange(9, citizen);
        addSetupChange(10, goon);

        addSetupChange(11, citizen);
        addSetupChange(12, citizen);

        addSetupChange(13, goon);
        addSetupChange(14, citizen);

        addSetupChange(15, goon, citizen, sheriff);
        addSetupChange(16, citizen);
    }

    @Override
    public String getShortDescription() {
        return getDescription()[0];
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "The classic game of Mafia you know and love"
        };
    }

    @Override
    public String getName() {
        return "Classic";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
