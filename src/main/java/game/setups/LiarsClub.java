package game.setups;

import game.logic.Faction;
import game.logic.FactionList;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Citizen;
import game.roles.Disfranchise;
import game.roles.Doctor;
import game.roles.Goon;
import game.roles.Sheriff;
import game.roles.Silence;
import game.roles.Stripper;
import game.roles.Thief;
import models.FactionRole;
import models.enums.VoteSystemTypes;
import services.FactionService;
import services.RoleService;

public class LiarsClub extends Setup {

    public static final String KEY = "liars";

    public LiarsClub(Narrator narrator) {
        Faction town = Town(narrator);
        Faction mafia = Mafia(narrator);
        Faction prostituteTempTeam = Outcast(narrator);
        Faction benign = Benign(narrator);

        Prioritize(new FactionList(mafia, prostituteTempTeam), new FactionList(town, benign));

        town.addSheriffDetectableTeam(mafia);
        town.setEnemies(mafia, prostituteTempTeam);

        narrator.setRule(SetupModifier.SKIP_VOTE, false);
        narrator.setRule(SetupModifier.DISCUSSION_LENGTH, 999);
        narrator.setRule(SetupModifier.DAY_LENGTH, 999);
        narrator.setRule(SetupModifier.NIGHT_LENGTH, 999);
        narrator.setRule(SetupModifier.TRIAL_LENGTH, 999);
        narrator.setRule(SetupModifier.VOTE_SYSTEM, VoteSystemTypes.DIMINISHING_POOL);
        narrator.setRule(SetupModifier.DAY_START, true);
        narrator.setRule(SetupModifier.LAST_WILL, false);
        narrator.setRule(SetupModifier.SELF_VOTE, true);
        narrator.setRule(SetupModifier.SECRET_VOTES, true);

        init(narrator, KEY);

        FactionRole cit = Citizen.template(town);
        cit.role.addModifier(RoleModifier.HIDDEN_FLIP, true);

        Role sheriffRole = RoleService.createRole(narrator, "Cop", Sheriff.class);
        sheriffRole.addModifier(RoleModifier.HIDDEN_FLIP, true);
        FactionRole sheriff = FactionService.createFactionRole(town, sheriffRole);

        FactionRole doctor = Doctor.template(town);
        doctor.role.addModifier(RoleModifier.HIDDEN_FLIP, true);

        Role alienRole = RoleService.createRole(narrator, "Alien", Citizen.class);
        FactionRole alien = FactionService.createFactionRole(benign, alienRole);
        alien.role.addModifier(RoleModifier.HIDDEN_FLIP, true);
        FactionRole thief = Thief.template(benign);

        Role prositituteRole = RoleService.createRole(narrator, "Prostitute", Stripper.class);
        FactionRole prostitute = FactionService.createFactionRole(prostituteTempTeam, prositituteRole);
        prostitute.role.addModifier(RoleModifier.HIDDEN_FLIP, true);
        prostitute.role.addModifier(RoleModifier.UNDETECTABLE, true);

        Role axeRole = RoleService.createRole(narrator, "Axe", Silence.class, Disfranchise.class);
        axeRole.addModifier(RoleModifier.HIDDEN_FLIP, true);
        axeRole.addModifier(AbilityModifier.SILENCE_ANNOUNCED, Silence.class, true);
        axeRole.addModifier(AbilityModifier.DISFRANCHISED_ANNOUNCED, Disfranchise.class, true);
        FactionRole axe = FactionService.createFactionRole(mafia, axeRole);

        FactionRole goon = Goon.template(mafia);
        goon.role.addModifier(RoleModifier.HIDDEN_FLIP, true);

        addInitialRole(cit, 4);
        addInitialRole(doctor);
        addInitialRole(sheriff);

        addInitialRole(prostitute);
        addInitialRole(goon, 2);

        addInitialRole(thief);
        addInitialRole(alien);

        addSetupChange(10, axe, goon, cit);
        addSetupChange(11, cit);
        addSetupChange(12, cit);
        addSetupChange(13, cit);
    }

    @Override
    public String getShortDescription() {
        return getDescription()[0];
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "Liar's club setup."
        };
    }

    @Override
    public boolean isPreset() {
        return true;
    }

    @Override
    public String getName() {
        return "Liars Club setup";
    }
}
