package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.roles.Citizen;
import game.roles.Goon;
import game.roles.Sheriff;
import models.FactionRole;

public class Cop9er extends Setup {

    public static final String KEY = "cop9er";

    public Cop9er(Narrator narrator) {
        Faction town = Town(narrator);
        Faction maf = Mafia(narrator);

        Prioritize(maf, town);
        town.setEnemies(maf);

        town.addSheriffDetectableTeam(maf);

        narrator.setRule(SetupModifier.DAY_START, Narrator.DAY_START);
        narrator.setRule(SetupModifier.SHERIFF_PREPEEK, true);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        FactionRole sheriff = Sheriff.template(town);
        FactionRole citizen = Citizen.template(town);

        FactionRole goon = Goon.template(maf);

        addInitialRole(sheriff);
        addInitialRole(goon, 2);
        addInitialRole(citizen, 6);
    }

    @Override
    public String getShortDescription() {
        return getDescription()[0];
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "9 Player Cop Game", "Cop knows an ally at the start of the game"
        };
    }

    @Override
    public String getName() {
        return "Cop9er";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
