package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.roles.Citizen;
import game.roles.Detective;
import game.roles.Goon;
import game.roles.Lookout;
import game.roles.Stripper;
import models.FactionRole;

public class VanillaB extends Setup {

    public static final String KEY = "vanillab";

    public VanillaB(Narrator narrator) {
        Faction town = Town(narrator);
        Faction mafia = Mafia(narrator);

        Prioritize(mafia, town);
        town.setEnemies(mafia);

        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.FOLLOW_GETS_ALL, false);
        narrator.setRule(SetupModifier.BLOCK_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_SUCCESS_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_FEEDBACK, false);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        FactionRole stripper = Stripper.template(town);
        FactionRole cit = Citizen.template(town);
        FactionRole detect = Detective.template(town);
        FactionRole lookout = Lookout.template(town);

        FactionRole goon = Goon.template(mafia);

        addInitialRole(cit, 3);
        addInitialRole(lookout);
        addInitialRole(stripper);
        addInitialRole(goon, 2);

        addSetupChange(8, cit);
        addSetupChange(9, cit);
        addSetupChange(10, goon);

        addSetupChange(11, detect);
        addSetupChange(12, cit);

        addSetupChange(13, goon);
        addSetupChange(14, cit);

        addSetupChange(15, goon, cit, detect);
        addSetupChange(16, cit);
    }

    @Override
    public String getShortDescription() {
        return "Stripper/Lookout setup";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "New Town Roles - Lookout, Detective Stripper", "Mafia must decide who does the killing"
        };
    }

    @Override
    public String getName() {
        return "Vanilla B";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
