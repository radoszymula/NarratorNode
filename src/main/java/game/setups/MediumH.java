package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.roles.Agent;
import game.roles.Assassin;
import game.roles.Blackmailer;
import game.roles.Burn;
import game.roles.Citizen;
import game.roles.Detective;
import game.roles.Doctor;
import game.roles.Goon;
import game.roles.Gunsmith;
import game.roles.Hidden;
import game.roles.Lookout;
import game.roles.Mayor;
import game.roles.Poisoner;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.Snitch;
import game.roles.Stripper;
import models.FactionRole;
import services.HiddenService;

public class MediumH extends Setup {

    public static final String KEY = "mediumh";

    public MediumH(Narrator narrator) {
        Faction town = Town(narrator);
        Faction maf = Mafia(narrator);
        Faction killer = Killer(narrator);

        Prioritize(killer, maf, town);

        town.addSheriffDetectableTeam(maf, killer);

        town.setEnemies(maf, killer);
        maf.setEnemies(killer);

        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.FOLLOW_GETS_ALL, false);
        narrator.setRule(SetupModifier.BLOCK_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_SUCCESS_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_FEEDBACK, true);
        narrator.setRule(SetupModifier.GS_DAY_GUNS, true);
        narrator.setRule(SetupModifier.ARSON_DAY_IGNITES, 1);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        FactionRole cit = Citizen.template(town);

        FactionRole assassin = Assassin.template(maf);
        FactionRole goon = Goon.template(maf);

        Hidden tpr = HiddenService.createHidden(narrator, "Hidden Town", Stripper.template(town),
                Gunsmith.template(town), Mayor.template(town), Snitch.template(town), Sheriff.template(town),
                Doctor.template(town), Lookout.template(town), Detective.template(town));

        final Hidden mpr = HiddenService.createHidden(narrator, "Hidden Mafia", Agent.template(maf),
                Blackmailer.template(maf));

        Hidden nkr = HiddenService.createHidden(narrator, "Hidden Killer", SerialKiller.template(killer),
                Poisoner.template(killer), Burn.template(killer));

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        addInitialRole(cit, 4);
        addInitialRole(tpr, 2);
        addInitialRole(assassin);
        addInitialRole(goon);

        addSetupChange(9, cit);
        addSetupChange(10, nkr);
        addSetupChange(11, cit);

        addSetupChange(12, tpr, cit, mpr);
        addSetupChange(13, tpr, goon, mpr);

        addSetupChange(14, cit);
        addSetupChange(15, goon);
        addSetupChange(16, tpr, goon, mpr);
    }

    @Override
    public String getShortDescription() {
        return "Day Action Mafia!";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "New Town Roles - Mayor, Snitch, Gunsmith", "New Mafia Role - Assassin",
                "New Neutral Roles - Arsonist, Poisoner", "Guns are shot during the day",
                "Arsonist can ignite once during the day"
        };
    }

    @Override
    public String getName() {
        return "Day Action Mafia";
    }

    @Override
    public boolean isPreset() {
        return true;
    }

}
