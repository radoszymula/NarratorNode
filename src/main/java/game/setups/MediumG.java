package game.setups;

import java.util.HashMap;

import game.logic.Faction;
import game.logic.FactionList;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Agent;
import game.roles.Armorsmith;
import game.roles.Blackmailer;
import game.roles.Blacksmith;
import game.roles.Citizen;
import game.roles.Detective;
import game.roles.Doctor;
import game.roles.Executioner;
import game.roles.FactionKill;
import game.roles.Framer;
import game.roles.Goon;
import game.roles.Gunsmith;
import game.roles.Hidden;
import game.roles.Jester;
import game.roles.Lookout;
import game.roles.Sheriff;
import game.roles.Stripper;
import models.FactionRole;
import services.FactionService;
import services.HiddenFactionRoleService;
import services.HiddenService;
import services.RoleService;
import services.SetupHiddenService;

public class MediumG extends Setup {

    public static final String KEY = "mediumg";

    public MediumG(Narrator narrator) {
        Faction town = Town(narrator);
        Faction yakuza = Mafia(narrator, 2);
        Faction maf = Mafia(narrator);
        Faction outcast = Outcast(narrator);
        Faction benign = Benign(narrator);

        Prioritize(new FactionList(maf, yakuza), outcast, town, benign);

        town.addSheriffDetectableTeam(maf, yakuza);

        town.setEnemies(maf, yakuza, outcast);
        maf.setEnemies(yakuza);

        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.FOLLOW_GETS_ALL, false);
        narrator.setRule(SetupModifier.BLOCK_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_SUCCESS_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_FEEDBACK, false);
        narrator.setRule(SetupModifier.JESTER_KILLS, 0);
        narrator.setRule(SetupModifier.JESTER_CAN_ANNOY, false);
        narrator.setRule(SetupModifier.EXECUTIONER_TO_JESTER, true);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        HashMap<String, Hidden> randomSlots = new HashMap<>();

        Role stripper = Stripper.template(narrator);

        FactionRole as = Armorsmith.template(town);
        FactionRole gs = Gunsmith.template(town);
        FactionRole sheriff = Sheriff.template(town);
        FactionRole doctor = Doctor.template(town);
        FactionRole detective = Detective.template(town);
        FactionRole lookout = Lookout.template(town);

        Hidden tpr = HiddenService.createHidden(narrator, "Hidden Town", FactionService.createFactionRole(town, stripper), as, gs,
                sheriff, doctor, lookout, detective);
        randomSlots.put(tpr.getName(), tpr);

        Role agent = Agent.template(narrator);
        Role bm = Blackmailer.template(narrator);
        Role framer = Framer.template(narrator);

        final Hidden mpr = HiddenService.createHidden(narrator, "Hidden Mafia", FactionService.createFactionRole(maf, agent),
                FactionService.createFactionRole(maf, bm), FactionService.createFactionRole(maf, stripper),
                FactionService.createFactionRole(maf, framer));

        final Hidden ypr = HiddenService.createHidden(narrator, "Hidden Yakuza", FactionService.createFactionRole(yakuza, agent),
                FactionService.createFactionRole(yakuza, bm), FactionService.createFactionRole(yakuza, stripper),
                FactionService.createFactionRole(yakuza, framer));

        Role evil_bs = RoleService.createRole(narrator, "Blacksmith", Blacksmith.class);
        evil_bs.addModifier(AbilityModifier.GS_FAULTY_GUNS, Blacksmith.class, true);
        evil_bs.addModifier(AbilityModifier.AS_FAKE_VESTS, Blacksmith.class, true);

        FactionRole maf_bs = FactionService.createFactionRole(maf, evil_bs);
        FactionRole yak_bs = FactionService.createFactionRole(yakuza, evil_bs);

        HiddenFactionRoleService.addSpawnableRoles(narrator, mpr, maf_bs);
        HiddenFactionRoleService.addSpawnableRoles(narrator, ypr, yak_bs);

        FactionRole neut_bs = FactionService.createFactionRole(outcast, evil_bs);
        FactionRole exec = Executioner.template(benign);
        exec.role.addChargeModifier(1);
        FactionRole jester = Jester.template(benign);

        final Hidden neutral = HiddenService.createHidden(narrator, "Hidden Neutral", jester, neut_bs, exec);

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        final FactionRole cit = Citizen.template(town);
        Role goon = Goon.template(narrator);
        FactionRole mafiaGoon = FactionService.createFactionRole(maf, goon);
        FactionRole yakGoon = FactionService.createFactionRole(yakuza, goon);

        addInitialRole(cit, 3);
        addInitialRole(tpr, 2);
        addInitialRole(mpr, 2);

        addSetupChange(8, neutral, mpr, mafiaGoon);
        addSetupChange(9, yakGoon);
        addSetupChange(9, new SetupChange() {
            @Override
            public void implementChange(Setup s) {
                s.narrator.getFaction(YAKUZA_C).modifyAbility(AbilityModifier.COOLDOWN, FactionKill.class, 1);
                s.narrator.getFaction(MAFIA_C).modifyAbility(AbilityModifier.COOLDOWN, FactionKill.class, 1);

                s.narrator.removeSetupHidden(neutral);
                SetupHiddenService.addSetupHidden(s.narrator, ypr, false);
            }

            @Override
            public void undoChange(Setup s) {
                s.narrator.getFaction(YAKUZA_C).modifyAbility(AbilityModifier.COOLDOWN, FactionKill.class, 0);
                s.narrator.getFaction(MAFIA_C).modifyAbility(AbilityModifier.COOLDOWN, FactionKill.class, 0);

                SetupHiddenService.addSetupHidden(s.narrator, neutral, false);
                s.narrator.removeSetupHidden(ypr);

            }
        });
        addSetupChange(10, cit);
        addSetupChange(11, ypr, yakGoon, mpr, mafiaGoon, tpr);
        addSetupChange(12, yakGoon, ypr, yakGoon);// 12

        addSetupChange(13, cit);// 13
        addSetupChange(14, cit);
        addSetupChange(14, new SetupChange() {
            @Override
            public void implementChange(Setup s) {
                s.narrator.getFaction(YAKUZA_C).modifyAbility(AbilityModifier.COOLDOWN, FactionKill.class, 0);
                s.narrator.getFaction(MAFIA_C).modifyAbility(AbilityModifier.COOLDOWN, FactionKill.class, 0);
            }

            @Override
            public void undoChange(Setup s) {
                s.narrator.getFaction(YAKUZA_C).modifyAbility(AbilityModifier.COOLDOWN, FactionKill.class, 1);
                s.narrator.getFaction(MAFIA_C).modifyAbility(AbilityModifier.COOLDOWN, FactionKill.class, 1);

            }
        });
        addSetupChange(15, mafiaGoon, yakGoon, ypr, cit, tpr);
        addSetupChange(16, neutral);
    }

    @Override
    public String getShortDescription() {
        return "Intermediate setup with two mafia teams";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "New Town Roles - Gunsmith, Armorsmith", "New Mafia Roles - Framer, Stripper, Gunsmith, Armorsmith",
                "New Neutral Roles - Gunsmith, Armorsmith", "Nontown Armorsmith and Gunsmith items can be fake",
                "Mafia can only kill every night if starting player size was at least 14"
        };
    }

    @Override
    public String getName() {
        return "Two Mafia";
    }

    @Override
    public boolean isPreset() {
        return true;
    }

}
