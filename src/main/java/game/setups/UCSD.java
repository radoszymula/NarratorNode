package game.setups;

import game.logic.Narrator;

public class UCSD extends GoT {

    public static final String KEY = "ucsd";

    public UCSD(Narrator narrator) {
        super(narrator);

        narrator.getFaction(GoT.baratheon_c).setName("Warren");
        narrator.getFaction(GoT.targaryen_c).setName("Muir");
        narrator.getFaction(GoT.tyrell_c).setName("Marshall");
        narrator.getFaction(GoT.stark_c).setName("Revelle");
        narrator.getFaction(GoT.lannister_c).setName("ERC");
        narrator.getFaction(GoT.martell_c).setName("Sixth");

    }

    @Override
    public String getShortDescription() {
        return "The massive 6 sided version of Mafia";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                getShortDescription()
        };
    }

    @Override
    public String getName() {
        return "Sixth vs Muir";
    }

    @Override
    public String getGenre() {
        return "ucsd";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
