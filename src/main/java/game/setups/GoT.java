package game.setups;

import game.logic.Faction;
import game.logic.FactionList;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.roles.Ability;
import game.roles.AbilityList;
import game.roles.Agent;
import game.roles.Amnesiac;
import game.roles.Architect;
import game.roles.Assassin;
import game.roles.Baker;
import game.roles.Blacksmith;
import game.roles.Bodyguard;
import game.roles.Bulletproof;
import game.roles.Burn;
import game.roles.Citizen;
import game.roles.Coroner;
import game.roles.Coward;
import game.roles.Detective;
import game.roles.Disguiser;
import game.roles.Doctor;
import game.roles.Douse;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.Framer;
import game.roles.GraveDigger;
import game.roles.Hidden;
import game.roles.Investigator;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Lookout;
import game.roles.Mason;
import game.roles.MassMurderer;
import game.roles.Mayor;
import game.roles.Operator;
import game.roles.Poisoner;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.Sleepwalker;
import game.roles.Snitch;
import game.roles.Spy;
import game.roles.Stripper;
import game.roles.Survivor;
import game.roles.Tailor;
import game.roles.Undouse;
import game.roles.Ventriloquist;
import game.roles.Veteran;
import game.roles.Vigilante;
import game.roles.Witch;
import game.roles.support.Gun;
import game.roles.support.Vest;
import models.FactionRole;
import services.FactionRoleService;
import services.FactionService;
import services.HiddenService;
import services.RoleAbilityModifierService;
import services.RoleService;
import util.LookupUtil;
import util.ModifierUtil;

public class GoT extends Setup {

    public static final String martell_c = "#FF0000";
    public static final String baratheon_c = "#880BFC";
    public static final String lannister_c = "#FFA500";
    public static final String tyrell_c = "#FFFF00";
    public static final String stark_c = "#6495ED";
    public static final String targaryen_c = "#00FF00";

    public static final String MARTELL_NAME = "Martell";

    public static final String ARMS_DEALER = "Arms Dealer";

    public static final String KEY = "got";

    // http://www.sc2mafia.com/forum/showthread.php/32863-Rolecards
    public GoT(Narrator narrator) {
        String targaryenName = "Targaryen";
        String martell_n = MARTELL_NAME;
        String tyrellName = "Tyrell";
        String starkName = "Stark";
        String baratheonName = "Baratheon";
        String lannisterName = "Lannister";

        Faction martell = FactionService.createFaction(narrator, martell_c, martell_n, "Unbowed, Unbent, Unbroken");
        Faction baratheon = FactionService.createFaction(narrator, baratheon_c, baratheonName, "Ours is the Fury");
        Faction lannister = FactionService.createFaction(narrator, lannister_c, lannisterName, "A Lannister always pays his debts");
        Faction tyrell = FactionService.createFaction(narrator, tyrell_c, tyrellName, "Growing Strong");
        Faction stark = FactionService.createFaction(narrator, stark_c, starkName, "Winter is Coming");
        Faction targaryen = FactionService.createFaction(narrator, targaryen_c, targaryenName, "Fire and Blood");

        Prioritize(martell, new FactionList(baratheon, lannister), new FactionList(targaryen, stark, tyrell));

        baratheon.setKnowsTeam(true);
        martell.setKnowsTeam(true);
        lannister.setKnowsTeam(true);

        lannister.setKillEnabled();
        lannister.setNightChat(true);
        baratheon.setKillEnabled();
        baratheon.setNightChat(true);

        targaryen.addSheriffDetectableTeam(lannister_c, martell_c, baratheon_c);

        stark.addSheriffDetectableTeam(lannister_c, tyrell_c, martell_c);

        tyrell.addSheriffDetectableTeam(stark_c, martell_c, baratheon_c);

        tyrell.setRule(FactionModifier.LAST_WILL, true);
        stark.setRule(FactionModifier.LAST_WILL, true);

        targaryen.setEnemies(martell, lannister, baratheon);
        stark.setEnemies(martell, lannister, tyrell);
        baratheon.setEnemies(lannister, tyrell);
        martell.setEnemies(tyrell);

        narrator.setRule(SetupModifier.CHARGE_VARIABILITY, 2);
        narrator.setRule(SetupModifier.DIFFERENTIATED_FACTION_KILLS, false);
        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.SPY_TARGETS_ENEMIES, true);
        narrator.setRule(SetupModifier.SPY_TARGETS_ALLIES, false);
        narrator.setRule(SetupModifier.GS_DAY_GUNS, false);
        narrator.setRule(SetupModifier.BREAD_PASSING, false);
        narrator.setRule(SetupModifier.SELF_BREAD_USAGE, false);
        narrator.setRule(SetupModifier.CHECK_DIFFERENTIATION, false);
        narrator.setRule(SetupModifier.CORONER_EXHUMES, false);
        narrator.setRule(SetupModifier.FOLLOW_GETS_ALL, false);
        narrator.setRule(SetupModifier.HEAL_SUCCESS_FEEDBACK, true);
        narrator.setRule(SetupModifier.HEAL_FEEDBACK, true);
        narrator.setRule(SetupModifier.HEAL_BLOCKS_POISON, false);
        narrator.setRule(SetupModifier.CIT_RATIO, 40);
        narrator.setRule(SetupModifier.CHARGE_VARIABILITY, 2);
        narrator.setRule(SetupModifier.ARCH_QUARANTINE, false);
        narrator.setRule(SetupModifier.MASON_PROMOTION, false);
        narrator.setRule(SetupModifier.MAYOR_VOTE_POWER, 3);

        narrator.setRule(SetupModifier.GD_REANIMATE, true);
        narrator.setRule(SetupModifier.ARSON_DAY_IGNITES, SetupModifiers.UNLIMITED);

        narrator.setRule(SetupModifier.JANITOR_GETS_ROLES, false);

        narrator.setRule(SetupModifier.TAILOR_FEEDBACK, true);
        narrator.setRule(SetupModifier.SNITCH_PIERCES_SUIT, true);

        narrator.setAlias(Gun.ALIAS, "sword");
        narrator.setAlias(Vest.ALIAS, "armor");

        // Faction copy roles
        Role alchemist = RoleService.createRole(narrator, "Alchemist", DrugDealer.class);
        Role architect = Architect.template(narrator);
        Role blacksmithRole = RoleService.createRole(narrator, ARMS_DEALER, Blacksmith.class);
        Role strategist = RoleService.createRole(narrator, "Strategist", Operator.class);
        Role wagonDriver = RoleService.createRole(narrator, "Wagon Driver", Driver.class);
        Role warg = RoleService.createRole(narrator, "Warg", Ventriloquist.class);
        Role whore = RoleService.createRole(narrator, "Whore", Stripper.class);

        // Martell only roles

        Role berserker = RoleService.createRole(narrator, "Berserker", MassMurderer.class);
        Role necromancer = RoleService.createRole(narrator, "Necromancer", GraveDigger.class);
        Role poisoner = RoleService.createRole(narrator, "Poisoner", Poisoner.class);
        Role serialKiller = RoleService.createRole(narrator, "Serial Killer", SerialKiller.class);
        Role survyvor = RoleService.createRole(narrator, "Survyvor", Bulletproof.class);

        Role firePriestess = RoleService.createRole(narrator, "Fire Priestess", Douse.class, Burn.class, Undouse.class);

        FactionRole armsDealer = FactionService.createFactionRole(martell, blacksmithRole);
        FactionRoleService.addModifier(armsDealer, AbilityModifier.AS_FAKE_VESTS, Blacksmith.class, true);
        FactionRoleService.addModifier(armsDealer, AbilityModifier.GS_FAULTY_GUNS, Blacksmith.class, true);
        
        FactionRole[] nonKillingParts = {
                FactionService.createFactionRole(martell, alchemist), FactionService.createFactionRole(martell, architect),
                FactionService.createFactionRole(martell, necromancer), FactionService.createFactionRole(martell, strategist), 
                FactionService.createFactionRole(martell, survyvor), FactionService.createFactionRole(martell, wagonDriver), 
                FactionService.createFactionRole(martell, warg), FactionService.createFactionRole(martell, whore), 
                Witch.template(martell),
        };

        FactionRole[] killingMartellRoles = {
                FactionService.createFactionRole(martell, berserker), FactionService.createFactionRole(martell, firePriestess),
                FactionService.createFactionRole(martell, poisoner), FactionService.createFactionRole(martell, serialKiller),
        };

        FactionRole[] nonKillingLeaderParts = getLeader(nonKillingParts);
        FactionRole[] killingMartellLeaderRoles = getLeader(killingMartellRoles);

        Hidden martellAllLeader = getMartellAllRolesLeader(nonKillingLeaderParts, killingMartellLeaderRoles);
        Hidden martellNonkilling_leader = HiddenService.createHidden(narrator, MARTELL_NAME + " Leader",
                nonKillingLeaderParts);
        Hidden martellKilling = HiddenService.createHidden(narrator, martell_n + " Killer", killingMartellRoles);
        Hidden martellNonkilling = HiddenService.createHidden(narrator, martell_n + " Member", nonKillingParts);

        // Faction copy roles
        Role coward = Coward.template(narrator);
        RoleAbilityModifierService.modify(coward, Coward.class, AbilityModifier.CHARGES, 4);
        Role jailor = Jailor.template(narrator);

        // Mafia only roles
        Role disguiser = Disguiser.template(narrator).addChargeModifier(1);
        Role janitor = Janitor.template(narrator).addChargeModifier(3);

        Role spyRole = Spy.template(narrator);
        RoleAbilityModifierService.modify(spyRole, Spy.class, AbilityModifier.CHARGES, 3);

        Role tailor = Tailor.template(narrator).addChargeModifier(2);

        Role[] mafiaRoles = new Role[] {
                Agent.template(narrator), alchemist, Assassin.template(narrator), coward, disguiser,
                Framer.template(narrator), jailor, janitor, Investigator.template(narrator), spyRole, strategist,
                tailor, wagonDriver, warg, whore
        };

        FactionRole[] lannisterRoles = new FactionRole[mafiaRoles.length];
        for(int i = 0; i < mafiaRoles.length; i++)
            lannisterRoles[i] = FactionService.createFactionRole(lannister, mafiaRoles[i]);

        FactionRole[] baratheonRoles = new FactionRole[mafiaRoles.length];
        for(int i = 0; i < mafiaRoles.length; i++)
            baratheonRoles[i] = FactionService.createFactionRole(baratheon, mafiaRoles[i]);

        Hidden lannisterMembers = HiddenService.createHidden(narrator, lannisterName + " Member", lannisterRoles);
        Hidden baratheonMembers = HiddenService.createHidden(narrator, baratheonName + " Member", baratheonRoles);
        Hidden lannisterLeader = HiddenService.createHidden(narrator, lannisterName + " Leader",
                getMafiaLeader(lannisterRoles));
        Hidden baratheonLeader = HiddenService.createHidden(narrator, baratheonName + " Leader",
                getMafiaLeader(baratheonRoles));

        // Faction copy roles
        Role bodyguard = Bodyguard.template(narrator);
        Role doctor = Doctor.template(narrator);
        Role sheriff = Sheriff.template(narrator);

        // Neutral only roles
        Role archer = RoleService.createRole(narrator, "Archer", Vigilante.class);
        archer.addChargeModifier(3);

        Role hunter = RoleService.createRole(narrator, "Hunter", Detective.class);

        FactionRole spyStark = FactionService.createFactionRole(stark, spyRole);
        FactionRoleService.addModifier(spyStark, AbilityModifier.CHARGES, Spy.class, 2);
        FactionRole spyTyrell = FactionService.createFactionRole(tyrell, spyRole);
        FactionRoleService.addModifier(spyTyrell, AbilityModifier.CHARGES, Spy.class, 2);

        Role watchman = RoleService.createRole(narrator, "Watchman", Lookout.class);

        Role[] neutralRoles = new Role[] {
                archer, bodyguard, doctor, jailor, sheriff, hunter, strategist, watchman, whore, wagonDriver
        };

        FactionRole[] starkRoles = new FactionRole[neutralRoles.length + 1];
        starkRoles[0] = spyStark;
        for(int i = 1; i <= neutralRoles.length; ++i)
            starkRoles[i] = FactionService.createFactionRole(stark, neutralRoles[i - 1]);
        FactionRole[] tyrellRoles = new FactionRole[neutralRoles.length + 1];
        tyrellRoles[0] = spyTyrell;
        for(int i = 1; i <= neutralRoles.length; ++i)
            tyrellRoles[i] = FactionService.createFactionRole(tyrell, neutralRoles[i - 1]);

        Hidden starkMembers = HiddenService.createHidden(narrator, starkName + " Member", starkRoles);
        Hidden tyrellMembers = HiddenService.createHidden(narrator, tyrellName + " Member", tyrellRoles);

        // Town roles
        FactionRole architectTown = FactionService.createFactionRole(targaryen, architect);
        FactionRoleService.addModifier(architectTown, AbilityModifier.SELF_TARGET, Architect.class, false);

        FactionRole cowardTargaryen = FactionService.createFactionRole(targaryen, coward);
        FactionRoleService.addModifier(cowardTargaryen, AbilityModifier.CHARGES, Coward.class, 3);

        FactionRole citizen = Citizen.template(targaryen);
        Role drunk = RoleService.createRole(narrator, "Drunk", Amnesiac.class);

        Role knight = RoleService.createRole(narrator, "Knight", Veteran.class);
        knight.addChargeModifier(3);

        Role nurse = RoleService.createRole(narrator, "Nurse", Doctor.class, Stripper.class);
        nurse.addCooldownModifier(Doctor.class, 1);
        nurse.addCooldownModifier(Stripper.class, 1);

        FactionRole sleepwalker = Sleepwalker.template(targaryen, citizen);

        FactionRole survivor = Survivor.template(targaryen);
        survivor.role.addChargeModifier(4);

        FactionRole[] townRoles = new FactionRole[] {
                FactionService.createFactionRole(targaryen, archer), architectTown, Baker.template(targaryen),
                Blacksmith.template(targaryen), FactionService.createFactionRole(targaryen, bodyguard), citizen,
                Coroner.template(targaryen), cowardTargaryen, FactionService.createFactionRole(targaryen, doctor),
                FactionService.createFactionRole(targaryen, drunk), FactionService.createFactionRole(targaryen, hunter),
                FactionService.createFactionRole(targaryen, knight), Mason.template(targaryen), Mayor.template(targaryen),
                FactionService.createFactionRole(targaryen, nurse), FactionService.createFactionRole(targaryen, sheriff),
                Snitch.template(targaryen), sleepwalker, survivor, FactionService.createFactionRole(targaryen, watchman),
                FactionService.createFactionRole(targaryen, wagonDriver), FactionService.createFactionRole(targaryen, whore),

        };
        Hidden targaryenMembers = HiddenService.createHidden(narrator, targaryenName + " Member", townRoles);

        init(narrator, KEY);

        addInitialRole(targaryenMembers, 7);
        addInitialRole(starkMembers, 2);
        addInitialRole(tyrellMembers, 2);
        addInitialRole(lannisterLeader);
        addInitialRole(lannisterMembers);
        addInitialRole(baratheonLeader);
        addInitialRole(baratheonMembers);
        addInitialRole(martellNonkilling_leader);

        addSetupChange(17, SetupChange.addHidden(targaryenMembers));
        addSetupChange(18, SetupChange.replaceAdd(tyrellMembers, targaryenMembers, baratheonMembers));
        addSetupChange(19, SetupChange.addHidden(targaryenMembers));
        addSetupChange(20, SetupChange.addHidden(targaryenMembers));
        addSetupChange(21, SetupChange.replaceAdd(starkMembers, targaryenMembers, lannisterMembers));
        addSetupChange(22, SetupChange.replaceAdd(martellAllLeader, martellNonkilling_leader, targaryenMembers));
        addSetupChange(23, SetupChange.replaceAdd(martellNonkilling_leader, martellAllLeader, martellKilling));
        addSetupChange(24, SetupChange.addHidden(targaryenMembers));
        addSetupChange(25, SetupChange.replaceAdd(tyrellMembers, targaryenMembers, starkMembers));
        addSetupChange(26, SetupChange.addHidden(targaryenMembers));
        addSetupChange(27, SetupChange.addHidden(martellNonkilling));

        addSetupChange(28, SetupChange.addHidden(targaryenMembers));
        addSetupChange(28, SetupChange.ModifyRole(baratheonLeader, RoleModifier.AUTO_VEST, 0, 1));
        addSetupChange(28, SetupChange.ModifyRole(lannisterLeader, RoleModifier.AUTO_VEST, 0, 1));
        addSetupChange(28, SetupChange.AddAbility(baratheonLeader, Bulletproof.class));
        addSetupChange(28, SetupChange.AddAbility(lannisterLeader, Bulletproof.class));
    }

    private Hidden getMartellAllRolesLeader(FactionRole[] killingRoles, FactionRole[] nonKillingRoles) {
        FactionRole[] roles = new FactionRole[killingRoles.length + nonKillingRoles.length];
        for(int i = 0; i < killingRoles.length; ++i)
            roles[i] = killingRoles[i];
        for(int i = 0; i < nonKillingRoles.length; ++i)
            roles[i + killingRoles.length] = nonKillingRoles[i];
        Narrator narrator = roles[0].faction.narrator;
        return HiddenService.createHidden(narrator, MARTELL_NAME + " Unknown", roles);
    }

    @SuppressWarnings("unchecked")
    private static Class<? extends Ability>[] addBulletProof(AbilityList abilityList) {
        Class<? extends Ability>[] classes = (Class<? extends Ability>[]) new Class<?>[abilityList.size() + 1];
        int i = 0;
        classes[i] = Bulletproof.class;
        for(Ability ability: abilityList)
            classes[++i] = ability.getClass();
        return classes;
    }

    @SuppressWarnings("unchecked")
    private static Class<? extends Ability>[] getClasses(AbilityList abilityList) {
        Class<? extends Ability>[] classes = (Class<? extends Ability>[]) new Class<?>[abilityList.size()];
        int i = 0;
        for(Ability ability: abilityList)
            classes[i++] = ability.getClass();
        return classes;
    }

    private FactionRole[] getLeader(FactionRole[] roles) {
        Faction faction = roles[0].faction;
        Narrator narrator = faction.narrator;
        FactionRole[] leaderRoles = new FactionRole[roles.length];

        Role role, leaderRole;
        for(int i = 0; i < roles.length; i++){
            role = roles[i].role;
            leaderRole = RoleService.createRole(narrator, role.getName() + " Leader", addBulletProof(role._abilities));
            ModifierUtil.copyModifierMap(leaderRole, role);
            leaderRole.addModifier(RoleModifier.UNBLOCKABLE, true);
            leaderRoles[i] = FactionService.createFactionRole(faction, leaderRole);
        }

        return leaderRoles;
    }

    private FactionRole[] getMafiaLeader(FactionRole[] roles) {
        Faction faction = roles[0].faction;
        Narrator narrator = faction.narrator;

        FactionRole[] leaderRoles = new FactionRole[roles.length];

        Role role, leaderRole;
        String leaderName;
        for(int i = 0; i < roles.length; i++){
            role = roles[i].role;
            leaderName = role.getName() + " Head";
            
            leaderRole = LookupUtil.findRole(narrator, leaderName);
            if(leaderRole == null) {
	            leaderRole = RoleService.createRole(narrator, leaderName, getClasses(role._abilities));
	            ModifierUtil.copyModifierMap(leaderRole, role);
	            leaderRole.addModifier(RoleModifier.UNDETECTABLE, true);
	            leaderRole.addModifier(RoleModifier.AUTO_VEST, 1);
            }
            leaderRoles[i] = FactionService.createFactionRole(faction, leaderRole);
        }

        return leaderRoles;
    }

    @Override
    public String getShortDescription() {
        return "The massive 6 sided version of Mafia";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                getShortDescription()
        };
    }

    @Override
    public String getName() {
        return "Game of Thrones";
    }

    @Override
    public boolean isPreset() {
        return true;
    }

    @Override
    public String getGenre() {
        return "got";
    }
}
