package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Citizen;
import game.roles.FactionKill;
import game.roles.Goon;
import game.roles.Investigator;
import game.roles.Miller;
import game.roles.Snitch;
import models.FactionRole;
import services.FactionRoleService;
import services.FactionService;
import services.RoleService;

public class Voltron extends Setup {

    public static final String KEY = "voltron";

    public Voltron(Narrator narrator) {
        Faction town = Town(narrator);
        Faction maf = Mafia(narrator);

        Prioritize(maf, town);
        town.setEnemies(maf);

        maf.modifyAbility(AbilityModifier.ZERO_WEIGHTED, FactionKill.class, true);

        narrator.setRule(SetupModifier.DAY_START, Narrator.DAY_START);
        narrator.setRule(SetupModifier.MILLER_SUITED, true);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        FactionRole cititzen = Citizen.template(town);

        Role millerRole = RoleService.createRole(narrator, "Miller", Miller.class);
        FactionRole miller = FactionService.createFactionRole(town, millerRole);
        FactionRoleService.setRoleReceive(miller, cititzen);

        FactionRole oracle = Snitch.template(town);
        Role bpRole = RoleService.createRole(narrator, "Bulletproof", Citizen.class);
        FactionRole bp = FactionService.createFactionRole(town, bpRole);
        bp.role.addModifier(RoleModifier.AUTO_VEST, 1);

        FactionRole invest = Investigator.template(maf);
        FactionRole goon = Goon.template(maf);

        addInitialRole(cititzen, 3);
        addInitialRole(miller);
        addInitialRole(oracle);
        addInitialRole(bp);
        addInitialRole(goon);
        addInitialRole(invest);
    }

    @Override
    public String getShortDescription() {
        return getDescription()[0];
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "8 Player Oracle/BP Game"
        };
    }

    @Override
    public String getName() {
        return "8 Player Oracle/BP Game";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
