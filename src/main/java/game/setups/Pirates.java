package game.setups;

import game.logic.Faction;
import game.logic.FactionList;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Agent;
import game.roles.Amnesiac;
import game.roles.Architect;
import game.roles.Armorsmith;
import game.roles.Blackmailer;
import game.roles.Bodyguard;
import game.roles.Burn;
import game.roles.Citizen;
import game.roles.Coroner;
import game.roles.Coward;
import game.roles.CultLeader;
import game.roles.Detective;
import game.roles.Disguiser;
import game.roles.Doctor;
import game.roles.Douse;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.ElectroManiac;
import game.roles.Executioner;
import game.roles.Framer;
import game.roles.Ghost;
import game.roles.Godfather;
import game.roles.Gunsmith;
import game.roles.Hidden;
import game.roles.Infiltrator;
import game.roles.Investigator;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Jester;
import game.roles.Joker;
import game.roles.Lookout;
import game.roles.Mayor;
import game.roles.Poisoner;
import game.roles.Scout;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.Spy;
import game.roles.Stripper;
import game.roles.Undouse;
import game.roles.Ventriloquist;
import game.roles.Veteran;
import game.roles.Vigilante;
import game.roles.Witch;
import models.FactionRole;
import services.FactionService;
import services.HiddenService;
import services.RoleService;

public class Pirates extends Setup {

    public static final String pirates_c = Constants.RED;
    public static final String interpol_c = Constants.PURPLE;
    public static final String rebels_c = Constants.YELLOW;
    public static final String marines_c = Constants.BLUE;
    public static final String village_c = Constants.GREEN;
    public static final String CULTIST_ALIAS = "Revolutionary";
    // http://www.sc2mafia.com/forum/showthread.php/17102-FM-XVII-Rules-Role-list-and-FAQ
    public static final String KEY = "pirates";

    public Pirates(Narrator narrator) {
        String pirates_n = "Pirate";
        String interpol_n = "CP9";
        String rebels_n = "Rebel";
        String marines_n = "Marine";
        String village_n = "Islander";
        String threat_n = "Psychotic";
        String benign_n = "Benign";

        Faction pirates = Mafia(narrator, pirates_c, pirates_n).setCanRecruitFrom(false);
        Faction interpol = Mafia(narrator, interpol_c, interpol_n).setCanRecruitFrom(false);
        Faction threat = Killer(narrator, THREAT_C, threat_n).setCanRecruitFrom(false);
        Faction rebels = Cult(narrator, rebels_c, rebels_n).setCanRecruitFrom(false);
        Faction marines = Cult(narrator, marines_c, marines_n).setCanRecruitFrom(false);
        Faction villagers = Town(narrator, village_c, village_n);
        Faction benigns = Benign(narrator).setCanRecruitFrom(false);

        Prioritize(new FactionList(pirates, interpol, threat), new FactionList(rebels, marines), benigns);

        pirates.setDescription("A group of real pirates with intentions to kill and steal!");
        marines.setDescription("Local authorities with a battleship.");
        rebels.setDescription("An evergrowing group, with their cell deep in the society of the islanders.");
        interpol.setDescription(
                "A group of dangerous assassins sent by the World Government.  Their orders are to kill every inhabitant of the island.");
        villagers.setDescription("A carefree group of people living on Cinco Island.");
        threat.setDescription("An insane fellow who just wants to watch the island burn.");
        benigns.setDescription("Natives who have no stake in who wins or loses.");

        marines.addSheriffDetectableTeam(pirates, rebels, threat);

        villagers.setEnemies(threat, pirates, interpol);
        marines.setEnemies(rebels, pirates);
        rebels.setEnemies(interpol);

        narrator.setRule(SetupModifier.DAY_START, Narrator.DAY_START);
        narrator.setRule(SetupModifier.DIFFERENTIATED_FACTION_KILLS, true);

        narrator.setRule(SetupModifier.GS_DAY_GUNS, false);
        narrator.setRule(SetupModifier.CIT_RATIO, 40);
        narrator.setRule(SetupModifier.ENFORCER_LEARNS_NAMES, true);
        narrator.setRule(SetupModifier.CORONER_EXHUMES, false);
        narrator.setRule(SetupModifier.MAYOR_VOTE_POWER, 2);

        narrator.setRule(SetupModifier.AMNESIAC_MUST_REMEMBER, true);
        narrator.setRule(SetupModifier.ARSON_DAY_IGNITES, 0);

        // narrator.setBool(Rules.WITCH_SELF, true);
        narrator.setRule(SetupModifier.WITCH_FEEDBACK, true);
        narrator.setRule(SetupModifier.SPY_TARGETS_ALLIES, true);
        narrator.setRule(SetupModifier.SPY_TARGETS_ENEMIES, false);

        narrator.setRule(SetupModifier.JANITOR_GETS_ROLES, false);

        narrator.setRule(SetupModifier.CULT_POWER_ROLE_CD, 0);
        narrator.setRule(SetupModifier.CONVERT_REFUSABLE, true);
        narrator.setRule(SetupModifier.CULT_PROMOTION, false);
        narrator.setRule(SetupModifier.CULT_KEEPS_ROLES, true);

        narrator.setRule(SetupModifier.BOUNTY_INCUBATION, 2);
        narrator.setRule(SetupModifier.BOUNTY_FAIL_REWARD, 2);

        // architect picking up chat
        narrator.setRule(SetupModifier.ARCH_QUARANTINE, true);

        narrator.setRule(SetupModifier.JESTER_CAN_ANNOY, true);
        narrator.setRule(SetupModifier.JESTER_KILLS, 14);

        narrator.setAlias(CultLeader.MINION_ALIAS, CULTIST_ALIAS);

        FactionRole mayor = Mayor.template(villagers);
        mayor.role.addModifier(RoleModifier.UNCONVERTABLE, true);

        Role stripperRole = Stripper.template(narrator);

        FactionRole doctor = Doctor.template(villagers);
        FactionRole cit = Citizen.template(villagers);
        FactionRole coroner = Coroner.template(villagers);
        FactionRole driver = Driver.template(villagers);
        FactionRole town_vet = Veteran.template(villagers);
        Role bsRole = RoleService.createRole(narrator, "Blacksmith", Armorsmith.class, Gunsmith.class);
        FactionRole blacksmith = FactionService.createFactionRole(villagers, bsRole);
        Hidden village_members = HiddenService.createHidden(narrator, "Hidden " + village_n, doctor, coroner, driver,
                blacksmith, FactionService.createFactionRole(villagers, stripperRole), town_vet);

        Role navyAdmiralRole = RoleService.createRole(narrator, "Navy Admiral", Sheriff.class);
        FactionRole navyAdmiral = FactionService.createFactionRole(marines, navyAdmiralRole);
        FactionRole vigi = Vigilante.template(marines);
        vigi.role.addChargeModifier(3);
        FactionRole lookout = Lookout.template(marines);
        FactionRole detective = Detective.template(marines);
        FactionRole jailor = Jailor.template(marines);
        FactionRole bodyguard = Bodyguard.template(marines);
        Hidden marine_members = HiddenService.createHidden(narrator, "Hidden " + marines_n, vigi, detective, lookout,
                jailor, bodyguard);

        Role infiltratorRole = RoleService.createRole(narrator, "Infiltrator", Infiltrator.class);
        FactionRole infiltrator = FactionService.createFactionRole(interpol, infiltratorRole);
        FactionRole witch = Witch.template(interpol);
        FactionRole drugDealer = DrugDealer.template(interpol);
        FactionRole coward = Coward.template(interpol);
        FactionRole spy = Spy.template(interpol);
        FactionRole agent = Agent.template(interpol);
        FactionRole architect = Architect.template(interpol);
        FactionRole ventrilo = Ventriloquist.template(interpol);
        Hidden interpol_members = HiddenService.createHidden(narrator, "Hidden " + interpol_n, witch, drugDealer,
                coward, spy, agent, architect, ventrilo);

        Role serialKillerRole = RoleService.createRole(narrator, "Serial Killer", SerialKiller.class);
        FactionRole sk = FactionService.createFactionRole(threat, serialKillerRole);
        Role poisonerRole = RoleService.createRole(narrator, "Poisoner", Poisoner.class);
        FactionRole poisoner = FactionService.createFactionRole(threat, poisonerRole);
        Role electromaniacRole = RoleService.createRole(narrator, "Electro Maniac", ElectroManiac.class);
        FactionRole em = FactionService.createFactionRole(threat, electromaniacRole);
        Role jokerRole = RoleService.createRole(narrator, "Joker", Joker.class);
        FactionRole joker = FactionService.createFactionRole(threat, jokerRole);
        Role arsonistRole = RoleService.createRole(narrator, "Arsonist", Douse.class, Undouse.class, Burn.class);
        FactionRole arsonist = FactionService.createFactionRole(threat, arsonistRole);
        Hidden threat_members = HiddenService.createHidden(narrator, "Hidden " + threat_n, sk, poisoner, em, joker,
                arsonist);
        threat_members.addModifier(RoleModifier.AUTO_VEST, 1);

        Role pirateCaptainRole = RoleService.createRole(narrator, "Pirate Captain", Godfather.class);
        FactionRole pirateCaptain = FactionService.createFactionRole(pirates, pirateCaptainRole);
        FactionRole disg = Disguiser.template(pirates);
        FactionRole janitor = Janitor.template(pirates);
        janitor.role.addChargeModifier(4);
        disg.role.addChargeModifier(2);
        pirateCaptain.role.addModifier(RoleModifier.UNDETECTABLE, true);
        Hidden pirate_members = HiddenService.createHidden(narrator, "Hidden " + pirates_n,
                FactionService.createFactionRole(pirates, stripperRole), janitor, Investigator.template(pirates),
                Framer.template(pirates), Blackmailer.template(pirates), disg);

        Role rebelLeaderRole = RoleService.createRole(narrator, "Rebel Leader", CultLeader.class);
        Role rebelScoutRole = RoleService.createRole(narrator, "Rebel Scout", Scout.class);
        FactionRole rebelScout = FactionService.createFactionRole(rebels, rebelScoutRole);
        FactionRole rebelLeader = FactionService.createFactionRole(rebels, rebelLeaderRole);

        FactionRole exec = Executioner.template(benigns);
        exec.role.addChargeModifier(1);

        Hidden neutral_members = HiddenService.createHidden(narrator, benign_n, Jester.template(benigns), exec,
                Amnesiac.template(benigns), Ghost.template(benigns));

        init(narrator, KEY);

        addInitialRole(pirateCaptain);
        addInitialRole(pirate_members);
        addInitialRole(infiltrator);
        addInitialRole(interpol_members);
        addInitialRole(rebelLeader);
        addInitialRole(navyAdmiral);
        addInitialRole(marine_members);
        addInitialRole(doctor);
        addInitialRole(village_members, 2);
        addInitialRole(cit, 4);

        addSetupChange(15, cit);

        addSetupChange(16, cit);

        addSetupChange(17, neutral_members);

        addSetupChange(18, pirate_members);

        addSetupChange(19, cit);

        addSetupChange(20, rebelScout, neutral_members, marine_members, cit, interpol_members);

        addSetupChange(21, cit);

        addSetupChange(22, neutral_members);

        addSetupChange(23, marine_members, neutral_members, threat_members);

        addSetupChange(24, cit);
        addSetupChange(24, SetupChange.ModifyRole(infiltrator.role, RoleModifier.AUTO_VEST, 1, 0));

        addSetupChange(25, neutral_members);

        addSetupChange(26, village_members);
        addSetupChange(26, SetupChange.ModifyRole(pirateCaptain.role, RoleModifier.AUTO_VEST, 1, 0));

        addSetupChange(27, cit);

        addSetupChange(28, cit);

        addSetupChange(29, interpol_members);
        addSetupChange(29, SetupChange.ModifyRole(infiltrator.role, RoleModifier.AUTO_VEST, 0, 1));

        addSetupChange(30, pirate_members);
        addSetupChange(30, SetupChange.ModifyRole(pirateCaptain.role, RoleModifier.AUTO_VEST, 0, 1));

        addSetupChange(31, village_members);
        addSetupChange(31, SetupChange.ModifyRole(infiltrator.role, RoleModifier.AUTO_VEST, 1, 0));

        addSetupChange(32, cit);

        addSetupChange(33, cit);
        addSetupChange(33, SetupChange.ModifyRole(pirateCaptain.role, RoleModifier.AUTO_VEST, 1, 0));

        addSetupChange(34, cit);

        addSetupChange(35, cit);
    }

    @Override
    public String getShortDescription() {
        return getDescription()[0];
    }

    @Override
    public String[] getDescription() {
        // TODO Auto-generated method stub
        return new String[] {
                "Fragos's 5 factioned 'Pirates' Game"
        };
    }

    @Override
    public String getName() {
        return "Treasure Island";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
