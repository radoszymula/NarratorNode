package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.roles.Citizen;
import game.roles.Goon;
import models.FactionRole;

public class Mountain extends Setup {

    public static final String KEY = "mountain";

    public Mountain(Narrator narrator) {
        Faction town = Town(narrator);
        Faction maf = Mafia(narrator);

        Prioritize(maf, town);
        town.setEnemies(maf);

        narrator.setRule(SetupModifier.DAY_START, Narrator.DAY_START);
        narrator.setRule(SetupModifier.HEAL_FEEDBACK, true);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        FactionRole citizen = Citizen.template(town);

        FactionRole goon = Goon.template(maf);

        addInitialRole(citizen, 4);
        addInitialRole(goon);

        addSetupChange(6, citizen);
        addSetupChange(7, citizen);
        addSetupChange(8, citizen);
        addSetupChange(9, citizen);
        addSetupChange(10, citizen);

        addSetupChange(11, goon);

        addSetupChange(12, citizen);
        addSetupChange(13, citizen);
        addSetupChange(14, citizen);
        addSetupChange(15, goon);

        addSetupChange(16, citizen);
        addSetupChange(17, citizen);
        addSetupChange(18, citizen);
        addSetupChange(19, citizen);
        addSetupChange(20, citizen);
    }

    @Override
    public String getShortDescription() {
        return getDescription()[0];
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "Only goons and villagers"
        };
    }

    @Override
    public String getName() {
        return "Mountainous";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
