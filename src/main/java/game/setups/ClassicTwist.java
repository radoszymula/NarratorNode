package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.roles.Bomb;
import game.roles.Citizen;
import game.roles.Doctor;
import game.roles.Goon;
import game.roles.Gunsmith;
import game.roles.Hidden;
import game.roles.Sheriff;
import game.roles.Snitch;
import models.FactionRole;
import services.HiddenService;

public class ClassicTwist extends Setup {

    public static final String KEY = "classictwist";

    public ClassicTwist(Narrator narrator) {
        Faction town = Town(narrator);
        Faction mafia = Mafia(narrator);

        Prioritize(mafia, town);

        town.addSheriffDetectableTeam(mafia);

        town.setEnemies(mafia);

        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.HEAL_SUCCESS_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_FEEDBACK, false);
        narrator.setRule(SetupModifier.GS_DAY_GUNS, true);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        FactionRole[] powerRoleComponents = new FactionRole[] {
                Bomb.template(town), Doctor.template(town), Gunsmith.template(town), Snitch.template(town)
        };

        Hidden powerRole = HiddenService.createHidden(narrator, "Power Role", powerRoleComponents);

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        FactionRole cit = Citizen.template(town);
        FactionRole sheriff = Sheriff.template(town);

        FactionRole goon = Goon.template(mafia);

        addInitialRole(cit, 3);
        addInitialRole(sheriff);
        addInitialRole(powerRole);
        addInitialRole(goon, 2);

        addSetupChange(8, cit);
        addSetupChange(9, cit);
        addSetupChange(10, goon);

        addSetupChange(11, powerRole);
        addSetupChange(12, cit);

        addSetupChange(13, goon);
        addSetupChange(14, cit);

        addSetupChange(15, goon, cit, sheriff);
        addSetupChange(16, cit);
    }

    @Override
    public String getShortDescription() {
        return "Epic Mafia's classic rotational setup";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "Simple Cop and random power role", "Power Role : Gunsmith, Bomb, Doctor, Snitch"
        };
    }

    @Override
    public String getName() {
        return "Classic Twist";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
