package game.setups;

import java.util.HashMap;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.roles.Agent;
import game.roles.Citizen;
import game.roles.Detective;
import game.roles.Doctor;
import game.roles.Goon;
import game.roles.Hidden;
import game.roles.Lookout;
import game.roles.Sheriff;
import game.roles.Stripper;
import models.FactionRole;
import services.HiddenService;

public class VanillaC extends Setup {

    public static final String KEY = "vanillac";

    public VanillaC(Narrator narrator) {
        Faction town = Town(narrator);
        Faction maf = Mafia(narrator);

        Prioritize(maf, town);
        town.setEnemies(maf);
        town.addSheriffDetectableTeam(maf);

        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.FOLLOW_GETS_ALL, false);
        narrator.setRule(SetupModifier.BLOCK_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_SUCCESS_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_FEEDBACK, false);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        HashMap<String, Hidden> randomSlots = new HashMap<>();

        FactionRole cit = Citizen.template(town);
        FactionRole stripper = Stripper.template(town);
        FactionRole sheriff = Sheriff.template(town);
        FactionRole detect = Detective.template(town);
        FactionRole lookout = Lookout.template(town);
        FactionRole doctor = Doctor.template(town);
        Hidden townPower = HiddenService.createHidden(narrator, "Hidden Town", stripper, sheriff, doctor, lookout,
                detect);
        randomSlots.put(townPower.getName(), townPower);

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        FactionRole goon = Goon.template(maf);
        FactionRole agent = Agent.template(maf);

        addInitialRole(cit, 3);
        addExposedInitialRole(townPower, 2);
        addInitialRole(goon);
        addInitialRole(agent);

        addSetupChange(8, cit);
        addSetupChange(9, cit);
        addSetupChange(10, goon);

        addSetupChange(11, townPower);
        addSetupChange(12, cit);

        addSetupChange(13, goon);
        addSetupChange(14, cit);

        addSetupChange(15, agent, cit, townPower);
        addSetupChange(16, cit);
    }

    @Override
    public String getShortDescription() {
        return "Classic Setup with basic confirmed roles";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "New Mafia Role - Agent", "Town roles are randomly generated"
        };
    }

    @Override
    public String getName() {
        return "Vanilla C";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
