package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Agent;
import game.roles.Amnesiac;
import game.roles.Blackmailer;
import game.roles.Bodyguard;
import game.roles.Bulletproof;
import game.roles.Burn;
import game.roles.Citizen;
import game.roles.Coroner;
import game.roles.Coward;
import game.roles.CultLeader;
import game.roles.Cultist;
import game.roles.Detective;
import game.roles.Disguiser;
import game.roles.Doctor;
import game.roles.Driver;
import game.roles.Executioner;
import game.roles.FactionKill;
import game.roles.Framer;
import game.roles.Godfather;
import game.roles.Goon;
import game.roles.Hidden;
import game.roles.Investigator;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Jester;
import game.roles.Lookout;
import game.roles.Marshall;
import game.roles.Mason;
import game.roles.MasonLeader;
import game.roles.MassMurderer;
import game.roles.Mayor;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.Spy;
import game.roles.Stripper;
import game.roles.Survivor;
import game.roles.Ventriloquist;
import game.roles.Veteran;
import game.roles.Vigilante;
import game.roles.Witch;
import models.FactionRole;
import services.FactionService;
import services.HiddenFactionRoleService;
import services.HiddenService;
import services.RoleAbilityModifierService;
import services.RoleService;

public class Sc2Mafia933 extends Setup {

    public static final String KEY = "sc2933";

    public Sc2Mafia933(Narrator narrator) {
        Faction benigns = Benign(narrator);
        Faction town = Town(narrator);
        Faction outcasts = Outcast(narrator);
        Faction cult = Cult(narrator);
        Faction maf = Mafia(narrator, 1);
        Faction threat = Killer(narrator);

        Prioritize(threat, maf, cult, outcasts, town, benigns);

        town.addSheriffDetectableTeam(maf, cult, threat);

        town.setEnemies(outcasts, maf, cult, threat);
        maf.setEnemies(cult, threat);
        cult.setEnemies(threat);

        maf.modifyAbility(AbilityModifier.ZERO_WEIGHTED, FactionKill.class, true);

        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.FOLLOW_GETS_ALL, false);
        narrator.setRule(SetupModifier.MASON_PROMOTION, true);
        narrator.setRule(SetupModifier.MASON_PROMOTION, false);
        narrator.setRule(SetupModifier.CONVERT_REFUSABLE, false);
        narrator.setRule(SetupModifier.CULT_KEEPS_ROLES, false);
        narrator.setRule(SetupModifier.CULT_PROMOTION, false);
        narrator.setRule(SetupModifier.CORONER_LEARNS_ROLES, true);
        narrator.setRule(SetupModifier.CIT_RATIO, 0);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        FactionRole vigilante = Vigilante.template(town);
        vigilante.role.addChargeModifier(2);
        FactionRole veteran = Veteran.template(town);
        veteran.role.addChargeModifier(3);

        Role marshallRole = RoleService.createRole(narrator, "Marshall", Marshall.class);
        marshallRole.addModifier(RoleModifier.UNIQUE, true);
        RoleAbilityModifierService.modify(marshallRole, Marshall.class, AbilityModifier.CHARGES, 2);
        FactionRole marshall = FactionService.createFactionRole(town, marshallRole);

        Role masonLeaderRole = RoleService.createRole(narrator, "Mason Leader", MasonLeader.class);
        masonLeaderRole.addChargeModifier(4);
        masonLeaderRole.addModifier(RoleModifier.UNIQUE, true);
        FactionRole masonLeader = FactionService.createFactionRole(town, masonLeaderRole);
        
        Role stripperRole = Stripper.template(narrator);
        Role jailorRole = Jailor.template(narrator);
        jailorRole.addChargeModifier(2);

        FactionRole bodyguard = Bodyguard.template(town);
        FactionRole busDriver = Driver.template(town);
        FactionRole citizen = Citizen.template(town);
        FactionRole coroner = Coroner.template(town);
        FactionRole detective = Detective.template(town);
        FactionRole doctor = Doctor.template(town);
        FactionRole jailor = FactionService.createFactionRole(town, jailorRole);
        FactionRole lookout = Lookout.template(town);
        FactionRole mason = Mason.template(town);
        FactionRole mayor = Mayor.template(town);
        FactionRole sheriff = Sheriff.template(town);
        FactionRole spy = Spy.template(town);
        FactionRole stripper = FactionService.createFactionRole(town, stripperRole);

        Hidden town_random = HiddenService.createHidden(narrator, Constants.TOWN_RANDOM_ROLE_NAME, bodyguard, busDriver,
                citizen, coroner, detective, doctor, lookout, jailor, marshall, mason, masonLeader, mayor, sheriff, spy,
                stripper, vigilante, veteran);

        Hidden town_killing = HiddenService.createHidden(narrator, Constants.TOWN_KILLING_ROLE_NAME, bodyguard, jailor,
                veteran, vigilante);

        Hidden town_invest = HiddenService.createHidden(narrator, Constants.TOWN_INVESTIGATIVE_ROLE_NAME, coroner,
                detective, lookout, sheriff);

        Hidden town_gov = HiddenService.createHidden(narrator, Constants.TOWN_GOVERNMENT_ROLE_NAME, marshall,
                masonLeader, mason, mayor);

        Hidden town_prot = HiddenService.createHidden(narrator, Constants.TOWN_PROTECTIVE_ROLE_NAME, bodyguard,
                busDriver, doctor, stripper);

        Hidden town_power = HiddenService.createHidden(narrator, Constants.TOWN_POWER_ROLE_NAME, busDriver, jailor, spy,
                veteran);

        Role godfatherRole = RoleService.createRole(narrator, "Godfather", Godfather.class, Bulletproof.class);
        godfatherRole.addModifier(RoleModifier.UNDETECTABLE, true);
        godfatherRole.addModifier(RoleModifier.UNIQUE, true);
        FactionRole godfather = FactionService.createFactionRole(maf, godfatherRole);

        FactionRole coward = Coward.template(maf);
        coward.role.addChargeModifier(3);

        FactionRole disguiser = Disguiser.template(maf);
        disguiser.role.addChargeModifier(1);

        FactionRole[] mafParts = new FactionRole[] {
                Goon.template(maf), Janitor.template(maf), Agent.template(maf), Investigator.template(maf),
                FactionService.createFactionRole(maf, stripperRole), FactionService.createFactionRole(maf, jailorRole), Blackmailer.template(maf), Framer.template(maf)
        };
        FactionRole[] modifiedMafia = new FactionRole[] {
                godfather, coward, disguiser
        };
        Hidden mafiaRandom = HiddenService.createHidden(narrator, Constants.MAFIA_RANDOM_ROLE_NAME, mafParts);
        HiddenFactionRoleService.addSpawnableRoles(narrator, mafiaRandom, modifiedMafia);

        FactionRole survivor = Survivor.template(benigns);
        survivor.role.addChargeModifier(4);

        FactionRole exec = Executioner.template(benigns);
        exec.role.addChargeModifier(1);

        Hidden neut_benign = HiddenService.createHidden(narrator, Constants.NEUTRAL_BENIGN_RANDOM_ROLE_NAME,
                Amnesiac.template(benigns), exec, Jester.template(benigns), survivor);

        Role cultistRole = RoleService.createRole(narrator, "Cultist", Cultist.class);
        FactionRole cultist = FactionService.createFactionRole(cult, cultistRole);
        Role cultLeaderRole = RoleService.createRole(narrator, "Cult Leader", CultLeader.class);
        cultLeaderRole.addModifier(RoleModifier.UNIQUE, true);
        FactionRole cultLeader = FactionService.createFactionRole(cult, cultLeaderRole);

        Hidden neut_evil = HiddenService.createHidden(narrator, Constants.NEUTRAL_EVIL_RANDOM_ROLE_NAME);
        FactionRole[] nEvilParts = new FactionRole[] {
                Witch.template(outcasts), Ventriloquist.template(outcasts)
        };

        // outcasts
        HiddenFactionRoleService.addSpawnableRoles(narrator, neut_evil, nEvilParts);
        HiddenFactionRoleService.addSpawnableRoles(narrator, neut_evil, cultist, cultLeader);

        Hidden neut_killing = HiddenService.createHidden(narrator, Constants.NEUTRAL_KILLING_RANDOM_ROLE_NAME,
                Burn.template(threat), SerialKiller.template(threat), MassMurderer.template(threat));

        Hidden neut_random = HiddenService.createHidden(narrator, Constants.NEUTRAL_RANDOM_ROLE_NAME);
        HiddenFactionRoleService.addSpawnableRoles(narrator, neut_random, neut_killing.getFactionRoles());
        HiddenFactionRoleService.addSpawnableRoles(narrator, neut_random, neut_benign.getFactionRoles());
        HiddenFactionRoleService.addSpawnableRoles(narrator, neut_random, neut_evil.getFactionRoles());

        Hidden any_rand = HiddenService.createHidden(narrator, Constants.ANY_RANDOM_ROLE_NAME);
        HiddenFactionRoleService.addSpawnableRoles(narrator, any_rand, town_random.getFactionRoles());
        HiddenFactionRoleService.addSpawnableRoles(narrator, any_rand, mafiaRandom.getFactionRoles());
        HiddenFactionRoleService.addSpawnableRoles(narrator, any_rand, neut_random.getFactionRoles());

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        addInitialRole(godfather);
        addInitialRole(mafiaRandom);
        addInitialRole(town_invest);
        addInitialRole(town_prot);
        addInitialRole(town_random, 3);

        addSetupChange(8, neut_benign);

        addSetupChange(9, neut_evil, neut_benign, town_random);

        addSetupChange(10, neut_killing, neut_evil, neut_benign);

        addSetupChange(11, neut_evil, neut_benign, town_power);

        addSetupChange(12, mafiaRandom, neut_evil, town_killing);

        addSetupChange(13, neut_benign, town_random, town_gov);

        addSetupChange(14, neut_evil, neut_benign, town_random);

        addSetupChange(15, neut_benign);

        addSetupChange(16, neut_benign);
    }

    @Override
    public String getShortDescription() {
        return "Typical setup on SC2 mod";
    }

    @Override
    public String[] getDescription() {
        return new String[0];
    }

    @Override
    public String getName() {
        return "Sc2Mafia's 933";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
