package game.setups;

import game.logic.Narrator;
import game.logic.support.Constants;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.CultLeader;
import game.roles.Hidden;
import models.FactionRole;
import services.FactionService;
import services.HiddenFactionRoleService;
import services.HiddenService;
import util.LookupUtil;

public class RoleMash extends Default {

    private Hidden swapName(String oldName, String newName) {
        Hidden oldRandom = LookupUtil.findHidden(narrator, oldName);
        narrator.deleteHidden(oldRandom);
        Hidden random = HiddenService.createHidden(narrator, newName, oldRandom.getFactionRoles());
        return random;
    }

    public static final String KEY = "mash";

    public RoleMash(Narrator narrator) {
        super(narrator);

        narrator.setRule(SetupModifier.CIT_RATIO, 45);

        init(narrator, KEY);

        Hidden town_random = swapName(Constants.TOWN_RANDOM_ROLE_NAME, "Hidden " + narrator.getFaction(TOWN_C));
        Hidden maf_random = swapName(Constants.MAFIA_RANDOM_ROLE_NAME, "Hidden " + narrator.getFaction(MAFIA_C));
        Hidden neut_benign = swapName(Constants.NEUTRAL_BENIGN_RANDOM_ROLE_NAME,
                "Hidden " + narrator.getFaction(BENIGN_C));
        Hidden neut_killing = swapName(Constants.NEUTRAL_KILLING_RANDOM_ROLE_NAME, "Hidden Threat");
        Hidden neut_evil = swapName(Constants.NEUTRAL_EVIL_RANDOM_ROLE_NAME,
                "Hidden " + narrator.getFaction(OUTCAST_C));

        FactionRole cLeader = LookupUtil.findFactionRole(narrator, "Cult Leader", CULT_C);
        cLeader.role.addChargeModifier(2);
        FactionRole cultistMinion = LookupUtil.findFactionRole(narrator, "Cultist", CULT_C);

        FactionRole enforcer = LookupUtil.findFactionRole(narrator, "Enforcer", TOWN_C);
        FactionRole clubber = LookupUtil.findFactionRole(narrator, "Clubber", TOWN_C);
        FactionRole bp = LookupUtil.findFactionRole(narrator, "Bulletproof", TOWN_C);
        FactionRole marshall = LookupUtil.findFactionRole(narrator, "Marshall", TOWN_C);

        town_random.removeRole(clubber);
        town_random.removeRole(enforcer);
        town_random.removeRole(bp);
        town_random.removeRole(marshall);

        neut_evil.removeRole(cLeader);
        neut_evil.removeRole(cultistMinion);
        HiddenFactionRoleService.addSpawnableRoles(narrator, neut_killing, cLeader);
        neut_evil.addModifier(RoleModifier.AUTO_VEST, 1);

        FactionService.deleteFaction(narrator, narrator.getFaction(YAKUZA_C).id);

        FactionRole bmer = LookupUtil.findFactionRole(narrator, "Blackmailer", MAFIA_C);
        FactionRole godfather = LookupUtil.findFactionRole(narrator, "Godfather", MAFIA_C);
        FactionRole goon = LookupUtil.findFactionRole(narrator, "Goon", MAFIA_C);

        maf_random.removeRole(godfather);
        maf_random.removeRole(goon);
        maf_random.removeRole(bmer);

        addInitialRole(maf_random, 2);
        addInitialRole(town_random, 5);

        addSetupChange(8, neut_benign);

        addSetupChange(9, neut_evil, neut_benign, town_random);

        addSetupChange(10, neut_killing, neut_evil, neut_benign);
        addSetupChange(10, SetupChange.RoleRotation(town_random, clubber));
        addSetupChange(10, SetupChange.RoleRotation(town_random, enforcer));

        addSetupChange(11, neut_evil, neut_benign, town_random);

        addSetupChange(12, maf_random, neut_evil, town_random);
        addSetupChange(12, SetupChange.RoleRotation(town_random, bp));
        addSetupChange(12, SetupChange.RoleRotation(maf_random, bmer));
        addSetupChange(12, SetupChange.ModifyCharge(cLeader.role, CultLeader.class, 3, 2));

        addSetupChange(13, neut_benign, town_random, town_random);

        addSetupChange(14, neut_evil, neut_benign, town_random);

        addSetupChange(15, neut_benign);

        addSetupChange(16, neut_benign);
    }

    @Override
    public String getShortDescription() {
        return "Voss's recommend setup. All roles enabled!";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "All roles enabled!"
        };
    }

    @Override
    public String getName() {
        return "Narrator Mash";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
