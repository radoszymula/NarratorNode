package game.setups;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import game.ai.RoleGroup;
import game.ai.RoleGroupList;
import game.logic.Faction;
import game.logic.FactionGroup;
import game.logic.Narrator;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.support.rules.FactionModifier;
import game.roles.Hidden;
import game.roles.Thief;
import models.FactionRole;
import models.SetupHidden;
import services.FactionService;
import services.RoleService;
import services.SetupHiddenService;
import util.HiddenUtil;

public class Setup {

    public static final String TOWN_C = Constants.BLUE;
    public static final String MAFIA_C = Constants.RED;
    public static final String YAKUZA_C = Constants.YELLOW;
    public static final String OUTCAST_C = Constants.SICKLY_GREEN;
    public static final String BENIGN_C = Constants.PINK;
    public static final String CULT_C = Constants.PURPLE;
    public static final String THREAT_C = Constants.BROWN;

    public static final boolean COLOR_COPY = true;

    public long id, ownerID;
    public String key, name = "";
    private String shortDescription = "";

    public Narrator narrator;

    public Setup() {
        this.initialRoles = new ArrayList<>();
        this.changes = new HashMap<>();
    }

    private ArrayList<SetupHidden> initialRoles;
    private HashMap<Integer, ArrayList<SetupChange>> changes;

    public void init(Narrator n, String name) {
        this.key = name;
        this.narrator = n;
    }

    public boolean isPreset() {
        return false;
    }

    static final String NK_DESCRIPTION = "Shunned by all, this twisted member wants revenge by killing all civilized people";
    static final String ANTI_TOWN_DESCRIP = "Shunned by the general populace, they plot their revenge in hiding and look to ally themselves with anyone that shares their goal.";

    public static Faction Cult(Narrator narrator) {
        return Cult(narrator, CULT_C, "Cult");
    }

    public static Faction Cult(Narrator narrator, String color, String name) {
        Faction cult = FactionService.createFaction(narrator, color, name,
                "A secretive organization, the cult looks to increase its ranks within the town");

        cult.setKnowsTeam(true);
        cult.setNightChat(true);
        cult.setCanRecruitFrom(false);
        cult.setRule(FactionModifier.PUNCH_SUCCESS, 15);

        return cult;
    }

    public static Faction Mafia(Narrator n) {
        return Mafia(n, 1);
    }

    public static Faction Mafia(Narrator narrator, int i) {
        String color, name;
        if(i == 1){
            color = MAFIA_C;
            name = "Mafia";
        }else{
            color = YAKUZA_C;
            name = "Yakuza";
        }
        return Mafia(narrator, color, name);
    }

    public static Faction Mafia(Narrator narrator, String color, String name) {
        Faction maf = FactionService.createFaction(narrator, color, name, MAF_DESCRIPTION);

        maf.setKillEnabled();
        maf.setKnowsTeam(true);
        maf.setNightChat(true);
        maf.setRule(FactionModifier.PUNCH_SUCCESS, 15);

        return maf;
    }

    public static Faction Town(Narrator narrator) {
        return Town(narrator, TOWN_C, "Town");
    }

    public static Faction Town(Narrator narrator, String color, String name) {
        Faction town = FactionService.createFaction(narrator, color, name,
                "The uninformed majority, these roles must eliminate all Mafia factions, the cult, and all evil neutrals.");

        town.setRule(FactionModifier.PUNCH_SUCCESS, 15);

        return town;
    }

    public static Faction Benign(Narrator narrator) {
        return Benign(narrator, BENIGN_C, "Benigns");
    }

    public static Faction Benign(Narrator narrator, String color, String name) {
        Faction benign = FactionService.createFaction(narrator, color, name, "Their motto is 'I don't care who wins.'");

        benign.setMustBeAliveToWin(true);
        benign.setRule(FactionModifier.PUNCH_SUCCESS, 15);

        return benign;
    }

    public static Faction Outcast(Narrator narrator) {
        Faction outcasts = FactionService.createFaction(narrator, OUTCAST_C, "Outcast", ANTI_TOWN_DESCRIP);

        outcasts.setMustBeAliveToWin(true);
        outcasts.setRule(FactionModifier.PUNCH_SUCCESS, 15);

        return outcasts;
    }

    public static Faction Killer(Narrator narrator, String color, String name) {
        Faction killer = FactionService.createFaction(narrator, color, name, NK_DESCRIPTION);

        killer.setMustBeAliveToWin(true);
        killer.setEnemies(killer);
        killer.setRule(FactionModifier.PUNCH_SUCCESS, 15);

        return killer;
    }

    public static Faction Killer(Narrator narrator) {
        return Killer(narrator, THREAT_C, "Threat");
    }

    protected static void Prioritize(FactionGroup... teamGroups) {
        for(int i = teamGroups.length - 1, j = 0; i >= 0; i--, j++)
            teamGroups[j].setPriority(i);
    }

    public static final String benigns_c = "#DDA0DD";
    public static final String sw_rebels_c = TOWN_C;
    public static final String empire_c = "#FF0000";

    public static final String REBEL_SOLDIER = "Rebel Soldier";
    public static final String OBI_WAN = "Obi Wan Kenobi";
    public static final String SW_DOCTOR = "21B Surgical Droid";

    /*
     * 20 man
     * http://www.sc2mafia.com/forum/showthread.php/33475-Special-Event-Cross-
     * Community-(20-players)?highlight=mafia+universe
     */

    public static Setup Vanilla20() {
        return null;
    }

    protected void addInitialRole(Hidden hidden, int count) {
        for(int j = 0; j < count; j++)
            initialRoles.add(new SetupHidden(hidden));
    }

    protected void addInitialRole(Hidden hidden) {
        initialRoles.add(new SetupHidden(hidden));
    }

    protected void addInitialRole(FactionRole factionRole) {
        if(!narrator.roles.contains(factionRole.role))
            RoleService.createRole(narrator, factionRole.role);
        if(!narrator.getFaction(factionRole.getColor())._roleSet.containsKey(factionRole.role))
            factionRole = FactionService.createFactionRole(factionRole.faction, factionRole.role);
        Hidden hidden = HiddenUtil.createHiddenSingle(narrator, factionRole);
        initialRoles.add(new SetupHidden(hidden));
    }

    protected void addInitialRole(FactionRole role, int count) {
        for(int j = 0; j < count; j++)
            addInitialRole(role);
    }

    protected void addExposedInitialRole(Hidden hidden, int count) {
        SetupHidden setupHidden;
        for(int j = 0; j < count; j++){
            setupHidden = new SetupHidden(hidden);
            setupHidden.isExposed = true;
            initialRoles.add(setupHidden);
        }
    }

    protected void addSetupChange(int playerCount, final Object... rts) {
        Hidden[] hiddens = new Hidden[rts.length];
        for(int i = 0; i < rts.length; i++){
            if(rts[i] instanceof FactionRole)
                hiddens[i] = HiddenUtil.createHiddenSingle(narrator, (FactionRole) rts[i]);
            else
                hiddens[i] = (Hidden) rts[i];
        }
        addSetupChange(playerCount, hiddens);
    }

    protected void addSetupChange(int playerCount, final Hidden... rts) {
        addSetupChange(playerCount, SetupChange.replaceAdd(rts));
    }

    protected void addSetupChange(int playerCount, SetupChange sc) {
        if(!changes.containsKey(playerCount))
            changes.put(playerCount, new ArrayList<SetupChange>());
        changes.get(playerCount).add(sc);
    }

    public Hidden getRandom(String hiddenName) {
        for(Hidden hidden: narrator.hiddens){
            if(hidden.getName().equals(hiddenName))
                return hidden;
        }
        return null;
    }

    public Collection<Hidden> getRandoms() {
        return narrator.hiddens;
    }

    public void applyRolesList() {
        int playerCount = Math.max(getMinPlayerCount(), narrator.getPlayerCount());
        playerCount = Math.min(playerCount, getMaxPlayerCount());
        applyRolesList(playerCount);
    }

    public void applyRolesList(int playerCount) {
        if(narrator.getRolesList().isEmpty()){
            for(SetupHidden setupHidden: initialRoles){
                SetupHiddenService.addSetupHidden(narrator, setupHidden.hidden, setupHidden.isExposed);
                if(setupHidden.isExposed)
                    SetupHiddenService.setExposed(setupHidden);
            }
        }

        int minRoleCount;
        if(changes.isEmpty())
            minRoleCount = getMinPlayerCount();
        else
            minRoleCount = Collections.min(changes.keySet()) - 1;
        int currentIndex = narrator.getRolesList().size() - (initialRoles.size() - minRoleCount);
        int change_quantity = playerCount - currentIndex;

        List<SetupChange> list;
        if(change_quantity > 0){ // need to add roles
            for(int i = currentIndex; i < playerCount && changes.containsKey(i + 1); i++){
                list = changes.get(i + 1);
                for(SetupChange sc: list)
                    sc.implementChange(this);
            }
            return;
        }

        if(playerCount < minRoleCount)
            return;

        // need to undo some changes here
        // this is for me to get from 16 to 15
        // from 15 to 16, i looked at 16, and made the change
        // i have to look at 16's change to get back to 15
        for(int i = currentIndex; i > playerCount && changes.containsKey(i); i--){
            list = changes.get(i);
            for(SetupChange sc: list)
                sc.undoChange(this);
        }

    }

    public int getMinPlayerCount() {
        RoleGroupList total = new RoleGroupList();
        for(SetupHidden setupHidden: initialRoles){
            if(!setupHidden.hidden.contains(Thief.class))
                total.add(new RoleGroup().add(setupHidden.hidden));
        }
        int count = total.size();
        if(count == initialRoles.size())
            return total.size();
        while (true){
            try{
                Narrator.hasOpposingSpawns(total);
                count--;
            }catch(NarratorException e){
                break;
            }
        }
        return count + 1;
    }

    public int getMaxPlayerCount() {
        if(changes.isEmpty())
            return initialRoles.size();
        return Collections.max(changes.keySet());
    }

    public static final String MAF_DESCRIPTION = "The informed minority, these roles must eliminate the Town, other Mafia factions, the cult and evil killing neutrals.";

    public static final String[] SETUP_LIST = {
            Classic.KEY, VanillaB.KEY, VanillaC.KEY, VanillaD.KEY, VanillaE.KEY,

            MediumG.KEY, MediumH.KEY, MediumI.KEY,

            RoleMash.KEY, MUMash.KEY,

            ClassicTwist.KEY, FiveP.KEY, UnreliableCops.KEY, Cop9er.KEY, Voltron.KEY, LiarsClub.KEY,

            GoT.KEY, Mountain.KEY, Pirates.KEY, BraveNewWorld.KEY, Sc2Mafia933.KEY, FreeForAll.KEY
    };

    public static Setup GetSetup(Narrator n, String setupName) {
        Setup s = null;
        switch (setupName){
        case Default.KEY:
            s = new Default(n.removeAllFactionsAndRoles());
            break;

        case Classic.KEY:
            s = new Classic(n.removeAllFactionsAndRoles());
            break;
        case VanillaB.KEY:
            s = new VanillaB(n.removeAllFactionsAndRoles());
            break;
        case VanillaC.KEY:
            s = new VanillaC(n.removeAllFactionsAndRoles());
            break;
        case VanillaD.KEY:
            s = new VanillaD(n.removeAllFactionsAndRoles());
            break;
        case VanillaE.KEY:
            s = new VanillaE(n.removeAllFactionsAndRoles());
            break;

        case MediumG.KEY:
            s = new MediumG(n.removeAllFactionsAndRoles());
            break;
        case MediumH.KEY:
            s = new MediumH(n.removeAllFactionsAndRoles());
            break;
        case MediumI.KEY:
            s = new MediumI(n.removeAllFactionsAndRoles());
            break;

        case ClassicTwist.KEY:
            s = new ClassicTwist(n.removeAllFactionsAndRoles());
            break;
        case FiveP.KEY:
            s = new FiveP(n.removeAllFactionsAndRoles());
            break;
        case UnreliableCops.KEY:
            s = new UnreliableCops(n.removeAllFactionsAndRoles());
            break;
        case Cop9er.KEY:
            s = new Cop9er(n.removeAllFactionsAndRoles());
            break;
        case Voltron.KEY:
            s = new Voltron(n.removeAllFactionsAndRoles());
            break;
        case LiarsClub.KEY:
            s = new LiarsClub(n.removeAllFactionsAndRoles());
            break;

        case GoT.KEY:
            s = new GoT(n.removeAllFactionsAndRoles());
            break;
        case BraveNewWorld.KEY:
            s = new BraveNewWorld(n.removeAllFactionsAndRoles());
            break;
        case Pirates.KEY:
            s = new Pirates(n.removeAllFactionsAndRoles());
            break;

        case Mountain.KEY:
            s = new Mountain(n.removeAllFactionsAndRoles());
            break;

        case Sc2Mafia933.KEY:
            s = new Sc2Mafia933(n.removeAllFactionsAndRoles());
            break;
        case RoleMash.KEY:
            s = new RoleMash(n.removeAllFactionsAndRoles());
            break;
        case MUMash.KEY:
            s = new MUMash(n.removeAllFactionsAndRoles());
            break;
        case FreeForAll.KEY:
            s = new FreeForAll(n.removeAllFactionsAndRoles());
            break;
        }

        return s;
    }

    public String getShortDescription() {
        return this.shortDescription;
    }

    public String[] getDescription() {
        return new String[] {};
    }

    public String getName() {
        return this.name;
    }

    public String getGenre() {
        return null;
    }
}
