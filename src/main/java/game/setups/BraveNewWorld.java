package game.setups;

import game.logic.Faction;
import game.logic.FactionList;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.roles.Agent;
import game.roles.Amnesiac;
import game.roles.Architect;
import game.roles.Armorsmith;
import game.roles.Baker;
import game.roles.Blacksmith;
import game.roles.Bodyguard;
import game.roles.Burn;
import game.roles.Citizen;
import game.roles.Clubber;
import game.roles.Coroner;
import game.roles.CultLeader;
import game.roles.Detective;
import game.roles.Disguiser;
import game.roles.Doctor;
import game.roles.Douse;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.Enforcer;
import game.roles.Executioner;
import game.roles.Framer;
import game.roles.Ghost;
import game.roles.Godfather;
import game.roles.GraveDigger;
import game.roles.Gunsmith;
import game.roles.Hidden;
import game.roles.Interceptor;
import game.roles.Investigator;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Jester;
import game.roles.Lookout;
import game.roles.MassMurderer;
import game.roles.Mayor;
import game.roles.Poisoner;
import game.roles.ProxyKill;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.Stripper;
import game.roles.Survivor;
import game.roles.TeamTakedown;
import game.roles.Undouse;
import game.roles.Veteran;
import game.roles.Vigilante;
import game.roles.Witch;
import game.roles.support.Bread;
import models.FactionRole;
import services.FactionRoleService;
import services.FactionService;
import services.HiddenService;
import services.RoleAbilityModifierService;
import services.RoleService;

public class BraveNewWorld extends Setup {

    // http://www.sc2mafia.com/forum/showthread.php/19219-The-Setup
    public static final String savages_c = Constants.PURPLE;
    public static final String somadealers_c = Constants.RED;
    public static final String SAVAGE_LEADER = "Savage Leader";
    public static final String DELTA_N = "Delta";
    public static final String KEY = "brave";

    public BraveNewWorld(Narrator narrator) {
        String savages_n = "Savage";
        String mafia_n = "Soma Dealer";
        String town_n = "Town";

        Faction mafia = Mafia(narrator, somadealers_c, mafia_n).setCanRecruitFrom(false);
        Faction town = Town(narrator, TOWN_C, town_n);
        Faction cult = Cult(narrator, savages_c, savages_n).setCanRecruitFrom(false);
        Faction benigns = Benign(narrator, BENIGN_C, "Epsilon");
        Faction threat = Outcast(narrator).setCanRecruitFrom(false);

        Prioritize(new FactionList(cult, mafia, threat), town, benigns);

        mafia.setDescription("");
        town.setDescription("");
        cult.setDescription("");
        threat.setDescription("");

        town.addSheriffDetectableTeam(cult, mafia, threat);

        town.setEnemies(cult, mafia, threat);
        mafia.setEnemies(cult);

        narrator.setRule(SetupModifier.DAY_START, Narrator.DAY_START);
        narrator.setRule(SetupModifier.CHARGE_VARIABILITY, 0);
        narrator.setRule(SetupModifier.DIFFERENTIATED_FACTION_KILLS, true);

        narrator.setRule(SetupModifier.GS_DAY_GUNS, false);
        narrator.setRule(SetupModifier.ENFORCER_LEARNS_NAMES, true);
        narrator.setRule(SetupModifier.CORONER_EXHUMES, true);
        narrator.setRule(SetupModifier.BREAD_PASSING, true);
        narrator.setRule(SetupModifier.MAYOR_VOTE_POWER, 3);
        narrator.setRule(SetupModifier.SELF_BREAD_USAGE, false);

        narrator.setRule(SetupModifier.HEAL_BLOCKS_POISON, true);
        narrator.setRule(SetupModifier.GUARD_REDIRECTS_CONVERT, true);
        narrator.setRule(SetupModifier.GUARD_REDIRECTS_DOUSE, true);
        narrator.setRule(SetupModifier.GUARD_REDIRECTS_POISON, true);

        narrator.setRule(SetupModifier.CULT_POWER_ROLE_CD, 0);
        narrator.setRule(SetupModifier.CULT_KEEPS_ROLES, false);
        narrator.setRule(SetupModifier.CONVERT_REFUSABLE, false);
        narrator.setRule(SetupModifier.CULT_PROMOTION, true);
        narrator.setRule(SetupModifier.GD_REANIMATE, false);

        narrator.setRule(SetupModifier.JANITOR_GETS_ROLES, true);

        narrator.setRule(SetupModifier.ARCH_BURN, true);
        narrator.setRule(SetupModifier.ARSON_DAY_IGNITES, SetupModifiers.UNLIMITED);
        narrator.setRule(SetupModifier.AMNESIAC_MUST_REMEMBER, true);
        narrator.setRule(SetupModifier.JESTER_CAN_ANNOY, true);
        narrator.setRule(SetupModifier.JESTER_KILLS, 25);
        narrator.setRule(SetupModifier.EXEC_TOWN, true);

        narrator.setRule(SetupModifier.PROXY_UPGRADE, true);

        final String CANNIBAL = "Cannibal";
        narrator.setAlias(CultLeader.MINION_ALIAS, CANNIBAL);
        narrator.setAlias(Bread.ALIAS, "soma");

        Role architectRole = Architect.template(narrator);
        Role driver = Driver.template(narrator);
        Role investigator = Investigator.template(narrator);
        Role jailorRole = Jailor.template(narrator);
        Role stripperRole = Stripper.template(narrator);
        stripperRole.addModifier(RoleModifier.UNBLOCKABLE, true);

        // Mafia only roles
        FactionRole disguiser = Disguiser.template(mafia);
        disguiser.role.addChargeModifier(1);

        Role godfatherRole = RoleService.createRole(narrator, "Godfather", Godfather.class);
        godfatherRole.addModifier(RoleModifier.UNDETECTABLE, true);
        godfatherRole.addModifier(RoleModifier.UNIQUE, true);
        FactionRole godfather = FactionService.createFactionRole(mafia, godfatherRole);

        FactionRole graveDigger = GraveDigger.template(mafia);
        graveDigger.role.addCooldownModifier(GraveDigger.class, 1);

        FactionRole interceptor = Interceptor.template(mafia);
        interceptor.role.addModifier(RoleModifier.UNIQUE, true);

        FactionRole kidnapper = FactionService.createFactionRole(mafia, jailorRole);
        FactionRoleService.addModifier(kidnapper, AbilityModifier.CHARGES, Jailor.class, 2);

        FactionRole poisoner = Poisoner.template(mafia);
        RoleAbilityModifierService.modify(poisoner.role, Poisoner.class, AbilityModifier.CHARGES, 2);
        poisoner.role.addModifier(RoleModifier.UNIQUE, true);

        FactionRole[] somaRoles = {
                Agent.template(mafia), DrugDealer.template(mafia), Framer.template(mafia), Janitor.template(mafia),

                // mafia only roles
                graveDigger, interceptor, kidnapper, poisoner,

                // duplicated roles
                FactionService.createFactionRole(mafia, architectRole), FactionService.createFactionRole(mafia, driver),
                FactionService.createFactionRole(mafia, investigator), FactionService.createFactionRole(mafia, stripperRole),
        };

        Hidden soma_members = HiddenService.createHidden(narrator, "Soma Dealer Hidden", somaRoles);

        // Savage Roles
        Role cultLeader = RoleService.createRole(narrator, SAVAGE_LEADER, CultLeader.class, ProxyKill.class,
                TeamTakedown.class);
        RoleAbilityModifierService.modify(cultLeader, CultLeader.class, AbilityModifier.CHARGES, 2);

        // Town only roles
        FactionRole architect = FactionService.createFactionRole(town, architectRole);

        Role blacksmithRole = RoleService.createRole(narrator, "Blacksmith", Armorsmith.class, Gunsmith.class);
        FactionRole blacksmith = FactionService.createFactionRole(town, blacksmithRole);

        FactionRole bodyguard = Bodyguard.template(town);
        FactionRole busDriver = FactionService.createFactionRole(town, driver);

        Role clubberRole = RoleService.createRole(narrator, "Clubber", Clubber.class, Baker.class);
        clubberRole.addModifier(RoleModifier.UNCONVERTABLE, true);
        clubberRole.addCooldownModifier(Baker.class, 1);
        FactionRole clubber = FactionService.createFactionRole(town, clubberRole);

        FactionRole coroner = Coroner.template(town);

        Role deltaRole = RoleService.createRole(narrator, DELTA_N, Citizen.class);
        FactionRole delta = FactionService.createFactionRole(town, deltaRole);

        FactionRole doctor = Doctor.template(town);
        FactionRole detective = Detective.template(town);

        Role enforcerRole = RoleService.createRole(narrator, "Enforcer", Enforcer.class, Baker.class);
        enforcerRole.addModifier(RoleModifier.UNCONVERTABLE, true);
        enforcerRole.addCooldownModifier(Baker.class, 1);
        FactionRole enforcer = FactionService.createFactionRole(town, enforcerRole);

        FactionRole jailor = FactionService.createFactionRole(town, jailorRole);
        FactionRole lookout = Lookout.template(town);

        Role mayorRole = RoleService.createRole(narrator, "Mayor", Mayor.class, Baker.class);
        mayorRole.addModifier(RoleModifier.UNCONVERTABLE, true);
        mayorRole.addModifier(RoleModifier.UNIQUE, true);
        mayorRole.addCooldownModifier(Baker.class, 1);
        FactionRole mayor = FactionService.createFactionRole(town, mayorRole);

        FactionRole sheriff = Sheriff.template(town);
        FactionRole stripper = FactionService.createFactionRole(town, stripperRole);
        FactionRole vigilante = Vigilante.template(town);
        FactionRole veteran = Veteran.template(town);

        Hidden town_members = HiddenService.createHidden(narrator, "Town Member", architect, blacksmith, bodyguard,
                busDriver, clubber, coroner, delta, detective, doctor, enforcer, jailor, lookout, sheriff, stripper,
                vigilante, veteran);

        Hidden alphas = HiddenService.createHidden(narrator, "Hidden Alpha", enforcer, mayor);

        Hidden betas = HiddenService.createHidden(narrator, "Hidden Beta", architect, blacksmith, bodyguard, doctor,
                busDriver, jailor, stripper, vigilante, veteran);

        Hidden gammas = HiddenService.createHidden(narrator, "Hidden Gamma", coroner, detective, lookout, sheriff);

        Role armsDealer = RoleService.createRole(narrator, "Arms Dealer", Blacksmith.class);
        Role arsonist = RoleService.createRole(narrator, "Arsonist", Douse.class, Burn.class, Undouse.class);
        arsonist.addModifier(RoleModifier.AUTO_VEST, 1);

        Role massMurderer = RoleService.createRole(narrator, "Mass Murderer", MassMurderer.class);
        massMurderer.addModifier(RoleModifier.AUTO_VEST, 1);

        Role devourer = RoleService.createRole(narrator, "Devourer", Jailor.class);
        devourer.addModifier(AbilityModifier.JAILOR_CLEAN_ROLES, Jailor.class, 2);

        Role serialKiller = RoleService.createRole(narrator, "Serial Killer", SerialKiller.class);
        serialKiller.addModifier(RoleModifier.AUTO_VEST, 1);
        serialKiller.addModifier(RoleModifier.UNBLOCKABLE, true);

        Hidden evilNeutrals = HiddenService.createHidden(narrator, "Hidden " + threat.getName(),
                FactionService.createFactionRole(threat, armsDealer), FactionService.createFactionRole(threat, arsonist),
                FactionService.createFactionRole(threat, devourer), FactionService.createFactionRole(threat, massMurderer),
                FactionService.createFactionRole(threat, serialKiller), Witch.template(threat));

        FactionRole amnesiac = Amnesiac.template(benigns);
        FactionRole ghost = Ghost.template(benigns);
        FactionRole executioner = Executioner.template(benigns);
        FactionRole jester = Jester.template(benigns);
        FactionRole survivor = Survivor.template(benigns);
        survivor.role.addChargeModifier(2);

        Hidden epsilons = HiddenService.createHidden(narrator, "Hidden " + benigns.getName(), amnesiac, executioner,
                ghost, jester, survivor);

        init(narrator, KEY);

        narrator.setRule(SetupModifier.CIT_RATIO, 66);
        addInitialRole(delta, 4); // 15
        addInitialRole(alphas);
        addInitialRole(betas);
        addInitialRole(gammas);
        addInitialRole(town_members, 3);
        addInitialRole(soma_members, 3);
        addInitialRole(HiddenService.createHidden(narrator, "Savage Leader", FactionService.createFactionRole(cult, cultLeader)));
        addInitialRole(epsilons);

        addSetupChange(16, town_members);
        addSetupChange(16, SetupChange.CitRatio(50, 66));

        addSetupChange(17, delta, soma_members, godfather);// giving mafia the bp here
        addSetupChange(18, delta);

        addSetupChange(19, evilNeutrals);

        addSetupChange(20, town_members);
        addSetupChange(20, SetupChange.CitRatio(40, 50));
        addSetupChange(20, SetupChange.ModifyRole(cultLeader, RoleModifier.AUTO_VEST, 1, 0));
        addSetupChange(20, SetupChange.ModifyRole(godfatherRole, RoleModifier.AUTO_VEST, 1, 0));
        addSetupChange(20, SetupChange.RoleRotation(alphas, clubber));
        addSetupChange(20, SetupChange.RoleRotation(town_members, clubber));

        addSetupChange(21, town_members);
        addSetupChange(21, SetupChange.CitRatio(50, 40));

        addSetupChange(22, soma_members);
        addSetupChange(22, SetupChange.ModifyCharge(cultLeader, CultLeader.class, 3, 2));

        addSetupChange(23, town_members);
        addSetupChange(23, SetupChange.CitRatio(43, 50));

        addSetupChange(24, delta);
        addSetupChange(24, SetupChange.ModifyCharge(survivor.role, Survivor.class, 3, 2));

        addSetupChange(25, soma_members);
        addSetupChange(25, SetupChange.ModifyCharge(cultLeader, CultLeader.class, 4, 3));
        addSetupChange(25, SetupChange.RuleChange(SetupModifier.SELF_BREAD_USAGE, true));

        addSetupChange(26, gammas);

        addSetupChange(27, delta);

        addSetupChange(28, epsilons);

        addSetupChange(29, town_members);
        addSetupChange(29, SetupChange.CitRatio(38, 43));

        addSetupChange(30, delta);
        addSetupChange(30, SetupChange.ModifyCharge(survivor.role, Survivor.class, 4, 3));

        addSetupChange(31, town_members);
        addSetupChange(31, SetupChange.CitRatio(33, 38));

        addSetupChange(32, alphas);

        addSetupChange(33, soma_members);
        addSetupChange(33, SetupChange.ModifyCharge(cultLeader, CultLeader.class, 5, 4));

        addSetupChange(34, betas);
        addSetupChange(34, SetupChange.CitRatio(44, 33));

        addSetupChange(35, delta);

        addSetupChange(36, epsilons);
    }

    @Override
    public String getShortDescription() {
        return getDescription()[0];
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "Clementine's Dystopian Mafia and Cult Game"
        };
    }

    @Override
    public String getName() {
        return "Brave New World";
    }

    @Override
    public boolean isPreset() {
        return true;
    }

}
