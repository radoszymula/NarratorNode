package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.roles.Agent;
import game.roles.Blackmailer;
import game.roles.Citizen;
import game.roles.Detective;
import game.roles.Doctor;
import game.roles.Goon;
import game.roles.Hidden;
import game.roles.Lookout;
import game.roles.Sheriff;
import game.roles.Stripper;
import models.FactionRole;
import services.HiddenService;

public class VanillaD extends Setup {

    public static final String KEY = "vanillad";

    public VanillaD(Narrator narrator) {
        Faction town = Town(narrator);
        Faction maf = Mafia(narrator);

        Prioritize(maf, town);
        town.setEnemies(maf);
        town.addSheriffDetectableTeam(maf);

        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.FOLLOW_GETS_ALL, false);
        narrator.setRule(SetupModifier.BLOCK_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_SUCCESS_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_FEEDBACK, false);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        FactionRole cit = Citizen.template(town);
        FactionRole stripper = Stripper.template(town);
        FactionRole sheriff = Sheriff.template(town);
        FactionRole detect = Detective.template(town);
        FactionRole lookout = Lookout.template(town);
        FactionRole doctor = Doctor.template(town);
        Hidden townPower = HiddenService.createHidden(narrator, "Hidden Town", stripper, sheriff, doctor, lookout,
                detect);

        FactionRole goon = Goon.template(maf);
        FactionRole agent = Agent.template(maf);
        FactionRole bm = Blackmailer.template(maf);
        Hidden mafiaPower = HiddenService.createHidden(narrator, "Hidden Mafia", agent, bm);

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        addInitialRole(cit, 3);
        addInitialRole(townPower, 2);
        addInitialRole(goon);
        addInitialRole(mafiaPower);

        addSetupChange(8, cit);
        addSetupChange(9, cit);
        addSetupChange(10, mafiaPower);

        addSetupChange(11, townPower);
        addSetupChange(12, cit);

        addSetupChange(13, goon);
        addSetupChange(14, cit);

        addSetupChange(15, mafiaPower, cit, townPower);
        addSetupChange(16, cit);
    }

    @Override
    public String getShortDescription() {
        return "Classic game with 'blackmailer'";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "New Mafia Role - Blackmailer", "Town/Mafia roles are randomly generated and hidden"
        };
    }

    @Override
    public String getName() {
        return "Silence of the Lambs";
    }

    @Override
    public boolean isPreset() {
        return true;
    }

}
