package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.rules.SetupModifier;
import game.roles.Amnesiac;
import game.roles.Architect;
import game.roles.Bodyguard;
import game.roles.Citizen;
import game.roles.Clubber;
import game.roles.CultLeader;
import game.roles.Cultist;
import game.roles.Detective;
import game.roles.Doctor;
import game.roles.Enforcer;
import game.roles.Hidden;
import game.roles.Jailor;
import game.roles.Jester;
import game.roles.Lookout;
import game.roles.Mason;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.Stripper;
import models.FactionRole;
import services.FactionService;
import services.HiddenService;
import services.RoleService;

public class MediumI extends Setup {

    public static final String KEY = "mediumi";

    public MediumI(Narrator narrator) {
        Faction town = Town(narrator);
        Faction cult = Cult(narrator);
        Faction killer = Killer(narrator);
        Faction benign = Benign(narrator);

        Prioritize(killer, cult, town, benign);

        town.addSheriffDetectableTeam(cult, killer);

        town.setEnemies(cult, killer);
        cult.setEnemies(killer);

        killer.setCanRecruitFrom(false);

        narrator.setRule(SetupModifier.CHARGE_VARIABILITY, 0);

        narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        narrator.setRule(SetupModifier.FOLLOW_GETS_ALL, false);
        narrator.setRule(SetupModifier.BLOCK_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_SUCCESS_FEEDBACK, false);
        narrator.setRule(SetupModifier.HEAL_FEEDBACK, false);
        narrator.setRule(SetupModifier.ENFORCER_LEARNS_NAMES, false);
        narrator.setRule(SetupModifier.MASON_PROMOTION, false);
        narrator.setRule(SetupModifier.AMNESIAC_MUST_REMEMBER, false);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        FactionRole cit = Citizen.template(town);
        FactionRole enforcer = Enforcer.template(town);
        FactionRole bg = Bodyguard.template(town);
        FactionRole clubber = Clubber.template(town);
        FactionRole stripper = Stripper.template(town);
        FactionRole sheriff = Sheriff.template(town);
        FactionRole doctor = Doctor.template(town);
        FactionRole lookout = Lookout.template(town);
        FactionRole detective = Detective.template(town);
        FactionRole mason = Mason.template(town);
        FactionRole architect = Architect.template(town);
        FactionRole jailor = Jailor.template(town);
        clubber.role.addChargeModifier(1);

        Hidden tpr = HiddenService.createHidden(narrator, "Hidden Town", stripper, sheriff, doctor, lookout, detective,
                mason, clubber, architect, jailor);

        final Hidden benign_random = HiddenService.createHidden(narrator, "Hidden Benign", Jester.template(benign),
                Amnesiac.template(benign));

        Role cultLeaderRole = RoleService.createRole(narrator, "Cult Leader", CultLeader.class);
        cultLeaderRole.addChargeModifier(2);
        FactionRole cultLeader = FactionService.createFactionRole(cult, cultLeaderRole);

        Role cultistRole = RoleService.createRole(narrator, "Cultist", Cultist.class);
        FactionRole cultist = FactionService.createFactionRole(cult, cultistRole);

        FactionRole sk = SerialKiller.template(killer);
        sk.role.addCooldownModifier(SerialKiller.class, 1);

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        addInitialRole(cit, 4);
        addInitialRole(tpr, 2);
        addInitialRole(cultLeader);

        addSetupChange(8, benign_random);
        addSetupChange(9, SetupChange.RoleRotation(tpr, enforcer));
        addSetupChange(9, cultist, benign_random, tpr);

        addSetupChange(10, sk);
        addSetupChange(10, SetupChange.ModifyCooldown(cultLeaderRole, CultLeader.class, 1, 0));
        addSetupChange(10, SetupChange.RoleRotation(tpr, bg));
        addSetupChange(11, cit);

        addSetupChange(12, benign_random);
        addSetupChange(13, cit);
        addSetupChange(13, SetupChange.ModifyCooldown(sk.role, SerialKiller.class, 0, 1));
        addSetupChange(13, SetupChange.ModifyCooldown(cultLeaderRole, CultLeader.class, 0, 1));

        addSetupChange(14, cit);
        addSetupChange(15, benign_random, cit, tpr);
        addSetupChange(15, SetupChange.ModifyCharge(cultLeaderRole, CultLeader.class, 3, 2));

        addSetupChange(16, cit);
    }

    @Override
    public String getShortDescription() {
        return "Basic Cult Game";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "New Town Roles - Mason, Clubber, Enforcer, Architect, Bodyguard, Jailor",
                "New Neutral Roles - Cultist, Cult Leader, Amnesiac", "No mafia!"
        };
    }

    @Override
    public String getName() {
        return "Worshippers of Cthulu!";
    }

    @Override
    public boolean isPreset() {
        return true;
    }

}
