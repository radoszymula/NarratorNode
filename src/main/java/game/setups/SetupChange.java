package game.setups;

import game.logic.Role;
import game.logic.support.RoleTemplate;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import game.roles.Hidden;
import models.FactionRole;
import services.HiddenFactionRoleService;
import services.RoleAbilityModifierService;
import services.SetupHiddenService;

public abstract class SetupChange {

    public abstract void implementChange(Setup s);

    public abstract void undoChange(Setup s);

    public static SetupChange addHidden(final Hidden hidden) {
        return new SetupChange() {
            @Override
            public void implementChange(Setup setup) {
                SetupHiddenService.addSetupHidden(setup.narrator, hidden, false);
            }

            @Override
            public void undoChange(Setup setup) {
                setup.narrator.removeSetupHidden(hidden);
            }
        };
    }

    public static SetupChange replaceAdd(final Hidden... roles) {
        return new SetupChange() {
            @Override
            public void implementChange(Setup s) {
                for(int i = 0; i < roles.length; i++){
                    if(i % 2 == 0){
                        SetupHiddenService.addSetupHidden(s.narrator, roles[i], false);
                    }else {
                        s.narrator.removeSetupHidden(roles[i]);
                    }
                }
            }

            @Override
            public void undoChange(Setup s) {
                for(int i = 0; i < roles.length; i++){
                    if(i % 2 == 0){
                        s.narrator.removeSetupHidden(roles[i]);
                    }else
                        SetupHiddenService.addSetupHidden(s.narrator, roles[i], false);
                }
            }
        };
    }

    public static SetupChange RuleChange(final SetupModifier rule, final boolean val) {
        return new SetupChange() {
            @Override
            public void implementChange(Setup s) {
                s.narrator.setRule(rule, val);
            }

            @Override
            public void undoChange(Setup s) {
                s.narrator.setRule(rule, !val);
            }
        };
    }

    public static SetupChange CitRatio(final int newInt, final int oldInt) {
        return new SetupChange() {
            @Override
            public void implementChange(Setup s) {
                s.narrator.setRule(SetupModifier.CIT_RATIO, newInt);
            }

            @Override
            public void undoChange(Setup s) {
                s.narrator.setRule(SetupModifier.CIT_RATIO, oldInt);
            }
        };
    }

    public static SetupChange ModifyRole(final RoleTemplate rt, final RoleModifier modifier, final boolean b) {
        return new SetupChange() {
            @Override
            public void implementChange(Setup s) {
                rt.addModifier(modifier, b);
            }

            @Override
            public void undoChange(Setup s) {
                rt.addModifier(modifier, !b);
            }
        };
    }

    public static SetupChange ModifyRole(final RoleTemplate rt, final RoleModifier modifier, final int newI,
            final int oldI) {
        return new SetupChange() {
            @Override
            public void implementChange(Setup s) {
                rt.addModifier(modifier, newI);
            }

            @Override
            public void undoChange(Setup s) {
                rt.addModifier(modifier, oldI);
            }
        };
    }

    public static SetupChange ModifyCharge(final Role role, final Class<? extends Ability> abilityClass, final int newI,
            final int oldI) {
        return new SetupChange() {
            @Override
            public void implementChange(Setup s) {
                RoleAbilityModifierService.modify(role, abilityClass, AbilityModifier.CHARGES, newI);
            }

            @Override
            public void undoChange(Setup s) {
                RoleAbilityModifierService.modify(role, abilityClass, AbilityModifier.CHARGES, oldI);
            }
        };
    }

    public static SetupChange ModifyCooldown(final Role rt, final Class<? extends Ability> c, final int newI,
            final int oldI) {
        return new SetupChange() {
            @Override
            public void implementChange(Setup s) {
                rt.addCooldownModifier(c, newI);
            }

            @Override
            public void undoChange(Setup s) {
                rt.addCooldownModifier(c, oldI);
            }
        };
    }

    public static SetupChange RoleRotation(final Hidden rm, final FactionRole role) {
        return new SetupChange() {
            @Override
            public void implementChange(Setup setup) {
                HiddenFactionRoleService.addSpawnableRoles(setup.narrator, rm, role);
            }

            @Override
            public void undoChange(Setup s) {
                rm.removeRole(role);
            }
        };
    }

    public static SetupChange AddAbility(final Hidden rt, final Class<? extends Ability> a) {
        return new SetupChange() {
            @Override
            public void implementChange(Setup s) {
                rt.addAbility(a);
            }

            @Override
            public void undoChange(Setup s) {
                rt.removeAbility(a);
            }
        };
    }
}
