package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.Agent;
import game.roles.Architect;
import game.roles.Armorsmith;
import game.roles.ArmsDetector;
import game.roles.Assassin;
import game.roles.Baker;
import game.roles.Blacksmith;
import game.roles.Bodyguard;
import game.roles.Bulletproof;
import game.roles.Citizen;
import game.roles.Coroner;
import game.roles.Coward;
import game.roles.Detective;
import game.roles.Disguiser;
import game.roles.Doctor;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.ElectroManiac;
import game.roles.FactionKill;
import game.roles.Framer;
import game.roles.GraveDigger;
import game.roles.Gunsmith;
import game.roles.Hidden;
import game.roles.Investigator;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Lookout;
import game.roles.Mason;
import game.roles.Mayor;
import game.roles.Operator;
import game.roles.ParityCheck;
import game.roles.Poisoner;
import game.roles.Sheriff;
import game.roles.Sleepwalker;
import game.roles.Snitch;
import game.roles.Spy;
import game.roles.Stripper;
import game.roles.Tailor;
import game.roles.Veteran;
import game.roles.Vigilante;
import game.roles.Witch;
import models.FactionRole;
import services.FactionService;
import services.HiddenService;
import services.RoleService;

public class MUMash extends Setup {

    public static final String KEY = "mumash";

    public MUMash(Narrator narrator) {
        Faction town = Town(narrator);
        Faction mafia = Mafia(narrator, 1);

        mafia.modifyAbility(AbilityModifier.HIDDEN, FactionKill.class, true);

        Prioritize(mafia, town);

        town.addSheriffDetectableTeam(mafia);

        town.setEnemies(mafia);

        narrator.setRule(SetupModifier.DAY_START, Narrator.DAY_START);
        narrator.setRule(SetupModifier.FOLLOW_GETS_ALL, false);
        narrator.setRule(SetupModifier.CIT_RATIO, 40);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        Role architect = Architect.template(narrator);
        Role assassin = Assassin.template(narrator);
        Role baker = Baker.template(narrator);
        Role busDriver = Driver.template(narrator);
        Role coward = Coward.template(narrator);
        coward.addChargeModifier(3);
        Role doctor = Doctor.template(narrator);
        Role drugDealer = DrugDealer.template(narrator);
        Role electromaniac = RoleService.createRole(narrator, "Electromaniac", ElectroManiac.class);
        Role framer = Framer.template(narrator);
        Role jailor = Jailor.template(narrator);
        jailor.addChargeModifier(2);

        Role janitor = Janitor.template(narrator);
        Role investigator = Investigator.template(narrator);
        Role operator = Operator.template(narrator);
        Role poisoner = RoleService.createRole(narrator, "Poisoner", Poisoner.class);
        Role stripper = Stripper.template(narrator);
        Role tailor = Tailor.template(narrator);
        tailor.addChargeModifier(3);
        Role witch = Witch.template(narrator);

        FactionRole citizen = Citizen.template(town);

        FactionRole veteran = Veteran.template(town);
        veteran.role.addChargeModifier(3);
        FactionRole vigilante = Vigilante.template(town);
        vigilante.role.addChargeModifier(2);

        FactionRole[] townRoles = new FactionRole[] {
                Armorsmith.template(town), ArmsDetector.template(town), Bodyguard.template(town),
                Bulletproof.template(town), citizen, Coroner.template(town), Detective.template(town),
                Gunsmith.template(town), Lookout.template(town), Mason.template(town), Mayor.template(town),
                ParityCheck.template(town), Sheriff.template(town), Sleepwalker.template(town, citizen),
                Snitch.template(town), Spy.template(town),

                // town only roles
                veteran, vigilante,

                // duplicated roles
                FactionService.createFactionRole(town, architect), FactionService.createFactionRole(town, assassin),
                FactionService.createFactionRole(town, baker), FactionService.createFactionRole(town, busDriver),
                FactionService.createFactionRole(town, coward), FactionService.createFactionRole(town, doctor),
                FactionService.createFactionRole(town, drugDealer), FactionService.createFactionRole(town, electromaniac),
                FactionService.createFactionRole(town, framer), FactionService.createFactionRole(town, investigator),
                FactionService.createFactionRole(town, jailor), FactionService.createFactionRole(town, janitor),
                FactionService.createFactionRole(town, operator), FactionService.createFactionRole(town, poisoner),
                FactionService.createFactionRole(town, stripper), FactionService.createFactionRole(town, tailor),
                FactionService.createFactionRole(town, witch),
        };
        Hidden town_random = HiddenService.createHidden(narrator, Constants.TOWN_RANDOM_ROLE_NAME, townRoles);

        FactionRole disg = Disguiser.template(mafia);
        disg.role.addChargeModifier(1);

        FactionRole bs = Blacksmith.templateEvil(mafia);

        FactionRole[] mafParts = new FactionRole[] {
                Agent.template(mafia), GraveDigger.template(mafia),

                // mafia only roles
                bs, disg,

                FactionService.createFactionRole(mafia, architect), FactionService.createFactionRole(mafia, assassin),
                FactionService.createFactionRole(mafia, baker), FactionService.createFactionRole(mafia, busDriver),
                FactionService.createFactionRole(mafia, coward), FactionService.createFactionRole(mafia, doctor),
                FactionService.createFactionRole(mafia, drugDealer), FactionService.createFactionRole(mafia, electromaniac),
                FactionService.createFactionRole(mafia, framer), FactionService.createFactionRole(mafia, investigator),
                FactionService.createFactionRole(mafia, jailor), FactionService.createFactionRole(mafia, janitor),
                FactionService.createFactionRole(mafia, operator), FactionService.createFactionRole(mafia, poisoner),
                FactionService.createFactionRole(mafia, stripper), FactionService.createFactionRole(mafia, tailor),
                FactionService.createFactionRole(mafia, witch)
        };
        Hidden maf_random = HiddenService.createHidden(narrator, Constants.MAFIA_RANDOM_ROLE_NAME, mafParts);

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        addInitialRole(maf_random, 1);
        addInitialRole(town_random, 3);

        addSetupChange(5, town_random);

        addSetupChange(6, town_random);

        addSetupChange(7, maf_random);

        addSetupChange(8, town_random);

        addSetupChange(9, maf_random);

        addSetupChange(10, town_random);

        addSetupChange(11, town_random);

        addSetupChange(12, town_random);

        addSetupChange(13, town_random);

        addSetupChange(14, town_random);

        addSetupChange(15, maf_random);

        addSetupChange(16, town_random);
    }

    @Override
    public String getShortDescription() {
        return "All roles enabled! (MU Style)";
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "All roles enabled! (MU Style)"
        };
    }

    @Override
    public String getName() {
        return "MU Mash";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
