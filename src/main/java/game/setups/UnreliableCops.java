package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.roles.FactionKill;
import game.roles.Framer;
import game.roles.Goon;
import game.roles.Miller;
import game.roles.Sheriff;
import models.FactionRole;

public class UnreliableCops extends Setup {

    public static final String KEY = "millercop7";

    public UnreliableCops(Narrator narrator) {
        Faction town = Town(narrator);
        Faction mafia = Mafia(narrator);

        Prioritize(mafia, town);
        town.setEnemies(mafia);

        town.addSheriffDetectableTeam(mafia);

        mafia.modifyAbility(AbilityModifier.ZERO_WEIGHTED, FactionKill.class, true);

        narrator.setRule(SetupModifier.DAY_START, Narrator.DAY_START);
        narrator.setRule(SetupModifier.MILLER_SUITED, true);

        /*
         * finished narrator editing
         * 
         * creating string to random role
         */

        /*
         * finished setup to role map
         * 
         * creating visible faction
         */

        init(narrator, KEY);

        FactionRole sheriff = Sheriff.template(town);
        FactionRole miller = Miller.template(town);

        FactionRole framer = Framer.template(mafia);
        FactionRole gf = Goon.template(mafia);

        addInitialRole(sheriff, 4);
        addInitialRole(miller);
        addInitialRole(gf);
        addInitialRole(framer);
    }

    @Override
    public String getShortDescription() {
        return getDescription()[0];
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "7 Player Cop/Miller Game"
        };
    }

    @Override
    public String getName() {
        return "7 Player Cop/Miller Game";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
