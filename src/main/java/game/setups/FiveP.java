package game.setups;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.roles.Citizen;
import game.roles.Goon;
import game.roles.Mayor;
import models.FactionRole;

public class FiveP extends Setup {

    public static final String KEY = "fivep";

    public FiveP(Narrator narrator) {
        Faction town = Town(narrator);
        Faction maf = Mafia(narrator);

        Prioritize(maf, town);
        town.setEnemies(maf);

        town.addSheriffDetectableTeam(maf);

        narrator.setRule(SetupModifier.DAY_START, Narrator.DAY_START);
        narrator.setRule(SetupModifier.MAYOR_VOTE_POWER, 0);

        /*
         * finished narrator editing
         *
         * creating string to random role
         */

        /*
         * finished setup to role map
         *
         * creating visible faction
         */

        init(narrator, KEY);

        FactionRole cit = Citizen.template(town);
        FactionRole mayor = Mayor.template(town);

        FactionRole mafo = Goon.template(maf);

        addInitialRole(cit, 3);
        addInitialRole(mayor);
        addInitialRole(mafo);
    }

    @Override
    public String getShortDescription() {
        return getDescription()[0];
    }

    @Override
    public String[] getDescription() {
        return new String[] {
                "Small 5-player mafia"
        };
    }

    @Override
    public String getName() {
        return "5 Player Mafia";
    }

    @Override
    public boolean isPreset() {
        return true;
    }
}
