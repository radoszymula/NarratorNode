package game.event;

import java.util.ArrayList;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.exceptions.ChatException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.roles.Disguiser;
import game.roles.Jailor;
import game.roles.Mason;

public class ChatMessage extends Message implements DaySpecificMessage {

    public String message;
    private StringChoice sender;

    public EventLog el;

    public ChatMessage(Narrator n) {
        super(n);
    }

    public ChatMessage(Player p, String message, String key) {
        super(p.narrator);
        Player originalPlayer = p;
        String originalKey = key;
        if(n.isDay() && n.getPlayerByName(key) != null){
            p = n.getPlayerByName(key);
            key = DayChat.KEY;
        }
        sender = new StringChoice(p);
        this.message = message;

        StringChoice sc = new StringChoice(p);
        sc.add(p, "You");
        add(sc, ": ", message);

        boolean selfTalking = false;

        EventManager events = n.getEventManager();
        if(!n.isStarted() || !n.isInProgress())
            el = events.getDayChat();
        else if(p.isDead()){
            el = events.getDeadChat();
            setVisibility(el.getMembers());
        }else if(key.equals(VoidChat.KEY)){
            setVisibility(p);
            dontShowPrivate();
            el = events.getNightLog(key);
            selfTalking = true;
        }else if(n.isDay()){
            if(key.equalsIgnoreCase(Constants.DAY_CHAT))
                el = events.getDayChat();
            else
                throw new ChatException(key);

        }else{
            el = events.getNightLog(key);
            if(el instanceof ArchitectChat){
                setVisibility(el.getMembers());
            }else{
                String nightChat = key;
                Faction t = n.getFaction(nightChat);
                if(t == null)
                    throw new ChatException(key);

                Player disguisedTarget;
                for(Player teamMate: t.getMembers()){
                    if(teamMate.isQuarintined())
                        continue;
                    disguisedTarget = Disguiser.getDisguised(teamMate);
                    if(t.hasNightChat() || Mason.IsMasonType(teamMate) || (disguisedTarget != null && Mason.IsMasonType(disguisedTarget)))
                        setVisibility(teamMate);
                }
                el = events.getNightLog(nightChat);
            }

        }
        el.add(this);
        if(p.narrator.isStarted())
            p.narrator.getEventManager().addCommand(originalPlayer, CommandHandler.SAY, originalKey, message);

        if(selfTalking){
            p.sendMessage(this);
        }else
            for(Player receiver: el.getMembers())
                receiver.sendMessage(this);
    }

    @Override
    public void finalize() {
        for(Player p: n._players){
            if(hasAccess(p.getName()))
                p.sendMessage(this);
        }
    }

    public StringChoice getSender() {
        return sender;
    }

    @Override
    public String getName() {
        return el.getName();
    }

    public String getIcon() {
        if(sender == null)
            return n.skipper.getIcon();
        return sender.getPlayer().getIcon();
    }

    private static JailChat getJailChat(Narrator n, String jailKey) {
        return (JailChat) n.getEventManager().getNightLog(jailKey);
    }

    private static ChatMessage JailChatMessage(Player p, String message, String jailKey) {
        ChatMessage cm = new ChatMessage(p.narrator);
        cm.message = message;

        JailChat jc = getJailChat(p.narrator, jailKey);

        jc.add(cm);
        cm.el = jc;

        cm.setVisibility(jc.getMembers());

        if(p.narrator.isStarted())
            p.narrator.getEventManager().addCommand(p, CommandHandler.SAY, jailKey, message);
        return cm;
    }

    public static ChatMessage JailCaptiveMessage(Player captive, String message, String jailKey) {
        ChatMessage cm = JailChatMessage(captive, message, jailKey);

        StringChoice sc = new StringChoice(captive);
        sc.add(captive, "You");
        cm.add(sc, ": ", message);
        cm.sender = sc;

        for(Player receiver: cm.el.getMembers()){
            receiver.sendMessage(cm);
        }

        return cm;
    }

    public static ChatMessage JailCaptorMessage(Player jailor, String message, String jailKey) {
        JailChat jc = getJailChat(jailor.narrator, jailKey);

        StringChoice sc = new StringChoice(jailor);
        sc.add(jailor, "You");
        sc.add(jc.getCaptive(), Jailor.CAPTOR);

        ChatMessage cm = JailChatMessage(jailor, message, jailKey);
        cm.add(sc, ": ", message);

        cm.sender = sc;

        for(Player receiver: cm.el.getMembers()){
            receiver.sendMessage(cm);
        }

        return cm;
    }

    @Override
    public ArrayList<String> getEnclosingChats(Narrator n, Player p) {
        if(el instanceof FactionChat && p != null && p.isQuarintined() && n.isInProgress())
            return new ArrayList<>();
        if(el instanceof DayChat)
            return addHeaders(getSpecificDayChat());
        return addHeaders(el.getName());
    }

    @Override
    public Integer getID() {
        return null;
    }

}
