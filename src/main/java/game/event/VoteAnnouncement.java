package game.event;

import java.util.ArrayList;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.Constants;
import game.logic.support.Util;

public class VoteAnnouncement extends Announcement implements DaySpecificMessage {

    /*
     * System Messages are dead people announcements on the start of the day
     */

    // unvoting
    public VoteAnnouncement(Player previous, Player voter) {
        super(voter.narrator);
        this.voter = voter;
        voteCount = voter.getVotePower();
        if(previous != null)
            this.prev = previous.getName();
    }

    public Player voter;
    public String voteTarget, prev;
    public int voteCount;

    public VoteAnnouncement(Player prev, Player voter, Player voted) {
        this(prev, voter);
        if(voter.getSkipper() != voted)
        	this.voteTarget = voted.getName();
        else
        	this.voteTarget = Util.TitleCase(Constants.SKIP_VOTE);
    }

    public String getName() {
        if(dc != null)
            return dc.getName();
        return null;
    }

    EventLog dc;

    // any changes i do here should be reflected in the super
    public void finalize() {
        EventManager em = n.getEventManager();
        dc = em.getDayChat().add(this);
    }

    @Override
    public ArrayList<String> getEnclosingChats(Narrator n, Player p) {
        return addHeaders(getSpecificDayChat());
    }

    @Override
    public Integer getID() {
        return null;
    }
}
