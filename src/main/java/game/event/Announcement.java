package game.event;

import java.util.ArrayList;

import game.logic.Narrator;
import game.logic.Player;

public class Announcement extends Message {

    public static final boolean NIGHT_DEATH = true;
    public static final boolean DAY_DEATH = false;
    private AnnouncementCheckCondition checkCondition;

    /*
     * System Messages are dead people announcements on the start of the day
     */
    public Announcement(Narrator narrator) {
        super(narrator);

    }

    // also this method should only be called by narrator.announcement
    // any changes i do here should be reflected in vote announcement
    @Override
    public void finalize() {
        EventManager em = n.getEventManager();
        em.getDayChat().add(this);
    }

    @Override
    public ArrayList<String> getEnclosingChats(Narrator n, Player p) {
        ArrayList<String> chats = new ArrayList<>();

        if(n.getDayNumber() >= getDay())
            chats.add(getSpecificDayChat());
        if(n.isInProgress() && p != null && p.isAlive())
            chats.add(Feedback.CHAT_NAME);
        return chats;
    }

    public boolean isAnnounceable() {
        if(checkCondition == null)
            return true;
        return checkCondition.isAnnounceable();
    }

    public void setCheckCondition(AnnouncementCheckCondition checkCondition) {
        this.checkCondition = checkCondition;
    }

}
