package game.event;

import java.util.ArrayList;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;

public class SelectionMessage extends Message {

    private EventLog el;
    private PlayerList toSend;
    private boolean isNightMessage;
    public Player owner; // need this in case I don't want to send out the selection message to the chat

    public SelectionMessage(Player owner, boolean teamSend) {
        this(owner, teamSend, owner.narrator.isNight());
    }

    public SelectionMessage(Player owner, boolean teamSend, boolean addToLog) {
        super(owner.narrator);
        this.owner = owner;
        this.isNightMessage = owner.narrator.isNight();
        toSend = new PlayerList();

        Narrator n = owner.narrator;
        if(addToLog && teamSend && n.isNight()){
            toSend = owner.getFactions().filterKnowsTeam().getFactionmates().filterUnquarantined();
            toSend = PlayerList.GetUnique(toSend);
            if(!toSend.contains(owner))
                toSend.add(owner);
            setVisibility(toSend);
            el = n.getEventManager().getNightLog(VoidChat.KEY);
        }else{
            if(n.isDay())
                el = n.getEventManager().getDayChat();
            else
                el = n.getEventManager().getNightLog(VoidChat.KEY);
            if(!teamSend){
                setPrivate();
                dontShowPrivate();
            }
            setVisibility(owner);
            toSend.add(owner);
        }
        if(addToLog)
            el.add(this);
    }

    public void recordCommand(Player p, String... command) {
        p.narrator.getEventManager().addCommand(p, command);
    }

    public void recordCommand(Player p, ArrayList<String> commandParts) {
        String[] commandArr = new String[commandParts.size()];
        commandArr = commandParts.toArray(commandArr);
        p.narrator.getEventManager().addCommand(p, commandArr);
    }

    public void pushOut() {
        toSend.sendMessage(this);
    }

    public void addToPushOut(PlayerList notSentYet) {
        for(Player p: notSentYet){
            if(!p.in(toSend))
                toSend.add(p);
        }
    }

    @Override
    public ArrayList<String> getEnclosingChats(Narrator n, Player p) {
        EventManager em = n.getEventManager();
        ArrayList<String> ret = new ArrayList<>();
        if(p != null && p.isDead())
            return new ArrayList<>();
        else if(!n.isInProgress()){// game over
            if(isNightMessage)
                addHeaders(ret, em.getNightLog(VoidChat.KEY).getName());
            else
                addHeaders(ret, getSpecificDayChat());
        }else{
            if(p != owner){ // handles faction chats
                if(!isNightMessage)
                    addHeaders(ret, getSpecificDayChat());
                else if(el instanceof FactionChat && ((FactionChat) el).getMembers().size() == 1)
                    addHeaders(ret, n.getEventManager().getNightLog(VoidChat.KEY).getName());
                else if(p != null && p.isQuarintined())
                    return ret;
                addHeaders(ret, el.getName());
            }else if(!isNightMessage){
                addHeaders(ret, getSpecificDayChat());
            }else if(!(el instanceof VoidChat)){
                if(p.isQuarintined())
                    addHeaders(ret, Feedback.CHAT_NAME, em.getNightLog(VoidChat.KEY).getName());
                else
                    addHeaders(ret, Feedback.CHAT_NAME, el.getName(), em.getNightLog(VoidChat.KEY).getName());
            }else{
                addHeaders(ret, Feedback.CHAT_NAME, el.getName());
            }
        }
        return ret;
    }

    @Override
    public Integer getID() {
        return null;
    }

}
