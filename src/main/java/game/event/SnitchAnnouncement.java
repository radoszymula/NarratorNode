package game.event;

import game.logic.Player;
import game.logic.Faction;
import game.logic.support.HTString;

public class SnitchAnnouncement extends Announcement {

    /*
     * System Messages are dead people announcements on the start of the day
     */
    public String teamName, roleName;
    public Player revealed;
    private boolean nightDeath;

    public SnitchAnnouncement(Player revealed, Faction t, String roleName) {
        super(revealed.narrator);
        this.revealed = revealed;
        this.teamName = t.getName();
        this.roleName = roleName;

        HTString htc = new HTString(teamName + " " + roleName, t.getColor());

        add("A reliable source has revealed ", revealed.getName(), " to be a ", htc, ".");

        nightDeath = revealed.narrator.isNight();
    }

    @Override
    public String getPicture() {
        return "lookout";
    }

    @Override
    public boolean isNightToDayAnnouncement() {
        return nightDeath;
    }
}
