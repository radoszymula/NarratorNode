package game.event;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.Constants;
import game.roles.Ghost;

public class DayChat extends EventLog {

    public static final String KEY = Constants.DAY_CHAT;
    public static final String PREGAME = "Pregame";

    private Narrator n;

    public DayChat(Narrator n) {
        super(n, n._players);
        this.n = n;
    }

    @Override
    public boolean hasAccess(String... name) {
        return true;
    }

    @Override
    public String getName() {
        if(!n.isStarted())
            return PREGAME;
        return "Day " + n.getDayNumber();
    }

    @Override
    public String getKey(Player p) {
        if(p == null)
            return null;
        else if(!isActive())
            return null;
        else if(!p.narrator.isInProgress())
            return KEY;
        else if(p.isPuppeted() && !p.hasPuppets())
            return null;
        else if(p.isSilenced()){
            if(p.hasPuppets())
                return p.getPuppets().getFirst().getName();
            return null;
        }else if(p.is(Ghost.class) && p.hasPuppets())
            return p.getPuppets().getFirst().getName();
        else if(p.isDead())
            return null;
        else
            return KEY;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String getType() {
        return "fas fa-sun";
    }

}
