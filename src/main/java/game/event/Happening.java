package game.event;

import java.util.ArrayList;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.rules.SetupModifier;
import models.enums.GamePhase;

public class Happening extends Message {

    public Happening(Narrator narrator) {
        super(narrator);
        setPrivate();

        if(!n.isStarted())
            n.prehappenings.add(this);
        else if(narrator.phase == GamePhase.ROLE_PICKING_PHASE){
            if(narrator.getBool(SetupModifier.DAY_START))
                n.getEventManager().getDayChat().add(this);
            else
                n.getEventManager().voidChat.add(this);
        }else if(narrator.phase == GamePhase.NIGHT_ACTION_SUBMISSION)
            n.getEventManager().voidChat.add(this);
        else
            n.getEventManager().getDayChat().add(this);
    }

    @Override
    public ArrayList<String> getEnclosingChats(Narrator n, Player unused) {
        ArrayList<String> chats = new ArrayList<>();
        EventManager em = n.getEventManager();
        if(n.isInProgress() && unused.isAlive()){
            chats.add(Feedback.CHAT_NAME);
        }
        if(getPhase() == GamePhase.NIGHT_ACTION_SUBMISSION)
            chats.add(em.getNightLog(VoidChat.KEY).getName());
        else if(getDay() != 0)
            chats.add(getSpecificDayChat());
        return chats;
    }

}
