package game.event;

import game.logic.Narrator;
import game.logic.PlayerList;

public abstract class NightChat extends EventLog {

    public NightChat(Narrator n, PlayerList members) {
        super(n, members);
    }

    /*
     * overridden by faction chat
     */
    public void refreshVisibility() {
    }
}
