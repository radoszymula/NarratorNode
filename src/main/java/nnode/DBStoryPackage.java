package nnode;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Narrator;
import game.logic.exceptions.StoryReaderError;
import game.logic.support.StoryPackage;
import game.logic.support.StoryReader;
import game.logic.support.Util;
import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifier;
import models.Command;
import models.schemas.FactionRoleSchema;
import models.schemas.FactionSchema;
import models.schemas.GameOverviewSchema;
import models.schemas.HiddenSchema;
import models.schemas.PlayerSchema;
import models.schemas.RoleSchema;
import models.schemas.SetupHiddenSchema;
import models.schemas.SheriffCheckableSchema;
import repositories.CommandRepo;
import repositories.Connection;
import repositories.FactionEnemyRepo;
import repositories.PlayerRepo;
import repositories.ReplayRepo;
import repositories.SetupHiddenRepo;
import repositories.SetupModifierRepo;
import repositories.SetupRepo;
import repositories.SheriffCheckableRepo;
import services.FactionRoleService;
import services.FactionService;
import services.HiddenService;
import services.RoleService;

public class DBStoryPackage implements StoryReader {

    public long setupID;

    long replayID;

    Connection connection;

    public static StoryPackage deserialize(Connection c, long replayID) {
        return StoryPackage.deserialize(new DBStoryPackage(replayID, c), replayID);
    }

    public DBStoryPackage(long replayID, Connection c) {
        this.replayID = replayID;
        this.connection = c;
        try{
            this.setupID = ReplayRepo.getSetupID(c, replayID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    public DBStoryPackage(Connection c) {
        this.connection = c;
    }

    @Override
    public HashMap<SetupModifier, Rule> getRules() {
        HashMap<SetupModifier, Rule> rules = new HashMap<>();

        try{
            ArrayList<Rule> setupModifiers = SetupModifierRepo.getBySetupID(connection, setupID);

            for(Rule modifier: setupModifiers)
                rules.put((SetupModifier) modifier.id, modifier);

            return rules;
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    @Override
    public GameOverviewSchema getGameOverview() {
        try{
            return ReplayRepo.getOverview(connection, replayID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    @Override
    public long getSetupOwnerID() {
        try{
            return SetupRepo.getOwnerID(connection, replayID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    @Override
    public List<FactionSchema> getTeamInfo() {
        return FactionService.getBySetupID(connection, setupID);
    }

    @Override
    public void setEnemies(Narrator n) {
        ArrayList<String[]> factionEnemies;
        try{
            factionEnemies = FactionEnemyRepo.getEnemyColors(connection, setupID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
        for(String[] enemyPairing: factionEnemies)
            n.getFaction(enemyPairing[0]).setEnemies(n.getFaction(enemyPairing[1]));
    }

    @Override
    public void setSheriffDetectables(Narrator n) {
        ArrayList<SheriffCheckableSchema> factionEnemies;
        try{
            factionEnemies = SheriffCheckableRepo.getSheriffCheckableColors(connection, setupID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
        for(SheriffCheckableSchema enemyPairing: factionEnemies)
            n.getFaction(enemyPairing.sheriffColor)
                    .addSheriffDetectableTeam(n.getFaction(enemyPairing.detectableColor));
    }

    @Override
    public Map<Long, RoleSchema> getRoles() {
        return RoleService.getBySetupID(connection, setupID);
    }

    @Override
    public List<HiddenSchema> getHiddens() {
        return HiddenService.getBySetupID(connection, setupID);
    }

    @Override
    public ArrayList<SetupHiddenSchema> getSetupHiddens() {
        try{
            return SetupHiddenRepo.getBySetupID(connection, setupID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    @Override
    public ArrayList<PlayerSchema> getPlayers() {
        try{
            return PlayerRepo.getByReplayID(connection, replayID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    private StoryReaderError getStoryReaderError(SQLException e) {
        String message = "Failed to execute DBStoryPackage query.";
        Util.log(e, message, e.getMessage());
        return new StoryReaderError(message);
    }

    @Override
    public ArrayList<Command> getCommands() {
        try{
            return CommandRepo.getByReplayID(connection, replayID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    @Override
    public Set<FactionRoleSchema> getFactionRoles() {
        return FactionRoleService.getBySetupID(connection, setupID);
    }
}
