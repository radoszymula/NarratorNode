package nnode;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import game.ai.Brain;
import game.ai.Computer;
import game.event.Announcement;
import game.event.ChatMessage;
import game.event.DayChat;
import game.event.DeathAnnouncement;
import game.event.EventList;
import game.event.EventLog;
import game.event.Feedback;
import game.event.Header;
import game.event.JailChat;
import game.event.Message;
import game.event.SelectionMessage;
import game.event.SnitchAnnouncement;
import game.event.VoidChat;
import game.event.VoteAnnouncement;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.UnknownRoleException;
import game.logic.listeners.NarratorListener;
import game.logic.support.AlternateLookup;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.DeathDescriber;
import game.logic.support.Random;
import game.logic.support.StoryPackage;
import game.logic.support.Util;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.HTMLDecoder;
import game.roles.Architect;
import game.roles.Ghost;
import game.roles.Jailor;
import game.setups.Classic;
import game.setups.Default;
import game.setups.Setup;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.Command;
import models.FactionRole;
import models.events.ActionSubmitEvent;
import models.events.BroadcastEvent;
import models.events.DayDeathEvent;
import models.events.DayStartEvent;
import models.events.GameEndEvent;
import models.events.GameStartEvent;
import models.events.NewVoteEvent;
import models.events.NightStartEvent;
import models.events.PhaseEndBidEvent;
import models.events.PhaseResetEvent;
import models.events.PlayerAddedEvent;
import models.events.RoleCardUpdate;
import models.events.RolePickingPhaseStartEvent;
import models.events.TrialPhaseStartEvent;
import models.events.VotePhaseStartEvent;
import models.events.VoteResetEvent;
import models.json_output.UserJson;
import nnode.StoryWriter.StoryWriterError;
import repositories.CommandRepo;
import repositories.Connection;
import repositories.PlayerRepo;
import repositories.ReplayRepo;
import repositories.SetupRepo;
import repositories.UserPermissionRepo;
import services.ActionService;
import services.ChatService;
import services.FactionService;
import services.ModeratorService;
import services.PhaseService;
import services.PlayerService;
import services.SetupService;
import services.UserService;
import util.LookupUtil;

public class Game implements NarratorListener {

    public static final long UNINITIALIZED_HOST_ID = -1;
    public static boolean runningTests = false;

    public Narrator narrator;
    public CommandHandler ch;
    public Setup _setup;

    public HashMap<Player, User> phoneBook;

    public String joinID;

    public Connection connection;
    private StoryWriter sw;

    public long hostID = UNINITIALIZED_HOST_ID;
    private boolean isPrivate;

    public Long seed;
    private Long id;

    private ArrayList<SnitchAnnouncement> snitchAnnouncements;

    public PlayerList repickers = new PlayerList();
    public AlternateLookup nodeSwitchLookup = new AlternateLookup() {
        @Override
        public Player lookup(String name) {
            try{
                long userID = Long.parseLong(name);
                User user = NodeSwitch.phoneBook.get(userID);
                if(user != null && user.isInGame() && user.game == Game.this && user.player != null)
                    return user.player;
            }catch(NumberFormatException e){
            }
            return null;
        }
    };

    private void init() {
        phoneBook = new HashMap<>();

        observers = new HashSet<>();

        readTracker = new HashMap<>();
        snitchAnnouncements = new ArrayList<>();
    }

    public Game(StoryPackage sp) throws SQLException {
        init();

        this.id = sp.replayID;
        this._setup = new Setup();
        this._setup.narrator = this.narrator = sp.narrator;

        ch = new CommandHandler(narrator);

        NodeSwitch.registerInstanceID(sp.instanceID, this);
        this.joinID = sp.instanceID;
        this.isPrivate = sp.isPrivate;

        connection = new Connection();

        addDeathDescriptions();

        User user;
        this.hostID = sp.hostID;
        for(Player player: sp.phoneBook.keySet()){
            user = UserService.get(sp.phoneBook.get(player));
            if(player.isComputer())
                NodeSwitch.phoneBook.remove(sp.phoneBook.get(player));
            user.game = this;
            user.player = player;

            phoneBook.put(player, user);
        }

        Throwable error = sp.runCommands();
        if(error != null)
            NodeSwitch.internalError(error);

        sw = new StoryWriter(new StoryWriterError() {
            @Override
            public boolean onError(SQLException e) {
                return true;
            }
        }, new ArrayList<Command>());
        sw.setGameID(sp.replayID, this.narrator._players.getDatabaseMap());
        sw.counter = sp.commands.size();
        narrator.addListener(sw);
        narrator.addListener(this);

        setBrain();

        try{
            NodeSwitch.serverWrite(PhaseResetEvent.request(this));
        }catch(JSONException e){
            Util.log(e, "Failed to send close lobby event.");
        }
    }

    private Game() {
        init();
        narrator = new Narrator();
        narrator.addListener(this);
        ch = new CommandHandler(narrator);

        joinID = NodeSwitch.getID(this);

        try{
            connection = new Connection();
        }catch(SQLException e){
            Util.log(e, "error starting dbConnection");
        }
    }

    public Game(String setupName, long hostID, boolean isPrivate) throws SQLException {
        this();
        this._setup = Setup.GetSetup(this.narrator, setupName);
        this._setup.ownerID = hostID;
        SetupService.create(this.connection, this._setup);

        this.isPrivate = isPrivate;

        this.id = ReplayRepo.create(this.connection, this.joinID, isPrivate, hostID, this.narrator.getSeed(),
                this._setup.id);
        this.hostID = hostID;
    }

    public Game(long setupID, long hostID, boolean isPrivate) throws SQLException {
        this();
        this._setup = SetupRepo.getSetup(this.connection, setupID, narrator);

        this.isPrivate = isPrivate;

        this.id = ReplayRepo.create(this.connection, this.joinID, isPrivate, hostID, this.narrator.getSeed(),
                this._setup.id);
        this.hostID = hostID;
    }

    public void removePlayer(User leaver) {
        phoneBook.remove(leaver.player);
        boolean wasObserver = observers.contains(leaver);
        if(!narrator.isStarted()){
            if(leaver.player != null)
                readTracker.get(DayChat.PREGAME).removeUser(leaver);
            if(!wasObserver)
                narrator.removePlayer(leaver.player);

            if(isHost(leaver)){
                changeHost(this.phoneBook.values().iterator().next());
                try{
                    ReplayRepo.setHostID(this.connection, this.hostID, this.id);
                }catch(SQLException e){
                    // nothing to do here. if it fails, the user is still out. maybe kill the game
                    // entirely?
                }
                resetSetupHiddenSpawns();
            }
        }
        leaver.player = null;
        leaver.game = null;
        observers.remove(leaver);
    }

    public static final int MAX_REGULAR_PLAYERS = 16;
    public static final int MIN_REGULAR_PLAYERS = 5;

    public void changeHost(User host) {
        repickers = new PlayerList();
        this.hostID = host.id;
    }

    public void resetSetupHiddenSpawns() {
        narrator.getRolesList().clearSetSpawns();
    }

    public PlayerList addComputers(int size) {
        PlayerList newPlayers = new PlayerList();
        Player player;
        int currentComputerSize = 1;
        while (size > 0){

            // Gets valid computer name
            while (narrator.getPlayerByID(
                    Computer.toLetter(currentComputerSize) + Computer.toLetter(currentComputerSize)) != null)
                currentComputerSize++;

            player = narrator.addPlayer(Computer.toLetter(currentComputerSize) + Computer.toLetter(currentComputerSize))
                    .setComputer();
            newPlayers.add(player);
            size--;
        }
        if(isAutoFillingRoles())
            applyRolesList();
        return newPlayers;
    }

    public void removeComputers(int size) {
        PlayerList pl = narrator.getAllPlayers().sortByName();

        Player comp;
        while (pl.getLast() != null){
            comp = pl.getLast();
            if(comp.isComputer()){
                narrator.removePlayer(comp);
                break;
            }
            pl.remove(comp);
        }

        if(size > 1)
            removeComputers(size - 1);
        else{
            if(isAutoFillingRoles())
                applyRolesList();
            sendGameState();
        }
    }

    public Player addPlayer(User user, String name) {
        Player player = null;
        synchronized (narrator.getPlayerByNameLock){
            name = PlayerService.getAcceptableName(name, narrator);
            player = narrator.addPlayer(name);
        }
        user.player = player;
        user.game = this;
        phoneBook.put(player, user);
        if(!readTracker.containsKey(DayChat.PREGAME)){
            HashSet<User> users = new HashSet<>();
            users.add(user);
            readTracker.put(DayChat.PREGAME, new UnreadManager(DayChat.PREGAME, users));
        }
        readTracker.get(DayChat.PREGAME).addPlayer(user);

        if(isAutoFillingRoles())
            applyRolesList();

        if(!hasModerator() && narrator.getPlayerCount() == 1)
            changeHost(user);
        else{
            try{
                NodeSwitch.serverWrite(PlayerAddedEvent.request(this, user));
            }catch(JSONException e){
                Util.log(e, "Failed to send new player event.");
            }
        }

        if(narrator.getPlayerCount() == narrator.getRolesList().size()){
            sendNotification(getHost().player, "Game is ready to start!");
            pushToSlackUsers("Game is ready to start!");
        }
        return player;
    }

    public HashSet<User> observers;

    public void addObserver(User np) {
        observers.add(np);
        np.game = this;
        try{
            JSONObject jo = sendGameState(np);
            np.write(jo);
            resetChat(np);
        }catch(JSONException e){
            e.printStackTrace();
        }

    }

    DeathDescriber dd;

    private void addDeathDescriptions() throws SQLException {
        if(dd != null)
            dd.cleanup();
        dd = new DatabaseDeath(narrator.getSeed());
        if(dd == null){
            dd = new DeathDescriber() {
                @Override
                public void populateDeath(Player dead, Announcement e) {
                    try{
                        String deaths = readFile("deaths", StandardCharsets.UTF_8);
                        JSONArray jArray = new JSONArray(deaths);
                        Random r = new Random();

                        String death = jArray.getString(r.nextInt(jArray.length()));
                        death = death.replaceAll("%s", dead.getName());

                        if(!dead.getDeathType().isCleaned()){
                            death += dead.getName() + " was a " + dead.getFaction().getName() + " " + dead.getRoleName()
                                    + ".";
                        }else{
                            death += "We could not determine what " + dead.getName() + " was.";
                        }

                        e.add(death);
                    }catch(IOException | JSONException e1){
                        e1.printStackTrace();
                    }
                }

                @Override
                public boolean hasDeath(Player dead) {
                    return true;
                }

                @Override
                public void setGenre(String s) {
                }

                @Override
                public void cleanup() {
                }
            };
        }else{
            ((DatabaseDeath) dd).setProductionMode(true);
        }
        narrator.setDeathDescriber(dd);

    }

    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public void playerJWrite(Player p, JSONObject j) throws JSONException {
        User np = phoneBook.get(p);
        np.write(j);
    }

    static JSONObject GetGUIObject() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.guiUpdate, true);
        jo.put(StateObject.server, false);
        jo.put(StateObject.type, new JSONArray());
        return jo;
    }

    public void startGame() {
        User host = getHost();
        // th is initialized first because if the game is started, a message will be
        // sent out
        // instance will not quite be set yet, and throw null pointers

        /*
         * String errorMessage = _n.willStart(); if(errorMessage != null){
         * host.warn(errorMessage); return; }
         */

        narrator._players.sortByName();

        try{
            if(seed != null)
                narrator.setSeed(seed);
            else{
                ArrayList<NarratorListener> nls = new ArrayList<>();
                nls.addAll(narrator.getListeners());
                seed = getSeed();
//                narrator.restartGame(false, false);
                narrator.setSeed(seed);
                narrator.getListeners().addAll(nls);
            }
            narrator.startGame(); // after this call is when the game state is pushed out to the clients
        }catch(NarratorException e){
            if(Config.getBoolean("test_mode"))
                e.printStackTrace();
            throw e;
        }catch(Throwable t){
            t.printStackTrace();
            if(host != null && t.getMessage() != null)
                host.sendWarning(t.getMessage());

            return;
        }

        try{
            ReplayRepo.setStarted(this.connection, this.id);
        }catch(SQLException e2){
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }

        try{
            addDeathDescriptions();
        }catch(SQLException e1){
            Util.log(e1, "Failed to add death descriptions.");
        }
        if(_setup != null){
            dd.setGenre(_setup.getGenre());
        }

        isPrivate = !NodeSwitch.instances.contains(this);
        NodeSwitch.instances.remove(this);

        // th.broadcast("I will accept messages in this channel if they start with a
        // \"!\"");

        sw = new StoryWriter(new StoryWriterError() {
            @Override
            public boolean onError(SQLException e) {
                return true;
            }
        }, narrator.getCommands());
        try{
            sw.setGameID(this.id, this.narrator._players.getDatabaseMap());
            PlayerService.savePlayersToGame(this.id, this.narrator, this.phoneBook, this.connection);
        }catch(SQLException e1){
            Util.log(e1);
        }

        narrator.addListener(sw);
        try{
            NodeSwitch.serverWrite(GameStartEvent.request(this));
            for(User user: phoneBook.values())
                NodeSwitch.serverWrite(RoleCardUpdate.request(this, user));
        }catch(JSONException e){
            Util.log(e, "Failed to send game start event.");
        }

        sendNotification("Game has started!");
        if(narrator.isDay())
            speakAnnouncement("Game has started.  It is daytime.");
        else
            speakAnnouncement("Game has started.  It is nighttime.");

        setBrain();
        sendGameState();
    }

    @Override
    public void onRolepickingPhaseStart() {
        PhaseService.setPhaseTimeout(this);
        try{
            NodeSwitch.serverWrite(RolePickingPhaseStartEvent.request(this));
        }catch(JSONException e){
            Util.log(e, "Failed to send role picking phase start event.");
        }
    }

    private long getSeed() throws SQLException {
        if(this._setup.getName().equalsIgnoreCase(Default.KEY) || this.narrator.hiddens.isEmpty())
            return narrator.getRandom().nextLong();
        String query = "SELECT SUM(1/variability) AS total FROM seed_variability WHERE setup = ? AND playerCount = ?;";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, _setup.key);
        ps.setInt(2, narrator.getPlayerCount());
        ResultSet rs = ps.executeQuery();

        if(!rs.next() || rs.getDouble(1) <= 0){
            rs.close();
            ps.close();
            return narrator.getRandom().nextLong();
        }
        double vari = narrator.getRandom().nextDouble() * rs.getDouble(1);
        rs.close();
        ps.close();

        ps = connection.prepareStatement(
                "SELECT seed, 1/variability FROM seed_variability WHERE playerCount = ? AND setup = ? AND variability > 0 ORDER BY RAND();");
        ps.setInt(1, narrator.getPlayerCount());
        ps.setString(2, _setup.key);
        rs = ps.executeQuery();

        while (rs.next()){
            vari -= rs.getDouble(2);
            if(vari <= 0)
                return rs.getLong(1);
        }

        return narrator.getRandom().nextLong();
    }

    private void resetGame() throws SQLException {
        User host = getHost();
        if(!isPrivate){
            host.warn("May not reset public games");
        }else if(narrator.isFirstDay() || narrator.isFirstNight()){
            narrator.restartGame(false, false); // don't restart game, don't reset prefs, dont reset comms
            narrator.setSeed(narrator.getRandom().nextLong());

            cancelBrain();

            PlayerRepo.deleteByReplayID(sw.connection, this.id);
            CommandRepo.deleteByReplayID(sw.connection, this.id);
            sw.closeSQL();
            sw = null;

            if(!isPrivate)
                NodeSwitch.instances.add(this);

            addDeathDescriptions();
            narrator.addListener(this);

            readTracker.put(DayChat.PREGAME, new UnreadManager(DayChat.PREGAME, getUsers()));

            sendGameState();
            resetChat();
        }else{
            host.warn("Game has progressed too far to reset.");
        }
    }

    Brain brain;

    private void setBrain() {
        if(narrator._players.hasComputers()){
            brain = new Brain(narrator, narrator._players.getComputers(), new Random().setSeed(narrator.getSeed()));
        }
    }

    public void cancelBrain() {
        brain = null;
    }

    void sendGameState(PlayerList players) {
        for(Player p: players)
            sendGameState(p);
    }

    public JSONObject sendGameState(User wp) throws JSONException {
        JSONObject jo = sendGameState(this, wp);
        wp.write(jo);
        return jo;
    }

    JSONObject sendGameState(Player p) {
        try{
            return sendGameState(this, this.phoneBook.get(p));
        }catch(JSONException e){
            Util.log(e);
            return new JSONObject();
        }
    }

    private static boolean shouldShowButton(User user) {
        if(user.player == null)
            return false;
        Player player = user.player;
        if(player == null)
            return false;
        Narrator n = player.narrator;
        if(!n.isInProgress())
            return true;
        if(player.isDead() && !player.is(Ghost.class))
            return false;
        if(n.isNight())
            return true;
        if(!player.hasDayAction(Jailor.JAIL, Architect.COMMAND) && !player.isPuppeted())
            return true;
        return false;
    }

    private boolean isHost(User wp) {
        return wp.id == this.hostID;
    }

    private boolean isHost(Player wp) {
        return phoneBook.get(wp).id == this.hostID;
    }

    public User getHost() {
        for(User webPlayer: this.phoneBook.values()){
            if(webPlayer.id == this.hostID)
                return webPlayer;
        }
        for(User observer: this.observers){
            if(observer.id == this.hostID)
                return observer;
        }
        if(this.hostID != -1)
            return new User(this.hostID);
        return null;
    }

    public static StateObject getGameState(Game game, User user) throws JSONException {
        Narrator narrator = game.narrator;

        StateObject gameState = game.getInstObject();
        gameState.addState(StateObject.PLAYERLISTS);
        gameState.addState(StateObject.GRAVEYARD);
        gameState.addState(StateObject.ROLEINFO);
        gameState.addState("setup");

        gameState.addKey(StateObject.gameStart, narrator.isStarted());
        gameState.addKey(StateObject.showButton, shouldShowButton(user));
        gameState.addKey(StateObject.joinID, game.joinID);
        gameState.addKey("joinID", game.joinID);
        gameState.addKey(StateObject.isObserver, user.player == null);
        gameState.addState(StateObject.RULES);
        if(game.getHost() != null)
            gameState.addKey("host", UserJson.toJson(game.getHost()));
        else
            gameState.addKey("host", UserJson.toJson(user));

        if(narrator.isStarted()){
            gameState.addState(StateObject.DAYLABEL);
            gameState.addState(StateObject.ACTIONS);
            gameState.addKey(StateObject.isDay, narrator.isDay());
            gameState.addState(StateObject.VOTES);
            if(narrator.isInProgress()){
                if(user.player != null && user.player._lastWill != null && !user.player._lastWill.isEmpty()){
                    // I will not continually send this upon last will updates. the client should
                    // have this
                    gameState.addKey(StateObject.lastWill, user.player._lastWill);
                }
                if(user.player != null && user.player._secretLastWill != null
                        && !user.player._secretLastWill.isEmpty()){
                    // I will not continually send this upon last will updates. the client should
                    // have this
                    gameState.addKey(StateObject.teamLastWill, user.player._secretLastWill);
                }
            }
            if(narrator.isDay()){
                gameState.addKey(StateObject.skipVote, game.getSkippingDayCount());
                gameState.addKey(StateObject.isSkipping,
                        user.player != null && narrator.skipper == game.narrator.voteSystem.getVoteTarget(user.player));
                if(user.player == null)
                    gameState.addKey(StateObject.getDayActions, new JSONArray());
                else
                    gameState.addKey(StateObject.getDayActions, new JSONArray(user.player.getDayCommands()));
            }
        }else{
            gameState.addKey(StateObject.hostID, game.hostID);
            gameState.addKey(StateObject.hasModerator, game.getHost().player == null);
            gameState.addKey(StateObject.setupName, game._setup.key);
            gameState.addKey(StateObject.isPrivateInstance, game.isPrivate());
            if(user.player != null)
                gameState.addKey(StateObject.playerName, user.player.getName());
            gameState.showHiddenRoles = game.getHost() == user && user != null;
        }
        return gameState;
    }

    public static JSONObject sendGameState(Game game, User user) throws JSONException {
        StateObject iObject = getGameState(game, user);
        return iObject.constructAndSend(user, game);
    }

    private void sendPlayerLists() {
        try{
            StateObject io = getInstObject();
            io.addState(StateObject.PLAYERLISTS);
            for(User user: phoneBook.values()){
                if(user != null){
                    io.addKey(StateObject.playerName, user.player.getName());
                    io.constructAndSend(user, this);
                }
            }

            io.removeKey(StateObject.playerName);
            for(User observer: observers){
                JSONObject jo = io.constructAndSend(observer, this);
                observer.write(jo);
            }
        }catch(JSONException e){
        }
    }

    public void sendGameState() {
        try{
            for(Player p: phoneBook.keySet())
                sendGameState(p);
            JSONObject jo;
            for(User observer: observers){
                jo = sendGameState(observer);
                observer.write(jo);
            }
        }catch(JSONException e){
            e.printStackTrace();
        }catch(Exception | Error e){
            NodeSwitch.internalError(e);
            throw e;
        }
    }

    public boolean hasModerator() {
        User host = getHost();
        return host != null && host.player == null;
    }

    public void sendRules() throws JSONException {
        StateObject so = getInstObject().addState(StateObject.RULES).addKey(StateObject.hasModerator, hasModerator())
                .addState("setup");
        so.constructAndSend(narrator._players, this);
        if(observers.isEmpty())
            return;
        JSONObject jo = so.constructAndSend(observers.iterator().next(), this);
        for(User observer: observers){
            observer.write(jo);
        }
    }

    private void sendRolesList() throws JSONException {
        StateObject so = getInstObject();
        so.addKey(StateObject.setupName, _setup.key);
        for(Player p: this.phoneBook.keySet()){
            so.showHiddenRoles = hasModerator() && isHost(p);
            so.constructAndSend(phoneBook.get(p), this);
        }

        so.showHiddenRoles = false;

        if(observers.isEmpty())
            return;
        JSONObject jo = so.constructAndSend(observers.iterator().next(), this);
        for(User observer: observers){
            observer.write(jo);
        }
    }

    public static final String team_regex = "^[\\p{L}.'-]+$";
    public static final Pattern pattern = Pattern.compile(team_regex, Pattern.CASE_INSENSITIVE);

    private void addTeam(JSONObject jo) throws JSONException {
        User host = getHost();
        String name = jo.getString(StateObject.teamName).replaceAll(" ", "");

        Matcher matcher = pattern.matcher(name);
        if(!matcher.find()){
            host.warn("Team has invalid characters");
            return;
        }

        for(Faction t: narrator.getFactions()){
            if(t.getName().equalsIgnoreCase(name)){
                host.warn("That team name is taken!");
                return;
            }
        }

        String color = jo.getString(StateObject.color);
        if(narrator.getFaction(color.toUpperCase()) != null || color.toUpperCase().equals(Constants.A_RANDOM)
                || color.toUpperCase().equals(Constants.A_NEUTRAL)){
            host.warn("That team color is already taken!");
            return;
        }

        String description;
        if(jo.has(StateObject.teamDescription))
            description = jo.getString(StateObject.teamDescription);
        else
            description = null;
        if(description == null || description.isEmpty())
            description = "Custom team implemented by " + host.player.getName();

        FactionService.createFaction(host.id, color, name, description);
        sendRules();
    }

    private void editTeamName(JSONObject jo) throws JSONException {
        if(!jo.has(StateObject.teamName) || !jo.has(StateObject.color))
            return;
        String newName = jo.getString(StateObject.teamName).replaceAll(" ", "");

        String color = jo.getString(StateObject.color);

        Matcher matcher = pattern.matcher(newName);
        if(!matcher.find()){
            getHost().warn("Team has invalid characters");
            return;
        }

        Faction faction = narrator.getFaction(color);
        String oldName = faction.getName();
        if(faction == null || faction.getName().equals(newName))
            return;

        try{
            FactionService.updateName(connection, faction, newName);
        }catch(NarratorException e){
            getHost().warn(e.getMessage());
        }

        StateObject so = getInstObject().addState(StateObject.RULES).addKey(StateObject.hasModerator, hasModerator())
                .addKey(StateObject.teamChangeName, oldName + '-' + newName);
        so.constructAndSend(narrator._players, this);
        if(observers.isEmpty())
            return;
        jo = so.constructAndSend(observers.iterator().next(), this);
        for(User observer: observers){
            observer.write(jo);
        }
    }

    private void editTeamDescription(JSONObject jo) throws JSONException {
        if(!jo.has(StateObject.teamChangeDescription) || !jo.has(StateObject.color))
            return;

        String color = jo.getString(StateObject.color);
        String description = jo.getString(StateObject.teamChangeDescription);

        Faction faction = narrator.getFaction(color);
        try{
            FactionService.updateDescription(connection, faction, description);
        }catch(NarratorException e){
            getHost().warn(e.getMessage());
        }

        sendRules();
    }

    private void addSheriffDetectable(JSONObject jo) throws JSONException {
        if(!jo.has(StateObject.color) || !jo.has(StateObject.detectableColor))
            return;
        String color = jo.getString(StateObject.color);
        String detectable = jo.getString(StateObject.detectableColor);

        if(narrator.getFaction(detectable) == null || narrator.getFaction(color) == null){
            getHost().warn("Unknown team");
            return;
        }

        narrator.getFaction(color).addSheriffDetectableTeam(detectable);

        sendRules();
    }

    private void removeSheriffDetectable(JSONObject jo) throws JSONException {
        User host = getHost();
        if(!jo.has(StateObject.color) || !jo.has(StateObject.detectableColor))
            return;
        String color = jo.getString(StateObject.color);
        String detectable = jo.getString(StateObject.detectableColor);

        if(narrator.getFaction(color) == null){
            host.warn("Unknown team");
            return;
        }

        narrator.getFaction(color).removeSheriffDetectableTeam(detectable);

        sendRules();
    }

    private void createFactionRole(JSONObject jo) throws JSONException {
        String roleName = jo.getString(StateObject.roleName);
        String color = jo.getString("color");

        Role role = LookupUtil.findRole(narrator, roleName);
        Faction faction = narrator.getFaction(color);

        if(role == null || faction == null)
            getHost().warn("Role name or color doesn't exist.");

        FactionService.createFactionRole(getHost().id, faction.id, role.getID());

        sendRules();
        if(hasModerator()){
            sendRolesList();
        }
    }

    private void removeFactionRole(JSONObject jo) throws JSONException {
        if(!jo.has(StateObject.color) || !jo.has(StateObject.roleName))
            return;
        String color = jo.getString(StateObject.color);
        String name = jo.getString(StateObject.roleName);

        FactionRole role = LookupUtil.findFactionRole(narrator, name, color);

        FactionService.deleteFactionRole(role);

        sendRules();
        sendRolesList();
    }

    private void removeTeamAlly(JSONObject jo) throws JSONException {
        if(!jo.has(StateObject.color) || !jo.has(StateObject.ally))
            return;
        String teamColor = jo.getString(StateObject.color);
        String allyColor = jo.getString(StateObject.ally);

        Faction team = narrator.getFaction(teamColor);
        Faction ally = narrator.getFaction(allyColor);

        team.setEnemies(ally);

        sendRules();
    }

    private void removeTeamEnemy(JSONObject jo) throws JSONException {
        if(!jo.has(StateObject.color) || !jo.has(StateObject.enemy))
            return;
        String teamColor = jo.getString(StateObject.color);
        String enemyColor = jo.getString(StateObject.enemy);

        Faction team = narrator.getFaction(teamColor);
        Faction enemy = narrator.getFaction(enemyColor);

        team.setAllies(enemy);

        sendRules();
    }

    private StateObject getInstObject() {
        return new InstObject(this);
    }

    public static class InstObject extends StateObject {

        Game game;
        JSONObject jo;

        public InstObject(Game game) {
            super(game.narrator);
            this.game = game;
            jo = new JSONObject();
            try{
                jo.put(StateObject.type, new JSONArray());
                jo.put(StateObject.guiUpdate, true);
            }catch(JSONException e){
                e.printStackTrace();
            }
        }

        @Override
        public JSONObject getObject() throws JSONException {
            return jo;
        }

        @Override
        public void write(User user, JSONObject jo) {
            if(user == null)
                return;
            try{
                user.write(jo);
            }catch(JSONException e){
                e.printStackTrace();
            }
        }
    }

    public boolean hasPermission(User np, int permission) {
        return hasPermission(np, permission, SEND_MESSAGE);
    }

    public boolean isPrivate() {
        if(!narrator.isStarted())
            return !NodeSwitch.instances.contains(this);
        return isPrivate;
    }

    private boolean hasPermission(User np, int permission, boolean sendMessage) {
        boolean hasPermission;
        try{
            hasPermission = UserPermissionRepo.hasPermission(this.connection, np.id, permission);
        }catch(SQLException e){
            np.sendMessage("Error with database, unable to process request.");
            Util.log(e, "Failed to retrieve user permission.");
            return false;
        }

        if(!hasPermission && sendMessage)
            np.sendMessage("You don't have this permissions!");
        return hasPermission;
    }

    private static final boolean SEND_MESSAGE = true;

    public synchronized void handlePlayerMessage(User user, JSONObject jo) throws JSONException, SQLException {
        String message = jo.getString(StateObject.message);
        if(message.length() == 0)
            return;
        if(message.equals(StateObject.requestGameState)){
            sendGameState(user);
            return;
        }

        if(message.equals(StateObject.requestChat)){
            resetChat(user);
            return;
        }

        if(message.equals(StateObject.requestUnreads)){
            handleRequestUnreads(user);
            return;
        }

        if(message.equals(StateObject.setReadChat)){
            if(jo.has(StateObject.setReadChat)){
                try{
                    String chatReadName = jo.getString(StateObject.setReadChat);
                    setRead(user, chatReadName);
                }catch(JSONException e){
                    e.printStackTrace();
                }
            }
            return;
        }

        // peek peekroleslist
        if(message.equals("getRolesList")){
            if(isPrivate() || hasPermission(user, Permission.GAME_EDITING)){
                if(!narrator.isStarted()){
                    user.sendMessage("Roles aren't assigned yet.");
                    return;
                }
                for(Player p: narrator.getAllPlayers().sortByDeath()){
                    user.sendMessage(
                            Message.accessHelper(p, Message.PRIVATE, narrator.getDayNumber(), NodeSwitch.htmlDecoder));
                }
            }
            return;
        }

        if(isHost(user) && !narrator.isInProgress()){
            if(message.equals(StateObject.removeBots)){
                if(isPrivate() || hasPermission(user, Permission.BOT_ADDING)){
                    if(narrator.isStarted()){
                        user.sendWarning("You can't remove bots in a game!");
                        return;
                    }
                    int size = jo.getInt("number_of_bots");
                    user.game.removeComputers(size);
                }
                return;
            }

            if(message.equals(StateObject.createTeam)){
                if(isAutoFillingRoles())
                    return;
                addTeam(jo);
                return;
            }else if(message.equals(StateObject.teamChangeName)){
                if(isAutoFillingRoles())
                    return;
                try{
                    editTeamName(jo);
                }catch(NarratorException e){
                    getHost().warn(e.getMessage());
                }
                return;
            }else if(message.equals(StateObject.teamChangeDescription)){
                if(isAutoFillingRoles())
                    return;
                editTeamDescription(jo);
                return;
            }else if(message.equals(StateObject.createFactionRole)){
                if(isAutoFillingRoles())
                    return;
                createFactionRole(jo);
                return;
            }else if(message.equals(StateObject.removeTeamRole)){
                if(isAutoFillingRoles())
                    return;
                removeFactionRole(jo);
                return;
            }else if(message.equals(StateObject.addSheriffDetectable)){
                if(isAutoFillingRoles())
                    return;
                addSheriffDetectable(jo);
                return;
            }else if(message.equals(StateObject.removeSheriffDetectable)){
                if(isAutoFillingRoles())
                    return;
                removeSheriffDetectable(jo);
                return;
            }else if(message.equals(StateObject.removeTeamAlly)){
                if(isAutoFillingRoles())
                    return;
                removeTeamAlly(jo);
                return;
            }else if(message.equals(StateObject.removeTeamEnemy)){
                if(isAutoFillingRoles())
                    return;
                removeTeamEnemy(jo);
                return;
            }

            if(message.equals(StateObject.hasModerator)){
                if(!jo.has(StateObject.hasModerator))
                    return;
                boolean newVal = jo.getBoolean(StateObject.hasModerator);
                if(newVal == hasModerator())
                    return;

                if(!newVal)
                    resetSetupHiddenSpawns();
                else
                    ModeratorService.create(user.id);

                if(SetupService.isPreset(_setup))
                    _setup.applyRolesList();

                sendRules();
                sendRolesList();
                return;
            }
        }

        if(message.equals(StateObject.resetGame) && isHost(user)){
            resetGame();
            return;
        }

        Player p = user.player;
        if(p == null && !isHost(user)){
            user.sendMessage("Unable to find find you in the game.  Action not submitted.");
            // throw new PlayerTargetingException(name + " wasn't found.");
            return;
        }

        if(message.equals(StateObject.nameChange)){
            if(narrator.isStarted()){
                WebCommunicator.sendWarning("Cannot change name midgame", user);
            }else{
                String name = jo.getString(StateObject.nameChange);
                if(p.getName().equals(name)){

                }else if(narrator.acceptablePlayerName(p, name)){
                    phoneBook.remove(p);
                    p.setName(name);
                    phoneBook.put(p, user);
                    sendPlayerLists();
                }else{
                    jo = new JSONObject();
                    jo.put(StateObject.nodeError, StateObject.nameChange);
                    user.warn(name + " is invalid!");
                    user.write(jo);
                }
            }
            return;
        }

        if(message.toLowerCase().startsWith("say " + Constants.DAY_CHAT + " -prefer ")){
            try{
                CommandHandler.prefer(p,
                        message.toLowerCase().replace("say " + Constants.DAY_CHAT + " -prefer ".toLowerCase(), ""));
            }catch(UnknownRoleException e){
                WebCommunicator.sendWarning("Can't prefer a role that isn't in the game", user);
            }
            return;
        }

        if(message.toLowerCase().equalsIgnoreCase(Constants.MOD_ADDBREAD)){
            if(hasPermission(user, Permission.GAME_EDITING)){
                if(narrator.isStarted()){
                    int bread = jo.getInt("bread");
                    String player_s = jo.getString("eater");
                    Player breadReceiver = narrator.getPlayerByName(player_s);
                    breadReceiver.addModBread(bread);
                    sendGameState(breadReceiver);
                }else{
                    WebCommunicator.sendWarning("Game hasn't started, no bread to give", user);
                }
                return;
            }
        }

        if(message.equals("ping") && !narrator.isStarted() && jo.has("ping")){
            String m = jo.getString("ping");
            for(User pinged: this.phoneBook.values()){
                if(pinged.player.getName().equalsIgnoreCase(m)){
                    pinged.ping(user);
                    return;
                }
            }
            user.sendMessage("Could not find that player.");
            return;
        }

        if(message.equals(StateObject.lastWill)){
            if(narrator.isInProgress() && narrator.getBool(SetupModifier.LAST_WILL) && p.isAlive()){
                p.setLastWill(jo.getString(StateObject.willText));
                user.warn("Last will updated!");
            }else{
                user.warn("Last will submission failed.");
            }
            return;
        }

        if(message.equals(StateObject.teamLastWill)){
            if(narrator.isInProgress() && p.getFaction().hasLastWill() && p.isAlive()){
                p.setTeamLastWill(jo.getString(StateObject.willText));
            }
            return;
        }

        try{
            ActionService.submitAction(user.id, message);
        }catch(NarratorException e){
            user.warn(e.getMessage());
        }catch(Throwable t){
            NodeSwitch.internalError(t);
            if(runningTests)
                throw t;
            t.printStackTrace();
            if(p == null)
                return;
            String warningMessage;
            if(t.getMessage() != null)
                warningMessage = t.getMessage();
            else
                warningMessage = "Server Error : This message has been logged.";
            WebCommunicator.sendWarning(warningMessage, user);
        }
    }

    public boolean isAutoFillingRoles() {
        return SetupService.isPreset(_setup);
    }

    public void autoFillRoles() {
        boolean changedSetup = false;
        if(Default.KEY.equals(_setup.key)){
            changedSetup = true;
            SetupService.setSetup(this, Classic.KEY);
        }

        applyRolesList();
        if(changedSetup)
            sendGameState();
        else{
            try{
                sendRolesList();
            }catch(JSONException e){
            }
        }
    }

    private void applyRolesList() {
        _setup.applyRolesList(narrator.getPlayerCount());
        try{
            long oldSetupID = _setup.id;
            SetupService.create(connection, _setup);
            ReplayRepo.setSetupID(connection, getID(), _setup.id);
            SetupRepo.deleteByID(connection, oldSetupID);
        }catch(SQLException e){
            getHost().warn("Unable to change setups.");
        }
    }

    public void setNightChatsUnread() {
        setUnread(narrator.getEventManager().getNightLog(VoidChat.KEY));
        String jailChatKey;
        EventLog jc;
        for(Player p: narrator.getAllPlayers()){
            if(p.isJailed() && phoneBook.containsKey(p)){
                jailChatKey = JailChat.GetKey(p, narrator.getDayNumber());
                jc = narrator.getEventManager().getNightLog(jailChatKey);
                if(phoneBook.containsKey(p))
                    setSingleUnread(jc, phoneBook.get(p));
            }
        }
    }

    protected void resetChat(User user) {
        try{
            JSONObject jo = ChatService.getUserChats(user.id);
            user.write(jo);
        }catch(JSONException e1){
            e1.printStackTrace();
        }
    }

    public static JSONArray AppendMessage(JSONArray chatLog, Message e, Player p, String key) throws JSONException {
        JSONObject jo = new JSONObject();
        if(e instanceof Header)
            return chatLog;
        else if(e instanceof SelectionMessage && ((SelectionMessage) e).owner == p)
            return chatLog;
        else if(e instanceof ChatMessage){
            ChatMessage cm = (ChatMessage) e;
            if(cm.getSender() == null)
                return chatLog;
            jo.put("text", cm.message);
            jo.put("icon", cm.getSender().getPlayer().getIcon());
            jo.put("sender", Message.accessHelper(cm.getSender(), key, e.getDay(), new HTMLDecoder(), cm.n));
        }else{
            String text = e.access(key, new HTMLDecoder()) + "\n";
            if(text.contains(Player.END_NIGHT_TEXT) || text.contains(Player.CANCEL_END_NIGHT_TEXT))
                return chatLog;
            if(text.length() <= 1)
                return chatLog;
            jo.put("text", text);

            if(e.getID() != null){
                jo.put("mID", e.getID());
                jo.put("pic", e.getPicture());
                jo.put("extras", e.getExtras());
            }
        }
        jo.put("messageType", e.getClass().getSimpleName());
        jo.put("day", e.getDay());
        jo.put("isDay", e.getPhase());
        String[] chatNames = Util.toStringArray(e.getEnclosingChats(e.n, p));
        Narrator n = e.n;
        if(n != null && n.isStarted() && n.isInProgress() && Arrays.asList(chatNames).contains(DayChat.PREGAME))
            return chatLog;
        jo.put("chat", new JSONArray(chatNames));

        if(isPlayerOwnerSelection(e, p)){
            jo.put(StateObject.chatClue, "myNightTarget");
        }
        if(e instanceof Feedback){
            jo.put(StateObject.chatClue, "feedback");
        }
        if(e instanceof DeathAnnouncement){
            jo.put(StateObject.chatClue, "deathAnnouncement");
        }
        if(e.isNightToDayAnnouncement())
            jo.put("nightDeath", true);
        chatLog.put(jo);
        return chatLog;
    }

    private static boolean isPlayerOwnerSelection(Message e, Player p) {
        if(e instanceof SelectionMessage){
            SelectionMessage sm = (SelectionMessage) e;
            return sm.owner == p;

            // i'd like to check for a finality here too.
        }
        return false;
    }

    private void resetChat() {
        for(User user: phoneBook.values())
            resetChat(user);

        for(User observer: observers){

            try{
                JSONArray chatLog = new JSONArray();
                for(Message e: narrator.getEventManager().getEvents(Message.PUBLIC))
                    AppendMessage(chatLog, e, null, Message.PUBLIC);

                JSONObject jo = new JSONObject();
                jo.put("message", chatLog);
                jo.put("chatReset", StateObject.getChatKeys(narrator, null));
                jo.put("server", false);
                observer.write(jo);
            }catch(JSONException e1){
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void onNightPhaseStart(PlayerList lynched, PlayerList poisoned, EventList e) {
        readTracker.remove(DayChat.PREGAME);
        sendGameState();
        resetChat();

        sendNotification("The day has ended! Submit your night actions.");

        setNightChatsUnread();

        PhaseService.setPhaseTimeout(this);
        try{
            NodeSwitch.serverWrite(NightStartEvent.request(this, lynched, poisoned));
        }catch(JSONException f){
            Util.log(f, "Failed to send day start message.");
        }
    }

    @Override
    public void onDayPhaseStart(PlayerList newDead) {
        readTracker.remove(DayChat.PREGAME);
        sendGameState();

        resetChat();
        sendNotification("A new day has started...");
        if(narrator.getInt(SetupModifier.DISCUSSION_LENGTH) != 0)
            PhaseService.setPhaseTimeout(this);

        try{
            NodeSwitch.serverWrite(DayStartEvent.request(this, newDead, this.snitchAnnouncements));
        }catch(JSONException e){
            Util.log(e, "Failed to send day start message.");
        }

        this.snitchAnnouncements.clear();

        setUnread(narrator.getEventManager().getDayChat());
    }

    @Override
    public void onVotePhaseStart() {
        PhaseService.setPhaseTimeout(this);
        try{
            NodeSwitch.serverWrite(VotePhaseStartEvent.request(this));
        }catch(JSONException e){
            Util.log(e, "Failed to send vote phase start message.");
        }
    }

    @Override
    public void onVotePhaseReset(int resetsRemaining) {
        PhaseService.setPhaseTimeout(this);
        try{
            NodeSwitch.serverWrite(VoteResetEvent.request(this, resetsRemaining));
        }catch(JSONException e){
            Util.log(e, "Failed to send out vote phase reset message.");
        }
    }

    @Override
    public void onTrialStart(Player trialedPerson) {
        PhaseService.setPhaseTimeout(this);
        try{
            NodeSwitch.serverWrite(TrialPhaseStartEvent.request(this, trialedPerson));
        }catch(JSONException e){
            Util.log(e, "Failed to send trial phase start message.");
        }
    }

    @Override
    public void onGameStart() {

    }

    @Override
    public void onGameEnd() {
        try{
            NodeSwitch.serverWrite(GameEndEvent.request(this));
        }catch(JSONException e){
            Util.log(e, "Failed to send trial phase start message.");
        }
        sendGameState();
        resetChat();
        broadcast(narrator.getWinMessage(), true);

        try{
            PlayerRepo.setExitedForAllPlayers(connection, this.getID());
        }catch(SQLException e){
            Util.log(e, "SQL Exception on setting exited for all players.");
        }

        try{
            sw.endGame(narrator);
        }catch(SQLException e){
            Util.log(e, "SQL Exception on end game");
            NodeSwitch.internalError(e);
            e.printStackTrace();
        }
        narrator.removeListener(sw);

        sendNotification(narrator.getWinMessage().access(Message.PUBLIC));

        killDBConnection();
    }

    public void killDBConnection() {
        if(dd != null){
            narrator.setDeathDescriber(null);
            dd.cleanup();
        }
        if(this.connection != null)
            this.connection.close();
        this.connection = null;
        if(sw == null)
            return;
        narrator.removeListener(sw);
        // _n.removeListener(this); //so when the brain ends the game, the update stats
        // isn't triggered.
        sw.closeSQL();
    }

    private Set<User> getUsers() {
        return new HashSet<User>(phoneBook.values());
    }

    // this tells you which players have said that they're current.
    public HashMap<String, UnreadManager> readTracker;
    public String phaseHash = "";

    // im only going to call this when something new happens in the specified chat
    // toExclude are people that I dont want to notify that there's a new 'unread
    // message in the chat'
    // nor do I want to remove them from the 'read'
    public void setUnread(EventLog el, ArrayList<User> toExclude) {
        String logName = el.getName();

        UnreadManager toSend;
        if(!readTracker.containsKey(logName)){
            toSend = new UnreadManager(logName, getUsers());
            readTracker.put(logName, toSend);

        }else{
            toSend = readTracker.get(el.getName());

        }
        toSend.push(toExclude);
    }

    public void setUnread(EventLog el) {
        setUnread(el, (ArrayList<User>) null);
    }

    public void setUnread(EventLog el, User user) {
        ArrayList<User> users = new ArrayList<>();
        if(user != null)
            users.add(user);
        setUnread(el, users);
    }

    public void setSingleUnread(EventLog el, User user) {
        if(user == null || !phoneBook.containsKey(user.player))
            return;

        String key = el.getName();
        if(!readTracker.containsKey(key)){
            PlayerList membersOfLog = el.getMembers().intersect(getWebUsers());
            Set<User> users = new HashSet<>();
            for(Player player: membersOfLog)
                if(phoneBook.containsKey(player))
                    users.add(phoneBook.get(player));
            readTracker.put(key, new UnreadManager(el.getName(), users));
        }

        UnreadManager alreadyRead = readTracker.get(el.getName());
        alreadyRead.moreUnread(user);
    }

    public PlayerList getWebUsers() {
        return new PlayerList(phoneBook.keySet());
    }

    protected void handleRequestUnreads(User user) {
        UnreadManager alreadyRead;
        for(String chat: readTracker.keySet()){
            alreadyRead = readTracker.get(chat);
            if(alreadyRead.getMembers().contains(user))
                alreadyRead.sendUnread(user);
        }
    }

    public void setRead(User user, String chatReadName) {
        UnreadManager haveRead = readTracker.get(chatReadName);
        if(haveRead != null)
            haveRead.setRead(user);
    }

    @Override
    public void onDayActionSubmit(Player p, Action a) {
        StateObject io = getInstObject();
        io.addState(StateObject.ACTIONS);
        io.addState(StateObject.PLAYERLISTS);
        io.constructAndSend(Player.list(p), this);
    }

    @Override
    public void onRoleReveal(Player revealer, Message e) {
        speakAnnouncement(e.access(Message.PUBLIC));
        broadcast(e, false);

        sendVotes(null);
        pushToDiscordUsers(e.access(Message.PUBLIC), e);
        pushToObservers(e);
        sendNotification(revealer.getDescription() + " has revealed as " + revealer.getRoleName() + "!");

        setUnread(narrator.getEventManager().getDayChat(), phoneBook.get(revealer));

        if(phoneBook.containsKey(revealer))
            try{
                sendGameState(phoneBook.get(revealer));
            }catch(JSONException e1){
                e1.printStackTrace();
            }
    }

    @Override
    public void onElectroExplosion(PlayerList deadPeople, DeathAnnouncement announcement) {
        resetChat();
        sendGameState();
        pushToObservers(announcement);
        sendNotification("There was a fiery explosion!");

        setUnread(narrator.getEventManager().getDayChat());

        try{
            NodeSwitch.serverWrite(DayDeathEvent.request(this, announcement));
        }catch(JSONException e1){
            Util.log(e1, "Failed to execute Day Death event.");
        }
    }

    @Override
    public void onDayBurn(Player arson, PlayerList burned, DeathAnnouncement e) {
        resetChat();
        sendGameState();
        pushToObservers(e);
        sendNotification("There was a fiery explosion!");

        setUnread(narrator.getEventManager().getDayChat());

        try{
            NodeSwitch.serverWrite(DayDeathEvent.request(this, e));
        }catch(JSONException e1){
            Util.log(e1, "Failed to execute Day Death event.");
        }
    }

    @Override
    public void onAssassination(Player assassin, Player target, DeathAnnouncement e) {
        resetChat();
        sendGameState();
        pushToObservers(e);

        setUnread(narrator.getEventManager().getDayChat());

        try{
            NodeSwitch.serverWrite(DayDeathEvent.request(this, e));
        }catch(JSONException e1){
            Util.log(e1, "Failed to execute Day Death event.");
        }
    }

    public void endGameIfNoParticipatingPlayersAlive() {
        for(User user: phoneBook.values()){
            if(user.player.isAlive())
                return;
            if(user.player.is(Ghost.class))
                return;
        }
        if(!narrator.isFinished())
            Brain.EndGame(narrator, this.seed);
    }

    private void pushToObservers(Message m) {
        for(User wb: observers){
            wb.sendMessage(m.access(Message.PUBLIC));
        }
    }

    private int getSkippingDayCount() {
        int count = 0;
        for(Player p: narrator._players){
            if(narrator.voteSystem.getVoteTarget(p) == narrator.skipper)
                count++;
        }
        return count;
    }

    private void sendVotes(VoteAnnouncement event) {
        try{
            for(Player p: phoneBook.keySet()){
                StateObject io = getInstObject();
                io.addState(StateObject.PLAYERLISTS);
                io.addState(StateObject.VOTES);
                io.addKey(StateObject.skipVote, getSkippingDayCount());
                io.addKey(StateObject.isSkipping, narrator.voteSystem.getVoteTarget(p) == narrator.skipper);
                io.constructAndSend(Player.list(p), this);
            }
            StateObject io = getInstObject();
            io.addState(StateObject.PLAYERLISTS);
            io.addState(StateObject.VOTES);
            io.addKey(StateObject.skipVote, getSkippingDayCount());
            io.addKey(StateObject.isSkipping, false);
            for(User observer: observers){
                JSONObject jo = io.constructAndSend(observer, this);
                observer.write(jo);
            }
        }catch(JSONException e){
        }

        if(event == null)
            return;
        try{
            NodeSwitch.serverWrite(NewVoteEvent.request(this, event));
        }catch(JSONException e1){
            Util.log(e1, "Failed to push vote event.");
        }

        if(!this.narrator.getBool(SetupModifier.SECRET_VOTES)){
            broadcast(event, false);
            sendNotification(event);
        }

        setUnread(narrator.getEventManager().getDayChat(), phoneBook.get(event.voter));
    }

    @Override
    public void onVote(Player voter, Player target, VoteAnnouncement e) {
        if(!voter.isComputer() && brain != null && !narrator.getBool(SetupModifier.HOST_VOTING)){
            if(target == voter.getSkipper())
                Brain.SkipDay(brain, narrator);
            else
                Brain.Vote(brain, target, null);
        }
        sendVotes(e);
    }

    @Override
    public void onVoteCancel(Player voter, Player prev, VoteAnnouncement e) {
        if(!voter.isComputer() && brain != null){
            Brain.Unvote(brain);
        }
        sendVotes(e);
    }

    @Override
    public void onTargetSelection(Player owner, SelectionMessage selectionMessage) {
        if(brain != null && !owner.isComputer())
            brain.nightAction();
        // feedback already being sent and stored
        StateObject so = getInstObject().addState(StateObject.PLAYERLISTS).addState(StateObject.ACTIONS);
        if(narrator.isDay())
            so.addState(StateObject.ROLEINFO);
        so.constructAndSend(phoneBook.get(owner), this);

        User user = phoneBook.get(owner);
        if(user == null)
            return;
        try{
            NodeSwitch.serverWrite(ActionSubmitEvent.request(this, user, selectionMessage));
        }catch(JSONException e1){
            Util.log(e1, "Failed to push vote event.");
        }
    }

    @Override
    public void onTargetRemove(Player owner, String command, PlayerList prev) {
        getInstObject().addState(StateObject.PLAYERLISTS).addState(StateObject.ACTIONS)
                .constructAndSend(phoneBook.get(owner), this);
    }

    @Override
    public void onEndNight(Player player, boolean forced) {
        if(!player.isComputer()){
            try{
                NodeSwitch.serverWrite(PhaseEndBidEvent.request(this));
            }catch(JSONException e1){
                Util.log(e1, "Failed to push end phase bid event.");
            }
            for(Player c: narrator._players){// n.getLivePlayers().remove(n.getEndedNightPeople()).remove(p)){
                if(c.isDead() && !c.is(Ghost.class))
                    continue;
                if(c == player)
                    continue;
                if(c.endedNight())
                    continue;
                if(!c.isComputer()){
                    return;
                }
            }
            narrator.forceEndNight();
        }
    }

    @Override
    public void onCancelEndNight(Player player) {
        try{
            NodeSwitch.serverWrite(PhaseEndBidEvent.request(this));
        }catch(JSONException e1){
            Util.log(e1, "Failed to push end phase bid event.");
        }
    }

    public void sendNotification(String s) {
        ArrayList<User> nList = getWebPlayerList(narrator.getAllPlayers());
        nList.addAll(observers);
        NodeSwitch.sendNotification(nList, "Narrator", s);

    }

    private ArrayList<User> getWebPlayerList(PlayerList pl) {
        ArrayList<User> nList = new ArrayList<>();

        for(Player p: pl){
            if(!p.isComputer())
                nList.add(phoneBook.get(p));
        }
        return nList;
    }

    public void sendNotification(Message e) {
        String text;
        for(Player p: narrator.getAllPlayers()){
            if(p.isComputer())
                continue;
            text = e.access(p);
            if(text.length() != 0){
                sendNotification(p, text);
            }
        }
    }

    @Override
    public void onMessageReceive(Player receiver, Message e) {
        User user = phoneBook.get(receiver);
        if(user != null)
            WebCommunicator.sendMessage(user, e);
        if(e instanceof ChatMessage){
            ChatMessage cm = (ChatMessage) e;
            if(cm.el != null)
                if(cm.getSender().getPlayer() != receiver){
                    setSingleUnread(cm.el, phoneBook.get(receiver));
                }else if(phoneBook.containsKey(receiver)){
                    setRead(phoneBook.get(receiver), cm.el.getName());
                }
        }else
            sendNotification(receiver, e.access(receiver));
    }

    @Override
    public void onWarningReceive(Player player, Message m) {
        User user = phoneBook.get(player);
        if(user != null)
            WebCommunicator.sendWarning(m.access(player), user);
    }

    public void sendNotification(Player player, String subtitle) {
        sendNotification(player, "Narrator", subtitle);
    }

    public void sendNotification(Player player, String title, String subtitle) {
        User np = phoneBook.get(player);
        if(np == null || !np.notificationCapable())
            return;
        ArrayList<User> nList = new ArrayList<>();
        nList.add(np);
        NodeSwitch.sendNotification(nList, title, subtitle);
    }

    @Override
    public void onModKill(PlayerList bad) {
        sendGameState();
        resetChat();

        if(narrator.isDay())
            setUnread(narrator.getEventManager().getDayChat());
        else if(narrator.isNight())
            setUnread(narrator.getEventManager().getNightLog(VoidChat.KEY));

        try{
            NodeSwitch.serverWrite(
                    DayDeathEvent.request(this, bad, bad.getStringName() + " has/have inexplicably died."));
        }catch(JSONException e1){
            Util.log(e1, "Failed to execute modkill event.");
        }
    }

    public void broadcast(Message m, boolean toSlack) {
        for(Player p: phoneBook.keySet()){
            p.sendMessage(m);
        }
        pushToObservers(m);

        if(toSlack)
            pushToDiscordUsers(m.access(Message.PUBLIC), m);
    }

    public void pushToSlackUsers(String s) {
        pushToDiscordUsers(s, null);
    }

    public void pushToDiscordUsers(String s, Message e) {
        try{
            NodeSwitch.serverWrite(BroadcastEvent.request(this, s, e));
        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    public boolean isFull() {
        if(narrator.isStarted())
            return false;
        int playerSize = narrator.getPlayerCount();

        int playerLimit;
        if(SetupService.isPreset(_setup))
            playerLimit = _setup.getMaxPlayerCount();
        else if(narrator.getRolesList().size() < 3)
            return false;
        else
            playerLimit = narrator.getRolesList().size();
        return playerSize >= playerLimit;
    }

    @Override
    public void onNightEnding() {
        // saveCommands();
    }

    public void speakAnnouncement(String s) {
        ArrayList<User> nList = new ArrayList<>();
        nList.addAll(observers);
        for(User user: phoneBook.values())
            nList.add(user);

        for(User user: nList)
            user.speech(s);
    }

    @Override
    public void onAnnouncement(Message m) {
        if(narrator.isNight() && m instanceof SnitchAnnouncement)
            this.snitchAnnouncements.add((SnitchAnnouncement) m);

        String message = m.access(Message.PUBLIC);
        speakAnnouncement(message);

        // why am i skipping this?
        if((m instanceof DeathAnnouncement || m instanceof SnitchAnnouncement) && narrator.isNight())
            return;
        broadcast(m, false);// don't announce, because my slackswitch will broadcast the messages there
    }

    public void pushEndNightReminder() {
        User user;
        if(narrator.isNight())
            for(Player p: narrator.getNightWaitingOn()){
                user = phoneBook.get(p);
                if(user != null){
                    user.warn("Remember to skip to day, if you've finished submitting your night action(s)!");
                }
            }
    }

    public Long getID() {
        return this.id;
    }

}
