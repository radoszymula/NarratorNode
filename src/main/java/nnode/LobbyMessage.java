package nnode;

import json.JSONException;
import json.JSONObject;

public class LobbyMessage {

    protected User sender;
    protected String message;

    public LobbyMessage(User np, String message) {
        this.sender = np;
        this.message = message;
    }

    public JSONObject access(User accessor) {
        JSONObject jo = new JSONObject();
        String name;
        if(accessor.equals(sender))
            name = "You";
        else
            name = "User " + accessor.id;
        try{
            jo.put("text", message);
            jo.put("sender", "<b>" + name + "</b>");
        }catch(JSONException e){
            e.printStackTrace();
        }
        return jo;
    }

}
