package nnode;

import java.sql.SQLException;

import game.logic.Player;
import game.logic.support.Util;
import json.JSONException;
import json.JSONObject;
import repositories.Connection;
import repositories.UserRepo;

public class User {

    public long id;

    public Player player;
    public Game game;

    public User(long userID) {
        this.id = userID;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(id);
    }

    public boolean isInLobby() {
        return game == null;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o.getClass() != getClass())
            return false;
        return id == ((User) o).id;

    }

    public void write(JSONObject jo) throws JSONException {
        jo.put("userID", id);
        NodeSwitch.nodePush(jo);
    }

    public Player joinGame(Game game, String name) {
        this.game = game;
        game.addPlayer(this, name);
        game.handleRequestUnreads(this);
        return player;
    }

    public boolean isInGame() {
        return game != null;
    }

    public boolean isGuest() {
        boolean isGuest = true;
        Connection c;
        try{
            c = new Connection();
        }catch(SQLException e1){
            Util.log(e1, "Couldn't instantiate connection");
            return isGuest;
        }
        try{
            isGuest = UserRepo.getIsGuest(c, id);
        }catch(SQLException e){
            Util.log(e, "Failed to determine if user is guest");
        }
        c.close();
        return isGuest;
    }

    private static final long MINUTE = 60000;
    private long lastPinged = -1;

    public void ping(User requester) throws JSONException { // TODO, move to app.js
        String name = player == null ? "This user" : player.getName();
        long currentTime = System.currentTimeMillis();
        JSONObject jo;
        if(currentTime - lastPinged < 2 * MINUTE){
            requester.warn(name + " cannot be pinged again so soon.");
        }else{
            jo = Game.GetGUIObject();
            jo.put("ping", true);
            write(jo);
            lastPinged = currentTime;
        }
    }

    public void sendMessage(String s) {
        JSONObject jo = new JSONObject();
        try{
            jo.put("message", s);
            write(jo);
        }catch(JSONException e){

        }

    }

    public boolean notificationCapable() {
        return false;// !isActive();
    }

    public boolean isObserving() {
        return game != null && game.observers.contains(this);
    }

    public void observeGame(Game inst) {
        inst.addObserver(this);
    }

    public void requestAllGameInfo() throws JSONException {
        game.sendGameState(this);
        game.resetChat(this);
        game.handleRequestUnreads(this);
    }

    public void sendWarning(String warning) {
        WebCommunicator.sendWarning(warning, this);
    }

    public void speech(String s) {
        JSONObject jo = new JSONObject();
        try{
            jo.put(StateObject.speechContent, s);
            write(jo);
        }catch(JSONException e){

        }

    }

    public void warn(String string) {
        sendWarning(string);
    }

    public boolean isHost() {
        return game.hostID == id;
    }

    public void sendUnread(String unreadChat, int count) {
        try{
            JSONObject jo = new JSONObject();
            jo.put(StateObject.unreadUpdate, unreadChat);
            jo.put(StateObject.count, count);
            write(jo);
        }catch(JSONException e){
            e.printStackTrace();
        }

    };

    @Override
    public String toString() {
        return "WebPlayer {" + id + "}";
    }
}
