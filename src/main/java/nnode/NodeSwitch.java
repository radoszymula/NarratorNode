package nnode;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import controllers.HttpController;
import game.event.EventDecoder;
import game.logic.support.AlternateLookup;
import game.logic.support.StoryPackage;
import game.logic.support.Util;
import game.logic.templates.HTMLDecoder;
import game.setups.Classic;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import repositories.Connection;
import repositories.ReplayRepo;
import services.UserService;

public class NodeSwitch {

    public static List<Game> instances = new ArrayList<>(); // available games to join
    public static Map<Long, User> phoneBook = new HashMap<>();
    private static Random r = new Random();
    public static HashMap<String, Game> idToInstance = new HashMap<>();
    public static SwitchListener switchListener;

    private static final String[] BAD_WORDS = {
            "ANUS", "ARSE", "CLIT", "COCK", "COON", "CUNT", "DAGO", "DAMN", "DICK", "DIKE", "DYKE", "FUCK", "GOOK",
            "HEEB", "HELL", "HOMO", "JIZZ", "KIKE", "KUNT", "KYKE", "MICK", "MUFF", "PAKI", "PISS", "POON", "PUTO",
            "SHIT", "SHIZ", "SLUT", "SMEG", "SPIC", "TARD", "TITS", "TWAT", "WANK", "NIGG", "GIZZ", "SMDK"
    };

    public interface NodeConnectionRefresh {
        public void onNewConnection(Connection c);
    }

    public void start() {
        Scanner scan = new Scanner(System.in);

        while (scan.hasNextLine()){

            String message = scan.nextLine().replace("\n", "");
            try{
                handleMessage(message);
            }catch(JSONException e){
                System.err.println("[ " + message + " ] was invalid");
                e.printStackTrace();
            }catch(NullPointerException f){
                f.printStackTrace();
            }catch(Throwable t){
                t.printStackTrace();
                break;
            }

        }
        System.err.println("java ending");

        scan.close();
    }

    public static JSONObject handleMessage(JSONObject nodeRequest) throws JSONException {
        if(nodeRequest.has(StateObject.server) && nodeRequest.getBoolean(StateObject.server)){
            handleServerMessage(nodeRequest);
        }else if(nodeRequest.has(StateObject.httpRequest) && nodeRequest.getBoolean(StateObject.httpRequest)){
            return HttpController.handleHTTPRequest(nodeRequest);
        }else{
            try{
                handlePlayerMessage(nodeRequest);
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void handleMessage(String message) throws JSONException {
        JSONObject nodeRequest = new JSONObject(message);
        handleMessage(nodeRequest);
    }

    public static Game getInstance(String hostUserName, User requester, String requesterName) {
        hostUserName = hostUserName.toLowerCase();
        for(Game inst: instances){
            if(inst.getHost().player.getName().toLowerCase().equals(hostUserName)){
                if(inst.isFull()){
                    inst.getHost().sendWarning(requesterName
                            + " is trying to join your game, but is unable to, because your game is full.  Add more roles!");
                    requester.sendWarning(
                            "That host's game is full, but the host has been informed of your request to join.");
                    return null;
                }
                return inst;
            }
        }
        return null;
    }

    private static final int offset = 'A';

    // could be optimized by not selecting 'bad numbers' first

    public static String getID(Game i) {
        String id = getID();
        return registerInstanceID(id, i);
    }

    public static String registerInstanceID(String id, Game i) {
        idToInstance.put(id, i);
        return id;
    }

    public static String getID() {
        int id = r.nextInt(456976) + 1;
        StringBuilder sb = new StringBuilder();

        int i;
        while (sb.length() < 4){
            i = (id % 26) + offset;
            sb.append(((char) i));
            id -= (i - offset);
            id /= 26;
        }
        String word = sb.toString();
        if(Arrays.asList(BAD_WORDS).contains(word))
            return getID();
        return word;
    }

    public static Game getGame(long userID) throws SQLException {
        Game game;
        if(!instances.isEmpty()){
            game = instances.get(0);
            for(Game i: instances){
                if(i.narrator.getPlayerCount() > game.narrator.getPlayerCount() && !i.isFull()){
                    game = i;
                }
            }
            if(!game.isFull() && !game.narrator.isStarted())
                return game;
        }
        boolean isPrivate = true;
        game = new Game(Classic.KEY, userID, isPrivate);
        instances.add(game);
        return game;
    }

    public static void handleServerMessage(JSONObject jo) throws JSONException {
        long userID = jo.getLong(StateObject.userID);
        User user = UserService.get(userID);
        switch (jo.getString(StateObject.message)){
        case "addBotUsers":
            if(user.isInLobby()){
                user.sendWarning("You can't add bot users in lobby!");
                return;
            }
            if(user.game.narrator.isStarted()){
                user.sendMessage("You can't add bots in a game!");
                return;
            }
            int size = jo.getInt("number_of_bots");
            user.game.addComputers(size);
            break;

        case "forceEndNight":
            if(user.isInLobby()){
                user.sendWarning("You aren't in any game!");
                return;
            }

            if(!user.game.narrator.isStarted()){
                user.sendWarning("Game hasn't started.");
                return;
            }

            user.game.narrator.forceEndNight();
            break;
        default:

        }
    }

    public static void handlePlayerMessage(JSONObject jo) throws JSONException, SQLException {
        long userID = jo.getLong(StateObject.userID);
        User user = UserService.get(userID);

        Game game = user.game;
        if(game != null){
            game.handlePlayerMessage(user, jo);
            return;
        }
    }

    public void write(long userID, JSONObject jo) throws JSONException {
        phoneBook.get(userID).write(jo);
    }

    public static void serverWrite(JSONObject requestObject) {
        try{
            requestObject.put(StateObject.server, true);
        }catch(JSONException e){
            e.printStackTrace();
        }
        nodePush(requestObject);
    }

    public static void nodePush(JSONObject jo) {
        if(switchListener != null)
            switchListener.onSwitchMessage(jo);
    }

    public interface SwitchListener {
        public void onSwitchMessage(JSONObject jo);
    }

    public static void sendNotification(ArrayList<User> arrayList, String title, String subtitle) {
        JSONObject jo = new JSONObject();
        try{
            jo.put(StateObject.message, "sendNotification");
            JSONArray jRecipients = new JSONArray();
            for(User np: arrayList){
                if(np != null && np.notificationCapable() && np.player != null)
                    jRecipients.put(np.player.getName());
                // else
                // System.out.println(np.name + " is active so, won't be sending something to
                // him");
            }
            if(jRecipients.length() == 0)
                return;
            jo.put("recipients", jRecipients);
            jo.put("title", title);
            jo.put("subtitle", subtitle);
        }catch(JSONException e){
            e.printStackTrace();
        }

        serverWrite(jo);
    }

    public static EventDecoder htmlDecoder = new HTMLDecoder();

    public static void reloadInstances() throws SQLException {
        NodeSwitch.idToInstance.clear();
        NodeSwitch.instances.clear();
        NodeSwitch.phoneBook.clear();
        Connection c = new Connection();
        ReplayRepo.deleteUnstarted(c);
        ArrayList<Long> unfinishedReplayIDs = ReplayRepo.getUnfinishedIDs(c);

        StoryPackage sp;
        for(Long unfinishedReplayID: unfinishedReplayIDs){
            try{
                sp = DBStoryPackage.deserialize(c, unfinishedReplayID);
            }catch(Exception | Error e){
                e.printStackTrace();
                Util.log(e, "Couln't unpack replay " + unfinishedReplayID + ".");
                continue;
            }
            if(sp == null)
                continue;

            if(sp.onlyComputers())
                ReplayRepo.markCompleted(c, unfinishedReplayID);
            else
                loadInstance(sp);
        }
        c.close();
    }

    public static void loadInstance(StoryPackage sp) throws SQLException {
        new Game(sp); // registering is taken care of inside
    }

    public static void StartDriver() throws ClassNotFoundException {
        Class.forName("org.mariadb.jdbc.Driver");
    }

    private static int internalErrors = 0;
    public static AlternateLookup nodeSwitchLookup;

    public static void internalError(Throwable e) {
        NodeSwitch.internalErrors++;
    }

    public static void resetInternalErrors() {
        internalErrors = 0;
    }

    public static boolean hasInternalErrors() {
        return internalErrors != 0;
    }

    public static void killDBConnections() {
        for(Game i: instances)
            i.killDBConnection();
        for(Game i: idToInstance.values()){
            i.killDBConnection();
        }
    }
}
