package nnode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Narrator;
import game.logic.support.StoryPackage;
import game.logic.support.StoryReader;
import game.logic.support.rules.Rule;
import game.logic.support.rules.SetupModifier;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.Command;
import models.schemas.FactionRoleSchema;
import models.schemas.FactionSchema;
import models.schemas.GameOverviewSchema;
import models.schemas.HiddenSchema;
import models.schemas.PlayerSchema;
import models.schemas.RoleSchema;
import models.schemas.SetupHiddenSchema;

public class JSONStoryPackage implements StoryReader {

    JSONObject data;

    public JSONStoryPackage(JSONObject jo) {
        this.data = jo;
    }

    @Override
    public HashMap<SetupModifier, Rule> getRules() {
        HashMap<SetupModifier, Rule> rules = new HashMap<>();

//		JSONObject ruleData;
//		try {
//			ruleData = data.getJSONObject(StoryConstants.MAIN_TABLE);
//			for(Rule rRule : new Rules().rules.values()){
//				if(rRule.isInt())
//					rRule.setInt(ruleData.getInt(rRule.id.toString()));
//				else
//					rRule.setBool(ruleData.getInt(rRule.id.toString()) != 0);
//				rules.put(rRule.id, rRule);
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}

        return rules;
    }

    @Override
    public ArrayList<FactionSchema> getTeamInfo() {
        ArrayList<FactionSchema> teamInfo = new ArrayList<>();

//		try {
//			JSONArray factions = data.getJSONArray(StoryConstants.FACTION_TABLE);
//			FactionSchema tp;
//			JSONObject jFaction;
//			for(int i = 0; i < factions.length(); i++){
//				jFaction = factions.getJSONObject(i);
//				
//				tp = new FactionSchema();
//				tp.color = jFaction.getString(StateObject.color);
//				tp.name = jFaction.getString("name");
//				
//				for(String fRule: Team.FACTION_RULES){
//					if(Team.isIntRule(fRule)){
//						tp.ruleValues.put(fRule, jFaction.getInt(fRule));
//					}else{
//						tp.ruleValues.put(fRule, jFaction.getInt(fRule) != 0);
//					}
//				}
//				
//				if(jFaction.has(StateObject.abilities)){
//					JSONArray abilities = jFaction.getJSONArray(StateObject.abilities);
//					for(int j = 0; j < abilities.length(); j++){
//						tp.abilities.add(abilities.getString(j));
//					}
//				}
//				if(jFaction.has(StateObject.modifierMap)){
//					JSONObject modifierMap = jFaction.getJSONObject(StateObject.modifierMap);
//					Iterator<?> iterator = modifierMap.keys();
//					while(iterator.hasNext()){
//						Object key = iterator.next();
//						tp.modifiers.put(key.toString(), modifierMap.get(key.toString()));
//					}
//				}
//					
//				
//				teamInfo.add(tp);
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}

        return teamInfo;
    }

    @Override
    public void setEnemies(Narrator n) {
        try{
            JSONArray jEnemies = data.getJSONArray(StoryConstants.ENEMIES_TABLE);

            JSONObject ene;
            String color1, color2;
            for(int i = 0; i < jEnemies.length(); i++){
                ene = jEnemies.getJSONObject(i);
                color1 = ene.getString(StoryConstants.ENEMY1);
                color2 = ene.getString(StoryConstants.ENEMY2);

                n.getFaction(color1).setEnemies(n.getFaction(color2));
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void setSheriffDetectables(Narrator n) {
        try{
            JSONArray jEnemies = data.getJSONArray(StoryConstants.SHERIFF_TABLE);

            JSONObject ene;
            String color1, color2;
            for(int i = 0; i < jEnemies.length(); i++){
                ene = jEnemies.getJSONObject(i);
                color1 = ene.getString(StoryConstants.SHERIFF_TEAM);
                color2 = ene.getString(StoryConstants.DETECTED_TEAM);

                n.getFaction(color1).addSheriffDetectableTeam(color2);
            }
        }catch(JSONException e){
            e.printStackTrace();
        }

    }

    @Override
    public Map<Long, RoleSchema> getRoles() {
        return new HashMap<>();
    }

    @Override
    public List<HiddenSchema> getHiddens() {
        List<HiddenSchema> rts = new ArrayList<>();

//		try {
//			JSONArray jRoles = data.getJSONArray(StoryConstants.ROLES_TABLE);
//			
//			JSONObject jRole;
//			HiddenSchema rt;
//			for(int i = 0; i < jRoles.length(); i++){
//				jRole = jRoles.getJSONObject(i);
//				
//				rt = new HiddenSchema();
//				if(!jRole.isNull(StoryConstants.ROLE_ID))
//					rt.rt_id = jRole.getInt(StoryConstants.ROLE_ID);
//				rt.roleName = jRole.getString(StoryConstants.ROLENAME);
//				//TODO serialize ability names
//				rt.color = jRole.getString(StoryConstants.COLOR);
//				if(!jRole.isNull(StoryConstants.SPAWN))
//					rt.spawn = jRole.getString(StoryConstants.SPAWN);
//				rt.exposed = jRole.getBoolean(StoryConstants.EXPOSED);
//				rts.add(rt);
//			}
//			
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}

        return rts;
    }

    public ArrayList<RoleSchema> getRandomMemberInfo(int rt_id) {
        ArrayList<RoleSchema> members = new ArrayList<>();

        try{
            JSONObject jList = data.getJSONObject(StoryConstants.ROLES_COMP_TABLE);
            JSONArray jMembers = jList.getJSONArray(Integer.toString(rt_id));

            RoleSchema mem;
            JSONObject jMem;
            for(int i = 0; i < jMembers.length(); i++){
                jMem = jMembers.getJSONObject(i);

                mem = new RoleSchema();
                mem.name = jMem.getString(StoryConstants.ROLENAME);
                // TODO serialize base names;

                members.add(mem);
            }

        }catch(JSONException e){

            e.printStackTrace();
        }

        return members;
    }

    @Override
    public ArrayList<PlayerSchema> getPlayers() {
        ArrayList<PlayerSchema> players = new ArrayList<>();

        try{
            JSONArray jPlayers = data.getJSONArray(StoryConstants.PLAYER_TABLE);

            PlayerSchema pg;
            JSONObject jPlayer;
            for(int i = 0; i < jPlayers.length(); i++){
                jPlayer = jPlayers.getJSONObject(i);

                pg = new PlayerSchema();
                pg.name = jPlayer.getString(StoryConstants.NAME);

                players.add(pg);
            }
        }catch(JSONException e){
            e.printStackTrace();
        }

        return players;
    }

    @Override
    public ArrayList<Command> getCommands() {
        ArrayList<Command> commands = new ArrayList<>();

//        JSONArray commands_table;
//        try{
//            commands_table = data.getJSONArray(StoryConstants.COMMANDS_TABLE);
//            for(int i = 0; i < commands_table.length(); i++)
//                commands.add(commands_table.getString(i));
//        }catch(JSONException e){
//            e.printStackTrace();
//        }

        return commands;
    }

    public static StoryPackage deserialize(String i) throws JSONException {
        StoryReader sr = new JSONStoryPackage(new JSONObject(i));
        return StoryPackage.deserialize(sr, 0);
    }

    @Override
    public ArrayList<SetupHiddenSchema> getSetupHiddens() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getSetupOwnerID() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Set<FactionRoleSchema> getFactionRoles() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GameOverviewSchema getGameOverview() {
        // TODO Auto-generated method stub
        return null;
    }

}
