package nnode;

public class StoryConstants {

    public static final String MAIN_TABLE = "saved_games";

    public static final String PLAYER_TABLE = "player_saved_games";

    public static final String FACTION_TABLE = "faction_saved_games";
    public static final String ENEMIES_TABLE = "enemies_saved_games";
    public static final String SHERIFF_TABLE = "sheriff_detectables_saved_games";

    public static final String ROLES_TABLE = "roles_list_saved_games";
    public static final String ROLES_COMP_TABLE = "roles_list_composition_saved_games";

    public static final String COMMANDS_TABLE = "commands_saved_games";

    public static final String SEED = "game_seed";
    public static final String HOST_NAME = "hostName";
    public static final String HOST_TOKEN = "hostToken";
    public static final String INSTANCE_ID = "instance_id";
    public static final String IS_PRIVATE = "isPrivate";

    public static final String ENEMY1 = "teamColor1";
    public static final String ENEMY2 = "teamColor2";

    public static final String SHERIFF_TEAM = "teamColor";
    public static final String DETECTED_TEAM = "detectedColor";

    public static final String ROLE_ID = "rt_id";
    public static final String COUNT = "count";
    public static final String ROLENAME = "roleName";
    public static final String COLOR = "color";
    public static final String SPAWN = "spawn";
    public static final String EXPOSED = "exposed";

    public static final String NAME = "name";
    public static final String PLAYERROLE = "role";
    public static final String TOKEN = "token";

}
