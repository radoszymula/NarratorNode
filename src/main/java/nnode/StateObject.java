package nnode;

import java.util.ArrayList;
import java.util.HashMap;

import game.event.DayChat;
import game.event.EventLog;
import game.event.Feedback;
import game.event.SelectionMessage;
import game.logic.DeathType;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.Rule;
import game.logic.support.rules.RuleBool;
import game.logic.support.rules.RuleInt;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.logic.templates.HTMLDecoder;
import game.roles.Ability;
import game.roles.BreadAbility;
import game.roles.Disguiser;
import game.roles.ElectroManiac;
import game.roles.Ghost;
import game.roles.Jailor;
import game.roles.Joker;
import game.roles.Marshall;
import game.roles.Mason;
import game.roles.Miller;
import game.roles.Spy;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.enums.GamePhase;
import models.json_output.SetupJson;
import services.VoteService;

public abstract class StateObject {

    public static final String RULES = "Rules";
    public static final String PLAYERLISTS = "PlayerLists";
    public static final String DAYLABEL = "DayLabel";
    public static final String GRAVEYARD = "Graveyard";
    public static final String ROLEINFO = "RoleInfo";
    public static final String ACTIONS = "Actions";
    public static final String VOTES = "Votes";
    public static final String SETUP = "setup";

    public static final String warning = "warning";
    public static final String message = "message";
    public static final String userID = "userID";
    public static final String NAME = "name";
    public static final String server = "server";
    public static final String httpRequest = "httpRequest";
    public static final String httpResponse = "httpResponse";
    public static final String requestID = "requestID";
    public static final String joinID = "joinID";

    private Narrator narrator;
    private HashMap<String, Object> extraKeys;
    public boolean showHiddenRoles = false;

    public StateObject(Narrator n) {
        states = new ArrayList<String>();
        this.narrator = n;
        extraKeys = new HashMap<>();
    }

    ArrayList<String> states;

    public StateObject addState(String state) {
        states.add(state);
        return this;
    }

//    private FactionRole getSpawn(Hidden setupHidden, int count) {
//        if(!narrator.rolesList.spawns.containsKey(setupHidden))
//            return null;
//        ArrayList<FactionRole> spawns = narrator.rolesList.spawns.get(setupHidden);
//        if(spawns.isEmpty())
//            return null;
//        return spawns.get(count);
//    }

    private void addJRolesList(Player requester, JSONObject state) {
        return;
//        JSONArray roles = new JSONArray();
//        JSONObject role, subRole;
//        JSONArray jPossibleRandoms;
//        Hidden rm;
//        Player givenTo;
//        boolean omnDead = requester != null && narrator.getBool(SetupModifier.OMNISCIENT_DEAD) && requester.isDead();
//        omnDead = omnDead || (!narrator.isInProgress() && narrator.isStarted());
//        PlayerList seenPlayers = new PlayerList();
//        HashMap<Hidden, Integer> givenSpawns = new HashMap<>();
//
//        Hidden r;
//        for(SetupHidden setupHidden: narrator.getRolesList().sort()){
//            role = new JSONObject();
//            givenTo = getGivenTo(setupHidden.hidden, seenPlayers);
//            if(givenTo != null)
//                seenPlayers.add(givenTo);
//
//            if(givenTo == null){
//                r = setupHidden.hidden;
//            }else if(omnDead){
//                r = new Role(narrator, givenTo.getColor(), givenTo.getRoleName(), givenTo.getAbilities());
//                role.put(StateObject.playerName, givenTo.getName());
//            }else if(setupHidden.hidden.isRandom()){
//                if(setupHidden.isExposed && narrator.isStarted())
//                    r = givenTo.initialAssignedRole.assignedRole;
//                else if(narrator.isStarted() && !narrator.isInProgress())
//                    r = givenTo.initialAssignedRole.assignedRole;
//                else
//                    r = setupHidden.hidden;
//            }else{
//                r = setupHidden.hidden;
//            }
//
//            role.put(StateObject.roleType, r.getName());
//            role.put(StateObject.color, r.getColors().iterator().next());
//
//            if(r.isRandom()){
//                role.put(StateObject.roleID, r.getID());
//                rm = r;
//                jPossibleRandoms = new JSONArray();
//
//                String key;
//                for(FactionRole factionRole: rm.getFactionRoles()){
//                    key = factionRole.toString();
//                    subRole = new JSONObject();
//                    subRole.put(StateObject.roleType, factionRole.role.getName());
//                    subRole.put(StateObject.color, factionRole.faction.getColor());
//                    subRole.put(StateObject.key, key);
//                    jPossibleRandoms.put(subRole);
//                }
//                FactionRole m;
//
//                if(!givenSpawns.containsKey(setupHidden.hidden))
//                    givenSpawns.put(setupHidden.hidden, 0);
//                m = getSpawn(setupHidden.hidden, givenSpawns.get(setupHidden.hidden));
//                if(m != null)
//                    givenSpawns.put(setupHidden.hidden, givenSpawns.get(setupHidden.hidden) + 1);
//
//                if(m != null && showHiddenRoles){
//                    subRole = new JSONObject();
//                    subRole.put(StateObject.roleType, m.getName());
//                    subRole.put(StateObject.color, r.getColors().iterator().next());
//                    role.put(StateObject.spawn, subRole);
//                }
//
//                role.put(StateObject.possibleRandoms, jPossibleRandoms);
//            }
//
//            roles.put(role);
//        }
//        state.getJSONArray(StateObject.type).put(StateObject.roles);
//        state.put(StateObject.roles, roles);
    }

    private void addJDayLabel(JSONObject state) throws JSONException {
        String dayLabel;
        if(!narrator.isStarted()){
            dayLabel = "Night 0";
        }else if(narrator.isDay()){
            dayLabel = "Day " + narrator.getDayNumber();
        }else{
            dayLabel = "Night " + narrator.getDayNumber();
        }
        state.put("dayNumber", narrator.getDayNumber());
        state.getJSONArray(StateObject.type).put(StateObject.dayLabel);
        state.put(StateObject.dayLabel, dayLabel);
    }

    private void addNullRoleInfo(JSONObject state) throws JSONException {
        JSONObject roleInfo = new JSONObject();
        roleInfo.put(StateObject.roleColor, "#FFFFFF");
        roleInfo.put(StateObject.roleName, "");
        roleInfo.put(StateObject.roleBaseName, "");
        roleInfo.put(StateObject.roleDescription, "");

        state.getJSONArray(StateObject.type).put(StateObject.roleInfo);
        state.put(StateObject.roleInfo, roleInfo);
    }

    private void addJRoleInfo(Player player, JSONObject state) throws JSONException {
        if(player == null){
            addNullRoleInfo(state);
            return;
        }

        JSONObject roleInfo = new JSONObject();
        roleInfo.put(StateObject.displayName, player.getName());

        state.getJSONArray(StateObject.type).put(StateObject.roleInfo);
        state.put(StateObject.roleInfo, roleInfo);
        if(!player.narrator.isStarted())
            return;

        ArrayList<String> description = player.getRoleSpecs();

        roleInfo.put(StateObject.roleColor, player.getFaction().getColor());
        roleInfo.put(StateObject.roleName, player.factionRole.getPlayerVisibleFactionRole().getName());
        roleInfo.put(StateObject.roleDescription, description);

        ArrayList<String> abilityNames = new ArrayList<>();
        for(Ability a: player.getAbilities()){
            if(a.isHiddenPassive())
                continue;
            abilityNames.add(a.getClass().getSimpleName());
        }
        roleInfo.put(StateObject.abilities, abilityNames);

        roleInfo.put(StateObject.breadCount, BreadAbility.getUseableBread(player));
        roleInfo.put(StateObject.isPuppeted, player.isPuppeted());
        roleInfo.put(StateObject.isBlackmailed, player.isSilenced());

        if(player.hasPuppets()){
            JSONArray jPuppets = new JSONArray();
            for(Player puppet: player.getPuppets()){
                jPuppets.put(puppet.getName());
            }
            roleInfo.put(StateObject.puppets, jPuppets);
        }

        if(player.is(Ghost.class)){
            String ghostWinCondition;
            if(player.isAlive()){
                ghostWinCondition = "The first step to winning is to die";
            }else{
                Player cause = player.getDeathType().getCause();
                if(cause == player && player.getCause() != null)
                    cause = player.getCause();
                if(cause == null || cause == player){
                    ghostWinCondition = "You've lost the game";
                }else{
                    Faction t = cause.getFaction();
                    if(t.isEnemy(t)){
                        ghostWinCondition = "You must assure the defeat of " + player.getName() + " to win.";
                    }else if(t.knowsTeam() || t._startingSize > 0)
                        ghostWinCondition = "You must assure the defeat of " + t.getName() + " to win.";
                    else{
                        ghostWinCondition = "You must assure the defeat of " + player.getName() + " to win.";
                    }
                }
            }
            roleInfo.put("ghostWinCondition", ghostWinCondition);
            if(player.isAlive())
                roleInfo.put(StateObject.isAlive, true);
            else
                roleInfo.put(StateObject.isAlive, "Ghost");
        }else{
            roleInfo.put(StateObject.isAlive, player.isAlive());
        }

        ArrayList<String> roleSpecs = player.getRoleSpecs();
        if(!roleSpecs.isEmpty())
            roleInfo.put("roleSpecs", roleSpecs);

        if(player.hasDayAction(Ability.INVITATION_ABILITY))
            roleInfo.put("acceptingRecruitment", player.ci.accepted);

        boolean electroExtraCharge = false;
        if(player.is(ElectroManiac.class)){
            ElectroManiac em = (ElectroManiac) player.getAbility(Ability.CHARGE);
            electroExtraCharge = em.usedDoubleAction == null;
        }
        roleInfo.put("electroExtraCharge", electroExtraCharge);
        if(player.is(Joker.class)){
            roleInfo.put("jokerActiveBounty", player.getAbility(Joker.class).activeBounty != null);
            roleInfo.put("jokerKills", player.getAbility(Joker.class).bounty_kills);
        }
        if(player.is(Jailor.class) && player.getAbility(Jailor.class).remainingCleanings > 0){
            roleInfo.put("jailCleanings", player.getAbility(Jailor.class).remainingCleanings);
        }
    }

    public static Object[] shouldShowPlayer(Player p, Player q) {
        if(p == q || p.isDead())
            return null;
        if(p.getFaction().equals(q.getFaction()) && p.getFaction().knowsTeam())
            return new Object[] {
                    q, q
            };
        for(Faction t: p.getFactions()){
            if(t.knowsTeam() && t.getMembers().contains(q)){
                return new Object[] {
                        q, q
                };
            }
        }
        if(Mason.IsMasonType(p) && q.getFactions().contains(p.getFaction()) && Mason.IsMasonType(q)){
            return new Object[] {
                    q, q
            };
        }

        if(!q.is(Disguiser.class) || q.isDead())
            return null;
        Disguiser dg = q.getAbility(Disguiser.class);
        if(dg.disguised == null)
            return null;

        Object[] subRet = shouldShowPlayer(p, dg.disguised);
        if(subRet == null)
            return null;
        return new Object[] {
                q, dg.disguised
        };
    }

    private void addJGraveYard(JSONObject state) throws JSONException {
        JSONArray graveYard = jGraveYard(narrator);
        state.getJSONArray(StateObject.type).put(StateObject.graveYard);
        state.put(StateObject.graveYard, graveYard);
    }

    public static JSONArray jGraveYard(Narrator n) throws JSONException {
        JSONArray graveYard = new JSONArray(), deathTypes;

        JSONObject graveMarker;
        String color, roleName;
        Long factionID;
        FactionRole factionRole;
        DeathType dt;
        for(Player p: n.getDeadPlayers().sortByDeath()){
            graveMarker = new JSONObject();
            if(p.getDeathType().isCleaned() || Marshall.isCurrentMarshallLynch(p)){
                color = Constants.A_CLEANED;
                factionID = null;
                roleName = "????";
            }else if(p.hasSuits()){
                factionRole = n.getPossibleMembers().get(p.suits.get(0));
                color = factionRole.faction.getColor();
                factionID = n.getFaction(color).id;
                roleName = factionRole.role.getName();
            }else if(p.is(Miller.class) && n.getBool(SetupModifier.MILLER_SUITED)){
                color = Miller.MillerColor(n);
                factionID = n.getFaction(color).id;
                roleName = Miller.getRoleFlip(p).getName();
            }else{
                color = p.getFaction().getColor();
                factionID = n.getFaction(color).id;
                roleName = p.getRoleName();
                // TODO again, i think i'll need to specify the abilities in this card
                if(p._lastWill != null && !p._lastWill.isEmpty())
                    graveMarker.put(StateObject.lastWill, p._lastWill);
            }
            graveMarker.put(StateObject.roleName, roleName);
            graveMarker.put(StateObject.color, color);
            graveMarker.put("factionID", factionID);
            graveMarker.put("name", p.getName());
            graveYard.put(graveMarker);

            dt = p.getDeathType();
            graveMarker.put("phase", dt.isDayDeath());
            graveMarker.put("day", dt.getDeathDay());

            deathTypes = new JSONArray();
            for(String[] s: dt.getList())
                deathTypes.put(s[1]);// 0 is the id, 1 is the description

            graveMarker.put("deathTypes", deathTypes);
        }

        return graveYard;
    }

    public static JSONObject getStateObjectActions(Player p) throws JSONException {
        JSONObject jActions = new JSONObject();
        JSONArray jActionList = new JSONArray();
        ActionList actions = null;
        if(p != null)
            actions = p.getActions();
        if(actions != null && (p.isAlive() || p.is(Ghost.class))){
            ArrayList<Action> subset = new ArrayList<>();

            String text, command;
            JSONObject jAction;
            SelectionMessage sm;
            JSONArray jPlayerNames;
            Faction t;
            for(Action a: actions){
                if(a.is(Ability.NIGHT_SEND) && !p.narrator.getPossibleMembers().hasMembersThatAffectSending(p.narrator))
                    continue;
                jAction = new JSONObject();
                sm = new SelectionMessage(p, true, false);
                jPlayerNames = new JSONArray();

                subset.clear();
                subset.add(a);
                text = "You will " + sm.add(a.getAbility().getActionDescription(subset)).access(p, new HTMLDecoder())
                        + ".";
                jAction.put("text", text);

                command = a.getCommand();
                if(command != null)
                    jAction.put("command", command.toLowerCase());
                else{
                    System.err.println("couldn't reverse this ability in StateObject");
                    System.err.println(p.toString());
                    p.getActions();
                    NodeSwitch.internalError(new NullPointerException());
                }
                if(a.is(Spy.COMMAND)){
                    t = p.narrator.getFaction(a.getOption());
                    jPlayerNames.put(t.getName());
                }else{
                    for(Player target: a.getTargets())
                        jPlayerNames.put(target.getName());
                }
                jAction.put(playerNames, jPlayerNames);
                jAction.put(option, a.getOption());
                jAction.put(option2, a.getOption2());
                jAction.put(option3, a.getOption3());

                jActionList.put(jAction);
            }

        }else{
            jActions.put("canAddAction", false);
        }

        jActions.put(StateObject.actionList, jActionList);
        return jActions;

    }

    private void addJActions(Player p, JSONObject state) throws JSONException {
        JSONObject jActions = getStateObjectActions(p);
        state.getJSONArray(StateObject.type).put(StateObject.actions);
        state.put(StateObject.actions, jActions);
    }

    private void addJRules(JSONObject state) throws JSONException {
        addJFactions(state);
        JSONObject jRules = new JSONObject();
        SetupModifiers rules = narrator._rules;
        Rule r;
        JSONObject ruleObject;
        for(SetupModifier key: rules.rules.keySet()){
            ruleObject = new JSONObject();
            r = rules.getRule(key);
            ruleObject.put("id", r.id);
            ruleObject.put(StateObject.NAME, SetupModifier.getLabel(narrator, key));
            if(r.getClass() == RuleInt.class){
                ruleObject.put("val", ((RuleInt) r).getValue());
            }else{
                ruleObject.put(StateObject.val, ((RuleBool) r).getValue());
            }
            jRules.put(r.id.toString(), ruleObject);
        }
        String id;
        for(Faction t: narrator.getFactions()){
            if(t.getColor().equals(Constants.A_SKIP))
                continue;

            for(FactionModifier fRuleID: FactionModifier.values()){

                id = t.getColor() + fRuleID;
                ruleObject = new JSONObject();
                ruleObject.put("id", id);
                ruleObject.put("name", fRuleID.getLabel(narrator));
                ruleObject.put("val", t._modifierMap.get(fRuleID));
                jRules.put(id, ruleObject);
            }
        }

        state.getJSONArray(StateObject.type).put(StateObject.rules);
        state.put(StateObject.rules, jRules);
    }

    /*
     * all factions object
     *
     * (faction name) -> faction object (faction color) -> faction object
     * factionName -> list of faction names
     *
     */

    /*
     * single faction object
     *
     * name -> faction name color -> faction color description -> faction
     * description isEditable -> can editFaction
     */

    private void addJFactions(JSONObject state) throws JSONException {
        JSONArray allies, enemies, factionNames = new JSONArray();
        JSONObject jFaction, allyInfo, jFactions = new JSONObject();
        ArrayList<Faction> team = new ArrayList<>();

        for(Faction t: narrator.getFactions())
            team.add(t);

        for(Faction f: narrator.getFactions()){
            if(narrator.getFaction(f.getColor()) != null)
                f = narrator.getFaction(f.getColor());
            jFaction = new JSONObject();

            jFaction.put(StateObject.color, f.getColor());
            jFaction.put("name", f.getName());
            jFaction.put("description", f.getDescription());
            jFaction.put("sheriffDetectables", f.getSheriffDetectables());

            jFaction.put(StateObject.rules, f.getRules());

            allies = new JSONArray();
            enemies = new JSONArray();
            for(Faction faction: narrator.getFactions()){
                allyInfo = new JSONObject();
                allyInfo.put("color", faction.getColor());
                allyInfo.put("name", faction.getName());
                if(faction.isEnemy(f))
                    enemies.put(allyInfo);
                else
                    allies.put(allyInfo);
            }
            jFaction.put("allies", allies);
            jFaction.put("enemies", enemies);

            factionNames.put(f.getColor());

            jFactions.put(f.getColor(), jFaction);
        }

        jFactions.put(StateObject.factionNames, factionNames);

        state.getJSONArray(StateObject.type).put(StateObject.factions);
        state.put(StateObject.factions, jFactions);
    }

    public static JSONArray getChatKeys(Narrator n, Player p) throws JSONException {
        JSONObject chat;
        JSONArray chats = new JSONArray();

        ArrayList<EventLog> logs;
        if(!n.isInProgress() && n.isStarted()){
            logs = n.getEventManager().getAllChats();
        }else if(p == null){
            logs = new ArrayList<>();
            logs.add(n.getEventManager().dayChat);
            logs.add(n.getEventManager().voidChat);
        }else{
            logs = p.getChats();
        }

        for(EventLog el: logs){
            chat = new JSONObject();

            chat.put(StateObject.chatName, el.getName());
            if(el.isActive() && el.getKey(p) != null)
                chat.put(StateObject.chatKey, el.getKey(p));

            chat.put(StateObject.chatType, el.getType());

            if(el instanceof DayChat){
                chat.put(StateObject.isDay, true);
            }

            chats.put(chat);
        }

        if(n.isInProgress() && p != null && (p.isAlive() || p.hasAbility(Ghost.class))){
            chat = new JSONObject();
            chat.put(StateObject.chatName, Feedback.CHAT_NAME);
            chat.put(StateObject.chatType, Feedback.TYPE);
            chats.put(chat);
        }else if(!n.isInProgress() && !n.isStarted()){
            chat = new JSONObject();
            chat.put(StateObject.chatName, "Pregame");
            chat.put(StateObject.chatType, Feedback.TYPE);
            chats.put(chat);
        }

        return chats;
    }

    private void addJVotes(JSONObject state, Game game) throws JSONException {
        if(!narrator.isDay())
            return;

        JSONObject voteHolder = VoteService.getVotes(game);

        state.getJSONArray(StateObject.type).put(StateObject.voteInfo);
        state.put(StateObject.voteInfo, voteHolder);
    }

    private int getVoterCount(Player p) {
        int count = 0;
        for(Player livingPlayer: narrator.getLivePlayers()){
            if(narrator.voteSystem.getVoteTarget(livingPlayer) == p)
                count++;
        }
        return count;
    }

    private JSONObject getJPlayerArray(Game game, PlayerList input, boolean canAddAnother, JSONObject optionTree)
            throws JSONException {
        JSONObject jAll = new JSONObject();
        JSONArray arr = new JSONArray();
        jAll.put("options", optionTree);
        jAll.put("canAddAction", canAddAnother);
        if(input.isEmpty())
            return jAll;

        JSONObject jo;
        for(Player pi: input){
            jo = new JSONObject();
            jo.put(StateObject.playerName, pi.getName());

            if(narrator.isStarted())
                jo.put(StateObject.playerVote, getVoterCount(pi));
            jo.put("isComputer", pi.isComputer());
            if(game != null && game.phoneBook.containsKey(pi))
                jo.put("userID", game.phoneBook.get(pi).id);
            arr.put(jo);
        }

        jAll.put("players", arr);

        return jAll;
    }

    private void addJPlayerLists(JSONObject state, Game game, Player p) throws JSONException {
        JSONObject playerLists = new JSONObject();
        playerLists.put(StateObject.type, new JSONArray());

        state.getJSONArray(StateObject.type).put(StateObject.playerLists);
        state.put(StateObject.playerLists, playerLists);

        if(!narrator.isStarted() || p == null || narrator.isFinished())
            return;
        if(narrator.phase != GamePhase.NIGHT_ACTION_SUBMISSION){
            PlayerList votes;
            if(narrator.isInProgress())
                votes = narrator.getLivePlayers();
            else
                votes = narrator.getAllPlayers();
            votes.sortByName();
            JSONObject names = getJPlayerArray(game, votes, false, null), optionTree;
            if(narrator.getBool(SetupModifier.HOST_VOTING)){
                playerLists.put("Controlled Voting", names);
                playerLists.getJSONArray(StateObject.type).put("Controlled Voting");
            }else if(p.isAlive() || !p.is(Ghost.class) || !p.hasPuppets()){
                playerLists.put("Vote", names);
                playerLists.getJSONArray(StateObject.type).put("Vote");
            }

            if(p.hasPuppets()){
                for(Player puppet: p.getPuppets()){
                    names = getJPlayerArray(game, votes, false, null);
                    playerLists.put(puppet.getName() + " Vote", names);
                    playerLists.getJSONArray(StateObject.type).put(puppet.getName() + " Vote");

                    names = getJPlayerArray(game, votes.copy().remove(puppet), false, null);
                    playerLists.put(puppet.getName() + " Punch", names);
                    playerLists.getJSONArray(StateObject.type).put(puppet.getName() + " Punch");
                }
            }

            PlayerList acceptableTargets;
            for(String s_ability: p.getDayCommands()){
                acceptableTargets = p.getAcceptableTargets(s_ability);
                if(acceptableTargets == null){// triggered by arson burn by having null acceptable targets
                    if(!s_ability.equalsIgnoreCase(Constants.RECRUITMENT)){
                        optionTree = getOptionTree(p, p.getAbility(s_ability).getAbilityNumber());
                        names = getJPlayerArray(game, new PlayerList(), p.getActions().canAddAnotherAction(s_ability),
                                optionTree);
                    }else{
                        optionTree = new JSONObject();
                        names = getJPlayerArray(game, new PlayerList(), false, optionTree);
                    }
                    playerLists.put(s_ability, names);
                    playerLists.getJSONArray(StateObject.type).put(s_ability);
                    continue;
                }
                if(acceptableTargets.size() == 1 && acceptableTargets.getFirst() == p)
                    // pretty sure this isn't used anymore
                    continue;
                if(acceptableTargets.isEmpty())// && ability == Ability.MAIN_ABILITY)
                    continue;
                names = getJPlayerArray(game, acceptableTargets, p.getActions().canAddAnotherAction(s_ability), null);
                playerLists.put(s_ability, names);
                playerLists.getJSONArray(StateObject.type).put(s_ability);
            }
            return;
        } // if is night
        if(narrator.isInProgress() && (p.isAlive() || p.is(Ghost.class))){
            ArrayList<Ability> abilities = p.getAvailableActions();
            JSONObject names, optionTree;
            for(Ability ability: abilities){
                if(!ability.isNightAbility(p))
                    continue;
                PlayerList acceptableTargets = ability.getAcceptableTargets(p);
                optionTree = getOptionTree(p, ability.getAbilityNumber());
                if(acceptableTargets == null){
                    names = getJPlayerArray(game, new PlayerList(),
                            p.getActions().canAddAnotherAction(ability.getAbilityNumber()), optionTree);
                }else{
                    if(acceptableTargets.isEmpty())
                        continue;
                    names = getJPlayerArray(game, acceptableTargets,
                            p.getActions().canAddAnotherAction(ability.getAbilityNumber()), optionTree);
                }

                playerLists.put(ability.getCommand(), names);
                playerLists.getJSONArray(StateObject.type).put(ability.getCommand());
            }
            if(p.is(Jailor.class)){
                Jailor jCard = (Jailor) p.getAbility(Jailor.MAIN_ABILITY);
                PlayerList jailedTargets = jCard.jailedTargets;
                if(!jailedTargets.isEmpty() && Jailor.lynchedHappened(narrator)){
                    names = getJPlayerArray(game, jailedTargets, false, null);
                    playerLists.put(Jailor.NO_EXECUTE, names);
                    playerLists.getJSONArray(StateObject.type).put(Jailor.NO_EXECUTE);
                }
            }
        }
    }

    public static JSONObject getOptionTree(Player p, int ability) throws JSONException {
        JSONObject megaMap = new JSONObject();
        JSONObject option2Tree;
        ArrayList<Option> oList1, oList2, oList3;

        Ability a = p.getAbility(ability);
        if(a == null)
            return megaMap;

        oList1 = a.getOptions(p);
        for(Option option1: oList1){
            oList2 = a.getOptions2(p, option1.getValue());
            if(oList2.isEmpty()){
                putOptNode(megaMap, option1, (JSONObject) null);
                continue;
            }
            option2Tree = new JSONObject();
            for(Option option2: oList2){
                oList3 = a.getOptions3(p, option1.getValue(), option2.getValue());
                if(oList3.isEmpty())
                    putOptNode(option2Tree, option2, (JSONObject) null);
                else
                    putOptNode(option2Tree, option2, oList3);
            }
            putOptNode(megaMap, option1, option2Tree);
        }

        return megaMap;
    }

    private static void putOptNode(JSONObject struct, Option option, ArrayList<Option> otherVals) throws JSONException {
        JSONObject jArray = new JSONObject();
        for(Option o: otherVals){
            jArray.put(o.getValue(), jOptionObject(o));
        }
        putOptNode(struct, option, jArray);
    }

    private static void putOptNode(JSONObject struct, Option o, JSONObject otherVals) throws JSONException {
        JSONObject jo = jOptionObject(o);

        if(otherVals == null)
            jo.put("map", 0);
        else
            jo.put("map", otherVals);
        struct.put(o.getValue(), jo);
    }

    private static JSONObject jOptionObject(Option o) throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.val, o.getValue());

        if(o.getColor() != null)
            jo.put(StateObject.color, o.getColor());
        if(!o.getName().equals(o.getValue()))
            jo.put(StateObject.NAME, o.getName());

        return jo;
    }

    public void addSetup(JSONObject state, Game game) throws JSONException {
        JSONObject setup = SetupJson.toJson(game._setup);
        state.put("setup", setup);
    }

    public abstract JSONObject getObject() throws JSONException;

    public abstract void write(User p, JSONObject jo);

    public void send(Game game, Player... players) {
        constructAndSend(Player.list(players), game);
    }

    public JSONObject construct(User user, Game game) throws JSONException {
        Player player = user == null ? null : user.player;
        JSONObject obj = getObject();
        for(String state: states){
            if(state.equals(RULES))
                addJRules(obj);
            else if(state.equals(PLAYERLISTS))
                addJPlayerLists(obj, game, player);
            else if(state.equals(DAYLABEL))
                addJDayLabel(obj);
            else if(state.equals(GRAVEYARD))
                addJGraveYard(obj);
            else if(state.equals(ROLEINFO))
                addJRoleInfo(player, obj);
            else if(state.equals(ACTIONS))
                addJActions(player, obj);
            else if(state.equals(VOTES))
                addJVotes(obj, user.game);
            else if(state.equals(SETUP))
                addSetup(obj, user.game);

        }
        for(String key: extraKeys.keySet())
            obj.put(key, extraKeys.get(key));
        return obj;
    }

    public JSONObject constructAndSend(User user, Game game) {
        try{
            JSONObject obj = construct(user, game);
            if(user != null)
                write(user, obj);
            return obj;
        }catch(JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    public void constructAndSend(PlayerList players, Game game) {
        User user;
        for(Player p: players){
            user = game.phoneBook.get(p);
            if(user != null)
                constructAndSend(user, game);
        }
    }

    public StateObject addKey(String key, Object val) {
        extraKeys.put(key, val);
        return this;
    }

    public void removeKey(String key) {
        extraKeys.remove(key);
    }

    public static final String guiUpdate = "guiUpdate";
    public static final String messageType = "messageType";

    public static final String isPrivateInstance = "isPrivateInstance";

    public static final String unreadUpdate = "unreadUpdate";
    public static final String count = "count";
    public static final String chatReset = "chatReset";
    public static final String chatKey = "chatKey";
    public static final String chatDay = "chatDay";
    public static final String chatType = "chatType";
    public static final String chatClue = "chatClue";
    public static final String chatName = "chatName";
    public static final String sender = "sender";

    public static final String dayLabel = "dayLabel";

    public static final String type = "type";

    public static final String playerList = "playerList";

    public static final String playerLists = "playerLists";

    public static final String requestGameState = "requestGameState";
    public static final String requestChat = "requestChat";
    public static final String requestUnreads = "requestUnreads";
    public static final String setReadChat = "setReadChat";

    public static final String roleType = "roleType";
    public static final String color = "color";
    public static final String description = "description";

    public static final String activeTeams = "activeTeams";

    public static final String displayName = "displayName";
    public static final String roleInfo = "roleInfo";
    public static final String roleColor = "roleColor";
    public static final String roleName = "roleName";
    public static final String roleBaseName = "roleBaseName"; // pretty sure this is unused now
    public static final String abilities = "abilities";
    public static final String roleDescription = "roleDescription";
    public static final String roleModifier = "roleModifier";
    public static final String puppets = "puppets";
    public static final String isAlive = "isAlive";

    public static final String isPuppeted = "isPuppeted";
    public static final String isBlackmailed = "isBlackmailed";
    public static final String breadCount = "breadCount";

    public static final String teamName = "teamName";
    public static final String teamDescription = "teamDescription";
    public static final String teamAllyColor = "teamAllyColor";
    public static final String teamAllyName = "teamAllyName";
    public static final String teamAllyRole = "teamAllyRole";

    public static final String teamChangeName = "teamChangeName";
    public static final String teamChangeColor = "teamChangeColor";
    public static final String teamChangeDescription = "teamChangeDescription";

    public static final String possibleRandoms = "possibleRandoms";
    public static final String spawn = "spawn";
    public static final String roleID = "roleID";
    public static final String setRandom = "setRandom";
    public static final String key = "key";

    public static final String gameStart = "gameStart";

    public static final String isDay = "isDay";
    public static final String dayNumber = "dayNumber";
    public static final String isObserver = "isObserver";
    public static final String getDayActions = "getDayActions";

    public static final String showButton = "showButton";

    public static final String graveYard = "graveYard";

    public static final String addAbility = "addAbility";
    public static final String hasModerator = "hiddenRandoms";
    public static final String hostID = "hostID";
    public static final String hostName = "hostName";

    public static final String rules = "rules";
    public static final String ruleID = "ruleID";
    public static final String val = "val";
    public static final String memberRuleExtras = "memberRuleExtras";
    public static final String modifierMap = "modifierMap";

    public static final String playerName = "playerName";
    public static final String playerVote = "playerVote";
    public static final String skipVote = "skipVote";
    public static final String isSkipping = "isSkipping";

    public static final String factions = "factions";
    public static final String factionNames = "factionNames";

    public static final String createTeam = "addTeam";
    public static final String removeTeamAlly = "removeTeamAlly";
    public static final String removeTeamEnemy = "removeTeamEnemy";
    public static final String enemy = "enemy";
    public static final String ally = "ally";
    public static final String createFactionRole = "addTeamRole";
    public static final String removeTeamRole = "removeTeamRole";
    public static final String addSheriffDetectable = "addSheriffDetectable";
    public static final String removeSheriffDetectable = "removeSheriffDetectable";
    public static final String detectableColor = "detectableColor";

    public static final String actions = "actions";

    public static final String oldAction = "oldAction";
    public static final String newAction = "newAction";
    public static final String targets = "targets";
    public static final String command = "command";
    public static final String option = "option";
    public static final String option2 = "option2";
    public static final String option3 = "option3";
    public static final String actionList = "actionList";
    public static final String playerNames = "playerNames";

    public static final String voteCounts = "voteCounts";
    public static final String voteInfo = "voteInfo";
    public static final String currentVotes = "currentVotes";
    public static final String finalScore = "finalScore";

    public static final String removeBots = "removeBotUsers";

    public static final String nodeError = "nodeError";

    public static final String action = "action";
    public static final String nameChange = "nameChange";
    public static final String resetGame = "resetGame";

    public static final String setupKey = "setupKey";
    public static final String setupName = "setupName";
    public static final String customization = "customization";

    public static final String lastWill = "lastWill";
    public static final String teamLastWill = "teamLastWill";
    public static final String willText = "willText";

    public static final String releaseToken = "releaseToken";
    public static final String token = "token";

    public static final String speechContent = "speechContent";
}
