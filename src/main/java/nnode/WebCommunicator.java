package nnode;

import game.event.Feedback;
import game.event.Message;
import game.event.SelectionMessage;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.enums.GamePhase;

public class WebCommunicator {

    public static void sendMessage(User np, Message f) {
        if(f instanceof SelectionMessage){
            if(np.player == ((SelectionMessage) f).owner)
                return;
        }
        if(f instanceof Feedback && np.player.narrator.isNight()){
            // these are cop and executioner game start
            if(f.getPhase() == GamePhase.NIGHT_ACTION_SUBMISSION || f.getDay() != 0)
                return;
        }
        if(np.player == null)
            return;

        try{
            JSONObject j = new JSONObject();
            j.put(StateObject.message, Game.AppendMessage(new JSONArray(), f, np.player, np.player.getName()));
            np.write(j);
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public static void sendWarning(String s, User user) {
        try{
            JSONObject jo = new JSONObject();
            jo.put(StateObject.message, StateObject.warning);
            jo.put(StateObject.warning, s);
            user.write(jo);
        }catch(JSONException e1){
            e1.printStackTrace();
        }
    }
}
