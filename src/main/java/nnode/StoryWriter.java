package nnode;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Pattern;

import game.logic.DeathType;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.listeners.CommandListener;
import game.logic.support.Constants;
import game.logic.support.Util;
import game.roles.Survivor;
import models.Command;
import repositories.CommandRepo;
import repositories.Connection;
import repositories.PlayerDeathRepo;
import repositories.PlayerRepo;
import repositories.ReplayRepo;
import repositories.UserRepo;

public class StoryWriter implements CommandListener {

    public interface StoryWriterError {
        boolean onError(SQLException e);
    }

    public Connection connection;
    private Long gameID;
    boolean erroredOut = false;
    private StoryWriterError swe;
    private ArrayList<Command> initCommands;

    public boolean logJSONInput = true;

    public StoryWriter(StoryWriterError swe, ArrayList<Command> initialCommands) {
        this.swe = swe;
        this.initCommands = initialCommands;
        try{
            this.connection = new Connection();
        }catch(SQLException e){
            e.printStackTrace();
            Util.log(e, "Failed to instantiate connection for StoryWriter.");
        }
    }

    public void setGameID(long gameID, Map<Long, Player> databaseMap) {
        this.gameID = gameID;
        if(this.initCommands != null)
            for(Command s: initCommands)
                onCommand(databaseMap.get(s.playerID), s.text);
        this.initCommands = null;
    }

    public long getReplayID() { // rename to getReplayID
        return this.gameID;
    }

    long counter = 0;

    @Override
    public void onCommand(Player player, String text) {
        if(erroredOut)
            return;
        text = text.substring(0, Math.min(550, text.length()));
        try{
            Long databaseID = player == null ? null : player.databaseID;
            CommandRepo.create(this.connection, ++counter, databaseID, text, this.gameID);
        }catch(SQLException e){
            Util.log(e, "Command insert failed");
            if(swe == null || swe.onError(e))
                erroredOut = true;
        }
    }

    public static Short deathDayToInt(Player p) {
        DeathType death = p.getDeathType();
        if(death == null)
            return (short) (p.narrator.getDayNumber() * 10);
        short d = (short) (death.getDeathDay() * 10);
        if(!death.isDayDeath()){
            d += 5;
        }
        return d;
    }

    public static String GetToken(String name, PreparedStatement ps) throws SQLException {
        ps.setString(2, name);
        ResultSet rs = ps.executeQuery();
        String token;
        if(rs.next())
            token = rs.getString(1);
        else
            token = null;
        rs.close();
        return token;
    }

    public static int GetPoints(Player p) {
        if(p.isWinner())
            return 10;
        return 5;
    }

    public static void UpdateStats(Narrator n, Connection c, long replayID) throws SQLException {
        String deathType = null;
        for(Player p: n._players){
            if(p.isDead()){
                for(String[] dType: p.getDeathType().attacks){
                    if(dType[0].startsWith(Constants.FACTION_KILL_FLAG[0]))
                        deathType = Constants.FACTION_KILL_FLAG[0];
                    else
                        deathType = dType[0];
                    break;
                }
            }else{
                deathType = Survivor.class.getSimpleName().toLowerCase();
            }
            if(deathType == null)
                throw new NarratorException(p.getID() + " is dead with no attacks.");

            PlayerRepo.updateDeathDayAndIsWinner(c, replayID, p.getID(), deathDayToInt(p), p.isWinner());
            PlayerDeathRepo.createDeath(c, replayID, p.getID(), deathType);
            UserRepo.updatePointsAndWinRate(c, replayID, p.getID(), GetPoints(p));
        }
    }

    public void endGame(Narrator n) throws SQLException {
        if(connection == null)
            return;
        ReplayRepo.markCompleted(connection, gameID);
        UpdateStats(n, connection, gameID);
        closeSQL();
    }

    public void closeSQL() {
        if(connection == null)
            return;
        connection.close();
        connection = null;
    }

    private ArrayList<String> commands;

    public ArrayList<String> enableLogging() {
        commands = new ArrayList<>();
        return commands;
    }

    public static void swapName(Connection c, long storyID, String oldName, String newName) throws SQLException {
        // update playerNames
        String infoQuery, updateQuery;
        infoQuery = "SELECT game_id, name FROM player_saved_games";
        updateQuery = "UPDATE player_saved_games SET name = ? WHERE game_id = ? and name = ?";
        PreparedStatement commandQuery = c.prepareStatement(updateQuery);
        commandQuery.setString(1, newName);
        commandQuery.setLong(2, storyID);
        commandQuery.setString(3, oldName);
        commandQuery.execute();
        commandQuery.close();

        // update roles_list_association
        updateQuery = "UPDATE roles_list_saved_games SET playerAssigned = ? WHERE game_id = ? and playerAssigned = ?";
        commandQuery = c.prepareStatement(updateQuery);
        commandQuery.setString(1, newName);
        commandQuery.setLong(2, storyID);
        commandQuery.setString(3, oldName);
        commandQuery.execute();
        commandQuery.close();

        infoQuery = "SELECT command_id, command FROM commands_saved_games where game_id = ?;";
        updateQuery = "UPDATE commands_saved_games SET command = ? WHERE command_id = ? and command = ?";

        commandQuery = c.prepareStatement(infoQuery);
        commandQuery.setLong(1, storyID);
        PreparedStatement commandUpdater = c.prepareStatement(updateQuery);

        String text, newText;
        long comand_id;
        ResultSet commandResult = commandQuery.executeQuery();
        while (commandResult.next()){
            comand_id = commandResult.getLong(1);

            text = commandResult.getString(2);
            newText = text.replaceAll(Pattern.quote(oldName), newName);

            if(text.equals(newText))
                continue;

            commandUpdater.setString(1, newText);
            commandUpdater.setLong(2, comand_id);
            commandUpdater.setString(3, text);
            commandUpdater.execute();
        }

        commandUpdater.close();

        commandResult.close();
        commandQuery.close();
    }

}
