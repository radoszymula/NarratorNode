package nnode;

import java.util.ArrayList;

import game.event.DeathAnnouncement;
import game.event.EventDecoder;
import game.event.Message;
import game.event.OGIMessage;
import game.event.SelectionMessage;
import game.event.VoteAnnouncement;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.PhaseException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownPlayerException;
import game.logic.exceptions.UnknownRoleException;
import game.logic.exceptions.UnknownTeamException;
import game.logic.exceptions.VotingException;
import game.logic.listeners.NarratorListener;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.roles.Ability;
import game.roles.Assassin;
import game.roles.Burn;
import game.roles.Mayor;

public abstract class IntegrationHandler extends CommandHandler implements NarratorListener {

    public PlayerList texters;

    public IntegrationHandler(Narrator n, PlayerList texters) {
        super(n);

        this.texters = texters;
    }

    public boolean canEditGame() {
        return false;
    }

    public void setTexters(PlayerList texters) {
        this.texters = texters;
    }

    public abstract void sendRoleCard(Player p);

    public abstract void sendHelpPrompt(Player owner);

    @Override
    public void onRolepickingPhaseStart() {
    }

    /*
     * public Object[] prefer(Player owner, String s){ Object[] result =
     * super.prefer(owner, s); if(result[0] == TEAM_PREFER){
     * owner.warn("You preferred to be on " + ((Team) result[1]).getName()); }else
     * if(result.length > 2){ owner.warn("You tried to prefer " + ((RoleTemplate)
     * result[1]).getName() + " but actually preferred to be a citizen type role");
     * }else{ owner.warn("You preferred to be a " + ((RoleTemplate)
     * result[1]).getName()); } return result; }
     */

    public static final String HELP = "commands";
    public static final String ROLE_INFO = "roles list";
    public static final String LIVE_PEOPLE = "get players";
    public static final String TEAM_INFO = "get team";
    public static final String NIGHT_HELP = "help night";
    public static final String NIGHT_HELP2 = "night help";
    public static final String DAY_HELP = "help day";
    public static final String DAY_HELP2 = "day help";
    public static final String VOTE_COUNT = "vote count";
    public static final String WAITING = "waiting";

    public void text(Player owner, String message, boolean sync) {
        while (message.substring(message.length() - 1).equals(" "))
            message = message.substring(0, message.length() - 1);
        switch (message.toLowerCase()){
        case HELP:
            sendHelpPrompt(owner);
            return;
        case ROLE_INFO:
            sendExtendedRoleInfo(owner);
            return;
        case LIVE_PEOPLE:
            sendLivePlayerList(owner);
            return;
        case TEAM_INFO:
            sendTeamInfo(owner);
            return;
        case NIGHT_HELP2:
        case NIGHT_HELP:
            sendNightTextPrompt(owner);
            return;
        case DAY_HELP2:
        case DAY_HELP:
            sendDayTextPrompt(owner);
            return;
        case VOTE_COUNT:
            sendVoteCount(owner);
            return;
        /*
         * case WAITING: sendWaiting(owner); return;
         */
        default:
            try{
                synchronized (narrator){
                    super.command(owner, message, "text", null, false);// no mod commands here
                }
            }catch(IllegalActionException e){
                if(e.getMessage().length() == 0){
                    new OGIMessage(owner, getHelpCommand());
                }else{
                    new OGIMessage(owner, e.getMessage());
                }

                printException(e);
            }catch(UnknownPlayerException f){
                new OGIMessage(owner,
                        "Unknown player name. Type " + SQuote(LIVE_PEOPLE) + " to get a list of players.");
                printException(f);
            }catch(UnknownRoleException q){
                new OGIMessage(owner, "Team/Role not found!");
                printException(q);
            }catch(UnknownTeamException e){
                new OGIMessage(owner, "Unknown team name. Type " + SQuote(NIGHT_HELP)
                        + " to get info about what you can do during the night.");
                printException(e);

            }catch(PlayerTargetingException g){
                new OGIMessage(owner, g.getMessage());
                printException(g);

            }catch(PhaseException e){
                new OGIMessage(owner, e.getMessage());
                printException(e);

            }catch(IllegalStateException e){
                new OGIMessage(owner, e.getMessage());
                printException(e);

            }

            catch(VotingException e){
                new OGIMessage(owner, e.getMessage());
                printException(e);
            }

        }
    }

    public abstract String getHelpCommand();

    private void printException(Throwable e) {
        System.err.println(e.getMessage());
    }

    // public abstract void sendWaiting(Player p);

    public abstract void sendExtendedRoleInfo(Player owner);

    public abstract void sendLivePlayerList(Player owner);

    public abstract void sendTeamInfo(Player p);

    public abstract void sendNightPrompt(Player p);

    @Override
    public void onGameStart() {

    }

    @Override
    public void onGameEnd() {
        broadcast(narrator.getWinMessage().access(Message.PUBLIC));
    }

    public void sendNightTextPrompt(Player texter) {
        StringBuilder message = new StringBuilder();
        for(String s: texter.getRoleSpecs())
            message.append(" " + s);

        String teamNightPrompt = texter.getFaction().getNightPrompt(texter);
        if(teamNightPrompt != null)
            message.append(teamNightPrompt + "\n");

        if(texter.getFaction().knowsTeam() && texter.getFaction().size() > 1)
            message.append("To talk to your allies : -  " + SQuote(SAY + " teamName message") + "\n");

        message.append("If you want to cancel all your night actions, type " + SQuote(Constants.CANCEL) + ".\n");
        message.append("After you're done submitting actions, text " + SQuote(END_NIGHT)
                + " so night can end.  If you want to cancel your bid to end night, type it again.\n");

        message.append("To see who hasn't ended the night : - " + SQuote(WAITING));

        new OGIMessage(texter, message.toString());
    }

    public void sendDayTextPrompt(Player texter) {
        String message = "To vote or change your vote : - " + SQuote(VOTE + " name");
        message += ".\nTo unvote: - " + SQuote(UNVOTE);
        message += ".\nTo end the day so that no one is publicly executed : - " + SQuote(SKIP_VOTE);
        message += ".\nTo see current vote counts : - " + SQuote(VOTE_COUNT);

        new OGIMessage(texter, message);

        if(texter.hasDayAction(Ability.INVITATION_ABILITY))
            new OGIMessage(texter, "To accept your recruitment invitation, text " + SQuote(Constants.ACCEPT)
                    + ". Otherwise, text " + SQuote(Constants.DECLINE));
        if(texter.hasDayAction(Burn.COMMAND)){
            new OGIMessage(texter, "To burn everyone you doused right now, text " + SQuote(Burn.BURN_LOWERCASE));
        }
        if(texter.hasDayAction(Mayor.COMMAND)){
            new OGIMessage(texter, "To reveal as mayor, text " + SQuote(Mayor.REVEAL_LOWERCASE));
        }
        if(texter.hasDayAction(Assassin.COMMAND))
            new OGIMessage(texter, "To assassinate someone, text " + SQuote(Assassin.ASSASSINATE_LOWERCASE + " name"));
    }

    public abstract void sendVoteCount(Player texter);

    @Override
    public void onModKill(PlayerList p) {
        broadcast(p + " suicided!");
    }

    @Override
    public void onDayPhaseStart(PlayerList dead) {
        if(dead != null){
            if(dead.isEmpty())
                broadcast("No one died!");
            else{
                for(Player deadPerson: dead){
                    if(deadPerson == null)
                        throw new NullPointerException();
                    broadcast(deadPerson.getDescription() + " was found " + deadPerson.getDeathType().toText());
                }
            }
        }

        broadcast("It is now daytime. Please vote.  Once someone has enough votes, the day will end.");

        for(Player p: texters){
            if(p.isSilenced())
                new OGIMessage(p, "You're blackmailed. You can't talk and can only vote to skip the day.");
        }

    }

    public abstract void broadcast(String s);

    public void broadcast(Message m) {
        broadcast(m.access(Message.PUBLIC, getDecoder()));
    }

    public abstract EventDecoder getDecoder();

    public String SQuote(String s) {
        return "\'" + s + "\'";
    }

    @Override
    public void onDayBurn(Player arson, PlayerList burned, DeathAnnouncement e) {

    }

    @Override
    public void onElectroExplosion(PlayerList deadPeople, DeathAnnouncement explosion) {
        broadcast(explosion.access(Message.PUBLIC));
    }

    @Override
    public void onTargetSelection(Player p, SelectionMessage selectionMessage) {

    }

    @Override
    public void onTargetRemove(Player p, String command, PlayerList r) {

    }

    public static String END_NIGHT_CONF = "You've ended the night";

    @Override
    public void onEndNight(Player p, boolean forced) {
        if(!forced)
            new OGIMessage(p, END_NIGHT_CONF);
    }

    public static String CANCEL_END_NIGHT_CONF = "You've canceled your motion to end the night";

    @Override
    public void onCancelEndNight(Player p) {
        new OGIMessage(p, CANCEL_END_NIGHT_CONF);
    }

    @Override
    public void onRoleReveal(Player m, Message e) {
        broadcast(m.getName() + " has revealed as " + m.getRoleName() + "!");
    }

    @Override
    public void onAssassination(Player assassin, Player victim, DeathAnnouncement e) {
        if(victim != null)
            broadcast(assassin.getName() + " assassinated " + victim.getDescription());
    }

    @Override
    public void onMessageReceive(Player receiver, Message s) {

    }

    @Override
    public void onWarningReceive(Player player, Message m) {

    }

    @Override
    public void onVote(Player p, Player q, VoteAnnouncement e) {
        if(narrator.isDay())
            broadcast(e);
    }

    @Override
    public void onVoteCancel(Player p, Player q, VoteAnnouncement e) {
        broadcast(e);
    }

    @Override
    public void onNightEnding() {

    }

    @Override
    public void onAnnouncement(Message nl) {
        broadcast(nl);
    }

    @Override
    public void onDayActionSubmit(Player submitter, Action a) {
        ArrayList<Object> list = a.getAbility().getActionDescription(a);
        list.add(0, "You will ");
        new OGIMessage(submitter, list);
    }
}
