package nnode;

import java.util.ArrayList;
import java.util.Random;

import game.ai.Brain;
import game.ai.Computer;
import game.logic.Narrator;
import game.logic.Player;
import game.setups.BraveNewWorld;
import game.setups.Setup;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;

public class Visualizer {
    static final int COUNT = 1000;

    static Random r;

    public static void main(String args[]) {
        System.out.print("[");

        ArrayList<Long> seeds = new ArrayList<>();

        long seed;
        r = new Random();
        for(int i = 0; i < COUNT; i++){
            seed = r.nextLong();
            // seed = Long.parseLong("221711864825612808");
            System.err.println(COUNT - i + "\t" + seed);
            if(i != 0)
                System.out.print(",");

            try{
                // runStory(seed);
                System.out.print(runStory(seed));
            }catch(Throwable t){
                t.printStackTrace();
                seeds.add(seed);
            }

        }

        System.out.print("]");

        System.err.println(seeds);
    }

    public static JSONObject runStory(long seed) throws JSONException {

        Narrator n = new Narrator();
        Setup s = new BraveNewWorld(n);

        for(int i = 0; i < 23; i++){
            n.addPlayer(Computer.toLetter(i + 1)).setComputer();
        }
        n.setSeed(seed);
        s.applyRolesList();

        Brain.EndGame(n, seed);

        JSONObject jo = new JSONObject();

        JSONArray players = new JSONArray();
        JSONObject x;
        int cultSize = 0;
        for(Player p: n._players){
            x = new JSONObject();
            x.put("role", p.getRoleName());
            x.put("team", p.getFaction().getName());
            x.put("winner", p.isWinner());
            if(p.isCulted())
                cultSize++;
            players.put(x);
        }
        jo.put("seed", seed);
        jo.put("game", players);
        jo.put("game_duration", n.getDayNumber());
        jo.put("cult_size", cultSize);

        return jo;
    }
}
