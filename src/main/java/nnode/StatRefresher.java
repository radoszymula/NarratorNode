package nnode;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import game.logic.support.StoryPackage;
import repositories.Connection;
import repositories.PlayerDeathRepo;
import repositories.PlayerRepo;
import repositories.UserRepo;

public class StatRefresher {

    static Connection c;

    public static void main(String args[]) throws SQLException {
        c = new Connection();
        ArrayList<Long> finishedGameIDs = getFinishedGameIDs(c);

        clearTables(c);

        ArrayList<Long> errorGameIDs = new ArrayList<>();

        StoryPackage sp;
        for(Long finID: finishedGameIDs){
            System.err.println(
                    "#" + finID + "\t(" + (finishedGameIDs.indexOf(finID) + 1) + " / " + finishedGameIDs.size() + ")");
            sp = DBStoryPackage.deserialize(c, finID);
            if(sp == null)
                continue;

            sp.runCommands();

            if(sp.narrator.isInProgress()){
                errorGameIDs.add(sp.replayID);
                continue;
            }

            StoryWriter.UpdateStats(sp.narrator, c, sp.replayID);
        }

        System.err.println(errorGameIDs);

        c.close();
    }

    static ArrayList<Long> getFinishedGameIDs(Connection connection) throws SQLException {
        ArrayList<Long> finishedGames = new ArrayList<>();

        String queryString = "SELECT id FROM replays WHERE instance_id is null;";

        PreparedStatement ps = connection.prepareStatement(queryString);
        ResultSet rs = ps.executeQuery();
        while (rs.next())
            finishedGames.add(rs.getLong(1));

        rs.close();
        ps.close();

        return finishedGames;
    }

    static ArrayList<Long> getGames(Connection connection) throws SQLException {
        ArrayList<Long> finishedGames = new ArrayList<>();

        String queryString = "SELECT game_id FROM saved_games;";

        PreparedStatement ps = connection.prepareStatement(queryString);
        ResultSet rs = ps.executeQuery();
        while (rs.next())
            finishedGames.add(rs.getLong(1));

        rs.close();
        ps.close();

        return finishedGames;
    }

    private static void clearTables(Connection c) throws SQLException {
        PlayerRepo.clearIsWinnerAndDeathDay(c);
        PlayerDeathRepo.deleteAll(c);
        UserRepo.clearPointsAndWinRate(c);
    }
}
