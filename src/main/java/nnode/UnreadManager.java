package nnode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import game.logic.support.Util;

public class UnreadManager {

    String logName;
    public HashMap<User, Integer> unreadCount;

    public UnreadManager(String name, Set<User> members) {
        unreadCount = new HashMap<>();
        for(User p: members)
            unreadCount.put(p, 0);
        this.logName = name;
    }

    public Set<User> getMembers() {
        return unreadCount.keySet();
    }

    public void push(ArrayList<User> toExclude) {
        if(toExclude == null)
            toExclude = new ArrayList<>();

        for(User user: unreadCount.keySet()){
            if(toExclude.contains(user))
                continue;
            increment(user);
        }
        for(User user: getMembers()){
            if(!toExclude.contains(user))
                sendUnread(user);
        }
    }

    public void moreUnread(User p) {
        if(!unreadCount.containsKey(p))
            unreadCount.put(p, 0);
        increment(p);
        sendUnread(p);
    }

    private void increment(User user) {
        int former = unreadCount.get(user);
        unreadCount.put(user, former + 1);
    }

    public void sendUnread(User user) {
        if(user.player != null){
            if(!unreadCount.containsKey(user)){
                Util.log(new NullPointerException(), "not found [" + logName + "] - " + user.player.getName());
                NodeSwitch.internalError(new NullPointerException());
            }else
                user.sendUnread(logName, unreadCount.get(user));

        }
    }

    public void setRead(User user) {
        if(user.player != null && unreadCount.containsKey(user))
            unreadCount.put(user, 0);

    }

    public int unreadCount(User user) {
        if(unreadCount.containsKey(user))
            return unreadCount.get(user);
        return 0;
    }

    public void addPlayer(User user) {
        unreadCount.put(user, 0);
    }

    public void removeUser(User user) {
        unreadCount.remove(user);
    }

    public int remove(User user) {
        if(!unreadCount.containsKey(user))
            return 0;
        return unreadCount.remove(user);
    }

    public void add(User user, int unreads) {
        unreadCount.put(user, unreads);
    }

}
