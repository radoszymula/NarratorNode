package nnode;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.Scanner;

import game.logic.support.StoryPackage;
import json.JSONException;

public class ProductionDebugger {

    public static final void main(String[] args) throws IOException, JSONException {
        StoryPackage sp = loadStory(41);

        sp.runCommands();

        System.err.println(sp.narrator.getEndedNightPeople());
    }

    public static String tackOn(long id) throws IOException, JSONException, SQLException {
        StoryPackage sp = loadStory(id);
        NodeSwitch.loadInstance(sp);

        return sp.instanceID;
    }

    public static StoryPackage loadStory(long id) throws IOException, JSONException {
        String rawData = Long.toString(id);
        String type = "application/x-www-form-urlencoded";
        String encodedData = URLEncoder.encode(rawData, "UTF-8");
        URL u = new URL("http://narrator.systeminplace.net/storyDownload");
        HttpURLConnection conn = (HttpURLConnection) u.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", type);
        conn.setRequestProperty("Content-Length", String.valueOf(encodedData.length()));
        OutputStream os = conn.getOutputStream();
        os.write(encodedData.getBytes());

        Scanner scanner = new Scanner(conn.getInputStream());
        scanner.useDelimiter("\\A");
        String result = scanner.hasNext() ? scanner.next() : "";

        scanner.close();
        return JSONStoryPackage.deserialize(result);
    }
}
