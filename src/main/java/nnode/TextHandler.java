package nnode;

import game.event.EventDecoder;
import game.event.EventList;
import game.event.OGIMessage;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.roles.Ability;
import game.roles.GunAbility;
import game.roles.support.Gun;
import game.roles.support.Vest;
import models.SetupHidden;

public class TextHandler extends IntegrationHandler {

    public TextHandler(Narrator n, PlayerList texters) {
        super(n, texters);
    }

    @Override
    public void broadcast(String s) {
        new OGIMessage(texters, s);
    }

    @Override
    public void onNightPhaseStart(PlayerList dead, PlayerList poisoned, EventList el) {
        broadcast("It is now nighttime.");

        boolean isFirstNight = narrator.isFirstNight();

        for(Player p: texters){
            sendNightPrompt(p);
            if(isFirstNight){
                sendNightTextPrompt(p);
            }
            if(GunAbility.getTotalGunCount(p) > 0)
                new OGIMessage(p, "To use your " + narrator.getAlias(Gun.ALIAS) + ", type "
                        + Ability.NQuote(Constants.GUN_COMMAND) + "\n");
            if(p.getPerceivedVestCount() > 0)
                new OGIMessage(p, "To use your " + narrator.getAlias(Vest.ALIAS) + ", type "
                        + Ability.NQuote(Constants.VEST_COMMAND) + "\n");

        }
    }

    @Override
    public void sendRoleCard(Player texter) {
        String message = "You are a " + texter.getRoleName() + "!";
        Faction t = texter.getFaction();
        if(!t.isSolo())
            message += " You are part of the " + t.getName() + ".";
        if(t.knowsTeam() && t.getMembers().getLivePlayers().size() > 1){
            message += " Your teammates are " + t.getMembers().sortByName().toString() + ".";
        }
        for(String s: texter.getRoleSpecs())
            message += (" " + s);

        new OGIMessage(texter, message);

    }

    @Override
    public void sendHelpPrompt(Player owner) {
        String message = "See roles List - " + SQuote(ROLE_INFO);
        message += "\nSee live players - " + SQuote(LIVE_PEOPLE);
        if(owner.getFaction().knowsTeam() && owner.getFaction().getMembers().size() > 1)
            message += "\nSee team members - " + SQuote(TEAM_INFO);
        message += "\nGet day help - " + SQuote(DAY_HELP);
        message += "\nGet night help - " + SQuote(NIGHT_HELP);
        new OGIMessage(owner, message);
    }

    @Override
    public String getHelpCommand() {
        return "Unknown command.  Type " + HELP + " to see a list of commands.";
    }

    @Override
    public void sendExtendedRoleInfo(Player owner) {
        String roles_list_message = "These are the roles in game:\n";
        for(SetupHidden setupHidden: narrator.getRolesList()){
            if(!setupHidden.hidden.isHiddenSingle())
                roles_list_message += setupHidden.hidden.getName() + "\n";
            else
                roles_list_message += narrator.getFaction(setupHidden.hidden.getColors().iterator().next()) + " "
                        + setupHidden.hidden.getName() + "\n";
        }
        roles_list_message = roles_list_message.substring(0, roles_list_message.length() - 1);
        new OGIMessage(owner, roles_list_message);
    }

    @Override
    public void sendLivePlayerList(Player owner) {
        String list_of_texters = "These are the list of people in the game: ";
        for(Player p: narrator.getLivePlayers())
            list_of_texters += (p.getName() + ", ");
        new OGIMessage(owner, list_of_texters);
    }

    @Override
    public void sendTeamInfo(Player p) {
        if(p.getFaction().knowsTeam()){
            Faction t = p.getFaction();
            PlayerList team = t.getMembers();
            if(team.isEmpty()){
                new OGIMessage(p, "You dont' have any teammembers.");
            }else{
                PlayerList living = team.getLivePlayers();
                if(living.isEmpty()){
                    new OGIMessage(p, "All your teammates died");
                }else
                    new OGIMessage(p, "You're teammates are: "
                            + p.getFaction().getMembers().getLivePlayers().sortByName().toString() + "");
            }
        }else
            new OGIMessage(p, "I can't tell you who's on your team.");
    }

    // @Override
    public void sendWaiting(Player p) {
        if(narrator.isNight())
            new OGIMessage(p,
                    "Waiting on " + narrator.getLivePlayers().remove(narrator.getEndedNightPeople()).getStringName());
        else
            new OGIMessage(p, "It's not nighttime");
    }

    @Override
    public void sendVoteCount(Player texter) {
//    	StringBuilder sb = new StringBuilder();
//    	sb.append("Vote Count\n");
//    	for(Player p: n._players){
//    		if(!p.getVoters().isEmpty()){
//    			sb.append(p.getName());
//    			sb.append(" : \t");
//    			sb.append(p.getVoteCount());
//    			sb.append("\n");
//    		}
//    	}
//    	int skipVoteCount = n.Skipper.getVoteCount();
//    	if(skipVoteCount != 0){
//    		sb.append("Skip Day : \t");
//    		sb.append(skipVoteCount);
//    	}else{
//    		
//    	}
//    	
//    	new OGIMessage(texter, sb.toString());
    }

    @Override
    public void sendNightPrompt(Player p) {
        String message = SQuote(END_NIGHT) + " so night can end.";
        if(p.getCommands().isEmpty()){
            new OGIMessage(p, "Type " + message);
        }else{
            new OGIMessage(p, "Submit your night action(s).  When you're done, type " + message);
        }
    }

    @Override
    public void onTrialStart(Player trailedPlayer) {
    }

    @Override
    public EventDecoder getDecoder() {
        return null;
    }

    @Override
    public void onVotePhaseStart() {
    }

    @Override
    public void onVotePhaseReset(int resetsRemaining) {
    }

}
