package nnode;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import game.ai.Computer;
import game.event.Announcement;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.Constants;
import game.logic.support.DeathDescriber;
import game.logic.support.HTString;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.Util;
import game.roles.support.Suit;
import repositories.Connection;

public class DatabaseDeath implements DeathDescriber {

    Connection dbConn;
    ArrayList<Integer> usedStories;
    Random r;
    String genre;
    boolean productionMode = false;

    public DatabaseDeath(long seed) throws SQLException {
        dbConn = new Connection();
        usedStories = new ArrayList<>();
        r = new Random();
        r.setSeed(seed);
    }

    @Override
    public void setGenre(String genre) {
        this.genre = genre;
    }

    public DatabaseDeath setProductionMode(boolean b) {
        productionMode = b;
        return this;
    }

    private static boolean GENRE = true, NO_GENRE = false, TEAM = true, NO_TEAM = false, KILL_TYPE = true,
            NO_KILL_TYPE = false, ROLE_TYPE = true, NO_ROLE_TYPE = false;

    String chosenStory;

    @Override
    public boolean hasDeath(Player dead) {
        try{
            chosenStory = getStory(dead, TEAM, KILL_TYPE, ROLE_TYPE, GENRE); // 111
            if(chosenStory == null)
                chosenStory = getStory(dead, TEAM, KILL_TYPE, NO_ROLE_TYPE, GENRE); // 110
            if(chosenStory == null)
                chosenStory = getStory(dead, TEAM, NO_KILL_TYPE, ROLE_TYPE, GENRE); // 101
            if(chosenStory == null)
                chosenStory = getStory(dead, NO_TEAM, KILL_TYPE, ROLE_TYPE, GENRE); // 011
            if(chosenStory == null)
                chosenStory = getStory(dead, TEAM, NO_KILL_TYPE, NO_ROLE_TYPE, GENRE); // 100
            if(chosenStory == null)
                chosenStory = getStory(dead, NO_TEAM, KILL_TYPE, NO_ROLE_TYPE, GENRE); // 010
            if(chosenStory == null)
                chosenStory = getStory(dead, NO_TEAM, NO_KILL_TYPE, ROLE_TYPE, GENRE); // 001
            if(chosenStory == null)
                chosenStory = getStory(dead, NO_TEAM, NO_KILL_TYPE, NO_ROLE_TYPE, GENRE); // 000

            if(genre == null)
                return chosenStory != null;

            if(genre != null){
                if(chosenStory == null)
                    chosenStory = getStory(dead, TEAM, KILL_TYPE, ROLE_TYPE, NO_GENRE); // 111
                if(chosenStory == null)
                    chosenStory = getStory(dead, TEAM, KILL_TYPE, NO_ROLE_TYPE, NO_GENRE); // 110
                if(chosenStory == null)
                    chosenStory = getStory(dead, TEAM, NO_KILL_TYPE, ROLE_TYPE, NO_GENRE); // 101
                if(chosenStory == null)
                    chosenStory = getStory(dead, NO_TEAM, KILL_TYPE, ROLE_TYPE, NO_GENRE); // 011
                if(chosenStory == null)
                    chosenStory = getStory(dead, TEAM, NO_KILL_TYPE, NO_ROLE_TYPE, NO_GENRE); // 100
                if(chosenStory == null)
                    chosenStory = getStory(dead, NO_TEAM, KILL_TYPE, NO_ROLE_TYPE, NO_GENRE); // 010
                if(chosenStory == null)
                    chosenStory = getStory(dead, NO_TEAM, NO_KILL_TYPE, ROLE_TYPE, NO_GENRE); // 001
                if(chosenStory == null)
                    chosenStory = getStory(dead, NO_TEAM, NO_KILL_TYPE, NO_ROLE_TYPE, NO_GENRE); // 000
            }
        }catch(SQLException e){
            Util.log(e, "SQL Database error");
            return false;
        }

        return chosenStory != null;

    }

    public String getAttackSQLStatement(String deathType) {
        String ret = "SELECT story_id, dtype FROM death_type_stories WHERE dtype ";
        if(deathType == null){
            ret += "is null;";
        }else{
            ret += "= \'" + deathType + "\' or dtype is null;";
        }
        return ret;
    }

    public String getRoleSQLStatement(String roleType) {
        if(roleType != null)
            roleType = roleType.toLowerCase();
        String ret = "SELECT story_id FROM death_role_stories WHERE role ";
        if(roleType == null){
            ret += "is null;";
        }else{
            ret += "= \'" + roleType + "\' or role is null;";
        }
        return ret;
    }

    private String getRoleID(Player dead) throws SQLException {
        String type;
        if(dead.getDeathType().isCleaned())
            type = Constants.JANITOR_DESCRIP;
        else if(dead.hasSuits()){
            type = dead.suits.get(0).getBaseRole().getDatabaseName();
        }else
            type = dead.getAbilities().getDatabaseName();
        return getValueID(type, "role_type_stories", "role_id", "role");
    }

    public static String getFactionID(String graveYardTeam, Narrator narrator) {
        ArrayList<Faction> teams = narrator.getFactions();
        Collections.sort(teams, new Comparator<Faction>() {
            @Override
            public int compare(Faction arg0, Faction arg1) {
                return arg0.getColor().compareTo(arg1.getColor());
            }

        });
        if(graveYardTeam == null)
            return "";
        int i = 1;
        for(Faction t: teams){
            if(t.getName().equals(graveYardTeam)){
                return Computer.toLetter(i);
            }
            i++;
        }
        return "";
    }

    private ArrayList<String> getDeathTypeIds(ArrayList<String[]> dTypes) throws SQLException {
        ArrayList<String> dTypeIDs = new ArrayList<>();
        for(String[] dType: dTypes){
            dTypeIDs.add(getValueID(dType[DEATH_ID], "death_type_stories", "death_id", "death"));
        }
        return dTypeIDs;
    }

    private String getValueID(String type, String table_name, String type_id, String type_value) throws SQLException {

        String qString = "SELECT " + type_id + " FROM " + table_name + " WHERE " + type_value + " = '" + type + "';";
        PreparedStatement ps = dbConn.prepareStatement(qString);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            String c = rs.getString(type_id);
            rs.close();
            ps.close();
            return c;
        }

        rs.close();
        ps.close();

        // didn't find a role
        qString = "SELECT MAX(" + type_id + ") FROM " + table_name + ";";
        ps = dbConn.prepareStatement(qString);
        rs = ps.executeQuery();

        String c = null;
        while (rs.next()){
            c = rs.getString("MAX(" + type_id + ")");
            if(c == null)
                break;
            c = ((char) (c.charAt(0) + 1)) + "";
            break;
        }

        rs.close();
        ps.close();

        if(c == null){
            qString = "INSERT INTO " + table_name + " (" + type_id + ", " + type_value + ") VALUES ('#', null);";
            ps = dbConn.prepareStatement(qString);
            ps.execute();

            ps.close();

            c = (STARTING_CHAR) + "";
        }

        if(c.equals("\\")){
            c = "\\\\";
        }

        qString = "INSERT INTO " + table_name + " (" + type_id + ", " + type_value + ") VALUES ('" + c + "', '" + type
                + "');";
        ps = dbConn.prepareStatement(qString);
        ps.execute();

        ps.close();

        return c;

    }

    private static final char STARTING_CHAR = 40;

    private Integer chooseStory(HashMap<Integer, String> stories) {
        ArrayList<Integer> storyIDs = new ArrayList<>();
        for(Integer storyID: stories.keySet()){
            if(!usedStories.contains(storyID))
                storyIDs.add(storyID);
        }
        if(storyIDs.isEmpty())
            return null;
        Collections.sort(storyIDs);
        return storyIDs.get(r.nextInt(storyIDs.size()));
    }

    public static final int DEATH_ID = 0;// constant death types are {id, human_readble_text};

    private String getStory(Player dead, boolean team_limit, boolean kill_type_limit, boolean role_limit,
            boolean genre_limit) throws SQLException {
        genre_limit = genre_limit && genre != null;
        ArrayList<String[]> deathTypes = dead.getDeathType().getList();
        ArrayList<String> deathTypeIDs = getDeathTypeIds(deathTypes);

        String roleID = getRoleID(dead);
        String teamID = getFactionID(dead.getGraveyardTeamName(), dead.narrator);

        String qString = "SELECT story, story_id FROM stories";
        if(role_limit || kill_type_limit || genre_limit || team_limit){
            qString += " WHERE ";

            if(team_limit){
                qString += "team LIKE '%" + teamID + "%'";
                if(role_limit || kill_type_limit || genre_limit)
                    qString += " AND ";
            }
            if(role_limit){
                qString += "role LIKE '%" + roleID + "%'";
                if(kill_type_limit || genre_limit)
                    qString += " AND ";
            }
            if(kill_type_limit){
                String q;
                for(String deathID: deathTypeIDs){
                    q = "death LIKE '%" + deathID + "%' AND ";
                    qString += q;
                }
                if(!genre_limit)
                    qString = qString.substring(0, qString.length() - 5);
            }
            if(genre_limit)
                qString += "GENRE = '" + genre + "'";

        }

        qString += ";";

        PreparedStatement ps = dbConn.prepareStatement(qString);
        ResultSet rs = ps.executeQuery();

        HashMap<Integer, String> stories = new HashMap<>();

        String story;
        int id;
        while (rs.next()){
            story = rs.getString("story");
            id = rs.getInt("story_id");
            stories.put(id, story);
        }

        if(!stories.isEmpty()){
            Integer chosenStory = chooseStory(stories);
            if(chosenStory != null){
                this.usedStories.add(chosenStory);
                return stories.get(chosenStory);
            }
        }

        return null;

    }

    // should not be called if chosenStory == null
    @Override
    public void populateDeath(Player dead, Announcement e) {
        String death = chosenStory;
        death = death.replaceAll("%s", dead.getName());
        if(!death.endsWith(".") && !death.endsWith("!"))
            death += ".";
        e.add(death);
        e.add("\n");

        String teamName, roleName, color;
        if(!dead.getDeathType().isCleaned()){
            e.add(" ", dead.getName(), " was a ");
            HTString publicHTSTring = null, privateHTString = null;
            if(dead.hasSuits()){
                Suit suit = dead.suits.get(0);
                color = suit.color;
                teamName = dead.narrator.getFaction(color).getName();
                roleName = suit.roleName;
                publicHTSTring = new HTString(teamName + " " + roleName, color);
            }
            teamName = dead.getFaction().getName();
            roleName = dead.getRoleName();
            color = dead.getColor();
            if(publicHTSTring != null)
                privateHTString = new HTString(teamName + " " + roleName, color);
            else
                publicHTSTring = new HTString(teamName + " " + roleName, color);

            StringChoice sc = new StringChoice(publicHTSTring);
            if(privateHTString != null){
                sc.add(Message.PRIVATE, privateHTString);
            }

            e.add(sc);

            e.add(". ");

            // death += hString.access(true) + ". ";
            String lastWill = dead.getLastWill(null);
            if(lastWill != null && !lastWill.isEmpty()){
                e.add("<br>Their last will was :<br>");
                e.add(lastWill);
            }
        }else{
            String publicHTSTring = " We could not determine what " + dead.getName() + " was.";
            StringChoice sc = new StringChoice(publicHTSTring);
            sc.add(Message.PRIVATE, " " + dead.getName() + " was a ");
            e.add(sc);

            teamName = dead.getFaction().getName();
            roleName = dead.getRoleName();
            color = dead.getColor();
            sc = new StringChoice("");
            sc.add(Message.PRIVATE, new HTString(teamName + " " + roleName, color));
        }

        chosenStory = null;
    }

    @Override
    public void cleanup() {
        dbConn.close();
    }
}
