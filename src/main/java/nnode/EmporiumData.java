package nnode;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import game.logic.Narrator;
import game.logic.Role;
import json.JSONException;
import json.JSONObject;
import models.schemas.RoleSchema;
import repositories.Connection;
import repositories.RoleRepo;
import services.RoleService;

public class EmporiumData {

    public static void main(String args[]) throws JSONException, SQLException {

        Narrator narrator = new Narrator();

        JSONObject jMember;

        System.out.println("{");
        List<Role> list = GetAllRoles(narrator);
        Role role;
        for(int i = 0; i < list.size(); i++){
            role = list.get(i);

            if(i != 0)
                System.out.println(",");
            jMember = new JSONObject();
            jMember.put(StateObject.roleName, role.getName());
            jMember.put(StateObject.description, role.getDescription());

            System.out.print(JSONObject.quote(role.getName()));
            System.out.print(":");
            System.out.print(jMember.toString());
        }
        System.out.print("}");
    }

    public static List<Role> GetAllRoles(Narrator narrator) throws SQLException {
        Connection c = new Connection();
        List<RoleSchema> roleSchemas = RoleRepo.getAllDistinct(c);
        c.close();
        List<Role> roles = new ArrayList<>();
        for(RoleSchema roleSchema: roleSchemas){
            roles.add(RoleService.createRole(narrator, roleSchema));
        }
        return roles;
    }

}
