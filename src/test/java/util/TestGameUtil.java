package util;

import java.sql.SQLException;
import java.util.Arrays;

import game.event.ArchitectChat;
import game.event.DeadChat;
import game.event.Message;
import game.logic.Role;
import game.logic.templates.BasicRoles;
import game.roles.Architect;
import game.roles.Disguiser;
import game.roles.MasonLeader;
import game.roles.SerialKiller;
import game.setups.Setup;
import integration.server.UserWrapper;
import json.JSONException;
import models.FactionRole;
import nnode.Game;
import nnode.User;
import services.ActionService;
import services.ChatService;
import services.FactionService;
import services.RoleService;

public class TestGameUtil {

    public static void buildRoom(Game game) throws JSONException {
        User architect = UserUtil.getByRoleName(game, "Architect");
        User citizen = UserUtil.getByRoleName(game, "Citizen");
        User masonLeader = UserUtil.getByRoleName(game, "Mason Leader");
        String command = Message.CommandCreator(Architect.COMMAND, masonLeader.player.getName(),
                citizen.player.getName());
        ActionService.submitAction(architect.id, command);
        game.narrator.forceEndDay();
    }

    public static Game createStartedGame() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        Game game = host.user.game;
        long factionID = game.narrator.getFaction(Setup.MAFIA_C).id;
        Role infiltratorRole = RoleService.createRole(host.id, "Infiltrator", Arrays.asList("Infiltrator"));
        FactionRole infiltrator = FactionService.createFactionRole(host.user.id, factionID, infiltratorRole.getID());

        BasicRoles.narrator = host.user.game.narrator;

        host.addSetupHidden(BasicRoles.Architect());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.Doctor());
        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(infiltrator);
        host.addSetupHidden(BasicRoles.Mason());
        host.addSetupHidden(BasicRoles.MasonLeader());
        host.addSetupHidden(BasicRoles.getMember(Setup.YAKUZA_C, new Disguiser()));
        host.addSetupHidden(BasicRoles.SerialKiller());

        while (game.phoneBook.size() < game.narrator.rolesList.size())
            UserFactory.joinPrivateGame(game.joinID);

        host.startGame(true);

        return game;
    }

    public static void disguiseByRoleName(Game game, String roleName) throws JSONException {
        User disguiser = UserUtil.getByRoleName(game, "Disguiser");
        User goon = UserUtil.getByRoleName(game, roleName);
        String command = Message.CommandCreator(Disguiser.COMMAND, goon.player.getName());
        ActionService.submitAction(disguiser.id, command);
        game.narrator.forceEndNight();
    }

    public static void recruitByRoleName(Game game, String roleName) throws JSONException {
        User recruited = UserUtil.getByRoleName(game, roleName);
        User masonLeader = UserUtil.getByRoleName(game, "Mason Leader");
        String command = Message.CommandCreator(MasonLeader.COMMAND, recruited.player.getName());
        ActionService.submitAction(masonLeader.id, command);
        game.narrator.forceEndNight();
    }

    public static void skipToDay(Game game) {
        game.narrator.forceEndNight();
    }

    public static void skipToNight(Game game) {
        game.narrator.forceEndDay();
    }

    public static void speakDeadByRoleName(Game game, String roleName) throws JSONException {
        User user = UserUtil.getByRoleName(game, roleName);
        ChatService.submit(user.id, DeadChat.KEY, FakeConstants.MESSAGE_TEXT);
    }

    public static void speakFactionByRoleName(Game game, String roleName) throws JSONException {
        User user = UserUtil.getByRoleName(game, roleName);
        ChatService.submit(user.id, user.player.getColor(), FakeConstants.MESSAGE_TEXT);
    }

    public static void speakRoom(Game game) throws JSONException {
        User citizen = UserUtil.getByRoleName(game, "Citizen");
        User masonLeader = UserUtil.getByRoleName(game, "Mason Leader");
        String chatKey = ArchitectChat.GetKey(citizen.player, masonLeader.player, game.narrator.getDayNumber());
        ChatService.submit(citizen.id, chatKey, FakeConstants.MESSAGE_TEXT);
    }

    public static void stabByRoleNameDoctor(Game game, String roleName) throws JSONException {
        User doctor = UserUtil.getByRoleName(game, roleName);
        User serialKiller = UserUtil.getByRoleName(game, "Serial Killer");
        String command = Message.CommandCreator(SerialKiller.COMMAND, doctor.player.getName());
        ActionService.submitAction(serialKiller.id, command);
        game.narrator.forceEndNight();
    }

    public static void voteOutByRoleName(Game game, String roleName) throws JSONException {
        User citizen = UserUtil.getByRoleName(game, roleName);
        int count = 0;
        for(User user: game.phoneBook.values()){
            if(user != citizen && count < (game.narrator._players.size() / 2 + 1)){
                count++;
                ActionService.submitAction(user.id, "vote " + citizen.player.getName());
            }
        }
    }
}
