package util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.support.CommandHandler;
import game.logic.support.CustomRoleAssigner;
import game.logic.support.RolePackage;
import game.logic.support.rules.SetupModifiers;
import game.logic.templates.PartialListener;
import game.roles.Ability;
import game.roles.AbilityList;
import game.roles.Executioner;
import game.roles.Hidden;
import game.roles.ItemAbility;
import integration.logic.SuperTest;
import junit.framework.TestCase;
import models.Command;
import models.FactionRole;
import models.SetupHidden;
import services.FactionService;
import services.HiddenService;
import services.RoleService;
import services.SetupHiddenService;

public class RepeatabilityTest extends TestCase {

    public void testDummy() {

    }

    public static void runRepeatabilityTest() {
        // runUniqueMessageIDTest(); //@TODO

        Narrator textNarrator = new Narrator();
        textNarrator._rules = new SetupModifiers(SuperTest.narrator._rules);
        textNarrator.setSeed(SuperTest.narrator.getSeed());

        HashMap<String, RolePackage> assignedRoles = new HashMap<>();
        HashMap<Integer, ArrayList<String>> deathDays = new HashMap<>();

        Integer key;
        for(Player p: SuperTest.narrator._players){
            assignedRoles.put(p.getID(), p.initialAssignedRole);
            if(p.isAlive())
                key = null;
            else
                key = p.getDeathDay();
            if(!deathDays.containsKey(key))
                deathDays.put(key, new ArrayList<>());
            deathDays.get(key).add(p.getID());
        }

        for(Role role: SuperTest.narrator.roles)
            RoleService.createRole(textNarrator, role);

        Faction textFaction;
        Role textRole;
        for(Faction t: SuperTest.narrator.getFactions()){
            textFaction = FactionService.createFaction(textNarrator, t.getColor(), t.getName());
            textFaction.matchProperties(t);
            for(Role role: t._roleSet.keySet()){
                textRole = LookupUtil.findRole(textNarrator, role.getName());
                FactionService.createFactionRole(textFaction, textRole);
            }
        }

        List<FactionRole> factionRoles;
        for(Hidden hidden: SuperTest.narrator.hiddens){
            factionRoles = new ArrayList<>();
            for(FactionRole factionRole: hidden.factionRolesMap.values()){
                factionRole = LookupUtil.findFactionRole(textNarrator, factionRole.role.getName(),
                        factionRole.getColor());
                factionRoles.add(factionRole);
            }
            HiddenService.createHidden(textNarrator, hidden.getName(), factionRoles);
        }

        Hidden hidden;
        for(SetupHidden setupHidden: SuperTest.narrator.getRolesList()){
            hidden = LookupUtil.findHidden(textNarrator, setupHidden.hidden.getName(),
                    setupHidden.hidden.getColors().iterator().next());
            SetupHiddenService.addSetupHidden(textNarrator, hidden, setupHidden.isExposed);
        }

        textNarrator.altRoleAssignment = new CustomRoleAssigner() {
            @Override
            public void assign() {
                HashMap<Hidden, Integer> hiddenCounts = new HashMap<>();
                for(RolePackage roleHidden: assignedRoles.values()){
                    Hidden hidden = roleHidden.assignedHidden;
                    if(!hiddenCounts.containsKey(hidden))
                        hiddenCounts.put(hidden, 0);
                    hiddenCounts.put(hidden, hiddenCounts.get(hidden) + 1);
                }
                textNarrator.generatedRoles.addAll(assignedRoles.values());
                textNarrator.generatedRoles.sort(new Comparator<RolePackage>() {

                    @Override
                    public int compare(RolePackage o1, RolePackage o2) {
                        int v1 = indexOf(o1);
                        int v2 = indexOf(o2);
                        return v1 - v2;
                    }

                    private int indexOf(RolePackage rp) {
                        SetupHidden setupHidden;
                        for(int i = 0; i < textNarrator.rolesList._setupHiddenList.size(); i++){
                            setupHidden = textNarrator.rolesList._setupHiddenList.get(i);
                            if(rp.assignedHidden == setupHidden.hidden)
                                return i;
                        }
                        return -1;
                    }

                });
                textNarrator.getRandom().reset();
                for(Player p: textNarrator._players.sortByID()){
                    assignedRoles.get(p.getID()).assign(p);
                }
                Integer count;
                for(SetupHidden setupHidden: textNarrator.rolesList){
                    count = hiddenCounts.get(setupHidden.hidden);
                    if(count == null || count == 0)
                        textNarrator.generatedRoles.add(setupHidden.hidden.generateRole(textNarrator));
                    else
                        hiddenCounts.put(setupHidden.hidden, count - 1);
                }

            }
        };

        Player text_p;
        for(Player p: SuperTest.narrator.getAllPlayers().sortByID()){
            text_p = textNarrator.addPlayer(p.getID());
            text_p.databaseID = p.databaseID;
            text_p.rolePrefer(p.initialAssignedRole.assignedRole.role.getName());
            text_p.teamPrefer(p.getInitialColor());
        }

        textNarrator.startGame();

        Player basePlayer;
        for(Player p: SuperTest.narrator._players){
            basePlayer = textNarrator.getPlayerByID(p.getID());
            assertEquals(p.getInitialColor(), basePlayer.getInitialColor());
            RepeatabilityTest.assertSameInitialAbilities(p, basePlayer);
            RepeatabilityTest.assertExecutionerTargetEquality(p, basePlayer);

            if(SuperTest.charges != null){
                RepeatabilityTest.assertChargeConsistency(p, basePlayer);
            }
        }

        Map<Long, Player> databaseMap = textNarrator._players.getDatabaseMap();
        ArrayList<Command> commands = SuperTest.narrator.getCommands();
        CommandHandler th = new CommandHandler(textNarrator);
        Command c;
        Player player;
        for(int i = 0; i < commands.size(); i++){
            c = commands.get(i);
            try{
                player = databaseMap.get(c.playerID);
                th.parseCommand(player, c.text);
            }catch(NarratorException e){
                System.err.println(textNarrator.getHappenings());
                throw e;
            }
        }

        SuperTest.assertEquals(SuperTest.narrator.phase, textNarrator.phase);
        SuperTest.assertEquals(SuperTest.narrator.getDayNumber(), textNarrator.getDayNumber());
        SuperTest.assertEquals(SuperTest.narrator.isInProgress(), textNarrator.isInProgress());
        if(!SuperTest.narrator.isInProgress() && SuperTest.narrator.isStarted()){
            String winMessage = SuperTest.narrator.getWinMessage().access(Message.PRIVATE);
            String textWinMessage = textNarrator.getWinMessage().access(Message.PRIVATE);
            assertEquals(winMessage, textWinMessage);
        }
        Player q;
        for(Player p: SuperTest.narrator._players){
            q = textNarrator.getPlayerByID(p.getID());
            SuperTest.assertEquals(p.isAlive(), q.isAlive());
            SuperTest.assertEquals(p.isWinner(), q.isWinner());
        }

        SuperTest.narrator.clearListeners();
        SuperTest.narrator.restartGame(false, true); // only reset prefs and coms
        SuperTest.narrator.altRoleAssignment = SuperTest.roleAssigner;
        SuperTest.narrator.startGame();
        SuperTest.assertTrue(SuperTest.narrator.isFirstPhase());
        assertEquals(SuperTest.narrator.getSeed(), textNarrator.getSeed());

        for(Player comparePlayer: SuperTest.narrator._players){
            SuperTest.assertTrue(comparePlayer.isAlive());
            basePlayer = textNarrator.getPlayerByID(comparePlayer.getID());
            RepeatabilityTest.assertSameInitialAbilities(basePlayer, comparePlayer);
            assertEquals(comparePlayer.getInitialColor(), basePlayer.getInitialColor());

            if(SuperTest.charges != null)
                RepeatabilityTest.assertChargeConsistency(basePlayer, comparePlayer);
        }

        SuperTest.narrator.addListener(new PartialListener() {
            @Override
            public void onDayPhaseStart(PlayerList newDead) {
                int dayNumber = SuperTest.narrator.getDayNumber() - 1;
                PlayerList currentDead = SuperTest.narrator.getDeadList(dayNumber);
                if(currentDead.isEmpty())
                    SuperTest.assertFalse(deathDays.containsKey(dayNumber));
                else{
                    SuperTest.assertTrue(deathDays.containsKey(dayNumber));
                    SuperTest.assertEquals(deathDays.get(dayNumber).size(), currentDead.size());
                }
            }
        });

        Map<Long, Player> databaseIDMap = SuperTest.narrator._players.getDatabaseMap();
        CommandHandler ch = new CommandHandler(SuperTest.narrator);
        Command command;
        for(int i = 0; i < commands.size(); i++){
            command = commands.get(i);
            try{
                ch.parseCommand(databaseIDMap.get(command.playerID), command.text);
            }catch(NarratorException e){
                e.printStackTrace();
                SuperTest.writeToFile("log1.tx", SuperTest.narrator);
                SuperTest.writeToFile("log2.tx", textNarrator);
                throw e;
            }
        }

        SuperTest.assertTrue(SuperTest.narrator.getDayNumber() == textNarrator.getDayNumber());
        if(!textNarrator.isInProgress()){
            boolean isEqual = SuperTest.narrator.getWinMessage().access(Message.PRIVATE)
                    .equals(textNarrator.getWinMessage().access(Message.PRIVATE));
            if(!isEqual){
                SuperTest.writeToFile("log1.tx", SuperTest.narrator);
                SuperTest.writeToFile("log2.tx", textNarrator);
                SuperTest.assertTrue(isEqual);
            }
        }

        for(Player p: SuperTest.narrator._players){
            q = textNarrator.getPlayerByID(p.getID());
            SuperTest.assertEquals(p.isAlive(), q.isAlive());
            SuperTest.assertEquals(p.isWinner(), q.isWinner());
        }

        SuperTest.roleAssigner = null;
    }

    public static void assertChargeConsistency(Player basePlayer, Player comparePlayer) {
        SuperTest.assertTrue(comparePlayer.narrator.isFirstPhase()); // just to make sure i'm comparing the right player

        AbilityList basePlayerAbilityList = basePlayer.initialAssignedRole.assignedRole.role._abilities;
        AbilityList comparePlayerAbilityList = comparePlayer.initialAssignedRole.assignedRole.role._abilities;
        if(basePlayerAbilityList.getNonpassive().isEmpty()){
            SuperTest.assertTrue(comparePlayerAbilityList.getNonpassive().isEmpty());
            return;
        }

        HashMap<String, Integer> chargeMap = SuperTest.charges.get(basePlayer.getID());
        for(Ability a: comparePlayer.role._abilities)
            SuperTest.assertEquals((int) chargeMap.get(a.getCommand()), a.getPerceivedCharges());
    }

    public static void assertSameInitialAbilities(Player basePlayer, Player comparePlayer) {
        AbilityList basePlayerAbilityList = basePlayer.initialAssignedRole.assignedRole.role._abilities;
        AbilityList comparePlayerAbilityList = comparePlayer.getInitialAbilities();

        if(basePlayerAbilityList.isEmpty())
            return;

        if(!basePlayer.initialAssignedRole.assignedRole.role.name
                .equals(comparePlayer.initialAssignedRole.assignedRole.role.name))
            basePlayer.toString();
        assertEquals(basePlayer.initialAssignedRole.assignedRole.role.name,
                comparePlayer.initialAssignedRole.assignedRole.role.name);

        for(int basei = 0, comparei = 0; basei < basePlayerAbilityList.size()
                && comparei < comparePlayerAbilityList.size();){
            if(basePlayerAbilityList.get(basei) instanceof ItemAbility){
                basei++;
                continue;
            }
            if(comparePlayerAbilityList.get(comparei) instanceof ItemAbility){
                comparei++;
                continue;
            }

            SuperTest.assertEquals(basePlayerAbilityList.get(basei).getClass(),
                    comparePlayerAbilityList.get(comparei).getClass());
            basei++;
            comparei++;
        }
    }

    public static void assertExecutionerTargetEquality(Player p, Player oP) {
        if(p.is(Executioner.class)){
            Executioner e1 = p.getAbility(Executioner.class);
            Executioner e2 = oP.getAbility(Executioner.class);

            assertEquals(e1.getTargets().getFirst().getID(), e2.getTargets().getFirst().getID());
        }
    }

}
