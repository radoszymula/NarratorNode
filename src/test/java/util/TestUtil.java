package util;

import java.util.ArrayList;
import java.util.Random;

import game.ai.Controller;
import game.logic.Narrator;
import game.logic.Role;
import game.logic.RolesList;
import game.logic.exceptions.NarratorException;
import game.logic.support.CustomRoleAssigner;
import game.logic.support.RolePackage;
import game.logic.support.rules.AbilityModifier;
import game.roles.Ability;
import game.roles.Hidden;
import integration.logic.SuperTest;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import services.RoleAbilityModifierService;

public class TestUtil {

    public static void modifyAbility(Class<? extends Ability> abilityClass, AbilityModifier modifier, boolean value) {
        for(Role r: SuperTest.narrator.roles){
            if(r.hasAbility(abilityClass))
                RoleAbilityModifierService.modify(r, abilityClass, modifier, value);
        }
    }

    public static void modifyAbility(Class<? extends Ability> abilityClass, AbilityModifier modifierID, int i) {
        for(Role role: SuperTest.narrator.roles)
            if(role.hasAbility(abilityClass))
                modifyAbility(role, abilityClass, modifierID, i);
    }

    public static void modifyAbility(Role role, Class<? extends Ability> abilityClass, AbilityModifier modifier,
            int value) {
        RoleAbilityModifierService.modify(role, abilityClass, modifier, value);
    }

    public static void assignRoles(Narrator narrator, Controller[] controllers, FactionRole[] roleTemplates) {
        SuperTest.roleAssigner = narrator.altRoleAssignment = new CustomRoleAssigner() {

            @Override
            public void assign() {
                ArrayList<RolePackage> hiddens = new ArrayList<>();
                RolesList rl = new RolesList();
                Hidden hidden;
                for(FactionRole obj: roleTemplates){
                    hidden = HiddenUtil.createHiddenSingle(narrator, obj);
                    rl.add(hidden);
                    hiddens.add(hidden.generateRole(narrator));
                }
                narrator.getRandom().reset();
                for(int i = 0; i < controllers.length; i++)
                    hiddens.get(i).assign(controllers[i].getPlayer(narrator));
            }

        };

    }

    public static void removeAbility(Role role, Class<? extends Ability> abilityClass) {
        SuperTest.sEditor.removeRoleAbility(role, abilityClass);
    }

    public static void addAbility(Role role, Class<? extends Ability> abilityClass) {
        SuperTest.sEditor.addRoleAbility(role, abilityClass);
    }

    public static String getRandomString(int targetStringLength) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for(int i = 0; i < targetStringLength; i++){
            int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

	public static void assertNoErrors(JSONObject response) throws JSONException {
		if(response.has("errors") && response.getJSONArray("errors").length() != 0)
			throw new NarratorException("Errors were found in JSON response.");
	}

}
