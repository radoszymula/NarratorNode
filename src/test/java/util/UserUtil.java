package util;

import nnode.Game;
import nnode.User;

public class UserUtil {

    public static User getByRoleName(Game game, String roleName) {
        for(User user: game.phoneBook.values()){
            if(user.player.getRoleName().equals(roleName))
                return user;
        }
        return null;
    }

}
