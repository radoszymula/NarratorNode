package util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.roles.Hidden;
import integration.logic.SuperTest;
import repositories.Connection;
import repositories.PlayerRepo;

public class Assertions {

    public static void assertRoleNotInSetup(Role excludedRole) {
        for(Role role: excludedRole.narrator.roles)
            SuperTest.assertNotSame(excludedRole, role);
    }

    public static void assertHiddenNotInSetup(Hidden excludedHidden) {
        for(Hidden hidden: SuperTest.narrator.hiddens){
            SuperTest.assertNotSame(excludedHidden, hidden);
        }
    }

    public static void notIn(long element, ArrayList<Long> list) {
        SuperTest.assertFalse(list.contains(element));
    }
    
    public static void assertSetupHiddensAreSame(Narrator n1, Narrator n2) {
    	List<String> list1 = new LinkedList<>();
    	List<String> list2 = new LinkedList<>();
    	
    	for(Player player: n1._players)
    		list1.add(player.getColor() + "@" + player.getRoleName());
    	
    	for(Player player: n2._players)
    		list2.add(player.getColor() + "@" + player.getRoleName());
    	
    	String key;
    	for(Player player: n2._players) {
    		key = player.getColor() + "@" + player.getRoleName();
    		if(!list1.contains(key))
    			throw new NarratorException("Couldn't find " + key);
    		list1.remove(key);
    	}
    }

    public static void assertReplayPlayerCount(long replayID) {
        try{
            Connection c = new Connection();
            SuperTest.assertEquals(3, PlayerRepo.getByReplayID(c, replayID).size());
            c.close();
        }catch(SQLException e){
            SuperTest.fail();
        }
    }

}
