package util;

import java.sql.SQLException;

import game.ai.Computer;
import integration.logic.SuperTest;
import integration.server.UserWrapper;
import json.JSONException;
import json.JSONObject;
import nnode.Permission;
import repositories.Connection;
import repositories.UserPermissionRepo;

public class UserFactory {

    public static final boolean MOD = true;
    public static final boolean PRIVATE = false;
    public static final boolean PUBLIC = true;
    public static int nameCounter = 1;

    public static UserWrapper hostPrivateCustomGame() throws JSONException, SQLException {
        return hostGame(UserFactory.PRIVATE, !UserFactory.MOD);
    }

    public static UserWrapper hostPublicGame() throws JSONException, SQLException {
        return hostGame(UserFactory.PUBLIC, !UserFactory.MOD);
    }

    public static UserWrapper HostGame(boolean public_hosting) throws JSONException, SQLException {
        return hostGame(public_hosting, !UserFactory.MOD);
    }

    public static UserWrapper hostGame(boolean public_hosting, boolean mod) throws JSONException, SQLException {
        return UserFactory.hostGame(Computer.toLetter(nameCounter++), public_hosting, mod);
    }

    public static UserWrapper hostGame(String name, boolean public_hosting, boolean mod)
            throws JSONException, SQLException {
        UserWrapper userWrapper = new UserWrapper(name);
        Connection c = new Connection();

        if(!mod){
            UserPermissionRepo.removeAllPermissions(c, userWrapper.id);
        }else{
            UserPermissionRepo.addPermission(c, userWrapper.id, Permission.GAME_EDITING);
            UserPermissionRepo.addPermission(c, userWrapper.id, Permission.BOT_ADDING);
            UserPermissionRepo.addPermission(c, userWrapper.id, Permission.KICK);
        }
        c.close();

        userWrapper.hostGame(public_hosting);
        SuperTest.narrator = userWrapper.user.game.narrator;
        return userWrapper;
    }

    public static UserWrapper joinPublicGame() throws JSONException {
        return UserFactory.joinPublicGame(Computer.toLetter(UserFactory.nameCounter++));
    }

    public static UserWrapper joinPublicGame(String name) throws JSONException {
        UserWrapper user = new UserWrapper(name);
        user.joinGame();
        return user;
    }

    public static UserWrapper joinPrivateGame(String gameID) throws JSONException {
        UserWrapper nc = new UserWrapper(Computer.toLetter(nameCounter++));
        nc.joinGame(gameID);
        return nc;
    }

    public static UserWrapper joinPrivateGame(String name, String gameID) throws JSONException {
        UserWrapper nc = new UserWrapper(name);
        nc.joinGame(gameID);
        return nc;
    }

    public static UserWrapper ObserveGame(String joinID) throws JSONException {
        UserWrapper userWrapper = new UserWrapper(Computer.toLetter(nameCounter++));

        JSONObject body = new JSONObject();
        body.put("joinID", joinID);
        body.put("userID", userWrapper.id);

        userWrapper.httpRequest("POST", "observers", body);
        userWrapper.requestWebPlayer();

        return userWrapper;
    }
}
