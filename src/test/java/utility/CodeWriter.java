package utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.TestException;
import game.logic.support.rules.SetupModifier;
import game.roles.Ability;
import models.Command;

public class CodeWriter {

    public static void main(String[] args) throws IOException {

        BufferedReader br = null;
        FileReader fr = null;

        fr = new FileReader("log1.tx");
        br = new BufferedReader(fr);
        String sCurrentLine;

        ArrayList<String> commands = new ArrayList<>();

        while ((sCurrentLine = br.readLine()) != null){
            commands.add(sCurrentLine);
        }
        if(br != null)
            br.close();
        if(fr != null)
            fr.close();

        // commands = cleanCommands(n, commands);

        FileWriter in = new FileWriter(new File("log1.tx"));
        for(String s: commands){
            in.write(s + "\n");
        }
        in.close();
    }

    ArrayList<Command> commands;
    Narrator n;
    ArrayList<NarratorErrorCondition> eConditions;

    public CodeWriter(Narrator n, ArrayList<Command> commands) {
        this.n = n;
        this.commands = commands;
        this.eConditions = new ArrayList<>();
    }

    public List<Command> filterCommands() {
        return cleanCommands(this);
    }

    public static List<Command> cleanCommands(CodeWriter cw) {
        List<Command> commands = cw.commands;
        List<Command> test;
        boolean foundError = false;
        for(int i = 0; i < commands.size(); i++){
            test = mycopy(commands);
            test.remove(i);
            if(cw.hasError()){
                commands = test;
                foundError = true;
                System.err.println(commands.size() + "\t" + i);
            }

            if(foundError && i + 1 == commands.size()){
                i = 0;
                foundError = false;
            }
        }

        return commands;

    }

    public static List<Command> mycopy(List<Command> input) {
        List<Command> output = new ArrayList<>();
        for(Command s: input)
            output.add(s);
        return output;
    }

    public void addErrorCondition(NarratorErrorCondition ec) {
        this.eConditions.add(ec);
    }

    public boolean hasError() {
        for(NarratorErrorCondition nec: eConditions){
            if(nec.hasError())
                return true;
        }
        return false;
    }

    public static boolean hasError(Narrator n, ArrayList<Command> commands) {
        n.restartGame(true, true); // auto restart game, reset prefs

//        CommandHandler ch = new CommandHandler(n);
        for(Command command: commands){
            try{
//                ch.parseCommand(command.player, command.text);
            }catch(TestException g){
                return true;
            }catch(NarratorException f){
                return false;
            }catch(Exception e){
                return false;
            }
        }
        return false;
    }

    /*
     * protected static void addRole(RandomMember r, int quantity){ for (int i = 0;
     * i < quantity; i++) n.addRole(r.copy()); }
     */

    protected static String toLetter(int i) {
        int letter = (i - 1) / 26;
        char c = (char) ((i - 1) % 26 + 65);
        if(letter == 0)
            return c + "";
        return toLetter(i / 26) + c;
    }

    public ArrayList<String> getPrefers(Player p) {
        ArrayList<String> ret = new ArrayList<>();
        StringBuilder code = new StringBuilder();

        code.append(p.getID());
        code.append(".teamPrefer(\"");
        code.append(p.getInitialColor());
        code.append("\");");
        ret.add(code.toString());
        code.setLength(0);

        code.append("alist = new AbilityList(");
        for(Ability a: p.getInitialAbilities()){
            code.append("new ");
            code.append(a.getClass().getSimpleName());
            code.append("(), ");
        }
        code.deleteCharAt(code.length() - 1);
        code.deleteCharAt(code.length() - 1);
        code.append(");");

        ret.add(code.toString());
        code.setLength(0);

        code.append(p.getID());
        code.append(".rolePrefer(alist);");

        ret.add(code.toString());
        code.setLength(0);

        return ret;
    }

    public String addRole(Player p) {
        StringBuilder code = new StringBuilder();
        code.append("member = new Member(\"");
        code.append(p.getInitialColor());
        code.append("\", \"");
        code.append(p.role.name);
        code.append("\",");
        for(Ability a: p.getInitialAbilities()){
            code.append(" new ");
            code.append(a.getClass().getSimpleName());
            code.append("(),");
        }
        code.deleteCharAt(code.length() - 1);
        code.append(");");

        code.append("\n");

        code.append("n.addRole(member);\n");
        code.append("assignedRoles.put(\"" + p.getID() + "\", member);");
        return code.toString();
    }

    public ArrayList<String> getPlayers() {
        ArrayList<String> ret = new ArrayList<>();
        ret.add("AbilityList alist;");
        ret.add("Member member;");
        ret.add("HashMap<String, RoleTemplate> assignedRoles = new HashMap<>();");
        StringBuilder code = new StringBuilder();
        PlayerList pl = n.getAllPlayers();
        pl.sortByID();
        for(Player p: pl){
            ret.add(addRole(p));
            code.append("Player ");
            code.append(p.getID());
            code.append("\t = n.addPlayer(\"");
            code.append(p.getID());
            code.append("\");");

            ret.add(code.toString());
            code.setLength(0);

            ret.addAll(getPrefers(p));
        }

        ret.add("n.altRoleAssignment = new CustomRoleAssigner(){\n\tpublic void assign() {\n\t\tn.getRandom().reset();\n\t\tfor(Player p: n._players.sortByID()){\n\t\t\tassignedRoles.get(p.getID()).getRole(n).assign(p);\n\t\t}\n\t}\n\n\tpublic RolesList getRemainingRoles(){\n\t\treturn new RolesList(assignedRoles.values());\n\t}\n};");

        return ret;
    }

    public String getSeed() {
        return "n.setSeed(Long.parseLong(\"" + n.getSeed() + "\"));\n\n";
    }

    public static String getCode(Narrator n) {
        CodeWriter cw = new CodeWriter(n);
        return cw.getCode();
    }

    public String getCode() {
        StringBuilder sb = new StringBuilder();

        for(String s: setup){
            sb.append(s);
            sb.append("\n");
        }

        for(String s: code){
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }

    ArrayList<String> code;
    ArrayList<String> setup;

    public CodeWriter(Narrator n) {
        this.n = n;
        code = new ArrayList<>();
        setup = new ArrayList<>();

        setup.add("n.removeAllTeams();");
        setup.add(getSeed());
        setup.addAll(getRules());
        setup.addAll(getTeamInfo());
        // setup.allAll(getRoleInfo());

        setup.addAll(getPlayers());

        setup.add("CommandHandler ch = new CommandHandler(n);");

        // cw.commands = cleanCommands(this);
        commands = n.getCommands();

        StringBuilder sb = new StringBuilder();
        for(Command s: commands){
//            s = s.replace("\n", "");
            sb.append("ch.parseCommand(\"");
            sb.append(s);
            sb.append("\");");
            code.add(sb.toString());
            sb.setLength(0);
        }
    }

    public ArrayList<String> getTeamInfo() {
        ArrayList<String> ret = new ArrayList<>();
//        ret.add("Team t;");
//        StringBuilder sb = new StringBuilder();
//        for(Faction t: n.getFactions()){
//            sb.append("t = n.addTeam(\"");
//            sb.append(t.getColor());
//            sb.append("\");");
//            ret.add(sb.toString());
//            sb.setLength(0);
//
//            // TODO sheriff detectables
//
//            if(t._modifierMap != null){
//                for(FactionModifier k: t._modifierMap.keySet()){
//                    sb.append("t.modifyAbility(");
//                    sb.append(k);
//                    sb.append(", ");
//                    sb.append(t._modifierMap.get(k).toString());
//                    sb.append(");");
//                    ret.add(sb.toString());
//                    sb.setLength(0);
//                }
//            }
//
//            if(t._abilities != null){
//                for(Ability a: t._abilities){
//                    sb.append("t.addAbility(new ");
//                    sb.append(a.getClass().getSimpleName().toString());
//                    sb.append("());");
//                    ret.add(sb.toString());
//                    sb.setLength(0);
//                }
//            }
//        }
//
//        for(Faction t: n.getFactions()){
//            // TODO sheriff detectables
//
//            for(String enemy: t.getEnemyColors()){
//                sb.append("t.addEnemy(\"");
//                sb.append(enemy);
//                sb.append("\");");
//                ret.add(sb.toString());
//                sb.setLength(0);
//            }
//
//        }
        return ret;
    }

    public ArrayList<String> getRules() {
        ArrayList<String> ret = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for(SetupModifier r: n._rules.rules.keySet()){
            sb.append("n.setRule(\"");
            sb.append(r);
            sb.append("\", ");
            if(n.getRule(r).isBool()){
                sb.append(n.getRule(r).getBoolVal());
            }else{
                sb.append(n.getRule(r).getIntVal());
            }
            sb.append(");");
            ret.add(sb.toString());
            sb.setLength(0);
        }
        return ret;
    }

}
