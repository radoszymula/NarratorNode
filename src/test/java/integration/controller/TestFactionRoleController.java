package integration.controller;

import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

import game.logic.Faction;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.roles.Ability;
import game.roles.Blacksmith;
import game.setups.Setup;
import integration.ControllerTestCase;
import integration.server.UserWrapper;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.schemas.AbilityModifierSchema;
import models.schemas.RoleModifierSchema;
import nnode.Game;
import repositories.FactionRoleModifierRepo;
import util.FakeConstants;
import util.TestRequest;
import util.TestUtil;

public class TestFactionRoleController extends ControllerTestCase {
    public void testFactionRoleAbilityModifierPutTrue() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        Game game = host.user.game;
        JSONObject request = new JSONObject();
        Faction benigns = host.user.game.narrator.getFaction(Setup.OUTCAST_C);
        FactionRole blacksmith = null;
        for(FactionRole factionRole: benigns._roleSet.values()){
            if(factionRole.role.hasAbility(Blacksmith.class))
                blacksmith = factionRole;
        }
        request.put("userID", host.id);
        request.put("factionRoleID", blacksmith.id);
        request.put("abilityID", blacksmith.role.getAbility(Blacksmith.class).id);
        request.put("value", false);
        request.put("name", AbilityModifier.AS_FAKE_VESTS.toString());

        JSONObject response = TestRequest.put("factionRoleAbilityModifiers", request);
        TestUtil.assertNoErrors(response);
        Ability ability = blacksmith.role.getAbility(Blacksmith.class);
        JSONObject modifierResponse = response.getJSONObject("response").getJSONObject("modifier");
        Map<Long, Map<Long, Set<AbilityModifierSchema>>> map = FactionRoleModifierRepo
                .getAbilityModifiers(game.connection, game._setup.id);
        Set<AbilityModifierSchema> modifiers = map.get(blacksmith.id)
                .get(blacksmith.role.getAbility(Blacksmith.class).id);

        assertFalse(ability.modifiers.getOrDefault(AbilityModifier.AS_FAKE_VESTS, false));
        assertFalse(modifierResponse.getBoolean("value"));
        for(AbilityModifierSchema modifierSchema: modifiers){
            if(modifierSchema.modifier != AbilityModifier.AS_FAKE_VESTS)
                continue;
            if(modifierSchema.boolValue == false)
                return;
        }
        fail("Did not find saved modifier in database");
    }

    public void testFactionRoleAbilityModifierPutFalse() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        Game game = host.user.game;
        JSONObject request = new JSONObject();
        Faction benigns = host.user.game.narrator.getFaction(Setup.OUTCAST_C);
        FactionRole blacksmith = null;
        for(FactionRole factionRole: benigns._roleSet.values()){
            if(factionRole.role.hasAbility(Blacksmith.class))
                blacksmith = factionRole;
        }
        request.put("userID", host.id);
        request.put("factionRoleID", blacksmith.id);
        request.put("abilityID", blacksmith.role.getAbility(Blacksmith.class).id);
        request.put("value", true);
        request.put("name", AbilityModifier.AS_FAKE_VESTS.toString());

        JSONObject response = TestRequest.put("factionRoleAbilityModifiers", request);
        TestUtil.assertNoErrors(response);
        Ability ability = blacksmith.role.getAbility(Blacksmith.class);
        JSONObject modifierResponse = response.getJSONObject("response").getJSONObject("modifier");
        Map<Long, Map<Long, Set<AbilityModifierSchema>>> map = FactionRoleModifierRepo
                .getAbilityModifiers(game.connection, game._setup.id);
        Set<AbilityModifierSchema> modifiers = map.get(blacksmith.id)
                .get(blacksmith.role.getAbility(Blacksmith.class).id);

        assertTrue(ability.modifiers.getOrDefault(AbilityModifier.AS_FAKE_VESTS, true));
        assertTrue(modifierResponse.getBoolean("value"));
        for(AbilityModifierSchema modifierSchema: modifiers){
            if(modifierSchema.modifier != AbilityModifier.AS_FAKE_VESTS)
                continue;
            if(modifierSchema.boolValue == true)
                return;
        }
        fail("Did not find saved modifier in database");
    }

    public void testFactionRoleModifier() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        Game game = host.user.game;
        FactionRole factionRole = host.user.game.narrator.getFactions().get(0)._roleSet.values().iterator().next();
        boolean value = !factionRole.roleModifiers.getOrDefault(RoleModifier.UNBLOCKABLE, true);
        JSONObject request = new JSONObject();
        request.put("userID", host.id);
        request.put("factionRoleID", factionRole.id);
        request.put("name", RoleModifier.UNBLOCKABLE.toString());
        request.put("value", value);

        JSONObject response = TestRequest.put("factionRoleModifiers", request);
        TestUtil.assertNoErrors(response);
        Map<Long, Set<RoleModifierSchema>> map = FactionRoleModifierRepo.getRoleModifiers(game.connection,
                game._setup.id);
        Set<RoleModifierSchema> modifiers = map.get(factionRole.id);

        for(RoleModifierSchema modifierSchema: modifiers){
            if(modifierSchema.modifier != RoleModifier.UNBLOCKABLE)
                continue;
            if(modifierSchema.boolValue == value)
                return;
        }
        fail("Did not find saved modifier in database");
    }
}
