package integration.controller;

import game.ai.Controller;
import game.logic.Player;
import game.logic.templates.BasicRoles;
import game.roles.Sheriff;
import integration.logic.SuperTest;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.json_output.ProfileJson;

public class TestProfileController extends SuperTest {
    public TestProfileController(String name) {
        super(name);
    }

    public void testSheriffNotInDayJson() throws JSONException {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Goon(), 2);

        dayStart();

        assertLackOfCommand(sheriff, Sheriff.COMMAND);
    }

    public void testSheriffInNightJson() throws JSONException {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Goon(), 2);

        nightStart();

        assertCommand(sheriff, Sheriff.COMMAND);
    }

    private static void assertLackOfCommand(Controller controller, String command) throws JSONException {
        command = command.toLowerCase();
        Player player = controller.getPlayer(narrator);
        JSONObject roleCard = ProfileJson.getRoleCard(player);
        JSONArray abilities = roleCard.getJSONArray("abilities");
        for(int i = 0; i < abilities.length(); i++)
            assertNotSame(abilities.getJSONObject(i).getString("command").toLowerCase(), command);
    }

    private static void assertCommand(Controller controller, String command) throws JSONException {
        command = command.toLowerCase();
        Player player = controller.getPlayer(narrator);
        JSONObject roleCard = ProfileJson.getRoleCard(player);
        JSONArray abilities = roleCard.getJSONArray("abilities");
        for(int i = 0; i < abilities.length(); i++)
            if(abilities.getJSONObject(i).getString("command").equalsIgnoreCase(command))
                return;
        fail(command + " not found");
    }
}
