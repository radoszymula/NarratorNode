package integration.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import game.logic.Narrator;
import game.roles.Hidden;
import game.setups.Classic;
import game.setups.GoT;
import game.setups.Setup;
import integration.ControllerTestCase;
import integration.server.InstanceTests;
import integration.server.UserWrapper;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.schemas.FactionSchema;
import repositories.Connection;
import repositories.FactionRepo;
import repositories.ReplayRepo;
import repositories.SetupRepo;
import util.FakeConstants;
import util.TestRequest;

public class TestSetupController extends ControllerTestCase {

    @Override
    public void setUp() throws Exception {
        InstanceTests.wipeTables();
    }

    public void testGetSetups() throws JSONException {
        new UserWrapper("userName");

        JSONObject response = TestRequest.get("setups", new JSONObject());

        assertEquals(0, response.getJSONArray("errors").length());
        JSONArray setups = response.getJSONArray("response");
        assertNotSame(0, setups.length());
    }

    public void testCreateSetup() throws JSONException, SQLException {
        UserWrapper user = new UserWrapper("userName");
        user.createSetup();
        Setup setup = getSetup(user.id);

        Connection connection = new Connection();
        Collection<FactionSchema> factions = FactionRepo.getBySetupID(connection, setup.id).values();
        assertNotSame(0, factions.size());
    }

    public void testSetupToggle() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.createSetup();
        long setupID = getSetup(host.id).id;
        host.hostGame(false, Classic.KEY);

        JSONObject presetRequest = new JSONObject(), response;
        presetRequest.put("setupKey", GoT.KEY);
        presetRequest.put("userID", host.id);
        presetRequest.put("joinID", host.getJoinID());

        JSONObject customRequest = new JSONObject();
        customRequest.put("setupID", setupID);
        customRequest.put("userID", host.id);
        customRequest.put("joinID", host.getJoinID());

        response = TestRequest.put("setups", customRequest);
        assertEquals(0, response.getJSONArray("errors").length());
        assertTrue(ReplayRepo.getAllIDs(host.user.game.connection).contains(host.getGameID()));
        assertNoGameOfThronesHiddens(host);

        response = TestRequest.put("setups", presetRequest);
        assertEquals(0, response.getJSONArray("errors").length());
        assertNoDefaultHiddens(host);

        response = TestRequest.put("setups", customRequest);
        assertEquals(0, response.getJSONArray("errors").length());
        assertNoGameOfThronesHiddens(host);

        response = TestRequest.put("setups", presetRequest);
        assertEquals(0, response.getJSONArray("errors").length());
        assertNoDefaultHiddens(host);

        response = TestRequest.put("setups", customRequest);
        assertEquals(0, response.getJSONArray("errors").length());
    }

    private Setup getSetup(long userID) throws SQLException {
        Connection connection = new Connection();
        List<Setup> setups = SetupRepo.getActiveByOwnerID(connection, userID);
        Setup setup = SetupRepo.getSetup(connection, setups.get(0).id, new Narrator());
        connection.close();
        return setup;
    }

    private static void assertNoGameOfThronesHiddens(UserWrapper user) {
        for(Hidden hidden: user.user.game.narrator.hiddens){
            assertFalse(hidden.getName().contains("Martell"));
        }
    }

    private static void assertNoDefaultHiddens(UserWrapper user) {
        for(Hidden hidden: user.user.game.narrator.hiddens){
            assertFalse(hidden.getName().contains("Any"));
        }
    }
}
