package integration.controller;

import java.sql.SQLException;

import game.logic.Faction;
import game.logic.Role;
import game.logic.support.rules.AbilityModifier;
import game.roles.Blacksmith;
import game.setups.Setup;
import integration.ControllerTestCase;
import integration.server.UserWrapper;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import util.FakeConstants;
import util.TestRequest;
import util.TestUtil;

public class TestRoleController extends ControllerTestCase {
    public void testGetAbilityModifierTrue() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        JSONObject request = new JSONObject();
        Faction benigns = host.user.game.narrator.getFaction(Setup.OUTCAST_C);
        Role blacksmith = null;
        for(FactionRole factionRole: benigns._roleSet.values()){
            if(factionRole.role.hasAbility(Blacksmith.class))
                blacksmith = factionRole.role;
        }
        blacksmith.addModifier(AbilityModifier.AS_FAKE_VESTS, Blacksmith.class, true);
        request.put("userID", host.id);
        request.put("roleID", blacksmith.getID());

        JSONObject response = TestRequest.get("roles", request).getJSONObject("response");
        TestUtil.assertNoErrors(response);
        JSONObject ability = response.getJSONArray("abilities").getJSONObject(0);
        JSONArray modifiers = ability.getJSONArray("modifiers");

        JSONObject modifier;
        for(int i = 0; i < modifiers.length(); i++){
            modifier = modifiers.getJSONObject(i);
            if(AbilityModifier.AS_FAKE_VESTS.toString().equals(modifier.getString("name"))){
                assertTrue(modifier.getBoolean("value"));
                return;
            }
        }
        fail("modifier not found");
    }

    public void testGetAbilityModifierFalse() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        JSONObject request = new JSONObject();
        Faction benigns = host.user.game.narrator.getFaction(Setup.OUTCAST_C);
        Role blacksmith = null;
        for(FactionRole factionRole: benigns._roleSet.values()){
            if(factionRole.role.hasAbility(Blacksmith.class))
                blacksmith = factionRole.role;
        }
        blacksmith.addModifier(AbilityModifier.AS_FAKE_VESTS, Blacksmith.class, false);
        request.put("userID", host.id);
        request.put("roleID", blacksmith.getID());

        JSONObject response = TestRequest.get("roles", request).getJSONObject("response");
        JSONObject ability = response.getJSONArray("abilities").getJSONObject(0);
        JSONArray modifiers = ability.getJSONArray("modifiers");

        JSONObject modifier;
        for(int i = 0; i < modifiers.length(); i++){
            modifier = modifiers.getJSONObject(i);
            if(AbilityModifier.AS_FAKE_VESTS.toString().equals(modifier.getString("name"))){
                assertFalse(modifier.getBoolean("value"));
                assertTrue(modifier.has("label"));
                return;
            }
        }
        fail("modifier not found");
    }
}
