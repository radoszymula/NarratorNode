package integration.controller;

import java.sql.SQLException;

import integration.ControllerTestCase;
import json.JSONException;
import json.JSONObject;
import nnode.Game;
import util.TestChatUtil;
import util.TestGameUtil;
import util.TestRequest;
import util.UserUtil;

public class TestChatController extends ControllerTestCase {

    public void testGetPreviousDeadChatMessages() throws JSONException, SQLException {
        Game game = TestGameUtil.createStartedGame();
        long deadID = UserUtil.getByRoleName(game, "Doctor").id;
        TestGameUtil.voteOutByRoleName(game, "Citizen");
        TestGameUtil.speakDeadByRoleName(game, "Citizen");
        TestGameUtil.stabByRoleNameDoctor(game, "Doctor");

        JSONObject serverResponse = sendChatRequest(deadID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    public void testGetPreviousMasonChatMessages() throws SQLException, JSONException {
        Game game = TestGameUtil.createStartedGame();
        long citizenID = UserUtil.getByRoleName(game, "Citizen").id;
        TestGameUtil.skipToNight(game);
        TestGameUtil.speakFactionByRoleName(game, "Mason");
        TestGameUtil.recruitByRoleName(game, "Citizen");

        JSONObject serverResponse = sendChatRequest(citizenID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    public void testGetPreviousArchitectChatMessages() throws SQLException, JSONException {
        Game game = TestGameUtil.createStartedGame();
        long architectID = UserUtil.getByRoleName(game, "Architect").id;
        TestGameUtil.buildRoom(game);
        TestGameUtil.speakRoom(game);
        TestGameUtil.skipToDay(game);

        JSONObject serverResponse = sendChatRequest(architectID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    public void testGetPreviousFactionMessagesFromDisguise() throws SQLException, JSONException {
        Game game = TestGameUtil.createStartedGame();
        long disguiserID = UserUtil.getByRoleName(game, "Disguiser").id;
        TestGameUtil.skipToNight(game);
        TestGameUtil.speakFactionByRoleName(game, "Goon");
        TestGameUtil.disguiseByRoleName(game, "Goon");

        JSONObject serverResponse = sendChatRequest(disguiserID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    public void testGetPreviousMasonMessagesFromDisguise() throws SQLException, JSONException {
        Game game = TestGameUtil.createStartedGame();
        long disguiserID = UserUtil.getByRoleName(game, "Disguiser").id;
        TestGameUtil.skipToNight(game);
        TestGameUtil.speakFactionByRoleName(game, "Mason");
        TestGameUtil.disguiseByRoleName(game, "Mason");

        JSONObject serverResponse = sendChatRequest(disguiserID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    public void testGetPreviousFactionMessagesFromInfiltrator() throws SQLException, JSONException {
        Game game = TestGameUtil.createStartedGame();
        long infiltratorID = UserUtil.getByRoleName(game, "Infiltrator").id;
        TestGameUtil.skipToNight(game);
        TestGameUtil.speakFactionByRoleName(game, "Mason");
        TestGameUtil.recruitByRoleName(game, "Infiltrator");

        JSONObject serverResponse = sendChatRequest(infiltratorID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    private static JSONObject sendChatRequest(long userID) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("userID", userID);
        return TestRequest.get("chats", request);
    }
}
