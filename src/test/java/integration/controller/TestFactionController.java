package integration.controller;

import java.sql.SQLException;

import game.logic.Faction;
import integration.ControllerTestCase;
import integration.server.UserWrapper;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import nnode.Game;
import repositories.FactionRepo;
import util.FakeConstants;
import util.TestRequest;

public class TestFactionController extends ControllerTestCase {

    public void testDeleteFaction() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        Faction toBeDeleted = host.user.game.narrator.getFactions().get(0);
        String color = toBeDeleted.getColor();
        JSONObject request = new JSONObject();
        request.put("factionID", toBeDeleted.id);
        request.put("userID", host.id);

        JSONObject response = TestRequest.delete("factions", request);
        Game game = host.user.game;

        for(Faction faction: game.narrator.getFactions())
            assertNotSame(faction.id, request.getLong("factionID"));
        JSONArray errors = response.getJSONArray("errors");
        assertEquals(0, errors.length());
        assertNull(FactionRepo.getIDBySetupIDAndColor(game.connection, game._setup.id, color));
        assertNull(game.narrator.getFaction(color));
    }

}
