package integration.controller;

import java.util.Iterator;

import game.logic.support.rules.SetupModifier;
import game.setups.Classic;
import integration.ControllerTestCase;
import integration.server.InstanceTests;
import integration.server.UserWrapper;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import nnode.Game;
import nnode.NodeSwitch;
import util.TestRequest;

public class TestGameController extends ControllerTestCase {

    @Override
    public void setUp() throws Exception {
        InstanceTests.wipeTables();
    }

    public void testCreateGame() throws JSONException {
        UserWrapper user = new UserWrapper("userName");
        JSONObject rules = new JSONObject();
        rules.put(SetupModifier.CHAT_ROLES.toString(), false);
        rules.put(SetupModifier.HOST_VOTING.toString(), false);
        rules.put(SetupModifier.NIGHT_LENGTH.toString(), 1);
        JSONObject request = new JSONObject();
        request.put("userID", user.id);
        request.put("setupKey", Classic.KEY);
        request.put("isPublic", false);
        request.put("rules", rules);
        request.put("hostName", "hostName");

        JSONObject response = TestRequest.post("games", request);
        JSONObject setup = response.getJSONObject("response").getJSONObject("setup");
        Game game = NodeSwitch.idToInstance.values().iterator().next();

        JSONArray errors = response.getJSONArray("errors");
        assertEquals(0, errors.length());
        assertEquals(1, game.narrator.getRule(SetupModifier.NIGHT_LENGTH).getIntVal());
        JSONObject setupModifiers = setup.getJSONObject("setupModifiers");
        JSONObject modifier;
        Iterator<?> iterator = setupModifiers.keys();
        while (iterator.hasNext()){
            modifier = setupModifiers.getJSONObject(iterator.next().toString());
            if(modifier.getString("name").equals(SetupModifier.NIGHT_LENGTH.toString())){
                assertEquals(1, modifier.getInt("value"));
                return;
            }
        }
        fail("didn't find night length in modifiers");
    }
}
