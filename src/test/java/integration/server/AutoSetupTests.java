package integration.server;

import java.sql.SQLException;

import game.ai.Computer;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.RolesList;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Citizen;
import game.roles.CultLeader;
import game.roles.Hidden;
import game.roles.Poisoner;
import game.roles.Sheriff;
import game.setups.BraveNewWorld;
import game.setups.Classic;
import game.setups.GoT;
import game.setups.Setup;
import game.setups.VanillaB;
import game.setups.VanillaE;
import integration.logic.SuperTest;
import json.JSONException;
import json.JSONObject;
import models.SetupHidden;
import models.json_output.ProfileJson;
import nnode.Game;
import nnode.NodeSwitch;
import util.LookupUtil;
import util.UserFactory;

public class AutoSetupTests extends InstanceTests {

    public AutoSetupTests(String name) {
        super(name);
    }

    public void testAutoRolesSetupGame() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.setSetup(Classic.KEY);

        Setup classic = new Classic(new Narrator());
        int classicCount = classic.getMinPlayerCount();

        assertEquals(classicCount, narrator.getRolesList().size());

        for(int i = 0; i < 11; i++)
            host.addWebPlayer();
        assertEquals(12, narrator.getRolesList().size());

        host.addSetupHidden(BasicRoles.Citizen());
        assertEquals(12, narrator.getRolesList().size());

        host.removeSetupHidden(LookupUtil.findSetupHidden(narrator, "Citizen"));
        assertEquals(12, narrator.getRolesList().size());

        // setups with minimum number of people, like GoT or max number of people like
        // classic

        // easier to explain the scenario:
        // set player limit to 5, and then change the setup to classic, which needs 7
        // minimum
        // try max example too for index out of bounds possible error
        /*
         * host.hasWarning = false;
         * 
         * host.changeRule(SetupModifierID.PLAYER_LIMIT, 5);
         * assertTrue(host.hasWarning); assertEquals(classicCount,
         * host.getRuleInt(SetupModifierID.PLAYER_LIMIT));
         * 
         * host.hasWarning = false;
         * 
         * host.changeRule(SetupModifierID.PLAYER_LIMIT, classic.getMaxPlayerCount() +
         * 1); assertTrue(host.hasWarning); assertEquals(classic.getMaxPlayerCount(),
         * host.getRuleInt(SetupModifierID.PLAYER_LIMIT));
         * assertEquals(classic.getMaxPlayerCount(), n.getRolesList().size());
         */
    }

    public void testChangeToAutoSetup() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.setSetup(Classic.KEY);

        int classicMinCount = new Classic(new Narrator()).getMinPlayerCount();

        assertEquals(classicMinCount, narrator.getRolesList().size());

        for(int i = 0; i < 7; i++)
            host.addWebPlayer();
        JSONObject playerAddEvent = null;
        for(JSONObject event: SwitchWrapper.serverStack){
            if(event.getString("event").equals("newPlayer"))
                playerAddEvent = event;
        }
        if(playerAddEvent == null)
            fail("playerAddEvent not fired");
        assertEquals(8, playerAddEvent.getJSONObject("setup").getJSONArray("setupHiddens").length());

        host.setSetup(VanillaB.KEY);
        host.startGame();
    }

    public void testCultAllies() throws JSONException {
        Setup s = new BraveNewWorld(new Narrator());
        for(int i = 0; i < 25; i++)
            s.narrator.addPlayer(Computer.toLetter(i + 1));

        s.applyRolesList();

        s.narrator.setRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        s.narrator.startGame();

        Player cLeader = s.narrator._players.filter(CultLeader.class).getFirst();
        Player delta = s.narrator._players.filter(Citizen.class).getFirst();

        cLeader.setTarget(CultLeader.MAIN_ABILITY, Sheriff.class.getSimpleName(), null, delta);
        s.narrator.forceEndNight();

        assertEquals(1, ProfileJson.getAllies(cLeader).length());
        assertEquals(1, ProfileJson.getAllies(delta).length());
    }

    public void testGoTRolesList() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.setSetup(GoT.KEY);

        Setup gotSetup = new GoT(new Narrator());

        assertEquals(gotSetup.getMinPlayerCount(), narrator.getRolesList().size());
        for(Hidden hidden: narrator.hiddens){
            if(hidden.getColors().contains(GoT.martell_c)){
                if(hidden.contains(Poisoner.class.getSimpleName())){
                    fail();
                }
            }
        }
    }

    public void testSwitchingAutoSetups() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.setSetup(GoT.KEY);

        assertEquals(new GoT(new Narrator()).getMinPlayerCount(), narrator.getRolesList().size());
    }

    public void testToAutoSetupNameCorrect() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.setSetup(Classic.KEY);

        assertEquals(Classic.KEY, host.setupName);
    }

    public void testSetupInitializations() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);

        for(String s: Setup.SETUP_LIST)
            host.setSetup(s);
    }

    public void testExposedRandom() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addBots(2);

        Hidden tk = LookupUtil.findHidden(host.user.game.narrator, Constants.TOWN_KILLING_ROLE_NAME);
        host.addSetupHidden(tk);
        host.addSetupHidden(BasicRoles.Agent());
        host.addSetupHidden(BasicRoles.Agent());

        RolesList rolesList = host.user.game.narrator.getRolesList();
        for(SetupHidden setupHidden: rolesList)
            host.setExposed(setupHidden.id);

        for(int i = 0; i < host.user.game.narrator.getRolesList().size(); i++){
            SetupHidden setupHidden = host.user.game.narrator.getRolesList().get(i);
            assertNotSame(0, setupHidden.id);
            if(setupHidden.hidden.getName().equals(tk.getName()))
                assertTrue(setupHidden.isExposed);
        }

        host.startGame();

        for(int i = 0; i < host.user.game.narrator.getRolesList().size(); i++){
            SetupHidden setupHidden = host.user.game.narrator.getRolesList().get(i);
            if(setupHidden.hidden.getName().equals(tk.getName()))
                assertTrue(setupHidden.isExposed);
        }

        SwitchWrapper.killDBConnections();

        reloadInstances();
        for(Game game: NodeSwitch.idToInstance.values()){
            assertNotNull(game._setup);
            for(SetupHidden setupHidden: game.narrator.getRolesList())
                assertTrue(setupHidden.isExposed);
        }
    }

    // test because setup e initially threw null pointer exceptions
    public void testSetupE() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.setSetup(VanillaE.KEY);
    }

    public void testSetupAllStarts() {
        Setup setup;
        Random r = new Random();
        long seed;
        for(String setupName: Setup.SETUP_LIST){
            setup = Setup.GetSetup(narrator, setupName);
            for(int i = setup.getMinPlayerCount(); i < setup.getMaxPlayerCount() + 1; i++){
                SuperTest.narrator = new Narrator();
                setup = Setup.GetSetup(narrator, setupName);
                for(int j = 0; j < i; j++){
                    narrator.addPlayer(Computer.toLetter(j + 1));
                }
                seed = r.nextLong();
                // seed = Long.parseLong("-7164896441002042899");
                // System.err.println("start Setup " + setup.name + " : " + seed);
                narrator.setSeed(seed);
                setup.applyRolesList();
                narrator.startGame();
            }
            SuperTest.narrator = new Narrator();
        }
    }
}
