package integration.server;

import java.sql.SQLException;

import game.logic.Faction;
import game.logic.templates.BasicRoles;
import game.roles.Hidden;
import game.setups.GoT;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.SetupHidden;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.StateObject;
import repositories.Connection;
import repositories.HiddenRepo;
import util.LookupUtil;
import util.UserFactory;

public class InstanceSetupTests extends InstanceTests {

    public InstanceSetupTests(String name) {
        super(name);
    }

    public void testRandomMembersChangingNameWhenTeamChanges() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        host.createTeam("Starky", GoT.lannister_c);
        host.createFactionRole("Citizen", GoT.lannister_c);
        host.createFactionRole("Sheriff", GoT.lannister_c);

        Hidden factionHidden = LookupUtil.GetFactionHidden(host.user.game.narrator, GoT.lannister_c);

        host.addSetupHidden(factionHidden);

        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.teamChangeName);
        jo.put(StateObject.color, GoT.lannister_c);
        jo.put(StateObject.teamName, "Lanny");
        host.push(jo);

        assertTrue(factionHidden.getName().contains("Lanny"));
    }

    public void testCreatedMembersInFaction() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        host.createTeam("Starky", GoT.lannister_c);
        host.createFactionRole("Citizen", GoT.lannister_c);
        host.createFactionRole("Sheriff", GoT.lannister_c);

        assertEquals(2, host.user.game.narrator.getFaction(GoT.lannister_c)._roleSet.size());
    }

    public void testEditTeamDescription() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PUBLIC, true);

        host.createTeam("Starky", GoT.lannister_c);
        host.createFactionRole("Citizen", GoT.lannister_c);
        host.createFactionRole("Operator", GoT.lannister_c);

        FactionRole customCit = LookupUtil.findFactionRole(narrator, "Citizen", GoT.lannister_c);
        assertNotNull(customCit);

        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.teamChangeDescription);
        jo.put(StateObject.color, GoT.lannister_c);
        jo.put(StateObject.teamChangeDescription, "lulz");
        host.push(jo);

        JSONObject starky = host.factions.getJSONObject(GoT.lannister_c);
        String desc = "lulz";
        assertEquals(desc, starky.getString(StateObject.description));

        host.addSetupHidden(customCit);
        host.addSetupHidden(BasicRoles.Agent());
        host.addSetupHidden(BasicRoles.BusDriver());

        SetupHidden setupHidden = narrator.rolesList.get(2);
        Connection conn = new Connection();
        Object hidden = HiddenRepo.getByID(conn, setupHidden.hidden.getID());
        conn.close();
        assertNotNull(hidden);

        host.addBots(2);
        String code = host.getJoinID();
        host.startGame();

        reloadInstances();

        Game i2 = NodeSwitch.idToInstance.get(code);
        Faction lulz = i2.narrator.getFaction(GoT.lannister_c);

        assertEquals(desc, lulz.getDescription());
    }

    // making roles available, adding the random role, and then making the roles
    // unavailable should delete those random roles
    public void testBlacklistingToOblivion() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        host.createTeam("Starky", GoT.lannister_c);
        assertNotNull(host.user.game.narrator.getFaction(GoT.lannister_c));

        host.createFactionRole("Citizen", GoT.lannister_c);
        host.createFactionRole("Sheriff", GoT.lannister_c);

        Hidden rm = null;
        for(Hidden hidden: host.user.game.narrator.hiddens)
            if(hidden.getColors().contains(GoT.lannister_c))
                rm = hidden;
        assertNotNull(rm);

        host.addSetupHidden(rm);
        host.addSetupHidden(rm);

        host.blacklist(rm.getFactionRoles().iterator().next());
        host.blacklist(rm.getFactionRoles().iterator().next());

        assertEquals(0, rm.getSize());
    }

    public void testBlacklistingRandomInfoRemoval() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        host.createTeam("Starky", GoT.lannister_c);
        host.createFactionRole("Citizen", GoT.lannister_c);
        host.createFactionRole("Sheriff", GoT.lannister_c);
        host.createFactionRole("Doctor", GoT.lannister_c);

        Hidden rm = LookupUtil.GetFactionHidden(narrator, GoT.lannister_c);
        assertNotNull(rm);

        JSONArray hiddens = host.setup.getJSONArray("hiddens");
        JSONObject starkyRandom = null;
        JSONObject o;
        String hiddenName;
        for(int i = 0; i < hiddens.length(); i++){
            o = hiddens.getJSONObject(i);
            hiddenName = o.getString("name");
            if("Starky Hidden".equals(hiddenName))
                starkyRandom = o;
        }
        assertNotNull(starkyRandom);

        host.blacklist(rm.getFactionRoles().iterator().next());
        assertEquals(2, rm.getSize());

        hiddens = host.setup.getJSONArray("hiddens");
        starkyRandom = null;
        for(int i = 0; i < hiddens.length(); i++){
            if("Starky Hidden".equals(hiddens.getJSONObject(i).get("name")))
                starkyRandom = hiddens.getJSONObject(i);
        }
        assertNotNull(starkyRandom);

        assertEquals(2, starkyRandom.getJSONArray("factionRoleIDs").length());
    }

    public void testBlackListSpaceNames() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        host.createTeam("Starky", GoT.lannister_c);
        host.createFactionRole("Drug Dealer", GoT.lannister_c);
        host.createFactionRole("Mass Murderer", GoT.lannister_c);
        host.createFactionRole("Serial Killer", GoT.lannister_c);

        Hidden rm = LookupUtil.GetFactionHidden(narrator, GoT.lannister_c);

        host.blacklist(rm.getFactionRoles().iterator().next());
    }
}
