package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import game.logic.Player;
import json.JSONException;
import json.JSONObject;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.NodeSwitch.SwitchListener;
import nnode.StateObject;
import repositories.Connection;
import repositories.SetupRepo;

public class SwitchWrapper {

    static ArrayList<JSONObject> stack;
    static List<JSONObject> serverStack;
    static ArrayList<Long> idCleanup;
    static HashMap<Long, UserWrapper> phoneBook;

    public static void init() {
        Game.runningTests = true;
        idCleanup = new ArrayList<Long>();
        phoneBook = new HashMap<>();
        stack = new ArrayList<>();
        serverStack = new ArrayList<>();

        NodeSwitch.switchListener = new SwitchListener() {
            @Override
            public void onSwitchMessage(JSONObject jo) {
                try{
                    if(jo.has("server") && jo.getBoolean("server")){
                        serverStack.add(jo);
                        return;
                    }
                    Long to_id;
                    if(jo.has(StateObject.userID)){
                        to_id = jo.getLong(StateObject.userID);
                    }else{
                        to_id = Long.valueOf(jo.getString("requestID"));
                        jo.put(StateObject.userID, to_id);
                    }
                    UserWrapper nc = phoneBook.get(to_id);
                    if(nc == null)
                        stack.add(jo);
                    else
                        phoneBook.get(to_id).receiveMessage(jo);
                }catch(JSONException e){
                    e.printStackTrace();
                }

            };
        };
    }

    public static void cleanUp() throws SQLException {
        killDBConnections();
        if(idCleanup.isEmpty())
            return;
        Connection c = new Connection();
        for(Long setupID: idCleanup){
            SetupRepo.deleteByID(c, setupID);
        }
        c.close();
    }

    public static JSONObject send(JSONObject jo) throws JSONException {
        return NodeSwitch.handleMessage(jo);
    }

    public static void killDBConnections() {
        NodeSwitch.killDBConnections();
    }

    public static ArrayList<UserWrapper> getUserWrappers(UserWrapper userWrapper) {
        ArrayList<UserWrapper> userWrappers = new ArrayList<>();

        long userID;
        for(Player player: userWrapper.user.game.phoneBook.keySet()){
            userID = userWrapper.user.game.phoneBook.get(player).id;
            userWrappers.add(SwitchWrapper.phoneBook.get(userID));
        }

        return userWrappers;
    }

}
