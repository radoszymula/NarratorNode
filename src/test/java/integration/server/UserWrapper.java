package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import game.ai.Controller;
import game.event.ChatMessage;
import game.event.DayChat;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.ChatException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PhaseException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.Modifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.TextController;
import game.roles.Ability;
import game.roles.Hidden;
import game.setups.Classic;
import game.setups.Setup;
import integration.logic.SuperTest;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.SetupHidden;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.StateObject;
import nnode.TextHandler;
import nnode.User;
import repositories.Connection;
import repositories.SetupRepo;
import repositories.UserRepo;
import util.HiddenUtil;
import util.LookupUtil;
import util.TestUtil;
import util.UserFactory;

public class UserWrapper extends TextController {

    public User user;// change to user
    String name;

    private int internalErrors = 0; // used for when json exceptions happen when creating JSON Objects

    ArrayList<Action> submittedActions;
    JSONObject submittedActionsJSON, role, setup;
    HashMap<String, PlayerList> abilities;
    HashMap<String, ArrayList<String>> chatLogs;
    HashMap<String, String> keys;
    boolean buttonVisible;
    boolean hasDayAction;
    public boolean hasWarning;
    String gameName;
    public long id;

    JSONObject rules, factions;

    public ArrayList<JSONObject> errors;

    public HashMap<String, Integer> unread;

    public String setupName;

    public UserWrapper(String name) {
        super();
        super.texter = new NodeTextController(this);
        this.name = name;

        this.id = createUserID();
        SwitchWrapper.phoneBook.put(id, this);

        abilities = new HashMap<>();
        chatLogs = new HashMap<>();
        keys = new HashMap<>();
        unread = new HashMap<>();
        submittedActions = new ArrayList<>();
        errors = new ArrayList<>();
        buttonVisible = true;
        hasWarning = false;
    }

    private static long createUserID() {
        try{
            Connection c = new Connection();
            Long userID = UserRepo.create(c, false);
            c.close();
            return userID;
        }catch(SQLException e){
            e.printStackTrace();
            throw new Error("Couldn't create user.");
        }
    }

    // TOOD rename to add user
    public UserWrapper addWebPlayer() throws JSONException {
        return UserFactory.joinPrivateGame(user.game.joinID);
    }

    // TOOD rename to add user
    public UserWrapper addWebPlayer(String name) throws JSONException {
        return UserFactory.joinPrivateGame(name, user.game.joinID);
    }

    public UserWrapper addWebPlayer(FactionRole input) throws JSONException {
    	input = LookupUtil.findFactionRole(user.game.narrator, input.getName(), input.getColor());
        Hidden hidden = null;
        for(FactionRole factionRole: input.faction._roleSet.values()){
            if(factionRole.role.getName().contentEquals(input.role.getName())){
                hidden = HiddenUtil.getHiddenSingle(user.game.narrator, factionRole);
                if(hidden == null){
                    createHidden(factionRole);
                    return addWebPlayer(input);
                }
                break;
            }
        }
        if(hidden == null)
            throw new Error("Role not found.");
        this.addSetupHidden(hidden);
        return UserFactory.joinPrivateGame(user.game.joinID).prefer(input);
    }

    private static class NodeTextController implements TextInput {

        UserWrapper nc;

        public NodeTextController(UserWrapper nc) {
            this.nc = nc;
        }

        @Override
        public void text(String p, String message, boolean sync) {
            JSONObject jo = new JSONObject();
            try{
                jo.put(StateObject.message, message);
                nc.push(jo);
            }catch(JSONException e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public String getName() {
        if(user == null)
            return name;
        if(user.player == null)
            return name;
        return user.player.getName();
    }

    public long getUserID() {
        return id;
    }

    public JSONObject httpRequest(String method, String url, JSONObject jo) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("method", method);
        request.put("url", url);
        request.put("body", jo);
        request.put("httpRequest", true);
        request.put("requestID", Long.toString(this.id));
        return SwitchWrapper.send(request);
    }

    void push(JSONObject jo) throws JSONException {
        jo.put(StateObject.userID, id);
        SwitchWrapper.send(jo);
    }

    public void requestWebPlayer() throws JSONException {
        user = NodeSwitch.phoneBook.get(id);
        this.requestGameState();
    }

    public void leaveGame() throws JSONException {
        JSONObject body = new JSONObject();
        body.put("leaverID", this.id);
        body.put("kickerID", this.id);
        httpRequest("DELETE", "players", body);
    }

    public JSONObject createSetup() throws JSONException {
        JSONObject setupRequest = new JSONObject();
        setupRequest.put("name", "mySetup" + this.id);
        JSONObject body;
        body = new JSONObject();
        body.put("userID", this.id);
        body.put("setup", setupRequest);
        return httpRequest("POST", "setups", body);
    }

    public void hostGame(boolean publicHosting) throws JSONException, SQLException {
        TestUtil.assertNoErrors(createSetup());
        Connection connection = new Connection();
        List<Setup> setups = SetupRepo.getActiveByOwnerID(connection, this.id);
        connection.close();
        Setup setup = setups.get(0);

        JSONObject rules = new JSONObject();

        JSONObject body = new JSONObject();
        body.put("rules", rules);
        body.put("userID", this.id);
        body.put("setupID", setup.id);
        body.put("isPublic", publicHosting);
        body.put("hostName", name);

        JSONObject response = httpRequest("POST", "games", body);
        TestUtil.assertNoErrors(response);

        requestWebPlayer();
        requestChats();
    }

    private void requestChats() throws JSONException {
        JSONObject body = new JSONObject();
        body.put("userID", this.id);
        JSONObject chatResponse = httpRequest("GET", "chats", body);
        JSONObject chat = chatResponse.getJSONObject("response");
        if(chat.has("message"))
            saveChats(chat);
    }

    public void hostGame(boolean publicHosting, String key) throws JSONException {
        JSONObject rules = new JSONObject();

        JSONObject body = new JSONObject();
        body.put("rules", rules);
        body.put("userID", this.id);
        body.put("setupKey", key);
        body.put("isPublic", publicHosting);
        body.put("hostName", name);

        httpRequest("POST", "games", body);
        requestWebPlayer();
    }

    public void joinGame(String joinID) throws JSONException {
        JSONObject body = new JSONObject();
        body.put("joinID", joinID);
        body.put("playerName", this.name);
        body.put("userID", this.id);
        httpRequest("POST", "players", body);
        requestWebPlayer();
        requestChats();
    }

    public void joinGame() throws JSONException {
        JSONObject body = new JSONObject();
        body.put("playerName", this.name);
        body.put("userID", this.id);
        httpRequest("POST", "players", body);
        requestWebPlayer();
        requestChats();
    }

    public JSONObject createHidden(FactionRole factionRole) throws JSONException {
        factionRole = LookupUtil.findFactionRole(user.game.narrator, factionRole.role.getName(),
                factionRole.faction.getColor());
        JSONArray factionRoleIDs = new JSONArray();
        factionRoleIDs.put(factionRole.id);
        JSONObject args = new JSONObject();
        args.put("userID", user.id);
        args.put("name", factionRole.role.getName());
        args.put("factionRoleIDs", factionRoleIDs);
        return httpRequest("POST", "hiddens", args);
    }

    public void addSetupHidden(FactionRole factionRole, int count) throws JSONException {
        Hidden hidden = LookupUtil.findHidden(user.game.narrator, factionRole.role.getName());
        if(hidden == null){
            createHidden(factionRole);
            hidden = LookupUtil.findHidden(user.game.narrator, factionRole.role.getName());
        }
        for(int i = 0; i < count; i++)
            addSetupHidden(hidden);
    }

    public JSONObject addSetupHidden(FactionRole factionRole) throws JSONException {
        Hidden hidden = LookupUtil.findHidden(user.game.narrator, factionRole.role.getName(),
                factionRole.faction.getColor());
        if(hidden == null){
            factionRole = LookupUtil.findFactionRole(user.game.narrator, factionRole.role.getName(),
                    factionRole.faction.getColor());
            JSONObject response = createHidden(factionRole);
            TestUtil.assertNoErrors(response);
            return addSetupHidden(factionRole);
        }
        return addSetupHidden(hidden.getID());
    }

    public void addSetupHidden(Hidden hidden, int count) throws JSONException {
        for(int j = 0; j < count; j++)
            addSetupHidden(hidden);
    }

    public JSONObject addSetupHidden(Hidden hidden) throws JSONException {
        hidden = LookupUtil.findHidden(user.game.narrator, hidden.getName());
        return addSetupHidden(hidden.getID());
    }

    public void setExposed(long setupHiddenID) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", user.id);
        args.put("setupHiddenID", setupHiddenID);
        args.put("isExposed", true);
        httpRequest("PUT", "setupHiddens", args);
    }

    public JSONObject addSetupHidden(long hiddenID) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", user.id);
        args.put("hiddenID", hiddenID);
        return httpRequest("POST", "setupHiddens", args);
    }

    public void removeSetupHidden(Role role) throws JSONException {
        SetupHidden setupHidden = LookupUtil.findSetupHidden(user.game.narrator, role.getName());
        removeSetupHidden(setupHidden);
    }

    public void removeSetupHidden(SetupHidden setupHidden) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", user.id);
        args.put("setupHiddenID", setupHidden.id);
        httpRequest("DELETE", "setupHiddens", args);
    }

    public void addAbility(Role m, Ability a) throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.addAbility);
        jo.put(StateObject.roleName, m.getName());
        jo.put(StateObject.addAbility, a.getClass().getSimpleName());

        push(jo);
    }

    public void modifyFactionRoleAbility(FactionRole factionRole, Class<? extends Ability> abilityClass, AbilityModifier modifier, Object value)
            throws JSONException {
        factionRole = LookupUtil.findFactionRole(user.game.narrator, factionRole.getName(), factionRole.getColor());
        JSONObject args = new JSONObject();
        args.put("userID", user.id);
        args.put("factionRoleID", factionRole.id);
        args.put("abilityID", factionRole.role.getAbility(abilityClass).id);
        args.put("value", value);
        args.put("name", modifier.toString());
        JSONObject response = httpRequest("PUT", "factionRoleAbilityModifiers", args);
        TestUtil.assertNoErrors(response);
    }

    public void modifyFactionRole(FactionRole factionRole, RoleModifier modifier, Object val) throws JSONException {
    	factionRole = LookupUtil.findFactionRole(user.game.narrator, factionRole.getName(), factionRole.getColor());
        JSONObject args = new JSONObject();
        args.put("userID", user.id);
        args.put("factionRoleID", factionRole.id);
        args.put("value", val);
        args.put("rules", rules);
        args.put("name", modifier.toString());
        JSONObject response = httpRequest("PUT", "factionRoleModifiers", args);
        TestUtil.assertNoErrors(response);
    }

    public String getJoinID() { // TODO rename to JoinID
        return user.game.joinID;
    }

    public long getGameID() { // TODO rename to JoinID
        return user.game.getID();
    }

    public int getWebSize() {
        return user.game.phoneBook.size();
    }

    public void resetGame() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.resetGame);

        push(jo);
    }

    public void setHiddenRandoms(boolean hidden) throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.hasModerator);
        jo.put(StateObject.hasModerator, hidden);

        push(jo);
    }

    public long startGame() throws JSONException {
        return startGame(Narrator.NIGHT_START, false);
    }

    public long startGame(boolean dayStart) throws JSONException {
        return startGame(dayStart, false);
    }

    public boolean autoStartCheck = true;

    public long startGame(boolean dayStart, boolean host_voting) throws JSONException {
        updateRule(SetupModifier.DAY_START, dayStart);
        Game game = user.game;
        if(autoStartCheck)
            SuperTest.assertEquals(game.narrator.getRolesList().size(), game.narrator._players.size());

        updateRule(SetupModifier.HOST_VOTING, host_voting);

        JSONObject args = new JSONObject();
        args.put("userID", user.id);
        args.put("start", true);
        JSONObject response = httpRequest("POST", "games", args);
        TestUtil.assertNoErrors(response);
        
        user.game.seed = Long.parseLong("0");
        if(game.narrator.isStarted()){
            long story_id = game.getID();

            if(game.narrator.getPlayerCount() >= Game.MIN_REGULAR_PLAYERS){
                int observers = game.observers.size();
                UserFactory.ObserveGame(game.joinID);
                SuperTest.assertEquals(observers + 1, game.observers.size());
            }

            return story_id;
        }else if(autoStartCheck)
            throw new PhaseException("Game wasn't started.");
        return -1;
    }

    public void quickStart(boolean start) throws JSONException {
        setSetup(Classic.KEY);

        startGame(start);
    }

    public Player getPlayer() {
        return user.player;
    }

    public void repick() throws JSONException {
        repick("");
    }

    public boolean isHost() {
        return this.user.id == this.user.game.hostID;
    }

    public void repick(UserWrapper user) throws JSONException {
        repick(user.getName());
    }

    public void repick(String name) throws JSONException {
        JSONArray activeUserIDs = new JSONArray();
        for(User gameUser: user.game.phoneBook.values())
            activeUserIDs.put(gameUser.id);
        JSONObject args = new JSONObject();
        args.put("activeUserIDs", activeUserIDs);
        args.put("repickTarget", name);
        args.put("userID", user.id);
        httpRequest("POST", "moderators", args);
    }

    public void kick(UserWrapper nc) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("playerID", nc.user.id);
        args.put("kickerID", this.id);
        httpRequest("DELETE", "players", args);
    }

    public void disconnect() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("server", true);
        jo.put(StateObject.message, "disconnect");
        push(jo);
    }

    public ArrayList<Controller> addBots(int i) throws JSONException {
        PlayerList bots = getBots();
        JSONObject args = new JSONObject();
        args.put("botCount", i);
        args.put("userID", this.id);
        
        JSONObject response = httpRequest("POST", "players", args);
        TestUtil.assertNoErrors(response);

        ArrayList<Controller> botControllers = new ArrayList<>();
        for(Player p: getBots().remove(bots).toArrayList())
            botControllers.add(p);
        return botControllers;
    }

    public PlayerList getBots() {
        PlayerList bots = new PlayerList();

        for(Player p: user.game.narrator._players){
            if(p.isComputer()){
                bots.add(p);
            }
        }

        return bots;
    }

    public UserWrapper prefer(FactionRole m) throws JSONException {
        prefer(m.role.getName());
        preferTeam(m.faction.getColor());
        return this;
    }

    public void preferTeam(String teamColor) throws JSONException {
        prefer(user.game.narrator.getFaction(teamColor).getName());
    }

    public void prefer(String pref) throws JSONException {
        JSONObject jo = new JSONObject();
        String input = "say " + Constants.DAY_CHAT + " -prefer ";
        input += pref;
        jo.put(StateObject.message, input);
        push(jo);
    }

    public HashMap<String, PlayerList> getActions() {
        return abilities;
    }

    public void receiveMessage(JSONObject jo) throws JSONException {
        if(jo.has(StateObject.guiUpdate)){
            if(user == null)
                return;
            if(jo.has(StateObject.rules)){
                saveRules(jo.getJSONObject(StateObject.rules));
            }
            if(jo.has(StateObject.factions)){
                saveFactions(jo.getJSONObject(StateObject.factions));
            }
            if(jo.has(StateObject.playerLists)){
                savePlayerLists(jo.getJSONObject(StateObject.playerLists));
            }
            if(jo.has(StateObject.actions)){
                saveActions(jo.getJSONObject(StateObject.actions));
            }
            if(jo.has(StateObject.showButton))
                buttonVisible = jo.getBoolean(StateObject.showButton);
            if(jo.has(StateObject.getDayActions))
                hasDayAction = jo.getJSONArray(StateObject.getDayActions).length() != 0;
            if(jo.has(StateObject.playerName))
                gameName = jo.getString(StateObject.playerName);
            if(jo.has(StateObject.setupName))
                this.setupName = jo.getString(StateObject.setupName);
            if(jo.has("setup"))
                this.setup = jo.getJSONObject("setup");
            if(jo.has(StateObject.roleInfo))
                role = jo.getJSONObject(StateObject.roleInfo);
        }else if(jo.has(StateObject.nodeError)){
            errors.add(jo);
        }else if(jo.has(StateObject.warning)){
            this.hasWarning = true;
        }else if(jo.has(StateObject.unreadUpdate)){
            String update = jo.getString(StateObject.unreadUpdate);
            unread.put(update, jo.getInt(StateObject.count));
        }else if(jo.has(StateObject.speechContent)){

        }else if(jo.has("requestID")){
            if(jo.getJSONArray("errors").length() != 0)
                this.hasWarning = true;
        }else{// should handle other handleobject possibilities
            saveChats(jo);
        }

    }

    boolean getRuleBool(SetupModifier ruleID) throws JSONException {
        return rules.getJSONObject(ruleID.toString()).getBoolean(StateObject.val);
    }

    int getRuleInt(SetupModifier ruleID) throws JSONException {
        return rules.getJSONObject(ruleID.toString()).getInt(StateObject.val);
    }

    private void saveRules(JSONObject jo) {
        this.rules = jo;
    }

    private void saveFactions(JSONObject jo) {
        this.factions = jo;
    }

    void updateRule(SetupModifier ruleID, boolean b) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", user.id);
        args.put("id", ruleID.toString());
        args.put("value", b);

        httpRequest("PUT", "setupModifiers", args);
    }

    void updateRule(Modifier ruleID, int i) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", user.id);
        args.put("id", ruleID.toString());
        args.put("value", i);

        httpRequest("PUT", "setupModifiers", args);
    }

    void setSetup(String name) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", user.id);
        args.put("setupKey", name);
        httpRequest("PUT", "setups", args);
    }

    public void createTeam(String teamName, String teamColor) throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.createTeam);
        jo.put(StateObject.teamName, teamName);
        jo.put(StateObject.color, teamColor);
        push(jo);
    }

    public void createFactionRole(String roleName, String teamColor) throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.createFactionRole);
        jo.put(StateObject.color, teamColor);
        jo.put(StateObject.roleName, roleName);

        push(jo);
    }

    public void blacklist(FactionRole factionRole) throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.removeTeamRole);
        jo.put(StateObject.color, factionRole.getColor());
        jo.put(StateObject.roleName, factionRole.role.getName());

        push(jo);
    }

    private void saveActions(JSONObject jo) throws JSONException {
        submittedActions.clear();
        submittedActionsJSON = jo;

        JSONArray jActions = jo.getJSONArray(StateObject.actionList), jPlayerNames;
        JSONObject jAction;
        Action a;
        Player p, me = getPlayer();
        String command, option1, option2, option3;
        PlayerList pl;
        for(int i = 0; i < jActions.length(); i++){
            jAction = jActions.getJSONObject(i);

            command = jAction.getString(StateObject.command);
            jPlayerNames = jAction.getJSONArray(StateObject.playerNames);
            pl = new PlayerList();
            for(int playerCounter = 0; playerCounter < jPlayerNames.length(); playerCounter++){
                p = me.narrator.getPlayerByName(jPlayerNames.getString(playerCounter));
                pl.add(p);
            }
            option1 = jAction.has(StateObject.option) ? jAction.getString(StateObject.option) : null;
            option2 = jAction.has(StateObject.option2) ? jAction.getString(StateObject.option2) : null;
            option3 = jAction.has(StateObject.option3) ? jAction.getString(StateObject.option3) : null;
            a = new Action(me, pl, Ability.ParseAbility(command), option1, option2, option3);
            submittedActions.add(a);
        }
    }

    private void savePlayerLists(JSONObject jo) throws JSONException {
        JSONArray types = jo.getJSONArray(StateObject.type);
        abilities.clear();
        PlayerList pl = new PlayerList();
        String type;
        JSONArray jPlayers;
        JSONObject jObject;
        String name;
        Player p;
        for(int i = 0; i < types.length(); i++){
            pl = new PlayerList();
            type = types.getString(i);
            if(!jo.getJSONObject(type).has("players"))
                continue;
            jPlayers = jo.getJSONObject(type).getJSONArray("players");

            for(int j = 0; j < jPlayers.length() && user.game != null; j++){
                jObject = jPlayers.getJSONObject(j);
                name = jObject.getString(StateObject.playerName);
                p = user.game.narrator.getPlayerByName(name);
                pl.add(p);
            }

            abilities.put(type, pl);
        }
    }

    private int getDayNumber(String input) {
        Matcher matcher = Pattern.compile("\\d+").matcher(input);
        matcher.find();
        return Integer.valueOf(matcher.group());
    }

    private void saveChats(JSONObject jo) throws JSONException {
        if(jo.has(StateObject.chatReset)){
            chatLogs.clear();
            keys.clear();
            JSONArray chats = jo.getJSONArray(StateObject.chatReset);
            JSONObject chatJSON;
            String chatName;
            for(int i = 0; i < chats.length(); i++){
                chatJSON = chats.getJSONObject(i);
                chatName = chatJSON.getString(StateObject.chatName);
                if(chatJSON.has(StateObject.isDay) && !chatName.equals(DayChat.PREGAME)){
                    for(int j = getDayNumber(chatName) - 1; j >= 1; j--){
                        this.chatLogs.put("Day " + j, new ArrayList<>());
                    }
                }
                this.chatLogs.put(chatName, new ArrayList<>());
                if(chatJSON.has(StateObject.chatKey)){
                    this.keys.put(chatName, chatJSON.getString(StateObject.chatKey));
                }
            }
        }
        JSONObject message;
        JSONArray chatNames;
        String chatLabel;
        if(jo.get(StateObject.message) instanceof JSONArray){
            JSONArray messages = jo.getJSONArray(StateObject.message);
            for(int i = 0; i < messages.length(); i++){
                message = messages.getJSONObject(i);
                chatNames = message.getJSONArray("chat");
                for(int k = 0; k < chatNames.length(); k++){
                    chatLabel = chatNames.getString(k);
                    pushToSingleChat(chatLabel, message);
                }
            }
        }else{
            jo.put("text", jo.get("message"));
            jo.remove("message");
            for(String key: this.chatLogs.keySet()){
                pushToSingleChat(key, jo);
            }
        }
    }

    private void pushToSingleChat(String chatLabel, JSONObject jo) throws JSONException {
        ArrayList<String> messages = this.chatLogs.get(chatLabel);
        if(messages == null){
            System.err.println(this.user.player.getChats());
            System.err.println(this.chatLogs);
            this.user.player.getEvents();
            throw new NullPointerException(chatLabel);
        }
        String text = jo.getString("text");
        if(jo.has(StateObject.messageType)
                && jo.getString(StateObject.messageType).equals(ChatMessage.class.getSimpleName()))
            text = jo.getString(StateObject.sender) + text;
        if(text.contains(TextHandler.END_NIGHT_CONF) || text.contains(TextHandler.CANCEL_END_NIGHT_CONF))
            throw new ChatException(text);
        messages.add(text);
    }

    @Override
    public void setNightTarget(String command, String opt1, String opt2, String opt3, Controller... ncs) {
        JSONObject jo = new JSONObject();
        try{
            jo.put(StateObject.command, command);

            JSONArray targets = new JSONArray();
            for(Controller nc: ncs)
                targets.put(nc.getPlayer(getPlayer().narrator).getName());

            jo.put(StateObject.targets, targets);

            jo.put(StateObject.option, opt1);
            jo.put(StateObject.option2, opt2);
            jo.put(StateObject.option3, opt3);
            jo.put("userID", this.getUserID());

            httpRequest("PUT", "actions", jo);
        }catch(JSONException e){
            throw new NarratorException();
        }
    }

    public void endPhase() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, CommandHandler.END_PHASE);
        push(jo);
    }

    public void doDayAction() {
        super.doDayAction(getPlayer().getAbilities().getDayAbilities().get(0).getCommand(), null);
    }

    public void doDayAction(Controller wp) {
        super.doDayAction(getPlayer().getAbilities().getDayAbilities().get(0).getCommand(), null, wp);
    }

    public void doDayAction(Controller... wps) {
        super.doDayAction(getPlayer().getAbilities().getDayAbilities().get(0).getCommand(), null, wps);
    }

    @Override
    public void cancelAction(int actionIndex) {
        try{
            JSONObject args = new JSONObject();
            args.put("command", "");
            args.put("actionIndex", actionIndex);
            args.put("userID", this.user.id);
            httpRequest("DELETE", "actions", args);
        }catch(JSONException e){
            throw new NarratorException();
        }
    }

    public void requestChat() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.requestChat);
        push(jo);
    }

    public void requestGameState() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.requestGameState);
        push(jo);
    }

    public void requestUnreads() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.requestUnreads);
        push(jo);
    }

    public boolean isCaughtUp() {
        for(Integer i: unread.values()){
            if(i != 0)
                return false;
        }
        return true;
    }

    public void setRead(String name) throws JSONException {
        unread.put(name, 0);

        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.setReadChat);
        jo.put(StateObject.setReadChat, name);

        push(jo);
    }

    public void changeName(String name) throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.nameChange);
        jo.put(StateObject.nameChange, name);
        push(jo);
    }

    @Override
    public void setLastWill(String s) {
        JSONObject jo = new JSONObject();
        try{
            jo.put(StateObject.message, StateObject.lastWill);
            jo.put(StateObject.willText, s);
            push(jo);
        }catch(JSONException e){
            e.printStackTrace();
            this.internalErrors++;
        }
    }

    public void lobbyNameChange(String string) throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.nameChange);
        jo.put(StateObject.action, true);
        jo.put(StateObject.playerName, string);
        push(jo);
    }

    public void submitAction(JSONObject action) throws JSONException {
        push(action);
    }

    @Override
    public String toString() {
        if(getPlayer() == null)
            return super.toString();
        return getPlayer().toString();
    }

    // mirrored in index.js -> submitHostVote
    public void submitHostSkipVote(UserWrapper... players) throws JSONException {
        JSONArray names = new JSONArray();
        for(UserWrapper wpw: players)
            names.put(wpw.getName());

        JSONObject jo = new JSONObject();
        jo.put(StateObject.command, "vote");
        jo.put("skip day", true);
        jo.put("unvote", false);
        jo.put(StateObject.targets, names);
        jo.put("userID", this.getUserID());

        httpRequest("PUT", "actions", jo);
    }

    @Override
    public void log(String string) {
        // TODO Auto-generated method stub
    }

    @Override
    public void setNightTarget() {
        this.setNightTarget(getPlayer().getAbilities().getNightAbilities(getPlayer()).getFirst().getCommand());
    }

    @Override
    public boolean cleanup() {
        return false;
    }

    @Override
    public void clearPreferences() {
        getPlayer().clearPreferences();
    }

    @Override
    public Controller setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean hasError() {
        return this.internalErrors != 0;
    }

    @Override
    public boolean isTargeting(String ability, Controller... targets) {
        // TODO use webplayer storage
        return getPlayer().isTargeting(ability, targets);
    }

    @Override
    public Player getPlayer(Narrator narrator) {
        return getPlayer().getPlayer(narrator);
    }

}
