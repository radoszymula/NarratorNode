package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import game.ai.Brain;
import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Ghost;
import game.roles.Goon;
import json.JSONException;
import json.JSONObject;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.Permission;
import repositories.Connection;
import repositories.PlayerRepo;
import repositories.ReplayRepo;
import repositories.UserPermissionRepo;
import services.GameService;
import util.UserFactory;

public class PlayerLeaveTests extends InstanceTests {

    public PlayerLeaveTests(String name) {
        super(name);
    }

    UserWrapper host;
    Game game;
    List<UserWrapper> users;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        host = UserFactory.hostPublicGame();
        game = host.user.game;
    }

    @Override
    public void tearDown() throws Exception {
        SwitchWrapper.stack.clear();
        super.tearDown();
    }

    public void testNotStartedGameNonHostLeaving() throws JSONException, SQLException {
        UserWrapper c1 = host.addWebPlayer();

        c1.leaveGame();

        assertActiveDBConnection();
        assertGameIDInDatabase();
        assertGameIsLookupable();
        assertGameCanBeJoined();
        assertNotInPhonebook(c1);
        assertNotInGame(c1);
        assertInPlayerTable(host);
        assertNotInPlayerTable(c1);
        assertHost(host);
        assertPlayerLeaveEventCalled(c1);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventNotCalled();
    }

    public void testNotStartedGameNonSoloHostLeaving() throws JSONException, SQLException {
        UserWrapper c1 = host.addWebPlayer();

        host.leaveGame();

        assertActiveDBConnection();
        assertGameIDInDatabase();
        assertGameIsLookupable();
        assertGameCanBeJoined();
        assertNotInPhonebook(host);
        assertNotInGame(host);
        assertInPlayerTable(c1);
        assertNotInPlayerTable(host);
        assertHost(c1);
        assertPlayerLeaveEventCalled(host);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventNotCalled();
    }

    public void testNotStartedGameSoloHostLeaving() throws JSONException, SQLException {
        // NO GIVEN

        host.leaveGame();

        assertInactiveDBConnection();
        assertGameIDNotInDatabase();
        assertGameIsNotLookupable();
        assertGameCannotBeJoined();
        assertNotInGame(host);
        assertNotInPlayerTable(host);
        assertPlayerLeaveEventNotCalled(host);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventCalled();
    }

    public void testDeleteGameInLobby() throws SQLException, JSONException {

        GameService.delete(game);

        assertInactiveDBConnection();
        assertGameIDNotInDatabase();
        assertGameIsNotLookupable();
        assertGameCannotBeJoined();
        assertNotInPhonebook(host);
        assertNotInGame(host);
        assertPlayerLeaveEventNotCalled(host);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventCalled();
    }

    public void testDeleteGameInGame() throws JSONException, SQLException {
        startGame();

        GameService.delete(game);

        assertInactiveDBConnection();
        assertGameIDNotInDatabase();
        assertGameIsNotLookupable();
        assertGameCannotBeJoined();
        assertNotInPhonebook(host);
        for(UserWrapper user: users)
            assertNotInPhonebook(user);
        assertNotInGame(host);
        for(UserWrapper user: users)
            assertNotInGame(user);
        assertPlayerLeaveEventNotCalled(host);
        for(UserWrapper user: users)
            assertPlayerLeaveEventNotCalled(user);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventCalled();
    }

    public void testNoLeavingGame() throws JSONException, SQLException {
        startGame();
        UserWrapper leaver = users.get(0);

        leaver.leaveGame();

        assertActiveDBConnection();
        assertGameIDInDatabase();
        assertGameIsLookupable();
        assertGameCannotBeJoined();
        assertInPhonebook(leaver);
        assertInGame(leaver);
        assertInPlayerTable(leaver);
        assertPlayerNotExited(leaver);
        assertPlayerLeaveEventNotCalled(leaver);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventNotCalled();
    }

    public void testGhostNoLeavingGame() throws JSONException, SQLException {
        startGame();
        UserWrapper leaver = getGhost();
        for(UserWrapper user: users){
            if(game.narrator.isDay())
                user.vote(leaver);
        }

        leaver.leaveGame();

        assertActiveDBConnection();
        assertGameIDInDatabase();
        assertGameIsLookupable();
        assertGameCannotBeJoined();
        assertInPhonebook(leaver);
        assertInGame(leaver);
        assertInPlayerTable(leaver);
        assertPlayerNotExited(leaver);
        assertPlayerLeaveEventNotCalled(leaver);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventNotCalled();
    }

    public void testLeavingIfNotParticipating() throws JSONException, SQLException {
        startGame();
        UserWrapper leaver = getGoon();
        for(UserWrapper user: users){
            if(game.narrator.isDay())
                user.vote(leaver);
        }

        leaver.leaveGame();

        assertActiveDBConnection();
        assertGameIDInDatabase();
        assertGameIsLookupable();
        assertGameCannotBeJoined();
        assertNotInPhonebook(leaver);
        assertNotInGame(leaver);
        assertInPlayerTable(leaver);
        assertPlayerExited(leaver);
        assertPlayerLeaveEventCalled(leaver);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventNotCalled();
    }

    public void testLeavingIfOnlyBotsRemain() throws JSONException, SQLException {
        host.addSetupHidden(BasicRoles.SerialKiller());
        host.addSetupHidden(BasicRoles.Citizen(), 2);
        host.addSetupHidden(BasicRoles.Goon(), 4);
        host.updateRule(SetupModifier.SELF_VOTE, true);
        UserPermissionRepo.addPermission(game.connection, host.id, Permission.BOT_ADDING);
        host.addBots(6);
        host.startGame(Narrator.DAY_START);
        host.vote(host);
        host.endPhase();

        host.leaveGame();

        assertTrue(game.narrator.isFinished());
        assertInactiveDBConnection();
        assertGameIDInDatabase();
        assertPlayerExited(host);
        assertGameIsNotLookupable();
        assertGameCannotBeJoined();
        assertNotInGame(host);
        assertInPlayerTable(host);
        assertLobbyCloseEventCalled();
        assertGameEndEventCalled();
    }

    public void testDeleteGameOnFinishedGame() throws JSONException, SQLException {
        endGame();

        GameService.delete(game);

        assertGameEndEventCalled();
        assertPlayerExited(host);
        for(UserWrapper user: users)
            assertPlayerExited(user);
        assertInactiveDBConnection();
        assertGameIDInDatabase();
        assertGameIsNotLookupable();
        assertGameCannotBeJoined();
        assertNotInGame(host);
        assertInPlayerTable(host);
        for(UserWrapper user: users)
            assertInPlayerTable(user);
        for(UserWrapper user: users)
            assertPlayerExited(user);
        assertLobbyCloseEventCalled();
        assertPlayerLeaveEventNotCalled(host);
        for(UserWrapper user: users)
            assertPlayerLeaveEventNotCalled(user);
    }

    public void testOnEndGame() throws JSONException, SQLException {
        // NO GIVEN

        endGame();

        assertGameEndEventCalled();
        assertLobbyCloseEventNotCalled();
        assertPlayerLeaveEventNotCalled(host);
        for(UserWrapper user: users)
            assertPlayerLeaveEventNotCalled(user);
        assertPlayerExited(host);
        for(UserWrapper user: users)
            assertPlayerExited(user);
        assertInactiveDBConnection();
        assertGameIDInDatabase();
        assertGameIsLookupable();
        assertGameCannotBeJoined();
        assertInGame(host);
        for(UserWrapper user: users)
            assertInGame(user);
        assertInPlayerTable(host);
        for(UserWrapper user: users)
            assertInPlayerTable(user);

        host.leaveGame();

        assertLobbyCloseEventNotCalled();
        assertPlayerLeaveEventCalled(host);
        assertPlayerExited(host);
        for(UserWrapper user: users)
            assertPlayerExited(user);
        assertInactiveDBConnection();
        assertGameIDInDatabase();
        assertGameIsLookupable();
        assertGameCannotBeJoined();
        assertNotInGame(host);
        for(UserWrapper user: users)
            assertInGame(user);
        assertInPlayerTable(host);
        for(UserWrapper user: users)
            assertInPlayerTable(user);

        for(int i = 1; i < users.size(); i++)
            users.get(i).leaveGame();

        assertPlayerExited(host);
        for(UserWrapper user: users)
            assertPlayerExited(user);
        assertInactiveDBConnection();
        assertGameIDInDatabase();
        assertGameIsLookupable();
        assertGameCannotBeJoined();
        assertNotInGame(host);
        for(int i = 1; i < users.size(); i++)
            assertNotInGame(users.get(i));
        assertInPlayerTable(host);
        for(UserWrapper user: users)
            assertInPlayerTable(user);

        users.get(0).leaveGame();
        assertPlayerLeaveEventNotCalled(users.get(0));
        assertLobbyCloseEventCalled();
        assertInactiveDBConnection();
        assertGameIDInDatabase();
        assertGameIsNotLookupable();
        assertGameCannotBeJoined();
        assertNotInGame(users.get(0));
        assertTrue(game.phoneBook.isEmpty());
    }

    private void startGame() throws JSONException {
        users = new ArrayList<>();
        for(int i = 0; i < 6; i++)
            users.add(host.addWebPlayer());
        host.addSetupHidden(BasicRoles.Ghost());
        host.addSetupHidden(BasicRoles.Vigilante());
        host.addSetupHidden(BasicRoles.SerialKiller());
        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.Citizen());
        host.startGame(Narrator.DAY_START);
    }

    private void endGame() throws JSONException {
        startGame();
        Brain.EndGame(game.narrator, 0);
    }

    private UserWrapper getGhost() {
        if(host.user.player.is(Ghost.class))
            return host;
        for(UserWrapper user: users)
            if(user.user.player.is(Ghost.class))
                return user;
        throw new NullPointerException();
    }

    private UserWrapper getGoon() {
        if(host.user.player.is(Ghost.class))
            return host;
        for(UserWrapper user: users)
            if(user.user.player.is(Goon.class))
                return user;
        throw new NullPointerException();
    }

    private void assertActiveDBConnection() {
        assertNotNull(game.connection);
    }

    private void assertInactiveDBConnection() {
        assertNull(game.connection);
    }

    private void assertGameIDInDatabase() throws SQLException {
        Connection connection = new Connection();
        List<Long> ids = ReplayRepo.getAllIDs(connection);
        connection.close();
        assertTrue(ids.contains(game.getID()));
    }

    private void assertGameIDNotInDatabase() throws SQLException {
        Connection connection = new Connection();
        List<Long> ids = ReplayRepo.getAllIDs(connection);
        connection.close();
        assertFalse(ids.contains(game.getID()));
    }

    private void assertGameCanBeJoined() {
        assertTrue(NodeSwitch.instances.contains(game));
    }

    private void assertGameCannotBeJoined() {
        assertFalse(NodeSwitch.instances.contains(game));
    }

    private void assertGameIsLookupable() {
        assertTrue(NodeSwitch.idToInstance.containsKey(game.joinID));
    }

    private void assertGameIsNotLookupable() {
        assertFalse(NodeSwitch.idToInstance.containsKey(game.joinID));
        assertFalse(NodeSwitch.instances.contains(game));
    }

    private void assertInPhonebook(UserWrapper user) {
        assertTrue(game.phoneBook.containsValue(user.user));
    }

    private void assertNotInPhonebook(UserWrapper user) {
        assertFalse(game.phoneBook.containsValue(user.user));
    }

    private void assertNotInGame(UserWrapper user) {
        assertNull(user.user.game);
    }

    private void assertInGame(UserWrapper user) {
        assertNotNull(user.user.game);
    }

    private void assertHost(UserWrapper user) {
        assertSame(game.getHost(), user.user);
    }

    private void assertPlayerNotExited(UserWrapper user) throws SQLException {
        Connection connection = new Connection();
        assertFalse(PlayerRepo.getIsExited(connection, game.getID(), user.id));
        connection.close();
    }

    private void assertPlayerExited(UserWrapper user) throws SQLException {
        Connection connection = new Connection();
        assertTrue(PlayerRepo.getIsExited(connection, game.getID(), user.id));
        connection.close();
    }

    private void assertPlayerLeaveEventNotCalled(UserWrapper user) throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("playerRemove"))
                continue;
            if(event.getLong("userID") != user.id)
                continue;
            fail("leave event fired");
        }
    }

    private void assertPlayerLeaveEventCalled(UserWrapper user) throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("playerRemove"))
                continue;
            if(event.getLong("userID") != user.id)
                continue;
            return;
        }
        fail("leave event not fired");
    }

    private void assertGameEndEventNotCalled() throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("gameEnd"))
                continue;
            if(!event.getString("joinID").equals(game.joinID))
                continue;
            fail("close event fired");
        }
    }

    private void assertGameEndEventCalled() throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("gameEnd"))
                continue;
            if(!event.getString("joinID").equals(game.joinID))
                continue;
            return;
        }
        fail("close event not fired");
    }

    private void assertLobbyCloseEventNotCalled() throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("closeLobby"))
                continue;
            if(!event.getString("joinID").equals(game.joinID))
                continue;
            fail("close event fired");
        }
    }

    private void assertLobbyCloseEventCalled() throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("closeLobby"))
                continue;
            if(!event.getString("joinID").equals(game.joinID))
                continue;
            return;
        }
        fail("close event not fired");
    }

    private void assertInPlayerTable(UserWrapper user) throws SQLException {
        Connection connection = new Connection();
        assertTrue(PlayerRepo.existsByGameIDAndUserID(connection, game.getID(), user.id));
        connection.close();
    }

    private void assertNotInPlayerTable(UserWrapper user) throws SQLException {
        Connection connection = new Connection();
        assertFalse(PlayerRepo.existsByGameIDAndUserID(connection, game.getID(), user.id));
        connection.close();
    }
}
