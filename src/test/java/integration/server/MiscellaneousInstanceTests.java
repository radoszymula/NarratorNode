
package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import game.ai.Controller;
import game.event.DayChat;
import game.event.JailChat;
import game.event.Message;
import game.event.VoidChat;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.support.CommandHandler;
import game.logic.support.action.ActionList;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Agent;
import game.roles.Architect;
import game.roles.Baker;
import game.roles.Blacksmith;
import game.roles.Bulletproof;
import game.roles.Disguiser;
import game.roles.Douse;
import game.roles.Driver;
import game.roles.FactionKill;
import game.roles.Framer;
import game.roles.Ghost;
import game.roles.Godfather;
import game.roles.GraveDigger;
import game.roles.Hidden;
import game.roles.Jailor;
import game.roles.Mayor;
import game.roles.SerialKiller;
import game.roles.Stripper;
import game.setups.BraveNewWorld;
import game.setups.Classic;
import game.setups.GoT;
import game.setups.Setup;
import game.setups.UnreliableCops;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.Command;
import models.FactionRole;
import models.json_output.ProfileJson;
import nnode.Game;
import nnode.StateObject;
import repositories.CommandRepo;
import repositories.Connection;
import repositories.SetupRepo;
import services.FactionService;
import util.Assertions;
import util.LookupUtil;
import util.UserFactory;

public class MiscellaneousInstanceTests extends InstanceTests {

    public MiscellaneousInstanceTests(String name) {
        super(name);
    }

    public static final boolean DAY = Narrator.DAY_START, NIGHT = Narrator.NIGHT_START;

    public void testJoinedStoryPlayerWritingBasic() throws SQLException, JSONException {
        UserWrapper host = UserFactory.hostPublicGame();
        host.addWebPlayer();
        assertEquals(2, host.getWebSize());
        host.addWebPlayer();
        assertEquals(3, host.getWebSize());

        host.addSetupHidden(BasicRoles.Agent());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.SerialKiller());

        long id = host.startGame();

        Assertions.assertReplayPlayerCount(id);
    }

    public void testGuestJoinedPlayerSize() throws SQLException, JSONException {
        UserWrapper host = UserFactory.hostPublicGame();
        host.addWebPlayer();
        assertEquals(2, host.getWebSize());
        host.addWebPlayer();
        assertEquals(3, host.getWebSize());

        host.addSetupHidden(BasicRoles.Agent());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.SerialKiller());
        long replayID = host.startGame();

        Assertions.assertReplayPlayerCount(replayID);
    }

    public void testEscortPlayerSizeLength() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        host.addBots(2);
        assertEquals(3, host.user.game.narrator._players.size());

        host.addSetupHidden(BasicRoles.Escort());
        host.addSetupHidden(BasicRoles.Arsonist());
        host.addSetupHidden(BasicRoles.Witch());

        host.prefer(BasicRoles.Escort());

        host.startGame();

        assertEquals(1, host.getActions().size());
        assertTrue(host.getActions().containsKey(Stripper.COMMAND));
    }

    public void testChatTabs() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        ArrayList<Controller> bots = host.addBots(2);
        assertEquals(2, bots.size());

        host.addSetupHidden(BasicRoles.Escort());
        host.addSetupHidden(BasicRoles.Arsonist());
        host.addSetupHidden(BasicRoles.Witch());

        host.prefer(BasicRoles.Witch());

        host.startGame();
        cancelBrain(host);

        Set<String> keys;
        assertTrue(narrator.isNight());
        assertTrue(narrator.isFirstNight());
        assertEquals(0, narrator.getDayNumber());
        assertEquals(2, host.chatLogs.size());
        assertTrue(host.chatLogs.containsKey(VoidChat.NAME + " " + host.user.game.narrator.getDayNumber()));

        host.endNight();

        assertTrue(narrator.isDay());
        keys = host.chatLogs.keySet();
        assertTrue(keys.contains("Day 1"));
        assertTrue(keys.contains("Night 0"));
        assertFalse(keys.contains("Night 1"));

        host.skipVote();
        host.endPhase();

        // n1
        assertTrue(narrator.isNight());

        host.endNight();

        // day 2
        keys = host.chatLogs.keySet();
        assertTrue(keys.contains("Day 1"));
        assertTrue(keys.contains("Day 2"));
        assertFalse(keys.contains("Night 0"));
        assertTrue(keys.contains("Night 1"));
        assertFalse(keys.contains("Night 2"));

        host.skipVote();
        host.endPhase();

        // night 2
        keys = host.chatLogs.keySet();
        assertTrue(keys.contains("Day 1"));
        assertTrue(keys.contains("Day 2"));
        assertFalse(keys.contains("Night 0"));
        assertFalse(keys.contains("Night 1"));
        assertTrue(keys.contains("Night 2"));

        host.endNight();

        host.vote(bots.get(1));
        host.endPhase();

        if(narrator.isInProgress()){
            host.endPhase();
        }

        assertGameOver();
        host.getPlayer().getChatKeys();
        assertEquals(1, host.keys.size());
    }

    public void testMafChatLogsDayTime() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        ArrayList<Controller> bots = host.addBots(7);

        Faction townFaction = host.user.game.narrator.getFaction(Setup.TOWN_C);
        Role drugDealer = LookupUtil.findRole(host.user.game.narrator, "Drug Dealer");
        FactionRole factionRole = FactionService.createFactionRole(host.id, townFaction.id, drugDealer.getID());
        
        for(int i = 0; i < 5; i++)
            host.addSetupHidden(BasicRoles.Chauffeur());
        for(int i = 0; i < 3; i++)
            host.addSetupHidden(factionRole);

        host.startGame();

        assertTrue(host.chatLogs.containsKey(narrator.getEventManager().voidChat.getName()));
        host.setNightTarget(FactionKill.COMMAND, bots.get(0));
        host.endNight();
    }

    public void testSeparateBasicRoles() throws JSONException, SQLException {
        UserWrapper host1 = UserFactory.hostGame(PUBLIC, UserFactory.MOD);
        UserWrapper host2 = UserFactory.hostGame(PUBLIC, UserFactory.MOD);

        host1.modifyFactionRoleAbility(BasicRoles.Blacksmith(), Blacksmith.class, AbilityModifier.GS_FAULTY_GUNS, true);
        host1.modifyFactionRole(BasicRoles.Blacksmith(), RoleModifier.UNDETECTABLE, true);

        Game i1 = host1.user.game;
        Game i2 = host2.user.game;

        assertNotSame(i1, i2);

        String name = BasicRoles.Blacksmith().role.getName();

        FactionRole rt1 = LookupUtil.findFactionRole(host1.user.game.narrator, name, Setup.TOWN_C);
        FactionRole rt2 = LookupUtil.findFactionRole(host2.user.game.narrator, name, Setup.TOWN_C);

        assertTrue(rt1.abilityModifiers.get(Blacksmith.class).getBoolean(AbilityModifier.GS_FAULTY_GUNS));
        assertTrue(rt1.roleModifiers.getBoolean(RoleModifier.UNDETECTABLE));

        try {
        	host2.modifyFactionRoleAbility(BasicRoles.Blacksmith(), Blacksmith.class, AbilityModifier.GS_FAULTY_GUNS, 1);
        	fail();
        }catch(NarratorException e) {}
        try {
        	host2.modifyFactionRole(BasicRoles.Blacksmith(), RoleModifier.AUTO_VEST, true);
        	fail();
        }catch(NarratorException e) {}

        assertFalse(rt2.abilityModifiers.containsKey(Blacksmith.class));
        assertEqual(0, rt2.roleModifiers.getOrDefault(RoleModifier.AUTO_VEST, 0));
    }

    public void testJailorTargeting() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        ArrayList<Controller> bots = host.addBots(2);

        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Kidnapper());
        host.addSetupHidden(BasicRoles.Kidnapper());

        host.prefer(BasicRoles.Jailor());

        host.startGame();
        cancelBrain(host);

        host.endNight();

        assertFalse(host.abilities.get(Jailor.JAIL).isEmpty());
        host.doDayAction(bots.get(0));
        assertFalse(host.submittedActions.isEmpty());

        host.cancelAction(0);
        assertTrue(host.submittedActions.isEmpty());
        assertTrue(host.user.game.narrator.actionStack.isEmpty());

        host.doDayAction(bots.get(0));
        ActionList jailedTargets = host.getPlayer().getActions();
        assertFalse(jailedTargets.isEmpty());
        assertEquals(bots.get(0).getPlayer(narrator), jailedTargets.getFirst().getTarget());
        host.vote(bots.get(1));
        host.endPhase();

        assertTrue(bots.get(0).getPlayer(narrator).isJailed());
        assertFalse(host.getPlayer().isJailed());
        assertTrue(host.getPlayer().getActions().isEmpty());
        assertTrue(narrator.isInProgress());
        assertTrue(host.getActions().containsKey(Jailor.NO_EXECUTE));

        host.endNight();

        assertFalse(narrator.isInProgress());
        host.requestChat();
    }

    public void testJailorChats() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(UserFactory.PRIVATE, !UserFactory.MOD);
        Controller bot = host.addBots(2).get(0);

        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Kidnapper());
        host.addSetupHidden(BasicRoles.Kidnapper());

        host.prefer(BasicRoles.Jailor());

        host.startGame(Narrator.DAY_START);
        cancelBrain(host);
        host.user.requestAllGameInfo();

        assertFalse(host.buttonVisible);

        host.doDayAction(bot);
        host.skipVote();
        host.endPhase();

        host.say("Hello", JailChat.GetKey(bot, 1));
        host.requestChat();
        assertFalse(host.chatLogs.get(JailChat.GetName(bot)).isEmpty());
    }

    public void testJailorChatLook() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper jailor = host;
        UserWrapper fodder = jailor.addWebPlayer();
        UserWrapper rando = jailor.addWebPlayer();

        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.Kidnapper());

        jailor.prefer(BasicRoles.Jailor());
        fodder.prefer(BasicRoles.Kidnapper());

        host.startGame(Narrator.DAY_START);
        jailor.user.requestAllGameInfo();

        assertFalse(jailor.buttonVisible);

        jailor.doDayAction(fodder);
        jailor.skipVote();
        fodder.skipVote();

        String key = JailChat.GetKey(fodder.getPlayer(), 1);
        String jailName = JailChat.GetName(fodder.getPlayer());

        assertFalse(fodder.abilities.containsKey(FactionKill.COMMAND));

        jailor.say("Hello", key);

        assertTrue(fodder.chatLogs.containsKey(jailName));
        partialContains(fodder.chatLogs.get(jailName), Jailor.CAPTOR);
        partialContains(fodder.chatLogs.get(jailName), Message.INITIAL);

        fodder.say("Dont kill me", key);
        // test look from both sides
        assertFalse(jailor.chatLogs.get(jailName).isEmpty());

        jailor.setNightTarget(Jailor.EXECUTE, fodder);
        jailor.endNight();
        fodder.endNight();
        rando.endNight();

        assertFalse(narrator.isInProgress());
    }

    public void testSetupCountRemainsConstant() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        Connection connection = host.user.game.connection;
        int setupCount = SetupRepo.getCount(connection) + 1;

        host.setSetup(GoT.KEY);
        assertEquals(setupCount, SetupRepo.getCount(connection));

        host.addBots(15);
        assertEquals(setupCount, SetupRepo.getCount(connection));

        host.user.game.seed = Long.parseLong("3825557511814427954");
        host.startGame();
        assertEquals(setupCount, SetupRepo.getCount(connection));

        host.user.game.cancelBrain();
        BrainEndGame = false;
        skipTearDown = true;
    }

    public void testBlacksmithModifier() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();

        host.setSetup(GoT.KEY);

        boolean armsDhealerHasModifier = false;
        for(FactionRole factionRole: host.user.game.narrator.getFaction(GoT.martell_c)._roleSet.values())
            if(factionRole.role.contains(Blacksmith.class))
                armsDhealerHasModifier = factionRole.abilityModifiers.get(Blacksmith.class)
                        .getBoolean(AbilityModifier.AS_FAKE_VESTS);
        
        assertTrue(armsDhealerHasModifier);
        
        host.addBots(15);

        assertEquals(16, host.user.game.narrator.getRolesList().size());
        List<Hidden> randoms = host.user.game.narrator.hiddens;
        Hidden hidden;
        for(int i = 0; i < randoms.size(); i++){
            hidden = randoms.get(i);
            if(!hidden.getColors().contains(GoT.martell_c))
                continue;
            for(FactionRole m: hidden.getFactionRoles()){
                if(m.role.is(Blacksmith.class))
                    assertTrue(m.role.getName().startsWith(GoT.ARMS_DEALER));
            }
        }

        host.user.game.cancelBrain();
        BrainEndGame = false;
        skipTearDown = true;
    }

    public void testGodfatherRuleTexts() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();

        Faction t = host.user.game.narrator.getFaction(Setup.MAFIA_C);
        boolean godfatherHasModifier = false;
        for(FactionRole rt: t._roleSet.values()){
            if(rt.role.contains(Godfather.class))
                godfatherHasModifier = rt.role.modifiers.getBoolean(RoleModifier.UNDETECTABLE);
        }
        assertTrue(godfatherHasModifier);

        ArrayList<Controller> nBots = host.addBots(3);

        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.DrugDealer());
        host.addSetupHidden(BasicRoles.Framer());
        host.addSetupHidden(BasicRoles.Godfather());

        host.prefer(BasicRoles.Godfather());

        host.startGame(Narrator.NIGHT_START);

        for(Controller nb: nBots){
            if(!nb.getPlayer(narrator).getFaction().equals(host.getPlayer().getFaction())){
                host.setNightTarget(FactionKill.COMMAND, nb);
            }
        }

        host.endNight();

        assertFalse(narrator.isInProgress());

        JSONObject facs = host.factions;

        assertFalse(facs.has(BasicRoles.Spy().toString()));
    }

    private void clearUnreads(UserWrapper... wpws) {
        for(UserWrapper wpw: wpws)
            for(int i = 0; i < wpw.unread.size();)
                wpw.unread.clear();
    }

    public void testNewMessageRecord1() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper p2 = host.addWebPlayer();
        UserWrapper p3 = host.addWebPlayer();

        host.addBots(2);

        host.addSetupHidden(BasicRoles.Mayor());
        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Sheriff());
        host.addSetupHidden(BasicRoles.Bulletproof());
        host.addSetupHidden(BasicRoles.Assassin());

        p3.prefer(BasicRoles.Mayor());
        p2.prefer(BasicRoles.Jailor());

        host.startGame(Narrator.DAY_START);
        cancelBrain(host);

        assertTrue(narrator.isDay());
        assertTrue(p3.getPlayer().is(Mayor.class));

        assertFalse(host.unread.isEmpty());

        String d1 = narrator.getEventManager().getDayChat().getName();
        assertUnreadCount(host, d1, 1);

        host.unread.clear();

        host.requestUnreads();
        assertUnreadCount(host, d1, 1);

        host.setRead(d1);
        assertUnreadCount(host, d1, 0);

        host.requestUnreads();
        assertTrue(host.isCaughtUp());

        p2.setRead(d1);

        host.vote(p2);
        assertUnreadCount(host, d1, 0);
        assertUnreadCount(p2, d1, 1);

        host.skipVote();
        assertUnreadCount(host, d1, 0);
        assertUnreadCount(p2, d1, 2);

        p2.setRead(d1);

        host.say("lol", narrator.getEventManager().getDayChat().getKey(host.getPlayer()));
        assertUnreadCount(host, d1, 0);
        assertUnreadCount(p2, d1, 1);
        assertEquals(1, p2.user.game.readTracker.get(d1).unreadCount(p2.user));

        p2.say("lol back", narrator.getEventManager().getDayChat().getKey(p2.getPlayer()));
        assertEquals(0, p2.user.game.readTracker.get(d1).unreadCount(p2.user));

        p2.skipVote();

        clearUnreads(host, p2, p3);

        p3.doDayAction();
        assertTrue(p3.unread.isEmpty());
        assertFalse(p2.unread.isEmpty());

        clearUnreads(host, p2, p3);
        p2.doDayAction(p3);
        p3.skipVote(); // 3rd vote, 5 total players

        assertUnreadCount(host, d1, 0);
        assertUnreadCount(p2, d1, 0);
        assertUnreadCount(p3, d1, 0);

        assertFalse(host.unread.isEmpty());
        assertEquals(1, p2.unread.size());
        assertEquals(2, p3.unread.size()); // for jail
    }

    private void assertUnreadCount(UserWrapper w1, String name, int count) {
        if(w1.unread.containsKey(name))
            assertEquals(count, (int) w1.unread.get(name));
        else if(count != 0)
            fail();
    }

    private static final String PREGAME = DayChat.PREGAME;

    public void testPregameChatting() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper p2 = host.addWebPlayer();
        UserWrapper p3 = host.addWebPlayer();

        assertUnreadCount(host, PREGAME, 0);
        assertUnreadCount(p2, PREGAME, 0);
        assertUnreadCount(p3, PREGAME, 0);

        host.say("hello", DayChat.KEY);

        assertUnreadCount(host, PREGAME, 0);
        assertUnreadCount(p2, PREGAME, 1);
        assertUnreadCount(p3, PREGAME, 1);

        host.say("hello2", DayChat.KEY);

        assertUnreadCount(host, PREGAME, 0);
        assertUnreadCount(p2, PREGAME, 2);
        assertUnreadCount(p3, PREGAME, 2);

        p2.setRead(PREGAME);

        assertUnreadCount(host, PREGAME, 0);
        assertUnreadCount(p2, PREGAME, 0);
        assertUnreadCount(p3, PREGAME, 2);
    }

    public void testNewMessageRecord2() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper p2 = host.addWebPlayer();
        UserWrapper p3 = host.addWebPlayer();

        host.addBots(2);

        host.addSetupHidden(BasicRoles.Mayor());
        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Assassin());
        host.addSetupHidden(BasicRoles.Bulletproof());
        host.addSetupHidden(BasicRoles.Assassin());

        host.prefer(BasicRoles.Assassin());
        p3.prefer(BasicRoles.Mayor());
        p2.prefer(BasicRoles.Jailor());

        host.startGame(Narrator.NIGHT_START);

        assertEquals(2, host.unread.size());

        host.endPhase();

        assertTrue(narrator.isDay());

        DayChat dc = narrator.getEventManager().getDayChat();
        String d1 = narrator.getEventManager().getDayChat().getName();
        clearUnreads(host);
        host.unread.clear();

        p2.say("test 2", dc.getKey(p2.getPlayer()));
        host.setRead(d1);
        host.requestUnreads();

        assertUnreadCount(host, d1, 0);
    }

    public void testInvalidDisplayName() throws JSONException, SQLException {
        UserWrapper nc = new UserWrapper(" ");
        nc.hostGame(true);
        // i can host a game even if the name inputted is garbage.
    }

    public void testSkipDayError() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        host.addWebPlayer();
        host.updateRule(SetupModifier.CHAT_ROLES, false);

        host.addBots(13);
        host.setSetup(BraveNewWorld.KEY);

        host.startGame(Narrator.DAY_START, true); // host voting

        host.submitHostSkipVote(host);
        BrainEndGame = false;
    }

    public void testAssassinDayAbility() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper p2 = host.addWebPlayer();

        host.addBots(4);

        host.addSetupHidden(BasicRoles.Mayor());
        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Assassin());
        host.addSetupHidden(BasicRoles.Bulletproof());
        host.addSetupHidden(BasicRoles.Assassin());
        host.addSetupHidden(Hidden.TownProtective());

        host.prefer(BasicRoles.Assassin());
        p2.prefer(BasicRoles.Jailor());

        host.updateRule(SetupModifier.OMNISCIENT_DEAD, true);

        host.startGame(Narrator.DAY_START);

        assertTrue(host.hasDayAction);
        host.doDayAction(p2);
        assertFalse(host.hasDayAction);
    }

    public void testMayorDayAbility() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper p2 = host.addWebPlayer();

        host.addBots(3);

        host.addSetupHidden(BasicRoles.Mayor());
        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Assassin());
        host.addSetupHidden(BasicRoles.Arsonist());
        host.addSetupHidden(BasicRoles.Assassin());

        host.prefer(BasicRoles.Mayor());
        p2.prefer(BasicRoles.Arsonist());

        host.startGame(Narrator.DAY_START);

        assertEquals(1, host.abilities.size());
        assertEquals(1, p2.abilities.size());
    }

    public void testLastWill() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();

        host.setSetup(Classic.KEY);
        host.updateRule(SetupModifier.LAST_WILL, true);

        host.addBots(6);

        host.startGame();

        host.setLastWill("hi");

        assertEquals("hi", host.getPlayer()._lastWill);
    }

    public void testSelfKillReverseError() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();

        host.addBots(14);
        host.setSetup(BraveNewWorld.KEY);
        assertEquals(15, host.user.game.narrator.rolesList.size());
        host.prefer(BasicRoles.Godfather());
        host.startGame();

        host.skipVote();
        cancelBrain(host);
        host.setNightTarget(FactionKill.COMMAND, host);
        host.endNight();

        isDead(host.getPlayer());

    }

    public void testSkipDayEndPhaseOrder() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();

        host.addBots(14);

        host.setSetup(Classic.KEY);

        host.startGame(Narrator.NIGHT_START);
        cancelBrain(host);
        host.endNight();

        host.user.game.narrator.forceEndDay();

        host.skipVote();

        Connection c = new Connection();
        List<String> commands = Command.getText(CommandRepo.getByReplayID(c, host.user.game.getID()));
        c.close();
        assertTrue(commands.get(commands.size() - 1).contains(CommandHandler.END_PHASE));
    }

    public void testComputerVoting() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();

        List<Controller> bots = host.addBots(14);

        host.setSetup(Classic.KEY);
        host.startGame(Narrator.DAY_START);

        host.vote(bots.get(0));

        assertTrue(narrator.isNight());

        host.endNight();

        if(host.getPlayer().isAlive()){
            host.skipVote();
            assertTrue(narrator.isNight());
        }
    }

    public void testComputerUnvoting() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PUBLIC, UserFactory.MOD);
        UserWrapper fodder = host.addWebPlayer();
        host.addWebPlayer();
        host.addWebPlayer();
        host.addWebPlayer();
        host.addWebPlayer();

        Controller bot = host.addBots(1).get(0);

        host.setSetup(UnreliableCops.KEY);

        host.startGame(Narrator.DAY_START);

        host.vote(fodder);

        assertVoteTarget(fodder.getPlayer(), host.getPlayer());
        assertVoteTarget(fodder.getPlayer(), bot.getPlayer(narrator));

        host.unvote();

        assertVoteTarget(null, host.getPlayer());
        assertVoteTarget(null, bot.getPlayer(narrator));

        host.user.game.cancelBrain();
    }

    public void testArchitectPlayerList() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PUBLIC, UserFactory.MOD);
        ArrayList<Controller> bots = host.addBots(10);

        FactionRole mafiaArchitect = LookupUtil.findFactionRole(host.user.game.narrator,
                Architect.class.getSimpleName(), Setup.MAFIA_C);
        for(int i = 0; i < 10; i++)
            host.addSetupHidden(mafiaArchitect);

        host.addSetupHidden(BasicRoles.Citizen());

        host.prefer(mafiaArchitect);
        host.startGame(DAY);

        PlayerList jBuilds = host.abilities.get(Architect.COMMAND);
        assertFalse(jBuilds.isEmpty());

        JSONArray targets = new JSONArray();
        targets.put(host.getName());
        targets.put(bots.get(0).getName());

        JSONObject action = new JSONObject();
        action.put(StateObject.command, Architect.COMMAND);
        action.put(StateObject.targets, targets);
        action.put("userID", host.getUserID());
        host.httpRequest("PUT", "actions", action);

        assertFalse(host.submittedActions.isEmpty());

        targets = new JSONArray();
        targets.put(host.getName());
        targets.put(bots.get(1).getName());

        action.put(StateObject.oldAction, 0);
        action.put(StateObject.targets, targets);

        host.httpRequest("PUT", "actions", action);

        Narrator n = host.getPlayer().narrator;
        assertTrue(host.getPlayer().getActions().getFirst()._targets.contains(bots.get(1).getPlayer(n)));
        assertFalse(host.getPlayer().getActions().getFirst()._targets.contains(bots.get(0).getPlayer(n)));

        host.skipVote();
        assertTrue(host.user.game.narrator.isNight());
        /// bots should follow. yay

        assertFalse(host.getPlayer().getRoleCreatedChats(1).isEmpty());
        assertTrue(bots.get(0).getPlayer(n).getRoleCreatedChats(1).isEmpty());
        assertFalse(bots.get(1).getPlayer(n).getRoleCreatedChats(1).isEmpty());
    }

    public void testDriverDoubleFeedback() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PUBLIC, UserFactory.MOD);
        ArrayList<Controller> bots = host.addBots(10);

        for(int i = 0; i < bots.size(); i++)
            host.addSetupHidden(BasicRoles.Witch());
        host.addSetupHidden(BasicRoles.BusDriver());

        host.prefer(BasicRoles.BusDriver());

        host.startGame(NIGHT);

        host.setNightTarget(Driver.COMMAND, host, bots.get(0));
        host.endNight();

        // header taken out
        // the selection message
        // the feedback
        partialContains(host.chatLogs.get("Day 1"), Driver.FEEDBACK);
        assertEquals(1, host.chatLogs.get("Night 0").size()); // was 2, but selection message is out now
    }

    public void testGhostAcceptableTargets() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PUBLIC, UserFactory.MOD);
        UserWrapper fodder = host.addWebPlayer();
        ArrayList<Controller> bots = host.addBots(10);

        host.addSetupHidden(BasicRoles.Ghost());
        host.addSetupHidden(BasicRoles.SerialKiller());

        for(int i = 0; i < bots.size(); i++)
            host.addSetupHidden(BasicRoles.Citizen());

        fodder.prefer(BasicRoles.SerialKiller());
        ;
        host.prefer(BasicRoles.Ghost());

        host.startGame(NIGHT);

        assertIsNight();

        fodder.setNightTarget(SerialKiller.COMMAND, host);
        fodder.endNight();
        host.endNight();

        fodder.skipVote();

        assertTrue(narrator.isNight());
        assertTrue(host.abilities.containsKey(Ghost.COMMAND));
        assertTrue(host.buttonVisible);

        fodder.endNight();

        assertTrue(narrator.isNight());
        host.setNightTarget(Ghost.COMMAND, bots.get(0));
        host.endNight();

        host.ventVote(bots.get(0), fodder);
        host.user.game.cancelBrain();
    }

    public void testSnitchDying() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);

        ArrayList<Controller> bots = host.addBots(15);
        host.addSetupHidden(BasicRoles.SerialKiller());
        host.addSetupHidden(BasicRoles.Snitch(), 15);

        host.prefer(BasicRoles.SerialKiller());

        host.startGame(Narrator.NIGHT_START);

        host.setNightTarget(SerialKiller.COMMAND, bots.get(0));
        host.endNight();
    }

    public void testDisguiserLeaving() throws JSONException, SQLException {
        UserWrapper disg = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        disg.addSetupHidden(BasicRoles.Disguiser());

        disg.updateRule(SetupModifier.CHAT_ROLES, true);
        disg.updateRule(SetupModifier.HOST_VOTING, false);

        Narrator narrator = disg.user.game.narrator;
        FactionRole yakAgent = LookupUtil.findFactionRole(narrator, Agent.class.getSimpleName(), Setup.YAKUZA_C);
        FactionRole yakFramer = LookupUtil.findFactionRole(narrator, Framer.class.getSimpleName(), Setup.YAKUZA_C);
        UserWrapper doc = disg.addWebPlayer(BasicRoles.Doctor());
        UserWrapper yak1 = disg.addWebPlayer(yakAgent);
        UserWrapper yak2 = disg.addWebPlayer(yakFramer);

        disg.startGame(Narrator.NIGHT_START);

        disg.setNightTarget(Disguiser.COMMAND, yak1);
        disg.endNight();
        doc.endNight();
        yak1.endNight();
        yak2.endNight();

        disg.skipVote();
        doc.skipVote();

        disg.setNightTarget(Faction.SEND, disg, yak2);
        disg.setNightTarget(FactionKill.COMMAND, yak2);
        doc.endNight();
        disg.endNight();
        yak2.endNight();

        isDead(yak2.getPlayer());
    }

    public void testDeadArsonActionText() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addSetupHidden(BasicRoles.Arsonist(), 5);
        host.addSetupHidden(BasicRoles.GraveDigger());
        host.addSetupHidden(BasicRoles.Citizen());

        ArrayList<Controller> bots = host.addBots(6);
        host.prefer(BasicRoles.GraveDigger());

        host.updateRule(SetupModifier.HOST_VOTING, false);
        host.startGame(Narrator.DAY_START);

        Narrator narrator = host.getPlayer().narrator;
        Controller nc = null;
        for(Controller nb: bots){
            if(nb.getPlayer(narrator).is(Douse.class)){
                nc = nb;
                break;
            }
        }
        if(nc == null)
            throw new Error("nc was null");

        bots.remove(nc);

        host.vote(nc);

        assertTrue(narrator.isNight());

        host.setNightTarget(GraveDigger.COMMAND, Douse.COMMAND, nc, bots.get(0));
        String commandText = host.submittedActionsJSON.getJSONArray(StateObject.actionList).getJSONObject(0)
                .getString("text");
        assertTrue(commandText.contains(bots.get(0).getName()));
    }

    public void testLynchNightDeathLog() throws JSONException {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Goon());

        dayStart();
        p1.vote(p2);
        p3.vote(p2);

        Message e = narrator.getEventManager().getDayChat().getEvents().getLast();

        JSONArray chatLog = new JSONArray();
        Game.AppendMessage(chatLog, e, p1.getPlayer(narrator), p1.getPlayer(narrator).getID());

        JSONObject jo = chatLog.getJSONObject(0);
        JSONArray chat = jo.getJSONArray("chat");

        String night1 = narrator.getEventManager().getNightLog(VoidChat.KEY).getName();
        for(int i = 0; i < chat.length(); i++){
            if(chat.getString(i).equals(night1))
                return;
        }
        fail();
    }

    // this is a copy
    public void testJailorFeedbackTest() throws JSONException, SQLException {
        UserWrapper baker = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        baker.addSetupHidden(BasicRoles.Baker());

        assertEquals(1, narrator.getRolesList().size());
        boolean found = false;
        for(FactionRole m: narrator.getFaction(Setup.MAFIA_C)._roleSet.values()){
            if(m.getName().equalsIgnoreCase(BasicRoles.Kidnapper().getName())){
                found = true;
                break;
            }
        }
        assertTrue(found);

        UserWrapper j1 = baker.addWebPlayer(BasicRoles.Kidnapper());
        UserWrapper j2 = baker.addWebPlayer(BasicRoles.Kidnapper());
        baker.addWebPlayer(BasicRoles.Kidnapper());

        assertEquals(4, narrator.getPlayerCount());
        assertEquals(4, narrator.getRolesList().size());

        baker.prefer(BasicRoles.Baker());
        baker.updateRule(SetupModifier.CHAT_ROLES, true);

        baker.startGame(Narrator.DAY_START);

        j1.doDayAction(j2);
        j1.skipVote();
        baker.endPhase();

        baker.setNightTarget(Baker.COMMAND, j2);
        baker.endPhase();
        baker.endPhase();
        baker.endPhase();

        j1.doDayAction(j2);
        j1.skipVote();
        baker.endPhase();

        j1.setNightTarget(Jailor.EXECUTE, j2);
        baker.endPhase();
        assertEquals(3, narrator.getLiveSize());
    }

    private void jail(UserWrapper p1, UserWrapper p2) {
        p1.doDayAction(p2);
    }

    public void testMasonSeesAllies() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addSetupHidden(BasicRoles.MasonLeader());

        UserWrapper framer = host.addWebPlayer(BasicRoles.Framer());
        UserWrapper clubber = host.addWebPlayer(BasicRoles.Clubber());
        UserWrapper disg = host.addWebPlayer(BasicRoles.Disguiser());
        host.modifyFactionRole(BasicRoles.Disguiser(), RoleModifier.UNDETECTABLE, true);

        host.updateRule(SetupModifier.CHAT_ROLES, true);

        host.startGame(Narrator.NIGHT_START);

//        assertNotNull(host.user.player.role.getID());
        isRole(host, BasicRoles.MasonLeader());
        isRole(framer, BasicRoles.Framer());
        isRole(clubber, BasicRoles.Clubber());
        isRole(disg, BasicRoles.Disguiser());

        disg.setNightTarget(Disguiser.COMMAND, host);

        host.endNight();
        disg.endNight();
        clubber.endNight();
        framer.endNight();

        disg.skipVote();
        clubber.skipVote();

        assertEquals(1, ProfileJson.getAllies(clubber.getPlayer()).length());

        long townFactionID = host.user.game.narrator.getFaction(Setup.TOWN_C).id;
        long mafiaFactionID = host.user.game.narrator.getFaction(Setup.MAFIA_C).id;

        JSONObject ally = ProfileJson.getAllies(framer.getPlayer()).getJSONObject(0);
        assertTrue(ally.getJSONObject("modifiers").has("undetectable"));
        assertEquals(host.getPlayer().getID(), ally.getString("name"));
        assertEquals(BasicRoles.Disguiser().getName(), ally.getString("roleName"));
        assertEquals(mafiaFactionID, ally.getLong("factionID"));
//        assertEquals(disg.user.player.role.getID(), (Long) ally.getLong("roleID"));

        ally = ProfileJson.getAllies(clubber.getPlayer()).getJSONObject(0);
//        assertEquals(host.user.player.role.getID(), (Long) ally.getLong("roleID"));
        assertFalse(ally.getJSONObject("modifiers").has("undetectable"));
        assertEquals(host.getPlayer().getID(), ally.getString("name"));
        assertEquals(BasicRoles.MasonLeader().getName(), ally.getString("roleName"));
        assertEquals(townFactionID, ally.getLong("factionID"));
    }

    public void testRemoveFromNightChats() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addSetupHidden(BasicRoles.Citizen());

        UserWrapper fodder = host;
        fodder.prefer(BasicRoles.Citizen());

        UserWrapper jailor = host.addWebPlayer(BasicRoles.Jailor());
        UserWrapper gf = host.addWebPlayer(BasicRoles.Godfather());
        UserWrapper agent = host.addWebPlayer(BasicRoles.Agent());
        host.addWebPlayer(BasicRoles.Agent());

        host.updateRule(SetupModifier.CHAT_ROLES, true);

        host.startGame(Narrator.DAY_START);

        jail(jailor, gf);
        host.endPhase();

        agent.say(gibberish(), agent.getPlayer().getColor());
        agent.setNightTarget(FactionKill.COMMAND, fodder);
        host.endPhase();

        jail(jailor, gf);
        host.endPhase();

        host.endPhase();
        host.endPhase();

        host.endPhase();

        jail(jailor, gf);
        host.endPhase();
    }

    public void testNeutralKillersImmune() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        for(Role m: host.user.game.narrator.roles){
            if(m.is(SerialKiller.class))
                if(m.is(Bulletproof.class))
                    return;
        }

        fail("Serial Killer was not invulnerable");
    }
}
