package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;

import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Citizen;
import game.roles.CultLeader;
import game.roles.FactionKill;
import game.roles.GunAbility;
import game.roles.Gunsmith;
import game.roles.Hidden;
import game.roles.Infiltrator;
import game.roles.Sleepwalker;
import game.setups.BraveNewWorld;
import game.setups.FreeForAll;
import game.setups.MediumG;
import game.setups.Pirates;
import game.setups.Setup;
import game.setups.UnreliableCops;
import integration.logic.SuperTest;
import json.JSONException;
import json.JSONObject;
import models.enums.VoteSystemTypes;
import models.json_output.ProfileJson;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.StateObject;
import nnode.User;
import repositories.CommandRepo;
import repositories.Connection;
import repositories.PlayerRepo;
import repositories.ReplayRepo;
import services.FactionModifierService;
import services.FactionService;
import util.UserFactory;

public class DatabaseTests extends InstanceTests {

    public DatabaseTests(String name) {
        super(name);
    }

    public void testReloadableInstancesMoreThanTwice() throws JSONException, SQLException {
        NodeSwitch.reloadInstances();
        UserWrapper host = UserFactory.hostPublicGame();
        int prevLength = NodeSwitch.idToInstance.size() - 1;

        UserWrapper p1 = host.addWebPlayer();
        UserWrapper p2 = host.addWebPlayer();

        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.SerialKiller());

        p1.prefer(BasicRoles.Goon());
        p2.prefer(BasicRoles.Goon());

        host.startGame(Narrator.NIGHT_START);

        reloadInstances();

        assertEquals(prevLength + 1, NodeSwitch.idToInstance.size());

        reloadInstances();

        assertEquals(prevLength + 1, NodeSwitch.idToInstance.size());
        SuperTest.BrainEndGame = false;
    }

    public void testFinishedValue() throws JSONException, SQLException {

        Connection c = new Connection();
        int prevFinishedLength = ReplayRepo.getUnfinishedIDs(c).size();
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper p1 = host.addWebPlayer();
        UserWrapper p2 = host.addWebPlayer();

        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.SerialKiller());

        p1.prefer(BasicRoles.Goon());
        p2.prefer(BasicRoles.Goon());

        host.startGame(Narrator.DAY_START);

        p1.vote(host);
        p2.vote(host);
        assertEquals(prevFinishedLength, ReplayRepo.getUnfinishedIDs(c).size());
        c.close();
    }

    public void testLeavePostGameEnding() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        ArrayList<UserWrapper> others = new ArrayList<>();
        int otherCount = 2;
        for(int i = 0; i < otherCount; i++)
            others.add(host.addWebPlayer());

        host.setSetup(FreeForAll.KEY);
        host.updateRule(SetupModifier.VOTE_SYSTEM, VoteSystemTypes.PLURALITY);
        host.startGame(Narrator.DAY_START);
        host.endPhase(); // goes to discussion

        host.vote(others.get(0));
        others.get(1).vote(others.get(0));
        host.endPhase();

        assertTrue(host.user.game.narrator.isFinished());
        host.leaveGame();
        assertNull(host.user.game);
    }

    public void testLeaveGame() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        ArrayList<UserWrapper> others = new ArrayList<>();
        int otherCount = 6;
        for(int i = 0; i < otherCount; i++)
            others.add(host.addWebPlayer());

        host.setSetup(UnreliableCops.KEY);
        host.startGame();
        assertTrue(narrator.isStarted());

        long replayID = host.user.game.getID();
        host.leaveGame();

        for(UserWrapper o: others){
            if(!narrator.isDay())
                break;
            o.vote(host);
        }

        Connection c = new Connection();
        boolean isExited = PlayerRepo.getIsExited(c, replayID, host.user.id);

        assertFalse(isExited);

        host.leaveGame();

        isExited = PlayerRepo.getIsExited(c, replayID, host.user.id);
        assertTrue(isExited);

        c.close();

        NodeSwitch.reloadInstances();

        reloadInstances();

        // hasn't made the contact yet, no reason to load the user
        assertEquals(otherCount, NodeSwitch.phoneBook.size());
    }

    public void testBotGameLeaving() throws JSONException, SQLException {
        NodeSwitch.idToInstance.clear();
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        host.addBots(13);
        UserWrapper wp = host.addWebPlayer();

        host.setSetup(MediumG.KEY);
        host.prefer(BasicRoles.Goon());
        wp.prefer(BasicRoles.Goon(Setup.YAKUZA_C));

        host.startGame(Narrator.NIGHT_START);
        long replay_id = host.getGameID();

        assertNotSame(host.getPlayer().getFaction().getColor(), wp.getPlayer().getFaction().getColor());

        wp.setNightTarget(FactionKill.COMMAND, host);
        host.setNightTarget(FactionKill.COMMAND, wp);
        host.endPhase();

        isDead(host, wp);
        assertGameOver();

        host.leaveGame();
        wp.leaveGame();

        Connection c = new Connection();
        assertTrue(ReplayRepo.getAllIDs(c).contains(replay_id));
        c.close();

        reloadInstances();

        // game is gone, not loading it.
        assertEquals(0, NodeSwitch.idToInstance.size());
    }

    public void testHostVotingRecreation() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        host.addWebPlayer(BasicRoles.Goon());
        host.addWebPlayer(BasicRoles.SerialKiller());

        host.addSetupHidden(Hidden.NeutralKillingRandom());

        host.updateRule(SetupModifier.HOST_VOTING, true);

        host.startGame();
        assertTrue(host.user.game.hostID == host.id);

        reloadInstances();

        User host2 = NodeSwitch.phoneBook.get(host.getUserID());
        assertTrue(host2.game.hostID == host2.id);
    }

    public void testHiddenRandomsHost() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        host.setHiddenRandoms(true);
        host.addWebPlayer(BasicRoles.Goon());
        host.addWebPlayer(BasicRoles.Goon());
        host.addWebPlayer(BasicRoles.SerialKiller());

        host.updateRule(SetupModifier.HOST_VOTING, true);

        host.startGame();
        assertNotSame(host.user.game.hostID, Game.UNINITIALIZED_HOST_ID);

        reloadInstances();

        Game i = NodeSwitch.idToInstance.values().iterator().next();
        assertNotSame(Game.UNINITIALIZED_HOST_ID, i.hostID);
    }

    public void testResetGameLeak() throws JSONException, SQLException {
        SuperTest.BrainEndGame = false;
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);

        host.setSetup(BraveNewWorld.KEY);

        for(int i = 0; i < 25; i++)
            host.addBots(1);

        assertEquals(26, narrator.getPlayerCount());

        host.startGame();

        host.resetGame();

        host.startGame();
        host.requestChat();
    }

    public void testNewModifiers() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper p1 = host.addWebPlayer(BasicRoles.Goon());
        UserWrapper p2 = host.addWebPlayer(BasicRoles.SerialKiller());

        host.addSetupHidden(Hidden.NeutralKillingRandom());

        host.startGame(Narrator.NIGHT_START);

        assertTrue(p2.getPlayer().isInvulnerable());

        p1.setNightTarget(FactionKill.COMMAND, host);
        p1.endNight();
        p2.endNight();
        host.endNight();

        isAlive(host.getPlayer());

        host.skipVote();
        p2.skipVote();

        p1.setNightTarget(FactionKill.COMMAND, p2);
        p1.endNight();
        p2.endNight();
        host.endNight();

        isAlive(p2.getPlayer());

        reloadInstances();

        User host2 = NodeSwitch.phoneBook.get(host.getUserID());
        assertNotNull(host2);
        assertTrue(host2.player.narrator.isStarted());
        assertTrue(host2.player.isAlive());

        User p22 = NodeSwitch.phoneBook.get(p2.getUserID());
        assertNotNull(p22);
        assertTrue(p22.player.narrator.isStarted());
        assertTrue(p22.player.isAlive());
    }

    public void testNewTeamModifiers() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper gunsmithGoon = host.addWebPlayer(BasicRoles.Goon());
        UserWrapper serialKiller = host.addWebPlayer(BasicRoles.SerialKiller());

        host.addSetupHidden(BasicRoles.Citizen());
        Narrator n = host.user.game.narrator;
        Faction mafiaFaction = n.getFaction(BasicRoles.Goon().getColor());
        long factionAbilityID = FactionService.createFactionAbility(host.id, mafiaFaction.id,
                Gunsmith.class.getSimpleName());
        FactionModifierService.modify(host.id, mafiaFaction.id, factionAbilityID, AbilityModifier.GS_FAULTY_GUNS, true);

        host.startGame(Narrator.NIGHT_START);

        assertTrue(serialKiller.getPlayer().isInvulnerable());

        gunsmithGoon.setNightTarget(Gunsmith.COMMAND, Gunsmith.FAULTY, serialKiller);
        gunsmithGoon.endNight();
        serialKiller.endNight();
        host.endNight();

        assertEquals(1, serialKiller.getPlayer().getAbility(GunAbility.class).getRealCharges());

        reloadInstances();
    }

    public void testInternalNameLoading() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        ArrayList<UserWrapper> clients = new ArrayList<>();
        clients.add(host);
        for(int i = 0; i < 3; i++)
            clients.add(host.addWebPlayer());

        host.addSetupHidden(BasicRoles.Sleepwalker());
        host.addSetupHidden(BasicRoles.Citizen(), 2);
        host.addSetupHidden(BasicRoles.Agent());

        host.startGame();

        Long sleepwalkerID = null;
        for(Player p: host.user.game.narrator.getAllPlayers()){
            if(p.is(Sleepwalker.class))
                sleepwalkerID = host.user.game.phoneBook.get(p).id;
        }
        User sleepwalkerWP = NodeSwitch.phoneBook.get(sleepwalkerID);
        assertNotNull(sleepwalkerID);

        boolean assertionFound = false;
        clients.add(host);
        for(UserWrapper client: clients){
            if(client.id == sleepwalkerID){
                assertEquals(Citizen.class.getSimpleName(), client.role.getString(StateObject.roleName));
                assertTrue(client.getPlayer().is(Sleepwalker.class));
                assertionFound = true;
            }
        }
        assertTrue(assertionFound);

        reloadInstances();

        sleepwalkerWP = NodeSwitch.phoneBook.get(sleepwalkerID);
        assertNotNull(sleepwalkerWP);

        assertTrue(sleepwalkerWP.player.is(Sleepwalker.class));
        assertEquals(Citizen.class.getSimpleName(), sleepwalkerWP.player.factionRole.receivedRole.getName());
        skipTearDown = true;
    }

    public void testAliasLoadAndCultVis() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        ArrayList<UserWrapper> clients = new ArrayList<>();
        clients.add(host);
        for(int i = 0; i < 31; i++)
            clients.add(host.addWebPlayer());

        host.setSetup(Pirates.KEY);

        host.startGame();

        assertEquals(Pirates.CULTIST_ALIAS, host.user.game.narrator.getAlias(CultLeader.MINION_ALIAS));

        int size = CommandRepo.getByReplayID(host.user.game.connection, host.getGameID()).size();
        assertTrue(size >= narrator.getAliases().size()); // alias size

        UserWrapper infil = null, rebel = null, villager = null;
        for(UserWrapper nc: clients){
            if(nc.getPlayer() == null)
                continue;
            if(nc.getPlayer().is(Citizen.class))
                villager = nc;
            if(nc.getPlayer().is(Infiltrator.class))
                infil = nc;
            if(nc.getPlayer().is(CultLeader.class))
                rebel = nc;
        }

        rebel.setNightTarget(CultLeader.COMMAND, infil);
        narrator.forceEndNight();

        assertIsDay();

        rebel.skipVote();
        narrator.forceEndDay();

        assertIsNight();

        rebel.setNightTarget(CultLeader.COMMAND, villager);
        narrator.forceEndNight();

        assertIsDay();
        long rebelFactionID = host.user.game.narrator.getFaction(Pirates.rebels_c).id;

//        assertEquals(villager.getPlayer().getID(), findCultistRoleID(villager.user.game));
        JSONObject jAlly = ProfileJson.getAllies(rebel.getPlayer()).getJSONObject(0);
        assertEquals(Pirates.CULTIST_ALIAS, jAlly.getString("roleName"));
        assertEquals(rebelFactionID, jAlly.getLong("factionID"));
//        assertEquals(villager.getPlayer().getID(), jAlly.getLong("roleID"));
    }
}
