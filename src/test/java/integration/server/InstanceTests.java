package integration.server;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import game.logic.Player;
import game.roles.Ability;
import integration.logic.SuperTest;
import models.FactionRole;
import nnode.NodeSwitch;
import repositories.Connection;
import repositories.SetupRepo;
import repositories.UserRepo;
import util.UserFactory;

public class InstanceTests extends SuperTest {

    protected static final boolean PRIVATE = UserFactory.PRIVATE, PUBLIC = UserFactory.PUBLIC;

    public InstanceTests(String name) {
        super(name);
    }

    @Override
    public void setUp() throws Exception {
        wipeTables();

        SwitchWrapper.init();
        super.setUp();
    }

    public static void wipeTables() throws SQLException {
        Connection c = new Connection();
        SetupRepo.deleteAll(c);// TODO figure out how to use less connections
        UserRepo.deleteAll(c);
        NodeSwitch.reloadInstances();
        c.close();
    }

    private static int getConnectionCount() throws SQLException {
        int count = -1;
        Connection c = new Connection();
        PreparedStatement ps = c.prepareStatement("show status where `variable_name` = 'Threads_connected';");
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            count = rs.getInt(2);
            break;
        }
        rs.close();
        ps.close();
        c.close();

        return count;
    }

    @Override
    public void tearDown() throws Exception {
        SwitchWrapper.cleanUp();
        Thread.sleep(1050);

        super.tearDown();
    }

    protected void reloadInstances() throws SQLException {
        reloadInstances(true);
    }

    protected void reloadInstances(boolean killCurrent) throws SQLException {
        if(killCurrent)
            SwitchWrapper.killDBConnections();
        SwitchWrapper.phoneBook.clear();
        NodeSwitch.reloadInstances();
    }

    public void isRole(UserWrapper w, FactionRole m) {
        Player p = w.getPlayer();
        assertEquals(p.getColor(), m.getColor());
        assertTrue(Ability.isEqualContent(m.role.getBaseAbility(), p.getAbilities()));
    }

    public void cancelBrain(UserWrapper userWrapper) {
        userWrapper.user.game.cancelBrain();
    }

    public void testMock() {
        // need this so build doesn't hiccup. It expects there to be tests in this file.
    }
}
