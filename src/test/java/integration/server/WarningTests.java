package integration.server;

import java.sql.SQLException;

import game.logic.Narrator;
import game.logic.templates.BasicRoles;
import game.roles.FactionKill;
import json.JSONException;
import util.UserFactory;

public class WarningTests extends InstanceTests {

    public WarningTests(String name) {
        super(name);
    }

    public void testMafWarning() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper p1 = host.addWebPlayer();
        UserWrapper p2 = host.addWebPlayer();

        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.SerialKiller());

        p1.prefer(BasicRoles.Goon());
        p2.prefer(BasicRoles.Goon());

        host.startGame(Narrator.NIGHT_START);

        isRole(p1, BasicRoles.Goon());
        isRole(p2, BasicRoles.Goon());

        p1.setNightTarget(FactionKill.COMMAND, host);
        p2.setNightTarget(FactionKill.COMMAND, host);
    }
}
