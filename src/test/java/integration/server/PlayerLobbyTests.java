package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

import game.event.DayChat;
import game.logic.Narrator;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.roles.DrugDealer;
import game.setups.BraveNewWorld;
import game.setups.Setup;
import game.setups.VanillaE;
import json.JSONException;
import nnode.NodeSwitch;
import nnode.UnreadManager;
import repositories.Connection;
import repositories.UserPermissionRepo;
import util.UserFactory;

public class PlayerLobbyTests extends InstanceTests {

    public PlayerLobbyTests(String name) {
        super(name);
    }

    public void testBasicHostRepick() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper c1 = host.addWebPlayer();
        UserWrapper c2 = host.addWebPlayer();

        host.repick();
        assertFalse(host.isHost());
        assertTrue(c1.isHost() || c2.isHost());
        assertFalse(c1.isHost() && c2.isHost());
    }

    public void testHostRepickSpecific() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper c2 = host.addWebPlayer();
        c2.repick();
        host.addWebPlayer();

        host.repick(c2);
        assertFalse(host.isHost());
        assertTrue(c2.isHost());

        assertTrue(host.user.game.repickers.isEmpty());
    }

    public void testSuperUserRepick() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(UserFactory.PRIVATE, UserFactory.MOD);
        Connection c = host.user.game.connection;
        UserWrapper c1 = host.addWebPlayer();
        UserWrapper c2 = host.addWebPlayer();

        assertEquals(host.user.game.hostID, host.id);
        UserPermissionRepo.removeAllPermissions(c, c1.id);

        c1.repick();
        assertEquals(host.user.game.hostID, host.id);
        assertTrue(host.user.game.repickers.isEmpty());

        host.repick();

        assertFalse(host.isHost());

        host.repick();

        assertTrue(host.isHost());

        host.repick(c2);

        assertFalse(host.isHost());
        assertTrue(c2.isHost());
    }

    public void testHostRepick() throws SQLException, JSONException {
        UserWrapper host = UserFactory.hostPublicGame();

        for(int i = 0; i < 8; i++)
            host.addWebPlayer();

        assertEquals(host.id, host.user.game.hostID);

        ArrayList<UserWrapper> userWrappers = SwitchWrapper.getUserWrappers(host);
        for(int i = 0; i < 5; i++)
            userWrappers.get(i).repick();

        assertNotSame(host.id, host.user.game.hostID);
        boolean foundHost = false;
        for(UserWrapper client: SwitchWrapper.getUserWrappers(host)){
            if(client.isHost()){
                client.repick(host.name);
                foundHost = true;
                break;
            }
        }
        if(!foundHost)
            fail("Host didn't change");
        assertTrue(host.isHost());
    }

    public void testKickOnlinePlayer() throws SQLException, JSONException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper n1 = host.addWebPlayer();

        host.kick(n1);

        assertEquals(1, host.user.game.phoneBook.size());
    }

    public void testKickOfflinePlayer() throws SQLException, JSONException {
        UserWrapper host = UserFactory.hostPublicGame();
        assertNotNull(host);
        UserWrapper n1 = host.addWebPlayer();
        n1.disconnect();

        host.kick(n1);

        assertEquals(1, host.user.game.phoneBook.size());
    }

    public void testHostPrivateGame() throws JSONException, SQLException {
        int prevGameSize = NodeSwitch.instances.size();
        UserFactory.hostPrivateCustomGame();
        UserWrapper host2 = UserFactory.hostPrivateCustomGame();

        assertNotNull(host2.user.game);

        assertEquals(prevGameSize + 2, NodeSwitch.idToInstance.size());
    }

    public void testResetGameWithBots() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.setSetup(BraveNewWorld.KEY);

        host.addBots(26);
        assertEquals(27, narrator.getPlayerCount());

        long seed = new Random().nextLong();
        seed = Long.parseLong("8525734128161416563");
        System.out.println("testResetGameWithBots Seed : " + seed);
        host.user.game.narrator.setSeed(seed);

        host.startGame();

        host.resetGame(); // doesn't reset it

        host.startGame();

        assertTrue(narrator.isInProgress());
        assertTrue(narrator.isStarted());
    }

    public void testResetGameNoBots() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.setSetup(BraveNewWorld.KEY);

        for(int i = 0; i < 27; i++)
            host.addWebPlayer();

        host.setHiddenRandoms(true);
        host.startGame();

        long seed = new Random().nextLong();
        System.out.println("testResetGameNoBots\t" + seed);
        host.user.game.seed = seed;

        assertTrue(narrator.isInProgress());
        assertTrue(narrator.isStarted());

        host.resetGame();

        host.startGame();

        assertTrue(narrator.isInProgress());
        assertTrue(narrator.isStarted());
    }

    public void testRemoveRole() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        assertNotSame(0, host.user.game.narrator.getFactions().size());

        host.addSetupHidden(BasicRoles.Citizen());
        assertEquals(1, host.user.game.narrator.getRolesList().size());

        host.removeSetupHidden(BasicRoles.Citizen().role);
        assertEquals(0, narrator.getRolesList().size());
    }

    public void testChangeName() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addWebPlayer();

        assertNotNull(host.user.game.phoneBook.get(host.getPlayer()));
        host.changeName("bigMoney");
        assertNotNull(host.user.game.phoneBook.get(host.getPlayer()));

        assertEquals("bigMoney", host.gameName);

        host.changeName(DrugDealer.BLOCKED);
        assertEquals(1, host.errors.size());

        host.errors.clear();
        host.changeName("bigMoney");
        assertTrue(host.errors.isEmpty());

        host.changeName("my.man");
        assertEquals(1, host.errors.size());
    }

    public void testBadInitialName() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        UserWrapper wb = host.addWebPlayer("my.man");
        assertNotSame(wb.getName(), "my.man");
    }

    public void testFullGameWarning() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addSetupHidden(BasicRoles.Citizen());

        UserWrapper fodder = host;
        fodder.prefer(BasicRoles.Citizen());

        for(int i = 0; i < 7; i++)
            host.addWebPlayer(BasicRoles.Goon());

        host.hasWarning = false;
        assertEquals(8, narrator.getPlayerCount());
        UserWrapper failedJoiner = host.addWebPlayer();
        UserWrapper failedJoiner2 = host.addWebPlayer();

        assertEquals(8, narrator.getPlayerCount());
        assertTrue(failedJoiner.hasWarning);
        assertTrue(failedJoiner2.hasWarning);
        assertTrue(host.hasWarning);

    }

    public void testMaxCharacterLimit1() throws JSONException, SQLException {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < Constants.MAX_DB_PLAYER_LENGTH + 1; i++){
            sb.append("q");
        }
        UserWrapper userWrapper = UserFactory.hostGame(sb.toString(), true, true);

        assertTrue(userWrapper.getPlayer().getName().length() <= Constants.MAX_DB_PLAYER_LENGTH);
    }

    // test same name 40 characters, should go to guest
    public void testMaxCharacterLimit2() throws JSONException, SQLException {
        NodeSwitch.reloadInstances(); // clears out the other open lobbies.
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < Constants.MAX_DB_PLAYER_LENGTH; i++){
            sb.append("q");
        }
        UserWrapper p = UserFactory.hostGame(sb.toString(), PUBLIC, UserFactory.MOD);
        assertEquals(1, p.user.game.narrator.getPlayerCount());
        assertEquals(sb.toString(), p.getPlayer().getName());

        UserWrapper p2 = UserFactory.joinPublicGame(sb.toString());

        assertEquals(2, p.user.game.narrator.getPlayerCount());
        assertEquals(2, p2.user.game.narrator.getPlayerCount());
        assertEquals(p.getJoinID(), p2.getJoinID());
        assertNotSame(sb.toString(), p2.getPlayer().getName());
    }

    public void testUnreadContains() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        UnreadManager uManager = host.user.game.readTracker.get(DayChat.PREGAME);

        assertTrue(uManager.unreadCount.containsKey(host.user));
        host.changeName("whoawee");
        assertTrue(uManager.unreadCount.containsKey(host.user));
    }

    public void testMaxAutoPlayerLimit() throws JSONException, SQLException {
        Setup s = new VanillaE(new Narrator());
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);

        host.hasWarning = false;
        host.setSetup(VanillaE.KEY);

        while (s.getMaxPlayerCount() != host.user.game.phoneBook.size())
            host.addWebPlayer();

        assertEquals(s.getMaxPlayerCount(), host.user.game.narrator.getPlayerCount());
        host.addWebPlayer();
        assertEquals(s.getMaxPlayerCount(), host.user.game.narrator.getPlayerCount());

        assertTrue(host.hasWarning);
        host.hasWarning = false;
        host.autoStartCheck = false;
        host.startGame();

        assertInProgress();
    }

    public void testMinAutoPlayerLimit() throws JSONException, SQLException {
        Setup s = new VanillaE(new Narrator());
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);

        host.hasWarning = false;
        host.setSetup(VanillaE.KEY);

        while (s.getMinPlayerCount() - 1 != host.user.game.phoneBook.size())
            host.addWebPlayer();

        host.autoStartCheck = false;
        System.err.println("###### THE FOLLOWING 'NOT ENOUGH PLAYERS' IS EXPECTED");
        try {
        	host.startGame();
        	fail();
        }catch(NarratorException e) {}
        System.err.println("###### THE PRECEEDING 'NOT ENOUGH PLAYERS' WAS EXPECTED");
        assertFalse(host.getPlayer().narrator.isStarted());
        assertTrue(host.hasWarning);
        host.hasWarning = false;

        host.addWebPlayer();
        host.startGame();

        assertInProgress();
    }
}
