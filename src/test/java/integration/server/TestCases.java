package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import game.ai.Brain;
import game.ai.Computer;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.support.CommandHandler;
import game.logic.support.StoryPackage;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.logic.templates.PartialListener;
import game.roles.Ability;
import game.roles.Hidden;
import game.setups.BraveNewWorld;
import game.setups.GoT;
import game.setups.Pirates;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Command;
import models.schemas.SetupHiddenSchema;
import nnode.DBStoryPackage;
import nnode.Game;
import nnode.NodeSwitch;
import nnode.StoryWriter;
import nnode.StoryWriter.StoryWriterError;
import repositories.Connection;
import repositories.ReplayRepo;
import repositories.SetupHiddenRepo;
import repositories.UserRepo;
import services.GameService;
import services.PlayerService;
import services.SetupService;

public class TestCases extends SuperTest {

    public TestCases(String name) {
        super(name);
    }

    private static final String INSTANCE_ID = "$$$$";
    private static final boolean IS_PRIVATE = true;
    Connection connection;

    public void testRandomMemberCopyMultiEntry() throws SQLException {
        addPlayer(Hidden.TownRandom());
        addPlayer(Hidden.TownRandom());
        addPlayer(BasicRoles.Assassin());

        narrator.startGame();

        connection = new Connection();
        Setup setup = new Setup();
        setup.init(narrator, "");
        SetupService.create(connection, setup);
        long replayID = ReplayRepo.create(connection, INSTANCE_ID, IS_PRIVATE, 0, narrator.getSeed(), setup.id);

        StoryWriter sw = new StoryWriter(new StoryWriterError() {
            @Override
            public boolean onError(SQLException e) {
                System.err.println(e);
                return false;
            };
        }, narrator.getCommands());
        sw.setGameID(replayID, narrator._players.getDatabaseMap());

        ArrayList<SetupHiddenSchema> setupHiddens = SetupHiddenRepo.getBySetupID(connection,
                ReplayRepo.getSetupID(connection, sw.getReplayID()));
        assertEquals(3, setupHiddens.size());

        DBStoryPackage.deserialize(connection, sw.getReplayID());
        ReplayRepo.deleteByID(connection, sw.getReplayID());
        connection.close();

        SuperTest.skipTearDown = true;
    }

    public void testSetupSerialization() throws SQLException {
        connection = new Connection();
        for(int runCount = 0; runCount < 9; runCount++){
            UserRepo.deleteAll(connection);
            Narrator narrator = new Narrator();
            HashMap<Integer, PlayerList> n1Deaths = new HashMap<>();
            narrator.addListener(new PartialListener() {
                @Override
                public void onDayPhaseStart(PlayerList newDead) {
                    int dayNumber = narrator.getDayNumber();
                    n1Deaths.put(dayNumber, newDead);
                }
            });
            long seed = random.nextLong();
//             seed = Long.parseLong("6980092171723503853");

            narrator.setSeed(seed);
            Setup setup = getSetup(narrator, runCount, seed);
            narrator.setRule(SetupModifier.CHAT_ROLES, true);

            for(int i = 0; i < setup.getMaxPlayerCount(); i++)
                narrator.addPlayer(Computer.toLetter(i + 1));
            setup.applyRolesList();

            narrator.startGame();

            HashMap<String, Integer> charges = new HashMap<>();

            for(Player p: narrator.getAllPlayers()){
                for(Ability a: p.getAbilities())
                    charges.put(p.getID() + a.getName(), a.getPerceivedCharges());
            }

            StoryWriter sw = new StoryWriter(new StoryWriterError() {
                @Override
                public boolean onError(SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            }, narrator.getCommands());
            SetupService.create(connection, setup);
            long gameID = ReplayRepo.create(connection, INSTANCE_ID, IS_PRIVATE, 0, narrator.getSeed(), setup.id);
            sw.setGameID(gameID, narrator._players.getDatabaseMap());
            narrator.addListener(sw);

            PlayerService.savePlayersToGame(gameID, narrator, new HashMap<>(), connection);
            Brain.EndGame(narrator, seed);

            long story_id = sw.getReplayID();

            final StoryPackage sp = DBStoryPackage.deserialize(connection, sw.getReplayID());
            assertEquals(setup.getMaxPlayerCount(), sp.narrator.getPlayerCount());
            assertEquals(setup.getMaxPlayerCount(), sp.narrator.getRolesList().size());
            assertEquals(sp.narrator.getSeed(), narrator.getSeed());

            for(Hidden rm: sp.narrator.hiddens)
                assertEquals(1, rm.getAlignmentCount());

            sp.narrator.getRandom().log = new ArrayList<>();
            sp.narrator.addListener(new PartialListener() {
                @Override
                public void onDayPhaseStart(PlayerList newDead) {
                    if(newDead == null)
                        assertNull(n1Deaths.get(sp.narrator.getDayNumber()));
                    else{
                        PlayerList otherDeaths = n1Deaths.get(sp.narrator.getDayNumber());
                        assertEquals(otherDeaths.size(), newDead.size());
                    }
                }
            });
            sp.narrator.altRoleAssignment = StoryPackage.GetRoleAssigner(sp);
            sp.narrator.startGame();

            for(Player p: sp.narrator.getAllPlayers()){
                for(Ability a: p.getAbilities())
                    assertEquals((int) charges.get(p.getID() + a.getName()), a.getPerceivedCharges());
                assertEquals(p.isInvulnerable(), narrator.getPlayerByID(p.getID()).isInvulnerable());
                assertEquals(p.isDetectable(), narrator.getPlayerByID(p.getID()).isDetectable());
                assertEquals(p.isBlockable(), narrator.getPlayerByID(p.getID()).isBlockable());
            }

            Map<Long, Player> playerDBMap = sp.narrator._players.getDatabaseMap();
            CommandHandler ch = new CommandHandler(sp.narrator);

            for(Command command: sp.commands){
                try{
                    ch.parseCommand(playerDBMap.get(command.playerID), command.text);
                }catch(NarratorException e){
                    e.printStackTrace();
                    ReplayRepo.deleteByID(connection, sw.getReplayID());
                    connection.close();
                    fail(Long.toString(seed));
                }
            }

            StoryPackage sp2 = DBStoryPackage.deserialize(connection, story_id);
            GameService.delete(new Game(sp2));
            assertFalse(NodeSwitch.hasInternalErrors());

            ReplayRepo.deleteByID(connection, sw.getReplayID());
            sw.closeSQL();
        }
        connection.close();
        NodeSwitch.killDBConnections();
    }

    private Setup getSetup(Narrator narrator, int runCount, long seed) {
        if(runCount % 3 == 1){
            System.out.println("Game of Thrones Seed (" + (runCount + 1) + ") : " + seed);
            return new GoT(narrator);
        }else if(runCount % 3 == 0){
            System.out.println("Brave New World Seed (" + (runCount + 1) + ") : " + seed);
            return new BraveNewWorld(narrator);
        }else{
            System.out.println("Treasure Island Seed (" + (runCount + 1) + ") : " + seed);
            return new Pirates(narrator);
        }
    }

}
