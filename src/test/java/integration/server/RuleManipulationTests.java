package integration.server;

import java.sql.SQLException;

import game.logic.Narrator;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.setups.Classic;
import game.setups.GoT;
import game.setups.Setup;
import json.JSONException;
import json.JSONObject;
import nnode.NodeSwitch;
import util.UserFactory;

public class RuleManipulationTests extends InstanceTests {

    public RuleManipulationTests(String name) {
        super(name);
    }

    public void testDayManipulation() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        host.updateRule(SetupModifier.DAY_START, Narrator.DAY_START);
        assertEquals(Narrator.DAY_START, narrator.getBool(SetupModifier.DAY_START));

        host.updateRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        assertEquals(Narrator.NIGHT_START, narrator.getBool(SetupModifier.DAY_START));
    }

    public void testPlayerLimitBounds() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        JSONObject response;
        for(int i = 0; i < 16; i++){
            response = host.addSetupHidden(BasicRoles.Agent());
            assertEquals(0, response.getJSONArray("errors").length());
        }
        response = host.addSetupHidden(BasicRoles.Agent());
        assertEquals(1, response.getJSONArray("errors").length());
    }

    public void testPlayerModLimitBounds() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(UserFactory.PUBLIC, UserFactory.MOD);

        JSONObject response;
        for(int i = 0; i < 20; i++){
            response = host.addSetupHidden(BasicRoles.Agent());
            assertEquals(0, response.getJSONArray("errors").length());
        }
    }

    public void testChargeVariability() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(UserFactory.PUBLIC, UserFactory.MOD);
        host.setSetup(Classic.KEY);

        host.updateRule(SetupModifier.CHARGE_VARIABILITY, 100);

        assertCharge(new Classic(new Narrator()));

        host.setSetup(GoT.KEY);
        assertCharge(new GoT(new Narrator()));

        host.updateRule(SetupModifier.CHARGE_VARIABILITY, 100);
        assertCharge(new GoT(new Narrator()));

        host.setSetup(GoT.KEY);
        assertCharge(new GoT(new Narrator()));
    }

    private static void assertCharge(Setup s) {
        assertCharge(s.narrator.getInt(SetupModifier.CHARGE_VARIABILITY));
    }

    private static void assertCharge(int charge) {
        int last_index = NodeSwitch.instances.size() - 1;
        assertEquals(charge, NodeSwitch.instances.get(last_index).narrator.getInt(SetupModifier.CHARGE_VARIABILITY));
    }
}
