package integration.unit;

import junit.framework.TestCase;
import models.Command;

public class TestCommandModel extends TestCase {
    public void testEquals() {
        Command command1 = new Command(Long.valueOf(1), "command");
        Command command2 = new Command(Long.valueOf(1), "command");

        assertTrue(command1.equals(command2));
    }

    public void testNotEquals() {
        Command command1 = new Command(Long.valueOf(1), "command");
        Command command2 = new Command(Long.valueOf(2), "command");

        assertFalse(command1.equals(command2));
    }
}
