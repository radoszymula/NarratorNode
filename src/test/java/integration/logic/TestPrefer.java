package integration.logic;

import game.ai.Controller;
import game.logic.exceptions.UnknownRoleException;
import game.logic.support.CommandHandler;
import game.logic.templates.BasicRoles;
import game.roles.Amnesiac;
import game.roles.Citizen;
import game.roles.Goon;
import game.roles.Hidden;
import game.roles.SerialKiller;
import game.setups.Setup;
import services.SetupHiddenService;

public class TestPrefer extends SuperTest {

    public TestPrefer(String name) {
        super(name);
    }

    public void testBasicPrefer() {
        Controller p3 = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        p3.rolePrefer(new Goon().getClass().getSimpleName());

        dayStart();

        assertTrue(p3.is(Goon.class));
    }

    public void testSpecificInRandom() {
        Controller a = addPlayer("A");
        addPlayer("B");
        addPlayer("C");

        CommandHandler ch = new CommandHandler(narrator);

        a.rolePrefer(Citizen.class.getSimpleName());

        String preference = "prefer Citizen";

        addRole(BasicRoles.SerialKiller());
        addRole(BasicRoles.SerialKiller());
        addSetupHidden(Hidden.TownRandom());
        SetupHiddenService.CreateHostSpawn(narrator, Hidden.TownRandom(), BasicRoles.Citizen());

        ch.tryDoubleCommand(a, preference, a.getName());

        dayStart();

        assertTrue(a.is(Citizen.class));
    }

    public void testNeutralPrefering() {
        Controller a = addPlayer("A");
        addPlayer("B");
        addPlayer("C");

        CommandHandler ch = new CommandHandler(narrator);

        addSetupHidden(Hidden.TownRandom());
        addRole(BasicRoles.SerialKiller());
        addSetupHidden(Hidden.NeutralBenignRandom());

        SetupHiddenService.CreateHostSpawn(narrator, Hidden.NeutralBenignRandom(), BasicRoles.Amnesiac());

        String preference = "prefer Amnesiac";
        ch.tryDoubleCommand(a, preference, a.getName());

        dayStart();

        assertTrue(a.is(Amnesiac.class));

        skipTearDown = true;
    }

    public void testPrefer() {
        Controller a = addPlayer("A");
        Controller b = addPlayer("B");
        Controller c = addPlayer("C");

        CommandHandler ch = new CommandHandler(narrator);

        String preference = "prefer Citizen";

        try{
            ch.tryDoubleCommand(a, preference, a.getName());
            fail();
        }catch(UnknownRoleException e){
        }

        addRole(BasicRoles.Citizen());
        addSetupHidden(Hidden.YakuzaRandom());
        addRole(BasicRoles.SerialKiller());

        ch.tryDoubleCommand(a, preference, a.getName());
        ch.tryDoubleCommand(c, "prefer Serial Killer", b.getName());
        ch.tryDoubleCommand(b, "prEfer Yakuza RandoM", c.getName());

        dayStart();

        assertTrue(a.is(Citizen.class));
        assertEquals(Setup.YAKUZA_C, b.getColor());
        assertTrue(c.is(SerialKiller.class));
    }
}
