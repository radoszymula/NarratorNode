package integration.logic;

import game.ai.Computer;
import game.ai.Controller;
import game.event.ChatMessage;
import game.logic.Faction;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.CultLeader;
import game.roles.Infiltrator;
import game.roles.MasonLeader;
import game.roles.Sheriff;
import game.roles.support.CultInvitation;
import game.setups.Pirates;
import game.setups.Setup;
import services.FactionService;
import services.RoleAbilityModifierService;
import services.RoleService;
import util.LookupUtil;

public class TestInfiltrator extends SuperTest {

    public TestInfiltrator(String s) {
        super(s);
    }

    ChatMessage cm;
    Controller infil, maf, cLeader, cultist;

    @Override
    public void roleInit() {
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Role role = RoleService.createRole(narrator, "Infiltrator", Infiltrator.class);
        infil = addPlayer(FactionService.createFactionRole(mafia, role));
        maf = addPlayer(BasicRoles.Goon());
        cultist = addPlayer(BasicRoles.Cultist());
        cLeader = addPlayer(BasicRoles.CultLeader());
    }

    private void nightTalking() {
        nightStart();
        say(cLeader, "whatup", cultist.getColor());
    }

    private void infilStatTest() {
        endNight();

        assertEquals(maf.getColor(), infil.getColor());
        assertTrue(infil.getPlayer(narrator).getFactions().contains(cLeader.getPlayer(narrator).getFaction()));
        assertTrue(infil.is(Infiltrator.class));
    }

    public void testBasic() {
        nightTalking();

        setTarget(cLeader, infil);

        infilStatTest();
    }

    public void testPromotion() {
        editRule(SetupModifier.CULT_PROMOTION, true);
        editRule(SetupModifier.CONVERT_REFUSABLE, false);
        editRule(SetupModifier.CULT_KEEPS_ROLES, false);

        nightTalking();

        setTarget(cLeader, CultLeader.MAIN_ABILITY, Sheriff.class.getSimpleName(), null, NULL_OPT, infil);

        infilStatTest();
    }

    public void testRefusal() {
        editRule(SetupModifier.CULT_PROMOTION, false);
        editRule(SetupModifier.CONVERT_REFUSABLE, true);
        editRule(SetupModifier.CULT_KEEPS_ROLES, true);

        addPlayer(BasicRoles.Citizen());

        nightTalking();

        setTarget(cLeader, infil);

        infilStatTest();

        assertFalse(infil.getPlayer(narrator).hasDayAction(Ability.INVITATION_ABILITY));

        try{
            infil.doDayAction(CultInvitation.COMMAND, Constants.ACCEPT);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testClubber() {
        Controller clubber = addPlayer(BasicRoles.Clubber());
        nightTalking();

        setTarget(cLeader, infil);
        infilStatTest();

        skipDay();
        setTarget(clubber, infil);
        endNight();

        isAlive(infil);
    }

    public void testCultMason() {
        Role role = LookupUtil.findRole(narrator, "Cult Leader");
        Faction faction = narrator.getFaction(Setup.YAKUZA_C);

        Controller mLeader = addPlayer(BasicRoles.MasonLeader());
        Controller cLeader2 = addPlayer(FactionService.createFactionRole(faction, role));

        nightTalking();
        setTarget(cLeader, infil);
        setTarget(cLeader2, infil);
        setTarget(mLeader, infil);

        endNight();

        assertEquals(4, infil.getPlayer(narrator).getFactions().size());

        skipDay();

        assertContains(infil.getChatKeys(), infil.getColor(), cLeader.getColor(), cLeader2.getColor(),
                mLeader.getColor());
    }

    public void testCultThenMasonThenCult() {
        Role role = LookupUtil.findRole(narrator, "Cult Leader");
        Faction faction = narrator.getFaction(Setup.YAKUZA_C);

        Controller mLeader = addPlayer(BasicRoles.MasonLeader());
        Controller cLeader2 = addPlayer(FactionService.createFactionRole(faction, role));
        Controller disguiser = new TestDisguiser("").neutDisguiser();

        editRule(SetupModifier.MASON_PROMOTION, true);

        setTarget(cLeader, infil);
        nextNight();

        isAlive(mLeader);

        setTarget(mLeader, infil);
        nextNight();

        assertFalse(infil.is(MasonLeader.class));
        isAlive(mLeader);

        setTarget(cLeader2, infil);
        endNight();

        assertEquals(4, infil.getPlayer(narrator).getFactions().size());

        skipDay();

        assertContains(infil.getChatKeys(), infil.getColor(), cLeader.getColor(), mLeader.getColor());

        setTarget(disguiser, infil);
        nextNight();

        assertContains(disguiser.getChatKeys(), infil.getColor(), cLeader.getColor(), mLeader.getColor());

        isDead(infil);
    }

    public void testAttainKill() {
        Role infiltratorRole = LookupUtil.findRole(narrator, "Infiltrator");
        Role cultLeaderRole = LookupUtil.findRole(narrator, "Cult Leader");
        Faction yakuza = narrator.getFaction(Setup.YAKUZA_C);
        Faction threat = narrator.getFaction(Setup.THREAT_C);

        Controller cLeader2 = addPlayer(FactionService.createFactionRole(yakuza, cultLeaderRole));
        Controller infil2 = addPlayer(FactionService.createFactionRole(threat, infiltratorRole));

        setTarget(cLeader2, infil2);
        nextNight();

        mafKill(infil2, infil);
        endNight();

        isDead(infil);
    }

    public void testPirateSetup() {
        Setup s = new Pirates(narrator.removeAllFactionsAndRoles());

        for(int i = 1; i < 29; i++)
            this.addPlayer(Computer.toLetter(i));

        s.applyRolesList();

        dayStart();

        skipDay();

        Controller infil = null, cl = null;
        for(Controller p: narrator._players){
            if(p.is(Infiltrator.class))
                infil = p;
            else if(p.is(CultLeader.class))
                cl = p;
        }

        setTarget(cl, infil);
        endNight();

        assertTrue(infil.getPlayer(narrator).getFactions().contains(cl.getPlayer(narrator).getFaction()));
    }

    public void testNoBackToBackModifier() {
        try{
            Role role = LookupUtil.findRole(narrator, "Infiltrator");
            RoleAbilityModifierService.modify(role, Infiltrator.class, AbilityModifier.BACK_TO_BACK, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            Role role = LookupUtil.findRole(narrator, "Infiltrator");
            RoleAbilityModifierService.modify(role, Infiltrator.class, AbilityModifier.ZERO_WEIGHTED, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
