package integration.logic;

import java.util.ArrayList;

import game.logic.Faction;
import game.roles.Ability;
import game.roles.AbilityList;
import game.setups.Setup;

public class TestFactionKill extends SuperTest {

	public TestFactionKill(String s) {
		super(s);
	}

	public void testRuleTexts() {
		Faction faction = narrator.getFaction(Setup.MAFIA_C);
		AbilityList abilities = faction.getAbilities();
		
		for(Ability ability: abilities) {
			ArrayList<String> ruleTexts = ability.getPublicDescription(narrator, faction.getName(), Setup.MAFIA_C, faction._abilities);
			for(String text: ruleTexts)
				assertFalse(text.isEmpty());
		}
	}
}
