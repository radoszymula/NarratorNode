package integration.logic;

import java.util.ArrayList;

import game.ai.Controller;
import game.event.Feedback;
import game.logic.Faction;
import game.logic.Player;
import game.logic.Role;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Amnesiac;
import game.roles.CultLeader;
import game.roles.Enforcer;
import game.roles.Hidden;
import game.roles.Mason;
import game.roles.MasonLeader;
import game.roles.Vigilante;
import game.setups.Setup;
import services.FactionRoleService;
import services.FactionService;
import util.LookupUtil;
import util.RepeatabilityTest;

public class TestMason extends SuperTest {

    public TestMason(String name) {
        super(name);
    }

    public void testBasic() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Arsonist());

        setTarget(ml, cit);
        endNight();

        isMason(cit);

        skipDay();
        setTarget(ml, doc);
        endNight();

        assertNotMason(doc);
    }

    public void testCultKilling() {
        addPlayer(BasicRoles.Citizen());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller cult = addPlayer(BasicRoles.CultLeader());

        setTarget(cult, maf);
        nextNight();

        assertStatus(maf, CultLeader.MAIN_ABILITY);

        setTarget(ml, maf);
        endNight();

        isDead(maf);
    }

    public void testMasonPromotion() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifier.MASON_PROMOTION, true);

        setTarget(ml, cit);
        nextNight();

        assertTrue(cit.is(Mason.class));

        mafKill(maf, ml);
        endNight();

        assertTrue(cit.is(MasonLeader.class));
    }

    public void testMasonLeaderPromotionDay() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Assassin());

        setTarget(ml, cit);
        endNight();

        assertTrue(cit.is(Mason.class));
        doDayAction(assassin, ml);

        isDead(ml);
        assertTrue(cit.is(MasonLeader.class));
        skipDay();

        assertTrue(cit.is(MasonLeader.class));
    }

    private void promotionOff() {
        editRule(SetupModifier.MASON_PROMOTION, false);
    }

    private void promotionOn() {
        editRule(SetupModifier.MASON_PROMOTION, true);
    }

    public void testNoPromotion() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller mason = addPlayer(BasicRoles.Mason());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Witch());

        promotionOff();

        setTarget(sk, ml);
        endNight();

        assertTrue(mason.is(Mason.class));
    }

    public void testMasonChat() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());

        setTarget(ml, cit1);
        assertEquals(1, ml.getChatKeys().size());
        assertEquals(1, maf.getChatKeys().size());

        nextNight();
        assertEquals(2, ml.getChatKeys().size());
        assertEquals(2, cit1.getChatKeys().size());
        assertEquals(1, cit2.getChatKeys().size());
        assertTrue(cit1.getChatKeys().contains(cit1.getColor()));

        say(cit1, gibberish(), cit1.getColor());
        partialContains(ml, gibberish);
        partialContains(cit1, gibberish);
        partialExcludes(cit2, gibberish);

        setTarget(ml, cit2);
        nextNight();
    }

    public void testEnclosingChatError() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Mason());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller maf = addPlayer(BasicRoles.Goon());

        nightStart();
        say(ml, gibberish(), Setup.TOWN_C);

        mafKill(maf, ml);
        nextNight();
    }

    public void testJailStopMasonChatForming() {
        Controller kid = addPlayer(BasicRoles.Kidnapper());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller mason = addPlayer(BasicRoles.Mason());

        jail(kid, ml);
        skipDay();

        assertEquals(1, mason.getChatKeys().size());
        nextNight();

        assertEquals(2, ml.getChatKeys().size());
        assertEquals(2, mason.getChatKeys().size());
    }

    public void testNoRecruitPowerlessRoles() {
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifier.MASON_NON_CIT_RECRUIT, false);
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 1);

        shoot(vigi, fodder);
        nextNight();

        setTarget(ml, vigi);
        endNight();

        assertTrue(vigi.is(Vigilante.class));
    }

    public void testRecruitPowerlessRoles() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 1);

        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifier.MASON_NON_CIT_RECRUIT, true);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        shoot(vigi, fodder);
        nextNight();

        assertFalse(vigi.getPlayer(narrator).isPowerRole());

        setTarget(ml, vigi);
        endNight();

        assertTrue(vigi.is(Mason.class));
    }

    public void testNonFactionCitizens() {
        Role role = LookupUtil.findRole(narrator, "Citizen");
        Faction faction = narrator.getFaction(Setup.BENIGN_C);
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit = addPlayer(FactionService.createFactionRole(faction, role));
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifier.MASON_NON_CIT_RECRUIT, true);

        setTarget(ml, cit);
        nextNight();

        assertFalse(cit.is(Mason.class));

        setTarget(ml, sheriff);
        endNight();

        assertFalse(sheriff.is(Mason.class));
    }

    public void testAutoMasonPromotionNight() {
        Controller mason = addPlayer(BasicRoles.Mason());
        addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Arsonist());

        promotionOn();

        nightStart();
        assertTrue(mason.is(MasonLeader.class));
    }

    public void testAutoMasonPromotionDay() {
        Controller mason = addPlayer(BasicRoles.Mason());
        Controller a2 = addPlayer(BasicRoles.Arsonist());
        Controller a1 = addPlayer(BasicRoles.Arsonist());

        promotionOn();

        dayStart();
        assertTrue(mason.is(MasonLeader.class));

        voteOut(a1, mason, a2);
    }

    public void testSingleMasonFail() {
        addPlayer(BasicRoles.Mason());
        addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Arsonist());

        promotionOff();

        try{
            dayStart();
            fail();
        }catch(IllegalGameSettingsException e){
        }
    }

    public void testSingleRandomNoMasons() {
        for(int i = 0; i < 1000; i++){
            newNarrator();
            // long seed = Long.parseLong("1981111489021495537");
            long seed = random.nextLong();
            narrator.setSeed(seed);
            addPlayer(Hidden.TownGovernment());
            addPlayer(Hidden.TownGovernment());
            addPlayer(BasicRoles.Poisoner());
            addPlayer(BasicRoles.Poisoner());

            promotionOff();

            dayStart();

            int singleMasonSize = narrator.getAllPlayers().filter(Mason.class).size();
            int masonLeaderSize = narrator.getAllPlayers().filter(MasonLeader.class).size();
            if(singleMasonSize == 1 && masonLeaderSize == 0)
                fail(Long.toString(seed));

            RepeatabilityTest.runRepeatabilityTest();
            BrainEndGame = false;
        }
    }

    public void testCultVsMason() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cl = addPlayer(BasicRoles.CultLeader());

        setTarget(ml, cit);
        setTarget(cl, cit);
        endNight();

        isAlive(cit);
        assertEquals(ml.getColor(), cit.getColor());
        assertTrue(cit.is(Mason.class));
    }

    public void testUnrecruitableMasons() {
        Controller mason = addPlayer(BasicRoles.Mason());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cl = addPlayer(BasicRoles.CultLeader());

        setTarget(cl, ml);
        nextNight();

        assertEquals(mason.getColor(), ml.getColor());

        setTarget(cl, mason);
        nextNight();

        assertEquals(mason.getColor(), ml.getColor());

    }

    public void testNoOtherMasonTarget() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller mason = addPlayer(BasicRoles.Mason());
        addPlayer(BasicRoles.Arsonist());

        nightStart();

        try{
            setTarget(ml, mason);
            fail();
        }catch(PlayerTargetingException e){
        }

        assertTrue(mason.getPlayer(narrator).getAcceptableTargets(MasonLeader.MAIN_ABILITY).isEmpty());
    }

    public void testAmnesiacIn() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller mason = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller amn2 = addPlayer(BasicRoles.Amnesiac());
        Controller ars = addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifier.MASON_PROMOTION, true);

        setTarget(ml, mason);
        endNight();

        voteOut(mason, ml, cit2, amn, ars);
        endNight();

        voteOut(ml, ars, amn, cit2);

        try{
            setTarget(amn, ml);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(amn, mason);
        setTarget(amn2, mason);
        endNight();

        assertTrue(amn.is(MasonLeader.class) || amn.is(Mason.class));
        assertTrue(amn2.is(MasonLeader.class) || amn.is(Mason.class));

        assertNotSameAbilities(amn, amn2);
    }

    public void testClubber() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller clubber = addPlayer(BasicRoles.Clubber());
        Controller mason = addPlayer(BasicRoles.Mason());
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller cLeader = addPlayer(BasicRoles.CultLeader());

        promotionOn();

        nightStart();

        assertTrue(mason.getPlayer(narrator).initialAssignedRole.assignedRole.role.contains(Mason.class));
        assertNotMason(mason);
        assertStatus(cult, CultLeader.MAIN_ABILITY);

        setTarget(clubber, cult);
        setTarget(cLeader, cit);
        nextNight();

        assertStatus(cit, CultLeader.MAIN_ABILITY);
        isDead(cult);

        setTarget(clubber, cit);
        say(clubber, gibberish(), clubber.getColor());

        partialContains(mason, gibberish);
        nextNight();

        isDead(cit);

        setTarget(clubber, cLeader);
        endNight();

        assertGameOver();
        isDead(cLeader, cit, cult);
        isAlive(clubber, mason);
    }

    public void testEnforcer() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller mL = addPlayer(BasicRoles.MasonLeader());
        Controller enforcer = addPlayer(BasicRoles.Enforcer());
        Controller cLeader = addPlayer(BasicRoles.CultLeader());

        promotionOn();

        nightStart();

        setTarget(enforcer, cit);
        setTarget(cLeader, cit);
        say(enforcer, gibberish(), enforcer.getColor());

        partialContains(mL, gibberish);
        nextNight();

        assertStatus(cit, CultLeader.MAIN_ABILITY, false);

        setTarget(enforcer, cit);
        setTarget(mL, cit);
        endNight();

        assertFalse(cit.is(Mason.class));

        skipDay();
        setTarget(cLeader, cit);
        endNight();

        assertStatus(cit, CultLeader.MAIN_ABILITY);
    }

    public void testAmnesiacEnforcer() {
        Controller enforcer = addPlayer(BasicRoles.Enforcer());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller fodder = addPlayer(BasicRoles.Witch());
        Controller witch = addPlayer(BasicRoles.Witch());

        voteOut(fodder, witch, amn, enforcer);
        setTarget(enforcer, amn);
        setTarget(amn, fodder);
        endNight();

        assertTrue(amn.is(Amnesiac.class));
    }

    public void testEnforcerLearning() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller enforcer = addPlayer(BasicRoles.Enforcer());
        Controller cLeader = addPlayer(BasicRoles.CultLeader());

        editRule(SetupModifier.ENFORCER_LEARNS_NAMES, true);

        setTarget(enforcer, cit);
        setTarget(cLeader, cit);
        endNight();

        seen(enforcer, cLeader);
    }

    public static void seen(Controller enforcer_c, Controller recruiter_c) {
        Player recruiter = recruiter_c.getPlayer(narrator);
        Player enforcer = enforcer_c.getPlayer(narrator);
        ArrayList<Object> parts = Enforcer.FeedbackGenerator(recruiter);
        Feedback f = new Feedback(enforcer.getSkipper());
        f.add(parts);
        f.setVisibility(enforcer);
        String feedback = f.access(enforcer);
        f.removeVisiblity(enforcer);
        partialContains(enforcer, feedback);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Mason(), AbilityModifier.BACK_TO_BACK, Mason.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Mason(), AbilityModifier.ZERO_WEIGHTED, Mason.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    private void isMason(Controller p) {
        assertTrue(p.is(Mason.class));
    }

    private void assertNotMason(Controller p) {
        assertFalse(p.is(Mason.class));
    }

}
