package integration.logic;

import java.util.Collection;

import game.ai.Controller;
import game.event.ChatMessage;
import game.event.EventList;
import game.event.EventLog;
import game.event.Message;
import game.event.VoidChat;
import game.logic.Faction;
import game.logic.Role;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.logic.templates.HTMLDecoder;
import game.roles.Ability;
import game.roles.Hidden;
import game.setups.Setup;
import services.FactionService;
import util.LookupUtil;

public class TestTalking extends SuperTest {

    public TestTalking(String name) {
        super(name);
    }

    public void testNightChat() {

        Controller r = addPlayer(BasicRoles.Agent());
        Controller y = addPlayer(Hidden.YakuzaRandom());
        Controller t = addPlayer(Hidden.TownInvestigative());

        dayStart();

        Collection<String> colors = Hidden.TownInvestigative().getColors();
        assertEquals(1, colors.size());
        assertContains(colors, t.getColor());

        voteOut(r, t, y);
        assertNotNull(narrator.getEventManager().getEventLog(y.getColor()));
        say(y, "hi", y.getColor());

        EventList yakNightChat0 = narrator.getEventManager().getNightLog(y.getColor()).getEvents();
        assertEquals(1, yakNightChat0.size());
        ChatMessage cm = (ChatMessage) yakNightChat0.events.get(0);
        assertFalse(cm.isPublic());
        assertFalse(cm.hasAccess(t.getName()));
    }

    public void testEndNightSet() {
        Role role = LookupUtil.findRole(narrator, "Agent");
        Faction town = narrator.getFaction(Setup.TOWN_C);
        Controller consort = addPlayer(BasicRoles.Consort());
        Controller townAgent = addPlayer(FactionService.createFactionRole(town, role));
        addPlayer(BasicRoles.Witch());

        nightStart();

        setTarget(consort, townAgent, Ability.NIGHT_KILL);
        cancelAction(consort, 0);
        endNight(consort);
        try{
            setTarget(consort, townAgent, Ability.NIGHT_KILL);
        }catch(PlayerTargetingException e){
        }

        cancelEndNight(consort);
        setTarget(consort, townAgent, Ability.NIGHT_KILL);
    }

    public void testDeadTalking() {
        Controller killer = addPlayer(BasicRoles.Agent());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller y = addPlayer(Hidden.YakuzaRandom());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller fodder = addPlayer(Hidden.TownInvestigative());
        addPlayer(Hidden.YakuzaRandom());
        addPlayer(BasicRoles.BusDriver());

        // modifyRole(BasicRoles.Godfather(), Ability.INVULNERABILITY_MODIFIER, false);

        setTarget(sk, fodder);
        mafKill(killer, y);
        setTarget(bm, y);
        endNight();

        isDead(y, fodder);
        say(y, gibberish(), Constants.DEAD_CHAT);
        say(fodder, "not blackmailed and can talk", Constants.DEAD_CHAT);

        partialContains(y, gibberish);
        partialExcludes(killer, gibberish);
    }

    public void testVoteDescriptionLeak() {
        Controller a = addPlayer(BasicRoles.ElectroManiac());
        Controller b = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Arsonist());

        vote(a, b);

        endDay();

        Message m = narrator.getEventManager().getDayChat().getEvents().getLast();

        String lynchText = m.toString();
        assertEquals(-1, lynchText.indexOf("["));

        assertTrue(m.access(Message.PRIVATE, new HTMLDecoder()).contains(a.getRoleName()));
    }

    public void testPreGameTalk() {
        CommandHandler ch = new CommandHandler(narrator);
        ch.command(null, "say " + Constants.DAY_CHAT + " hi", "Joe");
    }

    public void testDeadTeamPublicChat() {
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller maf2 = addPlayer(BasicRoles.Chauffeur());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller pois = addPlayer(BasicRoles.Poisoner());

        nightStart();

        say(maf, "hi", maf.getColor());
        ChatMessage cm = (ChatMessage) narrator.getEventManager().getEventLog(maf.getColor()).getEvents().getLast();
        assertFalse(cm.isPublic());

        mafKill(maf, maf);
        endNight();
        voteOut(maf2, cit, sk, pois);

        isDead(maf, maf2);

        assertFalse(cm.isPublic());
    }

    public void testEndGameChat() {
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();
        mafKill(maf, sk);
        setTarget(sk, vigi);
        shoot(vigi, maf);
        endNight();

        assertGameOver();
        assertTrue(narrator.getEventManager().getDayChat().isActive());
        assertNotNull(narrator.getEventManager().getDayChat().getKey(sk.getPlayer(narrator)));

        say(sk, gibberish(), Constants.DAY_CHAT);

        partialContains(gibberish);

        String happenings = narrator.getHappenings();
        assertTrue(happenings.contains("Day 1"));
    }

    public void testVoidChat() {
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();
        say(maf, "Talking to myself", VoidChat.KEY);
        say(sk, "Talking to myself", VoidChat.KEY);
        say(vigi, "Talking to myself", VoidChat.KEY);
    }

    public void testFactionChatGameOver() {
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());

        dayStart();
        voteOut(doc, cit, maf);

        mafKill(maf, cit);
        endNight();

        for(EventLog el: maf.getPlayer(narrator).getChats()){
            assertNotNull(el);
        }
    }
}
