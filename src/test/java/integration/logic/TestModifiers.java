package integration.logic;

import java.util.ArrayList;

import game.ai.Controller;
import game.logic.Faction;
import game.logic.Player;
import game.logic.Role;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Agent;
import game.roles.Armorsmith;
import game.roles.Baker;
import game.roles.Blackmailer;
import game.roles.Bulletproof;
import game.roles.Burn;
import game.roles.Doctor;
import game.roles.Driver;
import game.roles.FactionKill;
import game.roles.FactionSend;
import game.roles.Gunsmith;
import game.roles.Infiltrator;
import game.roles.Sheriff;
import game.roles.TeamTakedown;
import game.roles.Witch;
import game.setups.Setup;
import models.FactionRole;
import services.FactionAbilityModifierService;
import services.FactionModifierService;
import services.FactionService;
import services.RoleAbilityModifierService;
import services.RoleModifierService;
import services.RoleService;
import services.SetupModifierService;
import util.LookupUtil;
import util.TestChatUtil;

public class TestModifiers extends SuperTest {

    public TestModifiers(String name) {
        super(name);
    }

    public void test0AutoVest() {
        modifyAbilityCooldown(BasicRoles.Sheriff(), Sheriff.class, 0);
        assertEquals(0, BasicRoles.Sheriff().role._abilities.getAbility(Sheriff.class).modifiers
                .getInt(AbilityModifier.COOLDOWN));

        // ignores negative value for cooldown
        try{
            modifyAbilityCooldown(BasicRoles.Sheriff(), Sheriff.class, SetupModifiers.UNLIMITED);
            fail();
        }catch(NarratorException e){
        }

        modifyAbilityCooldown(BasicRoles.Sheriff(), Sheriff.class, 3);
        assertEquals(3, BasicRoles.Sheriff().role._abilities.getAbility(Sheriff.class).modifiers
                .getInt(AbilityModifier.COOLDOWN));

        try{
            modifyAbilityCooldown(BasicRoles.Sheriff(), Sheriff.class, -5);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testRunningOutOfBread() {
        modifyAbilityCharges(BasicRoles.Baker(), Baker.class, 1);

        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        nightStart();

        assertPassableBreadCount(1, baker);
        // assertEquals(1, baker.getBread().size());

        nextNight();

        assertPassableBreadCount(1, baker);
        // assertEquals(1, baker.getBread().size());

        setTarget(baker, cit);
        nextNight();

        assertPassableBreadCount(0, baker);
        // assertEquals(0, baker.getBread().size());
    }

    public void test0ChargesFail() {
        try{
            modifyAbilityCharges(BasicRoles.Armorsmith(), Armorsmith.class, 0);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testFactionKillPlusRoleAbility() {
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        addPlayer(BasicRoles.Citizen());

        modifyTeamAbility(Setup.MAFIA_C, AbilityModifier.ZERO_WEIGHTED, FactionKill.class, true);

        mafKill(bm, gs);
        // assertEquals(0, bm.getActionWeight());
        assertTrue(bm.getPlayer(narrator).getActions().canAddAnotherAction(Blackmailer.MAIN_ABILITY));
        assertFalse(bm.getPlayer(narrator).getActions().canAddAnotherAction(FactionKill.MAIN_ABILITY));
        setTarget(bm, mayor);
        assertActionSize(2, bm);
        assertFalse(bm.getPlayer(narrator).getActions().canAddAnotherAction(Blackmailer.MAIN_ABILITY));

        cancelAction(bm, 0);
        assertActionSize(1, bm);
        assertTrue(bm.getPlayer(narrator).getActions().canAddAnotherAction(Blackmailer.MAIN_ABILITY));
        assertFalse(bm.getPlayer(narrator).getActions().canAddAnotherAction(FactionKill.MAIN_ABILITY));

        setTarget(bm, mayor);
        cancelAction(bm, 1);
        assertTrue(bm.getPlayer(narrator).getActions().freeActions.isEmpty());
        assertFalse(bm.getPlayer(narrator).getActions().canAddAnotherAction(Blackmailer.MAIN_ABILITY));
        assertTrue(bm.getPlayer(narrator).getActions().canAddAnotherAction(FactionKill.MAIN_ABILITY));

        mafKill(bm, gs);
        assertActionSize(2, bm);

        cancelAction(bm, 0);
        cancelAction(bm, 0);

        setTarget(bm, mayor);
        // assertEquals(1, bm.getActionWeight());
        assertFalse(bm.getPlayer(narrator).getActions().canAddAnotherAction(Blackmailer.MAIN_ABILITY));
        assertTrue(bm.getPlayer(narrator).getActions().canAddAnotherAction(FactionKill.MAIN_ABILITY));
        mafKill(bm, gs);
        assertActionSize(2, bm);
        assertFalse(bm.getPlayer(narrator).getActions().canAddAnotherAction(Blackmailer.MAIN_ABILITY));
        assertFalse(bm.getPlayer(narrator).getActions().canAddAnotherAction(FactionKill.MAIN_ABILITY));

        mafKill(bm, gs);
        setTarget(bm, mayor);

        endNight();

        isDead(gs);
        assertStatus(mayor, Blackmailer.MAIN_ABILITY);
    }

    public void testTripleKill() {
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller baker1 = addPlayer(BasicRoles.Baker());
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller agent = addPlayer(BasicRoles.Agent());

        modifyTeamAbility(Setup.MAFIA_C, AbilityModifier.ZERO_WEIGHTED, FactionKill.class, true);

        setTarget(baker1, agent);
        setTarget(baker2, agent);
        nextNight();

        mafKill(agent, baker1);
        assertTrue(agent.getPlayer(narrator).getActions().canAddAnotherAction(FactionKill.MAIN_ABILITY));
        assertTrue(agent.getPlayer(narrator).getActions().canAddAnotherAction(Agent.MAIN_ABILITY));
        mafKill(agent, baker2);
        assertTrue(agent.getPlayer(narrator).getActions().canAddAnotherAction(FactionKill.MAIN_ABILITY));
        assertTrue(agent.getPlayer(narrator).getActions().canAddAnotherAction(Agent.MAIN_ABILITY));
        mafKill(agent, fodder);
        assertFalse(agent.getPlayer(narrator).getActions().canAddAnotherAction(FactionKill.MAIN_ABILITY));
        assertTrue(agent.getPlayer(narrator).getActions().canAddAnotherAction(Agent.MAIN_ABILITY));
        setTarget(agent, agent);
        assertFalse(agent.getPlayer(narrator).getActions().canAddAnotherAction(FactionKill.MAIN_ABILITY));
        assertFalse(agent.getPlayer(narrator).getActions().canAddAnotherAction(Agent.MAIN_ABILITY));
        assertActionSize(4, agent);
    }

    public void testSelfTargetingBasic() {
        Faction town = narrator.getFaction(Setup.TOWN_C);
        Role role = RoleService.createRole(narrator, "Gashmitch", Gunsmith.class, Armorsmith.class);
        FactionRole mash = FactionService.createFactionRole(town, role);

        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller gsbs = addPlayer(mash);
        addPlayer(BasicRoles.Arsonist());

        modifyAbilitySelfTarget(BasicRoles.Gunsmith(), true);
        modifyAbilitySelfTarget(mash, Armorsmith.class, true);

        try{
            modifyAbilitySelfTarget(BasicRoles.Arsonist(), Burn.class, true);
            fail();
        }catch(NarratorException e){
        }

        try{
            modifyTeamAbility(Setup.MAFIA_C, AbilityModifier.SELF_TARGET, FactionSend.class, false);
            fail();
        }catch(NarratorException e){
        }

        nightStart();

        ArrayList<String> gsRoleSpecs = gs.getPlayer(narrator).getRoleSpecs();
        assertContains(gsRoleSpecs, "You may target yourself.");

        ArrayList<String> gsRuleTexts = BasicRoles.Gunsmith().getPublicDescription();
        assertContains(gsRuleTexts, "May target themselves.");

        ArrayList<String> gsBsRoleSpecs = gsbs.getPlayer(narrator).getRoleSpecs();
        assertContains(gsBsRoleSpecs, "You may target yourself with the armorsmith ability.");

        ArrayList<String> gsBsRuleTexts = gsbs.getPlayer(narrator).initialAssignedRole.assignedRole
                .getPublicDescription();
        assertContains(gsBsRuleTexts, "May target themselves with the armorsmith ability.");

        ArrayList<String> arsRoleSpecs = gsbs.getPlayer(narrator).getRoleSpecs();
        partialExcludes(arsRoleSpecs, "You may not target yourself with the burn ability.");

        ArrayList<String> arsRuleTexts = mash.getPublicDescription();
        partialExcludes(arsRuleTexts, "May target themselves with the douse ability.");
        partialExcludes(arsRuleTexts, "May target themselves with the burn ability.");
        partialExcludes(arsRuleTexts, "May target themselves with the undouse ability.");

        setTarget(gs, gs);
        endNight();

        assertRealGunCount(1, gs);
    }

    public void testBadSelfTargets() {
        Role infiltratorRole = RoleService.createRole(narrator, "Infiltrator", Infiltrator.class);
        Role teamTakedownRole = RoleService.createRole(narrator, "TeamTakedowner", TeamTakedown.class);
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        FactionRole infiltrator = FactionService.createFactionRole(mafia, infiltratorRole);
        FactionRole teamTakedowner = FactionService.createFactionRole(mafia, teamTakedownRole);
        tryBadSelfTarget(BasicRoles.Amnesiac(), BasicRoles.Assassin(), BasicRoles.Bomb(), BasicRoles.Bulletproof(),
                BasicRoles.Citizen(), BasicRoles.Commuter(), BasicRoles.Commuter(), BasicRoles.Coward(),
                BasicRoles.Coroner(), BasicRoles.Cultist(), BasicRoles.Doctor(), BasicRoles.Executioner(),
                BasicRoles.Ghost(), BasicRoles.GraveDigger(), infiltrator, BasicRoles.Jailor(), BasicRoles.Marshall(),
                BasicRoles.Mason(), BasicRoles.Mayor(), BasicRoles.Miller(), BasicRoles.Sleepwalker(),
                BasicRoles.Survivor(), teamTakedowner, BasicRoles.Veteran(), BasicRoles.Vigilante());

        tryBadSelfTarget(Burn.class, BasicRoles.Arsonist());
    }

    private void tryBadSelfTarget(FactionRole... members) {
        Class<? extends Ability> toEdit;
        for(FactionRole m: members){
            toEdit = m.role.getBaseAbility().getFirst().getClass();
            tryBadSelfTarget(toEdit, m);
        }
    }

    private void tryBadSelfTarget(Class<? extends Ability> toEdit, FactionRole factionRole) {
        try{
            if(LookupUtil.findFactionRole(narrator, factionRole.role.getName(), factionRole.getColor()) == null)
                addPlayer(factionRole);

            modifyAbilitySelfTarget(factionRole, toEdit, true);
            fail(toEdit.getSimpleName() + " was able to be edited, but shouldn't be.");
        }catch(NarratorException e){
        }
        try{
            modifyAbilitySelfTarget(factionRole, toEdit, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testSelfTargetGSWarning() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller bd = addPlayer(BasicRoles.BusDriver());

        modifyAbilitySelfTarget(BasicRoles.Gunsmith(), true);
        modifyAbilitySelfTarget(BasicRoles.BusDriver(), false);

        nightStart();

        Driver roleCard = bd.getPlayer(narrator).getAbility(Driver.class);
        assertFalse(roleCard.modifiers.getBoolean(AbilityModifier.SELF_TARGET));
        assertEquals("You may not target yourself.", roleCard.getSelfTargetText());

        ArrayList<String> rTexts = BasicRoles.BusDriver().getPublicDescription();
        assertContains(rTexts, "May not target themselves.");

        witch(witch, gs, gs);
        nextNight();

        assertTotalGunCount(1, gs);

        witch(witch, gs, gs);
        setTarget(gs, gs);

        TestMafiaTeam.gotWarning(gs);
        TestChatUtil.resetWarnings();

        nextNight();

        assertTotalGunCount(2, gs);

        setTarget(gs, gs);
        TestMafiaTeam.gotWarning(gs);
        endNight();

        assertTotalGunCount(3, gs);
    }

    public void testBadModifierTypes() {
        try{
            RoleModifierService.modify(narrator, BasicRoles.Doctor().role, RoleModifier.AUTO_VEST, true);
            fail();
        }catch(IllegalGameSettingsException e){
        }

        try{
            RoleModifierService.modify(narrator, BasicRoles.Doctor().role, RoleModifier.UNDETECTABLE, 1);
            fail();
        }catch(IllegalGameSettingsException e){
        }

        try{
            RoleAbilityModifierService.modify(BasicRoles.Doctor().role, Doctor.class, AbilityModifier.CHARGES, true);
            fail();
        }catch(IllegalGameSettingsException e){
        }

        try{
            RoleAbilityModifierService.modify(BasicRoles.Doctor().role, Doctor.class, AbilityModifier.SELF_TARGET, 3);
            fail();
        }catch(IllegalGameSettingsException e){
        }

        try{
            SetupModifierService.modify(narrator, SetupModifier.DAY_START, 1);
            fail();
        }catch(IllegalGameSettingsException e){
        }

        try{
            SetupModifierService.modify(narrator, SetupModifier.DAY_LENGTH, true);
            fail();
        }catch(IllegalGameSettingsException e){
        }

        try{
            FactionModifierService.modify(narrator.getFaction(Setup.TOWN_C), FactionModifier.KNOWS_ALLIES, 3);
            fail();
        }catch(IllegalGameSettingsException e){
        }

        try{
            FactionModifierService.modify(narrator.getFaction(Setup.TOWN_C), FactionModifier.WIN_PRIORITY, true);
            fail();
        }catch(IllegalGameSettingsException e){
        }

        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        try{
            FactionAbilityModifierService.modify(mafia, FactionKill.class, AbilityModifier.COOLDOWN, true);
            fail();
        }catch(IllegalGameSettingsException e){
        }

        try{
            FactionAbilityModifierService.modify(mafia, FactionKill.class, AbilityModifier.ZERO_WEIGHTED, 3);
            fail();
        }catch(IllegalGameSettingsException e){
        }
    }

    public void testModifierText() {
        Controller arson = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.BusDriver());
        addPlayer(BasicRoles.Commuter());
        addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Ghost());

        dayStart();

        ArrayList<String> arsonRoleSpecs = ((Player) arson).getRoleSpecs();
        partialExcludes(arsonRoleSpecs, "target yourself with the burn ability");
        partialExcludes(arsonRoleSpecs, Bulletproof.NIGHT_ACTION_DESCRIPTION);
    }

    public void testWitchSelfTargeting() {
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Role role = RoleService.createRole(narrator, "WitchDoctor", Witch.class, Doctor.class);
        FactionRole witchDoctor = FactionService.createFactionRole(mafia, role);

        Controller mash = addPlayer(witchDoctor);
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Citizen(), 2);

        modifyAbilitySelfTarget(BasicRoles.Witch(), false);
        modifyAbilitySelfTarget(witchDoctor, Witch.class, false);

        nightStart();

        Ability roleCard = witch.getPlayer(narrator).getFirstAbility();
        assertEquals(false, roleCard.modifiers.getBoolean(AbilityModifier.SELF_TARGET));

        ArrayList<String> specs = roleCard.getRoleSpecs(witch.getPlayer(narrator));
        assertContains(specs, "You may not cause self-targets.");

        ArrayList<String> rTexts = BasicRoles.Witch().getPublicDescription();
        assertContains(rTexts, "May not cause self-targets.");

        specs = mash.getPlayer(narrator).getRoleSpecs();
        assertContains(specs, "You may not use your Witch ability to cause self-targets.");

        rTexts = mash.getPlayer(narrator).initialAssignedRole.assignedRole.getPublicDescription();
        assertContains(rTexts, "May not cause self-targets with the Witch ability.");

        try{
            witch(witch, mash, mash);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testHiddenFlip() {
        Controller flipper = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Sheriff(), 2);

        modifyRole(BasicRoles.Citizen(), RoleModifier.HIDDEN_FLIP, true);

        setTarget(sk, flipper);

        endNight();

        assertEquals(Constants.A_CLEANED, flipper.getPlayer(narrator).getGraveyardColor());
        assertNull(flipper.getPlayer(narrator).getGraveyardRoleName());
    }

    public void testModifiersAreIntOrBool() {
        for(SetupModifier setupModifier: SetupModifier.values())
            assertTrue(Ability.IsIntModifier(setupModifier) || Ability.IsBoolModifier(setupModifier)
                    || !SetupModifiers.IsRoleRule(setupModifier));
        for(AbilityModifier abilityModifier: AbilityModifier.values())
            assertTrue(Ability.IsIntModifier(abilityModifier) || Ability.IsBoolModifier(abilityModifier));
        for(FactionModifier factionModifier: FactionModifier.values())
            assertTrue(Ability.IsIntModifier(factionModifier) || Ability.IsBoolModifier(factionModifier));
        for(RoleModifier roleModifier: RoleModifier.values())
            assertTrue(Ability.IsIntModifier(roleModifier) || Ability.IsBoolModifier(roleModifier));
    }
}
