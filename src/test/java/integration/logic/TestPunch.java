package integration.logic;

import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.FactionKill;
import game.setups.Setup;

public class TestPunch extends SuperTest {

    public TestPunch(String name) {
        super(name);
    }

    public void testBasic() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller jester = addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.SerialKiller());

        punchHit(Setup.TOWN_C, 100);
        punchHit(Setup.BENIGN_C, 0);

        dayStart();

        assertFalse(jester.getPlayer(narrator).getDayAbilities().get(0).getAcceptableTargets(jester.getPlayer(narrator))
                .isEmpty());

        assertTrue(jester.getPlayer(narrator).hasDayAction(Constants.PUNCH));

        punch(jester, cit);
        isAlive(cit);

        punch(cit, cit2);
        isDead(cit2);
    }

    public void testUnallowed() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());

        punchHit(Setup.TOWN_C, 100);
        punchHit(Setup.BENIGN_C, 0);
        punchHit(Setup.MAFIA_C, 0);
        
        try {
        	punchHit(Setup.TOWN_C, 101);
        	fail();
        }catch(NarratorException e) {}
        try {
        	punchHit(Setup.BENIGN_C, -1);
        	fail();
        }catch(NarratorException e) {}

        assertEquals(100, narrator.getFaction(Setup.TOWN_C).getPunchSuccess());
        assertEquals(0, narrator.getFaction(Setup.BENIGN_C).getPunchSuccess());

        mafKill(maf, cit2);
        badPunch(cit2, maf);// nighttime targeting
        setTarget(bm, sk);
        setTarget(vent, bm);
        endNight();

        badPunch(cit, (Controller) null);
        badPunch(null, cit);
        badPunch(cit2, cit);
        badPunch(cit, cit2);
        badPunch(cit, cit2, maf);
        badPunch(cit, narrator.skipper);

        // double
        punch(maf, cit);
        badPunch(maf, cit);

        badPunch(sk, cit);// bmed
        badPunch(bm, vent);// puppetted
    }

    public void testNoPunch() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifier.PUNCH_ALLOWED, false);
        dayStart();

        badPunch(maf, cit);
        badPunch(cit, maf);
    }

    public void testElectro() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        punchHit(Setup.TOWN_C, 100);

        electrify(em, cit, cit2);
        endNight();

        punch(cit, cit2);
        isDead(cit, cit2);
        isWinner(em);
        assertGameOver();
    }

    public void testPunchPuppet() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        addPlayer(BasicRoles.Ventriloquist());

        punchHit(Setup.TOWN_C, 100);

        assertFalse(narrator.getFaction(Setup.TOWN_C).hasAbility(FactionKill.MAIN_ABILITY));

        setTarget(vent, cit);
        endNight();

        ventPunch(cit, cit2, vent);
        isDead(cit2);
    }

    public void testParity() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller maf1 = addPlayer(BasicRoles.Goon());
        Controller maf2 = addPlayer(BasicRoles.Goon());
        Controller maf3 = addPlayer(BasicRoles.Goon());

        punchHit(Setup.MAFIA_C, 100);

        dayStart();
        assertIsDay();

        punch(cit2, cit3);
        punch(cit3, cit2);

        assertContains(cit1.getCommands(), Constants.PUNCH);
        assertFalse(cit2.getCommands().contains(Constants.PUNCH));

        punch(maf3, cit1);
        punch(maf1, maf2);
        assertIsNight();
    }

    public void testCanPunchOnDay2() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller maf1 = addPlayer(BasicRoles.Goon());

        punchHit(Setup.MAFIA_C, 0);

        punch(maf1, cit2);

        skipDay();
        endNight();

        punch(maf1, cit1);
    }

    // puppeted punching

    private void punchHit(String townC, int i) {
        editRule(SetupModifier.PUNCH_ALLOWED, true);
        setTeamRule(townC, FactionModifier.PUNCH_SUCCESS, i);
    }

    private void badPunch(Controller p1, Controller... p2) {
        try{
            punch(p1, p2);
            fail();
        }catch(NarratorException | NullPointerException e){
        }
    }

    // discord on/off
    //
}
