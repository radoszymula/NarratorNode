package integration.logic;

import game.ai.Controller;
import game.logic.Faction;
import game.logic.Player;
import game.logic.Role;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Armorsmith;
import game.roles.Blackmailer;
import game.roles.Coroner;
import game.roles.CultLeader;
import game.roles.Cultist;
import game.roles.Detective;
import game.roles.Doctor;
import game.roles.FactionKill;
import game.roles.Hidden;
import game.roles.Investigator;
import game.roles.Lookout;
import game.roles.Sheriff;
import game.roles.support.CultInvitation;
import game.setups.Setup;
import models.FactionRole;
import services.FactionRoleService;
import services.FactionService;
import util.LookupUtil;

public class TestCult extends SuperTest {

    public TestCult(String name) {
        super(name);
    }

    public void testOpposingTeam() {
        addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Cultist());
        addPlayer(BasicRoles.Cultist());

        editRule(SetupModifier.CULT_PROMOTION, false);

        assertBadGameSettings();

        addPlayer(BasicRoles.Citizen());
        nightStart();
    }

    public void testNoChargeKick() {
        modifyRole(BasicRoles.Citizen(), RoleModifier.UNCONVERTABLE, true);
        modifyAbilityCharges(BasicRoles.CultLeader(), CultLeader.class, 1);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.SerialKiller());

        setTarget(cl, cit);
        assertRealChargeRemaining(1, cl);
        endNight();

        assertStatus(cit, CultLeader.MAIN_ABILITY, false);
        assertRealChargeRemaining(1, cl);
    }

    public void testBasicAction() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Cultist());

        setTarget(cl, cit);

        endNight();

        assertGameOver();
    }

    public void testKeepRoleAndChat() {
        Controller sher = addPlayer(BasicRoles.Sheriff());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.CULT_KEEPS_ROLES, true);
        editRule(SetupModifier.CULT_PROMOTION, false);

        setTarget(cl, sher);

        endNight();

        assertTrue(sher.getPlayer(narrator).hasAbility(Sheriff.MAIN_ABILITY));

        // andChat portion
        skipDay();

        partialContains(sher.getChatKeys(), cl.getColor());
    }

    public void testChangeRole() {
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller doc = addPlayer(BasicRoles.Doctor());

        editRule(SetupModifier.CULT_KEEPS_ROLES, false);
        editRule(SetupModifier.CULT_PROMOTION, false);

        setTarget(cl, doc);

        endNight();

        assertFalse(doc.getPlayer(narrator).hasAbility(Doctor.MAIN_ABILITY));
        isAlive(cult);
    }

    private void cooldown(int cooldown) {
        modifyAbilityCooldown(BasicRoles.CultLeader(), cooldown);
    }

    public void testCultConversionCooldown() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cL = addPlayer(BasicRoles.CultLeader());

        int cooldown = 3;
        cooldown(3);
        editRule(SetupModifier.CULT_PROMOTION, false);

        setTarget(cL, doc);

        nextNight();

        for(int i = 0; i < cooldown; i++){
            try{
                setTarget(cL, cit);
                fail();
            }catch(PlayerTargetingException e){
            }
            nextNight();
        }

        setTarget(cL, cit);
    }

    public void testPowerRoleConversionCooldown() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cL = addPlayer(BasicRoles.CultLeader());

        int cd = 5;
        editRule(SetupModifier.CULT_POWER_ROLE_CD, cd);
        cooldown(0);
        editRule(SetupModifier.CULT_PROMOTION, false);

        setTarget(cL, cit2);
        nextNight();

        setTarget(cL, doc);
        nextNight();

        for(int i = 0; i < cd; i++){
            try{
                setTarget(cL, cit);
                fail();
            }catch(PlayerTargetingException e){
            }
            nextNight();
        }

        setTarget(cL, cit);
    }

    public void testMultipleConversions() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller cit4 = addPlayer(BasicRoles.Citizen());
        Controller cL = addPlayer(BasicRoles.CultLeader());

        cooldown(0);
        editRule(SetupModifier.CULT_PROMOTION, false);

        setTarget(cL, cit1);
        endNight();

        skipDay();

        setTarget(cL, cit2);
        endNight();

        voteOut(cit4, cit1, cit2, cit3);
    }

    public void testMayorCult() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller cL = addPlayer(BasicRoles.CultLeader());

        editRule(SetupModifier.CULT_KEEPS_ROLES, true);
        editRule(SetupModifier.CULT_PROMOTION, false);

        setTarget(cL, mayor);
        endNight();

        reveal(mayor);
        voteOut(cit, mayor);

        assertGameOver();
    }

    public void testMafTeamUnrecruitable() {
        Controller cL = addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());

        narrator.getFaction(BasicRoles.Goon().getColor()).setCanRecruitFrom(false);

        editRule(SetupModifier.CULT_PROMOTION, false);

        setTarget(cL, maf);

        endNight();

        assertEquals(BasicRoles.Goon().getColor(), maf.getColor());
    }

    public void testCultRuleText() {
        editRule(SetupModifier.CONVERT_REFUSABLE, true);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);
        FactionRole cultLeader = BasicRoles.CultLeader();
        assertContains(new CultLeader().getPublicDescription(narrator, cultLeader.getName(), cultLeader.getColor(),
                cultLeader.role._abilities), SetupModifier.CONVERT_REFUSABLE.getActiveFormat());
    }

    public void testKeepSendingPower() {
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller cult = addPlayer(BasicRoles.CultLeader());

        editRule(SetupModifier.CULT_PROMOTION, false);

        nightStart();

        setTarget(cult, maf);
        endNight();

        assertEquals(cult.getColor(), maf.getColor());
        skipDay();

        assertTrue(maf.getCommands().contains(FactionKill.COMMAND));

        assertTrue(maf.getPlayer(narrator)
                .isAcceptableTarget(maf.getPlayer(narrator).action(bm.getPlayer(narrator), Ability.NIGHT_KILL)));

        mafKill(maf, bm);
        endNight();

        isDead(bm);
    }

    public void testAllies() {
        Player chf = addPlayer(BasicRoles.Chauffeur()).getPlayer(narrator);
        Player bm = addPlayer(BasicRoles.Blackmailer()).getPlayer(narrator);
        Player cult = addPlayer(BasicRoles.CultLeader()).getPlayer(narrator);

        editRule(SetupModifier.CULT_KEEPS_ROLES, false);
        editRule(SetupModifier.CULT_PROMOTION, false);

        setTarget(cult, bm);
        endNight();

        assertTrue(chf.alliesWith(bm));
        assertFalse(chf.alliesWith(cult));

        assertTrue(bm.alliesWith(cult));
        assertFalse(bm.alliesWith(chf));

        assertTrue(cult.alliesWith(bm));
        assertFalse(cult.alliesWith(chf));
    }

    public void testRefuseCultConversion() {
        Controller chf = addPlayer(BasicRoles.Chauffeur());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller cult = addPlayer(BasicRoles.CultLeader());

        editRule(SetupModifier.CONVERT_REFUSABLE, true);
        editRule(SetupModifier.CULT_PROMOTION, false);

        setTarget(cult, bm);
        endNight();

        assertEquals(chf.getColor(), bm.getColor());

        assertFalse(bm.getPlayer(narrator).hasDayAction(Blackmailer.MAIN_ABILITY));
        assertTrue(bm.getPlayer(narrator).hasDayAction(Ability.INVITATION_ABILITY));

        skipDay();

        setTarget(cult, bm);
        endNight();

        assertEquals(chf.getColor(), bm.getColor());

        doDayAction(bm, Ability.INVITATION_ABILITY, Constants.ACCEPT);
        assertEquals(chf.getColor(), bm.getColor());

        skipDay();
        assertEquals(cult.getColor(), bm.getColor());
    }

    public void testVentRefuse() {
        addPlayer(BasicRoles.Vigilante(), 2);
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller cl = addPlayer(BasicRoles.CultLeader());

        editRule(SetupModifier.CULT_PROMOTION, false);
        editRule(SetupModifier.CONVERT_REFUSABLE, true);

        setTarget(cl, vent);
        setTarget(vent, cl);
        endNight();

        doDayAction(vent, CultInvitation.MAIN_ABILITY, Constants.ACCEPT);
        skipDay();

        assertEquals(vent.getColor(), cl.getColor());
    }

    public void testrefuseCultCommandHandler() {
        Controller chf = addPlayer(BasicRoles.Chauffeur());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller cult = addPlayer(BasicRoles.CultLeader());

        editRule(SetupModifier.CULT_PROMOTION, false);
        editRule(SetupModifier.CONVERT_REFUSABLE, true);

        setTarget(cult, bm);

        endNight();

        assertEquals(chf.getColor(), bm.getColor());

        command(bm, Constants.DECLINE);

        assertFalse(bm.getPlayer(narrator).hasDayAction(Blackmailer.MAIN_ABILITY));
        assertTrue(bm.getPlayer(narrator).hasDayAction(Ability.INVITATION_ABILITY));

        skipDay();

        setTarget(cult, bm);
        endNight();

        assertEquals(chf.getColor(), bm.getColor());

        command(bm, Constants.ACCEPT);
        assertEquals(chf.getColor(), bm.getColor());

        skipDay();
        assertEquals(cult.getColor(), bm.getColor());
    }

    public void testCultNotKnowingTeammates() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Framer());
        addPlayer(BasicRoles.Framer());
        addPlayer(Hidden.AnyRandom());

        editRule(SetupModifier.CULT_PROMOTION, false);

        setTeamRule(BasicRoles.CultLeader().getColor(), FactionModifier.KNOWS_ALLIES, false);

        try{
            dayStart();
            fail();
        }catch(IllegalGameSettingsException e){
        }
    }

    public void testCultLeaderSameTarget() {
        Role role = LookupUtil.findRole(narrator, "Cult Leader");
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Controller cl1 = addPlayer(BasicRoles.CultLeader());
        Controller cl2 = addPlayer(FactionService.createFactionRole(mafia, role));
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.CULT_PROMOTION, false);

        setTarget(cl1, cit);
        setTarget(cl2, cit);

        endNight();

        assertEquals(Setup.TOWN_C, cit.getColor());
    }

    public void testDoubleCultLeader() {
        addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.CULT_PROMOTION, false);

        try{
            dayStart();
            fail();
        }catch(IllegalRoleCombinationException | IllegalGameSettingsException e){
        }
    }

    public void testSheriffDetection() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller cl = addPlayer(BasicRoles.CultLeader());

        editRule(SetupModifier.CULT_PROMOTION, false);

        setTarget(cl, sheriff);
        setTarget(sheriff, maf);
        endNight();

        partialContains(sheriff, maf.getPlayer(narrator).getFaction().getName());

        skipDay();

        setTarget(sheriff, maf);
        endNight();

        partialContains(sheriff, maf.getPlayer(narrator).getFaction().getName(), 2);
    }

    public void testOptionWhenNoPowerup() {
        addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cult = addPlayer(BasicRoles.CultLeader());

        editRule(SetupModifier.CULT_PROMOTION, false);

        try{
            promote(cult, cit, Sheriff.class);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testAliasCultPromotionOn() {
        Controller cult = addPlayer(BasicRoles.CultLeader());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.CULT_PROMOTION, false);
        editRule(SetupModifier.CONVERT_REFUSABLE, false);
        editRule(SetupModifier.CULT_KEEPS_ROLES, true);
        cooldown(0);

        String alias = "Cannibal";

        narrator.setAlias(CultLeader.MINION_ALIAS, alias);

        setTarget(cult, cit2);
        endNight();

        assertEquals(alias, cit2.getRoleName());
    }

    public void testAliasNight() {
        Controller cult = addPlayer(BasicRoles.CultLeader());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.CULT_PROMOTION, true);
        editRule(SetupModifier.CONVERT_REFUSABLE, false);
        editRule(SetupModifier.CULT_KEEPS_ROLES, false);
        cooldown(0);

        String alias = "Cannibal";

        narrator.setAlias(CultLeader.MINION_ALIAS, alias);

        promote(cult, cit2, Cultist.class);
    }

    public void testCultPromotion() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller coroner = addPlayer(BasicRoles.Coroner());
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller doctor2 = addPlayer(BasicRoles.Doctor());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller detect2 = addPlayer(BasicRoles.Detective());
        Controller cult = addPlayer(BasicRoles.CultLeader());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller amnes = addPlayer(BasicRoles.Amnesiac());
        Controller jester = addPlayer(BasicRoles.Jester());
        Controller jester2 = addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifier.CULT_PROMOTION, true);
        editRule(SetupModifier.CONVERT_REFUSABLE, false);
        editRule(SetupModifier.CULT_KEEPS_ROLES, true);
        cooldown(0);

        String alias = "Cannibal";

        narrator.setAlias(CultLeader.MINION_ALIAS, alias);

        try{
            nightStart();
            fail();
        }catch(IllegalGameSettingsException e){
        }

        editRule(SetupModifier.CULT_KEEPS_ROLES, false);

        voteOut(detect2, sheriff, coroner, doctor, doctor2, lookout, detect, cult, cit1, jester, amnes, cit2, cit3);

        assertEquals(6,
                cult.getPlayer(narrator).getAbility(CultLeader.class).getOptions(cult.getPlayer(narrator)).size());

        promote(cult, cit1, Investigator.class);
        nextNight();

        assertTrue(cit1.is(Investigator.class));
        assertEquals(5,
                cult.getPlayer(narrator).getAbility(CultLeader.class).getOptions(cult.getPlayer(narrator)).size());

        promote(cult, cit2, Cultist.class);
        nextNight();

        assertTrue(cit2.is(Cultist.class));

        try{
            promote(cult, doctor, Armorsmith.class);
            fail();
        }catch(PlayerTargetingException e){
        }
        try{
            promote(cult, doctor, Investigator.class);
            fail();
        }catch(PlayerTargetingException e){
        }
        promote(cult, doctor, Sheriff.class);
        nextNight();

        assertTrue(doctor.is(Cultist.class));

        setTarget(cult, sheriff);
        nextNight();

        assertTrue(sheriff.is(Sheriff.class));
        assertTrue(cult.getPlayer(narrator).getOptions(CultLeader.COMMAND)
                .contains(new Option(Lookout.class.getSimpleName())));

        promote(cult, lookout, Cultist.class);
        nextNight();

        assertTrue(lookout.is(Lookout.class));

        promote(cult, coroner, Coroner.class);
        nextNight();

        assertTrue(coroner.is(Cultist.class));

        promote(cult, detect, Coroner.class);
        nextNight();

        assertTrue(coroner.is(Cultist.class));

        promote(cult, doctor2, Cultist.class);
        nextNight();

        assertTrue(doctor2.is(Cultist.class));

        setTarget(amnes, detect2);
        promote(cult, cit3, Detective.class);
        nextNight();

        assertTrue(amnes.is(Detective.class));
        assertTrue(cit3.is(Detective.class));

        promote(cult, jester, Coroner.class);
        nextNight();

        assertTrue(jester.is(Cultist.class));

        setTarget(cult, jester2);
        nextNight();

        assertTrue(jester2.is(Cultist.class));

        // test for name change, and put it here. the option should say 'savage' if the
        // cultist name is a savage
        // recruiting 'non powerless roles' like 'epsilons' specifically jester
    }

    public void testLosePassiveCharge() {
        Controller cult = addPlayer(BasicRoles.CultLeader());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller surv = addPlayer(BasicRoles.Survivor());
        Controller baker = addPlayer(BasicRoles.Baker());
        addPlayer(BasicRoles.Goon(), 2);

        editRule(SetupModifier.CULT_KEEPS_ROLES, false);
        editRule(SetupModifier.SELF_BREAD_USAGE, true);
        cooldown(0);

        setTarget(cult, baker);
        endNight();

        assertPassableBreadCount(0, baker);
        skipDay();

        setTarget(cult, surv);
        endNight();

        assertTotalVestCount(0, surv);
        skipDay();

        setTarget(cult, vigi);
        endNight();

        assertTotalGunCount(0, vigi);
    }

    public void testGetItems() {
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());

        cooldown(0);
        editRule(SetupModifier.CULT_POWER_ROLE_CD, 0);
        editRule(SetupModifier.CONVERT_REFUSABLE, true);
        editRule(SetupModifier.CULT_PROMOTION, false);
        editRule(SetupModifier.CULT_KEEPS_ROLES, true);

        setTarget(cl, cit);
        setTarget(as, cit);

        endNight();

        assertRealVestCount(1, cit);
        assertEquals(cit.getColor(), cl.getColor());
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Cultist(), AbilityModifier.ZERO_WEIGHTED, Cultist.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Cultist(), AbilityModifier.BACK_TO_BACK, Cultist.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    private void promote(Controller cl, Controller target, Class<? extends Ability> c) {
        if(c == null)
            setTarget(cl, target);
        else
            setTarget(cl, CultLeader.MAIN_ABILITY, c.getSimpleName(), null, NULL_OPT, target);
    }
}
