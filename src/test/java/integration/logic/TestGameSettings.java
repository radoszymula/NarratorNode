package integration.logic;

import game.logic.Player;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Hidden;
import services.HiddenService;

public class TestGameSettings extends SuperTest{
	
	public TestGameSettings(String s){
		super(s);
	}
	
	public void testChatRoleSpawn(){
		Hidden hidden = HiddenService.createHidden(narrator, "rm1",
				BasicRoles.Ventriloquist(),
				BasicRoles.Architect(),
				BasicRoles.Disguiser());
		
		Player p = addPlayer(hidden);
		addPlayer(BasicRoles.Agent());
		addPlayer(BasicRoles.Arsonist());
		
		editRule(SetupModifier.CHAT_ROLES, false);
		
		try{
			nightStart();
			fail();
		}catch(NarratorException e){}
		
		assertEquals(3, hidden.getSize());
		assertTrue(narrator.hiddens.contains(hidden));
		
		removeSetupHidden(hidden);
		narrator.removePlayer(p);
		
		assertEquals(2, narrator.getPlayerCount());
		assertFalse(narrator.getRolesList().contains(hidden));
		assertEquals(2, narrator.getRolesList().size());
		
		addPlayer(BasicRoles.Ventriloquist());
		
		try{
			nightStart();
			fail();
		}catch(IllegalGameSettingsException e){}
		
		
	}
}
