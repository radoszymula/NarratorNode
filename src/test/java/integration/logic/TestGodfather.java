package integration.logic;

import game.logic.exceptions.NarratorException;
import game.logic.support.rules.AbilityModifier;
import game.logic.templates.BasicRoles;
import game.roles.Godfather;
import game.setups.Setup;
import services.FactionRoleService;

public class TestGodfather extends SuperTest {

    public TestGodfather(String name) {
        super(name);
    }

    public void testDifferentTeams() {

        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Godfather(Setup.YAKUZA_C));

        nightStart();
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Godfather(), AbilityModifier.BACK_TO_BACK, Godfather.class,
                    false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Godfather(), AbilityModifier.ZERO_WEIGHTED, Godfather.class,
                    false);
            fail();
        }catch(NarratorException e){
        }
    }
}
