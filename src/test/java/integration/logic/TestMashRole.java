package integration.logic;

import game.ai.Controller;
import game.logic.Faction;
import game.logic.Role;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.AbilityList;
import game.roles.Amnesiac;
import game.roles.Architect;
import game.roles.Baker;
import game.roles.Citizen;
import game.roles.Doctor;
import game.roles.DrugDealer;
import game.roles.Ghost;
import game.roles.Hidden;
import game.roles.Jailor;
import game.roles.Lookout;
import game.roles.SerialKiller;
import game.roles.support.Bread;
import game.setups.Setup;
import models.FactionRole;
import services.FactionService;
import services.RoleAbilityModifierService;
import services.RoleService;
import util.Assertions;
import util.HiddenUtil;

public class TestMashRole extends SuperTest {

    public TestMashRole(String s) {
        super(s);
    }

    public void testBakerLookout() {
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Role mashedRole = RoleService.createRole(narrator, "LookoutBaker", Lookout.class, Baker.class);

        Controller mash = addPlayer(FactionService.createFactionRole(mafia, mashedRole));
        Controller jester1 = addPlayer(BasicRoles.Jester());
        Controller jester2 = addPlayer(BasicRoles.Jester());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.SELF_BREAD_USAGE, true);

        nightStart();

        assertPassableBreadCount(1, mash);
        setTarget(mash, cit1, Lookout.MAIN_ABILITY);
        setTarget(mash, cit2, Lookout.MAIN_ABILITY);
        assertActionSize(2, mash);
        cancelAction(mash, 1);
        assertActionSize(1, mash);
        setTarget(mash, cit2, Lookout.MAIN_ABILITY);
        setTarget(mash, cit2, Lookout.MAIN_ABILITY);
        setTarget(jester1, cit1);
        setTarget(jester2, cit2);

        endNight();

        TestLookout.seen(mash, jester1);
        TestLookout.seen(mash, jester2);
    }

    public void testDifferentCharges() {
        Faction mafiaFaction = narrator.getFaction(Setup.MAFIA_C);
        Role role = RoleService.createRole(narrator, "LookoutDoctor", Doctor.class, Lookout.class);
        RoleAbilityModifierService.modify(role, Doctor.class, AbilityModifier.CHARGES, 1);

        Controller mash = addPlayer(FactionService.createFactionRole(mafiaFaction, role));
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 3);

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        nightStart();

        assertRealChargeRemaining(SetupModifiers.UNLIMITED, mash, Lookout.MAIN_ABILITY);
        assertRealChargeRemaining(1, mash, Doctor.MAIN_ABILITY);

        setTarget(mash, cit1, Doctor.MAIN_ABILITY);
        endNight();

        assertRealChargeRemaining(SetupModifiers.UNLIMITED, mash, Lookout.MAIN_ABILITY);
        assertRealChargeRemaining(0, mash, Doctor.MAIN_ABILITY);

    }

    public void testBadAbilities() {
        badAbilitiyList(new Amnesiac(), new Citizen());
    }

    private void badAbilitiyList(Ability... a) {
        try{
            new AbilityList(a);
            fail();
        }catch(IllegalRoleCombinationException e){
        }
    }

    // no using other abilities while dead
    public void testGhostAbilities() {
        Role ghostWithTeamRole = RoleService.createRole(narrator, "GhostDoctor", Doctor.class, Ghost.class);
        FactionRole ghostWithTeam = FactionService.createFactionRole(narrator.getFaction(Setup.MAFIA_C), ghostWithTeamRole);
        Hidden hiddenGhostWithTeam = HiddenUtil.createHiddenSingle(narrator, ghostWithTeam);

        Controller ghosty = addPlayer(ghostWithTeam);
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        assertBadGameSettings();
        
        deleteRole(ghostWithTeamRole);
        Assertions.assertRoleNotInSetup(ghostWithTeamRole);
        Assertions.assertHiddenNotInSetup(hiddenGhostWithTeam);
        assertEquals(2, narrator.getRolesList().size());

        Controller ars = addPlayer(BasicRoles.Arsonist());
        Role ghostDoctorRole = RoleService.createRole(narrator, "ghost doctor", Ghost.class, Doctor.class);
        Faction benigns = narrator.getFaction(Setup.BENIGN_C);
        addSetupHidden(HiddenUtil.createHiddenSingle(narrator, FactionService.createFactionRole(benigns, ghostDoctorRole)));

        nightStart();
        try{
            setTarget(ghosty, lookout, Ghost.MAIN_ABILITY);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(ghosty, lookout, Doctor.MAIN_ABILITY);
        setTarget(lookout, lookout);
        endNight();

        TestLookout.seen(lookout, ghosty);

        voteOut(ghosty, ars, lookout, cit2);

        try{
            setTarget(ghosty, lookout, Doctor.MAIN_ABILITY);
            fail();
        }catch(PlayerTargetingException e){
        }
        setTarget(ghosty, lookout, Ghost.MAIN_ABILITY);

        endNight();

        isPuppeted(lookout);
        hasPuppets(ghosty);
    }

    public void testRoleNamesUnique() {
        Controller p = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Doctor());

        Role failedMember = new Role(narrator, p.getName(), new Doctor(), new SerialKiller());

        try{
            createRole(failedMember);
            fail();
        }catch(NamingException e){
        }

        try{
            narrator.addPlayer(BasicRoles.Agent().getName());
            fail();
        }catch(NamingException e){
        }

        failedMember = new Role(narrator, DrugDealer.WIPE, new Doctor(), new SerialKiller());

        try{
            createRole(failedMember);
            fail();
        }catch(NamingException e){
        }
    }

    public void testMashAbilityCooldown() {
        Role mashedRole = RoleService.createRole(narrator, "LookoutBaker", Lookout.class, Baker.class);
        mashedRole.addCooldownModifier(Baker.class, 2);
        FactionRole mashed = FactionService.createFactionRole(narrator.getFaction(Setup.MAFIA_C), mashedRole);

        Controller mash = addPlayer(mashed);
        addPlayer(BasicRoles.Citizen(), 2);

        editRule(SetupModifier.SELF_BREAD_USAGE, true);

        nightStart();

        // n0
        assertPassableBreadCount(1, mash);
        nextNight();

        // n1
        assertPassableBreadCount(1, mash);
        nextNight();

        // n2
        assertPassableBreadCount(1, mash);
        nextNight();

        assertPassableBreadCount(2, mash);
    }

    public void testMashAbilityCooldownNoSelfBread() {
        Role mashedRole = RoleService.createRole(narrator, "LookoutBaker", Lookout.class, Baker.class);
        mashedRole.addCooldownModifier(Baker.class, 2);
        FactionRole mashed = FactionService.createFactionRole(narrator.getFaction(Setup.MAFIA_C), mashedRole);

        Controller mash = addPlayer(mashed);
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);

        editRule(SetupModifier.SELF_BREAD_USAGE, false);

        nightStart();

        // n0
        assertUseableBreadCount(0, mash);
        nextNight();

        // n1
        assertPassableBreadCount(1, mash);
        setTarget(mash, cit, Bread.MAIN_ABILITY);
        assertUseableBreadCount(0, mash);
        nextNight();

        // n2
        assertPassableBreadCount(1, cit);
        assertPassableBreadCount(0, mash);
        assertCooldownRemaining(2, mash, Baker.class);
        try{
            setTarget(mash, Bread.COMMAND, null, null, NULL_OPT, cit);
            fail();
        }catch(PlayerTargetingException e){
        }
        nextNight();

        // n3
        assertPassableBreadCount(0, mash);
        try{
            setTarget(mash, Bread.COMMAND, null, null, NULL_OPT, cit);
            fail();
        }catch(PlayerTargetingException e){
        }
        nextNight();

        // n4
        assertPassableBreadCount(1, mash);
        // assertEquals(1, mash.getBread().size());
        setTarget(mash, cit, Bread.MAIN_ABILITY);
        assertUseableBreadCount(0, mash);
        nextNight();

        // n5
        assertPassableBreadCount(2, cit);
    }

    public void testArchitectJailorCanceling() {
        Role mashedRole = RoleService.createRole(narrator, "JailorArchitect", Architect.class, Jailor.class);
        FactionRole mashed = FactionService.createFactionRole(narrator.getFaction(Setup.TOWN_C), mashedRole);

        Controller jarch = addPlayer(mashed);
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller goon = addPlayer(BasicRoles.Goon());

        setTarget(baker, jarch);
        endNight();

        jail(jarch, baker);
        arch(jarch, baker, goon);
        cancelAction(jarch, 0);
        assertNotNull(jarch.getPlayer(narrator).getAction(Architect.MAIN_ABILITY));
        assertNull(jarch.getPlayer(narrator).getAction(Jailor.MAIN_ABILITY));
    }

    public void testPassingAndSubmittingBread() {
        Role mashedRole = RoleService.createRole(narrator, "LoookoutBaker", Lookout.class, Baker.class);
        FactionRole mashed = FactionService.createFactionRole(narrator.getFaction(Setup.MAFIA_C), mashedRole);

        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller mash = addPlayer(mashed);
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller cit4 = addPlayer(BasicRoles.Citizen());

        narrator.setRule(SetupModifier.BREAD_PASSING, true);
        narrator.setRule(SetupModifier.SELF_BREAD_USAGE, true);

        nightStart();
        assertPassableBreadCount(1, mash);

        setTarget(mash, cit, Lookout.MAIN_ABILITY);
        setTarget(mash, cit3, Bread.MAIN_ABILITY);
        assertActionSize(2, mash);
        setTarget(mash, cit4, Lookout.MAIN_ABILITY);
        setTarget(mash, cit2, Bread.MAIN_ABILITY);
        assertActionSize(2, mash);

        // try replacing the action as well

        endNight();
    }

}
