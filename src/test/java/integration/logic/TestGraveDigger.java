package integration.logic;

import java.util.ArrayList;

import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.Faction;
import game.logic.Role;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Blackmailer;
import game.roles.Burn;
import game.roles.Douse;
import game.roles.DrugDealer;
import game.roles.Framer;
import game.roles.GraveDigger;
import game.roles.Stripper;
import game.roles.Undouse;
import game.roles.Veteran;
import game.roles.Visit;
import game.roles.support.Gun;
import game.setups.Setup;
import services.FactionService;
import util.LookupUtil;

public class TestGraveDigger extends SuperTest {

    public TestGraveDigger(String s) {
        super(s);
    }

    Controller gd;

    @Override
    public void roleInit() {
        gd = addPlayer(BasicRoles.GraveDigger());
    }

    private void basicDig(Controller... target) {
        String command;
        if(target[0].getPlayer(narrator).getAbilities().getNightAbilities(target[0].getPlayer(narrator)).isEmpty())
            command = null;
        else{
            Ability a = target[0].getPlayer(narrator).getAbilities().getNightAbilities(target[0].getPlayer(narrator))
                    .getFirst();
            if(GraveDigger.isBannedAction(a.getAbilityNumber()))
                command = null;
            else
                command = a.getCommand();
        }
        setTarget(gd, GraveDigger.MAIN_ABILITY, command, null, NULL_OPT, target);
    }

    public void testGraveDiggerDeadSK() {
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        voteOut(sk, cit, doc, gd);
        isDead(sk);

        try{
            setTarget(sk, cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        basicDig(sk, cit);

        endNight();

        try{
            setTarget(sk, cit);
        }catch(PlayerTargetingException e){
        }

        isDead(cit);
    }

    public void testGraveDiggerDocSave() {
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller doc = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Citizen());

        nightStart();
        try{
            setTarget(gd, doc);
            fail();
        }catch(PlayerTargetingException e){
        }
        setTarget(sk, doc);

        nextNight();

        basicDig(doc, gd);
        setTarget(sk, gd);

        endNight();

        isAlive(sk);
    }

    public void testGunsmithGivingAlliesGuns() {
        Role role = LookupUtil.findRole(narrator, "Gunsmith");
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);

        Controller gs = addPlayer(FactionService.createFactionRole(mafia, role));
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifier.GS_FAULTY_GUNS, false);

        dayStart();
        voteOut(gs, maf, gd, cit);

        basicDig(gs, maf);

        endNight();

        assertTotalGunCount(1, maf);
    }

    public void testGunsmithGivingAlliesGunsNoTeamKnowing() {
        Role role = LookupUtil.findRole(narrator, "Gunsmith");
        Faction mafia = narrator.getFaction(Setup.OUTCAST_C);

        Controller gs = addPlayer(FactionService.createFactionRole(mafia, role));
        Controller maf = addPlayer(BasicRoles.Agent());
        Controller cit = addPlayer(BasicRoles.Citizen());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifier.GS_FAULTY_GUNS, false);

        voteOut(gs, maf, gd, cit);

        isDead(gs);

        basicDig(gs, maf);

        endNight();

        assertTotalGunCount(1, maf);
    }

    public void testGraveDiggerVigilanteLimitedShots() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 1);

        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        dayStart();
        voteOut(vigi, cit, gd, doc);

        setTarget(gd, GraveDigger.MAIN_ABILITY, Gun.COMMAND, vigi, cit);

        endNight();

        isDead(cit);

        assertTotalGunCount(0, vigi);

        skipDay();

        setTarget(gd, GraveDigger.MAIN_ABILITY, Gun.COMMAND, vigi, doc);
        setTarget(lookout, doc);

        endNight();
        TestLookout.seen(lookout, vigi);
        isAlive(doc);
    }

    public void testGraveDiggerCitizen() {
        Controller digger = gd;
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller maf = addPlayer(BasicRoles.Goon());

        dayStart();

        voteOut(cit, digger, lookout, maf);

        basicDig(cit, maf);
        setTarget(lookout, maf);

        endNight();

        TestLookout.seen(lookout, cit);
    }

    public void testGraveDiggingArson() {
        Controller arson = addPlayer(BasicRoles.Arsonist());
        Controller arson2 = addPlayer(BasicRoles.Arsonist());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller bd = addPlayer(BasicRoles.BusDriver());

        dayStart();

        voteOut(arson, gd, cit, doc, bd);

        setTarget(gd, GraveDigger.MAIN_ABILITY, Douse.COMMAND, arson, cit);
        nextNight();

        assertStatus(cit, Douse.MAIN_ABILITY);

        setTarget(gd, GraveDigger.MAIN_ABILITY, Undouse.COMMAND, arson, cit);
        setTarget(arson2, doc);

        nextNight();

        setTarget(gd, GraveDigger.MAIN_ABILITY, Burn.COMMAND, arson);

        endNight();

        isDead(doc);
        isAlive(cit);
    }

    public void testDiggingUpVeteranKillingAll() {
        Controller amnes = addPlayer(BasicRoles.Amnesiac());
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller coroner = addPlayer(BasicRoles.Coroner());

        dayStart();
        voteOut(veteran, coroner, gd, amnes);

        setTarget(amnes, veteran);
        setTarget(coroner, veteran);
        basicDig(veteran, amnes);

        endNight();

        isDead(amnes, coroner, gd);
        assertGameOver();
    }

    public void testDiggingVeteranVisitation() {
        modifyRole(BasicRoles.Veteran(), AbilityModifier.CHARGES, 1);

        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        alert(veteran);
        endNight();

        assertPerceivedChargeRemaining(0, veteran, Veteran.MAIN_ABILITY);

        voteOut(veteran, lookout, gd, cit);

        basicDig(veteran, cit);
        setTarget(lookout, cit);
        endNight();

        isAlive(gd);
        TestLookout.seen(lookout, veteran);

    }

    public void testDiggerBusDriver() {
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        voteOut(bd, cit, gd, sk);

        basicDig(bd, cit, cit2);
        setTarget(sk, cit);

        endNight();

        isAlive(cit);
        isDead(cit2);
    }

    public void testDiggingGraveDigger() {
        Controller digger1 = addPlayer(BasicRoles.GraveDigger());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller bd = addPlayer(BasicRoles.BusDriver());

        voteOut(digger1, gd, cit, bd);

        basicDig(digger1, cit);
        setTarget(lookout, cit);

        endNight();

        TestLookout.seen(lookout, digger1);
    }

    public void testNoVisitCorpses() {
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller agent_killed = addPlayer(BasicRoles.Agent());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        voteOut(amn, agent_killed, cit, mayor, witch);

        basicDig(amn, gd);
        setTarget(lookout, gd);
        endNight();
        TestLookout.seen(lookout, amn);

        voteOut(agent_killed, cit, mayor, gd, witch);

        basicDig(agent_killed, gd);
        setTarget(lookout, gd);
        endNight();
        TestLookout.seen(lookout, agent_killed);

        voteOut(cit, mayor, gd, witch);

        basicDig(cit, gd);
        setTarget(lookout, gd);
        endNight();
        TestLookout.seen(lookout, cit);

        voteOut(mayor, gd, witch, lookout);

        basicDig(mayor, gd);
        setTarget(lookout, gd);
        endNight();
        TestLookout.seen(lookout, mayor);

    }

    public void testBreadedDigger() {
        Controller digger = gd;
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller baker = addPlayer(BasicRoles.Baker());

        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 2);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        nightStart();

        assertTotalGunCount(2, vigi);

        setTarget(baker, digger);
        setTarget(baker2, digger);
        endNight();

        voteOut(vigi, lookout, baker, doc, detect, baker2);

        isDead(vigi);

        corpseShoot(digger, vigi, doc);
        corpseShoot(digger, vigi, doc2);
        corpseShoot(digger, vigi, lookout);
        setTarget(lookout, lookout);

        assertTotalGunCount(2, vigi);

        endNight();

        isDead(doc, doc2);
        isAlive(lookout);
        TestLookout.seen(lookout, vigi);
    }

    public void testSpyCorpse() {
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller digger = gd;
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller witch = addPlayer(BasicRoles.Witch());

        voteOut(spy, digger, lookout, witch);

        basicDig(spy, witch);
        setTarget(lookout, witch);
        endNight();

        TestLookout.seen(lookout, spy);
    }

    public void testSpyCorpseCH() {
        Controller digger = gd;
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller witch = addPlayer(BasicRoles.Witch());

        voteOut(spy, digger, lookout, witch);

        diggerTarget(digger, Visit.COMMAND, null, spy, witch);
        setTarget(lookout, witch);
        endNight();

        TestLookout.seen(lookout, spy);
    }

    public void testConflictingSubmissions() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 1);

        Controller digger1 = gd;
        Controller digger2 = addPlayer(BasicRoles.GraveDigger());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller vigi = addPlayer(BasicRoles.Vigilante());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        voteOut(vigi, digger1, digger2, cit);

        setTarget(lookout, digger2);
        corpseShoot(digger1, vigi, digger2);
        corpseShoot(digger2, vigi, digger1);
        endNight(digger2, digger1);
        endNight();

        isDead(digger1);
        isAlive(digger2);
        TestLookout.seen(lookout, vigi);
    }

    private void corpseShoot(Controller digger1, Controller vigi, Controller digger2) {
        setTarget(digger1, GraveDigger.MAIN_ABILITY, Gun.COMMAND, null, NULL_OPT, vigi, digger2);
    }

    public void testConflictingSubmissions2() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 1);

        Controller digger1 = gd;
        Controller digger2 = addPlayer(BasicRoles.GraveDigger());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller vigi = addPlayer(BasicRoles.Vigilante());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        voteOut(vigi, digger1, digger2, cit);

        setTarget(lookout, digger1);
        corpseShoot(digger1, vigi, digger2);
        corpseShoot(digger2, vigi, digger1);
        endNight(digger1, digger2);
        endNight();

        isDead(digger2);
        isAlive(digger1);
        TestLookout.seen(lookout, vigi);
    }

    public void testCorpseFramer() {
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller digger = gd;
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());

        voteOut(framer, digger, cit, sheriff);

        setTarget(sheriff, cit);
        setTarget(digger, GraveDigger.MAIN_ABILITY, Framer.COMMAND, framer.getColor(), NULL_OPT, framer, cit);
        endNight();

        TestSheriff.seen(sheriff, framer.getColor());
    }

    // commands dont get logged by the dead people
    // this shouldn't happen because i'm literally adding the action itself.

    public void testCorpseFramerCH() {
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller digger = gd;
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());

        voteOut(framer, digger, cit, sheriff);
        setTarget(sheriff, cit);

        diggerTarget(digger, Framer.COMMAND, framer.getColor(), framer, cit);
        endNight();

        TestSheriff.seen(sheriff, framer.getColor());
    }

    public void testCorpseDrugCH() {
        Controller digger = gd;
        Controller dealer = addPlayer(BasicRoles.DrugDealer());
        Controller stripper = addPlayer(BasicRoles.Escort());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());

        voteOut(dealer, digger, stripper, sheriff);
        setTarget(sheriff, stripper);

        diggerTarget(digger, DrugDealer.COMMAND, DrugDealer.BLOCKED, dealer, sheriff);
        endNight();

        partialContains(sheriff, Stripper.FEEDBACK);
    }

    public void testNotUsingTeamAbilityAndBannedActions() {
        Controller goon = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        addTeamAbility(Setup.MAFIA_C, Stripper.class);

        voteOut(goon, vet, cit2, gd);

        try{
            diggerTarget(gd, Blackmailer.COMMAND, null, goon, vet);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            diggerTarget(gd, GraveDigger.COMMAND, null, goon);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            diggerTarget(gd, Stripper.COMMAND, null, goon, vet);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testCantTargetCorpseAgain() {
        Controller stripper = addPlayer(BasicRoles.Escort());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.GD_REANIMATE, false);

        voteOut(stripper, vigi, cit, gd);

        basicDig(stripper, vigi);
        shoot(vigi, gd);
        nextNight();

        isAlive(gd);

        try{
            basicDig(stripper, vigi);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void diggerTarget(Controller gDigger, String option, String option2, Controller... players) {
        Action gdAction = new Action(gDigger.getPlayer(narrator), ControllerList.list(players).toPlayerList(narrator),
                GraveDigger.MAIN_ABILITY, option, option2);
        ArrayList<String> commands = gDigger.getPlayer(narrator).getAbility(GraveDigger.MAIN_ABILITY)
                .getCommandParts(gdAction);
        Action a = gDigger.getPlayer(narrator).getAbility(GraveDigger.MAIN_ABILITY)
                .parseCommand(gDigger.getPlayer(narrator), commands);

        setTarget(gDigger, GraveDigger.COMMAND, a.getOption(), a.getOption2(), players);
    }
}
