package integration.logic;

import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.Faction;
import game.logic.Role;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.CultLeader;
import game.roles.FactionKill;
import game.roles.GraveDigger;
import game.roles.ProxyKill;
import game.roles.Stripper;
import game.setups.Setup;
import models.FactionRole;
import services.FactionService;
import services.RoleService;

public class TestProxyKill extends SuperTest {

    public TestProxyKill(String name) {
        super(name);
    }

    public void testCLKillMashSend() {
        Faction cultFaction = narrator.getFaction(Setup.CULT_C);
        Role role = RoleService.createRole(narrator, "SavageLeader", CultLeader.class, ProxyKill.class);
        FactionRole mashedRole = FactionService.createFactionRole(cultFaction, role);

        Controller cl = addPlayer(mashedRole);
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller fodder2 = addPlayer(BasicRoles.Citizen());
        Controller strip = addPlayer(BasicRoles.Escort());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifier.CULT_PROMOTION, true);
        editRule(SetupModifier.CONVERT_REFUSABLE, false);
        editRule(SetupModifier.CULT_KEEPS_ROLES, false);
        modifyAbilityCooldown(BasicRoles.CultLeader(), CultLeader.class, 0);

        setTarget(cl, cit, CultLeader.MAIN_ABILITY);
        nextNight();

        assertTrue(cl.getPlayer(narrator).hasAbility(ProxyKill.MAIN_ABILITY));
        assertFalse(cit.getPlayer(narrator).hasAbility(ProxyKill.MAIN_ABILITY));
        assertEquals(2, cl.getCommands().size());
        assertTrue(cit.getCommands().isEmpty());

        proxySend(cl, cl, fodder);
        nextNight();

        isDead(fodder);

        TestGhost.assertCause(fodder, cl);
        ;

        proxySend(cl, cit, fodder2);
        setTarget(lookout, fodder2);
        endNight();

        isDead(fodder2);
        TestLookout.seen(lookout, cit);

        skipDay();

        setTarget(strip, cit);
        proxySend(cl, cit, strip);
        endNight();

        isAlive(strip);
        partialExcludes(cl, Stripper.FEEDBACK);
        partialContains(cit, Stripper.FEEDBACK);

        skipDay();

        setTarget(strip, cl);
        setTarget(baker, cl);
        proxySend(cl, cit, strip);
        endNight();

        isDead(strip);

        skipDay();

        proxySend(cl, cit, lookout);
        proxySend(cl, cit, baker);
        assertActionSize(2, cl);

        endNight();

        isDead(baker, lookout);
        assertPassableBreadCount(0, cl);
    }

    private void proxySend(Controller owner, Controller killer, Controller target) {
        setTarget(owner, ControllerList.list(killer, target), ProxyKill.MAIN_ABILITY);
    }

    private static FactionRole ProxyKill() {
        Faction cult = narrator.getFaction(Setup.CULT_C);
        Role role = RoleService.createRole(narrator, "ProxyKill", ProxyKill.class);
        return FactionService.createFactionRole(cult, role);
    }

    public void testBadTargeting1() {
        Controller fodder1 = addPlayer(BasicRoles.Citizen());
        Controller px = addPlayer(ProxyKill());
        addPlayer(BasicRoles.Goon());

        try{
            proxySend(px, fodder1, fodder1);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            setTarget(px, ControllerList.list(px), ProxyKill.MAIN_ABILITY);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            setTarget(px, ControllerList.list(px, px, px, px), ProxyKill.MAIN_ABILITY);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testElectroProxyKill() {
        Controller em = addPlayer(BasicRoles.ElectroManiac());
        Controller px = addPlayer(ProxyKill());
        Controller maf = addPlayer(BasicRoles.Cultist());
        Controller cit = addPlayer(BasicRoles.Citizen());

        electrify(em, maf, cit);
        proxySend(px, maf, cit);
        endNight();

        isDead(maf);
        isAlive(px, em);
    }

    public void testWitchProxyKill() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller px = addPlayer(ProxyKill());
        Controller maf = addPlayer(BasicRoles.Cultist());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller lookout2 = addPlayer(BasicRoles.Lookout());

        witch(witch, px, cit);
        setTarget(lookout, cit);
        proxySend(px, maf, cit2);
        setTarget(lookout2, cit2);
        endNight();

        isDead(cit2);
        isAlive(cit);
        TestLookout.seen(lookout, px);
        TestLookout.seen(lookout2, maf);

        skipDay();

        witch(witch, maf, cit);
        proxySend(px, maf, witch);
        endNight();

        isAlive(witch);
        isDead(cit);
    }

    public void testGainingNightKill() {
        Controller prox = addPlayer(ProxyKill());
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen(), 2);

        editRule(SetupModifier.PROXY_UPGRADE, true);

        reveal(mayor);
        vote(cult, prox);
        vote(mayor, prox);

        mafKill(cult, mayor);
        endNight();

        isDead(mayor);
        skipDay();

        assertCooldownRemaining(0, cult, FactionKill.class);
    }

    public void testNoGainingNightKill() {
        Controller prox = addPlayer(ProxyKill());
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen(), 2);

        editRule(SetupModifier.PROXY_UPGRADE, false);

        reveal(mayor);
        vote(cult, prox);
        vote(mayor, prox);

        try{
            mafKill(cult, mayor);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testCorpseProxy() {
        Controller prox = addPlayer(ProxyKill());
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller gd = addPlayer(BasicRoles.GraveDigger());
        addPlayer(BasicRoles.Citizen(), 2);

        editRule(SetupModifier.PROXY_UPGRADE, true);

        reveal(mayor);
        vote(cult, prox);
        vote(gd, prox);
        vote(mayor, prox);

        mafKill(cult, mayor);
        setTarget(gd, GraveDigger.MAIN_ABILITY, null, prox, cult);
        endNight();

        isDead(mayor, cult, prox);
    }
}
