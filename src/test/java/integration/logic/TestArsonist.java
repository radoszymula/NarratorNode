package integration.logic;

import java.util.ArrayList;

import game.ai.Controller;
import game.event.EventList;
import game.event.Message;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.logic.templates.HTMLDecoder;
import game.roles.Burn;
import game.roles.Citizen;
import game.roles.Douse;
import game.roles.Undouse;
import services.FactionRoleService;

public class TestArsonist extends SuperTest {

    private static final int DOUSE = Douse.MAIN_ABILITY;
    private static final int UNDOUSE = Undouse.MAIN_ABILITY;

    public TestArsonist(String name) {
        super(name);
    }

    public void testBasicAbility() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        setTarget(a1, c1, DOUSE);
        setTarget(a2);

        endNight();

        isDead(c1);
        assertEquals(0, c1.getPlayer(narrator).getDeathDay());
    }

    public void testModifierTextNoArchitect() {
        Controller arson = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        dayStart();

        ArrayList<String> arsonRoleSpecs = ((Player) arson).getRoleSpecs();
        partialExcludes(arsonRoleSpecs, Burn.ARCHITECT_ROOM_SPREAD_TEXT);
        partialExcludes(arsonRoleSpecs, Burn.ARCHITECT_ROOM_NO_SPREAD_TEXT);
    }

    public void testFeedback() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Citizen());

        setName(c1, "Ciitizen");

        setTarget(a1, c1, DOUSE);
        partialExcludes(a1, Citizen.class.getSimpleName());
    }

    public void testBurnUndouse() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);
        Controller a1 = addPlayer(BasicRoles.Arsonist());

        nightStart();
        burn(a1);

        nextNight();

        setTarget(a1, c1, DOUSE);
    }

    public void testBusArson() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller a1 = addPlayer(BasicRoles.Arsonist());

        setTarget(a1, c1, DOUSE);
        drive(bd, bd, c1);

        endNight();

        assertStatus(c1, Douse.MAIN_ABILITY, false);
        assertStatus(bd, Douse.MAIN_ABILITY, true);
    }

    public void testArsonDayBurnEnd() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        nightStart();

        setTarget(a1, c1, DOUSE);
        setTarget(a2, c2, DOUSE);

        endNight();

        vote(c1, a1);
        vote(c2, a1);
        vote(a1, c3);
        vote(a2, c3);

        assertInProgress();
        burn(a2);

        assertGameOver();
    }

    public void testArsonDayBurnedNoEnd() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        setAllies(BasicRoles.Arsonist(), BasicRoles.Arsonist());

        setTarget(a1, c1, DOUSE);
        setTarget(a2, c2, DOUSE);

        endNight();

        vote(c1, a1);
        vote(c2, a1);
        vote(a2, c3);

        assertInProgress();
        burn(a2);

        assertTrue(c1.getPlayer(narrator).getDeathType().getList().contains(Constants.ARSON_KILL_FLAG));
        assertFalse(c1.getPlayer(narrator).getDeathType().isLynch());

        EventList el = narrator.getEventManager().getEvents(Message.PUBLIC);

        assertTrue(el.access(Message.PUBLIC, new HTMLDecoder()).contains("explosion"));

        isDead(c1, c2);

        vote(a1, c3);

        isDead(c3);
        isDead(c2);
        isDead(c1);

        assertGameOver();

        isWinner(a2);
    }

    public void testUndouseWhileOtherBurning() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.BusDriver());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        setTarget(a1, a1);
        nextNight();

        burn(a2);

        setTarget(a1, a2, UNDOUSE);

        partialContains(a1, "You will douse yourself");

        endNight();

        isAlive(a1, a2, c1, c2, c3);
    }

    public void testUndousingText() {
        addPlayer(BasicRoles.Citizen(), 2);
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller ars = addPlayer(BasicRoles.Arsonist());

        nightStart();

        setTarget(ars, c3);
        setTarget(ars, c3, Undouse.MAIN_ABILITY);

        partialContains(ars, "dousing");
    }

    public void testCantBurnImmediatelyAfter() {
        addPlayer(BasicRoles.Citizen(), 3);
        Controller a1 = addPlayer(BasicRoles.Arsonist());

        nightStart();

        burn(a1);

        endNight();

        assertFalse(a1.getPlayer(narrator).hasDayAction(Burn.COMMAND));

        skipDay();
        endNight();

        burn(a1);

        skipDay();

        assertFalse(a1.getPlayer(narrator).getAbility(Burn.COMMAND).isAcceptableTarget(a1.getPlayer(narrator)));
        assertTrue(a1.getPlayer(narrator).getAcceptableTargets(Burn.COMMAND) != null);
    }

    public void testCantDayBurnAgain() {
        addPlayer(BasicRoles.Citizen(), 3);
        Controller a1 = addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifier.ARSON_DAY_IGNITES, 1);

        dayStart();

        burn(a1);

        skipDay();
        endNight();

        partialExcludes(a1.getCommands(), Burn.COMMAND);
    }

    public void testSelfKill() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.BusDriver());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        nightStart();

        setTarget(a1);
        setTarget(a2, a2, DOUSE);

        endNight();

        isAlive(a1);
        skipDay();
        setTarget(a1, a1, DOUSE);
        setTarget(a2, a2, DOUSE);

        endNight();
        skipDay();

        setTarget(a1);
        setTarget(a2);
        endNight();

        isDead(a1, a2);
    }

    public void testDousedNotBurning() {
        addPlayer(BasicRoles.BusDriver(), 2);
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        setTarget(a1, a1, DOUSE);
        setTarget(a2, a2, DOUSE);

        nextNight();

        assertNull(a1.getPlayer(narrator).getAcceptableTargets(Burn.COMMAND));
        setTarget(a1);
        endNight();

        isDead(a1);
    }

    public void testGameEnd() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller arsonist = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Amnesiac(), 6);

        setTarget(arsonist, cit);
        endNight();

        burn(arsonist);

        isDead(cit);
        assertGameOver();
    }

    public void testNoBurning() {
        addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller ars = addPlayer(BasicRoles.Arsonist());

        setTarget(ars, cit);
        nextNight();

        burn(ars);
        cancelAction(ars, 0);
        assertActionSize(0, ars);

        endNight();

        isAlive(cit);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Arsonist(), AbilityModifier.BACK_TO_BACK, Burn.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightedModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Arsonist(), AbilityModifier.ZERO_WEIGHTED, Burn.class, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
