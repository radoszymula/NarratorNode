package integration.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import game.ai.Controller;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.support.rules.RoleModifier;
import game.logic.templates.BasicRoles;
import game.roles.Citizen;
import game.roles.Doctor;
import game.roles.Hidden;
import game.roles.Sheriff;
import game.setups.Setup;
import models.FactionRole;
import services.FactionRoleService;
import services.FactionService;
import services.HiddenFactionRoleService;
import services.HiddenService;
import services.SetupHiddenService;
import util.LookupUtil;

public class TestHidden extends SuperTest {

    public TestHidden(String name) {
        super(name);
    }

    public void testSetRole() {
        Hidden rm = Hidden.TownRandom();
        SetupHiddenService.CreateHostSpawn(narrator, rm, BasicRoles.Citizen());
        SetupHiddenService.CreateHostSpawn(narrator, rm, BasicRoles.Sheriff());

        Controller c1 = addPlayer(rm);
        Controller c2 = addPlayer(rm);
        addPlayer(BasicRoles.SerialKiller());

        nightStart();

        assertTrue(c1.is(Citizen.class) || c1.is(Sheriff.class));
        assertTrue(c2.is(Sheriff.class) || c2.is(Citizen.class));
    }

    public void testUniqueDouble() {
        Hidden rm = Hidden.TownRandom();
        SetupHiddenService.CreateHostSpawn(narrator, rm, BasicRoles.Mayor());

        try{
            SetupHiddenService.CreateHostSpawn(narrator, rm, BasicRoles.Mayor());
            fail();
        }catch(NarratorException e){
        }
    }

    public void testSettingUniqueSetupHiddens() {
        addRole(BasicRoles.Doctor());
        addRole(BasicRoles.Doctor());

        try{
            modifyRole(BasicRoles.Doctor(), RoleModifier.UNIQUE, true);
            fail();
        }catch(NarratorException e){
        }

    }

    public void testNewModifiers1() {
        addPlayer(BasicRoles.Citizen());
        Controller host = addPlayer(BasicRoles.Goon());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        mafKill(host, sk);

        endNight();
        assertInProgress();

    }

    public void testNewModifiers2() {
        addPlayer(BasicRoles.Citizen());
        Controller host = addPlayer(BasicRoles.Goon());
        Controller nk = addPlayer(Hidden.NeutralKillingRandom());

        mafKill(host, nk);

        endNight();
        assertInProgress();

    }

    public void testUniqueOrderings() {
        addPlayer(BasicRoles.Detective());
        addPlayer(BasicRoles.Witch());
        String[] teamColors = new String[] {
                Setup.BENIGN_C, Setup.CULT_C, Setup.MAFIA_C, Setup.YAKUZA_C
        };

        Hidden rm3 = HiddenService.createHidden(narrator, "ColorCit3");
        Hidden rm4 = HiddenService.createHidden(narrator, "ColorCit2");
        ArrayList<FactionRole> mems = new ArrayList<>();
        Role doctorRole = LookupUtil.findRole(narrator, "Doctor");
        FactionRole m;
        for(String c: teamColors){
            m = FactionService.createFactionRole(narrator.getFaction(c), doctorRole);
            FactionRoleService.addModifier(m, RoleModifier.UNIQUE, true);
            mems.add(m);
        }
        FactionRoleService.addModifier(mems.get(0), RoleModifier.UNIQUE, false);
        for(int i = 0; i < mems.size(); i++){
            if(i != 0)
                HiddenFactionRoleService.addSpawnableRoles(narrator, rm3, mems.get(i));
            HiddenFactionRoleService.addSpawnableRoles(narrator, rm4, mems.get(i));
        }

        for(int i = 0; i < 3; i++)
            SetupHiddenService.addSetupHidden(narrator, rm3, false);
        SetupHiddenService.addSetupHidden(narrator, rm4, false);

        assertEquals(3, rm3.getUniqueCount());
        assertEquals(3, rm4.getUniqueCount());

        narrator.addPlayer("rm1");
        narrator.addPlayer("rm2");
        narrator.addPlayer("rm3");
        narrator.addPlayer("rm4");

        dayStart();
        Set<String> colors = new HashSet<>();
        for(Controller p: narrator._players){
            if(!p.is(Doctor.class))
                continue;
            if(colors.contains(p.getColor()))
                fail();
            colors.add(p.getColor());
        }
        assertEquals(teamColors.length, colors.size());
    }
}
