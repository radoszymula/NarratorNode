package integration.logic;

import java.util.ArrayList;

import game.ai.Controller;
import game.ai.ControllerList;
import game.event.Feedback;
import game.event.SelectionMessage;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownTeamException;
import game.logic.support.Option;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Spy;
import game.roles.Stripper;
import game.setups.Setup;
import services.FactionRoleService;

public class TestSpy extends SuperTest {

    public TestSpy(String s) {
        super(s);
    }

    public void testSpyBasic() {
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller cons = addPlayer(BasicRoles.Consort());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller fodder = addPlayer(BasicRoles.Citizen());

        nightStart();
        mafKill(maf, fodder);
        setTarget(cons, doc);
        try{
            spy(spy, doc.getColor());
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            spy(spy, null);
            fail();
        }catch(UnknownTeamException e){
        }

        spy(spy, cons.getColor());

        endNight();

        seen(spy, doc, fodder);
    }

    public void testSpyNoDoubleTargets() {
        addPlayer(BasicRoles.Doctor());
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller cons = addPlayer(BasicRoles.Consort());
        Controller maf = addPlayer(BasicRoles.Goon());

        nightStart();
        mafKill(maf, fodder);
        setTarget(cons, fodder);
        try{
            spy(spy, spy.getColor());
            fail();
        }catch(PlayerTargetingException e){
        }

        spy(spy, maf.getColor());

        endNight();

        seen(spy, fodder);
    }

    public void testSpyAndFramer() {
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller cons = addPlayer(BasicRoles.Consort());
        addPlayer(BasicRoles.Doctor());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller fodder = addPlayer(BasicRoles.Citizen());

        nightStart();
        mafKill(maf, fodder);
        setTarget(cons, fodder);
        try{
            spy(spy, spy.getColor());
            fail();
        }catch(PlayerTargetingException e){
        }

        spy(spy, maf.getColor());

        endNight();

        seen(spy, fodder);
    }

    public void testSpyTexting() {
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller chauf = addPlayer(BasicRoles.Chauffeur());
        addPlayer(BasicRoles.SerialKiller());

        nightStart();

        String message = "sPyabilIty ma FIA";
        command(spy, message);

        drive(chauf, chauf, spy);
        endNight();

        seen(spy, chauf, spy);
    }

    public void testSpyFeedback() {
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller cons = addPlayer(BasicRoles.Consort());
        addPlayer(BasicRoles.Witch());

        nightStart();

        spy(spy, cons.getColor());

        assertActionSize(1, spy);

        SelectionMessage sm = new SelectionMessage(spy.getPlayer(narrator), false);

        ArrayList<Action> actions = spy.getPlayer(narrator).getActions().getActions(), subset = new ArrayList<>();
        subset.add(actions.get(0));
        sm.add(spy.getPlayer(narrator).getAbility(Spy.COMMAND).getActionDescription(subset));

        assertTrue(sm.toString().length() > 4);
    }

    public void testSpyBlock() {
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller consort = addPlayer(BasicRoles.Consort());
        addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.BLOCK_FEEDBACK, true);

        nightStart();

        String message = "sPyabiLITy ma FIA";
        command(spy, message);

        setTarget(consort, spy);
        endNight();

        partialContains(spy, Stripper.FEEDBACK);
        partialExcludes(spy, Spy.FEEDBACK);
        partialExcludes(spy, Spy.NO_VISIT);
    }

    public void testSpyAlliesOnly() {
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller cons = addPlayer(BasicRoles.Consort());
        Controller ars = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.SPY_TARGETS_ENEMIES, false);

        try{
            spy(spy, cons.getColor());
            fail();
        }catch(PlayerTargetingException | UnknownTeamException e){
        }

        try{
            spy(spy, ars.getColor());
            fail();
        }catch(PlayerTargetingException e){
        }

        assertFalse(spy.getPlayer(narrator).getOptions(Spy.COMMAND)
                .contains(new Option(cons.getPlayer(narrator).getFaction())));
    }

    public void testSpyNotAlliesOnly() {
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller cons = addPlayer(BasicRoles.Consort());
        addPlayer(BasicRoles.Witch());

        nightStart();

        assertTrue(spy.getPlayer(narrator).getOptions(Spy.COMMAND)
                .contains(new Option(cons.getPlayer(narrator).getFaction())));
    }

    public void testSpyEnemiesOnly() {
        Controller jester = addPlayer(BasicRoles.Jester());
        Controller arson = addPlayer(BasicRoles.Arsonist());
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller cons = addPlayer(BasicRoles.Consort());
        addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.SPY_TARGETS_ALLIES, false);
        editRule(SetupModifier.SPY_TARGETS_ENEMIES, true);

        nightStart();
        try{
            spy(spy, spy.getColor());
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            spy(spy, spy.getColor());
            fail();
        }catch(PlayerTargetingException e){
        }

        assertTrue(spy.getPlayer(narrator).getOptions(Spy.COMMAND)
                .contains(new Option(cons.getPlayer(narrator).getFaction())));
        assertTrue(spy.getPlayer(narrator).getOptions(Spy.COMMAND)
                .contains(new Option(arson.getPlayer(narrator).getFaction())));
        assertFalse(spy.getPlayer(narrator).getOptions(Spy.COMMAND)
                .contains(new Option(jester.getPlayer(narrator).getFaction())));

        spy(spy, arson.getColor());
    }

    public void testSpyNotEnemiesOnly() {
        Controller jester = addPlayer(BasicRoles.Jester());
        Controller spy = addPlayer(BasicRoles.Spy());
        addPlayer(BasicRoles.Consort());
        addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.SPY_TARGETS_ALLIES, true);

        nightStart();

        assertTrue(spy.getPlayer(narrator).getOptions(Spy.COMMAND)
                .contains(new Option(jester.getPlayer(narrator).getFaction())));
    }

    public void testWitchSpy() {
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller maf = addPlayer(BasicRoles.Goon());

        nightStart();
        spy(spy, maf.getColor());
        witch(witch, spy, maf);
        setTarget(lookout, maf);
        endNight();

        TestLookout.seen(lookout, spy);
    }

    public void testChargedSpy() {
        modifyRole(BasicRoles.Spy(), AbilityModifier.CHARGES, 1);

        Controller spy = addPlayer(BasicRoles.Spy());
        Controller maf = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        nightStart();
        spy(spy, maf.getColor());
        nextNight();

        assertPerceivedChargeRemaining(0, spy);

        try{
            spy(spy, maf.getColor());
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testNoBackToBackSpying() {
        FactionRoleService.addModifier(BasicRoles.Spy(), AbilityModifier.BACK_TO_BACK, Spy.class, false);

        Controller spy = addPlayer(BasicRoles.Spy());
        Controller goon = addPlayer(BasicRoles.Goon());
        Controller goon2 = addPlayer(BasicRoles.Goon(Setup.YAKUZA_C));

        nightStart();
        spy(spy, goon.getColor());
        nextNight();

        try{
            spy(spy, goon.getColor());
            fail();
        }catch(NarratorException e){
        }

        spy(spy, goon2.getColor());
    }

    public static void seen(Controller spy, Controller... players) {
        seen(spy, ControllerList.list(players));
    }

    public static void seen(Controller spy_c, ControllerList visitors) {
        Player spy = spy_c.getPlayer(narrator);
        ArrayList<Object> parts = Spy.FeedbackGenerator(visitors.toPlayerList(narrator));
        Feedback f = new Feedback(spy.getSkipper());
        f.add(parts);
        f.setVisibility(spy);
        String feedback = f.access(spy);
        f.removeVisiblity(spy);
        partialContains(spy, feedback);
    }
}
