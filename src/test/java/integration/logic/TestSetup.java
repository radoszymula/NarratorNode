package integration.logic;

import game.ai.Computer;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.roles.Clubber;
import game.roles.CultLeader;
import game.roles.Goon;
import game.roles.Hidden;
import game.setups.BraveNewWorld;
import game.setups.Classic;
import game.setups.LiarsClub;
import game.setups.MediumG;
import game.setups.Setup;
import models.FactionRole;
import services.SetupHiddenService;
import util.LookupUtil;

public class TestSetup extends SuperTest {

    public TestSetup(String name) {
        super(name);
    }

    public void testApplySetup() {
        narrator.removeAllFactionsAndRoles();
        Setup s = new Classic(narrator);
        s.applyRolesList();

        assertEquals(7, narrator.getRolesList().size());

        for(int i = 0; i < 8; i++){
            narrator.addPlayer(Computer.toLetter(i + 1));
        }
        s.applyRolesList();
        assertEquals(8, narrator.getRolesList().size());

        for(int i = 8; i < 16; i++){
            narrator.addPlayer(Computer.toLetter(i + 1));
        }
        s.applyRolesList();
        assertEquals(16, narrator.getRolesList().size());
    }

    public void testApplySetupE() {
        Setup s = new MediumG(narrator.removeAllFactionsAndRoles());

        for(int i = 0; i < 9; i++)
            narrator.addPlayer(Computer.toLetter(i + 1));

        s.applyRolesList();
        Faction f = narrator.getFaction(Setup.YAKUZA_C);
        for(FactionRole factionRole: f._roleSet.values()){
            if(factionRole.role.hasAbility(Goon.class))
                return;
        }
        fail("Goon wasn't found");
    }

    public void testCultSurviving() {
        Setup s = new BraveNewWorld(narrator.removeAllFactionsAndRoles());
        for(int i = 0; i < 25; i++)
            addPlayer(Computer.toLetter(i + 1));

        s.applyRolesList();

        FactionRole role = LookupUtil.findFactionRole(narrator, "Clubber", BraveNewWorld.TOWN_C);
        Hidden hidden = LookupUtil.findHidden(narrator, "Hidden Alpha");
        SetupHiddenService.CreateHostSpawn(narrator, hidden, role);

        nightStart();

        Player cl = narrator.getAllPlayers().filter(CultLeader.class).getFirst();
        Player clubber = narrator.getAllPlayers().filter(Clubber.class).getFirst();

        assertNotNull(cl);
        assertNotNull(clubber);
        assertEquals(1, cl.getRealAutoVestCount());

        setTarget(clubber, cl);
        endNight();

        isAlive(cl);
        skipTearDown = true;
    }

    public void testSetupAllAddRemove() {
        Setup setup;
        for(String setupName: Setup.SETUP_LIST){
            narrator = new Narrator();
            setup = Setup.GetSetup(narrator, setupName);
            for(int j = 0; j < setup.getMinPlayerCount(); j++){
                narrator.addPlayer(Computer.toLetter(j + 1));
            }
            setup.applyRolesList();
            if(!setupName.equals(LiarsClub.KEY))
                assertEquals(narrator.getRolesList().size(), narrator.getPlayerCount());
            while (narrator.getPlayerCount() < setup.getMaxPlayerCount()){
                narrator.addPlayer(Computer.toLetter(narrator.getPlayerCount() + 2));
                setup.applyRolesList();
                if(!setupName.equals(LiarsClub.KEY))
                    assertEquals(narrator.getRolesList().size(), narrator.getPlayerCount());
            }
            while (narrator.getPlayerCount() > setup.getMinPlayerCount()){
                narrator.removePlayer(narrator._players.getLast());
                setup.applyRolesList();
                if(!setupName.equals(LiarsClub.KEY))
                    assertEquals(narrator.getPlayerCount(), narrator.getRolesList().size());
            }
        }
    }
}
