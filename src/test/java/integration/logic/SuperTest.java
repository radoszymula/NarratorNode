package integration.logic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import game.ai.Brain;
import game.ai.Computer;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.ChatMessage;
import game.event.DeathAnnouncement;
import game.event.EventList;
import game.event.EventLog;
import game.event.Feedback;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.TestException;
import game.logic.exceptions.UnknownTeamException;
import game.logic.exceptions.UnsupportedMethodException;
import game.logic.listeners.NarratorListener;
import game.logic.support.CommandHandler;
import game.logic.support.CustomRoleAssigner;
import game.logic.support.Random;
import game.logic.support.Util;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.logic.templates.ControllerGenerator;
import game.logic.templates.HTMLDecoder;
import game.logic.templates.NativeControllerGenerator;
import game.logic.templates.NativeSetupController;
import game.logic.templates.PartialListener;
import game.roles.Ability;
import game.roles.AbilityList;
import game.roles.Baker;
import game.roles.Blackmailer;
import game.roles.BreadAbility;
import game.roles.Burn;
import game.roles.Commuter;
import game.roles.CultLeader;
import game.roles.Douse;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.ElectroManiac;
import game.roles.FactionKill;
import game.roles.Framer;
import game.roles.GunAbility;
import game.roles.Hidden;
import game.roles.Jailor;
import game.roles.Janitor;
import game.roles.Marshall;
import game.roles.Mayor;
import game.roles.Operator;
import game.roles.Poisoner;
import game.roles.Punch;
import game.roles.PuppetPunch;
import game.roles.Tailor;
import game.roles.VestAbility;
import game.roles.Veteran;
import game.roles.Witch;
import game.roles.support.Bread;
import game.roles.support.Gun;
import game.roles.support.Vest;
import game.setups.Default;
import junit.framework.AssertionFailedError;
import junit.framework.ComparisonFailure;
import junit.framework.TestCase;
import models.FactionRole;
import nnode.Game;
import services.FactionRoleService;
import services.FactionService;
import services.HiddenService;
import util.HiddenUtil;
import util.RepeatabilityTest;
import util.TestChatUtil;

public abstract class SuperTest extends TestCase {

    protected static final String NULL_OPT = null;
    public static Random random = new Random();

    protected static final int SEND = Faction.SEND_;
    protected static final int KILL = Ability.NIGHT_KILL;
    protected static final int GUN = Ability.SHOOT;
    protected static final int ARMOR = Ability.VEST_ABILITY;

    public SuperTest(String name) {
        super(name);
    }

    public void roleInit() {
    }

    public static Narrator narrator;
    public static CustomRoleAssigner roleAssigner; // used for sleepwalker tests
    protected static ControllerGenerator generator;
    public static HashMap<Player, Controller> controllerMap = new HashMap<>();

    public static boolean printHappenings = false;
    public static boolean skipCurrentNativeTest = false;
    public static boolean BrainEndGame = true;
    public static boolean checkEnclosingChats = true;

    @Override
    public void runTest() throws Throwable {
        SuperTest.generator = new NativeControllerGenerator();
        nativeTest();

        SuperTest.skipCurrentNativeTest = false;
        SuperTest.checkEnclosingChats = true;
        SuperTest.skipTearDown = false;
    }

    private void nativeTest() throws Throwable {
        if(skipCurrentNativeTest)
            return;
        newNarrator();
        printHappenings = true;
        try{
            roleInit();
            Brain brain = new Brain(narrator, new Random().setSeed(0));
            super.runTest();
            if(narrator.isStarted() && BrainEndGame){
                List<NarratorListener> listeners = narrator.getListeners();
                for(int i = 0; i < listeners.size(); i++)
                    if(listeners.get(i) instanceof Game)
                        narrator.removeListener(listeners.get(i));
                brain.endGame();
            }
            BrainEndGame = true;
        }catch(NarratorException e){
            if(printHappenings)
                System.err.println(narrator.getHappenings());
            throw e;
        }catch(TestException e){
            e.printStackTrace();
            // writeToFile("log1.tx", n);
            throw e;
        }catch(Exception | Error e){
            TestChatUtil.reset();
            e.printStackTrace();
            if(printHappenings)
                System.err.println(narrator.getHappenings());
            throw e;
        }

        if(!skipTearDown && narrator.isStarted())
            RepeatabilityTest.runRepeatabilityTest();
    }

    public static HashMap<String, HashMap<String, Integer>> charges;
    public static NativeSetupController sEditor;

    protected void newNarrator() {
        generator.reset();

        sEditor = generator.getSetupController();
        narrator = generator.getNarrator();
        if(checkEnclosingChats)
            narrator.addListener(enclosingChatChecker());
        BasicRoles.narrator = narrator;
        BasicRoles.setup = new Default(narrator);
        TestChatUtil.reset();

        key = 1;

        narrator.setSeed(0);
        narrator.setRule(SetupModifier.CHARGE_VARIABILITY, 2);
        narrator.setRule(SetupModifier.CHAT_ROLES, true);
        charges = null;
        controllerMap.clear();
        narrator.addListener(new PartialListener() {
            private boolean ended = false;

            @Override
            public void onGameEnd() {
                if(ended)
                    throw new NarratorException("Game already ended!!");
                ended = true;
            }

            @Override
            public void onGameStart() {
                for(Player player: narrator._players){
                    TestChatUtil.addPlayer(player);
                }
            }

            @Override
            public void onAnnouncement(Message announcement) {
                for(Player player: TestChatUtil.getPlayers()){
                    onMessageReceive(player, announcement);
                }
            }

            @Override
            public void onMessageReceive(Player player, Message message) {
                TestChatUtil.onMessageReceive(player, message);
            }

            @Override
            public void onWarningReceive(Player player, Message warning) {
                TestChatUtil.onWarningReceive(player, warning);
            }
        });
    }

    public static boolean NativeTesting() {
        return generator instanceof NativeControllerGenerator;
    }

    public void checkEventEncapsulation() {
        for(Player p: narrator._players){
            for(Message playerEvent: p.getEvents()){
                for(String chat: playerEvent.getEnclosingChats(narrator, p)){
                    if(!mapsToEvent(p, chat) && !chat.equals(Feedback.CHAT_NAME)){
                        System.err.println(playerEvent);
                        System.err.println(Arrays.asList(playerEvent.getEnclosingChats(narrator, p)));
                        System.err.println(p.getChats());
                        mapsToEvent(p, chat);
                        playerEvent.getEnclosingChats(narrator, p);
                        p.getChats();
                        p.getEvents();
                        throw new TestException(p.getID());
                    }
                }
            }
        }
    }

    private boolean mapsToEvent(Player p, String s) {
        for(EventLog el: p.getChats()){
            if(el.getName().equals(s))
                return true;
            if(el.getName().startsWith("Day ") && s.startsWith("Day "))
                return true;
        }
        return false;
    }

    private PartialListener enclosingChatChecker() {
        return new PartialListener() {
            @Override
            public void onNightPhaseStart(PlayerList lynched, PlayerList poisoned, EventList events) {
                checkEventEncapsulation();
            }

            @Override
            public void onDayPhaseStart(PlayerList newDead) {
                checkEventEncapsulation();
            }
        };
    }

    public static boolean skipTearDown = false;

    @Override
    public void tearDown() throws Exception {
        SuperTest.charges = null;
        if(generator != null)
            generator.cleanup();
        super.tearDown();
    }

    public void writeRandoms(Narrator n1, Narrator n2) {
        writeToFile("input.tx", n1);
        writeToFile("output.tx", n2);
    }

    public static int key = 1;

    static String getSlaveName() {
        return Computer.NAME + Computer.toLetter(key++);
    }

    protected Player addPlayer(String name) {
        Controller c = generator.getActionController(name);
        Player player = narrator.getPlayerByID(name);

        controllerMap.put(player, c);
        return player;
    }

    protected Controller addPlayer(FactionRole factionRole) {
        String name = factionRole.role.getName().replaceAll(" ", "") + Computer.toLetter(key++);
        Controller c = generator.getActionController(name);
        Player p = narrator.getPlayerByID(name);
        p.rolePrefer(factionRole.role.name);
        p.teamPrefer(factionRole.getColor());
        sEditor.addHidden(HiddenUtil.createHiddenSingle(narrator, factionRole));
        controllerMap.put(p, c);
        return c;
    }

    protected ControllerList addPlayer(FactionRole role, int count) {
        ControllerList pl = new ControllerList();
        for(int i = 0; i < count; i++)
            pl.add(addPlayer(role));
        return pl;
    }

    protected void addPlayer(Hidden rt, int count) {
        for(int i = 0; i < count; i++)
            addPlayer(rt);
    }

    protected Player addPlayer(Hidden randomRole) {
        String name = getSlaveName();
        Controller c = generator.getActionController(name);
        Player p = narrator.getPlayerByID(name);
        sEditor.addHidden(randomRole);
        for(String s: randomRole.getColors()){
            p.teamPrefer(s);
        }
        for(FactionRole factionRole: randomRole.getFactionRoles())
            p.rolePrefer(factionRole.role.getName());

        controllerMap.put(p, c);
        return p;
    }

    public void setName(Controller c1, String string) {
        c1.setName(string);
    }

    protected void addRole(FactionRole role) {
        Hidden hidden = HiddenUtil.getHiddenSingle(narrator, role);
        if(hidden == null)
            hidden = HiddenUtil.createHiddenSingle(narrator, role);
        sEditor.addHidden(hidden);
    }

    // rename to createRole
    protected void createRole(Role m) {
        sEditor.createRole(m);
    }

    protected void addSetupHidden(Hidden r) {
        sEditor.addHidden(r);
    }

    protected void addRole(Hidden hidden, int quantity) {
        for(int i = 0; i < quantity; i++)
            sEditor.addHidden(hidden);
    }

    protected void addRole(FactionRole member, int quantity) {
        for(int i = 0; i < quantity; i++){
            sEditor.addHidden(HiddenUtil.createHiddenSingle(narrator, member));
        }
    }

    protected void deleteRole(Role role) {
        sEditor.deleteRole(role);
    }

    protected void deleteHidden(Hidden hidden) {
        sEditor.deleteHidden(hidden);
    }

    protected void createHidden(Hidden hidden) {
        sEditor.createHidden(hidden);
    }

    protected void removeSetupHidden(Hidden hidden) {
        narrator.removeSetupHidden(hidden);
    }

    protected void endNight(Controller... p) {
        for(Controller x: p){
            if(!x.getPlayer(narrator).endedNight())
                x.endNight();
        }
    }

    protected void cancelEndNight(Controller r) {
        r.cancelEndNight();
    }

    protected void setTarget(Controller person) {
        if(!narrator.isStarted())
            nightStart();
        int ability;
        if(person.is(Burn.class))
            ability = Burn.MAIN_ABILITY;
        else
            ability = person.getPlayer(narrator).getAbilities().get(0).getAbilityNumber();
        setTarget(person, ability, (String) null);
    }

    protected void setTarget(Controller person, Controller target) {
        if(!narrator.isStarted())
            nightStart();
        if(person.is(Baker.class))
            controllerMap.get(person).setNightTarget(Bread.COMMAND, target);
        else if(person.is(Douse.class))
            controllerMap.get(person).setNightTarget(Douse.COMMAND, target);
        else{
            String action = person.getPlayer(narrator).role._abilities.get(0).getCommand();
            controllerMap.get(person).setNightTarget(action, target);
        }
    }

    protected void setTarget(Controller person, Controller target, int ability) {
        setTarget(person, ability, null, null, (String) null, target);
    }

    protected void setTarget(Controller person, Controller target1, Controller target2) {
        if(!narrator.isStarted())
            nightStart();
        setTarget(person, person.getPlayer(narrator).getAbilities().get(0).getAbilityNumber(), (String) null,
                (String) null, target1, target2);
    }

    protected void setTarget(Controller person, ControllerList targets, int ability) {
        setTarget(person, ability, null, null, targets.toArray());
    }

    protected void setTarget(Controller person, String command, String option, String option2, Controller... targets) {
        setTarget(person, Ability.ParseAbility(command), option, option2, targets);
    }

    protected void setTarget(Controller person, String command, String option, String option2, String option3,
            Controller... targets) {
        setTarget(person, Ability.ParseAbility(command), option, option2, option3, targets);
    }

    protected void setTarget(Controller person, int ability, String option, Controller... targets) {
        setTarget(person, ability, option, null, targets);
    }

    protected void setTarget(Controller person, int ability, String option, String option2, ControllerList targets) {
        setTarget(person, ability, option, option2, targets.toArray());
    }

    protected void setTarget(Controller person, int ability, String option, String option2, String option3,
            ControllerList targets) {
        setTarget(person, ability, option, option2, option3, targets.toArray());
    }

    protected void setTarget(Controller person, int ability, String option, String option2, Controller... targets) {
        setTarget(person, ability, option, option2, null, targets);
    }

    protected void setTarget(Controller person, int ability, String option, String option2, String option3,
            Controller... targets) {
        if(!narrator.isStarted())
            nightStart();
        if(person.getPlayer(narrator).getAbility(ability) == null)
            throw new PlayerTargetingException("Ability not found");
        Player p = person.getPlayer(narrator);
        person.setNightTarget(p.getAbility(ability).getCommand(), option, option2, option3, targets);

        Action lastAction = p.getActions().getActions().get(p.getActions().size() - 1);
        p.getAbility(ability).parseCommand(p, p.getAbility(ability).getCommandParts(lastAction));
    }

    protected void setTarget(Player person, String command, String option, String option2, String option3,
            Player... targets) {
        if(!narrator.isStarted())
            nightStart();
        controllerMap.get(person).setNightTarget(command, option, option2, option3, targets);

        Action lastAction = person.getActions().getActions().get(person.getActions().size() - 1);
        if(person.getAbility(command) != null)
            person.getAbility(command).parseCommand(person, person.getAbility(command).getCommandParts(lastAction));
    }

    protected void cancelAction(Controller person, int index) {
        person.cancelAction(index);
    }

    protected void clearTargets(Controller person) {
        person.clearTargets();
    }

    protected void frame(Controller framer, Controller framed, String team) {
        if(!narrator.isStarted())
            nightStart();
        framer.setNightTarget(Framer.COMMAND, team, framed);
    }

    protected void spy(Controller spy, String team) {
        if(!narrator.isStarted())
            nightStart();
        spy.spy(team);
    }

    protected void drive(Controller driver, Controller t1, Controller t2) {
        if(!narrator.isStarted())
            nightStart();
        setTarget(driver, Driver.MAIN_ABILITY, null, t1, t2);
    }

    protected void tele(Controller op, Controller... target) {
        if(!narrator.isStarted())
            nightStart();
        setTarget(op, Operator.MAIN_ABILITY, null, null, target);
    }

    protected void witch(Controller witch, Controller target, Controller victimTarget) {
        if(!narrator.isStarted())
            nightStart();
        setTarget(witch, Witch.MAIN_ABILITY, null, target, victimTarget);
    }

    protected void jail(Controller jailor, Controller f1) {
        if(!narrator.isStarted())
            dayStart();
        doDayAction(jailor, Jailor.COMMAND, f1);
    }

    protected void arch(Controller arc, Controller... targets) {
        if(!narrator.isStarted())
            dayStart();
        doDayAction(arc, targets);
    }

    protected void drug(Controller dd, Controller arch, String drug) {
        setTarget(dd, DrugDealer.MAIN_ABILITY, drug, arch);
    }

    protected void tailor(Controller tailor, Controller target, Controller template) {
        if(!narrator.isStarted())
            nightStart();
        setTarget(tailor, Tailor.MAIN_ABILITY, template.getColor(), template.getPlayer(narrator).getRoleName(), target);
    }

    protected void vest(Controller p) {
        if(!narrator.isStarted())
            nightStart();
        p.vest();
    }

    protected void burn(Controller arso) {
        if(!narrator.isStarted())
            nightStart();
        if(narrator.isNight())
            arso.setNightTarget();
        else
            arso.doDayAction(Burn.COMMAND, null);
    }

    protected void commute(Controller commuter) {
        if(!narrator.isStarted())
            nightStart();
        commuter.setNightTarget(Commuter.COMMAND);
    }

    protected void alert(Controller veteran) {
        if(!narrator.isStarted())
            nightStart();
        controllerMap.get(veteran).setNightTarget(Veteran.COMMAND);
    }

    protected void reveal(Controller mayor) {
        if(!narrator.isStarted())
            dayStart();
        controllerMap.get(mayor).doDayAction(Mayor.COMMAND, null);
    }

    protected void order(Controller marshall) {
        if(!narrator.isStarted())
            dayStart();
        controllerMap.get(marshall).doDayAction(Marshall.COMMAND, null);
    }

    protected void skipDay() {
        for(Player p: narrator.getAllPlayers()){
            if(!narrator.isDay())
                return;
            Player voteTarget = narrator.voteSystem.getVoteTarget(p);
            if(voteTarget != narrator.skipper && !p.isPuppeted() && p.isAlive())
                controllerMap.get(p).skipVote();
            if(p.hasPuppets()){
                for(Player puppet: p.getPuppets())
                    controllerMap.get(p).ventSkipVote(puppet);
            }
        }
    }

    protected void lynch(Controller target, ControllerList voters) {
        voteOut(target, voters.toArray());
    }

    protected void voteOut(Controller target, Controller... voters) {
        if(!narrator.isStarted())
            dayStart();
        for(Controller v: voters){
            if(narrator.isDay())
                vote(v, target);
            else
                break;
        }
    }

    protected void endPhase() {
        narrator.endPhase();
    }

    protected void endNight() {
        for(Player p: narrator.getNightWaitingOn())
            controllerMap.get(p).endNight();
    }

    protected void endDay() {
        narrator.forceEndDay();
    }

    protected void nextNight() {
        endNight();
        skipDay();
    }

    protected void nextDay() {
        skipDay();
        endNight();
    }

    public static List<Feedback> getFeedback(Controller p, int day) {
        return TestChatUtil.getFeedbackMessages(p.getPlayer(narrator), day);
    }

    public static void assertEquals(Object s1, Object s2) {
        try{
            junit.framework.TestCase.assertEquals(s1, s2);
        }catch(AssertionFailedError e){
            if(printHappenings)
                System.out.println(narrator.getHappenings());
            if(s1 == null)
                s1 = "null";
            if(s2 == null)
                s2 = "null";
            throw new ComparisonFailure(Long.toString(narrator.getSeed()), s1.toString(), s2.toString());
        }
    }

    public static void assertEquals(Narrator n1, Narrator n2) {
        try{
            junit.framework.TestCase.assertEquals(n1, n2);
        }catch(AssertionFailedError e){
            writeToFile("input.txt", n1);
            writeToFile("output.txt", n2);
            // System.out.println(n1.getHappenings());
            // System.out.println("#");
            // System.out.println(n2.getPrivateEvents(false));
            throw new ComparisonFailure("", n1.toString(), n2.toString());
        }
    }

    public static void assertEquals(boolean s2, boolean s1) {
        try{
            junit.framework.TestCase.assertEquals(s1, s2);
        }catch(AssertionFailedError e){
            throw new AssertionFailedError(s2 + " but was " + s1);
        }
    }

    public static void assertTrue(boolean b) {
        try{
            junit.framework.TestCase.assertTrue(b);
        }catch(AssertionFailedError e){
            if(printHappenings)
                System.out.println(narrator.getHappenings());
            throw new AssertionFailedError();
        }
    }

    public static void assertContains(Collection<? extends Object> list, Object... object) {
        for(Object o: object)
            assertTrue(list.contains(o));
    }

    public static void assertContains(ControllerList list, Controller... players) {
        for(Controller p: players)
            assertTrue(list.contains(p));
    }

    public static void assertContains(PlayerList list, Player... players) {
        for(Player p: players)
            assertTrue(list.contains(p));
    }

    public static void assertFalse(boolean b) {
        try{
            junit.framework.TestCase.assertFalse(b);
        }catch(AssertionFailedError e){
            System.out.print(narrator.getHappenings());
            throw new AssertionFailedError();
        }
    }

    public static void fail(long t) {
        fail(Long.toString(t));
    }

    public static void fail(String q) {
        if(printHappenings)
            System.out.print(narrator.getHappenings());
        TestCase.fail(q);
    }

    public static void fail() {
        fail("");
    }

    public static void assertEquals(int s1, int s2) {
        try{
            junit.framework.TestCase.assertEquals(s2, s1);
        }catch(AssertionFailedError e){
            if(printHappenings)
                System.out.println(narrator.getHappenings());
            TestCase.assertEquals(s1, s2);
        }
    }

    public static void assertEqual(int s1, int s2) {
        assertEquals(s1, s2);
    }

    protected static void assertNotSameAbilities(Controller p, Controller q) {
        AbilityList a1 = p.getPlayer(narrator).getAbilities();
        AbilityList a2 = q.getPlayer(narrator).getAbilities();
        assertFalse(Ability.isEqualContent(a1, a2));
    }

    protected static void isWinner(Controller... ps) {
        for(Controller p: ps){
            try{
                assertTrue(p.getPlayer(narrator).isWinner());
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer(narrator).getDescription() + " is a loser!");
            }
        }
    }

    protected static void isLoser(Controller... ps) {
        for(Controller p: ps){
            try{
                assertFalse(p.getPlayer(narrator).isWinner());
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer(narrator).getDescription() + " is a winner!");
            }
        }
    }

    protected static void assertNoAcceptableTargets(Controller c, String command) {
        PlayerList acceptableTargets = c.getPlayer(narrator).getAcceptableTargets(command);
        assertNotNull(acceptableTargets);
        assertTrue(acceptableTargets.isEmpty());
    }

    protected static void isAlive(Controller... ps) {
        for(Controller p: ps){
            try{
                assertTrue(p.getPlayer(narrator).isAlive());
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer(narrator).getDescription() + " came up dead!");
            }
        }
    }

    protected static void isDead(Controller... ps) {
        for(Controller p: ps){
            try{
                assertFalse(p.getPlayer(narrator).isAlive());
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer(narrator).getDescription() + " came up alive!");
            }
        }
    }

    protected static void isInvuln(Controller c) {
        isInvuln(c, true);
    }

    protected static void isInvuln(Controller c, boolean isInvuln) {
        assertEquals(isInvuln, c.getPlayer(narrator).isInvulnerable());
    }

    protected static void assertVoteTarget(Controller voteTargetC, Controller voterC) {
        Player voter = voterC.getPlayer(narrator);
        ;
        Player voteTarget;
        if(voteTargetC == null)
            voteTarget = null;
        else if(voteTargetC == narrator.skipper)
            voteTarget = narrator.skipper;
        else
            voteTarget = voteTargetC.getPlayer(narrator);

        assertEquals(voteTarget, narrator.voteSystem.getVoteTarget(voter));
    }

    protected static void assertVotePower(int count, Controller p) {
        assertEquals(count, p.getPlayer(narrator).getVotePower());
    }

    protected static void hasSuits(Controller... ps) {
        for(Controller p: ps){
            try{
                assertStatus(p, Tailor.MAIN_ABILITY);
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer(narrator).getDescription() + " doesn't have suits!");
            }
        }
    }

    protected static void isCharged(Controller... ps) {
        for(Controller p: ps){
            try{
                assertStatus(p, ElectroManiac.MAIN_ABILITY);
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer(narrator).getDescription() + " isn't charged!");
            }
        }
    }

    protected ArrayList<Message> addAnnouncementListener() {
        AnnouncementListener aListener = new AnnouncementListener();
        narrator.addListener(aListener);
        return aListener.announcements;
    }

    class AnnouncementListener extends PartialListener {
        public ArrayList<Message> announcements;

        public AnnouncementListener() {
            announcements = new ArrayList<>();
        }

        @Override
        public void onAnnouncement(Message nl) {
            announcements.add(nl);
        }
    }

    protected static void assertGameOver() {
        assertFalse(narrator.isInProgress());

    }

    protected static void dayStart() {
        sEditor.changeRule(SetupModifier.DAY_START, Narrator.DAY_START);
        startGame();
    }

    protected void restartGame() {
        sEditor.restartGame();
        startGame();
    }

    protected void startDay() {
        dayStart();
    }

    protected void startNight() {
        nightStart();
    }

    protected static void assertBadGameSettings() {
        try{
            sEditor.startGame();
            fail();
        }catch(IllegalGameSettingsException | IllegalRoleCombinationException e){
        }
    }

    protected static void nightStart() {
        sEditor.changeRule(SetupModifier.DAY_START, Narrator.NIGHT_START);
        startGame();
    }

    private static void startGame() {
        for(Hidden hidden: new LinkedList<>(narrator.hiddens)){
            if(!narrator.rolesList.contains(hidden))
                HiddenService.delete(narrator, hidden);
        }
        for(Faction faction: narrator.getFactions()){
            for(FactionRole factionRole: new HashSet<>(faction._roleSet.values())){
                if(!narrator.rolesList.contains(factionRole))
                    FactionService.deleteFactionRole(factionRole);
            }
        }
        sEditor.startGame();

        collectCharges();
    }

    private static void collectCharges() {
        charges = new HashMap<>();
        HashMap<String, Integer> subChargeMap;
        for(Player p: narrator.getAllPlayers()){
            subChargeMap = new HashMap<>();
            for(Ability a: p.getAbilities()){
                subChargeMap.put(a.getCommand(), a.getPerceivedCharges());
            }
            charges.put(p.getID(), subChargeMap);
        }
    }

    protected void simulationTest(long t) {
        Random r = new Random();
        r.setSeed(t);
        Brain b = new Brain(narrator, r);
        b.talkingEnabled = false;
        b.setNarrator(narrator);
        try{
            b.endGame();
        }catch(UnknownTeamException e){
            e.printStackTrace();
            fail(t + "");
        }
    }

    public static void writeToFile(String s, Narrator n) {
        try{
            FileWriter in = new FileWriter(new File(s));
            // in.write(CodeWriter.getCode(n));
            // in.write(getCommandsList(n));
            in.write(n.getHappenings());
            // in.write(n.getRandom().getLog());
            in.close();
        }catch(IOException | NullPointerException e){
            e.printStackTrace();
        }
    }

    protected void partialContains(String partialFeedback) {
        for(Player p: narrator._players)
            partialContains(p, partialFeedback, 1);
    }

    protected static void partialContains(Controller p, String partialFeedback) {
        partialContains(p, partialFeedback, 1);
    }

    protected static void partialContains(Controller controller, String partialFeedback, int count) {
        Player player = controller.getPlayer(narrator);
        List<Message> messages = TestChatUtil.getMessages(player);
        messages = TestChatUtil.filterByText(messages, player, partialFeedback);
        assertEquals(count, messages.size());
    }

    protected static void partialContains(List<String> comm, String partialFeedback, int count) {
        assertEquals(count, Util.partialContains(comm, partialFeedback));
    }

    protected static void partialIncludes(ArrayList<String> list, String partialFeedback) {
        partialContains(list, partialFeedback);
    }

    protected static void partialExcludes(Controller controller, String partialFeedback) {
        Player player = controller.getPlayer(narrator);
        List<Message> messages = TestChatUtil.getMessages(player);
        messages = TestChatUtil.filterByText(messages, player, partialFeedback);
        assertTrue(messages.isEmpty());
    }

    protected void partialExcludes(List<String> comm, String partialFeedback) {
        partialContains(comm, partialFeedback, 0);
    }

    protected static void partialContains(List<String> comm, String partialFeedback) {
        partialContains(comm, partialFeedback, 1);
    }

    protected Action getVestAction(Controller p) {
        return new Action(p.getPlayer(narrator), Ability.VEST_ABILITY);
    }

    protected void shoot(Controller shooter, Controller target) {
        if(!narrator.isStarted())
            nightStart();
        if(narrator.isDay())
            shooter.doDayAction(Gun.COMMAND, null, target);
        else
            shooter.setNightTarget(Gun.COMMAND, target);
    }

    protected void bread(Controller breader, Controller target) {
        breader.setNightTarget(Bread.COMMAND, target);
    }

    public void send(Controller sender, Controller... target) {
        if(!narrator.isStarted())
            nightStart();
        sender.setNightTarget(Faction.SEND, target);
    }

    public void mafKill(Controller sender, Controller target) {
        if(!narrator.isStarted())
            nightStart();
        sender.setNightTarget(FactionKill.COMMAND, target);
    }

    protected void electrify(Controller electro, Controller... p) {
        for(Controller target: p){
            setTarget(electro, target);
            nextNight();
        }
    }

    protected void command(Controller p, String... commands) {
        StringBuilder sb = new StringBuilder();
        for(String c: commands){
            sb.append(c);
            sb.append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);
        new CommandHandler(narrator).command(p.getPlayer(narrator), sb.toString(), p.getName());
    }

    public void setSetup(String setupName) {
        sEditor.setSetup(setupName);
    }

    public void setAllies(FactionRole m1, FactionRole m2) {
        sEditor.setAllies(m1.getColor(), m2.getColor());
    }

    public void setEnemies(FactionRole m1, FactionRole m2) {
        sEditor.setEnemies(m1.getColor(), m2.getColor());
    }

    public void addSheriffDetectable(FactionRole m1, FactionRole m2) {
        this.addSheriffDetectable(m1.getColor(), m2.getColor());
    }

    public void addSheriffDetectable(String t1, String t2) {
        sEditor.addSheriffDetectable(t1, t2);
    }

    public void removeSheriffDetectable(String t1, String t2) {
        sEditor.removeSheriffDetectable(t1, t2);
    }

    // TODO figure out how to set alias for front end
    public void setAlias(String key, String value) {
        narrator.setAlias(key, value);
    }

    public static void editRule(SetupModifier ruleArr, boolean b) {
        sEditor.changeRule(ruleArr, b);
    }

    public static void editRule(SetupModifier ruleArr, int i) {
        sEditor.changeRule(ruleArr, i);
    }

    public static void modifyRole(FactionRole factionModifier, RoleModifier modifierID, int val) {
        sEditor.modifyRole(factionModifier.role, modifierID, val);
    }

    public static void modifyRole(FactionRole m, AbilityModifier modifierID, int val) {
        modifyFactionRoleAbility(m, modifierID, m.role.getAbility().getClass(), val);
    }

    public static void modifyRole(FactionRole m, RoleModifier modifierID, boolean val) {
        sEditor.modifyRole(m.role, modifierID, val);
    }

    // rename to modifyAbility
    public static void modifyRoleAbility(Role role, AbilityModifier modifierID, Class<? extends Ability> abilityClass,
            boolean val) {
        sEditor.modifyRoleAbility(role, modifierID, abilityClass, val);
    }

    public static void modifyFactionRoleAbility(FactionRole factionRole, AbilityModifier modifierID,
            Class<? extends Ability> abilityClass, int val) {
        sEditor.modifyRoleAbility(factionRole.role, modifierID, abilityClass, val);
    }

    public static void modifyAbilitySelfTarget(FactionRole factionRole, boolean val) {
        Class<? extends Ability> firstClass = factionRole.role.getBaseAbility().getFirst().getClass();
        modifyAbilitySelfTarget(factionRole, firstClass, val);
    }

    public static void modifyAbilitySelfTarget(FactionRole factionRole, Class<? extends Ability> abilityClass,
            boolean val) {
        FactionRoleService.addModifier(factionRole, AbilityModifier.SELF_TARGET, abilityClass, val);
    }

    public static void modifyAbilityCharges(FactionRole m, int val) {
        sEditor.modifyRoleAbility(m.role, AbilityModifier.CHARGES, m.role.getAbility().getClass(), val);
    }

    public static void modifyAbilityCharges(FactionRole factionRole, Class<? extends Ability> abilityClass, int val) {
        sEditor.modifyRoleAbility(factionRole.role, AbilityModifier.CHARGES, abilityClass, val);
    }

    public static void modifyAbilityCooldown(FactionRole m, int val) {
        modifyFactionRoleAbility(m, AbilityModifier.COOLDOWN, m.role.getAbility().getClass(), val);
    }

    public static void modifyAbilityCooldown(FactionRole m, Class<? extends Ability> abilityClass, int val) {
        modifyFactionRoleAbility(m, AbilityModifier.COOLDOWN, abilityClass, val);
    }

    public static void modifyRole(Role role, AbilityModifier modifierID, boolean val) {
        if(role.getAbilityCount() != 1)
            throw new UnsupportedMethodException();

        modifyRoleAbility(role, modifierID, role.getAbility().getClass(), val);
    }

    public void addTeamAbility(String color, Class<? extends Ability> abilityClass) {
        sEditor.addTeamAbility(color, abilityClass.getSimpleName());
    }

    public void removeTeamAbility(String color, Class<? extends Ability> abilityClass) {
        sEditor.removeTeamAbility(color, abilityClass.getSimpleName());
    }

    public void modifyTeamAbility(String color, AbilityModifier modifier, Class<? extends Ability> abilityClass,
            boolean b) {
        sEditor.modifyTeamAbility(color, modifier, abilityClass, b);
    }

    public void modifyTeamAbility(String color, AbilityModifier modifier, Class<? extends Ability> abilityClass,
            int i) {
        sEditor.modifyTeamAbility(color, modifier, abilityClass, i);
    }

    public static void setTeamRule(String team, FactionModifier rule, int val) {
        sEditor.setTeamRule(team, rule, val);
    }

    public void setTeamRule(Faction faction, FactionModifier rule, int val) {
        setTeamRule(faction.getColor(), rule, val);
    }

    public void setTeamRule(Faction faction, FactionModifier rule, boolean val) {
        setTeamRule(faction.getColor(), rule, val);
    }

    public void setTeamRule(String team, FactionModifier rule, boolean val) {
        sEditor.setTeamRule(team, rule, val);
    }

    public void assertIsDay() {
        assertTrue(narrator.isDay());
    }

    public void assertIsNight() {
        assertTrue(narrator.isNight());
    }

    public void assertInProgress() {
        assertTrue(narrator.isInProgress());
    }

    public void assertDayActionSize(int i) {
        assertEquals(i, narrator.actionStack.size());
    }

    public void vote(Controller voter, Controller target) {
        if(!narrator.isStarted())
            dayStart();
        voter.vote(target);
    }

    public void punch(Controller voter, Controller... targets) {
        if(!narrator.isStarted())
            dayStart();
        voter.doDayAction(Punch.COMMAND, null, targets);
    }

    public void skipVote(Controller... voters) {
        if(!narrator.isStarted())
            dayStart();
        for(Controller voter: voters)
            voter.skipVote();
    }

    public void unvote(Controller voter) {
        if(!narrator.isStarted())
            dayStart();
        voter.unvote();
    }

    public void ventVote(Controller puppet, Controller target, Controller vent) {
        if(!narrator.isStarted())
            dayStart();
        vent.ventVote(puppet, target);
    }

    public void ventPunch(Controller puncher, Controller target, Controller vent) {
        if(!narrator.isStarted())
            dayStart();
        vent.doDayAction(PuppetPunch.COMMAND, null, puncher, target);
    }

    public void ventSkipVote(Controller voter, Controller vent) {
        if(!narrator.isStarted())
            dayStart();
        vent.ventSkipVote(voter);
    }

    public static void ventUnvote(Controller voter, Controller vent) {
        if(!narrator.isStarted())
            dayStart();
        vent.ventUnvote(voter);
    }

    public static void hasPuppets(Controller c) {
        hasPuppets(c, true);
    }

    public static void isPuppeted(Controller c) {
        isPuppeted(c, true);
    }

    public static void hasPuppets(Controller c, boolean hasPuppets) {
        if(hasPuppets)
            assertTrue(c.getPlayer(narrator).hasPuppets());
        else
            assertFalse(c.getPlayer(narrator).hasPuppets());
    }

    public static void isPuppeted(Controller c, boolean isPuppeted) {
        if(isPuppeted)
            assertTrue(c.getPlayer(narrator).isPuppeted());
        else
            assertFalse(c.getPlayer(narrator).isPuppeted());
    }

    public void doDayAction(Controller p, Controller... targets) {
        if(!narrator.isStarted())
            dayStart();
        String roleDayAbility = p.getPlayer(narrator).getAbilities().getDayAbilities().get(0).getCommand();
        doDayAction(p, roleDayAbility, targets);
    }

    public void doDayAction(Controller p, String action, Controller... targets) {
        if(!narrator.isStarted())
            dayStart();
        p.doDayAction(action, null, targets);
    }

    public void doDayAction(Controller p, int ability, String option, Controller... targets) {
        if(!narrator.isStarted())
            dayStart();
        String command = Ability.GetAbilityCopy(ability).getCommand();
        p.doDayAction(command, option, targets);
    }

    public ChatMessage say(Controller c1, String messageText, String key) {
        int preCommandSize = narrator.getCommands().size();
        ChatMessage message = c1.say(messageText, key);
        assertEquals(preCommandSize + 1, narrator.getCommands().size());
        return message;
    }

    public void assertFactionController(Controller maf, String c) {
        assertFactionController(maf, narrator.getFaction(c), FactionKill.MAIN_ABILITY);
    }

    public void assertFactionController(Controller maf, String c, int ability) {
        assertFactionController(maf, narrator.getFaction(c), ability);
    }

    public void assertFactionController(Controller maf, Faction team) {
        assertFactionController(maf, team, FactionKill.MAIN_ABILITY);
    }

    public void assertFactionController(Controller maf, Faction team, int ability) {
        assertEquals(maf.getPlayer(narrator), team.getCurrentController(ability));
    }

    public static void assertStatus(Controller c, int status) {
        assertStatus(c, status, true);
    }

    public static void assertStatus(Controller c, int status, boolean hasStatus) {
        Player p = c.getPlayer(narrator);
        switch (status){
        case Poisoner.MAIN_ABILITY:
            assertEquals(hasStatus, p.isPoisoned());
            return;
        case CultLeader.MAIN_ABILITY:
            assertEquals(hasStatus, p.isCulted());
            return;
        case Douse.MAIN_ABILITY:
            assertEquals(hasStatus, p.isDoused());
            return;
        case Blackmailer.MAIN_ABILITY:
            assertEquals(hasStatus, p.isSilenced());
            assertEquals(hasStatus, p.isDisenfranchised());
            return;
        case Janitor.MAIN_ABILITY:
            if(p.isDead())
                assertEquals(hasStatus, p.getDeathType().isCleaned());
            else
                assertEquals(hasStatus, !p.getCleaners().isEmpty());
            return;
        case Jailor.MAIN_ABILITY:
            assertEquals(hasStatus, p.isJailed());
            return;
        case ElectroManiac.MAIN_ABILITY:
            assertEquals(hasStatus, p.isCharged());
            return;
        case Tailor.MAIN_ABILITY:
            assertEquals(hasStatus, p.hasSuits());
            return;
        }
        fail("unknown status");
    }

    public void assertAttackSize(int size, Controller p) {
        assertEquals(size, p.getPlayer(narrator).getDeathType().size());
    }

    public void assertActionSize(int size, Controller p) {
        assertEquals(size, p.getPlayer(narrator).getActions().size());
    }

    public void assertUseableBreadCount(int breadNumber, Controller breaded) {
        assertEquals(breadNumber, BreadAbility.getUseableBread(breaded.getPlayer(narrator)));
    }

    public void assertPassableBreadCount(int breadNumber, Controller c) {
        assertEquals(breadNumber, BreadAbility.getPassableBread(c.getPlayer(narrator)));
    }

    public void assertFakeGunCount(int assertedGunCount, Controller gunHolder) {
        if(!gunHolder.getPlayer(narrator).hasAbility(GunAbility.class))
            assertEquals(0, assertedGunCount);
        assertEquals(assertedGunCount, gunHolder.getPlayer(narrator).getAbility(GunAbility.class).getFakeCharges());
    }

    public void assertFakeVestCount(int assertedVestCount, Controller c2) {
        if(!c2.getPlayer(narrator).hasAbility(VestAbility.class))
            assertEquals(0, assertedVestCount);
        assertEquals(assertedVestCount, c2.getPlayer(narrator).getAbility(VestAbility.class).getFakeCharges());
    }

    // TODO rename to perceived
    public void assertTotalVestCount(int assertedVestCount, Controller vestHolder) {
        Player vestController = vestHolder.getPlayer(narrator);
        if(!vestController.hasAbility(VestAbility.class))
            assertEquals(0, assertedVestCount);
        else
            assertEquals(assertedVestCount, vestController.getAbility(VestAbility.class).getPerceivedCharges());
    }

    public void assertRealVestCount(int assertedVestCount, Controller vestHolder) {
        if(!vestHolder.getPlayer(narrator).hasAbility(VestAbility.class))
            assertEquals(0, assertedVestCount);
        assertEquals(assertedVestCount, vestHolder.getPlayer(narrator).getAbility(VestAbility.class).getRealCharges());
    }

    public void assertRealGunCount(int assertedGunCount, Controller gunHolder) {
        if(!gunHolder.getPlayer(narrator).hasAbility(GunAbility.class))
            assertEquals(0, assertedGunCount);
        assertEquals(assertedGunCount, gunHolder.getPlayer(narrator).getAbility(GunAbility.class).getRealCharges());
    }

    public void assertTotalGunCount(int gunNumber, Controller gunned) {
        assertEquals(gunNumber, GunAbility.getTotalGunCount(gunned.getPlayer(narrator)));
    }

    public void assertPerceivedAutovestCount(int count, Controller c) {
        assertEquals(count, c.getPlayer(narrator).getPerceivedAutoVestCount());
    }

    public void assertRealChargeRemaining(int val, Controller p) {
        assertRealChargeRemaining(val, p, p.getPlayer(narrator).getFirstAbility().getAbilityNumber());
    }

    public void assertRealChargeRemaining(int val, Controller p, int command) {
        assertEquals(val, p.getPlayer(narrator).getAbility(command).getRealCharges());
    }

    public void assertPerceivedChargeRemaining(int val, Controller c, int ability) {
        assertEquals(val, c.getPlayer(narrator).getAbility(ability).getPerceivedCharges());
    }

    public void assertPerceivedChargeRemaining(int val, Controller c) {
        assertEquals(val, c.getPlayer(narrator).getFirstAbility().getPerceivedCharges());
    }

    public void assertCooldownRemaining(int val, Controller p) {
        assertEquals(val, p.getPlayer(narrator).getFirstAbility().getRemainingCooldown());
    }

    public void assertCooldownRemaining(int val, Controller p, Class<? extends Ability> c) {
        assertEquals(val, p.getPlayer(narrator).getAbility(c).getRemainingCooldown());
    }

    public void assertChatKeysSize(int val, Controller p) {
        this.assertChatKeys(val, p);
    }

    public void assertChatKeys(int val, Controller p) {
        assertEquals(val, p.getChatKeys().size());
    }

    public void assertChatLogSize(int val, Controller p) {
        assertEquals(val, p.getPlayer(narrator).getChats().size());
    }

    public void assertSuited(Controller dead, Controller looksLike) {
        isDead(dead);
        DeathAnnouncement da;
        String access;
        for(Message m: narrator.getEventManager().getEvents(Message.PUBLIC)){
            if(m instanceof DeathAnnouncement){
                da = (DeathAnnouncement) m;
                if(da.dead.contains(dead.getPlayer(narrator))){
                    access = m.access(Message.PUBLIC, new HTMLDecoder());
                    assertTrue(access.contains(looksLike.getPlayer(narrator).getColor()));
                    assertTrue(access.contains(looksLike.getPlayer(narrator).getRoleName()));

                    access = m.access(Message.PRIVATE, new HTMLDecoder());
                    assertTrue(access.contains(dead.getColor()));
                    assertTrue(access.contains(dead.getPlayer(narrator).getRoleName()));
                    return;
                }
            }
        }
        fail();
    }

    protected static String gibberish;
    static final int len = 40;

    protected String gibberish() {
        gibberish = Util.getRandomString(len);
        return gibberish;
    }

    public Gun getGun(Controller shooter, int i) {
        return shooter.getPlayer(narrator).getAbility(GunAbility.class).get(i);
    }

    public Vest getVest(Controller vester, int i) {
        return vester.getPlayer(narrator).getAbility(VestAbility.class).get(i);
    }
}
