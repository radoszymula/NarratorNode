package integration.logic;

import game.ai.Controller;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import models.enums.GamePhase;
import models.enums.VoteSystemTypes;

public class TestDiminishingPoolVoteSystem extends SuperTest{
	
	public TestDiminishingPoolVoteSystem(String name) {
		super(name);
	}
	
	Controller p1, p2, p3, p4, p5, p6, p7, p8;
	@Override
	public void roleInit(){
		editRule(SetupModifier.VOTE_SYSTEM, VoteSystemTypes.DIMINISHING_POOL);
		editRule(SetupModifier.TRIAL_LENGTH, 1);
		
		p1 = addPlayer(BasicRoles.Citizen());
		p2 = addPlayer(BasicRoles.Goon());
		p3 = addPlayer(BasicRoles.Godfather(Setup.YAKUZA_C));
		p4 = addPlayer(BasicRoles.SerialKiller());
		p5 = addPlayer(BasicRoles.CultLeader());
		p6 = addPlayer(BasicRoles.Survivor());
		p7 = addPlayer(BasicRoles.Survivor());
		p8 = addPlayer(BasicRoles.Survivor());
		SuperTest.BrainEndGame = false;
    	SuperTest.skipTearDown = true;
	}

	public void testVoteDiminishingPoolHappyPaths() {
    	voteOut(p8, p1, p2, p3, p4, p5);
    	voteOut(p7, p6, p8);
    	
    	endPhase();
    	
    	assertEquals(GamePhase.TRIAL_PHASE, narrator.phase);
    	Player trialedPlayer = SuperTest.narrator.voteSystem.getPlayerOnTrial();
    	assertTrue(trialedPlayer == p7 || trialedPlayer == p8);
    	assertEquals(p7, narrator.voteSystem.getVoteTarget(p8.getPlayer(narrator)));
    	
    	try {
    		vote(p1, p2);
    		fail();
    	}catch(NarratorException e) {}
    	
    	endPhase();

    	assertEquals(GamePhase.TRIAL_PHASE, narrator.phase);
    	Player trialedPlayer2 = SuperTest.narrator.voteSystem.getPlayerOnTrial();
    	assertNotSame(trialedPlayer2, trialedPlayer);
    	assertTrue(trialedPlayer2 == p7 || trialedPlayer2 == p8);
    	
    	endPhase();
    	
    	assertEquals(GamePhase.VOTE_PHASE, narrator.phase);
    	try {
    		vote(p1, p2);
    		fail();
    	}catch(NarratorException e) {}
    	
    	voteOut(p8, p1, p2, p3, p7);
    	skipVote(p5, p6, p8);
    	endPhase();
    	
    	isDead(p8);
    	assertIsNight();
    }
	
	public void testNoRevote() {
		vote(p1, p8);
		
		try {
			vote(p1, p8);
			fail();
		}catch(NarratorException e) {}
	}
	
	public void testEndingGameInTie() {
		editRule(SetupModifier.SKIP_VOTE, false);
		voteOut(p8, p1, p3, p5, p7);
		voteOut(p7, p2, p4, p6, p8);
		
		endPhase();
		//trial
		endPhase();
		//trial
		endPhase();
		
		endPhase();
		endPhase();
		endPhase();
		
		isLoser(p1, p2, p3, p4, p5, p6, p7, p8);
		isDead(p1, p2, p3, p4, p5, p6, p7, p8);
		assertGameOver();
	}
	
	public void testNoVotesAfterTrialThenSkip() {
		editRule(SetupModifier.SKIP_VOTE, true);
		voteOut(p8, p1, p3, p5, p7);
		voteOut(p7, p2, p4, p6, p8);
		
		endPhase();
		//trial
		endPhase(); //ending trial of p8/p7
		//trial
		endPhase(); //ending trial of p8/p7
		
		endPhase();
		
		assertIsNight();
	}
	
	public void testNoSkipperOnTrial() {
		voteOut(p8, p1, p3, p5, p7);
		skipVote(p2, p4, p6, p8);
		
		endPhase();
		//trial
		endPhase(); //ends p8 trial

		assertEquals(GamePhase.VOTE_PHASE, narrator.phase);

		vote(p1, p2);
		
		voteOut(p8, p1, p2, p3, p4, p5);
		endPhase();
		
		isDead(p8);
		assertIsNight();
	}
	
	public void testMarshalWithPlurality() {
		Controller marshall = addPlayer(BasicRoles.Marshall());
		editRule(SetupModifier.MARSHALL_EXECUTIONS, 1);
		
		doDayAction(marshall);
		
		voteOut(p8, p1, p3, p5, p7);
		endPhase();
		endPhase();//ends p8 trial
		
		voteOut(p8, p1, p2, p3, p4, p5);
		endPhase();
		
		isDead(p8);
		assertIsDay();
		
		voteOut(marshall, p1, p2, p3);
		endPhase();
		
		assertEquals(GamePhase.TRIAL_PHASE, narrator.phase);
		
		endPhase();//end marshall trial
		
		voteOut(marshall, p1);
		endPhase();
		
		isDead(marshall);
		assertIsNight();
	}
	
	public void testEliminatingLowestVote() {
		vote(p1, p5);
		vote(p2, p5);
		vote(p3, p7);
		vote(p4, p7);
		vote(p5, p8);
		vote(p6, p8);
		vote(p7, p6);
		vote(p8, p6);
		
		endPhase();
		assertEquals(GamePhase.TRIAL_PHASE, narrator.phase);
		endPhase();//end trial1
		assertEquals(GamePhase.TRIAL_PHASE, narrator.phase);
		endPhase();//end trial2
		assertEquals(GamePhase.TRIAL_PHASE, narrator.phase);
		endPhase();//end trial3
		assertEquals(GamePhase.TRIAL_PHASE, narrator.phase);
		endPhase();//end trial4
		assertEquals(GamePhase.VOTE_PHASE, narrator.phase);
		
		assertIsDay();
		
		vote(p1, p8);
		vote(p2, p8);
		vote(p3, p8);
		vote(p4, p7);
		vote(p5, p7);
		vote(p6, p7);
		vote(p7, p6);
		vote(p8, p6);
		
		endPhase();
		
		try {
			vote(p4, p6);
			fail();
		}catch(NarratorException e) {}
	}
	
	public void testUnvoting() {
		vote(p1, p8);
		unvote(p1);
		voteOut(p8, p2);
		voteOut(p7, p3, p4, p5);
		endPhase();
		endPhase(); //ends p7 trial
		
		assertEquals(GamePhase.VOTE_PHASE, narrator.phase);
	}
	
	public void testSkipOneVoting() {
		skipVote(p1);
		endPhase();
		
		assertIsNight();
	}
	
	public void testSkipNoVoting() {
		editRule(SetupModifier.SKIP_VOTE, true);
		dayStart();
		endPhase();
		
		assertIsNight();
	}
	
	public void testNoTrial() {
		editRule(SetupModifier.TRIAL_LENGTH, 0);
		
		voteOut(p8, p1, p3, p5, p7);
		voteOut(p7, p2, p4, p6, p8);
		endPhase();
		
		try {
			vote(p7, p2);
			fail();
		}catch(NarratorException e) {}
		vote(p2, p7);
	}
	
	public void testSingleVotedUpNoOtherVotes() {
		voteOut(p8, p1, p2, p3, p4, p5, p6, p7);
		endPhase();
		//start p8 trial
		endPhase();
		//end p8 trial
		
		vote(p8, p7);
		vote(p7, p6);
		vote(p6, p5);
		vote(p5, p4);
		vote(p4, p3);
		vote(p3, p2);
		vote(p2, p1);
		vote(p1, p8);
		
		endPhase();
		
		assertIsNight();
		isDead(p8);
	}

	public void testSingleVotedUpTwice() {
		voteOut(p8, p1, p2, p3, p4, p5, p6, p7);
		endPhase();
		//start p8 trial
		endPhase();
		//end p8 trial
		
		voteOut(p7, p1, p2, p3, p4, p5, p6, p8);
		endPhase();
		//start p7 trial
		endPhase();
		//end p7 trial

		try {
			vote(p1, p2);
			fail();
		}catch(NarratorException e) {}
		vote(p1, p7);
		vote(p2, p8);
	}
	
	public void test322Tie() {
		voteOut(p8, p1, p2, p3, p4, p5, p6, p7);
		endPhase();
		//start p8 trial
		endPhase();
		//vote phase
		voteOut(p8, p1, p2, p3, p4, p5, p6, p7);
		endPhase();
		//p8 dies
		
		assertIsNight();
		
		endPhase();
		//dayTime
		
		voteOut(p4, p1, p2, p3);
		voteOut(p1, p4, p5);
		voteOut(p2, p6, p7);
		
		endPhase();
		//end of initial voting
		endPhase();
		//first trial over
		endPhase();
		//second trial over
		endPhase();
		//3rd trial over
		
		voteOut(p4, p1, p2, p3);
		voteOut(p1, p4, p5);
		voteOut(p2, p6, p7);
		endPhase();
		
		assertIsDay();
	}
	
	public void test631Death() {
		Controller p9 = addPlayer(BasicRoles.Survivor());
		Controller p10 = addPlayer(BasicRoles.Survivor());

		vote(p1, p4);
		vote(p2, p8);
		vote(p3, p2);
		vote(p4, p10);
		vote(p5, p4);
		vote(p6, p2);
		vote(p7, p9);
		vote(p8, p7);
		vote(p9, p8);
		vote(p10, p8);
		
		endPhase();
		//first trial
		endPhase();
		//second trial
		endPhase();
		//third trial
		endPhase();
		
		voteOut(p4, p6);
		voteOut(p2, p8, p4, p3);
		voteOut(p8, p1, p7, p10, p5, p2, p9);
		endPhase();
		
		assertIsNight();
		isDead(p8);
		
	}
	
	public void testModKillIn3Way() {
		editRule(SetupModifier.SELF_VOTE, true);
		eliminate(p8);
		eliminate(p7);
		eliminate(p6);
		eliminate(p5);
		eliminate(p4);
		
		p3.getPlayer(narrator).modkill();
		assertIsNight();
	}
	
	private void eliminate(Controller toRemove) {
		Controller[] controllers = new Controller[] {p1, p2, p3, p4, p5, p6, p7, p8};
		for(Controller controller: controllers) {
			if(controller.getPlayer(narrator).isAlive())
				vote(controller, toRemove);
		}
		endPhase();
		endPhase();
		for(Controller controller: controllers) {
			if(controller.getPlayer(narrator).isAlive())
				vote(controller, toRemove);
		}
		endPhase();
		assertIsNight();
		isDead(toRemove);
		endPhase();
	}
}
