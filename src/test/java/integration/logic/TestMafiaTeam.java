package integration.logic;

import game.ai.Controller;
import game.logic.Faction;
import game.logic.Player;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Assassin;
import game.roles.Blackmailer;
import game.roles.FactionKill;
import game.roles.FactionSend;
import game.roles.Janitor;
import game.roles.Veteran;
import game.setups.Setup;
import models.Command;
import models.FactionRole;
import util.TestChatUtil;

public class TestMafiaTeam extends SuperTest {

    public TestMafiaTeam(String s) {
        super(s);
    }

    public void testEqualSending() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller goon1 = addPlayer(BasicRoles.Goon());
        Controller goon2 = addPlayer(BasicRoles.Goon());

        mafKill(goon1, cit1);
        mafKill(goon2, cit2);
        endNight(goon2);
        endNight();

        isDead(cit2);

        mafKill(goon1, cit1);
        mafKill(goon2, cit3);
        endNight(goon1);
        endNight();

        isDead(cit1);
        isAlive(cit3);
    }

    /*
     * tests another mafia type doing the killing
     */
    public void testJanitorKill() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Lookout());
        Controller jan1 = addPlayer(BasicRoles.Janitor());
        Controller jan2 = addPlayer(BasicRoles.Janitor());

        nightStart();

        setTarget(jan1, jan2, SEND);
        setTarget(jan2, jan2, SEND);
        setTarget(jan2, cit1, KILL);
        setTarget(jan1, cit1);

        endNight();

        isDead(cit1);

        skipDay();

        setTarget(jan1, jan2);
        setTarget(jan1, jan1, SEND);

        setTarget(jan2, jan1, SEND);

        endNight();

        isAlive(cit2);
    }

    public void testSkSendingTeam() {
        addPlayer(BasicRoles.Detective());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();

        try{
            setTarget(sk, FactionSend.COMMAND, null, null, NULL_OPT, (maf));
            fail();
        }catch(PlayerTargetingException e){
        }

    }

    public void testBmerNotWantingToKill() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        setTarget(bm, cit);
        witch(witch, bm, cit);

        endNight();

        assertInProgress();
        assertStatus(cit, Blackmailer.MAIN_ABILITY);
    }

    public void testActionOrder() {
        addPlayer(BasicRoles.Lookout());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller jan = addPlayer(BasicRoles.Janitor());
        addPlayer(BasicRoles.Goon());

        nightStart();
        setTarget(jan, jan, SEND);
        setTarget(jan, cit2);

        assertTrue(Ability.is(Faction.SEND, Ability.NIGHT_SEND));
        assertEquals(Janitor.MAIN_ABILITY, jan.getPlayer(narrator).getActions().getActions().get(0).ability);
        // p.reverseParse(a.ability)
    }

    public void testMafiosoVsJanitorVote() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller jan = addPlayer(BasicRoles.Janitor());
        Controller maf = addPlayer(BasicRoles.Goon());

        nightStart();

        mafKill(jan, cit1);
        mafKill(maf, cit2);

        endNight();

        isDead(cit2);
        isAlive(cit1);
    }

    public void testMafioso() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Lookout());
        Controller mafia1 = addPlayer(BasicRoles.Goon());
        Controller mafia2 = addPlayer(BasicRoles.Goon());

        dayStart();
        skipDay();

        // where mafia have conflicting
        // test targeting mafia

        mafKill(mafia1, cit);
        setTarget(mafia2, cit, KILL);
        setTarget(mafia2, mafia1, SEND);

        endNight();
        isDead(cit);

    }

    public void testIronWillStatus() {
        modifyRole(BasicRoles.Goon(), RoleModifier.UNBLOCKABLE, true);

        Controller maf1 = addPlayer(BasicRoles.Goon());
        Controller escort = addPlayer(BasicRoles.Escort());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        assertFalse(maf1.getPlayer(narrator).isBlockable());

        setTarget(escort, maf1);
        mafKill(maf1, escort);
        endNight();

        isDead(escort);
    }

    public void testInvalidTeamSettings() {
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Citizen());

        removeTeamAbility(BasicRoles.Coward().getColor(), FactionKill.class);
        setTeamRule(BasicRoles.Goon().faction, FactionModifier.KNOWS_ALLIES, false);
        setTeamRule(BasicRoles.Goon().faction, FactionModifier.HAS_NIGHT_CHAT, true);

        try{
            nightStart();
            fail();
        }catch(IllegalGameSettingsException e){
        }
    }

    public void testSEND() {
        Controller sher = addPlayer(BasicRoles.Lookout());
        Controller maf1 = addPlayer(BasicRoles.Goon());
        Controller bm2 = addPlayer(BasicRoles.Blackmailer());
        Controller jan = addPlayer(BasicRoles.Janitor());
        Controller agent = addPlayer(BasicRoles.Chauffeur());

        nightStart();

        mafKill(agent, agent);
        send(agent, maf1);

        try{
            send(maf1, sher);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            send(bm2, sher);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            send(jan, sher);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            send(sher, sher);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            mafKill(sher, bm2);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            send(sher, sher);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            mafKill(sher, sher);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testAutoSend() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        Controller m1 = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Agent());

        nightStart();

        setTarget(m1, cit, FactionKill.MAIN_ABILITY);
        endNight();

        isDead(cit);
    }

    public void testAllThree() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Lookout(), 4);
        Controller jan = addPlayer(BasicRoles.Janitor());
        Controller maf = addPlayer(BasicRoles.Goon());

        dayStart();
        skipDay();

        setTarget(jan, cit);
        setTarget(jan, cit, KILL);
        setTarget(jan, maf, SEND);
        mafKill(maf, cit);

        endNight();

        assertIsDay();
    }

    public void testLynchTarget() {
        Controller f1 = addPlayer(BasicRoles.Framer());
        Controller f2 = addPlayer(BasicRoles.Framer());
        Controller f3 = addPlayer(BasicRoles.Framer());
        Controller f4 = addPlayer(BasicRoles.Framer());
        Controller f5 = addPlayer(BasicRoles.Framer());
        Controller f6 = addPlayer(BasicRoles.Framer());
        addPlayer(BasicRoles.Framer());
        addPlayer(BasicRoles.Framer());
        addPlayer(BasicRoles.Citizen());

        dayStart();
        voteOut(f1, f2, f3, f4, f5, f6);

        assertFalse(f2.getPlayer(narrator)
                .isAcceptableTarget(f2.getPlayer(narrator).action(f1.getPlayer(narrator), FactionKill.MAIN_ABILITY)));
    }

    public void testMafiaNotKnowingTeammates() {
        addPlayer(BasicRoles.Framer());
        addPlayer(BasicRoles.Framer());
        addPlayer(BasicRoles.Citizen());

        setTeamRule(BasicRoles.Framer().faction, FactionModifier.KNOWS_ALLIES, false);

        try{
            dayStart();
            fail();
        }catch(IllegalGameSettingsException e){
        }
    }

    public void testAcceptableTargets() {
        Controller mafioso = addPlayer(BasicRoles.Goon());
        Controller assassin = addPlayer(BasicRoles.getMember(Setup.YAKUZA_C, new Assassin()));
        Controller godfather = addPlayer(BasicRoles.Godfather(Setup.YAKUZA_C));

        nightStart();

        assertFalse(mafioso.getPlayer(narrator).getAcceptableTargets(FactionKill.MAIN_ABILITY).isEmpty());
        assertFalse(godfather.getPlayer(narrator).getAcceptableTargets(FactionKill.MAIN_ABILITY).isEmpty());
        assertFalse(assassin.getPlayer(narrator).getAcceptableTargets(FactionKill.MAIN_ABILITY).isEmpty());
    }

    public void testAutoCancelingNoCommandTrace() {
        Controller m1 = addPlayer(BasicRoles.Goon());
        Controller m2 = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        nightStart();

        mafKill(m1, cit);
        mafKill(m2, cit2);
        endNight();

        assertTrue(cit.getPlayer(narrator).isDead() || cit2.getPlayer(narrator).isDead());
        assertFalse(cit.getPlayer(narrator).isDead() && cit2.getPlayer(narrator).isDead());

        for(Command c: narrator.getCommands())
            assertFalse(c.text.contains(Constants.CANCEL));
    }

    public static void gotWarning(Controller p) {
        Player player = p.getPlayer(narrator);
        assertFalse(TestChatUtil.getWarnings(player).isEmpty());
        TestChatUtil.resetWarnings(player);
    }

    public static void noWarning(Controller p) {
        Player player = p.getPlayer(narrator);
        assertTrue(TestChatUtil.getWarnings(player).isEmpty());
    }

    public void testIgnoredSend() {
        Controller m1 = addPlayer(BasicRoles.Goon());
        Controller m2 = addPlayer(BasicRoles.Goon());
        Controller jan = addPlayer(BasicRoles.Janitor());
        Controller elec = addPlayer(BasicRoles.ElectroManiac());

        nightStart();

        mafKill(m1, elec); // m1 controls kill
        gotWarning(m2);
        gotWarning(jan);

        mafKill(jan, elec); // jan, m1 both want kill, m1 gets it
        assertFactionController(m1, m1.getColor(), FactionKill.MAIN_ABILITY);
        gotWarning(jan);
        noWarning(jan);

        clearTargets(jan);
        TestChatUtil.resetWarnings();

        mafKill(m2, elec); // m2, m1 want kill
        assertActionSize(2, m2);
        assertEquals(null, narrator.getFaction(m1.getColor()).getCurrentController(FactionKill.MAIN_ABILITY));
        gotWarning(jan);
        gotWarning(m2);
        gotWarning(m1);

        clearTargets(m2);
        TestChatUtil.resetWarnings();

        mafKill(jan, elec); // jan, m1 both want kill
        gotWarning(jan);

        assertFactionController(m1, m1.getColor(), FactionKill.MAIN_ABILITY);
        send(m2, jan);
        assertFactionController(jan, m1.getColor(), FactionKill.MAIN_ABILITY);
        gotWarning(jan);
        gotWarning(m2);
        gotWarning(m1);

        clearTargets(m2);
        gotWarning(jan);
        gotWarning(m2);
        gotWarning(m1);

        // canceling and the result is null?

        clearTargets(m1);
        clearTargets(jan);
        TestChatUtil.resetWarnings();

        mafKill(m2, elec);
        mafKill(m1, elec);
        send(jan, m2);

        TestChatUtil.resetWarnings();
        clearTargets(jan);
        gotWarning(jan);
        gotWarning(m2);
        gotWarning(m1);
    }

    public void testAutoCancelSend() {
        Controller mafia = addPlayer(BasicRoles.Goon());
        Controller mafia2 = addPlayer(BasicRoles.Goon());
        Controller baker = addPlayer(BasicRoles.Baker());
        addPlayer(BasicRoles.Operator());

        nightStart();

        mafKill(mafia, mafia2);
        cancelAction(mafia, 0);

        // the send was canceled
        assertActionSize(0, mafia);

        mafKill(mafia, mafia2);
        send(mafia, mafia2);

        Action sendAction = mafia.getPlayer(narrator).getAction(FactionSend.MAIN_ABILITY);
        assertEquals(2, sendAction._targets.size());
        assertActionSize(2, mafia);

        cancelAction(mafia, 0);// cancelling kill

        // send isn't canceled
        assertFalse(mafia.getPlayer(narrator).getActions().isEmpty());

        setTarget(baker, mafia);

        endNight();

        assertEquals(4, narrator.getLiveSize());
        skipDay();

        // breaded
        mafKill(mafia, baker);
        mafKill(mafia, mafia2);
        assertActionSize(3, mafia);

        cancelAction(mafia, 0);
        // send isn't canceled
        assertFalse(mafia.getPlayer(narrator).getActions().isEmpty());

        cancelAction(mafia, 0);
        // no longer doing the action, no longer sending yourself
        assertFalse(mafia.getPlayer(narrator).getAction(FactionSend.MAIN_ABILITY).getTargets()
                .contains(mafia.getPlayer(narrator)));
    }

    public void testDeathFeedbackDifferenTeams() {
        Controller m = addPlayer(BasicRoles.Goon());
        Controller y = addPlayer(BasicRoles.Goon(Setup.YAKUZA_C));
        Controller fodder1 = addPlayer(BasicRoles.Citizen());
        Controller fodder2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Amnesiac());

        editRule(SetupModifier.DIFFERENTIATED_FACTION_KILLS, true);

        mafKill(m, fodder1);
        mafKill(y, fodder2);
        endNight();

        isDead(fodder1, fodder2);

        partialContains(fodder2, y.getPlayer(narrator).getFaction().getName());
        partialContains(fodder1, m.getPlayer(narrator).getFaction().getName());
    }

    public void testKillCooldown() {
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        String mafColor = BasicRoles.Goon().getColor();
        modifyTeamAbility(mafColor, AbilityModifier.COOLDOWN, FactionKill.class, 1);

        nightStart();

        assertTrue(maf.getPlayer(narrator).getFaction().hasAbility(FactionKill.MAIN_ABILITY));
        mafKill(maf, cit);
        assertFalse(maf.getPlayer(narrator).getFaction().getAbility(FactionKill.MAIN_ABILITY).isOnCooldown());
        endNight();

        isDead(cit);

        skipDay();

        assertEquals(1,
                maf.getPlayer(narrator).getFaction().getAbility(FactionKill.MAIN_ABILITY).getRemainingCooldown());
        try{
            mafKill(maf, cit2);
            fail();
        }catch(PlayerTargetingException e){
        }

        assertTrue(maf.getPlayer(narrator).getAcceptableTargets(FactionKill.MAIN_ABILITY).isEmpty());
        assertTrue(maf.getPlayer(narrator).getAcceptableTargets(Faction.SEND_).isEmpty());
    }

    public void testKillCooldownBlocked() {
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller esc = addPlayer(BasicRoles.Escort());
        addPlayer(BasicRoles.Citizen());

        modifyTeamAbility(BasicRoles.Goon().getColor(), AbilityModifier.COOLDOWN, FactionKill.class, 1);

        nightStart();
        assertTrue(maf.getPlayer(narrator).getFaction().hasAbility(FactionKill.MAIN_ABILITY));

        mafKill(maf, esc);
        setTarget(esc, maf);

        nextNight();

        mafKill(maf, esc);
        endNight();

        isDead(esc);
    }

    public void testKillNoCooldown() {
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Agent());

        mafKill(maf, cit);
        endNight();

        isDead(cit);

        skipDay();

        mafKill(maf, cit2);
        assertFalse(maf.getPlayer(narrator).getAcceptableTargets(FactionKill.MAIN_ABILITY).isEmpty());
        assertFalse(maf.getPlayer(narrator).getAcceptableTargets(Faction.SEND_).isEmpty());
    }

    public void testClassicKill() {
        Controller m1 = addPlayer(BasicRoles.Goon());
        Controller m2 = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());

        nightStart();

        mafKill(m1, cit);
        mafKill(m2, cit);

        endNight();

        assertGameOver();
    }

    public void testSendPriority() {
        Controller mafioso2 = addPlayer(BasicRoles.Blackmailer());
        Controller mafioso = addPlayer(BasicRoles.Janitor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        setTarget(baker, mafioso);
        nextNight();

        setTarget(lookout, cit);
        mafKill(mafioso, cit);
        mafKill(mafioso, baker);
        mafKill(mafioso2, baker);
        endNight();

        TestLookout.seen(lookout, mafioso);
        isDead(baker, cit);
    }

    // test for 2 teammembers with bread, but want to use different abilities.

    public void testNoFriendlyFire() {
        Controller mafioso2 = addPlayer(BasicRoles.Blackmailer());
        Controller mafioso = addPlayer(BasicRoles.Janitor());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        try{
            mafKill(mafioso, mafioso2);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testFriendlyFire() {
        Controller mafioso2 = addPlayer(BasicRoles.Blackmailer());
        Controller mafioso = addPlayer(BasicRoles.Janitor());
        addPlayer(BasicRoles.BusDriver());

        nightStart();

        mafKill(mafioso, mafioso2);
    }

    public void testNoSend() {
        noSendTest(BasicRoles.Goon(), BasicRoles.Goon(), BasicRoles.Citizen());
        sendTest(BasicRoles.Goon(), BasicRoles.Goon(), BasicRoles.Witch(), BasicRoles.Citizen());
    }

    public void testVeteranAbility() {
        Controller vet = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Sheriff());

        addTeamAbility(Setup.MAFIA_C, Veteran.class);

        alert(vet);
        endNight();

        isInvuln(vet, false);
    }

    private void sendTest(FactionRole... ms) {
        Controller maf = null;
        for(int i = ms.length - 1; i >= 0; i--){
            maf = addPlayer(ms[i]);
        }

        nightStart();

        assertTrue(maf.getPlayer(narrator).hasAbility(Ability.NIGHT_SEND));

        newNarrator();
    }

    private void noSendTest(FactionRole... ms) {
        Controller maf = null;
        for(int i = ms.length - 1; i >= 0; i--){
            maf = addPlayer(ms[i]);
        }

        nightStart();

        assertFalse(maf.getPlayer(narrator).hasAbility(Ability.NIGHT_SEND));

        newNarrator();
    }
}
