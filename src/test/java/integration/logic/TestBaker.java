package integration.logic;

import java.util.List;

import game.ai.Controller;
import game.event.Feedback;
import game.logic.Faction;
import game.logic.Role;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Baker;
import game.roles.BreadAbility;
import game.roles.Doctor;
import game.roles.Douse;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.GraveDigger;
import game.roles.Undouse;
import game.roles.support.Bread;
import game.setups.Setup;
import models.Command;
import services.FactionRoleService;
import services.FactionService;
import util.LookupUtil;

public class TestBaker extends SuperTest {

    Controller baker;

    @Override
    public void roleInit() {
        baker = addPlayer(BasicRoles.Baker());
    }

    public TestBaker(String name) {
        super(name);
    }

    private void setAlias() {
        narrator.setAlias(Bread.ALIAS, "soma");
    }

    // double role
    public void testBasicFunction() {
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller maf1 = addPlayer(BasicRoles.Goon());
        Controller maf2 = addPlayer(BasicRoles.Goon());

        setAlias();

        setTarget(baker, vig);

        nextNight();

        assertPassableBreadCount(1, vig);

        shoot(vig, maf1);
        shoot(vig, maf2);

        assertActionSize(2, vig);

        endNight();

        isDead(maf1, maf2);
    }

    public void testWitchGSBaker() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        setTarget(baker, witch);
        setTarget(gs, cit);

        nextNight();

        List<Feedback> feedbacks = getFeedback(witch.getPlayer(narrator), 0);
        assertFalse(feedbacks.isEmpty());

        shoot(cit, witch);
        witch(witch, cit, cit);
        witch(witch, gs, witch);

        endNight();

        isAlive(witch);
        isDead(cit);

    }

    public void testBreadCommandHandler() {
        Controller a = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        nightStart();
        command(narrator.skipper, Constants.MOD_ADDBREAD, Integer.toString(2), a.getPlayer(narrator).getID());

        assertPassableBreadCount(2, a);

        assertTrue(Command.getText(narrator.getCommands())
                .contains(Constants.MOD_ADDBREAD + " 2 " + a.getPlayer(narrator).getID()));
    }

    public void testBreadTargeting() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.BREAD_PASSING, false);

        setTarget(baker, doc);

        nextNight();

        try{
            bread(doc, witch);
            fail();
        }catch(PlayerTargetingException e){
        }

        assertFalse(doc.getCommands().contains(Bread.COMMAND));
        assertTrue(baker.getCommands().contains(Bread.COMMAND));

        assertUseableBreadCount(1, doc);

        setTarget(doc, witch);
        assertTrue(doc.isTargeting(Doctor.COMMAND, witch));

        setTarget(doc, baker);
        assertTrue(doc.isTargeting(Doctor.COMMAND, baker));
        assertTrue(doc.isTargeting(Doctor.COMMAND, witch));

        partialContains(doc, " and ");

        endNight();
    }

    public void testUnstartedGameSetTarget() {
        Controller bd = addPlayer(BasicRoles.BusDriver());

        try{
            setTarget(baker, bd);
            fail();
        }catch(IllegalGameSettingsException e){
        }
    }

    public void testDriverCancelationWithBread() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller goon = addPlayer(BasicRoles.Goon());

        setTarget(baker, bd);

        nextNight();

        drive(bd, baker, goon);
        cancelAction(bd, 0);
        assertFalse(bd.isTargeting(Driver.COMMAND, baker));
        mafKill(goon, goon);

        endNight();

        isDead(goon);
    }

    public void testBakerMultiBread() {
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.BREAD_PASSING, false);

        setAlias();

        nightStart();

        assertUseableBreadCount(0, baker);
        assertUseableBreadCount(0, baker2);

        setTarget(baker, baker2);

        nextNight();

        partialContains(baker2, Bread.GetFeedback(narrator));
        assertUseableBreadCount(1, baker2);

        setTarget(baker2, cit);
        setTarget(baker2, witch);
        setTarget(baker, cit);

        nextNight();

        assertUseableBreadCount(2, cit);
        assertUseableBreadCount(0, baker);
        assertUseableBreadCount(1, witch);
        assertUseableBreadCount(0, baker);
    }

    public void testWitchForceBreadPassing() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.BREAD_PASSING, true);

        setTarget(baker, cit);
        nextNight();

        witch(witch, cit, witch);
        nextNight();

        assertPassableBreadCount(1, witch);
    }

    public void testBakerCorpse() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller gd = addPlayer(BasicRoles.GraveDigger());

        voteOut(baker, cit1, cit2, gd);

        setTarget(gd, GraveDigger.MAIN_ABILITY, Bread.COMMAND, null, NULL_OPT, baker, gd);
        endNight();

        assertPassableBreadCount(1, gd);
    }

    public void testNonBakerCorpse() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller gd = addPlayer(BasicRoles.GraveDigger());

        voteOut(cit1, baker, cit2, gd);

        setTarget(gd, GraveDigger.MAIN_ABILITY, Bread.COMMAND, null, NULL_OPT, cit1, gd);
        endNight();

        assertPassableBreadCount(0, gd);
    }

    public void testAmnesiacKeepingBreads() {
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller dead = addPlayer(BasicRoles.SerialKiller());

        dayStart();

        voteOut(dead, baker, witch, amn);

        for(int i = 0; i < 3; i++){
            setTarget(baker, amn);
            nextNight();
        }

        setTarget(amn, dead);

        nextNight();
        assertUseableBreadCount(3, amn);
    }

    public void newNarratorWitchForceVest() {
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        witch(witch, baker, baker);

        nextNight();

        // originally baker->baker but i want to get rid of the unused errors
        witch(witch, as, sk);
        // cant really force witch to make someone use a vest
    }

    public void testBDDoubleAction() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(baker, bd);

        nextNight();
        drive(bd, sk, c1);
        drive(bd, vig, bd);
        shoot(vig, bd);
        setTarget(sk, c1);

        endNight();

        isDead(sk, vig);
    }

    public void testBreadedAmnesiac() {
        Controller as2 = addPlayer(BasicRoles.Armorsmith());
        Controller as1 = addPlayer(BasicRoles.Armorsmith());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTarget(baker, amn);
        endNight();

        voteOut(as1, as2, baker, lookout, witch);

        endNight();

        voteOut(as2, baker, lookout, witch);

        setTarget(amn, as1);
        setTarget(amn, as2);
        assertActionSize(1, amn);
        assertTrue(amn.getPlayer(narrator).getActions().canAddAnotherAction(Bread.MAIN_ABILITY));
        witch(witch, amn, lookout);
        setTarget(lookout, lookout);

        endNight();

        TestLookout.seen(lookout, amn);
    }

    public void testArsonBoom() {
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller arson = addPlayer(BasicRoles.Arsonist());

        setTarget(arson, as);
        setTarget(baker, arson);
        setTarget(baker2, arson);

        nextNight();
        setTarget(baker, arson);
        setTarget(baker2, arson);

        nextNight();

        setTarget(arson, as, Undouse.MAIN_ABILITY);
        setTarget(arson);
        setTarget(arson, baker);
        setTarget(arson, baker2);

        assertActionSize(4, arson);

        endNight();

        isAlive(as);
        isDead(baker, baker2);
        assertStatus(as, Douse.MAIN_ABILITY, false);
    }

    public void testQuadrupleKill() {
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller baker3 = addPlayer(BasicRoles.Baker());
        Controller baker4 = addPlayer(BasicRoles.Baker());
        Controller maf = addPlayer(BasicRoles.Goon());

        nightStart();

        setTarget(baker, maf);
        setTarget(baker2, maf);
        setTarget(baker3, maf);
        setTarget(baker4, maf);

        nextNight();

        mafKill(maf, baker);
        mafKill(maf, baker2);
        mafKill(maf, baker3);
        mafKill(maf, baker4);

        endNight();
        isDead(baker, baker2, baker3, baker4);
    }

    public void testVigiWithBread() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 1);

        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller witch1 = addPlayer(BasicRoles.Witch());
        Controller witch2 = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);
        editRule(SetupModifier.BREAD_PASSING, false);

        nightStart();
        assertTotalGunCount(1, vigi);

        setTarget(baker, vigi);

        nextNight();

        shoot(vigi, witch1);
        shoot(vigi, witch2);

        assertActionSize(1, vigi);

        witch(witch1, vigi, baker);
        endNight();
        isDead(baker);
        isAlive(witch1, witch2);

        skipDay();

    }

    public void testVigiWithBread2() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 1);

        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller witch1 = addPlayer(BasicRoles.Witch());
        Controller witch2 = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.BREAD_PASSING, true);

        setTarget(baker, vigi);

        nextNight();

        bread(vigi, witch2);
        witch(witch1, vigi, vigi);
        witch(witch2, vigi, vigi);
        endNight();
    }

    public void testDruggedBread() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        drug(dd, sk, DrugDealer.BREAD);
        nextNight();

        assertUseableBreadCount(1, sk);

        setTarget(sk, baker);
        setTarget(sk, mayor);

        assertActionSize(2, sk);

        endNight();

        isDead(baker);
        isAlive(mayor);
    }

    public void testDruggedChargeUseup() {
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller armorsmith = addPlayer(BasicRoles.Armorsmith());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());
        Controller drugDealer = addPlayer(BasicRoles.DrugDealer());

        drug(drugDealer, vigi, DrugDealer.BREAD);
        setTarget(armorsmith, vigi);
        nextNight();

        shoot(vigi, armorsmith);
        vest(vigi);
        setTarget(serialKiller, vigi);

        endNight();

        isDead(armorsmith, vigi);
    }

    public void testTryingToDoubleBurn() {
        Controller arson = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Poisoner());

        modifyRole(BasicRoles.Survivor(), AbilityModifier.CHARGES, 2);

        setTarget(baker, arson);

        nextNight();

        setTarget(arson);
        setTarget(arson);

        assertActionSize(1, arson);
    }

    public void testTryingToDoubleDouse() {
        Controller arson = addPlayer(BasicRoles.Arsonist());
        Controller poisoner = addPlayer(BasicRoles.Poisoner());

        modifyRole(BasicRoles.Survivor(), AbilityModifier.CHARGES, 2);

        setTarget(baker, arson);

        nextNight();

        setTarget(arson, baker);
        setTarget(arson, poisoner);

        assertActionSize(2, arson);
    }

    public void testTryingToUseTwoVests() {
        Controller surv = addPlayer(BasicRoles.Survivor());
        addPlayer(BasicRoles.Poisoner());

        modifyRole(BasicRoles.Survivor(), AbilityModifier.CHARGES, 2);

        setTarget(baker, surv);

        nextNight();

        vest(surv);
        vest(surv);

        assertActionSize(1, surv);
    }

    public void testBasicBreadPassing() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.BREAD_PASSING, true);

        setTarget(baker, cit);

        // baker bread count only counts initialized breads
        assertUseableBreadCount(0, baker);

        nextNight();

        assertPassableBreadCount(1, cit);

        bread(cit, witch);
        endNight();

        assertPassableBreadCount(0, baker);
        assertPassableBreadCount(0, cit);
        assertPassableBreadCount(1, witch);

        skipDay();

        bread(baker, cit);
        bread(baker, witch);
        assertActionSize(1, baker);
    }

    public void testDruggedBreadPassing() {
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        editRule(SetupModifier.BREAD_PASSING, true);

        drug(dd, cit, DrugDealer.BREAD);
        nextNight();

        bread(cit, dd);
        setTarget(lookout, dd);
        endNight();

        assertPassableBreadCount(0, dd);
        assertPassableBreadCount(0, cit);
        TestLookout.seen(lookout, cit);
    }

    public void testForcedBreadPassing() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.BREAD_PASSING, true);

        nightStart();

        assertFalse(cit.getPlayer(narrator).getActions().canAddAnotherAction(Ability.BREAD_ABILITY,
                witch.getPlayer(narrator)));
        assertTrue(baker.getPlayer(narrator).getActions().canAddAnotherAction(Ability.BREAD_ABILITY,
                cit.getPlayer(narrator)));

        setTarget(baker, cit);
        nextNight();

        assertTrue(cit.getPlayer(narrator).getActions().canAddAnotherAction(Ability.BREAD_ABILITY,
                witch.getPlayer(narrator)));
        bread(cit, witch);
        assertFalse(cit.getPlayer(narrator).getActions().canAddAnotherAction(Ability.BREAD_ABILITY,
                witch.getPlayer(narrator)));
        cancelAction(cit, 0);

        witch(witch, cit, witch);
        endNight();

        assertPassableBreadCount(0, cit);
        assertPassableBreadCount(1, witch);
    }

    public void testTeammatePassing() {
        Role role = LookupUtil.findRole(narrator, "Baker");
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);

        Controller witch = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller baker = addPlayer(FactionService.createFactionRole(mafia, role));

        editRule(SetupModifier.BREAD_PASSING, true);

        nightStart();
        BreadAbility ba = baker.getPlayer(narrator).getAbility(BreadAbility.class);
        assertFalse(ba.get(0).isInitialized());

        try{
            setTarget(baker, maf);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(baker, witch);
        nextNight();

        bread(witch, baker);
        nextNight();

        setTarget(baker, maf);
        try{
            setTarget(baker, maf);
            fail();
        }catch(PlayerTargetingException e){
        }
        setTarget(baker, witch);
    }

    public void testSelfMadeUsage() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.SELF_BREAD_USAGE, true);

        setTarget(gs, baker);
        nextNight();

        setTarget(gs, baker);
        nextNight();

        assertPassableBreadCount(3, baker);
        assertTotalGunCount(2, baker);

        shoot(baker, gs);
        shoot(baker, witch);

        assertActionSize(2, baker);

        endNight();

        isDead(gs, witch);
    }

    public void testNoPassingBreadToSelf() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifier.BREAD_PASSING, true);

        setTarget(baker, cit);
        nextNight();

        try{
            setTarget(cit, cit, BreadAbility.MAIN_ABILITY);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testPassingBreadToSelf() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Chauffeur());

        editRule(SetupModifier.BREAD_PASSING, true);

        setTarget(baker, cit);
        nextNight();

        setTarget(cit, cit, BreadAbility.MAIN_ABILITY);
        endNight();

        assertPassableBreadCount(1, cit);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Baker(), AbilityModifier.BACK_TO_BACK, Baker.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Baker(), AbilityModifier.ZERO_WEIGHTED, Baker.class, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
