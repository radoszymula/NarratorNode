package integration.logic;

import game.logic.Faction;
import game.logic.Player;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.TeamSettingsException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import services.FactionService;
import util.TestUtil;

public class TeamTests extends SuperTest {

    public TeamTests(String name) {
        super(name);
    }

    public void testChangeTeamNameAfterStart() {
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Tailor());
        addPlayer(BasicRoles.SerialKiller());

        nightStart();

        try{
            narrator.getFaction(Setup.THREAT_C).setName("Joe");
            fail();
        }catch(TeamSettingsException e){
        }
    }

    public void testInvalidTeamColor() {
        tryTeamColor(null);
        tryTeamColor(Setup.MAFIA_C);
        tryTeamColor("#234");
        tryTeamColor(Constants.A_NEUTRAL);
        tryTeamColor(Constants.A_RANDOM);
        tryTeamColor(Constants.A_SKIP);
        tryTeamColor("FFFFFE", GOOD);
        tryTeamColor("Fd1ddd", GOOD);
    }

    private static final boolean GOOD = true, BAD = false;

    private void tryTeamColor(String color) {
        tryTeamColor(color, BAD);
    }

    private void tryTeamColor(String color, boolean isGoodColor) {
        try{
            Faction faction = FactionService.createFaction(narrator, color, TestUtil.getRandomString(8));
            if(!isGoodColor)
                fail();
            FactionService.deleteFaction(narrator, faction.id);
        }catch(NarratorException e){
            if(isGoodColor)
                fail();
        }
    }

    public void testInvalidTeamName() {
        String name = "Crypt";
        String tName = "TheTeam";
        narrator.addPlayer(name);
        Faction t = FactionService.createFaction(narrator, "ff0011", TestUtil.getRandomString(8));
        try{
            t.setName(name);
            fail();
        }catch(NamingException e){
        }
        t.setName(tName);
        Faction t2 = FactionService.createFaction(narrator, "#fe0021", TestUtil.getRandomString(8));
        try{
            t2.setName(tName);
            fail();
        }catch(NamingException e){
        }
        try{
            t2.setName("Randoms");
            fail();
        }catch(NamingException e){
        }
        try{
            t2.setName("Random");
            fail();
        }catch(NamingException e){
        }
        try{
            t2.setName("Neutrals");
            fail();
        }catch(NamingException e){
        }
        try{
            t2.setName("Neutral");
            fail();
        }catch(NamingException e){
        }
        try{
            t2.setName(BasicRoles.Doctor().getName());
            fail();
        }catch(NamingException e){
        }
    }

    public void testPrefers() {
        Player p = addPlayer("joe");
        p.teamPrefer(Setup.TOWN_C);
        assertTrue(p._tPrefers.contains(narrator.getFaction(Setup.TOWN_C)));
    }

    // not allowing teams to add/remove abilities if game's started
    // check everyone's nightstart, night end, since it depends on who actually did
    // it.

}
