package integration.logic;

import java.util.ArrayList;

import game.ai.Controller;
import game.event.DeathAnnouncement;
import game.event.Message;
import game.logic.Faction;
import game.logic.Player;
import game.logic.Role;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PhaseException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Doctor;
import game.roles.DrugDealer;
import game.roles.GraveDigger;
import game.roles.GunAbility;
import game.roles.Gunsmith;
import game.roles.Vigilante;
import game.roles.support.Gun;
import game.setups.Setup;
import services.FactionService;
import util.LookupUtil;

public class TestGunsmith extends SuperTest {

    public TestGunsmith(String name) {
        super(name);
    }

    public void testBasicFunction() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller maf = addPlayer(BasicRoles.Goon());

        faultyGunsOff();

        try{
            faultyGun(gs, cit);
            fail();
        }catch(PlayerTargetingException e){
        }
        setTarget(gs, cit);

        assertFalse(Gunsmith.FaultyGuns(gs.getPlayer(narrator)));
        assertEquals(0, gs.getPlayer(narrator).getOptions(Gunsmith.COMMAND).size());

        endNight();

        assertTotalGunCount(1, cit);

        skipDay();

        assertTrue(cit.getCommands().contains(Constants.GUN_COMMAND));
        shoot(cit, maf);

        endNight();

        isDead(maf);
        assertGameOver();
        isWinner(gs);
    }

    public void testGunFeedback() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        addPlayer(BasicRoles.Godfather());

        setTarget(gs, cit);

        endNight();

        partialContains(cit, Gunsmith.GetGunReceiveMessage(narrator));
    }

    public void testLimitActionFeedback() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller maf = addPlayer(BasicRoles.Goon());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifier.GS_FAULTY_GUNS, false);

        setTarget(gs, doc);

        endNight();
        assertTotalGunCount(1, doc);

        skipDay();

        assertTrue(doc.getCommands().contains(Constants.GUN_COMMAND));
        shoot(doc, gs);
        shoot(doc, maf);

        setTarget(doc, gs);
        assertTrue(doc.getPlayer(narrator).getActions().getTargets(GUN).isEmpty());

        cancelAction(doc, 0);
        setTarget(doc, gs);
        setTarget(doc, gs, GUN);
        assertTrue(doc.getPlayer(narrator).getActions().getTargets(Doctor.MAIN_ABILITY).isEmpty());

        setTarget(doc, maf, GUN);

        endNight();

        isDead(maf);
        assertGameOver();
        isWinner(gs);
    }

    public void testGivingToTeammates() {
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Role gunsmithRole = LookupUtil.findRole(narrator, "Gunsmith");
        Controller gs = addPlayer(FactionService.createFactionRole(mafia, gunsmithRole));
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Cultist());

        nightStart();

        try{
            setTarget(gs, gf);
            fail();
        }catch(PlayerTargetingException e){
        }

        witch(witch, gs, gf);
        endNight();

        assertTotalGunCount(0, gf);
    }

    public void testSelfTargetGun() {
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Role gunsmithRole = LookupUtil.findRole(narrator, "Gunsmith");
        Controller gs = addPlayer(FactionService.createFactionRole(mafia, gunsmithRole));
        Controller elector = addPlayer(BasicRoles.Elector());
        addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Doctor());

        setTarget(gs, elector);

        nextNight();

        try{
            shoot(elector, elector);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testBusDriverGunning() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        addPlayer(BasicRoles.Witch());

        setTarget(gs, bd);

        nextNight();

        partialContains(bd.getCommands(), Constants.GUN_COMMAND);
        shoot(bd, gs);
    }

    public void testWitchTriggering() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller witch = addPlayer(BasicRoles.Witch());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifier.GS_FAULTY_GUNS, false);

        witch(witch, gs, gs);

        nextNight();

        assertTotalGunCount(1, gs);

        // wants to give gun to witch, but witch will make gs GUN gf
        witch(witch, gs, maf);
        setTarget(gs, maf, GUN);
        setTarget(gs, witch);

        nextNight();

        isAlive(maf);
        assertTotalGunCount(1, maf);

        witch(witch, gs, maf);
        setTarget(gs, maf);
        setTarget(gs, witch, GUN);

        nextNight();

        isDead(maf);
    }

    public void testWitchTriggeringVigilante() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, SetupModifiers.UNLIMITED);

        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTarget(gs, vig);
        assertTotalGunCount(1, vig);

        nextNight();
        assertTotalGunCount(2, vig);

        // wants to give gun to witch, but witch will make gs GUN gf
        witch(witch, vig, cit);

        nextNight();

        isDead(cit);
        assertTotalGunCount(1, vig);
        assertEquals(Constants.VIGILANTE_KILL_FLAG, getGun(vig, 0).getFlag());

        assertTrue(cit.getPlayer(narrator).getDeathType().getList().contains(Constants.GUN_KILL_FLAG));

        setTarget(vig, gs, GUN);
        endNight();
        assertTrue(gs.getPlayer(narrator).getDeathType().getList().contains(Constants.VIGILANTE_KILL_FLAG));
    }

    public void testExecutionerWeildingGun() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller executioner = addPlayer(BasicRoles.Executioner());
        addPlayer(BasicRoles.Witch());

        setTarget(gs, executioner);

        nextNight();

        setTarget(executioner, gs, GUN);

        endNight();
    }

    public void assertRealVigiGuns(int count, Controller vigi) {
        assertEquals(count, Vigilante.GetRealVigiGunCount(vigi.getPlayer(narrator)));
    }

    public void testDrugGun() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 1);

        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        nightStart();

        Vigilante vCard = vigi.getPlayer(narrator).getAbility(Vigilante.class);
        assertFalse(vCard.unlimitedGuns);

        drug(dd, vigi, DrugDealer.GUN_RECEIVED);
        // using gs gun
        setTarget(gs, vigi);

        endNight();

        assertTotalGunCount(3, vigi);
        assertRealGunCount(2, vigi);
        assertFakeGunCount(1, vigi);
        assertRealVigiGuns(1, vigi);

        skipDay();

        // n1 using real gun
        shoot(vigi, cit);

        endNight();

        isDead(cit);
        assertTotalGunCount(2, vigi);
        assertRealGunCount(1, vigi);
        assertFakeGunCount(1, vigi);
        assertRealVigiGuns(1, vigi);

        skipDay();

        // n2 using fake gun
        shoot(vigi, dd);

        endNight();

        isAlive(dd);
        assertTotalGunCount(1, vigi);
        assertRealGunCount(1, vigi);
        assertFakeGunCount(0, vigi);
        assertRealVigiGuns(1, vigi);

        skipDay();

        // using vigi gun
        shoot(vigi, dd);

        endNight();

        isDead(dd);
        assertTotalGunCount(0, vigi);
        assertRealGunCount(0, vigi);
        assertFakeGunCount(0, vigi);
        assertRealVigiGuns(0, vigi);
    }

    public void testDayGun() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller vigi2 = addPlayer(BasicRoles.Vigilante());

        dayGunOn();

        setTarget(gs, cit);
        nextNight();

        setTarget(gs, vigi);
        nextNight();

        assertFalse(cit.getPlayer(narrator).getAbility(GunAbility.class).isNightAbility(cit.getPlayer(narrator)));
        try{
            setTarget(cit, witch, Ability.SHOOT);
            fail();
        }catch(NarratorException e){
        }
        assertFalse(cit.getCommands().contains(Constants.GUN_COMMAND));

        assertTrue(vigi.getCommands().contains(Constants.GUN_COMMAND));

        endNight();

        try{
            shoot(vigi2, witch);
            fail();
        }catch(NarratorException e){
        }

        shoot(cit, witch);

        isDead(witch);
        assertGameOver();
    }

    public void testDayGunLimit() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller gs2 = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Witch());

        dayGunOn();

        setTarget(gs, cit);
        setTarget(gs2, cit);
        endNight();

        shoot(cit, gs);

        try{
            shoot(cit, gs2);
            fail();
        }catch(IllegalActionException | PlayerTargetingException e){
        }

        isDead(gs);
    }

    public void testDayGunDrugged() {
        addPlayer(BasicRoles.Gunsmith(), 2);
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller dealer = addPlayer(BasicRoles.DrugDealer());
        Controller dealer2 = addPlayer(BasicRoles.DrugDealer());

        dayGunOn();

        drug(dealer, cit, DrugDealer.GUN_RECEIVED);
        drug(dealer2, cit, DrugDealer.GUN_RECEIVED);

        endNight();

        shoot(cit, dealer);

        partialContains(cit, Gun.IMAGINARY_GUN);
        partialExcludes(dealer, Gun.IMAGINARY_GUN);

        try{
            shoot(cit, dealer2);
            fail();
        }catch(IllegalActionException | PlayerTargetingException e){
        }

        isAlive(dealer);

        assertTotalGunCount(1, cit);
    }

    public void testDayShootCorrectGun() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller dealer = addPlayer(BasicRoles.DrugDealer());

        dayGunOn();

        drug(dealer, cit, DrugDealer.GUN_RECEIVED);
        setTarget(gs, cit);
        endNight();

        assertTrue(getGun(cit.getPlayer(narrator), 0).isReal());
        assertTotalGunCount(2, cit);
        shoot(cit, dealer);

        try{
            shoot(cit, dealer);
            fail();
        }catch(PhaseException | PlayerTargetingException e){
        }

        isDead(dealer);
        assertTotalGunCount(1, cit);
    }

    private void dayGunOn() {
        editRule(SetupModifier.GS_DAY_GUNS, true);
    }

    public void testDayShootImmune() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller gf = addPlayer(BasicRoles.Godfather());

        dayGunOn();

        setTarget(gs, cit);

        endNight();

        shoot(cit, gf);

        isAlive(gf);
        assertVoteTarget(null, cit);
        assertVoteTarget(null, gs);
    }

    public void testDayGunCH() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Witch());

        dayGunOn();

        setTarget(gs, cit);

        endNight();

        command(cit, Constants.GUN_COMMAND, witch.getName());

        for(Message m: narrator.getEventManager().getEvents(Message.PUBLIC)){
            if(m instanceof DeathAnnouncement && ((DeathAnnouncement) m).dead.contains(witch.getPlayer(narrator))){
                assertFalse(m.isNightToDayAnnouncement());
            }
        }
    }

    public void testFaultyGun() {
        faultyGunsOn();

        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller fodder2 = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Witch());

        setTarget(gs, Gunsmith.MAIN_ABILITY, null, fodder);
        assertEquals(Gunsmith.REAL, gs.getPlayer(narrator).getActions().getFirst().getOption());

        command(gs, Gunsmith.COMMAND, cit.getName());
        nextNight();

        assertTrue(getGun(cit.getPlayer(narrator), 0).isReal());

        command(cit, Gun.COMMAND, fodder.getName());
        command(gs, Gunsmith.COMMAND, cit.getName(), Gunsmith.FAULTY);
        nextNight();

        isDead(fodder);

        command(cit, Gun.COMMAND, gs.getName());
        faultyGun(gs, fodder2);
        endNight();

        isAlive(gs);
        isDead(cit);

        partialContains(cit, Gunsmith.GetFaultyDeathFeedback(narrator));
        assertTrue(getGun(fodder2.getPlayer(narrator), 0).isFaulty());

        Action a = new Action(gs.getPlayer(narrator), Player.list(doc.getPlayer(narrator)), Gunsmith.MAIN_ABILITY,
                Gunsmith.FAULTY, null);
        ArrayList<String> commandParts = gs.getPlayer(narrator).getAbility(Gunsmith.MAIN_ABILITY).getCommandParts(a);
        ArrayList<String> comp = new ArrayList<>();
        comp.add(Gunsmith.COMMAND);
        comp.add(doc.getName());
        comp.add(Gunsmith.FAULTY);

        assertEquals(comp, commandParts);
    }

    public void testFaultyGunProtection() {
        faultyGunsOn();

        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller gs2 = addPlayer(BasicRoles.Gunsmith());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller agent = addPlayer(BasicRoles.Agent());

        faultyGun(gs, witch);
        faultyGun(gs2, agent);
        nextNight();

        shoot(witch, gs);
        shoot(agent, gs2);
        setTarget(doc, witch);
        setTarget(bg, agent);
        endNight();

        isDead(agent, bg);
        isAlive(witch);

    }

    public void testCowardShootingSelf() {
        faultyGunsOn();

        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller coward = addPlayer(BasicRoles.Coward());

        faultyGun(gs, coward);
        setTarget(baker, coward);
        nextNight();

        setTarget(coward, baker);
        shoot(coward, gs);
        endNight();

        isDead(coward);
        isAlive(gs, baker);
    }

    private void faultyGun(Controller gs, Controller target) {
        setTarget(gs, Gunsmith.MAIN_ABILITY, Gunsmith.FAULTY, target);
    }

    private void faultyGunsOn() {
        modifyRole(BasicRoles.Gunsmith().role, AbilityModifier.GS_FAULTY_GUNS, true);
    }

    private void faultyGunsOff() {
        modifyRole(BasicRoles.Gunsmith().role, AbilityModifier.GS_FAULTY_GUNS, false);
    }

    public void testDayFaultyGun() {
        faultyGunsOn();

        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Bodyguard());
        addPlayer(BasicRoles.Arsonist());

        dayGunOn();

        nightStart();

        assertEquals(2, gs.getPlayer(narrator).getOptions(Gunsmith.COMMAND).size());

        faultyGun(gs, fodder);
        endNight();

        shoot(fodder, gs);
        isDead(fodder);
    }

    public void testGraveDiggerGivingFakeGun() {
        faultyGunsOn();

        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller digger = addPlayer(BasicRoles.GraveDigger());
        addPlayer(BasicRoles.Arsonist());

        dayGunOn();

        voteOut(gs, fodder, electro, digger);

        setTarget(digger, GraveDigger.MAIN_ABILITY, Gunsmith.COMMAND, Gunsmith.FAULTY, gs, fodder);
        endNight();

        shoot(fodder, digger);

        assertInProgress();
        isAlive(digger);
        isDead(fodder);
        assertInProgress();

        skipDay();

        command(digger, GraveDigger.COMMAND, gs.getName(), electro.getName());
        command(digger, GraveDigger.COMMAND, gs.getName(), electro.getName(), Gunsmith.COMMAND, Gunsmith.REAL);
        command(digger, GraveDigger.COMMAND, gs.getName(), electro.getName(), Gunsmith.COMMAND, Gunsmith.FAULTY);
        endNight();

        assertTotalGunCount(1, electro);
        assertTrue(getGun(electro, 0).isFaulty());
    }
}
