package integration.logic;

import java.util.ArrayList;

import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.Role;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Blacksmith;
import game.roles.GraveDigger;
import game.setups.Setup;
import models.FactionRole;
import services.FactionRoleService;
import services.RoleAbilityModifierService;
import util.LookupUtil;
import util.RepeatabilityTest;
import util.TestUtil;

public class TestBlacksmith extends SuperTest {

    public TestBlacksmith(String name) {
        super(name);
    }

    private static final String REAL = Blacksmith.REAL;
    private static final String FAKE = Blacksmith.FAKE;
    private static final String NOGUN = Blacksmith.NOGUN;
    private static final String FAULTY = Blacksmith.FAULTY;
    private static final String NULL = null;

    private static Controller bs, c1, c2, gd, sk;

    private void setupBasic(BlacksmithBlock block, String opt2, String opt3, boolean gun, boolean vest) {
        c1 = addPlayer(BasicRoles.Doctor());
        c2 = addPlayer(BasicRoles.Doctor());
        bs = addPlayer(EvilBlacksmith());
        sk = addPlayer(BasicRoles.SerialKiller());
        gd = addPlayer(BasicRoles.GraveDigger());

        fakeVests();
        fakeGuns();

        ControllerList players = new ControllerList();
        if(gun)
            players.add(c1);
        if(vest)
            players.add(c2);

        setTarget(bs, Blacksmith.MAIN_ABILITY, opt2, opt3, players);
        nextNight();

        block.run();
        // cleanup that usually happens when tests 'finish'
        RepeatabilityTest.runRepeatabilityTest();
        charges = null;

        newNarrator();
        c1 = addPlayer(BasicRoles.Citizen());
        c2 = addPlayer(BasicRoles.Citizen());
        bs = addPlayer(EvilBlacksmith());
        gd = addPlayer(BasicRoles.GraveDigger());
        sk = addPlayer(BasicRoles.SerialKiller());

        fakeGuns();
        fakeVests();

        dayStart();

        voteOut(bs, c1, c2, gd);

        players = new ControllerList();
        players.add(bs);
        if(gun)
            players.add(c1);
        if(vest)
            players.add(c2);

        setTarget(gd, GraveDigger.MAIN_ABILITY, Blacksmith.COMMAND, opt2, opt3, players);
        nextNight();
        block.run();
    }

    public void testSelection() {
        Controller p3 = addPlayer(BasicRoles.Blacksmith());
        Controller p1 = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());

        nightStart();

        Blacksmith bs = new Blacksmith();
        ArrayList<Object> s = bs.getActionDescription(new Action(p3.getPlayer(narrator), Blacksmith.MAIN_ABILITY,
                Blacksmith.NOGUN, Blacksmith.REAL, p1.getPlayer(narrator)));
        for(Object o: s){
            if(o instanceof String){
                if(o.toString().toLowerCase().contains(Constants.VEST_COMMAND.toLowerCase()))
                    ;
                return;
            }
        }
        fail();
    }

    public void testFaultyCommands() {
        fakeGuns();
        fakeVests();

        Controller bs = addPlayer(BasicRoles.Blacksmith());
        Controller p1 = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());

        nightStart();

        command(bs, Blacksmith.COMMAND, p1.getName(), Blacksmith.REAL, Blacksmith.FAKE);
        assertEquals(Blacksmith.NOARMOR, bs.getPlayer(narrator).getActions().getFirst().getOption2());
    }

    interface BlacksmithBlock {
        void run();
    }

    public void testRealGun() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, c2);
                endNight();

                isDead(c2);
            }
        }, REAL, NOGUN, true, false);
    }

    public void testFakeGun() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, c2);
                endNight();

                isDead(c1);
            }
        }, FAULTY, NOGUN, true, false);
    }

    public void testRealVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                setTarget(sk, c2);
                assertRealVestCount(1, c2);
                vest(c2);
                endNight();

                isAlive(c2);
            }
        }, NOGUN, REAL, false, true);
    }

    public void testFakeVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                setTarget(sk, c2);
                assertFakeVestCount(1, c2);
                vest(c2);
                endNight();

                isDead(c2);
            }
        }, NOGUN, FAKE, false, true);
    }

    public void testRealGunRealVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, gd);
                setTarget(sk, c2);
                vest(c2);
                endNight();

                isAlive(c2);
                isDead(gd);
            }
        }, REAL, REAL, true, true);
    }

    public void testRealGunFakeVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, gd);
                setTarget(sk, c2);
                vest(c2);
                endNight();

                isDead(gd, c2);
            }
        }, REAL, FAKE, true, true);
    }

    public void testFakeGunFakeVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, gd);
                setTarget(sk, c2);
                vest(c2);
                endNight();

                isDead(c1, c2);
            }
        }, FAULTY, FAKE, true, true);
    }

    public void testFakeGunRealVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, gd);
                setTarget(sk, c2);
                vest(c2);
                endNight();

                isDead(c1);
                isAlive(c2);
            }
        }, FAULTY, REAL, true, true);
    }

    public void testFactionRoleOverrideModifier() {
        Controller bs_good = addPlayer(BasicRoles.Blacksmith());
        Controller bs_bad = addPlayer(EvilBlacksmith());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());

        Role role = LookupUtil.findRole(narrator, "Blacksmith");
        RoleAbilityModifierService.modify(role, Blacksmith.class, AbilityModifier.GS_FAULTY_GUNS, true);
        FactionRoleService.addModifier(BasicRoles.Blacksmith(), AbilityModifier.GS_FAULTY_GUNS,
                Blacksmith.class, false);

        nightStart();

        try{
            setTarget(bs_good, Blacksmith.MAIN_ABILITY, FAKE, FAKE, citizen);
            fail();
        }catch(PlayerTargetingException e){
        }
        setTarget(bs_bad, Blacksmith.MAIN_ABILITY, FAULTY, FAKE, citizen, citizen2);

        nextNight();

        shoot(citizen, citizen2);
        setTarget(bs_bad, Blacksmith.MAIN_ABILITY, REAL, FAKE, bs_good, citizen);
        nextNight();

        isDead(citizen);

        vest(citizen2);
        shoot(bs_good, citizen2);

        endNight();

        isDead(citizen2);
    }

    public void testGoodBadBlacksmith() {
        Role blacksmithRole = LookupUtil.findRole(narrator, "Blacksmith");
        FactionRole BADBlacksmithFactionRole = LookupUtil.findFactionRole(narrator, "Blacksmith", Setup.OUTCAST_C);

        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller bs_good = addPlayer(BasicRoles.Blacksmith());
        Controller bs_bad = addPlayer(EvilBlacksmith());

        modifyRole(blacksmithRole, AbilityModifier.GS_FAULTY_GUNS, false);
        modifyRole(blacksmithRole, AbilityModifier.AS_FAKE_VESTS, false);
        
        FactionRoleService.addModifier(BADBlacksmithFactionRole, AbilityModifier.GS_FAULTY_GUNS, Blacksmith.class, true);
        FactionRoleService.addModifier(BADBlacksmithFactionRole, AbilityModifier.AS_FAKE_VESTS, Blacksmith.class, true);

        nightStart();
        
        assertEquals(Setup.TOWN_C, bs_good.getColor());

        Ability bs_card = bs_bad.getPlayer(narrator).getAbility(Blacksmith.MAIN_ABILITY);
        assertTrue(bs_card.modifiers.getOrDefault(AbilityModifier.AS_FAKE_VESTS, false));
        assertTrue(bs_card.modifiers.getOrDefault(AbilityModifier.GS_FAULTY_GUNS, false));

        bs_card = bs_good.getPlayer(narrator).getAbility(Blacksmith.MAIN_ABILITY);
        assertFalse(bs_card.modifiers.getOrDefault(AbilityModifier.AS_FAKE_VESTS, true));
        assertFalse(bs_card.modifiers.getOrDefault(AbilityModifier.GS_FAULTY_GUNS, true));

        try{
            setTarget(bs_good, Blacksmith.MAIN_ABILITY, FAKE, FAKE, cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(bs_good, Blacksmith.MAIN_ABILITY, REAL, REAL, cit, cit2);
        setTarget(bs_bad, Blacksmith.MAIN_ABILITY, FAULTY, FAKE, cit, cit2);

        assertNotNull(bs_bad.getPlayer(narrator).getActions().getFirst().getOption());
        assertNotNull(bs_bad.getPlayer(narrator).getActions().getFirst().getOption2());

        nextNight();
        shoot(cit, bs_good);
        nextNight();

        shoot(cit, bs_bad);
        endNight();
        isDead(cit);

        assertRealVestCount(1, cit2);
    }

    public void testBadTargetingRealEverything() {
        Controller bs = addPlayer(BasicRoles.Blacksmith());
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        realGuns();
        realVests();

        nightStart();

        badTarget(bs, NULL, NULL, p1);
        badTarget(bs, null, FAKE, p1);
        badTarget(bs, NOGUN, null, p1, p2);
        badTarget(bs);
        badTarget(bs, p1);
        badTarget(bs, bs);
        badTarget(bs, p1, p1);
        badTarget(bs, p1, p2, sk);
        badTarget(bs, FAULTY, null, p1);
    }

    public void badTarget(Controller bs, String gunOpt, String vestOpt, Controller... targets) {
        try{
            setTarget(bs, Blacksmith.MAIN_ABILITY, gunOpt, vestOpt, targets);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void badTarget(Controller bs, Controller... targets) {
        badTarget(bs, null, null, targets);
    }

    public static void fakeGuns() {
        TestUtil.modifyAbility(Blacksmith.class, AbilityModifier.GS_FAULTY_GUNS, true);
    }

    public static void fakeVests() {
        TestUtil.modifyAbility(Blacksmith.class, AbilityModifier.AS_FAKE_VESTS, true);
    }

    public static FactionRole EvilBlacksmith() {
        return BasicRoles.getMember(Setup.OUTCAST_C, new Blacksmith());
    }

    public static void realGuns() {
        TestUtil.modifyAbility(Blacksmith.class, AbilityModifier.AS_FAKE_VESTS, false);
    }

    public static void realVests() {
        TestUtil.modifyAbility(Blacksmith.class, AbilityModifier.AS_FAKE_VESTS, false);
    }
}
