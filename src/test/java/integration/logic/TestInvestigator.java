package integration.logic;

import game.ai.Controller;
import game.logic.Faction;
import game.logic.Role;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Assassin;
import game.roles.Citizen;
import game.roles.Snitch;
import game.setups.Setup;
import services.FactionService;
import services.RoleService;

public class TestInvestigator extends SuperTest {

    public TestInvestigator(String s) {
        super(s);
    }

    public void testBasicAbility() {
        addPlayer(BasicRoles.Citizen(), 2);
        Controller invest = addPlayer(BasicRoles.Investigator());
        Controller ass = addPlayer(BasicRoles.Assassin());

        setTarget(invest, ass);

        endNight();

        seen(invest, Assassin.class);
    }

    public static void seen(Controller p, Class<? extends Ability> c) {
        partialContains(p, c.getSimpleName() + ".");
    }

    public static void notSeen(Controller p, Class<? extends Ability> c) {
        partialExcludes(p, c.getSimpleName() + ".");
    }

    public void testUndetectableSelection() {
        Controller invest = addPlayer(BasicRoles.Investigator());
        Controller gf = addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Citizen());

        setTarget(invest, gf);
        endNight();

        partialContains(invest, Citizen.class.getSimpleName());
    }

    public void testUndetectableSelectionNewName() {
        Role role = RoleService.createRole(narrator, "Villager", Citizen.class);
        Faction faction = narrator.getFaction(Setup.TOWN_C);
        Controller invest = addPlayer(BasicRoles.Investigator());
        Controller gf = addPlayer(BasicRoles.Godfather());
        addPlayer(FactionService.createFactionRole(faction, role));

        nightStart();

        assertFalse(gf.getPlayer(narrator).isDetectable());
        assertNotNull(Snitch.getCitizenTeam(narrator));

        setTarget(invest, gf);
        endNight();

        partialContains(invest, "Villager");
    }

    public void testFramedTarget() {
        Controller snitch = addPlayer(BasicRoles.Snitch());
        Controller invest = addPlayer(BasicRoles.Investigator());
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller framer = addPlayer(BasicRoles.Framer());

        setTarget(invest, gf);
        frame(framer, gf, snitch.getColor());
        endNight();

        partialContains(invest, Snitch.class.getSimpleName() + ".");
    }
}
