package integration.logic;

import java.util.ArrayList;

import game.ai.Controller;
import game.event.Announcement;
import game.event.Message;
import game.logic.Faction;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Hidden;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.setups.Setup;
import models.SetupHidden;
import services.SetupHiddenService;
import util.LookupUtil;

public class TestSheriff extends SuperTest {

    public TestSheriff(String s) {
        super(s);
    }

    // tests if sheriff is valid
    public void testSheriffCheck() {
        // serial killer test
        narrator.toString();
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(Hidden.TownRandom());
        removeSheriffDetectable(Setup.TOWN_C, Setup.THREAT_C);

        assertBadGameSettings();

        // cleanup
        removeSetupHidden(Hidden.TownRandom());
        SetupHidden setupHidden = LookupUtil.findSetupHidden(narrator, "Serial Killer");
        SetupHiddenService.deleteSetupHidden(narrator, setupHidden);

        // mafia
        addSetupHidden(Hidden.TownInvestigative());
        addSetupHidden(Hidden.MafiaRandom());
        removeSheriffDetectable(Setup.TOWN_C, Setup.MAFIA_C);

        assertBadGameSettings();
    }

    public void testSheriffNoDifference() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        addSheriffDetectable(BasicRoles.Citizen(), BasicRoles.SerialKiller());
        editRule(SetupModifier.CHECK_DIFFERENTIATION, false);

        assertTrue(narrator.getFaction(Setup.TOWN_C).sheriffDetects(Setup.THREAT_C));

        nightStart();

        assertTrue(sheriff.is(Sheriff.class));
        assertTrue(sk.is(SerialKiller.class));
        assertTrue(narrator.getFaction(Setup.TOWN_C).sheriffDetects(Setup.THREAT_C));
        assertTrue(narrator.getFaction(sheriff.getColor()).sheriffDetects(sk.getColor()));

        setTarget(sheriff, sk);
        endNight();

        partialContains(sheriff, "suspicious");

        skipDay();

        setTarget(sheriff, cit);
        endNight();

        partialContains(sheriff, "not suspicious");
    }

    public void testPregamePeekNight() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller towny = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifier.SHERIFF_PREPEEK, true);

        nightStart();

        partialContains(sheriff, towny.getName());
    }

    public void testPregamePeekDay() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller towny = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifier.SHERIFF_PREPEEK, true);

        dayStart();

        partialContains(sheriff, towny.getName());
    }

    public static void seen(Controller sheriff, String teamColor) {
        seen(sheriff, narrator.getFaction(teamColor));
    }

    public static void seen(Controller sheriff, Faction team) {
        ArrayList<Object> list = Sheriff.generateFeedback(team, narrator);

        Message om = new Announcement(narrator).add(list);

        String message = om.access(sheriff.getPlayer(narrator));
        partialContains(sheriff, message);
    }
}
