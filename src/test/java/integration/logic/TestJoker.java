package integration.logic;

import java.util.ArrayList;

import game.ai.Controller;
import game.ai.ControllerList;
import game.event.Message;
import game.logic.Faction;
import game.logic.Role;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.DrugDealer;
import game.roles.Joker;
import game.setups.Setup;
import services.FactionService;
import services.RoleService;

public class TestJoker extends SuperTest {

    public TestJoker(String s) {
        super(s);
    }

    ArrayList<Message> announcements;
    Controller joker;

    @Override
    public void roleInit() {
        joker = addPlayer(BasicRoles.Joker());
        announcements = addAnnouncementListener();
    }

    public void testBasic() {
        Controller citB = addPlayer(BasicRoles.Citizen());
        Controller citC = addPlayer(BasicRoles.Citizen());
        Controller citD = addPlayer(BasicRoles.Citizen());
        Controller citE = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.BOUNTY_INCUBATION, 1);
        editRule(SetupModifier.BOUNTY_INCUBATION, 0);
        assertEquals(1, narrator.getInt(SetupModifier.BOUNTY_INCUBATION));
        editRule(SetupModifier.BOUNTY_INCUBATION, 2);

        bounty(citE);
        endNight();

        isBountied(citE, 2);
        skipDay();
        isBountied(citE);
        endNight();

        isBountied(citE);
        skipDay();

        assertBountyExpired(citE);

        bounty(citD, citB, citC);
        endNight();

        isDead(citB, citC);
        isBountied(citD);
        isNotBountied(citE);
    }

    public void testBadAttempts() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller arc = addPlayer(BasicRoles.Architect());

        try{
            onlyKill(cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        bounty(cit);

        nextNight();

        try{
            bounty(arc);
            fail();
        }catch(PlayerTargetingException e){
        }

        nextNight();
        try{
            bounty(arc, arc);
            fail();
        }catch(PlayerTargetingException e){
        }
        bounty(arc);
    }

    public void test1DayLimit() {
        Controller cit = addPlayer(BasicRoles.Architect());
        Controller arc = addPlayer(BasicRoles.Architect());
        addPlayer(BasicRoles.Architect());

        editRule(SetupModifier.BOUNTY_INCUBATION, 1);

        bounty(cit);
        nextNight();
        bounty(cit, arc);
        endNight();

        isDead(arc);
        isBountied(cit);
    }

    public void testManipulation() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.BOUNTY_INCUBATION, 1);

        bounty(witch);
        witch(witch, joker, bd);
        endNight();

        isNotBountied(bd);
        isBountied(witch);
        skipDay();

        // successful bounty here

        bounty(witch);
        drive(bd, witch, bd);
        endNight();

        isBountied(witch);
        isNotBountied(bd);

        skipDay();
        // 2 successful bounties
        assertEquals(2 * narrator.getInt(SetupModifier.BOUNTY_FAIL_REWARD), getRoleCard().bounty_kills);

        onlyKill(witch);
        witch(witch, joker, cit);
        endNight();

        // 2 sucess, 1 kill used

        isDead(cit);
        skipDay();

        bounty(witch, bd);
        witch(witch, joker, witch);
        endNight();

        isDead(witch);
        // 2 success, 2 kills used
        assertJokerKills(2, 2);
    }

    public void testWitchAddingBounty() {
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.BOUNTY_INCUBATION, 1);

        witch(witch, joker, witch);
        endNight();

        isBountied(witch);
        skipDay();

        assertJokerKills(1, 0);
        witch(witch, joker, witch);
        endNight();

        isDead(witch);
    }

    private void assertJokerKills(int bounties, int usedKills) {
        assertEquals((bounties * narrator.getInt(SetupModifier.BOUNTY_FAIL_REWARD)) - usedKills,
                getRoleCard().bounty_kills);
    }

    public void testVisitation() {
        Controller detective = addPlayer(BasicRoles.Detective());
        addPlayer(BasicRoles.Citizen());

        setTarget(detective, joker);
        bounty(detective);
        endNight();

        TestDetective.seen(detective, detective);
    }

    public void testDeadJoker() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifier.MAYOR_VOTE_POWER, 3);

        bounty(mayor);
        endNight();

        reveal(mayor);
        vote(mayor, joker);
        isDead(joker);

        endNight();
        isBountied(mayor, narrator.getInt(SetupModifier.BOUNTY_INCUBATION) - 1);
        skipDay();

        assertBountyExpired(mayor);
    }

    public void testGhostJoker() {
        Controller ghost = addPlayer(BasicRoles.Ghost());
        ControllerList cits = addPlayer(BasicRoles.Citizen(), 3);

        bounty(ghost);
        endNight();

        lynch(ghost, cits);
        endNight();

        lynch(joker, cits);
        TestGhost.assertCause(ghost, joker);
        isWinner(ghost);
    }

    public void testMarshallGhostBounty() {
        addPlayer(BasicRoles.Arsonist());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller marshall = addPlayer(BasicRoles.Marshall());
        ControllerList cits = addPlayer(BasicRoles.Citizen(), 4);

        editRule(SetupModifier.MARSHALL_EXECUTIONS, 1);

        nightStart();
        assertEquals(1, narrator.getInt(SetupModifier.MARSHALL_EXECUTIONS));

        bounty(ghost);
        endNight();

        order(marshall);
        assertEquals(2, narrator._lynches);
        lynch(joker, cits);
        vote(marshall, joker);
        assertEquals(1, narrator._lynches);
        isDead(joker);

        lynch(ghost, cits);
        isDead(ghost);

        assertIsNight();
        TestGhost.assertCause(ghost, cits.getLast());
    }

    public void testHasBountyNotCauseGhost() {
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.GS_DAY_GUNS, true);

        setTarget(gs, cit);
        bounty(ghost);
        endNight();

        shoot(cit, ghost);

        voteOut(gs, joker, cit);
        TestGhost.assertCause(ghost, cit);
        ;
    }

    public void testElectroAllActions() {
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.BOUNTY_INCUBATION, 1);
        
        Role role = RoleService.createRole(narrator, "WeakJoker", Joker.class);
        Faction faction = narrator.getFaction(Setup.THREAT_C);
        joker = addPlayer(FactionService.createFactionRole(faction, role));

        bounty(cit);
        isInvuln(joker, false);
        isInvuln(electro);
        endNight(joker);
        electrify(electro, joker, cit);

        bounty(electro, cit);
        endNight();

        isBountied(electro);
        isDead(cit);
    }

    public void testCorpseGhostGraveDigger() {
        Controller gd = addPlayer(BasicRoles.GraveDigger());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller dit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.BOUNTY_INCUBATION, 2);

        voteOut(joker, gd, ghost, cit);

        setTarget(gd, joker, ghost);
        endNight();

        isBountied(ghost, narrator.getInt(SetupModifier.BOUNTY_INCUBATION));
        voteOut(ghost, gd, cit, dit);
        assertIsNight();

        TestGhost.assertCause(ghost, gd);

        setTarget(gd, joker, gd);
        endNight();

        isBountied(gd);

        skipDay();
        setTarget(gd, joker, cit);
        endNight();

        isNotBountied(cit);
    }

    public void testBakerJoker() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.BOUNTY_INCUBATION, 2);
        editRule(SetupModifier.BREAD_PASSING, false);

        setTarget(baker, joker);
        setTarget(gs, joker);
        bounty(baker); // main bounty
        nextNight();

        bounty(gs); // side bounty, jokerBread = 1
        setTarget(baker, joker);
        assertFalse(joker.getPlayer(narrator).getActions()
                .canAddAnotherAction(joker.getPlayer(narrator).action(cit.getPlayer(narrator))));
        endNight();

        assertPassableBreadCount(0, joker);
        assertUseableBreadCount(1, joker);
        isBountied(baker, 1);
        isBountied(gs, 2);
        skipDay(); // main bounty ends

        bounty(cit); // main bounty
        // assertEquals(1, joker.getActionWeight());
        assertTrue(joker.getPlayer(narrator).getActions().canAddAnotherAction(Ability.SHOOT));
        shoot(joker, cit);
        assertActionSize(2, joker);
        cancelAction(joker, 1);
        nextNight();

        shoot(joker, cit);

        assertNotNull(getRoleCard().activeBounty);
        assertUseableBreadCount(1, joker);

        Action a = joker.getPlayer(narrator).action(cit.getPlayer(narrator));
        assertTrue(joker.getPlayer(narrator).getActions().canAddAnotherAction(a));
        assertFalse(a.getAbility().canSubmitWithoutBread(a));
    }

    public void testFakeRoleblockable() {
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Escort());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);
        modifyAbilityCharges(BasicRoles.Joker(), 1);

        drug(dd, joker, DrugDealer.BLOCKED);
        bounty(dd);
        endNight();

        isBountied(dd);
        assertRealChargeRemaining(0, joker, Joker.MAIN_ABILITY);
    }

    public void testFakeBread() {
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller baker = addPlayer(BasicRoles.Baker());

        drug(dd, joker, DrugDealer.BREAD);
        bounty(dd);
        endNight();
        skipDay();

        bounty(baker);
        endNight();

        isNotBountied(baker);
        assertPassableBreadCount(0, joker);
    }

    public void testCommuter() {
        Controller commuter = addPlayer(BasicRoles.Commuter());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller fodder = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.BOUNTY_INCUBATION, 1);

        commute(commuter);
        bounty(commuter);
        endNight();

        isNotBountied(commuter);
        skipDay();

        bounty(fodder);
        nextNight();

        bounty(cit, commuter);
        commute(commuter);
        endNight();

        isAlive(commuter);
        isBountied(cit);
        skipDay();

        bounty(commuter, cit);
        commute(commuter);
        endNight();

        isDead(cit);
        isNotBountied(commuter);
        skipDay();
    }

    public void testSimple() {
        Controller bakerA = addPlayer(BasicRoles.Baker());
        Controller bakerB = addPlayer(BasicRoles.Baker());

        dayStart();
        skipDay();

        setTarget(bakerA, joker);
        setTarget(bakerB, joker);
        bounty(bakerB);
        endNight();
        skipDay();

        Action a = new Action(joker.getPlayer(narrator), Joker.MAIN_ABILITY, bakerA.getPlayer(narrator));
        assertTrue(joker.getPlayer(narrator).getActions().canAddAnotherAction(a));
        setTarget(joker, bakerA);
        endNight();
    }

    private void bounty(Controller... bounties) {
        setTarget(joker, ControllerList.list(bounties), Joker.MAIN_ABILITY);
    }

    private void onlyKill(Controller... bounties) {
        setTarget(joker, Joker.MAIN_ABILITY, Joker.NO_BOUNTY, bounties);
    }

    private void isBountied(Controller p, int days) {
        isBountied(p);
        String text = Joker.DayStartAnnouncementCreator(p.getPlayer(narrator), days).access(Message.PUBLIC);
        for(Message a: announcements){
            if(a.access(Message.PUBLIC).equals(text) && a.getDay() == narrator.getDayNumber())
                return;
        }
        fail();
    }

    private void assertBountyExpired(Controller p) {
        isNotBountied(p);
        String text = Joker.DayEndAnnouncementCreator(p.getPlayer(narrator)).access(Message.PUBLIC);
        for(Message a: announcements){
            if(a.access(Message.PUBLIC).equals(text) && a.getDay() == narrator.getDayNumber())
                return;
        }
        fail();

    }

    private static void isBountied(Controller p) {
        assertTrue(p.getPlayer(narrator).isBountied());
    }

    private static void isNotBountied(Controller citA) {
        assertFalse(citA.getPlayer(narrator).isBountied());
    }

    private Joker getRoleCard() {
        return joker.getPlayer(narrator).getAbility(Joker.class);
    }
}
