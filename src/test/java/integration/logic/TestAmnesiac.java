package integration.logic;

import game.ai.Controller;
import game.logic.Faction;
import game.logic.Role;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.logic.templates.BasicRoles;
import game.roles.Amnesiac;
import game.roles.Goon;
import game.roles.GunAbility;
import game.roles.Hidden;
import game.roles.Survivor;
import game.roles.VestAbility;
import game.roles.Veteran;
import game.roles.Vigilante;
import game.setups.Setup;
import services.FactionRoleService;
import services.FactionService;
import util.LookupUtil;

public class TestAmnesiac extends SuperTest {

    // has github page

    public TestAmnesiac(String name) {
        super(name);
    }

    // tests amnesiac not doing anything, and the basic ability
    public void testBasicAbility() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller m1 = addPlayer(BasicRoles.Goon());
        Controller a1 = addPlayer(BasicRoles.Amnesiac());

        dayStart();
        voteOut(c2, m1, a1, c1);
        endNight(); // makes sure no null pointer is thrown when amnesiac doesn'tdo anything
        skipDay();

        try{
            setTarget(a1, c1);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(a1, c2);

        endNight();

        assertEquals(a1.getColor(), c1.getColor());
    }

    // previous issue where the amnesiac turned mafia still was a member of the
    // benign team
    public void testMafSend() {
        Controller v = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Goon());
        Controller m2 = addPlayer(BasicRoles.Goon());
        Controller a1 = addPlayer(BasicRoles.Amnesiac());

        nightStart();

        Faction benign = narrator.getFaction(BasicRoles.Amnesiac().getColor());

        shoot(v, m2);
        nextNight();

        assertIsNight();
        assertInProgress();

        setTarget(a1, m2);
        endNight();

        assertFalse(benign.hasMember(a1.getPlayer(narrator)));
    }

    // amnesiac taking a team that is about to die
    public void testTeamExtinction() {
        Controller m1 = addPlayer(BasicRoles.Goon());
        Controller m2 = addPlayer(BasicRoles.Goon());
        Controller a1 = addPlayer(BasicRoles.Amnesiac());
        Controller v = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        shoot(v, m1);
        endNight();
        skipDay();
        shoot(v, m2);
        setTarget(a1, m1);
        endNight();

        Faction mafia = narrator.getFaction(m1.getColor());
        assertTrue(mafia.isAlive());
        assertInProgress();
    }

    // witch to live people
    public void testWitchToLive() {
        Controller c = addPlayer(BasicRoles.Citizen());
        Controller v = addPlayer(BasicRoles.Vigilante());
        Controller a = addPlayer(BasicRoles.Amnesiac());
        Controller w = addPlayer(BasicRoles.Witch());

        dayStart();
        voteOut(v, w, a, c);

        witch(w, a, w);
        endNight();

        partialContains(a, Amnesiac.LIVE_FEEDBACK);
    }

    // this works because doused is a Controller status
    public void testArsonPersist() {
        Controller c = addPlayer(BasicRoles.Citizen());
        Controller v = addPlayer(BasicRoles.Vigilante());
        Controller amnes = addPlayer(BasicRoles.Amnesiac());
        Controller ars = addPlayer(BasicRoles.Arsonist());

        nightStart();

        setTarget(ars, amnes);
        endNight();
        voteOut(v, c, amnes, ars);

        setTarget(amnes, v);

        endNight();
        skipDay();

        shoot(amnes, c);
        setTarget(ars);
        endNight();

        isDead(amnes);
    }

    public void test1Use() {
        modifyRole(BasicRoles.Survivor(), AbilityModifier.CHARGES, 1); // going to make sure at least one
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, SetupModifiers.UNLIMITED);
        modifyRole(BasicRoles.Veteran(), AbilityModifier.CHARGES, SetupModifiers.UNLIMITED);

        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller a_vet = addPlayer(BasicRoles.Amnesiac());
        Controller a_vig = addPlayer(BasicRoles.Amnesiac());
        Controller a_sur = addPlayer(BasicRoles.Amnesiac());
        Controller surv = addPlayer(BasicRoles.Survivor());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.AMNESIAC_KEEPS_CHARGES, false);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        dayStart();

        voteOut(vet, vigi, cit, surv, witch, a_vig);

        shoot(vigi, surv);

        endNight();
        voteOut(vigi, a_vet, a_vig, a_sur, witch);

        setTarget(a_vet, vet); // a1 taking the vet role
        setTarget(a_vig, vigi);
        setTarget(a_sur, surv);

        endNight();

        assertPerceivedChargeRemaining(SetupModifiers.UNLIMITED, a_vig, GunAbility.MAIN_ABILITY);
        assertPerceivedChargeRemaining(1, a_sur, VestAbility.MAIN_ABILITY);
        assertPerceivedChargeRemaining(SetupModifiers.UNLIMITED, a_vet, Veteran.MAIN_ABILITY);
    }

    public void testVestKeepingPreviousRole() {
        Controller surv = addPlayer(BasicRoles.Survivor());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.AMNESIAC_KEEPS_CHARGES, false);

        modifyRole(BasicRoles.Survivor(), AbilityModifier.CHARGES, 1);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        setTarget(as, amn);
        vest(surv);

        endNight();

        voteOut(surv, amn, as, witch);

        setTarget(amn, surv);

        endNight();

        assertTotalVestCount(2, amn);
    }

    // amnesiac remembering unique cleaned role

    public void testUniqueTargeting() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller ars = addPlayer(BasicRoles.Arsonist());

        voteOut(mayor, cit, amn, ars);

        try{
            setTarget(amn, mayor);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testGunKeepingPreviousRole() {
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller fodder = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);
        editRule(SetupModifier.AMNESIAC_KEEPS_CHARGES, false);
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 1);

        setTarget(gs, amn);
        shoot(vigi, fodder);

        endNight();

        voteOut(vigi, amn, gs, witch);

        setTarget(amn, vigi);

        endNight();

        assertTotalGunCount(2, amn);
    }

    public void testKeepCharges() {
        modifyRole(BasicRoles.Veteran(), AbilityModifier.CHARGES, SetupModifiers.UNLIMITED);
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 10);
        modifyRole(BasicRoles.Survivor(), AbilityModifier.CHARGES, 1); // going to make sure at least one

        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller a1 = addPlayer(BasicRoles.Amnesiac());
        Controller a2 = addPlayer(BasicRoles.Amnesiac());
        Controller a3 = addPlayer(BasicRoles.Amnesiac());
        Controller surv = addPlayer(BasicRoles.Survivor());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.AMNESIAC_KEEPS_CHARGES, true);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        assertEquals(SetupModifiers.UNLIMITED, BasicRoles.Veteran().role._abilities.getAbility(Veteran.class).modifiers
                .getInt(AbilityModifier.CHARGES));
        assertEquals(1,
                BasicRoles.Survivor().role.getAbility(Survivor.class).modifiers.getInt(AbilityModifier.CHARGES));
        assertEquals(10,
                BasicRoles.Vigilante().role.getAbility(Vigilante.class).modifiers.getInt(AbilityModifier.CHARGES));

        nightStart();
        vest(surv);
        shoot(vigi, surv);

        endNight();

        voteOut(vet, vigi, cit, surv, witch, a2);

        shoot(vigi, surv);

        endNight();
        voteOut(vigi, a1, a2, a3, witch);

        setTarget(a1, vet); // a1 taking the vet role
        setTarget(a2, vigi);
        setTarget(a3, surv);

        endNight();

        assertTotalVestCount(1, a3);
        assertPerceivedChargeRemaining(SetupModifiers.UNLIMITED, a1, Veteran.MAIN_ABILITY);
        assertTotalGunCount(8, a2);
    }

    public void testUniqueRoleTaking() {
        Controller m = addPlayer(BasicRoles.Mayor());
        Controller gf1 = addPlayer(BasicRoles.Godfather());
        Controller gf2 = addPlayer(BasicRoles.Godfather(Setup.YAKUZA_C));
        Controller a = addPlayer(BasicRoles.Amnesiac());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.BusDriver());

        modifyRole(BasicRoles.Goon(), RoleModifier.UNIQUE, true);
        modifyRole(BasicRoles.getMember(Setup.YAKUZA_C, new Goon()), RoleModifier.UNIQUE, false);

        nightStart();

        mafKill(gf1, gf1);
        mafKill(gf2, gf2);
        setTarget(sk, m);

        endNight();
        assertInProgress();
        skipDay();

        try{
            setTarget(a, gf1);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            setTarget(a, gf2);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            setTarget(a, m);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testTownAmnesiac() {
        Role role = LookupUtil.findRole(narrator, "Amnesiac");
        Faction faction = narrator.getFaction(Setup.TOWN_C);

        Controller amn = addPlayer(FactionService.createFactionRole(faction, role));
        Controller town = addPlayer(Hidden.TownRandom());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller maf = addPlayer(BasicRoles.Agent());
        Controller maf2 = addPlayer(BasicRoles.Chauffeur());

        town.clearPreferences();

        dayStart();

        assertTrue(amn.is(Amnesiac.class));

        voteOut(maf, amn, town, witch);

        assertFalse(amn.getPlayer(narrator).isAcceptableTarget(
                new Action(amn.getPlayer(narrator), maf.getPlayer(narrator), Amnesiac.MAIN_ABILITY, null)));
        try{
            setTarget(amn, maf);
            fail();
        }catch(PlayerTargetingException e){
        }

        endNight();

        voteOut(maf2, amn, town, witch);

        setTarget(amn, maf2);
        endNight();

        assertEquals(town.getColor(), amn.getColor());
    }

    public void testAmnesiacMustRemember() {
        Controller amnesiac = addPlayer(BasicRoles.Amnesiac());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());

        editRule(SetupModifier.AMNESIAC_MUST_REMEMBER, true);

        mafKill(maf, cit);
        endNight();

        isLoser(amnesiac);
    }

    public void testAmnesiacNoCharge() {
        try{
            modifyAbilityCharges(BasicRoles.Amnesiac(), Amnesiac.class, 1);
            fail();
        }catch(IllegalGameSettingsException e){
        }

        assertFalse(BasicRoles.Amnesiac().role.getAbility(Amnesiac.class).modifiers.hasKey(AbilityModifier.CHARGES));
    }

    public void testGetInvulnerability() {
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller wit = addPlayer(BasicRoles.Witch());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller amn = addPlayer(BasicRoles.Amnesiac());

        shoot(vig, sk);
        endNight();

        assertTrue(sk.getPlayer(narrator).isInvulnerable());
        voteOut(sk, vig, wit, amn);

        setTarget(amn, sk);
        nextNight();

        shoot(vig, amn);
        endNight();

        isAlive(amn);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Amnesiac(), AbilityModifier.BACK_TO_BACK, Amnesiac.class, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
