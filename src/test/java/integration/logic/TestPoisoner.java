package integration.logic;

import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Poisoner;;

public class TestPoisoner extends SuperTest {

    public TestPoisoner(String name) {
        super(name);
    }

    public void testBasic() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        Controller poi = addPlayer(BasicRoles.Poisoner());

        nightStart();

        setTarget(poi, cit);
        endNight();

        skipDay();

        partialContains(cit, Poisoner.POISONER_FEEDBACK);

        isDead(cit);
        partialContains(Poisoner.BROADCAST_MESSAGE);
        partialContains(cit, Poisoner.DEATH_FEEDBACK);

        assertTrue(cit.getPlayer(narrator).getDeathType().getList().contains(Constants.POISON_KILL_FLAG));
    }

    public void testSelfImmuneDeath() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller poi = addPlayer(BasicRoles.Poisoner());

        drive(bd, poi, gf);
        setTarget(poi, gf);

        nextNight();
        isDead(poi);
    }

    public void testImmune() {
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller poi = addPlayer(BasicRoles.Poisoner());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        setTarget(poi, gf);

        nextNight();
        isAlive(gf);

        partialContains(gf, Poisoner.POISONER_FAILED_FEEDBACK);
    }

    public void testDoctorSaveOptionOff() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller poi = addPlayer(BasicRoles.Poisoner());

        editRule(SetupModifier.HEAL_BLOCKS_POISON, false);

        nightStart();

        setTarget(doc, cit);
        setTarget(poi, cit);

        nextNight();
        isDead(cit);
    }

    public void multiPoisoningSingleFeedback() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller poi = addPlayer(BasicRoles.Poisoner());
        Controller poi2 = addPlayer(BasicRoles.Poisoner());

        nightStart();

        setTarget(poi2, cit);
        setTarget(poi, cit);

        endNight();

        partialExcludes(cit, Poisoner.POISONER_FAILED_FEEDBACK);
        partialContains(cit, Poisoner.POISONER_FEEDBACK);
    }

    public void testPoisonGameContinues() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller poi = addPlayer(BasicRoles.Poisoner());
        addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.BusDriver());

        nightStart();

        setTarget(poi, cit);

        endNight();
        skipDay();

        assertInProgress();
    }

    public void testPoisonDoctorHeal() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller poi = addPlayer(BasicRoles.Poisoner());

        editRule(SetupModifier.HEAL_BLOCKS_POISON, true);

        setTarget(doc, cit);
        setTarget(poi, cit);
        endNight();

        assertStatus(cit, Poisoner.MAIN_ABILITY, false);
    }

    public void testPoisonDoctorNoHeal() {
        addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller poi = addPlayer(BasicRoles.Poisoner());

        editRule(SetupModifier.HEAL_BLOCKS_POISON, true);

        setTarget(poi, cit);
        endNight();

        assertStatus(cit, Poisoner.MAIN_ABILITY);
        skipDay();

        isDead(cit);
    }

    public void testSelfPoisoning() {
        Controller poisoner = addPlayer(BasicRoles.Poisoner());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        addPlayer(BasicRoles.Citizen());

        poisoner.getPlayer(narrator);

        drive(bd, bd, poisoner);
        setTarget(poisoner, poisoner);

        TestMafiaTeam.gotWarning(poisoner);

        endNight();

        assertStatus(bd, Poisoner.MAIN_ABILITY);
    }

    public void testNoSelfPoisoning() {
        Controller poisoner = addPlayer(BasicRoles.Poisoner());
        addPlayer(BasicRoles.Citizen(), 2);

        poisoner.getPlayer(narrator);

        try{
            setTarget(poisoner, poisoner);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testAutovestTrigger() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller poisoner = addPlayer(BasicRoles.Poisoner());
        addPlayer(BasicRoles.Citizen(), 2);

        modifyRole(BasicRoles.Sheriff(), RoleModifier.AUTO_VEST, 1);

        setTarget(poisoner, sheriff);
        endNight();

        assertPerceivedAutovestCount(0, sheriff);
        assertStatus(sheriff, Poisoner.MAIN_ABILITY, false);
    }

    // ghost doctor blocks poison on
    // poisoning someone with an autovest should trigger it.
}
