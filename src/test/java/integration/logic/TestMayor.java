package integration.logic;

import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.Role;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Hidden;
import game.roles.Mayor;
import game.setups.Setup;
import models.Command;
import models.FactionRole;
import models.SetupHidden;
import services.FactionRoleService;
import services.FactionService;
import services.HiddenService;
import services.RoleService;
import services.SetupHiddenService;
import util.LookupUtil;

public class TestMayor extends SuperTest {

    public TestMayor(String name) {
        super(name);
    }

    public void testCommands() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Witch());

        vote(mayor, witch);
        reveal(mayor);

        partialExcludes(Command.getText(narrator.getCommands()), Constants.UNVOTE);
    }

    public void testVotes() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Citizen());

        dayStart();

        assertEquals(Mayor.MAIN_ABILITY, Ability.ParseAbility(Mayor.COMMAND));

        reveal(mayor);

        try{
            reveal(mayor);
            fail();
        }catch(NarratorException e){
        }

        vote(mayor, witch);
        assertGameOver();
    }

    public void testCultConversion() {
        modifyRole(BasicRoles.Mayor(), RoleModifier.UNCONVERTABLE, true);

        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        try{
            setTarget(mayor);
            fail();
        }catch(NarratorException e){
        }

        String originalColor = mayor.getColor();

        setTarget(cl, mayor);
        endNight();

        assertEquals(mayor.getColor(), originalColor);
    }

    public void testNotEnoughToLynch() {
        assertEquals(false, narrator.getBool(SetupModifier.HOST_VOTING));
        ControllerList cits = addPlayer(BasicRoles.Citizen(), 6);
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller witch = addPlayer(BasicRoles.Witch());

        setMayorVotes(3);
        dayStart();

        vote(witch, mayor);
        vote(mayor, witch);
        reveal(mayor);
        vote(cits.get(0), witch);

        assertGameOver();
    }

    private static void setMayorVotes(int vPower) {
        editRule(SetupModifier.MAYOR_VOTE_POWER, vPower);
    }

    public void testMultiMayor2() {
        addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Mayor());

        assertBadGameSettings();

        SetupHidden setupHidden = LookupUtil.findSetupHidden(narrator, "Mayor");
        SetupHiddenService.deleteSetupHidden(narrator, setupHidden);

        try{
            RoleService.createRole(narrator, "Mayor", Mayor.class);
            fail();
        }catch(NamingException e){
        }

        setupHidden = LookupUtil.findSetupHidden(narrator, "Mayor");
        SetupHiddenService.deleteSetupHidden(narrator, setupHidden);
        assertNull(LookupUtil.findSetupHidden(narrator, "Mayor"));
    }

    public void testMultiMayor3() {
        addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Mayor());

        try{
            dayStart();
            fail();
        }catch(IllegalRoleCombinationException | IllegalGameSettingsException e){
        }

        SetupHidden setupHidden = LookupUtil.findSetupHidden(narrator, "Mayor");
        SetupHiddenService.deleteSetupHidden(narrator, setupHidden);

        Role role = RoleService.createRole(narrator, "Pseudo Mayor", Mayor.class);
        FactionRole factionRole = FactionService.createFactionRole(narrator.getFaction(Setup.TOWN_C), role);
        Hidden hidden = HiddenService.createHidden(narrator, "test", factionRole);
        addSetupHidden(hidden);

        dayStart();

    }

    public void testMultiMayor() {
        Controller toRemove = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Mayor());

        assertBadGameSettings();

        SetupHidden setupHidden = LookupUtil.findSetupHidden(narrator, "Mayor");
        SetupHiddenService.deleteSetupHidden(narrator, setupHidden);

        FactionRole m = BasicRoles.Mayor();
        Hidden hidden = HiddenService.createHidden(narrator, "test", m);
        addSetupHidden(hidden);

        assertBadGameSettings();

        removeSetupHidden(hidden);
        narrator.removePlayer(toRemove.getPlayer(narrator));
        for(int i = 0; i < 50; i++)
            addPlayer(Hidden.TownRandom());

        dayStart();

        int mayors = 0;
        for(Controller p: narrator.getAllPlayers())
            if(p.is(Mayor.class))
                mayors++;
        assertEquals(1, mayors);
    }

    public void testBlackmailedReveal() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller blackmailer = addPlayer(BasicRoles.Blackmailer());
        addPlayer(BasicRoles.ElectroManiac());

        nightStart();

        setTarget(blackmailer, mayor);

        endNight();

        partialExcludes(mayor.getCommands(), Mayor.COMMAND);

        try{
            reveal(mayor);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testInnocentChild() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifier.MAYOR_VOTE_POWER, 0);

        vote(mayor, cit);
        reveal(mayor);

        assertIsDay();

        unvote(mayor);

        vote(mayor, cit);
        assertIsDay();

        assertVoteTarget(cit, mayor);
        assertEquals(1, mayor.getPlayer(narrator).getVotePower());
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Mayor(), AbilityModifier.BACK_TO_BACK, Mayor.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Mayor(), AbilityModifier.ZERO_WEIGHTED, Mayor.class, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
