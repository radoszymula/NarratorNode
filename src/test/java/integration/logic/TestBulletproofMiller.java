package integration.logic;

import java.util.List;

import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Bulletproof;
import game.roles.Miller;
import services.FactionRoleService;

public class TestBulletproofMiller extends SuperTest {

    public TestBulletproofMiller(String s) {
        super(s);
    }

    public void testBulletProofBasic() {
        Controller bp = addPlayer(BasicRoles.Bulletproof());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller gf = addPlayer(BasicRoles.Godfather());

        setTarget(sk, bp);
        mafKill(gf, bp);

        endNight();

        isAlive(bp, sk, gf);
    }

    public void testBpElectric() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller bp = addPlayer(BasicRoles.Bulletproof());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        electrify(em, bp, sheriff);

        setTarget(doc, sheriff);
        setTarget(sheriff, bp);

        endNight();

        isAlive(bp, sheriff, doc, em);
    }

    public void testBPAssassination() {
        Controller bp = addPlayer(BasicRoles.Bulletproof());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.SerialKiller());

        dayStart();

        doDayAction(assassin, bp);

        isDead(bp);
    }

    public void testMillerSheriffCheck() {
        Controller miller = addPlayer(BasicRoles.Miller());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller agent = addPlayer(BasicRoles.Agent());

        editRule(SetupModifier.CHECK_DIFFERENTIATION, true);

        setTarget(sheriff, miller);

        assertEquals(agent.getColor(), miller.getPlayer(narrator).getFrameStatus().getColor());

        endNight();

        partialContains(sheriff, agent.getPlayer(narrator).getFaction().getName());
        // the bullet points are coming up as evil to sheriffs (indicating miller), and
        // what they will turn up as to sheriff checks
        List<String> publicDescription = miller.getPlayer(narrator).getAbility(Miller.class)
                .getPublicDescription(narrator, null, null, null);
        assertEquals(2, publicDescription.size());
    }

    public void testDeathSuited() {
        Controller miller = addPlayer(BasicRoles.Miller());
        Controller goon = addPlayer(BasicRoles.Goon());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.MILLER_SUITED, true);

        voteOut(miller, goon, cit1, cit2);

        assertSuited(miller, goon);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Miller(), AbilityModifier.BACK_TO_BACK, Miller.class, false);
            fail();
        }catch(NarratorException e){
        }

        try{
            FactionRoleService.addModifier(BasicRoles.Bulletproof(), AbilityModifier.BACK_TO_BACK, Bulletproof.class,
                    false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Miller(), AbilityModifier.ZERO_WEIGHTED, Miller.class, false);
            fail();
        }catch(NarratorException e){
        }

        try{
            FactionRoleService.addModifier(BasicRoles.Bulletproof(), AbilityModifier.ZERO_WEIGHTED, Bulletproof.class,
                    false);
            fail();
        }catch(NarratorException e){
        }
    }
}
