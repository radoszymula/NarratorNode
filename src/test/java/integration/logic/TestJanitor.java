package integration.logic;

import game.ai.Controller;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Doctor;
import game.roles.Janitor;
import util.TestUtil;

public class TestJanitor extends SuperTest {

    public TestJanitor(String name) {
        super(name);
    }

    public void testJanitor() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller jan = addPlayer(BasicRoles.Janitor());
        Controller jan2 = addPlayer(BasicRoles.Janitor());
        Controller maf = addPlayer(BasicRoles.Goon());

        mafKill(maf, cit2);
        setTarget(jan2, cit2);
        setTarget(jan, cit);
        endNight();

        assertStatus(cit, Janitor.MAIN_ABILITY);
        assertStatus(cit2, Janitor.MAIN_ABILITY);
        assertEquals(Constants.HIDDEN_KILL_FLAG, cit2.getPlayer(narrator).getDeathType().getList().get(0));

        skipDay();
        endNight();

        assertStatus(cit, Janitor.MAIN_ABILITY, false);
        assertStatus(cit2, Janitor.MAIN_ABILITY);
    }

    public void testJanGetRole() {
        addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller jan = addPlayer(BasicRoles.Janitor());

        editRule(SetupModifier.JANITOR_GETS_ROLES, true);

        setTarget(jan, doc);
        setTarget(sk, doc);
        endNight();

        seen(jan, Doctor.class);
    }

    public void testJanNotSeen() {
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller janitor = addPlayer(BasicRoles.Janitor());
        Controller janitor2 = addPlayer(BasicRoles.Janitor());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifier.JANITOR_GETS_ROLES, true);

        setTarget(janitor, doctor);
        setTarget(janitor2, doctor);
        endNight();

        notSeen(janitor, Doctor.class);

        voteOut(doctor, janitor, citizen, serialKiller);

        seen(janitor, Doctor.class);
        seen(janitor2, Doctor.class);
    }

    public void testOnSuccessCDay() {
        TestUtil.modifyAbility(BasicRoles.Janitor().role, Janitor.class, AbilityModifier.COOLDOWN, 1);

        Controller janitor = addPlayer(BasicRoles.Janitor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        setTarget(janitor, cit);
        endNight();

        assertCooldownRemaining(0, janitor);

        skipDay();

        assertCooldownRemaining(0, janitor);

        setTarget(janitor, cit);
        endNight();

        assertCooldownRemaining(0, janitor);

        voteOut(cit, janitor, cit2);

        assertCooldownRemaining(1, janitor);
    }

    private static void seen(Controller jan, Class<? extends Ability> c) {
        TestInvestigator.seen(jan, c);
    }

    private static void notSeen(Controller jan, Class<? extends Ability> c) {
        TestInvestigator.notSeen(jan, c);
    }
}
