package integration.logic;

import game.ai.Controller;
import game.logic.Faction;
import game.logic.Role;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Assassin;
import game.setups.Setup;
import services.FactionService;
import util.LookupUtil;

public class TestParity extends SuperTest {

    public TestParity(String name) {
        super(name);
    }

    public void testBasic() {
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller maf2 = addPlayer(BasicRoles.Goon());
        Controller dead = addPlayer(BasicRoles.Citizen());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        mafKill(maf, dead);
        endNight();

        assertIsNight();

        mafKill(maf, cit1);
        endNight();

        assertIsDay();
        skipDay();

        mafKill(maf2, cit2);
        endNight();

        assertGameOver();
    }

    public void testJailorExecutionStops() {
        addPlayer(BasicRoles.Jailor());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon(), 2);

        dayStart();
        assertIsDay();
    }

    public void testJailorNoExecutions() {
        modifyRole(BasicRoles.Jailor(), AbilityModifier.CHARGES, 1);

        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller dead = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon(), 2);

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        jail(jailor, dead);
        skipDay();

        setTarget(jailor, dead);
        endNight();

        assertIsNight();
    }

    public void testPoisoned() {
        Controller esc = addPlayer(BasicRoles.Escort());
        Controller poisoner = addPlayer(BasicRoles.Poisoner());
        Controller g1 = addPlayer(BasicRoles.Goon());
        Controller g2 = addPlayer(BasicRoles.Goon());

        setTarget(poisoner, g1);
        endNight();

        assertIsNight();
        assertEquals(3, narrator.getLiveSize());

        mafKill(g2, esc);
        setTarget(esc, g2);
        endNight();

        assertIsDay();
    }

    public void testGreaterParity() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon(), 2);

        dayStart();
        assertIsDay();
    }

    public void testArsonWithoutDouses() {
        addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon(), 2);

        editRule(SetupModifier.ARSON_DAY_IGNITES, 0);

        dayStart();
        assertIsNight();
    }

    public void testArsonWithDouseseDayStart() {
        addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon(), 2);

        editRule(SetupModifier.ARSON_DAY_IGNITES, 1);
        dayStart();

        assertIsNight();
    }

    public void testArsonWithDouseseNightStart() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon(), 2);
        Controller ars = addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifier.ARSON_DAY_IGNITES, 1);

        nightStart();
        endNight();

        assertIsDay();
        burn(ars);

        assertIsNight();
    }

    public void testInvulnerable() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Goon(), 2);

        dayStart();
        assertIsNight();
    }

    public void testGunShootIntoParity() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller goon = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon(), 2);

        editRule(SetupModifier.GS_DAY_GUNS, true);

        setTarget(gs, cit);
        endNight();

        shoot(cit, goon);
        assertIsNight();
    }

    public void testGunShootOutOfParity() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon(), 2);

        editRule(SetupModifier.GS_DAY_GUNS, true);

        setTarget(gs, cit);
        endNight();

        assertIsDay();
    }

    public void testOppositeAssassin() {
        addPlayer(BasicRoles.getMember(Setup.YAKUZA_C, new Assassin()));
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon(), 2);

        dayStart();

        assertIsDay();
    }

    public void testAssassinIntoParity() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Citizen(), 2);
        addPlayer(BasicRoles.Goon());

        dayStart();
        doDayAction(assassin, Assassin.COMMAND, cit);
        isDead(cit);

        assertIsNight();
    }

    public void testMayorNotRevealed() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifier.MAYOR_VOTE_POWER, 1);

        dayStart();
        assertIsDay();

        reveal(mayor);
        skipDay();

        setTarget(bm, mayor);
        endNight();

        assertIsNight();
    }

    public void testBmedUnrevealedMayor() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifier.MAYOR_VOTE_POWER, 0);
        nightStart();

        setTarget(bm, mayor);
        endNight();

        assertIsNight();
    }

    public void testRevealedMayorParity() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon(), 3);

        editRule(SetupModifier.MAYOR_VOTE_POWER, 1);

        dayStart();
        assertIsDay();

        reveal(mayor);
        assertIsNight();
    }

    public void testMafiaJailor() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        Controller kidnapper = addPlayer(BasicRoles.Kidnapper());
        addPlayer(BasicRoles.Goon());

        dayStart();
        jail(kidnapper, cit);
        skipVote(kidnapper);

        assertIsNight();
    }

    public void testMafiaJailorDoubleTrouble() {
        addPlayer(BasicRoles.Citizen(), 2);
        Controller kid1 = addPlayer(BasicRoles.Kidnapper());
        Controller kid2 = addPlayer(BasicRoles.Kidnapper());

        dayStart();

        assertIsDay();

        skipVote(kid1);
        assertIsDay();

        skipVote(kid2);

        assertIsNight();
    }

    public void testPuppetedParityTeam() {
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller goon = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        mafKill(goon, ghost);
        endNight();

        assertIsNight();

        setTarget(ghost, goon);
        endNight();

        assertIsDay();
    }

    public void testVentControlsVotes() {
        Role role = LookupUtil.findRole(narrator, "Ventriloquist");
        Faction faction = narrator.getFaction(Setup.MAFIA_C);
        Controller vent = addPlayer(FactionService.createFactionRole(faction, role));
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);

        setTarget(vent, cit);
        endNight();

        assertIsNight();
    }

    // TOOD
    public void testCultNoKill() {
        addPlayer(BasicRoles.Cultist(), 2);
        addPlayer(BasicRoles.Citizen(), 2);

        dayStart();
        assertIsDay();
    }
}
