package integration.logic;

import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Coward;
import game.roles.FactionKill;

public class TestCoward extends SuperTest {

    public TestCoward(String s) {
        super(s);
    }

    // tests if sheriff is valid
    public void testSheriffCheck() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller coward = addPlayer(BasicRoles.Coward());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Witch());

        setTarget(sk, coward);
        setTarget(coward, cit);

        endNight();

        isDead(cit);
        isAlive(coward);
    }

    public void testBadTarget() {
        addPlayer(BasicRoles.Citizen(), 2);
        Controller coward = addPlayer(BasicRoles.Coward());

        try{
            setTarget(coward, coward);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testLookoutSeeing() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller coward = addPlayer(BasicRoles.Coward());

        setTarget(lookout, cit);
        setTarget(coward, cit);

        endNight();

        TestLookout.seen(lookout, coward);
    }

    public void testCowardAndVeteran() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller coward = addPlayer(BasicRoles.Coward());
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());

        setTarget(coward, vet);
        setTarget(vet);
        setTarget(sheriff, coward);
        endNight();

        isDead(coward, sheriff);
    }

    public void testCharges() {
        modifyRole(BasicRoles.Coward(), AbilityModifier.CHARGES, 1);

        Controller detective = addPlayer(BasicRoles.Detective());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller coward = addPlayer(BasicRoles.Coward());

        removeTeamAbility(BasicRoles.Coward().getColor(), FactionKill.class);
        assertFalse(narrator.getFaction(BasicRoles.Coward().getColor()).hasSharedAbilities());

        assertEquals(1, BasicRoles.Coward().role._abilities.getAbility(Coward.class).modifiers
                .getInt(AbilityModifier.CHARGES));
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        nightStart();

        assertPerceivedChargeRemaining(1, coward, Coward.MAIN_ABILITY);

        setTarget(coward, vet);

        nextNight();

        try{
            setTarget(coward, vet);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(detective, coward);
        witch(witch, coward, sheriff);

        endNight();

        TestDetective.seen(detective, sheriff);
    }
}
