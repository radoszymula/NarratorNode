package integration.logic;

import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Marshall;
import services.FactionRoleService;

public class TestMarshall extends SuperTest {

    public TestMarshall(String name) {
        super(name);
    }

    Controller marshall;

    @Override
    public void roleInit() {
        marshall = addPlayer(BasicRoles.Marshall());
    }

    private void order() {
        order(marshall);
    }

    private void setExtraLynches(int i) {
        editRule(SetupModifier.MARSHALL_EXECUTIONS, i);
    }

    public void testBasics() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller agent = addPlayer(BasicRoles.Agent());

        order();

        try{
            order();
            fail();
        }catch(NarratorException e){
        }

        voteOut(c1, marshall, c2, agent);

        try{
            vote(c1, c2);
            fail();
        }catch(NarratorException e){
        }

        assertIsDay();
        isDead(c1);
        assertVoteTarget(null, marshall);

        voteOut(c2, marshall, agent);

        assertIsNight();
        isDead(c2);
    }

    public void testFewerLynchesThanLiving() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller agent = addPlayer(BasicRoles.Agent());

        setExtraLynches(1);

        order();

        voteOut(c1, c2, c3, agent);
        voteOut(c2, c3, agent, marshall);

        assertIsNight();
    }

    public void testParity() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller maf1 = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());

        setExtraLynches(3);

        order();

        voteOut(cit1, cit2, maf1, marshall);
        assertIsNight();
    }

    public void testEndGame() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller maf1 = addPlayer(BasicRoles.Goon());

        setExtraLynches(3);

        order();

        voteOut(maf1, cit1, marshall);

        assertGameOver();
        try{
            vote(cit1, marshall);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testOrderingTodayTomorrow() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        modifyAbilityCharges(BasicRoles.Marshall(), 2);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        dayStart();
        assertRealChargeRemaining(2, marshall, Marshall.MAIN_ABILITY);

        order();
        assertRealChargeRemaining(1, marshall, Marshall.MAIN_ABILITY);

        skipDay();
        endNight();

        order();
        assertRealChargeRemaining(0, marshall, Marshall.MAIN_ABILITY);

        skipDay();
        endNight();

        try{
            order();
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Marshall(), AbilityModifier.BACK_TO_BACK, Marshall.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Marshall(), AbilityModifier.ZERO_WEIGHTED, Marshall.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    // visual checks
    // no graveyard showing up, no announcement
    // no nonpositive numbers when manipulating marshall lynches

}
