package integration.logic;

import java.util.ArrayList;

import integration.unit.TestCommandModel;
import integration.unit.TestCondorcet;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class LogicTests {

    public static ArrayList<Class<? extends TestCase>> NarratorTests() {
        ArrayList<Class<? extends TestCase>> tests = new ArrayList<>();

        tests.add(TestSheriff.class);
        tests.add(TestInvestigator.class);
        tests.add(TestDetective.class);
        tests.add(TestLookout.class);
        tests.add(TestSpy.class);
        tests.add(TestCoroner.class);
        tests.add(TestArmsDetector.class);
        tests.add(TestParityCop.class);

        tests.add(TestDoctor.class);
        tests.add(TestBlocker.class);
        tests.add(TestBodyguard.class);
        tests.add(TestArmorsmith.class);

        tests.add(TestDriver.class);
        tests.add(TestOperator.class);
        tests.add(TestCoward.class);

        tests.add(TestVeteran.class);
        tests.add(TestJailor.class);
        tests.add(TestGunsmith.class);
        tests.add(TestVigilante.class);

        tests.add(TestSnitch.class);
        tests.add(TestMayor.class);
        tests.add(TestMarshall.class);
        tests.add(TestMason.class);
        tests.add(TestArchitect.class);
        tests.add(TestBaker.class);
        tests.add(TestBlacksmith.class);
        tests.add(TestCommuter.class);

        tests.add(TestPunch.class);
        tests.add(TestBomb.class);
        tests.add(TestBulletproofMiller.class);
        tests.add(TestSleepwalker.class);

        tests.add(TestFactionSend.class);
        tests.add(TestMafiaTeam.class);
        tests.add(TestTeamTakedown.class);
        tests.add(TestProxyKill.class);

        tests.add(TestBlackmailer.class);
        tests.add(TestSilencer.class);
        tests.add(TestDisfranchise.class);
        tests.add(TestJanitor.class);
        tests.add(TestGodfather.class);
        tests.add(TestFramer.class);
        tests.add(TestDisguiser.class);
        tests.add(TestAssassin.class);
        tests.add(TestDrugDealer.class);
        tests.add(TestTailor.class);
        tests.add(TestInfiltrator.class);

        tests.add(TestThief.class);
        tests.add(TestGhost.class);
        tests.add(TestJester.class);
        tests.add(TestExecutioner.class);
        tests.add(TestAmnesiac.class);
        tests.add(TestSurvivor.class);
        tests.add(TestAutoVest.class);

        tests.add(TestWitch.class);
        tests.add(TestGraveDigger.class);
        tests.add(TestVentriloquist.class);
        tests.add(TestElector.class);

        tests.add(TestCult.class);
        tests.add(TestScout.class);

        tests.add(TestMassMurderer.class);
        tests.add(TestInterceptor.class);
        tests.add(TestArsonist.class);
        tests.add(TestSerialKiller.class);
        tests.add(TestElectromaniac.class);
        tests.add(TestPoisoner.class);
        tests.add(TestJoker.class);

        tests.add(TestTalking.class);
        tests.add(TestLastWill.class);
        tests.add(TestRolesList.class);
        tests.add(TestEvent.class);
        tests.add(TestPluralityVoteSystem.class);
        tests.add(TestDiminishingPoolVoteSystem.class);
        tests.add(TestStructure.class);
        tests.add(TestGameSettings.class);
        tests.add(TestPrefer.class);
        tests.add(TestParity.class);
        tests.add(TeamTests.class);
        tests.add(TestHidden.class);
        tests.add(TestSetup.class);

        tests.add(TestBrain.class);
        tests.add(TestCommandModel.class);
        tests.add(TestCondorcet.class);

        tests.add(TestMashRole.class);
        tests.add(TestModifiers.class);

        return tests;
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(LogicTests.class.getName());

        for(Class<? extends TestCase> testCase: NarratorTests())
            suite.addTestSuite(testCase);

        return suite;
    }

}
