package integration.logic;

import game.ai.Controller;
import game.event.DeathAnnouncement;
import game.event.Message;
import game.logic.Faction;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Assassin;
import game.roles.FactionKill;
import game.setups.Setup;
import services.FactionRoleService;
import services.FactionService;
import util.LookupUtil;
import util.TestUtil;

public class TestAssassin extends SuperTest {

    public TestAssassin(String s) {
        super(s);
    }

    public void testBasicAbility() {
        addPlayer(BasicRoles.Citizen());
        Controller voteFodder = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller ass = addPlayer(BasicRoles.Assassin());

        dayStart();

        assertTrue(ass.getPlayer(narrator).hasDayAction(Assassin.COMMAND));

        try{
            doDayAction(ass, ass);
            fail();
        }catch(PlayerTargetingException e){
        }

        doDayAction(ass, c3);

        isDead(c3);

        vote(voteFodder, ass);
        assertTrue(narrator.getCommands().get(5).text.toLowerCase().contains(Assassin.COMMAND.toLowerCase()));
    }

    public void testAssassinNightKillUnselect() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Lookout());
        Controller assass = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Assassin());

        mafKill(assass, c1);

        cancelAction(assass, 1);

        assertFalse(assass.getPlayer(narrator).getActions().isTargeting(assass.getPlayer(narrator), Faction.SEND_));

        // used to throw index out of bounds exception
        Action oldA = assass.getPlayer(narrator).getAction(FactionKill.MAIN_ABILITY);
        Action newA = new Action(assass.getPlayer(narrator), FactionKill.MAIN_ABILITY, c2.getPlayer(narrator));
        assass.getPlayer(narrator).getActions().replace(oldA, newA);
    }

    public void testReverseParse() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller assass = addPlayer(BasicRoles.Assassin());

        mafKill(assass, c1);
        assertActionSize(1, assass);

        endNight();

        isDead(c1);
    }

    public void testCommand() {
        Controller c3 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);
        Controller ass = addPlayer(BasicRoles.Assassin());

        dayStart();

        command(ass, Assassin.COMMAND + " " + c3.getName());

        isDead(c3);
    }

    public void testEndDay() {
        addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller ass = addPlayer(BasicRoles.Assassin());

        dayStart();

        doDayAction(ass, c3);

        isDead(c3);

        isWinner(ass);
    }

    public void testGameEnd() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Goon(), 5);

        dayStart();
        doDayAction(assassin, cit);

        assertGameOver();
    }

    public void testAssassinTargeting() {
        Controller goon = addPlayer(BasicRoles.Goon());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Citizen());

        try{
            doDayAction(assassin, goon);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testEndGameNightlessAssassin() {
        addPlayer(BasicRoles.Citizen());
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Assassin());

        editRule(SetupModifier.NIGHT_LENGTH, 0);
        removeTeamAbility(Setup.MAFIA_C, FactionKill.class);
        assertFalse(narrator.getFaction(Setup.MAFIA_C).hasSharedAbilities());

        doDayAction(p3, p1);

        assertGameOver();
    }

    public void testAssassinTargetingEnemyHasAssassin() {
        Role assassinRole = LookupUtil.findRole(narrator, "Assassin");
        Faction town = narrator.getFaction(Setup.TOWN_C);
        Controller goon = addPlayer(BasicRoles.Goon());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller sin2 = addPlayer(FactionService.createFactionRole(town, assassinRole));
        Controller cit = addPlayer(BasicRoles.Citizen());

        doDayAction(assassin, goon);
        doDayAction(sin2, cit);
    }

    public void testSecretAssassinKill() {
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);
        secretKill();

        doDayAction(assassin, cit);

        for(Message m: narrator.getEventManager().getEvents(Message.PUBLIC)){
            if(m instanceof DeathAnnouncement){
                assertFalse(m.access(Message.PUBLIC).contains(assassin.getName()));
                return;
            }
        }
        throw new NarratorException();
    }

    public void testNoZeroWeightkModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Assassin(), AbilityModifier.ZERO_WEIGHTED, Assassin.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    private void secretKill() {
        TestUtil.modifyAbility(Assassin.class, AbilityModifier.SECRET_KILL, true);
    }
}
