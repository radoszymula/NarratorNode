package integration.logic;

import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.support.rules.AbilityModifier;
import game.logic.templates.BasicRoles;
import game.roles.Bomb;
import services.FactionRoleService;

public class TestBomb extends SuperTest {

    public TestBomb(String name) {
        super(name);
    }

    public void testBasic() {
        addPlayer(BasicRoles.Citizen());
        Controller bomb = addPlayer(BasicRoles.Bomb());
        Controller maf = addPlayer(BasicRoles.Goon());

        mafKill(maf, bomb);

        endNight();

        assertGameOver();
        isDead(bomb, maf);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Bomb(), AbilityModifier.BACK_TO_BACK, Bomb.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Bomb(), AbilityModifier.ZERO_WEIGHTED, Bomb.class, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
