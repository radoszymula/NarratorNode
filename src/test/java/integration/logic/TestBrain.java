package integration.logic;

import java.util.ArrayList;
import java.util.List;

import game.ai.Brain;
import game.ai.Claim;
import game.ai.Computer;
import game.ai.Controller;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.support.Random;
import game.logic.templates.BasicRoles;
import game.roles.Hidden;
import game.setups.MediumH;
import game.setups.Setup;
import models.Command;
import models.FactionRole;
import services.HiddenService;
import util.Assertions;
import util.RepeatabilityTest;

public class TestBrain extends SuperTest {

    public TestBrain(String name) {
        super(name);
    }

    public void testBrain() {
        SuperTest.printHappenings = false;
        SuperTest.checkEnclosingChats = false;
        Long prevSeed = null;
        for(int j = 0; j < 3; j++){

            newNarrator();

            long t = new Random().nextLong();
//             t = Long.parseLong("-7997947417764629821");

            if(prevSeed != null && t == prevSeed)
                break;
            prevSeed = t;

            System.out.println("Brain" + (j + 1) + ": " + t);
            narrator.setSeed(t);

            addPlayer(Hidden.TownRandom(), 50);
            addPlayer(Hidden.MafiaRandom(), 21);
            addPlayer(Hidden.YakuzaRandom(), 21);
            addPlayer(Hidden.NeutralRandom(), 8);

            dayStart();

            try{
                simulationTest(t);
            }catch(Exception e){
                writeToFile("log1.tx", narrator);
                e.printStackTrace();
                fail(t);
            }

            RepeatabilityTest.runRepeatabilityTest();
        }
        skipTearDown = true;
    }

    public void testSafeClaim() {
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller m3 = addPlayer(BasicRoles.Goon());
        Controller m2 = addPlayer(BasicRoles.Goon());
        Controller m1 = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        dayStart();

        Brain b = new Brain(narrator, narrator.getRandom());
        assertEquals(7, b.publicRoles.size());

        ArrayList<String> safeColors = b.getSafeColors(narrator);
        assertEquals(1, safeColors.size());
        assertEquals(c2.getColor(), safeColors.get(0));

        voteOut(c2, c1, m1, m2, m3);
        assertEquals(6, b.publicRoles.size());
        mafKill(m1, c1);
        endNight();

        isDead(c1);
        safeColors = b.getSafeColors(narrator);
        assertEquals(1, safeColors.size());
        assertEquals(5, b.publicRoles.size());
        assertEquals(m1.getColor(), safeColors.get(0));
    }

    public void roleClaim() {

        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        Brain b = new Brain(narrator, narrator.getRandom());
        Claim c = b.computers.get(c2).roleClaim();
        assertTrue(c.believable(b.computers.get(c1)));
    }

    public void testAvailable() {
        FactionRole a = BasicRoles.Citizen();
        FactionRole b = BasicRoles.Doctor();
        FactionRole c = BasicRoles.Sheriff();
        FactionRole d = BasicRoles.Sheriff();
        Hidden r1 = HiddenService.createHidden(narrator, "ABCD", a, b, c, d);
        Hidden r2 = HiddenService.createHidden(narrator, "ABC", a, b, c);
        Hidden r3 = HiddenService.createHidden(narrator, "AB", a, b);

        addPlayer(r1);
        addPlayer(r2);
        addPlayer(r3);
        addPlayer(BasicRoles.Goon());

        dayStart();

        Brain brain = new Brain(narrator, new Random());
        assertEquals(3, brain.count(a));
    }

    public void testCrossOff() {
        FactionRole a = BasicRoles.Citizen();
        FactionRole b = BasicRoles.Doctor();
        FactionRole c = BasicRoles.Sheriff();
        FactionRole d = BasicRoles.Sheriff();
        Hidden r1 = HiddenService.createHidden(narrator, "ABCD", a, b, c, d);
        Hidden r2 = HiddenService.createHidden(narrator, "ABC", a, b, c);
        Hidden r3 = HiddenService.createHidden(narrator, "AB", a, b);

        addPlayer(r1);
        addPlayer(r2);
        addPlayer(r3);
        addPlayer(BasicRoles.Goon());

        dayStart();

        Brain brain = new Brain(narrator, new Random());
        brain.publicRoles.crossOff(a);
        assertEquals(3, brain.publicRoles.size());
    }

    public void testCompetingClaims() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Doctor());
        Controller p4 = addPlayer(BasicRoles.Goon());

        dayStart();
        Brain brain = new Brain(narrator, new Random());

        Computer c1 = brain.computers.get(p1);
        Computer c2 = brain.computers.get(p2);
        Computer c3 = brain.computers.get(p3);
        Computer c4 = brain.computers.get(p4);

        c4.roleClaim();
        c1.roleClaim();
        c2.roleClaim();
        c3.roleClaim();

        assertEquals(4, brain.getClaims().size());
    }

    public void testBrainMassBlackmailer() {
        for(int j = 0; j < 2; j++){
            newNarrator();
            long seed = new Random().nextLong();
            // seed = Long.parseLong("-6474779550190453190");
            System.out.println("Mass Blackmailer: " + seed);
            narrator.setSeed(seed);

            addPlayer(BasicRoles.Detective(), 5);
            addPlayer(BasicRoles.Blackmailer(), 16);

            dayStart();

            simulationTest(seed);
            RepeatabilityTest.runRepeatabilityTest();
        }
        skipTearDown = true;
    }

    public void testRepeatability() {
        String key = MediumH.KEY;
        Long gameSeed = 508764355394196991l;
        Long brainSeed = 9082865660368775388l;
        int playerCount = 16;

        Setup s1 = Setup.GetSetup(new Narrator(), key);
        Narrator n1 = s1.narrator;
        for(int i = 0; i < playerCount; i++)
            n1.addPlayer(Computer.toLetter(i + 1));

        Setup s2 = Setup.GetSetup(new Narrator(), key);
        Narrator n2 = s2.narrator;
        for(int i = 0; i < playerCount; i++)
            n2.addPlayer(Computer.toLetter(i + 1));

        s1.applyRolesList();
        s2.applyRolesList();

        n1.setSeed(gameSeed);
        n2.setSeed(gameSeed);
        
        n1.startGame();
        n2.startGame();

        Assertions.assertSetupHiddensAreSame(n1, n2);
        
        Controller p2;
        for(Controller p1: n1._players){
            p2 = n2.getPlayerByID(((Player) p1).getID());
            assertEquals(p1.getColor(), p2.getColor());
            assertEquals(p1.getRoleName(), p2.getRoleName());
        }
        
        assertEquals(n1.getRandom().log, n2.getRandom().log);

        Brain.EndGame(n1, brainSeed);
        Brain.EndGame(n2, brainSeed);

        assertEquals(n1.getRandom().log, n2.getRandom().log);
        List<Command> commands1 = n1.getCommands();
        List<Command> commands2 = n2.getCommands();
        assertEquals(commands1.size(), commands2.size());

        skipTearDown = true;
    }

    public void testRandom() {
        Random r = new Random();
        long l = r.nextLong();
        r.setSeed(l);

        r.reset();
        l = r.nextLong();

        r.reset();
        assertEquals(l, r.nextLong());

        r.reset();
        assertEquals(l, r.nextLong());

    }
}
