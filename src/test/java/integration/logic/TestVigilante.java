package integration.logic;

import game.logic.exceptions.NarratorException;
import game.logic.support.rules.AbilityModifier;
import game.logic.templates.BasicRoles;
import game.roles.Vigilante;
import services.FactionRoleService;

public class TestVigilante extends SuperTest {
    public TestVigilante(String s) {
        super(s);
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Vigilante(), AbilityModifier.ZERO_WEIGHTED, Vigilante.class,
                    false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Vigilante(), AbilityModifier.BACK_TO_BACK, Vigilante.class,
                    false);
            fail();
        }catch(NarratorException e){
        }
    }
}
