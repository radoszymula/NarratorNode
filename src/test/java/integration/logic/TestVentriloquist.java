package integration.logic;

import game.ai.Controller;
import game.event.DayChat;
import game.event.DeadChat;
import game.logic.exceptions.ChatException;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.VotingException;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Assassin;
import game.roles.Burn;
import game.roles.GraveDigger;
import game.roles.GunAbility;
import game.roles.Jailor;
import game.roles.Mayor;
import game.roles.PuppetVote;
import game.roles.Ventriloquist;

public class TestVentriloquist extends SuperTest {

    public TestVentriloquist(String name) {
        super(name);
    }

    public void testBasicVent() {
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller puppet = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());

        dayStart();
        partialExcludes(vent.getCommands(), PuppetVote.COMMAND);
        skipDay();

        setTarget(vent, puppet);
        endNight();

        hasPuppets(vent);
        partialContains(vent.getCommands(), PuppetVote.COMMAND);

        partialContains(puppet, Ventriloquist.FEEDBACK);

        assertNull(narrator.getEventManager().getDayChat().getKey(puppet.getPlayer(narrator)));

        try{
            say(puppet, "I cannot say this", DayChat.KEY);
            fail();
        }catch(ChatException e){
        }

        try{
            vote(puppet, vent);
            fail();
        }catch(VotingException e){
        }

        try{
            skipVote(puppet);
            fail();
        }catch(VotingException e){
        }

        try{
            vent.doDayAction(PuppetVote.COMMAND, null, cit);
            fail();
        }catch(VotingException | PlayerTargetingException | IllegalActionException e){
        }

        isPuppeted(cit, false);
        try{
            ventVote(cit, vent, cit);
            fail();
        }catch(VotingException | IllegalActionException | PlayerTargetingException e){
        }

        try{
            ventVote(cit, cit, puppet);
            fail();
        }catch(VotingException | IllegalActionException | PlayerTargetingException e){
        }

        ventVote(puppet, cit, vent);

        vote(vent, cit);

        endNight();
        assertGameOver();
    }

    public void testSkipUnvoting() {
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller puppet = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());

        dayStart();
        partialExcludes(vent.getCommands(), PuppetVote.COMMAND);
        skipDay();

        setTarget(vent, puppet);
        endNight();

        ventVote(puppet, cit, vent);
        try{
            skipVote(puppet);
            fail();
        }catch(VotingException e){
        }

        ventSkipVote(puppet, vent);

        assertVoteTarget(narrator.skipper, puppet);

        try{
            unvote(puppet);
            fail();
        }catch(VotingException e){
        }
        ventUnvote(puppet, vent);

        assertVoteTarget(null, puppet);
    }

    public void testDayActionStopping() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller ars = addPlayer(BasicRoles.Arsonist());
        Controller assassin = addPlayer(BasicRoles.Assassin());

        editRule(SetupModifier.GS_DAY_GUNS, true);

        setTarget(gs, mayor);
        setTarget(ars, mayor);
        setTarget(vent, mayor);

        endNight();

        partialExcludes(mayor.getCommands(), Mayor.COMMAND);
        partialExcludes(mayor.getCommands(), GunAbility.COMMAND);
        partialContains(ars.getCommands(), Burn.COMMAND);

        try{
            shoot(mayor, vent);
            fail();
        }catch(IllegalActionException | PlayerTargetingException e){
        }

        try{
            reveal(mayor);
            fail();
        }catch(IllegalActionException | PlayerTargetingException e){
        }

        skipDay();

        setTarget(vent, assassin);
        endNight();

        reveal(mayor);
        assertVotePower(narrator.getInt(SetupModifier.MAYOR_VOTE_POWER) + 1, mayor);
        // gotta add puppet vote as an ability, not keep reinsantiating it.
        partialContains(vent.getCommands(), PuppetVote.COMMAND);

        try{
            doDayAction(assassin, vent);
            fail();
        }catch(IllegalActionException | PlayerTargetingException e){
        }
        isAlive(vent);

        skipDay();

        setTarget(vent, jailor);
        endNight();

        doDayAction(assassin, mayor);
        partialExcludes(assassin.getCommands(), Assassin.COMMAND);
        partialExcludes(jailor.getCommands(), Jailor.COMMAND);

        try{
            jail(jailor, vent);
            fail();
        }catch(NarratorException e){
        }
        skipDay();

        setTarget(ars, assassin);
        setTarget(vent, ars);

        endNight();

        assertNoAcceptableTargets(ars, Burn.COMMAND);
        partialIncludes(jailor.getCommands(), Jailor.COMMAND);
    }

    public void testDeadChatAccess() {
        Controller fodder2 = addPlayer(BasicRoles.Citizen());
        Controller fodder3 = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller fodder = addPlayer(BasicRoles.Witch());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller poisoner = addPlayer(BasicRoles.Poisoner());

        setTarget(vent, fodder);
        setTarget(sk, fodder);

        endNight();

        say(fodder, "Can anyone hear me?", DeadChat.KEY);
        skipDay();

        setTarget(vent, fodder2);
        endNight();

        try{
            say(fodder2, "Can I really not say anything?", DayChat.KEY);
            fail();
        }catch(ChatException e){
        }

        doDayAction(assassin, fodder2);
        say(fodder2, "Can anyone hear ME?", DeadChat.KEY);

        skipDay();

        setTarget(poisoner, fodder3);
        setTarget(vent, fodder3);
        endNight();

        skipDay();
        isDead(fodder3);
        say(fodder3, "Can anyone hear ME?", DeadChat.KEY);
    }

    public void testVentSameTarget() {
        Controller vent1 = addPlayer(BasicRoles.Ventriloquist());
        Controller vent2 = addPlayer(BasicRoles.Ventriloquist());
        Controller vent3 = addPlayer(BasicRoles.Ventriloquist());
        Controller fodder1 = addPlayer(BasicRoles.Citizen());
        Controller fodder3 = addPlayer(BasicRoles.Framer());
        Controller ass1 = addPlayer(BasicRoles.Assassin());
        Controller ass2 = addPlayer(BasicRoles.Assassin());

        setTarget(vent3, fodder3);
        setTarget(vent2, fodder1);
        setTarget(vent1, fodder1);
        endNight(vent1);
        endNight();

        isPuppeted(fodder1);
        hasPuppets(vent2, false);
        hasPuppets(vent1);
        assertEquals(vent1, fodder1.getPlayer(narrator).getPuppeteer());

        try{
            ventVote(vent1, fodder3, vent1);
            fail();
        }catch(VotingException e){
        }

        doDayAction(ass1, vent3);

        vote(fodder3, ass1);
        isPuppeted(vent3, false);

        doDayAction(ass2, vent1);
        isPuppeted(fodder1);
        hasPuppets(vent2);

        ventVote(fodder1, vent2, vent2);
    }

    public void testVentDyingAfterVentAction() {
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller fodder = addPlayer(BasicRoles.Framer());
        Controller mover = addPlayer(BasicRoles.Detective());
        addPlayer(BasicRoles.Amnesiac());

        electrify(electro, mover, vent);

        setTarget(vent, fodder);
        setTarget(mover, vent);

        endNight();

        isPuppeted(fodder, false);
    }

    public void testBreadedVent() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTarget(baker, vent);
        nextNight();

        setTarget(vent, baker);
        setTarget(vent, cit);
        endNight();

        ventVote(baker, vent, vent);
        ventVote(cit, vent, vent);
        vote(witch, vent);

        isDead(vent);
    }

    public void testBMedVentrilo() {
        Controller bmer = addPlayer(BasicRoles.Blackmailer());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller puppet = addPlayer(BasicRoles.Citizen());

        setTarget(bmer, vent);
        setTarget(vent, puppet);
        endNight();

        try{
            vote(vent, bmer);
            fail();
        }catch(VotingException e){
        }

        ventVote(puppet, bmer, vent);
        assertTrue(vent.getPlayer(narrator).getPuppets().contains(puppet.getPlayer(narrator)));
        try{
            say(vent, "I can't talk from my mouth", DayChat.KEY);
            fail();
        }catch(ChatException e){
        }
        assertEquals(1, vent.getChatKeys().size());
        assertTrue(vent.getChatKeys().contains(puppet.getName()));
        say(vent, gibberish(), puppet.getName());
        partialContains(gibberish);
    }

    public void testDisguisedVented() {
        Controller citizenA = addPlayer(BasicRoles.Citizen());
        Controller citizenB = addPlayer(BasicRoles.Citizen());
        Controller disguiserC = addPlayer(BasicRoles.Disguiser());
        Controller disguiserD = addPlayer(BasicRoles.Disguiser());
        Controller agentE = addPlayer(BasicRoles.Agent());
        Controller ventF = addPlayer(BasicRoles.Ventriloquist());
        Controller poisonerG = addPlayer(BasicRoles.Poisoner());

        setTarget(ventF, citizenA);
        setTarget(disguiserC, citizenA);
        endNight();

        isPuppeted(disguiserC);
        assertVotePower(1, disguiserC);

        ventVote(disguiserC, agentE, ventF);
        vote(ventF, agentE);
        vote(disguiserD, agentE);
        vote(citizenB, agentE);

        isDead(agentE);

        setTarget(ventF, citizenB);
        setTarget(disguiserD, citizenB);
        endNight();

        ventVote(disguiserD, poisonerG, ventF);
        vote(ventF, poisonerG);
        vote(disguiserC, poisonerG);
    }

    public void testDeadPuppet() {
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Architect());

        setTarget(vent, fodder);
        endNight();

        doDayAction(assassin, fodder);
        hasPuppets(vent, false);
    }

    public void testVentCooldown() {
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller pupp = addPlayer(BasicRoles.Poisoner());
        Controller pup2 = addPlayer(BasicRoles.Agent());
        Controller witch = addPlayer(BasicRoles.Witch());

        modifyRoleAbility(BasicRoles.Ventriloquist().role, AbilityModifier.BACK_TO_BACK, Ventriloquist.class, false);

        setTarget(vent, pupp);
        nextNight();

        assertFalse(
                pupp.getPlayer(narrator).in(vent.getPlayer(narrator).getAcceptableTargets(Ventriloquist.MAIN_ABILITY)));
        try{
            setTarget(vent, pupp);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(vent, pup2);
        nextNight();

        assertFalse(
                pup2.getPlayer(narrator).in(vent.getPlayer(narrator).getAcceptableTargets(Ventriloquist.MAIN_ABILITY)));
        try{
            setTarget(vent, pup2);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(vent, pupp);
        witch(witch, vent, pup2);
        endNight();

        assertNull(pup2.getPlayer(narrator).getPuppeteer());
        hasPuppets(vent, false);
    }

    public void testNoBackToBackFromWitch() {
        Controller ventriloquist = addPlayer(BasicRoles.Ventriloquist());
        Controller puppet = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        modifyRoleAbility(BasicRoles.Ventriloquist().role, AbilityModifier.BACK_TO_BACK, Ventriloquist.class, false);

        setTarget(witch, ventriloquist, puppet);
        nextNight();

        try{
            setTarget(ventriloquist, puppet);
            fail();
        }catch(NarratorException e){
        }

        setTarget(witch, ventriloquist, puppet);
        endNight();

        hasPuppets(ventriloquist, false);
    }

    public void testVentControllingVent() {
        Controller vent1 = addPlayer(BasicRoles.Ventriloquist());
        Controller vent2 = addPlayer(BasicRoles.Ventriloquist());
        Controller cit = addPlayer(BasicRoles.Citizen());

        setTarget(vent2, vent1);
        setTarget(vent1, cit);

        endNight();

        say(vent1, "You puppetted me!", cit.getName());
        ventVote(vent1, cit, vent2);

        say(vent2, "haha", vent1.getName());
        ventVote(vent1, vent2, vent2);
    }

    public void testVentCorpse() {
        Controller digger = addPlayer(BasicRoles.GraveDigger());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        voteOut(vent, digger, cit, lookout);

        setTarget(lookout, cit);
        setTarget(digger, GraveDigger.COMMAND, null, null, vent, cit);
        endNight();

        TestLookout.seen(lookout, vent);
        isPuppeted(cit, false);
        partialExcludes(cit, Ventriloquist.FEEDBACK);
    }
}
