package integration.logic;

import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Detective;
import game.roles.Sleepwalker;
import junit.framework.AssertionFailedError;
import models.FactionRole;
import services.FactionRoleService;
import util.TestUtil;

public class TestSleepwalker extends SuperTest {

    public TestSleepwalker(String s) {
        super(s);
    }

    public void testSleepwalkerBasic() {
        Controller sw = addPlayer(BasicRoles.Sleepwalker());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller cit = addPlayer(BasicRoles.Citizen());

        TestUtil.assignRoles(narrator, new Controller[] {
                sw, detect, witch, cit
        }, new FactionRole[] {
                BasicRoles.Sleepwalker(), BasicRoles.Detective(), BasicRoles.Witch(), BasicRoles.Citizen()
        });

        setTarget(detect, sw);
        endNight();

        assertTrue(sw.getPlayer(narrator).getActions().isEmpty());

        printHappenings = false;
        try{
            TestDetective.seen(detect, sw);
            return;
        }catch(AssertionFailedError e){
        }
        try{
            TestDetective.seen(detect, cit);
            return;
        }catch(AssertionFailedError e){
        }
        try{
            TestDetective.seen(detect, witch);
            return;
        }catch(AssertionFailedError e){
        }
        printHappenings = true;
        TestDetective.seen(detect, detect);

    }

    public void testElectroVest() {
        Controller sw = addPlayer(BasicRoles.Sleepwalker());
        Controller aSmith = addPlayer(BasicRoles.Armorsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller em = addPlayer(BasicRoles.ElectroManiac());
        Controller em2 = addPlayer(BasicRoles.ElectroManiac());
        Controller escort = addPlayer(BasicRoles.Escort());

        TestUtil.assignRoles(narrator, new Controller[] {
                sw, aSmith, cit, cult, em, em2, escort
        }, new FactionRole[] {
                BasicRoles.Sleepwalker(), BasicRoles.Armorsmith(), BasicRoles.Citizen(), BasicRoles.Cultist(),
                BasicRoles.ElectroManiac(), BasicRoles.ElectroManiac(), BasicRoles.Escort()
        });

        narrator.setSeed(1);// for the randome chance that

        setTarget(aSmith, sw);
        nextNight();

        electrify(em, aSmith, cit, cult);
        setTarget(em, sw);
        setTarget(em2, escort);
        setTarget(escort, sw);

        nextNight();

        isCharged(sw, aSmith, cit, cult, escort);
        vest(sw);

        endNight();

        assertEquals(1, narrator.getDeadSize());
    }

    public void testFramerRandomElement() {
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller sw = addPlayer(BasicRoles.Sleepwalker());
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller maf = addPlayer(BasicRoles.Goon());

        TestUtil.assignRoles(narrator, new Controller[] {
                detect, fodder, sw, framer, maf
        }, new FactionRole[] {
                BasicRoles.Detective(), BasicRoles.Citizen(), BasicRoles.Sleepwalker(), BasicRoles.Framer(),
                BasicRoles.Goon()
        });

        editRule(SetupModifier.FOLLOW_GETS_ALL, false);

        nightStart();
        mafKill(maf, fodder);
        frame(framer, sw, maf.getColor());
        setTarget(detect, sw);

        endNight();

        TestDetective.seen(detect, fodder);
    }

    public void testSwGunsmith() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller sw = addPlayer(BasicRoles.Sleepwalker());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller framer = addPlayer(BasicRoles.Framer());

        TestUtil.assignRoles(narrator, new Controller[] {
                gs, detect, sw, citizen, framer
        }, new FactionRole[] {
                BasicRoles.Gunsmith(), BasicRoles.Detective(), BasicRoles.Sleepwalker(), BasicRoles.Citizen(),
                BasicRoles.Framer()
        });

        editRule(SetupModifier.FOLLOW_GETS_ALL, true);

        setTarget(gs, sw);

        nextNight();

        shoot(sw, gs);
        setTarget(detect, sw);

        endNight();

        TestDetective.seen(detect, gs);
    }

    public void testSwWitch() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller sw = addPlayer(BasicRoles.Sleepwalker());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());

        TestUtil.assignRoles(narrator, new Controller[] {
                witch, detect, sw, cit1, cit2, cit3
        }, new FactionRole[] {
                BasicRoles.Witch(), BasicRoles.Detective(), BasicRoles.Sleepwalker(), BasicRoles.Citizen(),
                BasicRoles.Citizen(), BasicRoles.Citizen()
        });

        setTarget(detect, sw);
        witch(witch, sw, detect);

        endNight();

        TestDetective.seen(detect, detect);
    }

    public void testSleepWalkerVesting() {
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller sw = addPlayer(BasicRoles.Sleepwalker());
        Controller agent = addPlayer(BasicRoles.Agent());
        Controller cit = addPlayer(BasicRoles.Citizen());

        TestUtil.assignRoles(narrator, new Controller[] {
                as, sw, agent, cit
        }, new FactionRole[] {
                BasicRoles.Armorsmith(), BasicRoles.Sleepwalker(), BasicRoles.Agent(), BasicRoles.Citizen()
        });

        setTarget(as, sw);
        nextNight();

        setTarget(agent, sw);
        vest(sw);
        nextNight();

        partialExcludes(agent, Detective.NO_VISIT);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Sleepwalker(), AbilityModifier.BACK_TO_BACK, Sleepwalker.class,
                    false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Sleepwalker(), AbilityModifier.ZERO_WEIGHTED, Sleepwalker.class,
                    false);
            fail();
        }catch(NarratorException e){
        }
    }
}
