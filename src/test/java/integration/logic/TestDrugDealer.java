package integration.logic;

import game.ai.Controller;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Baker;
import game.roles.Disfranchise;
import game.roles.Doctor;
import game.roles.Douse;
import game.roles.DrugDealer;
import game.roles.Hidden;
import game.roles.Silence;
import game.roles.Spy;
import game.roles.Stripper;

public class TestDrugDealer extends SuperTest {

    public TestDrugDealer(String s) {
        super(s);
    }

    public void testDrugBasic() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller detect = addPlayer(BasicRoles.Detective());
        addPlayer(BasicRoles.Arsonist());

        nightStart();

        try{
            drug(dd, sheriff, null);
            fail();
        }catch(PlayerTargetingException e){
        }

        drug(dd, sheriff, DrugDealer.DOUSED);
        setTarget(detect, dd);
        setTarget(sheriff, dd);

        endNight();

        partialContains(sheriff, Douse.DOUSED_FEEDBACK);

        TestDetective.seen(detect, sheriff);
    }

    public void testDrugExists() {
        addPlayer(BasicRoles.Baker());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        addPlayer(Hidden.NeutralKillingRandom());

        try{
            drug(dd, sheriff, DrugDealer.DRIVEN);
            fail();
        }catch(PlayerTargetingException e){
        }

        assertFalse(dd.getPlayer(narrator).getOptions().get(DrugDealer.COMMAND).contains(new Option(DrugDealer.GUARDED)));
        assertTrue(
                dd.getPlayer(narrator).getOptions().get(DrugDealer.COMMAND).contains(new Option(DrugDealer.POISONED)));

        drug(dd, sheriff, DrugDealer.DOUSED);
        setTarget(sheriff, dd);

        endNight();

        partialContains(sheriff, Douse.DOUSED_FEEDBACK);
    }

    public void testDrugBlock() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Consort());

        drug(dd, sheriff, DrugDealer.BLOCKED);
        setTarget(sheriff, dd);
        setTarget(baker, sheriff);

        endNight();

        partialContains(sheriff, Baker.BreadReceiveMessage(narrator));
        partialContains(sheriff, Stripper.FEEDBACK);
    }

    public void testDrugBlockAndAdd() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Arsonist());

        setTarget(baker, dd);
        endNight();

        skipDay();

        drug(dd, sheriff, DrugDealer.DOUSED);
        setTarget(sheriff, dd);

        endNight();

        partialContains(sheriff, Douse.DOUSED_FEEDBACK);
    }

    public void testDrugItemBlock() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller dd = addPlayer(BasicRoles.DrugDealer());

        nightStart();
        setTarget(baker, sheriff);
        drug(dd, sheriff, DrugDealer.WIPE);
        setTarget(sheriff, dd);

        endNight();

        partialExcludes(sheriff, Baker.BreadReceiveMessage(narrator));
    }

    public void testDrugOperationOrder() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller dd2 = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Doctor());

        drug(dd, sheriff, DrugDealer.WIPE);
        endNight(dd);
        drug(dd2, sheriff, DrugDealer.DOCTOR);
        setTarget(sheriff, baker);

        endNight();

        partialExcludes(sheriff, Doctor.TARGET_FEEDBACK);
        partialExcludes(sheriff, "Your target");
    }

    public void testBlackmailWipeDrug() {
        Controller citizen = addPlayer(BasicRoles.Sheriff());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller blackmailer = addPlayer(BasicRoles.Blackmailer());

        nightStart();

        drug(dd, citizen, DrugDealer.WIPE);
        setTarget(blackmailer, citizen);

        endNight();

        partialContains(citizen, Silence.FEEDBACK);
        partialContains(citizen, Disfranchise.FEEDBACK);
    }

    public void testImmuneDrug() {
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Doctor());

        drug(dd, sk, DrugDealer.DOCTOR);

        endNight();

        partialContains(sk, Constants.NIGHT_IMMUNE_TARGET_FEEDBACK);
    }

    public void testCommand() {
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Consort());
        addPlayer(BasicRoles.Doctor());

        nightStart();

        command(dd, DrugDealer.COMMAND + " " + sk.getName() + " " + DrugDealer.BLOCKED.toLowerCase());

        endNight();

        partialContains(sk, Stripper.FEEDBACK);
    }

    public void testVigiFakeBlockedGettingGunsBack() {
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller dealer = addPlayer(BasicRoles.DrugDealer());
        Controller fodder1 = addPlayer(BasicRoles.Agent());
        Controller fodder2 = addPlayer(BasicRoles.Consort());

        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 2);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        setTarget(baker, vigi);

        nextNight();

        drug(dealer, vigi, DrugDealer.BLOCKED);
        shoot(vigi, fodder1);
        shoot(vigi, fodder2);

        endNight();
        isDead(fodder1, fodder2);

        assertTotalGunCount(2, vigi);
        assertRealGunCount(0, vigi);
        assertFakeGunCount(2, vigi);
        assertUseableBreadCount(1, vigi);
    }

    public void testFeedbackFakeBlock() {
        modifyRole(BasicRoles.Spy(), AbilityModifier.CHARGES, 1);

        Controller spy = addPlayer(BasicRoles.Spy());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Escort());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        drug(dd, spy, DrugDealer.BLOCKED);
        spy(spy, dd.getColor());
        endNight();

        partialContains(spy, Stripper.FEEDBACK);

        assertPerceivedChargeRemaining(0, spy, Spy.MAIN_ABILITY);
    }

    public void testVigiFakeBreadGetBack() {
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller dealer = addPlayer(BasicRoles.DrugDealer());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller fodder1 = addPlayer(BasicRoles.Agent());
        Controller fodder2 = addPlayer(BasicRoles.Consort());
        Controller fodder3 = addPlayer(BasicRoles.Agent());
        Controller fodder4 = addPlayer(BasicRoles.Consort());

        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, 2);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        setTarget(baker, vigi);

        nextNight();

        drug(dealer, vigi, DrugDealer.BLOCKED);
        shoot(vigi, fodder1);
        shoot(vigi, fodder2);
        setTarget(baker, vigi);
        setTarget(as, vigi);

        endNight();

        isDead(fodder1, fodder2);
        assertTotalGunCount(2, vigi);
        assertRealGunCount(0, vigi);
        assertFakeGunCount(2, vigi);
        assertUseableBreadCount(2, vigi);

        skipDay();

        shoot(vigi, fodder3);
        shoot(vigi, fodder4);
        vest(vigi);
        assertActionSize(3, vigi);
        endNight();

        isAlive(fodder3, fodder4);
        assertUseableBreadCount(0, vigi);
        assertTotalGunCount(0, vigi);
        assertRealGunCount(0, vigi);
        assertFakeGunCount(0, vigi);
        assertTotalVestCount(0, vigi);
    }
}
