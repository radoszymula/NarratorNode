package integration.logic;

import game.ai.Controller;
import game.logic.Faction;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.RoleModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Hidden;
import game.roles.Veteran;
import game.setups.Setup;
import services.FactionRoleService;
import services.FactionService;
import services.HiddenService;
import util.LookupUtil;

public class TestVeteran extends SuperTest {

    public TestVeteran(String s) {
        super(s);
    }

    public void testBGSave() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Witch());

        nightStart();

        assertNull(vet.getPlayer(narrator).getAcceptableTargets(Veteran.COMMAND));

        setTarget(vet);
        setTarget(vig, vet, GUN);
        setTarget(bg, vig);

        endNight();

        isAlive(vig);
        assertTrue(vet.is(Veteran.class));
        isAlive(vet);
        isDead(bg);
    }

    public void testCommandTest() {
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Witch());

        setTarget(sk, vet);
        command(vet, Veteran.COMMAND);
        endNight();

        isDead(sk);
    }

    public void testLimits() {
        modifyRole(BasicRoles.Veteran(), AbilityModifier.CHARGES, 1);

        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vet2 = addPlayer(BasicRoles.Veteran());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Lookout());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        nightStart();

        setTarget(vet);
        endNight();

        skipDay();

        // out of shots
        try{
            setTarget(vet, vet);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(vet2);
        witch(witch, vet, vet2);

        endNight();

        isAlive(witch);
        isDead(vet);
    }

    public void testImmune() {
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller mm = addPlayer(BasicRoles.MassMurderer());

        nightStart();

        setTarget(vet);
        setTarget(mm, vet);
        setTarget(sk, vet);

        endNight();

        isWinner(vet);
        isLoser(sk, mm);
    }

    public void testLimitations() {
        modifyRole(BasicRoles.Veteran(), AbilityModifier.CHARGES, 1);

        Controller detective = addPlayer(BasicRoles.Detective());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        nightStart();

        alert(vet);

        endNight();
        skipDay();

        try{
            alert(vet);
            fail();
        }catch(PlayerTargetingException e){
        }

        witch(witch, vet, detective);

        setTarget(detective, vet);

        endNight();

        TestDetective.seen(detective, detective);

        assertEquals(3, narrator.getLiveSize());
    }

    public void testBasicFunction() {
        PlayerList players = new PlayerList();
        Hidden hidden = HiddenService.createHidden(narrator, "Single Targetables", BasicRoles.Armorsmith(),
                BasicRoles.Detective());

        assertTrue(hidden.getSize() != 0);

        for(int i = 0; i < 10; i++){
            addPlayer(hidden);
            players.add(narrator.getAllPlayers());
        }
        Role citizenRole = LookupUtil.findRole(narrator, "Citizen");
        Role veteranRole = LookupUtil.findRole(narrator, "Veteran");

        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Controller cit = addPlayer(FactionService.createFactionRole(mafia, citizenRole));
        Controller vet = addPlayer(FactionService.createFactionRole(mafia, veteranRole));

        nightStart();

        for(Controller p: players)
            setTarget(p, vet);
        try{
            setTarget(vet, cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(vet);

        endNight();

        assertGameOver();
        isWinner(vet);
    }

    public void testDocSave() {

        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Witch());

        nightStart();

        setTarget(doc, vig);
        setTarget(vet);
        setTarget(vig, vet, GUN);

        endNight();

        isAlive(vet);
        assertEquals(4, narrator.getLiveSize());
    }

    public void testBDTwiceVisits() {
        Controller bd1 = addPlayer(BasicRoles.BusDriver());
        Controller bd2 = addPlayer(BasicRoles.BusDriver());
        Controller doc1 = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller doc3 = addPlayer(BasicRoles.Doctor());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vet2 = addPlayer(BasicRoles.Veteran());
        addPlayer(BasicRoles.Witch());

        setTarget(doc1, bd1);
        setTarget(doc2, bd2);
        setTarget(doc3, bd2);
        setTarget(vet);
        setTarget(vet2);
        drive(bd1, vet, vet2);
        drive(bd2, vet2, vet);
        endNight();

        isAlive(bd2);
        isDead(bd1);
    }

    public void testNotImmuneAnymore() {
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Agent(), 2);

        alert(veteran);
        nextNight();

        setTarget(sk, veteran);
        endNight();

        isDead(veteran);
    }

    public void testImmunityOnGameEnd() {
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Agent(), 1);

        alert(veteran);

        setTarget(sk, veteran);
        endNight();

        isInvuln(veteran, false);
    }

    public void testVeteranCulted() {
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller mafia = addPlayer(BasicRoles.Goon());

        editRule(SetupModifier.CULT_KEEPS_ROLES, false);

        setTarget(doc, cl);
        setTarget(cl, veteran);
        mafKill(mafia, veteran);
        alert(veteran);
        endNight();

        isInvuln(veteran, false);
    }

    public void testVeteranCultedJesterKilled() {
        addPlayer(BasicRoles.Citizen(), 2);
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller jester = addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Witch());

        modifyRole(BasicRoles.CultLeader(), RoleModifier.AUTO_VEST, 1);

        editRule(SetupModifier.CULT_KEEPS_ROLES, false);

        vote(vet, jester);
        narrator.forceEndDay();

        isAlive(vet);

        alert(vet);
        setTarget(cl, vet);
        endNight();
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Veteran(), AbilityModifier.BACK_TO_BACK, Veteran.class, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
