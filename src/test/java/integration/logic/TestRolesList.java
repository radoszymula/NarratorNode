package integration.logic;

import game.logic.RolesList;
import game.logic.templates.BasicRoles;
import game.roles.Hidden;
import util.HiddenUtil;

public class TestRolesList extends SuperTest {

    public TestRolesList(String name) {
        super(name);
    }

    public void testEqualRolesList() {
        RolesList rl1 = new RolesList();
        rl1.add(HiddenUtil.createHiddenSingle(narrator, BasicRoles.Citizen()));
        rl1.add(Hidden.TownInvestigative());
        rl1.add(HiddenUtil.createHiddenSingle(narrator, BasicRoles.Arsonist()));

        RolesList rl2 = new RolesList();
        rl2.add(Hidden.TownInvestigative());
        rl2.add(HiddenUtil.createHiddenSingle(narrator, BasicRoles.Citizen()));
        rl2.add(HiddenUtil.createHiddenSingle(narrator, BasicRoles.Arsonist()));

        assertEquals(rl1.hashCode(), rl2.hashCode());
    }

}
