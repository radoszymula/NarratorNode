package integration.logic;

import java.util.ArrayList;

import game.ai.Controller;
import game.event.DeathAnnouncement;
import game.event.Message;
import game.logic.Faction;
import game.logic.MemberList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownRoleException;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.logic.templates.HTMLDecoder;
import game.roles.Citizen;
import game.roles.CultLeader;
import game.roles.Driver;
import game.roles.DrugDealer;
import game.roles.GraveDigger;
import game.roles.Sheriff;
import game.roles.Tailor;
import game.setups.Setup;

public class TestTailor extends SuperTest {

    public TestTailor(String name) {
        super(name);
    }

    /*
     * not testing these features because of the nature of the implementation
     * 
     * invest sheriff coroner framing
     */

    Controller tailor;

    @Override
    public void roleInit() {
        tailor = addPlayer(BasicRoles.Tailor());
    }

    private void suit(String color, String roleName, Controller target) {
        setTarget(tailor, Tailor.MAIN_ABILITY, color, roleName, target);
    }

    public void testBasic() {
        Controller cit = addPlayer(BasicRoles.Sheriff());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifier.TAILOR_FEEDBACK, true);

        setTarget(sk, cit);
        tailor(tailor, cit, witch);

        endNight();
        assertSuited(cit, witch);
        partialContains(cit, Tailor.FEEDBACK);
    }

    public void testBadOptions() {
        Controller cit = addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.SerialKiller());

        nightStart();

        try{
            suit(Setup.BENIGN_C, "Roofus", cit);
            fail();
        }catch(UnknownRoleException | PlayerTargetingException e){
        }
        try{
            suit(Setup.MAFIA_C, "Roofus", cit);
            fail();
        }catch(UnknownRoleException | PlayerTargetingException e){
        }
    }

    public void testCharges() {
        setCharges(1);

        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller coroner = addPlayer(BasicRoles.Coroner());

        nightStart();

        tailor(tailor, cit, coroner);

        nextNight();

        try{
            tailor(tailor, cit, coroner);
            fail();
        }catch(PlayerTargetingException e){
        }
        ;
    }

    public void testDDCharges() {
        setCharges(1);

        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller escort = addPlayer(BasicRoles.Escort());

        drug(dd, tailor, DrugDealer.BLOCKED);
        tailor(tailor, dd, escort);

        endNight();

        assertPerceivedChargeRemaining(1, tailor);
    }

    private void setCharges(int i) {
        modifyRole(BasicRoles.Tailor(), AbilityModifier.CHARGES, i);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);
    }

    public void testElectroCharges() {
        setCharges(1);

        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller doc = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Doctor());

        electrify(electro, tailor);

        setTarget(doc, tailor);
        tailor(tailor, electro, doc);

        endNight();

        isAlive(tailor);
        assertPerceivedChargeRemaining(0, tailor);

        voteOut(electro, tailor, doc, this.tailor);

        assertSuited(electro, doc);
    }

    public void testSnitch() {
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller snitch = addPlayer(BasicRoles.Snitch());
        Controller doc = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Snitch());

        modifyAbilitySelfTarget(BasicRoles.Tailor(), true);
        editRule(SetupModifier.SNITCH_PIERCES_SUIT, false);

        nightStart();
        
        assertTrue(tailor.getPlayer(narrator).getAbility(Tailor.class).modifiers.getBoolean(AbilityModifier.SELF_TARGET));
        
        setTarget(doc, tailor);
        tailor(tailor, tailor, doc);
        setTarget(snitch, tailor);

        nextNight();
        hasSuits(tailor);
        mafKill(tailor, snitch);

        ArrayList<Message> announcements = addAnnouncementListener();

        endNight();

        TestSnitch.SnitchAnnouncement(announcements, doc.getRoleName(), tailor.getRoleName());
    }

    public void testSnitchPiercing() {
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller snitch = addPlayer(BasicRoles.Snitch());
        Controller doc = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Snitch());

        modifyAbilitySelfTarget(BasicRoles.Tailor(), true);
        editRule(SetupModifier.SNITCH_PIERCES_SUIT, true);

        setTarget(doc, tailor);
        tailor(tailor, tailor, doc);
        setTarget(snitch, tailor);

        nextNight();
        hasSuits(tailor);
        mafKill(tailor, snitch);

        ArrayList<Message> announcements = addAnnouncementListener();

        endNight();

        TestSnitch.SnitchAnnouncement(announcements, tailor.getRoleName(), doc.getRoleName());
    }

    public void testDayLynchSuitDisappear() {
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cit = addPlayer(BasicRoles.Doctor());
        Controller fodder = addPlayer(BasicRoles.Agent());

        nightStart();

        tailor(tailor, fodder, sk);

        endNight();

        voteOut(tailor, doc, sk, cit);

        setTarget(sk, fodder);

        endNight();

        assertSuited(fodder, fodder);
    }

    public void testNightKillSuitPersist() {
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Agent());

        startNight();

        electrify(electro, tailor, cit);

        tailor(tailor, cit, tailor);

        endNight();

        assertSuited(cit, tailor);
    }

    public void testNightKillSuitDisappear() {
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller cit = addPlayer(BasicRoles.BusDriver());
        Controller agent = addPlayer(BasicRoles.Agent());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(sk, cit);
        tailor(tailor, cit, tailor);
        mafKill(agent, tailor);

        endNight();

        assertSuited(cit, cit);
    }

    public void testWitch() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        tailor(tailor, sk, tailor);
        setTarget(detect, tailor);
        witch(witch, tailor, cit);
        setTarget(sk, cit);

        endNight();

        assertSuited(cit, tailor);
        TestDetective.seen(detect, cit);
    }

    public void testJanOverride() {
        Controller janitor = addPlayer(BasicRoles.Janitor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller tailor = addPlayer(BasicRoles.Tailor());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        tailor(tailor, cit, janitor);

        nextNight();
        mafKill(tailor, cit);
        setTarget(janitor, cit);

        endNight();
        DeathAnnouncement da;
        String access, access2;
        for(Message m: narrator.getEventManager().getEvents(Message.PUBLIC)){
            if(m instanceof DeathAnnouncement){
                da = (DeathAnnouncement) m;
                if(da.dead.contains(cit.getPlayer(narrator))){
                    access = m.access(Message.PUBLIC, new HTMLDecoder());
                    assertTrue(access.contains("???") || access.contains("could not"));
                    if(!access.contains("could not"))
                        assertTrue(access.contains(Constants.INITIAL));

                    access2 = m.access(Message.PRIVATE, new HTMLDecoder());
                    ;
                    if(!access.contains("could not"))
                        assertTrue(access2.contains(cit.getColor()));
                    assertTrue(access2.contains(cit.getRoleName()));
                    return;
                }
            }
        }
        fail();
    }

    public void testTailorCorpse() {
        setCharges(1);

        Controller digger = addPlayer(BasicRoles.GraveDigger());
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Veteran());

        setTarget(sk, tailor);
        nextNight();

        setTarget(digger, GraveDigger.MAIN_ABILITY, Tailor.COMMAND, sk.getColor(), sk.getRoleName(), tailor, cit);

        nextNight();
        super.assertRealChargeRemaining(0, tailor, Tailor.MAIN_ABILITY);
        hasSuits(cit);

        setTarget(digger, GraveDigger.MAIN_ABILITY, Tailor.COMMAND, cit.getColor(), cit.getRoleName(), tailor, cit);
        setTarget(sk, cit);

        endNight();

        assertSuited(cit, sk);
    }

    public void testTailorCommand() {
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Armorsmith());

        nightStart();

        command(tailor, Tailor.COMMAND, cit.getName(), sk.getPlayer(narrator).getFaction().getName(), sk.getRoleName());
        setTarget(sk, cit);

        endNight();

        assertSuited(cit, sk);
    }

    public void testNoCultSuit() {
        Controller cl = addPlayer(BasicRoles.Cultist());
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller citizen = addPlayer(BasicRoles.Citizen());

        nightStart();
        try{
            setTarget(tailor, Tailor.MAIN_ABILITY, cl.getColor(), tailor.getRoleName(), citizen);
        }catch(UnknownRoleException | PlayerTargetingException e){
        }

    }

    public void testCultedSuit() {
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller citizen = addPlayer(BasicRoles.Citizen());

        nightStart();

        MemberList mList = narrator.getPossibleMembers();
        assertTrue(mList.abilityExists(narrator, CultLeader.class));

        ArrayList<Faction> tailorTeams = mList.getFactions(Tailor.class, narrator);
        assertEquals(2, tailorTeams.size());

        ArrayList<Faction> citTeams = mList.getFactions(Citizen.class, narrator);
        assertEquals(2, citTeams.size());

        setTarget(tailor, Tailor.MAIN_ABILITY, cl.getColor(), tailor.getRoleName(), citizen);

        endNight();

        assertEquals(cl.getColor(), citizen.getPlayer(narrator).suits.get(0).color);

        // check that culted teams are allowed, if cult leader exists
        // check that roles list 'members' has this role.
    }

    public void testTailorCorpseCH() {
        Controller gd = addPlayer(BasicRoles.GraveDigger());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen());

        voteOut(tailor, gd, cit, sk);

        command(gd, GraveDigger.COMMAND, tailor.getName(), cit.getName(), sk.getName(), Tailor.COMMAND,
                sk.getPlayer(narrator).getFaction().getName(), sk.getRoleName());
        setTarget(sk, cit);
        endNight();

        assertSuited(cit, sk);
    }

    public void testPromotionTailorRoles() {
        addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.BusDriver());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.CULT_KEEPS_ROLES, false);
        editRule(SetupModifier.CULT_PROMOTION, true);

        nightStart();

        MemberList ml = narrator.getPossibleMembers();
        assertEquals(1, ml.getOtherColors(new Driver()).size());
        assertEquals(1, ml.getOtherColors(new Sheriff()).size());
    }
}
