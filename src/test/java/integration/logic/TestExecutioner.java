package integration.logic;

import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.support.Random;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Bulletproof;
import game.roles.Executioner;
import game.roles.Jester;
import game.roles.SerialKiller;
import game.setups.Setup;
import models.FactionRole;
import services.FactionRoleService;
import services.FactionService;
import services.RoleAbilityModifierService;
import services.RoleService;
import util.TestUtil;

public class TestExecutioner extends SuperTest {

    public TestExecutioner(String name) {
        super(name);
    }

    public void testWin() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller exec = addPlayer(BasicRoles.Executioner());

        dayStart();

        SetExecTarget(exec, cit);

        voteOut(cit, exec, maf);

        isWinner(maf, exec);
    }

    public static void SetExecTarget(Controller exec, Controller target) {
        Executioner.SetTarget(exec.getPlayer(narrator), target.getPlayer(narrator));

        skipTearDown = true;
    }

    /*
     * public void testUnconvertableAfterWin(){ Controller exec =
     * addPlayer(BasicRoles.Executioner()); Controller cit =
     * addPlayer(BasicRoles.Citizen()); Controller maf =
     * addPlayer(BasicRoles.Mafioso()); Controller cultLeader =
     * addPlayer(BasicRoles.CultLeader());
     * 
     * n.getRandom().queuePlayer(cit);
     * 
     * dayStart(); assertEquals(((Executioner) exec.getRole()).getTarget(exec),
     * cit);
     * 
     * lynch(cit, exec, maf, cultLeader); setTarget(cultLeader, exec);
     * 
     * endNight(); assertTrue(exec.getRole().isWinner(exec, n));
     * assertEquals(Constants.A_CULT, exec.getColor()); }
     */

    public void testLoss() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller exec = addPlayer(BasicRoles.Executioner());

        dayStart();

        SetExecTarget(exec.getPlayer(narrator), maf.getPlayer(narrator));

        voteOut(cit, exec, maf);

        isWinner(maf);
        isLoser(exec);
    }

    public void testExecDeathLoss() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller exec = addPlayer(BasicRoles.Executioner());

        nightStart();

        SetExecTarget(exec, cit);

        setTarget(maf, exec, KILL);

        endNight();

        isLoser(exec);
    }

    public void testTargetDeathLoss() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller exec = addPlayer(BasicRoles.Executioner());

        nightStart();
        SetExecTarget(exec, cit);

        setTarget(maf, cit, KILL);

        endNight();

        isLoser(exec);
    }

    public void testPermanentInvul() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller exec = addPlayer(BasicRoles.Executioner());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        TestUtil.addAbility(BasicRoles.Executioner().role, Bulletproof.class);

        nightStart();
        SetExecTarget(exec, cit);

        setTarget(sk, exec);
        endNight();

        assertInProgress();
    }

    public void testInvulFailure() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller exec = addPlayer(BasicRoles.Executioner());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifier.EXECUTIONER_WIN_IMMUNE, true);

        nightStart();
        SetExecTarget(exec, cit);

        setTarget(sk, exec);
        endNight();

        assertGameOver();
    }

    public void testInvulSuccess() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller exec = addPlayer(BasicRoles.Executioner());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifier.EXECUTIONER_WIN_IMMUNE, true);

        dayStart();
        SetExecTarget(exec, cit);

        voteOut(cit, cit2, exec, sk);

        setTarget(sk, exec);

        endNight();

        assertGameOver();
    }

    public void testExecToJester() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller exec = addPlayer(BasicRoles.Executioner());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifier.EXECUTIONER_TO_JESTER, true);

        nightStart();
        SetExecTarget(exec, cit);

        setTarget(sk, cit);
        endNight();

        assertTrue(exec.is(Jester.class));
        assertFalse(exec.is(Executioner.class));

        voteOut(exec, cit2, sk);

        endNight();

        isWinner(exec);
    }

    public void testTownOnlyTarget() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller exec = addPlayer(BasicRoles.Executioner());
        addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifier.EXEC_TOWN, true);

        long seed = new Random().nextLong();
        // seed = Long.parseLong("-8495726696867851616");

        System.out.println("Exec town test - " + seed);

        narrator.setSeed(seed);

        dayStart();
        assertEquals(3, narrator.getFaction(Setup.TOWN_C)._startingSize);
        assertEquals(1, narrator.getFaction(Setup.BENIGN_C)._startingSize);
        assertEquals(2, narrator.getFaction(Setup.MAFIA_C)._startingSize);

        int original = narrator.getEventManager().dayChat.getEvents().events.get(0).getDay();

        Controller target = getTarget(exec);
        assertTrue(target == cit1 || target == cit2 || target == cit3);

        skipDay();
        nextNight();

        assertEqual(original, narrator.getEventManager().dayChat.getEvents().events.get(0).getDay());
    }

    private Controller getTarget(Controller exec) {
        return exec.getPlayer(narrator).getAbility(Executioner.class).getTarget();
    }

    // test
    // need to start off with at least as many charges as there are town

    public void testCharges() {
        Role mashedRole = RoleService.createRole(narrator, "Serial Executioner", SerialKiller.class, Executioner.class);
        FactionRole mashed = FactionService.createFactionRole(narrator.getFaction(Setup.BENIGN_C), mashedRole);
        RoleAbilityModifierService.modify(mashedRole, Executioner.class, AbilityModifier.CHARGES, 2);
        assertEquals(2, mashedRole._abilities.getAbility(Executioner.class.getSimpleName()).modifiers
                .getInt(AbilityModifier.CHARGES));

        Controller exec = addPlayer(mashed);
        Controller doc1 = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller doc3 = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifier.EXECUTIONER_TO_JESTER, false);
        editRule(SetupModifier.CHARGE_VARIABILITY, 7);

        assertFalse(new Executioner().hasCooldownAbility());
        assertNotNull(new Executioner().hasChargeAbility());

        nightStart();

        assertEquals(1, exec.getPlayer(narrator).getAbility(Executioner.class).getRealCharges());

        Controller target1 = getTarget(exec);

        setTarget(exec, target1, SerialKiller.MAIN_ABILITY);
        endNight();

        Controller target2 = getTarget(exec);

        isAlive(exec);
        assertNotSame(target2, target1);

        skipDay();

        Controller liveDoc = ControllerList.list(doc1, doc2, doc3).getLivePlayers(narrator).getFirst();
        setTarget(liveDoc, exec);
        setTarget(exec, target2, SerialKiller.MAIN_ABILITY);
        endNight();

        isDead(exec);
        assertNotSame(0, exec.getPlayer(narrator).getDeathType().attacks.size());
    }

    public void testNoChoices() {
        Controller vigilante = addPlayer(BasicRoles.Vigilante());
        Controller maf = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Executioner());

        editRule(SetupModifier.EXEC_TOWN, true);
        modifyAbilityCharges(BasicRoles.Executioner(), 100);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);

        shoot(vigilante, maf);
        mafKill(maf, vigilante);
        endNight();
    }

    public void testKeepOtherAbilities() {
        Role mashedRole = RoleService.createRole(narrator, "Serial Executioner", SerialKiller.class, Executioner.class);
        FactionRole mashed = FactionService.createFactionRole(narrator.getFaction(Setup.BENIGN_C), mashedRole);
        RoleAbilityModifierService.modify(mashedRole, Executioner.class, AbilityModifier.CHARGES, 2);
        assertEquals(2, mashedRole._abilities.getAbility(Executioner.class.getSimpleName()).modifiers
                .getInt(AbilityModifier.CHARGES));

        Controller exec = addPlayer(mashed);
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifier.EXECUTIONER_TO_JESTER, true);

        nightStart();

        Controller target1 = getTarget(exec);

        setTarget(exec, target1, SerialKiller.MAIN_ABILITY);
        endNight();

        assertEquals(2, exec.getPlayer(narrator).getAbilities().size());
        assertTrue(exec.is(SerialKiller.class));
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Executioner(), AbilityModifier.ZERO_WEIGHTED, Executioner.class,
                    false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Executioner(), AbilityModifier.BACK_TO_BACK, Executioner.class,
                    false);
            fail();
        }catch(NarratorException e){
        }
    }
}
