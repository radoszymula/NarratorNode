package integration.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import game.ai.Computer;
import game.ai.Controller;
import game.event.ArchitectChat;
import game.event.Message;
import game.logic.Faction;
import game.logic.Narrator;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.Role;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PhaseException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.VotingException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.StoryPackage;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.support.rules.SetupModifiers;
import game.logic.templates.BasicRoles;
import game.logic.templates.NativeStoryPackage;
import game.roles.Armorsmith;
import game.roles.Citizen;
import game.roles.Doctor;
import game.roles.DrugDealer;
import game.roles.FactionKill;
import game.roles.Goon;
import game.roles.Gunsmith;
import game.roles.Hidden;
import game.roles.Jester;
import game.roles.SerialKiller;
import game.roles.Sheriff;
import game.roles.TeamTakedown;
import game.roles.UnsetRole;
import game.roles.Ventriloquist;
import game.setups.Setup;
import junit.framework.TestCase;
import models.Command;
import models.SetupHidden;
import services.FactionService;
import services.RoleService;
import services.SetupHiddenService;
import util.LookupUtil;

public class TestStructure extends SuperTest {

    public TestStructure(String name) {
        super(name);
    }

    public void testPhaseEndCommand() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Detective());
        addPlayer(BasicRoles.Chauffeur());
        addPlayer(BasicRoles.SerialKiller());

        dayStart();

        vote(p1, p3);

        command(narrator.skipper, CommandHandler.END_PHASE);

        assertIsNight();
        isDead(p3);

        command(narrator.skipper, CommandHandler.END_PHASE);

        assertIsDay();
    }

    // initial setup
    public void testMinimumRoleSize() {
        for(int i = 0; i < 3; i++)
            addPlayer(Computer.toLetter(i + 1));

        assertEquals(3, narrator.getPlayerCount());

        addRole(BasicRoles.Citizen());

        // tests
        assertBadGameSettings();

        addRole(BasicRoles.Citizen());
        addRole(BasicRoles.Goon());

        assertEquals(3, narrator.getRolesList().size());
        nightStart();
    }

    public void testTargetDead() {
        // people shouldn't be able to target dead people
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        vote(cit3, cit);
        vote(cit2, cit);
        vote(cit, sk);
        vote(sk, cit);

        assertNotSame(cit.getPlayer(narrator).getIcon(), cit2.getPlayer(narrator).getIcon());

        try{
            setTarget(sk, cit);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testSameCharges() {
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller jailor = addPlayer(BasicRoles.Jailor());

        narrator.setSeed(1);
        nightStart();

        newNarrator();

        Controller veteran2 = addPlayer(BasicRoles.Veteran());
        Controller spy2 = addPlayer(BasicRoles.Spy());
        Controller tailor2 = addPlayer(BasicRoles.Tailor());
        Controller jailor2 = addPlayer(BasicRoles.Jailor());

        narrator.setSeed(1);
        nightStart();

        chargeEquality(veteran, veteran2);
        chargeEquality(spy, spy2);
        chargeEquality(tailor, tailor2);
        chargeEquality(jailor, jailor2);

        SuperTest.BrainEndGame = false;
    }

    // guaranteeing its only the single role
    private void chargeEquality(Controller p1, Controller p2) {
        assertEquals(p1.getPlayer(narrator).getAbilities().get(0).getPerceivedCharges(),
                p2.getPlayer(narrator).getAbilities().get(0).getPerceivedCharges());
    }

    public void testLotOfFeedback() {

        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller agent = addPlayer(BasicRoles.Lookout());
        Controller cons = addPlayer(BasicRoles.Consort());
        Controller cont = addPlayer(BasicRoles.Consort());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        setTarget(agent, bd);
        setTarget(cont, agent);
        setTarget(cons, agent);
        witch(witch, agent, agent);

        endNight();
    }

    public void testDoubleFeedbackOld() {

        addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller vig = addPlayer(BasicRoles.Vigilante());

        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();

        setTarget(sk, cit2);
        setTarget(vig, cit2, GUN);
        setTarget(doc, cit2);

        endNight();

        isDead(cit2);

    }

    // makes sure that all setups have an opponent
    public void testValidOpponentCheck() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        try{
            dayStart();
            fail();
        }catch(IllegalGameSettingsException e){
        }
    }

    public void testValidOpponentCheck2() {
        addPlayer(Hidden.AnyRandom());
        addPlayer(Hidden.MafiaRandom());
        addPlayer(Hidden.TownRandom());
        addPlayer(Hidden.NeutralRandom());

        nightStart();
    }

    // tests to see if there are enough players
    public void testPlayerSizeCheck() {
        addPlayer("Test1");
        addPlayer("Test2");

        addRole(BasicRoles.Sheriff());
        addRole(BasicRoles.Sheriff());
        addRole(BasicRoles.Goon());

        try{
            dayStart();
            fail();
        }catch(IllegalGameSettingsException e){
        }

    }

    // regular game
    public void testGameStructure() {
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller mafia1 = addPlayer(BasicRoles.Goon());
        Controller mafia2 = addPlayer(BasicRoles.Agent());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifier.HEAL_SUCCESS_FEEDBACK, true);
        editRule(SetupModifier.HEAL_FEEDBACK, true);

        nightStart();

        assertEquals(0, narrator.getDayNumber());

        assertInProgress();

        setTarget(serialKiller, mafia1);
        setTarget(doctor, mafia1);

        setTarget(sheriff, citizen);

        setTarget(mafia2, citizen, KILL);
        setTarget(mafia1, mafia2, SEND);
        setTarget(mafia2, mafia2, SEND);

        endNight();

        isAlive(mafia1);
        isAlive(mafia2);
        isAlive(sheriff);
        isAlive(serialKiller);
        isAlive(doctor);

        isDead(citizen);

        partialContains(citizen, Goon.ANONYMOUS_DEATH_FEEDBACK);
        partialContains(doctor, Doctor.SUCCESFULL_HEAL);
        partialContains(sheriff, Sheriff.NOT_SUSPICIOUS);
        partialContains(mafia1, Doctor.TARGET_FEEDBACK);
        // assertEquals(0, serialKiller.getNightFeedback().size());
        // assertEquals(0, mafia2.getNightFeedback().size());

        assertInProgress();
        assertEquals(1, narrator.getDayNumber());

        assertEquals(1, narrator.getDeadSize());
        assertEquals(citizen, (narrator.getDeadList(0)).get(0));

        assertIsDay();

        // dead person voting
        try{
            vote(citizen, sheriff);
            fail();
        }catch(VotingException e){
        }

        // person voting dead person
        try{
            vote(sheriff, citizen);
            fail();
        }catch(VotingException | PlayerTargetingException e){
        }

        vote(sheriff, mafia2);

        assertVoteTarget(mafia2, sheriff);

        unvote(sheriff);
        vote(sheriff, mafia2);
        skipVote(sheriff);
        vote(serialKiller, mafia2);

        skipDay();

        /*
         * sheriff doctor serial killer mafia1 mafia2
         */

        setTarget(sheriff, serialKiller);
        setTarget(doctor, sheriff);
        setTarget(mafia1, serialKiller, KILL);
        setTarget(mafia1, mafia1, SEND);
        setTarget(mafia2, mafia1, SEND);
        setTarget(serialKiller, sheriff);

        endNight();

        isAlive(serialKiller);
        assertEquals(1, narrator.getDeadSize());

        voteOut(mafia2, sheriff, mafia1, doctor);

        try{
            vote(serialKiller, mafia2);
            fail();
        }catch(VotingException | PlayerTargetingException e){
        }

        isDead(mafia2);

        /*
         * sheriff doctor serial killer mafia1
         */

        setTarget(doctor, mafia1);
        setTarget(serialKiller, doctor);
        setTarget(mafia1, sheriff, KILL);

        endNight();

        isWinner(serialKiller);
    }

    public void testDeathDay() {
        Controller A = addPlayer(BasicRoles.Doctor());
        Controller C = addPlayer(BasicRoles.Sheriff());
        Controller E = addPlayer(BasicRoles.Escort());
        Controller G = addPlayer(BasicRoles.Citizen());
        Controller H = addPlayer(BasicRoles.Sheriff());
        Controller I = addPlayer(BasicRoles.Sheriff());
        Controller K = addPlayer(BasicRoles.Sheriff());
        Controller voss = addPlayer(BasicRoles.Mayor());
        Controller P = addPlayer(BasicRoles.Goon());
        Controller Q = addPlayer(BasicRoles.Blackmailer());
        Controller R = addPlayer(BasicRoles.Jester());
        Controller T = addPlayer(BasicRoles.Jester());
        Controller S = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Lookout());
        addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Escort());
        addPlayer(BasicRoles.Veteran());
        addPlayer(BasicRoles.Chauffeur());
        addPlayer(BasicRoles.Goon());

        dayStart();

        PlayerList players = narrator.getAllPlayers();

        voteOut(I, R, A, C, E, G, H, K, P, Q, T, voss);

        players.sortByDeath();
        assertTrue(players.get(19) == I);

        isDead(I);
        assertEqual(I.getPlayer(narrator).getDeathDay(), 1);
        assertTrue(I.getPlayer(narrator).getDeathType().isLynch());

        setTarget(S, A);

        endNight();

        isDead(A);
        assertEqual(1, A.getPlayer(narrator).getDeathDay());

        players.sortByDeath();
        assertTrue(players.get(18) == I);
        assertTrue(players.get(19) == A);

    }

    public void testModKill() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller cit4 = addPlayer(BasicRoles.Citizen());
        Controller cit5 = addPlayer(BasicRoles.Citizen());

        Controller maf = addPlayer(BasicRoles.Goon());

        dayStart();

        vote(cit, cit2);

        cit.getPlayer(narrator).modkill();

        assertVoteTarget(null, cit);

        skipDay();

        endNight(cit2, cit3, maf, cit5);

        command(cit4, CommandHandler.MODKILL);

        assertIsDay();

        vote(cit2, maf);
        vote(cit5, maf);
        vote(cit3, cit2);
        vote(maf, cit2);

        cit3.getPlayer(narrator).modkill();

        assertGameOver();
    }

    public void testChangeRole() {
        Controller doc1 = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Sheriff());

        nightStart();

        narrator.changeRole(doc1.getPlayer(narrator), BasicRoles.Citizen());
        Controller cit = doc1;
        try{
            setTarget(cit, doc2);
            fail();
        }catch(PlayerTargetingException e){
        }

        narrator.changeRole(cit.getPlayer(narrator), BasicRoles.Sheriff());
        Controller sheriff = cit;
        assertTrue(sheriff.is(Sheriff.class));

        setTarget(sheriff, sk);
        assertTrue(sk.getPlayer(narrator).in(sheriff.getPlayer(narrator).getTargets(Sheriff.MAIN_ABILITY)));
        assertTrue(sk.is(SerialKiller.class));

        endNight();
        assertTrue(doc1.getPlayer(narrator).getInitialAbilities().contains(Doctor.class));
        assertFalse(doc1.getPlayer(narrator).getInitialAbilities().contains(Citizen.class));

        assertFalse(cit.getPlayer(narrator).getInitialAbilities().contains(Citizen.class));
        assertFalse(cit.getPlayer(narrator).getInitialAbilities().contains(Sheriff.class));
        // assertTrue(doc1.getNightFeedback().contains("Your target is a Serial
        // Killer!"));

    }

    /*
     * tests killing during the day and night
     */

    public void testEndGameText() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(sk, cit2);

        endNight();

        assertFalse(narrator.getHappenings().contains("Night 1"));
        assertEquals(1, narrator.getDayNumber());
        assertTrue(narrator.getHappenings().contains("Day 1"));
    }

    public void testSingleNightAction() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(sk, cit);
        endNight();

        isDead(cit);
        skipVote(cit2, cit3);

        try{
            setTarget(cit, narrator.skipper);
            fail();
        }catch(PlayerTargetingException e){
        }

        assertTrue(narrator.getDeadList(narrator.getDayNumber() - 1).contains(cit.getPlayer(narrator)));
    }

    public void testCitizen() {
        Controller citizen1 = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        nightStart();

        try{
            setTarget(citizen1, citizen2);
            fail();
        }catch(PlayerTargetingException e){
        }
        try{
            setTarget(citizen1, narrator.skipper);
        }catch(PlayerTargetingException e){
        }
    }

    public void testHappenings() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller sher = addPlayer(BasicRoles.Sheriff());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller vig2 = addPlayer(BasicRoles.Vigilante());

        Controller agent = addPlayer(BasicRoles.Agent());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller chf = addPlayer(BasicRoles.Chauffeur());
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller jan = addPlayer(BasicRoles.Janitor()); // 20
        Controller maf = addPlayer(BasicRoles.Goon());

        Controller exec = addPlayer(BasicRoles.Executioner());
        Controller jest = addPlayer(BasicRoles.Jester());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, SetupModifiers.UNLIMITED);

        dayStart();

        reveal(mayor);
        skipDay();

        frame(framer, mayor, framer.getColor());
        drive(bd, exec, jest);
        shoot(vig, witch);
        witch(witch, vig, vig2);
        shoot(vig2, vig);
        drive(chf, bd, vig);
        setTarget(sher, mayor);

        endNight();

        assertIsDay();

        voteOut(mayor, lookout, detect, doc, doc2, vig, sk, exec, jest, maf, witch);

        assertIsNight();

        // packageTest();
        setTarget(detect, lookout);
        setTarget(lookout, doc);
        setTarget(agent, cit);
        mafKill(maf, jest);
        setTarget(bm, cit);
        setTarget(sher, maf);
        setTarget(doc, jest);
        setTarget(jan, exec);
        setTarget(sk, doc);
        setTarget(doc2, doc);
        shoot(vig, framer);

        endNight();

        // packageTest();

        vote(sk, exec);
        voteOut(cit, sher, doc, doc2, exec, bm, jan, detect, chf, witch, vig);
        ;
        // cit lynched

        setTarget(lookout, detect);
        setTarget(detect, lookout);
        setTarget(sk, doc);
        mafKill(maf, jan);
        // jan and doc die

        endNight();

        skipDay();
        setTarget(sk, detect);
        mafKill(bm, lookout);
        send(agent, bm);
        // lookout and detective die here

        endNight();

        voteOut(sk, jest, exec, maf, agent, doc2, witch, chf);
        ;
        // sk lynched

        send(maf, bm);
        mafKill(bm, bm);
        // bm dead

        endNight();

        skipDay();
        endNight();

        voteOut(sher, jest, exec, doc2, agent, witch, chf);
        // sher lynched

        mafKill(agent, agent);

        mafKill(agent, agent);

        endNight();

        voteOut(chf, exec, doc2, vig, witch);

        shoot(vig, witch);
        // vig.setTarget(witch);
        mafKill(maf, vig);

        endNight();
        assertEquals(1, maf.getPlayer(narrator).getFaction().size());
        isDead(vig);
        isDead(witch);

        voteOut(jest, exec, doc2, maf);

        endNight();

        assertFalse(narrator.isInProgress());

        // .packageTest();

        // System.out.println(n.getPrivateEvents(false));
    }

    public void testPackaging() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifier.CHARGES, SetupModifiers.UNLIMITED);

        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller mayor = addPlayer(BasicRoles.Mayor());// 6
        Controller sher = addPlayer(BasicRoles.Sheriff());
        Controller vig = addPlayer(BasicRoles.Vigilante());// 8

        Controller agent = addPlayer(BasicRoles.Agent());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller chf = addPlayer(BasicRoles.Chauffeur());
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller jan = addPlayer(BasicRoles.Janitor());// 20
        Controller maf = addPlayer(BasicRoles.Goon());

        Controller exec = addPlayer(BasicRoles.Executioner());// 15
        Controller jest = addPlayer(BasicRoles.Jester()); // 16
        Controller witch = addPlayer(BasicRoles.Witch()); // 17
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        narrator.setSeed(random.nextLong());

        dayStart();

        // packageTest();

        reveal(mayor);
        skipDay();

        // packageTest();

        frame(framer, mayor, framer.getColor());
        drive(bd, exec, jest);
        shoot(vig, witch);
        witch(witch, vig, vig);
        drive(chf, bd, vig);
        setTarget(sher, mayor);

        endNight();
        voteOut(mayor, lookout, detect, doc, doc2, vig, sk, exec, jest, maf, witch);

        setTarget(detect, lookout);
        setTarget(lookout, doc);
        setTarget(agent, cit);
        mafKill(maf, jest);
        setTarget(bm, cit);
        setTarget(sher, maf);
        setTarget(doc, jest);
        setTarget(jan, exec);
        setTarget(sk, doc);
        setTarget(doc2, doc);
        shoot(vig, framer);

        endNight();

        vote(sk, exec);
        voteOut(cit, sher, doc, doc2, exec, bm, jan, detect, chf, witch, vig);
        ;
        // cit lynched

        setTarget(lookout, detect);
        setTarget(detect, lookout);
        setTarget(sk, doc);
        mafKill(maf, jan);
        // jan and doc die

        endNight();

        skipDay();
        setTarget(sk, detect);
        setTarget(bm, lookout, KILL);
        setTarget(agent, bm, SEND);
        // lookout and detective die here

        endNight();

        voteOut(sk, jest, exec, maf, agent, doc2, witch, chf);
        ;
        // sk lynched

        send(maf, bm);
        mafKill(bm, bm);
        // bm dead

        endNight();

        skipDay();
        endNight();

        voteOut(sher, jest, exec, doc2, agent, witch, chf);
        // sher lynched

        // makes sure that when the mafia has no target, no null pointer is thrown
        mafKill(agent, agent);

        endNight();

        voteOut(chf, exec, doc2, vig, witch);

        shoot(vig, witch);
        mafKill(maf, vig);

        endNight();
        assertEquals(1, narrator.getFaction(maf.getColor()).getMembers().size());
        isDead(vig);
        isDead(witch);

        voteOut(jest, exec, doc2, maf);

        endNight();

        assertGameOver();
    }

    public void testEndNightBarring() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        assertTrue(narrator.isFirstNight());

        endNight(witch, sk);
        try{
            setTarget(sk, cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            witch(witch, cit, cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        nextNight();
        assertFalse(narrator.isFirstNight());
    }

    public void testCommandRecording() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Witch());
        Controller p3 = addPlayer(BasicRoles.Jester());

        dayStart();

        vote(p1, p2);
        say(p3, "why thoe votes yo?", Constants.DAY_CHAT);

        List<String> commands = Command.getText(narrator.getCommands());
        assertEquals(7, commands.size());
        assertTrue(commands.get(5).contains("vote"));
        assertTrue(commands.get(6).contains("say"));
    }

    public void testCommandsParsing() {
        SuperTest.checkEnclosingChats = false;
        long seed = random.nextLong();
        // seed = Long.parseLong("-6379197890631150960");

        System.out.println("Text: " + seed);
        setupNarratorForTexting(seed);

        dayStart();
        simulationTest(seed);

        ArrayList<Command> commands = narrator.getCommands();
        Narrator n_old = narrator;
        setupNarratorForTexting(seed);
        StoryPackage sp = NativeStoryPackage.deserialize(n_old);
        narrator = sp.narrator;
        narrator.altRoleAssignment = StoryPackage.GetRoleAssigner(sp);

        narrator.startGame();

        assertTrue(narrator.isStarted());

        Controller newP;
        for(Controller oldP: n_old._players){
            newP = narrator.getPlayerByID(oldP.getPlayer(narrator).getID());
            assertFalse(newP.getPlayer(narrator).role._abilities.contains(UnsetRole.class));
            assertEquals(oldP.getPlayer(narrator).getInitialColor(), newP.getPlayer(narrator).getInitialColor());
            assertEquals(oldP.getPlayer(narrator).getInitialAbilities(),
                    newP.getPlayer(narrator).getInitialAbilities());
        }

        for(Player player: narrator._players)
            player.databaseID = Math.abs(player.getID().hashCode());

        Map<Long, Player> playerDBMap = narrator._players.getDatabaseMap();
        Controller owner;
        CommandHandler th = new CommandHandler(narrator);
        Player submitter;
        try{
            for(Command command: commands){
                if(command.text.length() == 0)
                    continue;

                owner = playerDBMap.get(command.playerID);
                if(owner != null)
                    submitter = owner.getPlayer(narrator);
                else
                    submitter = null;
                th.command(submitter, command.text, "");
            }
        }catch(IllegalActionException | VotingException | PlayerTargetingException | PhaseException
                | NullPointerException e){
            e.printStackTrace();
            writeToFile("log1.txt", n_old);
            writeToFile("log2.txt", narrator);
            TestCase.fail(Long.toString(seed));
            return;
        }

        assertTrue(narrator.getDayNumber() == n_old.getDayNumber());
        assertEquals(narrator.getWinMessage().access(Message.PRIVATE), n_old.getWinMessage().access(Message.PRIVATE));
        Controller q;
        for(Controller p: narrator._players){
            q = n_old.getPlayerByID(p.getPlayer(narrator).getID());
            assertEquals(p.getPlayer(narrator).isAlive(), q.getPlayer(narrator).isAlive());
            assertEquals(p.getPlayer(narrator).isWinner(), q.getPlayer(narrator).isWinner());
        }
    }

    private void setupNarratorForTexting(long seed) {
        newNarrator();
        narrator.setSeed(seed);

        addPlayer(BasicRoles.Framer(), 50);
        addPlayer(Hidden.MafiaRandom(), 20);
        addPlayer(Hidden.YakuzaRandom(), 20);
        addPlayer(Hidden.NeutralRandom(), 10);
    }

    public void testHeaders() {
        Controller p1 = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        nextNight();
        String happenings, oldHappenings;
        assertTrue(narrator.getHappenings().contains("Night 0"));
        assertTrue(narrator.getHappenings().contains("Night 1"));

        oldHappenings = narrator.getHappenings();
        endNight(p1);

        happenings = narrator.getHappenings();
        String happeningsRemainder = happenings.replace(oldHappenings, "");
        assertFalse(happeningsRemainder.contains("Night 1"));

        nextNight();
        assertTrue(narrator.getHappenings().contains("Night 0"));
        assertTrue(narrator.getHappenings().contains("Night 1"));
        assertTrue(narrator.getHappenings().contains("Night 2"));
    }

    public void testActionStackClearedOnGameOver() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Witch());

        mafKill(maf, cit);
        endNight();

        // if i ever were to change this, i need to account for things like amnesiac
        // turning into something on game ending.
        // or else parsing this in the 'state object' will throw errors
        assertActionSize(0, maf);
    }

    public void testLetterer() {
        test("A", 1);
        test("Z", 26);
        test("AA", 27);
        test("AZ", 27 + 25);
    }

    public void testTeams() {
        addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        dayStart();

        assertEquals(2, narrator.getFactions().size());
        ArrayList<Faction> team = narrator.getFactions();
        Collections.sort(team, new Comparator<Faction>() {
            @Override
            public int compare(Faction o1, Faction o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        assertEquals(BasicRoles.Agent().getColor(), team.get(0).getColor());
    }

    private void test(String letter, int i) {
        assertEquals(letter, Computer.toLetter(i));
    }

    public void testDuplicateName() {
        Controller bob = addPlayer("Bob");
        addPlayer("Joe");

        try{
            addPlayer("Bob");
            fail();
        }catch(NamingException e){
        }

        try{
            bob.setName("joe");
            fail();
        }catch(NamingException e){
        }
    }

    public void testReplaceAction() {
        Controller gunsmith = addPlayer(BasicRoles.Gunsmith());
        Controller mafioso = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());

        nightStart();
        setTarget(gunsmith, cit);
        Action a = gunsmith.getPlayer(narrator).getAction(Gunsmith.MAIN_ABILITY);
        Action newAction = new Action(gunsmith.getPlayer(narrator), Gunsmith.MAIN_ABILITY, mafioso.getPlayer(narrator));
        gunsmith.getPlayer(narrator).getActions().replace(a, newAction);

        endNight();
        assertTotalGunCount(1, mafioso);
        Command newCommand = new Command(gunsmith.getPlayer(narrator), Gunsmith.COMMAND + " " + mafioso.getName());
        List<Command> commands = narrator.getCommands();
        assertTrue(commands.get(6).equals(newCommand));
        assertTrue(commands.contains(newCommand));
    }

    public void testSheriffCleanup() {
        addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Citizen());

        dayStart();

        for(Faction t: narrator.getFactions()){
            assertTrue(t.getSheriffDetectables().isEmpty());
        }
    }

    public void testSheriffCleanup2() {
        addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Citizen());

        dayStart();

        for(Faction t: narrator.getFactions()){
            if(t.getColor().equals(BasicRoles.Sheriff().getColor()))
                assertFalse(t.getSheriffDetectables().isEmpty());
            else
                assertTrue(t.getSheriffDetectables().isEmpty());
        }
    }

    // testName
    // badname
    public void testReservedNames() {
        String nameSplit = ":\t";

        tryName(Armorsmith.FAKE);
        tryName(Gunsmith.REAL);
        tryName(DrugDealer.BLOCKED);
        tryName(Constants.PUNCH);
        tryName(Ventriloquist.VENT_PUNCH);
        tryName(Ventriloquist.VENT_SKIP_VOTE);
        tryName(Ventriloquist.VENT_UNVOTE);
        tryName(Ventriloquist.VENT_VOTE);
        tryName(Setup.TOWN_C);
        tryName(nameSplit);
        tryName("Doug" + nameSplit);
        tryName("Doug" + nameSplit + "Bow");
        tryName("");
        tryName(null);
        tryName("boy.ohboy");
        tryName("boy---ohboy");
        tryName("boy$ohboy");
        tryName("doug" + ArchitectChat.KEY_SEPERATOR + "george");
        tryName("1");
        ArrayList<Faction> teams = narrator.getFactions();
        for(Faction t: teams){
            tryName(t.getName());
            tryName(t.getColor());
        }
    }

    public void testNightless() {
        editRule(SetupModifier.NIGHT_LENGTH, 0);
        assertEqual(0, narrator.getInt(SetupModifier.NIGHT_LENGTH));

        Role role = RoleService.createRole(narrator, "TakedownJester", Jester.class, TeamTakedown.class);
        Faction faction = narrator.getFaction(Setup.BENIGN_C);

        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Bomb());
        addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Executioner());
        addPlayer(BasicRoles.Bulletproof());
        addPlayer(BasicRoles.Mason());
        addPlayer(FactionService.createFactionRole(faction, role));

        editRule(SetupModifier.JESTER_CAN_ANNOY, false);
        editRule(SetupModifier.JESTER_KILLS, 1);
        editRule(SetupModifier.DAY_START, false);

        assertBadGameSettings();

        editRule(SetupModifier.DAY_START, true);

        assertBadGameSettings();

        setTeamRule(Setup.MAFIA_C, FactionModifier.HAS_NIGHT_CHAT, false);

        assertBadGameSettings();

        removeTeamAbility(Setup.MAFIA_C, FactionKill.class);
        assertFalse(narrator.getFaction(Setup.MAFIA_C).hasSharedAbilities());

        editRule(SetupModifier.JESTER_CAN_ANNOY, true);

        assertBadGameSettings();

        editRule(SetupModifier.JESTER_CAN_ANNOY, false);

        addPlayer(BasicRoles.Sheriff());
        editRule(SetupModifier.SHERIFF_PREPEEK, false);

        assertBadGameSettings();

        editRule(SetupModifier.SHERIFF_PREPEEK, true);

        Controller p = addPlayer(BasicRoles.Spy());

        assertBadGameSettings();

        SetupHidden setupHidden = LookupUtil.findSetupHidden(narrator, "Spy");
        SetupHiddenService.deleteSetupHidden(narrator, setupHidden);
        addRole(BasicRoles.Citizen());

        dayStart();

        try{
            skipVote(p);
            fail();
        }catch(NarratorException e){
        }

        for(Controller x: narrator._players){
            if(x == p)
                continue;
            vote(x, p);
            if(p.getPlayer(narrator).isDead())
                break;
        }

        assertIsDay();
    }

    public void testEndGameNightless() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Goon());

        removeTeamAbility(Setup.MAFIA_C, FactionKill.class);
        assertFalse(narrator.getFaction(Setup.MAFIA_C).hasSharedAbilities());
        editRule(SetupModifier.NIGHT_LENGTH, 0);

        dayStart();

        vote(p1, p3);
        vote(p2, p3);

        assertGameOver();
    }

    public void testNameTargeting() {
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Vigilante());

        nightStart();

        command(sk, SerialKiller.COMMAND + " 3");
        endNight();

        assertEquals(1, narrator.getDeadSize());
    }

    public void testCapitalization() {
        Controller a = addPlayer(BasicRoles.Citizen()).setName("a");
        Controller b = addPlayer(BasicRoles.Citizen()).setName("B");
        Controller c = addPlayer(BasicRoles.Goon()).setName("c");
        Controller d = addPlayer(BasicRoles.Godfather()).setName("D");

        nightStart();

        assertTrue(a == narrator.getPlayerByName("1"));
        assertTrue(b == narrator.getPlayerByName("2"));
        assertTrue(c == narrator.getPlayerByName("3"));
        assertTrue(d == narrator.getPlayerByName("4"));
    }

    private void tryName(String name) {
        try{
            narrator.addPlayer(name);
            fail();
        }catch(NamingException e){
        }
    }
}
