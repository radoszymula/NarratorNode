package integration.logic;

import game.ai.Controller;
import game.logic.Faction;
import game.logic.Role;
import game.logic.exceptions.NarratorException;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Goon;
import game.roles.TeamTakedown;
import game.setups.Setup;
import models.FactionRole;
import services.FactionService;
import services.RoleAbilityModifierService;
import services.RoleService;

public class TestTeamTakedown extends SuperTest {

    public TestTeamTakedown(String name) {
        super(name);
    }

    public void testBasic() {
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Role role = RoleService.createRole(narrator, "Egg", Goon.class, TeamTakedown.class);
        FactionRole mashM = FactionService.createFactionRole(mafia, role);

        Controller mash = addPlayer(mashM);
        Controller agent = addPlayer(BasicRoles.Agent());
        Controller agent2 = addPlayer(BasicRoles.Agent());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.MAYOR_VOTE_POWER, 4);

        reveal(mayor);
        voteOut(mash, mayor);

        setTarget(agent, agent2);
        setTarget(agent2, agent);
        endNight();

        assertEquals(2, narrator.getDeadSize());
        assertTrue(agent.getPlayer(narrator).isDead() || agent2.getPlayer(narrator).isDead());
        assertFalse(agent.getPlayer(narrator).isDead() && agent2.getPlayer(narrator).isDead());
    }

    public void testSingle() {
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Role role = RoleService.createRole(narrator, "Egg", Goon.class, TeamTakedown.class);
        FactionRole mashM = FactionService.createFactionRole(mafia, role);

        Controller mash = addPlayer(mashM);
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen(), 2);

        setTarget(sk, mash);
        endNight();

        isDead(mash);
        isAlive(sk);
        assertEquals(3, narrator.getLiveSize());
    }

    public void testDouble() {
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Role role = RoleService.createRole(narrator, "Egg", Goon.class, TeamTakedown.class);
        FactionRole mashM = FactionService.createFactionRole(mafia, role);

        Controller mash = addPlayer(mashM);
        addPlayer(BasicRoles.Agent());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen(), 2);

        setTarget(sk, mash);
        endNight();

        isDead(mash);
        isAlive(sk);
        assertEquals(4, narrator.getLiveSize());
    }

    public void testLargeTeam() {
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Role role = RoleService.createRole(narrator, "Egg", Goon.class, TeamTakedown.class);
        FactionRole mashM = FactionService.createFactionRole(mafia, role);

        Controller mash = addPlayer(mashM);
        addPlayer(BasicRoles.Agent(), 5);

        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen(), 2);

        setTarget(sk, mash);
        endNight();

        isDead(mash);
        isAlive(sk);
        assertEquals(6, narrator.getLiveSize());
    }

    public void testLargeTeamDay() {
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Role role = RoleService.createRole(narrator, "Egg", Goon.class, TeamTakedown.class);
        FactionRole mashM = FactionService.createFactionRole(mafia, role);

        Controller mash = addPlayer(mashM);
        addPlayer(BasicRoles.Agent(), 6);
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifier.MAYOR_VOTE_POWER, 6);

        reveal(mayor);
        voteOut(mash, mayor);

        endNight();

        isDead(mash);
        assertEquals(4, narrator.getDeadSize());
    }

    public void testHealMassSuicide() {
        Faction mafia = narrator.getFaction(Setup.MAFIA_C);
        Role role = RoleService.createRole(narrator, "Egg", TeamTakedown.class);
        FactionRole mashM = FactionService.createFactionRole(mafia, role);

        Controller doc1 = addPlayer(BasicRoles.Doctor());
        Controller cultLeader = addPlayer(mashM);
        Controller cultist1 = addPlayer(BasicRoles.Cultist());
        Controller cultist2 = addPlayer(BasicRoles.Cultist());
        Controller doc2 = addPlayer(BasicRoles.Doctor());

        voteOut(cultLeader, doc1, doc2, cultist1, cultist2);

        setTarget(doc1, cultist1);
        setTarget(doc2, cultist2);
        endNight();

        isAlive(doc1, doc2, cultist1, cultist2);
    }

    public void testNoBackToBackModifier() {
        try{
            Role role = RoleService.createRole(narrator, "Egg", TeamTakedown.class);
            RoleAbilityModifierService.modify(role, TeamTakedown.class, AbilityModifier.BACK_TO_BACK, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            Role role = RoleService.createRole(narrator, "Egg", TeamTakedown.class);
            RoleAbilityModifierService.modify(role, TeamTakedown.class, AbilityModifier.ZERO_WEIGHTED, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
