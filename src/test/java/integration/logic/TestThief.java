package integration.logic;

import java.util.ArrayList;
import java.util.HashMap;

import game.ai.Computer;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.support.CustomRoleAssigner;
import game.logic.support.Random;
import game.logic.support.RolePackage;
import game.logic.support.action.Action;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.FactionModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Citizen;
import game.roles.FactionKill;
import game.roles.Goon;
import game.roles.Thief;
import game.setups.LiarsClub;
import game.setups.Setup;
import junit.framework.ComparisonFailure;
import models.FactionRole;
import models.SetupHidden;
import models.enums.GamePhase;
import models.enums.VoteSystemTypes;
import services.FactionRoleService;
import services.RoleService;
import util.LookupUtil;

public class TestThief extends SuperTest {

    public TestThief(String name) {
        super(name);
    }

    @Override
    public void roleInit() {
        super.addRole(Thief.template(narrator.getFaction(Setup.BENIGN_C)));
        int townPriority = narrator.getFaction(BasicRoles.Citizen().getColor()).getPriority();
        setTeamRule(BasicRoles.Goon().getColor(), FactionModifier.WIN_PRIORITY, townPriority);
    }

    public void testGameStartWithMoreRoles2() {
        addPlayer(BasicRoles.Citizen(), 5);
        addPlayer(BasicRoles.Goon(), 2);
        addPlayer(BasicRoles.Jester());
        addRole(BasicRoles.Jester());

        dayStart();
    }

    public void testThiefSetupMin5() {
        Setup setup = Setup.GetSetup(narrator, LiarsClub.KEY);
        assertEquals(8, setup.getMinPlayerCount());
        for(int j = 0; j < setup.getMinPlayerCount(); j++){
            narrator.addPlayer(Computer.toLetter(j + 1));
        }
        setup.applyRolesList();
        editRule(SetupModifier.VOTE_SYSTEM, VoteSystemTypes.PLURALITY);
        narrator.startGame();
    }

    public void testGameStartWithMoreRoles3() {
        addPlayer(BasicRoles.Citizen(), 6);
        addPlayer(BasicRoles.Goon(), 2);
        addPlayer(BasicRoles.Jester());
        addRole(BasicRoles.Jester());

        dayStart();
    }

    public void testThiefDoesntMatter() {
        addPlayer(BasicRoles.Citizen(), 5);
        addPlayer(BasicRoles.Goon(), 2);

        try{
            dayStart();
            fail();
        }catch(NarratorException e){
        }
    }

    public void testGameStartWithNotEnoughEnemies() {
        addPlayer(BasicRoles.Citizen(), 5);
        addPlayer(BasicRoles.Goon(), 1);
        addRole(BasicRoles.Jester());

        try{
            dayStart();
            fail();
        }catch(NarratorException e){
        }
    }

    public void testGameStartWithNotEnoughEnemies2() {
        addPlayer(BasicRoles.Citizen(), 5);
        addPlayer(BasicRoles.Goon(), 2);
        addRole(BasicRoles.Jester());
        addRole(BasicRoles.Jester());

        try{
            dayStart();
            fail();
        }catch(NarratorException e){
        }
    }

    public void testUniqueThief() {
        addPlayer(BasicRoles.Citizen(), 4);
        addPlayer(BasicRoles.Goon(), 4);
        addRole(BasicRoles.Jester());
        super.addRole(BasicRoles.Thief());

        try{
            dayStart();
            fail();
        }catch(NarratorException e){
        }
    }

    public void testThiefCantHaveEnemies() {
        addPlayer(BasicRoles.Citizen(), 4);
        addPlayer(BasicRoles.Goon(), 4);
        addRole(BasicRoles.Jester());
        RoleService.deleteRole(LookupUtil.findRole(narrator, "Thief"));
        addRole(Thief.template(narrator.getFaction(Setup.MAFIA_C)));

        assertBadGameSettings();
    }

    public void testThiefPick() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Goon());
        Controller c4 = addPlayer(BasicRoles.Goon());
        ControllerList controllerList = ControllerList.list(c1, c2, c3, c4);
        addRole(BasicRoles.Citizen());

        setThiefAssigner(controllerList);

        nightStart();

        assertEquals(GamePhase.ROLE_PICKING_PHASE, narrator.phase);
        assertTrue(c1.is(Thief.class));

        pickRole(c1, BasicRoles.Goon());
        pickRole(c1, BasicRoles.Citizen());
        endPhase();

        assertEquals(GamePhase.NIGHT_ACTION_SUBMISSION, narrator.phase);
        assertTrue(c1.is(Citizen.class));

        String happenings = narrator.getHappenings();
        String thiefPickText = c1.getName() + " became a Town Citizen";
        assertTrue(happenings.contains(thiefPickText));
    }

    public void testHappeningTextDay() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Goon());
        Controller c4 = addPlayer(BasicRoles.Goon());
        ControllerList controllerList = ControllerList.list(c1, c2, c3, c4);
        addRole(BasicRoles.Citizen());

        setThiefAssigner(controllerList);

        dayStart();

        assertEquals(GamePhase.ROLE_PICKING_PHASE, narrator.phase);
        assertTrue(c1.is(Thief.class));

        pickRole(c1, BasicRoles.Goon());
        pickRole(c1, BasicRoles.Citizen());
        endPhase();

        assertTrue(c1.is(Citizen.class));

        String happenings = narrator.getHappenings();
        String thiefPickText = c1.getName() + " became a Town Citizen";
        assertTrue(happenings.contains(thiefPickText));
    }

    public void testHappeningTextNight() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Goon());
        Controller c4 = addPlayer(BasicRoles.Goon());
        ControllerList controllerList = ControllerList.list(c1, c2, c3, c4);
        addRole(BasicRoles.Citizen());

        setThiefAssigner(controllerList);

        nightStart();

        assertEquals(GamePhase.ROLE_PICKING_PHASE, narrator.phase);
        assertTrue(c1.is(Thief.class));

        pickRole(c1, BasicRoles.Goon());
        pickRole(c1, BasicRoles.Citizen());
        endPhase();

        assertEquals(GamePhase.NIGHT_ACTION_SUBMISSION, narrator.phase);
        assertTrue(c1.is(Citizen.class));

        String happenings = narrator.getHappenings();
        String thiefPickText = c1.getName() + " became a Town Citizen";
        assertTrue(happenings.contains(thiefPickText));
    }

    public void testBadRequests() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Goon());
        Controller c4 = addPlayer(BasicRoles.Goon());
        ControllerList controllerList = ControllerList.list(c1, c2, c3, c4);
        addRole(BasicRoles.Citizen());

        setThiefAssigner(controllerList);

        nightStart();

        assertEquals(GamePhase.ROLE_PICKING_PHASE, narrator.phase);
        assertTrue(c1.is(Thief.class));

        try{
            pickRole(c1, BasicRoles.Thief());
            fail();
        }catch(NarratorException e){
        }
        try{
            c1.setNightTarget(Thief.COMMAND, "Mayor");
            fail();
        }catch(NarratorException e){
        }
        try{
            setTarget(c1, c2);
            fail();
        }catch(NarratorException e){
        }
        try{
            setTarget(c1);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testPickingMafiaTeam() {
        String mafiaColor = BasicRoles.Goon().getColor();
        Controller thief = addPlayer(BasicRoles.Citizen());
        Controller goon = addPlayer(BasicRoles.Citizen());
        Controller cit1 = addPlayer(BasicRoles.Goon());
        Controller cit2 = addPlayer(BasicRoles.Goon());
        ControllerList controllerList = ControllerList.list(thief, goon, cit1, cit2);
        addRole(BasicRoles.Citizen());

        setThiefAssigner(controllerList);

        nightStart();

        assertEquals(GamePhase.ROLE_PICKING_PHASE, narrator.phase);
        assertTrue(thief.is(Thief.class));

        pickRole(thief, BasicRoles.Goon());
        endPhase();

        assertEquals(GamePhase.NIGHT_ACTION_SUBMISSION, narrator.phase);
        assertTrue(thief.is(Goon.class));
        // this will ensure when executioners targets are being picked, the former thief
        // is elligible to be a target
        assertEquals(2, narrator.getFaction(mafiaColor)._startingSize);

        setTarget(thief, cit1, FactionKill.MAIN_ABILITY);
        say(thief, gibberish(), mafiaColor);
        partialContains(thief, gibberish);
        partialContains(goon, gibberish);
    }

    public void testThiefNotPicking() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Goon());
        Controller c4 = addPlayer(BasicRoles.Goon());
        ControllerList controllerList = ControllerList.list(c1, c2, c3, c4);
        addRole(BasicRoles.Citizen());

        setThiefAssigner(controllerList);

        nightStart();

        assertEquals(GamePhase.ROLE_PICKING_PHASE, narrator.phase);
        assertTrue(c1.is(Thief.class));

        endPhase();

        assertEquals(GamePhase.NIGHT_ACTION_SUBMISSION, narrator.phase);
        assertFalse(c1.is(Thief.class));
    }

    public void testForcedChoice() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Goon());
        Controller c4 = addPlayer(BasicRoles.Goon());
        ControllerList controllerList = ControllerList.list(c1, c2, c3, c4);
        addRole(BasicRoles.Citizen());

        int townPriority = narrator.getFaction(BasicRoles.Citizen().getColor()).getPriority();
        setTeamRule(BasicRoles.Goon().getColor(), FactionModifier.WIN_PRIORITY, townPriority + 1);

        setThiefAssigner(controllerList);

        nightStart();

        try{
            pickRole(c1, BasicRoles.Citizen());
            fail();
        }catch(NarratorException e){
            assertEquals(
                    "Roles with lower priority are not allowed to be picked.  'Mafia' or 'Goon' are the only allowed choices.",
                    e.getMessage());
        }

        Player thief = c1.getPlayer(narrator);
        Ability thiefAbility = thief.getAbility(Thief.class);

        Action exampleAction = thiefAbility.getExampleAction(c1.getPlayer(narrator));
        assertEquals(0, exampleAction.getTargets().size());
        assertNotNull(exampleAction.getOption());

        String usage = thiefAbility.getUsage(thief, new Random());
        try{
            assertEquals(usage, Thief.COMMAND + " Citizen");
        }catch(ComparisonFailure e){
            assertEquals(usage, Thief.COMMAND + " Goon");
        }
    }

    private void pickRole(Controller c, FactionRole role) {
        c.setNightTarget(Thief.COMMAND, role.getName());
    }

    private void setThiefAssigner(ControllerList controllerList) {
        narrator.altRoleAssignment = new CustomRoleAssigner() {

            @Override
            public void assign() {
                HashMap<String, ArrayList<RolePackage>> keyToCount = new HashMap<>();
                String key;
                for(SetupHidden setupHidden: narrator.rolesList){
                    key = setupHidden.hidden.toString();
                    if(!keyToCount.containsKey(setupHidden.hidden.toString()))
                        keyToCount.put(key, new ArrayList<>());
                    keyToCount.get(key).add(setupHidden.hidden.generateRole(narrator));
                }
                HashMap<Integer, String> countToKey = new HashMap<>();
                for(String cKey: keyToCount.keySet())
                    countToKey.put(keyToCount.get(cKey).size(), cKey);

                String thiefKey = countToKey.get(1);
                String goonKey = countToKey.get(2);
                String citKey = countToKey.get(3);

                keyToCount.get(thiefKey).get(0).assign(controllerList.get(0).getPlayer(narrator));
                keyToCount.get(goonKey).get(0).assign(controllerList.get(1).getPlayer(narrator));
                keyToCount.get(citKey).get(0).assign(controllerList.get(2).getPlayer(narrator));
                keyToCount.get(citKey).get(1).assign(controllerList.get(3).getPlayer(narrator));
            }
        };
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Thief(), AbilityModifier.BACK_TO_BACK, Thief.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Thief(), AbilityModifier.ZERO_WEIGHTED, Thief.class, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
