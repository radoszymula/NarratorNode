package integration.logic;

import game.ai.Controller;
import game.logic.Faction;
import game.logic.Role;
import game.logic.templates.BasicRoles;
import game.roles.Ability;
import game.roles.Scout;
import game.setups.Setup;
import services.FactionService;
import services.RoleService;

public class TestScout extends SuperTest {

    public TestScout(String s) {
        super(s);
    }

    Controller scout;

    @Override
    public void roleInit() {
        Faction faction = narrator.getFaction(Setup.CULT_C);
        Role role = RoleService.createRole(narrator, "Scout", Scout.class);
        scout = this.addPlayer(FactionService.createFactionRole(faction, role));
    }

    public void testBasicAbility() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller citizen = addPlayer(BasicRoles.Citizen());

        notRecruitable(sheriff);
        isRecruitable(citizen);
    }

    private void notRecruitable(Controller p) {
        if(narrator.isDay())
            skipDay();
        setTarget(scout, p);
        endNight();

        partialContains(scout, Scout.NOT_RECRUITABLE);
    }

    private void isRecruitable(Controller p) {
        if(narrator.isDay())
            skipDay();
        setTarget(scout, p);
        endNight();

        partialContains(scout, Scout.RECRUITABLE);
    }

    public static void seen(Controller p, Class<? extends Ability> c) {
        partialContains(p, c.getSimpleName() + ".");
    }

    public static void notSeen(Controller p, Class<? extends Ability> c) {
        partialExcludes(p, c.getSimpleName() + ".");
    }
}
