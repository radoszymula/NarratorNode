package integration.logic;

import game.ai.Controller;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.VotingException;
import game.logic.templates.BasicRoles;
import game.roles.Blackmailer;
import game.roles.Disfranchise;
import game.roles.FactionSend;
import game.roles.Goon;
import game.roles.Silence;
import game.setups.Setup;

public class TestBlackmailer extends SuperTest{

	public TestBlackmailer(String name) {
		super(name);
	}
	
	public void testBlackmailer(){
		addPlayer(BasicRoles.Citizen(), 2);
    	Controller cit2    = addPlayer(BasicRoles.Citizen());
    	Controller bm_r    = addPlayer(BasicRoles.Blackmailer());
    	Controller maf_r_d = addPlayer(BasicRoles.Goon());
    	Controller bm_o_e  = addPlayer(BasicRoles.getMember(Setup.YAKUZA_C, new Blackmailer()));
    	
    	setTarget(bm_r, bm_o_e);
    	setTarget(bm_o_e, maf_r_d);//red maf bmed
    	
    	endNight();
   
    	partialContains(maf_r_d, Silence.FEEDBACK);
    	partialContains(maf_r_d, Disfranchise.FEEDBACK);
    	
    	//players can't vote other players
    	try{
    		vote(bm_o_e, bm_r);
    		fail();
    	} catch(VotingException e){}
    	
    	//only allowed to skip the night
    	skipDay();
    	
    	assertStatus(maf_r_d, Blackmailer.MAIN_ABILITY);
    	
    	//neither players can vote anyone to send for the kill
    	try{
    		setTarget(maf_r_d, bm_r, SEND);
    		fail();
    	}catch(PlayerTargetingException e){}
    	
    	setTarget(bm_o_e, maf_r_d);//blackmailing, not killing
    	
    	//setTarget(bm_r, bm_r, SEND);
    	setTarget(bm_r, bm_o_e, KILL);//bm_o won't be able to bm maf because he died before he could get to him
    	
    	endNight();
    	
    	assertStatus(maf_r_d, Blackmailer.MAIN_ABILITY, false);
    	vote(maf_r_d, cit2);
    }
    
    public void testBlackmailedTeam(){
    	Controller bm1 = addPlayer(BasicRoles.Blackmailer());
    	Controller bm2 = addPlayer(BasicRoles.Blackmailer());
    	Controller r1 = addPlayer(BasicRoles.getMember(Setup.YAKUZA_C, new Blackmailer()));
    	Controller r2 = addPlayer(BasicRoles.getMember(Setup.YAKUZA_C, new Blackmailer()));
    	Controller r3 = addPlayer(BasicRoles.getMember(Setup.YAKUZA_C, new Goon()));
    	
    	nightStart();
    	
    	setTarget(bm1, r1);
    	setTarget(bm2, r2);
    	
    	endNight();
    	skipDay();
    	
    	if(r2.getPlayer(narrator).isDead())
    		r2 = r3;
    	else
    		r1 = r3;
    	
    	setTarget(bm1, r1);
    	setTarget(bm2, r2);
    	setTarget(r1, bm1, KILL);
    	
    	endNight();
    	isDead(bm1);
    	
    }
    
    /*
     * also tests that the blackmail wear off
     */
    public void testMultiTeamSubmission(){
    	Controller cit1 = addPlayer(BasicRoles.Citizen());
    	Controller cit2 = addPlayer(BasicRoles.Citizen());
    	Controller cit3 = addPlayer(BasicRoles.Citizen());
    	Controller bm   = addPlayer(BasicRoles.Blackmailer());
    	
    	setTarget(bm, cit1);
    	endNight();
    	
    	try{
    		vote(cit1, bm);
    		fail();
    	}catch(VotingException e){}
    	try{
    		unvote(cit1);
        	fail();
    	}catch(VotingException e){}
    	
    	skipDay();
    	endNight();

    	vote(cit1, bm);
    	skipVote(cit3);
		
		skipDay();
		//multi submission
    	
    	setTarget(bm, cit1);
    	setTarget(bm, cit2, KILL);
    	
    	try{
    		setTarget(bm, bm, FactionSend.MAIN_ABILITY);
    		fail();
    	}catch(PlayerTargetingException e){}
    	
    	endNight();
    	
    	isDead(cit2);
    }
}
