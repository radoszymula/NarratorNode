package integration.logic;

import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.rules.AbilityModifier;
import game.logic.support.rules.SetupModifier;
import game.logic.templates.BasicRoles;
import game.roles.DrugDealer;
import game.roles.Survivor;
import game.roles.support.Vest;
import services.FactionRoleService;

public class TestSurvivor extends SuperTest {
    public TestSurvivor(String name) {
        super(name);
    }

    private static void setVests(int i) {
        modifyRole(BasicRoles.Survivor(), AbilityModifier.CHARGES, i);
        editRule(SetupModifier.CHARGE_VARIABILITY, 0);
    }

    public void testBasicSurvivor() {
        setVests(4);

        Controller c = addPlayer(BasicRoles.Citizen());
        Controller v = addPlayer(BasicRoles.Vigilante());
        Controller s = addPlayer(BasicRoles.Survivor());
        Controller goon = addPlayer(BasicRoles.Goon());

        nightStart();

        assertNull(s.getPlayer(narrator).getAcceptableTargets(Vest.COMMAND));

        // basic role card checks
        assertTrue(s.getPlayer(narrator).getRoleName().equals(Survivor.class.getSimpleName()));
        assertTrue(s.getPlayer(narrator).getRoleSpecs().contains(Survivor.GetNightActionDescription(narrator)));

        // since citizens are the basis for survivors, need to check that citizens can't
        // use vests without actually having vests
        try{
            vest(c);
            fail();
        }catch(PlayerTargetingException e){
        }
        assertTrue(s.getCommands().get(0).equals(Constants.VEST_COMMAND));
        vest(s);
        setTarget(v, s, GUN);
        mafKill(goon, s);

        endNight();

        isAlive(s);
        assertFalse(s.getPlayer(narrator).isVesting());
        assertInProgress();

        voteOut(c, v, s, goon);

        mafKill(goon, v);
        setTarget(v, goon, GUN);
        endNight();

        assertGameOver();
        isWinner(s);
        isLoser(goon, c);
    }

    public void testMMWitchVisit() {
        setVests(1);

        addPlayer(BasicRoles.Citizen());
        Controller s = addPlayer(BasicRoles.Survivor());
        Controller w = addPlayer(BasicRoles.Witch());
        Controller mm = addPlayer(BasicRoles.MassMurderer());

        vest(s);
        setTarget(mm, mm);
        witch(w, s, mm);

        endNight();

        isAlive(s);

        skipDay();
        setTarget(mm, mm);
        witch(w, s, mm);

        endNight();

        isDead(s);
    }

    // also tests usage
    public void testCultLeaderConversions() {
        setVests(1);
        Controller s = addPlayer(BasicRoles.Survivor());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();

        assertTotalVestCount(1, s);

        setTarget(cl, s);
        endNight();
        skipDay();
        setTarget(sk, s);
        vest(s);
        endNight();

        assertInProgress();

        skipDay();
        setTarget(sk, s);
        try{
            vest(s);
            fail();
        }catch(PlayerTargetingException e){
        }
        endNight();

        assertGameOver();
    }

    public void testBlockVest() {

        addPlayer(BasicRoles.Citizen());
        Controller a = addPlayer(BasicRoles.Agent());
        Controller c = addPlayer(BasicRoles.Consort());
        Controller s = addPlayer(BasicRoles.Survivor());

        nightStart();
        vest(s);
        mafKill(a, s);
        setTarget(c, s);

        endNight();

        isDead(s);
    }

    public void testArsonAndVest() {

        addPlayer(BasicRoles.Citizen());
        Controller s = addPlayer(BasicRoles.Survivor());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        nightStart();

        setTarget(a1, s);
        setTarget(a2);
        vest(s);

        endNight();

        isDead(s);
    }

    public void testDrugBlockFail() {
        setVests(1);

        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller surv = addPlayer(BasicRoles.Survivor());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller dd2 = addPlayer(BasicRoles.DrugDealer());
        Controller cons = addPlayer(BasicRoles.Consort());

        setTarget(cons, surv);
        vest(surv);
        drug(dd, surv, DrugDealer.BLOCKED);

        endNight();

        assertTotalVestCount(1, surv);
        assertFakeVestCount(0, surv);
        assertRealVestCount(1, surv);

        skipDay();

        vest(surv);
        setTarget(sk, surv);
        drug(dd, surv, DrugDealer.BLOCKED);
        drug(dd2, surv, DrugDealer.BLOCKED);

        endNight();

        isAlive(surv);
        assertTotalVestCount(1, surv);
        assertFakeVestCount(1, surv);
        assertRealVestCount(0, surv);

        skipDay();

        assertTrue(surv.getPlayer(narrator).isAcceptableTarget(getVestAction(surv.getPlayer(narrator))));

        vest(surv);
        setTarget(sk, surv);

        endNight();

        isDead(surv);
    }

    public void testNoImmunityOnGameOver() {
        Controller survivor = addPlayer(BasicRoles.Survivor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());

        mafKill(maf, cit);
        vest(survivor);
        endNight();

        isInvuln(survivor, false);
    }

    public void testSurvivorImmunityJesterKill() {
        Controller survivor = addPlayer(BasicRoles.Survivor());
        Controller jester = addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Mason());
        addPlayer(BasicRoles.Mason());

        vote(survivor, jester);
        narrator.forceEndDay();

        vest(survivor);
        endNight();

        isInvuln(survivor, false);

        skipDay();
        for(int i = 0; i < 5; i++){
            nextNight();
        }

        assertAttackSize(1, survivor);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Survivor(), AbilityModifier.BACK_TO_BACK, Survivor.class, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleService.addModifier(BasicRoles.Survivor(), AbilityModifier.ZERO_WEIGHTED, Survivor.class, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
