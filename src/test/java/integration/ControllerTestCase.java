package integration;

import integration.server.SwitchWrapper;
import junit.framework.TestCase;

public abstract class ControllerTestCase extends TestCase {

    @Override
    public void runTest() throws Throwable {
        SwitchWrapper.init();
        super.runTest();
    }
}
